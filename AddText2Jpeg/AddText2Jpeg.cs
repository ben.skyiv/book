using System.Drawing;

sealed class AddText2Jpeg
{
    static void Main()
    {
        var img = Image.FromFile("/home/ben/src/book/CTF/2018-11-27/Misc-3/2.jpg");
        var g = Graphics.FromImage(img);
        var font = new Font("宋体", 128);
        var brush = new SolidBrush(Color.Red);
        var p = new PointF(30, 30);
        g.DrawString("中国123", font, brush, p);
        img.Save("/tmp/a.jpg");
    }
}
