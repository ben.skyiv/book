using System;
using System.Text;
using Skyiv.Utils;

static class Str2oct
{
  static readonly Encoding[] encodes = {
    Encoding.UTF8, Encoding.Unicode, Encoding.GetEncoding("GB18030")
  };

  static string ToOctString(this byte[] bs)
  {
    var sb = new StringBuilder();
    foreach (var n in bs) sb.AppendFormat("\\{0}",
      ((long)n).ToStringBase(8).PadLeft(3, '0'));
    return sb.ToString();
  }

  static void Main(string[] args)
  {
    var s = (args.Length > 0) ? args[0] : "测试";
    Console.WriteLine(s);
    foreach (var encode in encodes) {
      var bs = encode.GetBytes(s);
      Console.WriteLine("{0,-27}: {1}", encode, bs.ToOctString());
    }
  }
}
