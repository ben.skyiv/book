#include <stdio.h>
#include <gmp.h>

int main(void)
{
  printf("GMP_Version: %s\n", gmp_version);
  printf("limb_bits  : %d\n", mp_bits_per_limb);
  printf("GMP_CC     : %s\n", __GMP_CC);
  printf("GMP_CFLAGS : %s\n", __GMP_CFLAGS);
  return 0;
}

