using System;
using System.IO;
using System.Collections.Generic;

var dict = new Dictionary<string, int>();
var total = 0;
for (; ; total++) {
  var s = Console.ReadLine();
  if (s == null) break;
  int n;
  dict.TryGetValue(s, out n);
  dict[s] = n + 1;
}
Console.WriteLine("Total : {0:N0}", total);
Console.WriteLine("Unique: {0:N0}", dict.Count);
var cnts = new Dictionary<int, int>();
foreach (var kvp in dict) {
  int n;
  cnts.TryGetValue(kvp.Value, out n);
  cnts[kvp.Value] = n + 1;
}
foreach (var kvp in cnts)
  Console.WriteLine("> {0,4}: {1:N0}", kvp.Key, kvp.Value);
