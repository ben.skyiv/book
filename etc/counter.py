#!/usr/bin/env python3
from collections import Counter
s = Counter()
s.update(input())
s = ''.join(c for c, _ in s.most_common())
n = s.find('}') + 1
print(s[:None if n == 0 else n])
