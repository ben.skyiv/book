#include <stdio.h>
#include <float.h>
#include <stdlib.h>

#define PR(x) printf("%11s: %2lu bytes\n", #x, (unsigned long)sizeof(x))

int main(void)
{
  PR(size_t);
  PR(void *);
  PR(char);
  PR(short);
  PR(int);
  PR(long);
  PR(long long);
  PR(__int128);
  PR(float);
  PR(double);
  PR(long double);
  PR(__float128);
  printf("\n---- DIG -------EPSILON -----------MIN -----------MAX\n");
  printf(" FLT %3d %-14E %-14E %-14E\n", FLT_DIG, FLT_EPSILON, FLT_MIN, FLT_MAX);
  printf(" DBL %3d %-14E %-14E %-14E\n", DBL_DIG, DBL_EPSILON, DBL_MIN, DBL_MAX);
  printf("LDBL %3d %-14LE %-14LE %-14LE\n", LDBL_DIG, LDBL_EPSILON, LDBL_MIN, LDBL_MAX);
  printf("---- --- -------------- -------------- --------------\n");
  printf("RAND_MAX: %d\n", RAND_MAX);
  return 0;
}
