import System.Environment ( getArgs )
import Math.NumberTheory.Primes.Factorisation ( factorise )
main = do s <- getArgs; print . factorise . read . head $ s

