using System;

sealed class DeatTimeDiff
{
  static void Main(string[] args)
  {
    if (args.Length == 2)
    {
      var dt1 = DateTime.Parse(args[0]);
      var dt2 = DateTime.Parse(args[1]);
      Console.WriteLine("[{0}] [{1}] [{2}]", dt1, dt2, dt2-dt1);
    }
  }
}
