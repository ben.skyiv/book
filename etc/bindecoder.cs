using System;
using System.IO;
using System.Text;

static class Bindecoder
{
  static void Main()
  {
    try { // isBin: true - Binary, false - Hexadecimal
      var isBin = Path.GetFileName(Environment.GetCommandLineArgs()[0]).StartsWith("bin");
      var sb = new StringBuilder();
      for (string s; (s = Console.In.ReadLine()) != null; ) sb.Append(s.Replace(" ", ""));
      using (var sw = new BinaryWriter(Console.OpenStandardOutput())) {
        var s = sb.ToString();
        var digits = (isBin ? 8 : 2);
        if (s.Length % digits != 0) throw new Exception("Total length error");
        var bs = new byte[s.Length / digits];
        for (var i = 0; i < bs.Length; i++)
          bs[i] = Convert.ToByte(s.Substring(digits * i, digits), isBin ? 2 : 16);
        sw.Write(bs);
      }
    } catch (Exception ex) {
      Console.Error.WriteLine(ex.Message);
    }
  }
}

