using System;
using System.Collections;
using System.Collections.Generic;

sealed class Factors
{
  BitArray Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new BitArray(max + 1);
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  int[] GetPrimes(int max)
  {
    var primes = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) primes.Add(i);
    primes.Add(int.MaxValue);
    return primes.ToArray();
  }

  SortedList<long, int> Merge(IEnumerable<long> list)
  {
    var v = new SortedList<long, int>();
    foreach (var p in list)
    {
      int n;
      if (!v.TryGetValue(p, out n)) v.Add(p, 1);
      else v[p] = n + 1;
    }
    return v;
  }
  
  SortedList<long, int> GetFactors(long x)
  {
    var list = new List<long>();
    var limit = (int)Math.Sqrt(x);
    var primes = GetPrimes(limit);
    for (int i = 0, p = primes[i]; p <= limit; p = primes[i])
      if (x % p != 0) i++;
      else { list.Add(p); limit = (int)Math.Sqrt(x /= p); }
    if (x != 1) list.Add(x);
    return Merge(list);
  }

  static void Main(string[] args)
  {
    foreach (var n in new Factors().GetFactors(long.Parse(args[0])))
      Console.Write(" " + n.Key + ((n.Value != 1) ? ("^" + n.Value) : ""));
    Console.WriteLine();
  }
}

