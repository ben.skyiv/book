#!/usr/bin/python
from sys import stdin
from collections import Counter

stdin.reconfigure(errors='replace')
counter, cnts = Counter(), {}
counter.update(stdin)
print(f'Total : {counter.total():>10,}')
print(f'Unique: {len(counter):>10,}')
for key, value in counter.items():
    cnts[value] = cnts.get(value, 0) + 1
for key, value in cnts.items():
    print(f'> {key:>4d}: {value:>10,}')
print(counter.most_common(20))
