file1, file2 = '/home/ben/repo/passwd1', '/home/ben/repo/passwd2'
print(f'{file1}\n{file2}')
with open(file1) as f: s1 = set(f)
with open(file2, errors='replace') as f: s2 = set(f)
print(f'file1 : {len(s1):>10,}')
print(f'file2 : {len(s2):>10,}')
print(f'total : {len(s1 | s2):>10,}')
print(f'common: {len(s1 & s2):>10,}')
print(f'diff1 : {len(s1 - s2):>10,}')
print(f'diff2 : {len(s2 - s1):>10,}')
