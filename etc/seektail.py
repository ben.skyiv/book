#!/usr/bin/python
from sys import argv
from pathlib import Path

tails = (
    ('1D 77 56 51 03 05 04 00', 'rar5',   '52 61 72 21 1A 07 01 00', 'Tail'),
    ('C4 3D 7B 00 40 07 00',    'rar4',   '52 61 72 21 1A 07 00',    'Tail'),
    ('50 4B 01 02',             'zip',    '50 4B 03 04',             'Mid'),
    ('50 4B 05 06',             'zip',    '50 4B 03 04',             'Tail'),
    ('AE 42 60 82',             'png',    '89 50 4E 47',             'Tail'),
    ('4D 4D 2A 00',             'tiff',   '49 49 2A 00',             'Tail'),
    ('FF D9',                   'jpeg',   'FF D8 FF',                'Tail'),
    ('00 3B',                   'gif89a', '47 49 46 38 39 61',       'Tail'),
    ('00 3B',                   'gif87a', '47 49 46 38 37 61',       'Tail'),
    ('', 'pdf',     '25 50 44 46 2D 31 2E', ''),
    ('', 'wav',     '57 41 56 45 66 6D 74', ''),
    ('', 'xml',     '3C 3F 78 6D 6C',       ''),
    ('', 'rtf',     '7B 5C 72 74 66',       ''),
    ('', 'mpeg',    '00 00 01 B3',          ''),
    ('', 'mpeg',    '00 00 01 BA',          ''),
    ('', '7z',      '37 7A BC AF',          ''),
    ('', 'wav/avi', '52 49 46 46',          ''),
    ('', 'doc/xls', 'D0 CF 11 E0',          ''),
    ('', 'bz/bz2',  '42 5A 68',             ''),
    ('', 'mp3',     '49 44 33',             ''),
    ('', 'exe',     '4D 5A 90',             ''),
)


def seek_tail():
    success = False
    for tail, file_type, head, flag in tails:
        if len(tail) == 0: break
        i1 = bs.find(bytearray.fromhex(tail))
        if i1 < 0: continue
        success = True
        i2 = bs.find(bytearray.fromhex(head))
        i2 = 'None' if i2 < 0 else f'{i2:04X}'
        print(f'{i1:08X}: {file_type:<7} ({tail} {flag:>4}) ({head}: {i2})')
    return success


def seek_head(limit):
    success = False
    for head, file_type in heads.items():
        key = bytearray.fromhex(head)[-limit:]
        i0, cnt = 0, 0
        while i0 < len(bs):
            i1 = bs.find(key, i0)
            if i1 < 0: break
            if cnt > 0 and not flagH: break
            i0 = i1 + len(key)
            i2 = bs.find(bytearray.fromhex(head))
            i2 = 'None' if i2 < 0 else f'{i2:04X}'
            s1 = ' '.join(f'{c:02X}' for c in key)
            s2 = '' if flagH else f' ({head}: {i2})'
            print(f'{i1:08X}: {file_type:<7} ({s1} Head){s2}')
            success, cnt = True, cnt + 1
    return success


if len(argv) < 2:
    print(f'Usage: {Path(argv[0]).name} file_name [h | H]')
    print('    h: seek (uncompleted) head')
    print('    H: seek all (completed) heads')
    exit()
try:
    with open(argv[1], 'rb') as f: bs = f.read()
except FileNotFoundError as e: print(e); exit()
flagH = len(argv) >= 3 and argv[2] == 'H'
success = False if flagH else seek_tail()
if not success or (len(argv) >= 3 and argv[2] == 'h'):
    heads = {head: file_type for _, file_type, head, _ in tails}
    if not seek_head(0 if flagH else 3) and not flagH: seek_head(2)
