using System;
using System.Text;

static class UnihanTester
{
  static readonly Encoding GB18030 = Encoding.GetEncoding("GB18030");

  static void Main()
  {
    Console.WriteLine("-Code Chars---- Unicode---- GB18030---- UTF8------- [--]");
    foreach (var n in new int[]{ 0x3F, 0x25CB, 0x3400, 0x4DB5,
      0x4E00, 0x9FA5, 0x9FA6, 0x9FCB, 0x9FCC, 0xF900, 0xFA2D, 0xFA30, 0xFA70,
      0x20000, 0x2A6D6, 0x2A700, 0x2B734, 0x2B740, 0x2B81D, 0x2F800, 0x2FA1D })
    {
      var s = new string(GetUtf16Chars(n));
      Console.WriteLine("{1,5:X} {2,-9} {3,-11} {4,-11} {5,-11} [{0}]",
        s, n, GetCharsHexString(s),
        BitConverter.ToString(Encoding.Unicode.GetBytes(s)),
        BitConverter.ToString(GB18030.GetBytes(s)),
        BitConverter.ToString(Encoding.UTF8.GetBytes(s)));
    }
  }

  static string GetCharsHexString(string s)
  {
    var sb = new StringBuilder();
    foreach (var c in s) sb.AppendFormat("{0:X4}-", (int)c);
    if (sb.Length > 0) sb.Length--;
    return sb.ToString();
  }

  static char[] GetUtf16Chars(int n)
  {
    if (n <= char.MaxValue) return new char[]{ (char)n };
    return new char[]{ (char)(0xD800 | ((n - 0x10000) >> 10)), (char)(0xDC00 | (n & 0x3FF)) };
  }  
}
