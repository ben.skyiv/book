﻿namespace HelloMobile.ViewModels;
using System;
using System.Text;

public class MainViewModel : ViewModelBase
{
    static string prefix = "System.Text.";
    public string Greeting => "欢迎来到银河系！";
    public string SysInfo { get; private set; }

    public MainViewModel()
    {
        var encode = Encoding.Default.ToString();
        if (encode != null && encode.StartsWith(prefix)) encode = encode.Substring(prefix.Length);
        SysInfo = string.Format("OS : {0}\nCLR: {1}\n{2}",
            Environment.OSVersion, Environment.Version, encode);
    }
}
