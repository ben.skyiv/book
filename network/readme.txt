https://wiki.archlinuxcn.org/wiki/NetworkManager
$ sudo pacman -S networkmanager
$ sudo pacman -S nm-connection-editor   # GUI
$ sudo pacman -S network-manager-applet # 系统托盘图标
$ systemctl --type=service | grep -i Network
$ systemctl status NetworkManager.service
$ systemctl status systemd-resolved
$ nmcli device wifi list
$ sudo nmcli device wifi connect SSID_或_BSSID password 密码
$ sudo nmcli device wifi connect 666666 password Icbc123456 hidden yes
