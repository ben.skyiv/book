using System;
using System.IO;
using System.Text;
using System.Threading;
using Skyiv.Common;

namespace Skyiv.Util
{
  static class Download
  {
    static readonly Encoding Encode = Encoding.GetEncoding("GBK");
    static readonly string BaseUri = "https://www.52zwxs.com/1_1634/";
    static readonly string bookName = "他在悄悄说爱我";
    
    static string GetChapterName(string s)
    {
      string key1 = "<title>" + bookName +" ", key2 = "_52小说网</title>";
      if (!s.StartsWith(key1) || !s.EndsWith(key2)) return null;
      s = s.Substring(key1.Length);
      s = s.Substring(0, s.Length - key2.Length);
      return s;
    }

    static string GetNextUri(string s)
    {
      var i = s.IndexOf("章节列表");
      if (i < 0) return null;
      s = s.Substring(i);
      i = s.IndexOf(BaseUri);
      if (i < 0) return null;
      s = s.Substring(i);
      i = s.IndexOf("\">下一章<");
      if (i < 0) throw new Exception("GetNextUri()");
      s = s.Substring(0, i);
      return s;
    }

    static void Run()
    {
      using (var writer = new StreamWriter(bookName + ".txt")) {
        writer.WriteLine("《{0}》 {1}", bookName, BaseUri);
        var key1 = "<div id=\"content\">";
        for (var uri = BaseUri + "364892.html"; uri != BaseUri; ) {
          string nextUri = null, chapterName = null;
          using (var reader = new StreamReader(uri.GetInputStream(), Encode)) {
            var line = 0;
            for (; ; ) {
              var s = reader.ReadLine();
              if (s == null) break;
              s = s.Trim();
              if (chapterName == null) chapterName = GetChapterName(s);
              if (nextUri == null) nextUri = GetNextUri(s);
              if (line == 0 && !s.StartsWith(key1)) continue;
              if (line > 0 && s.StartsWith("<script>")) break;
              if (s == "<br />") continue;
              if (line == 0) {
                if (nextUri == null || chapterName == null)
                  throw new Exception("nextUri or chapterName");
                Console.Write("{0}: {1}: ", nextUri, chapterName);
                writer.WriteLine("{0}{1}{0}", Environment.NewLine, chapterName);
              }
              line++;
              s = s.Replace("&nbsp;", " ")
                   .Replace(key1, "")
                   .Replace("<br />", "")
                   .Replace("</div>", "")
                   .Replace("<script>readx();</script>", "");
              writer.WriteLine(s);
            }
            Console.WriteLine(line);
          }
          uri = nextUri;
          writer.Flush();
          Thread.Sleep(10 * 1000);
        }
      }
    }

    static void Main()
    {
      try { Run(); }
      catch (Exception ex) { Console.WriteLine("Error: " + ex.Message); }
    }
  }
}

