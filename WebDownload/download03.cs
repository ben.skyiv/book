using System;
using System.IO;
using System.Text;
using Skyiv.Common;

namespace Skyiv.Util
{
  static class Download
  { // bookName = 不负卿宠
    static readonly Encoding Encode = Encoding.GetEncoding("GB18030");//GBK
    static readonly string MainUri = "https://www.txtnovels.com";
    static readonly string BaseUri = MainUri + "/54_54877/";

    static void Run()
    {
      using (var writer = new StreamWriter("/home/ben/tmp/a01.txt")) {
        for (var uri = BaseUri + "26653164.html"; uri != BaseUri; ) {
          using (var reader = new StreamReader(uri.GetInputStream(), Encode)) {
            var line = 0;
            for (; ; ) {
              var s = reader.ReadLine();
              if (s == null) break;
              s = s.Trim();
              line++;
              if (line == 129) writer.WriteLine(s);
            }
          }
          writer.Flush();
          break;
        }
      }
    }

    static void Main()
    {
      try { Run(); }
      catch (Exception ex) { Console.WriteLine("Error: " + ex.Message); }
    }
  }
}

