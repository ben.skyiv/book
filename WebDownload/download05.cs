using System;
using System.IO;
using System.Text;
using System.Threading;
using Skyiv.Common;

namespace Skyiv.Util
{
  static class Download
  { // bookName = 萌宝来袭（闪婚夫妻的宠娃日常）
    static readonly Encoding Encode = Encoding.GetEncoding("GB18030");//GBK
    static readonly string UriBase = "https://www.tsxs.org";
    static readonly string UriSuffix = "/72/72009/40393513.html";//26170690
    static readonly string NextPageSuffix = "下一页</a>";
    static readonly string Prefix1 = "<h1>闪婚夫妻宠娃日常txt全文下载"; //章节目录
    static readonly string Prefix2 = "<br/>&nbsp;&nbsp;&nbsp;&nbsp;";
    static readonly string Suffix1 = "</h1>";
    static readonly string Suffix2 = "";
    
    static string GetNextUri(string s)
    {
      if (!s.EndsWith(NextPageSuffix)) return null;
      var i = s.IndexOf('"'); if (i < 0) return null;
      s = s.Substring(i + 1);
      i = s.IndexOf('"'); if (i < 0) return null;
      return s.Substring(0, i);
    }

    static void Write(StreamWriter writer, string s, string prefix,  string suffix)
    {
      if (s.EndsWith(suffix)) s = s.Substring(0, s.Length - suffix.Length);
      if (s.StartsWith(prefix)) writer.WriteLine(s.Substring(prefix.Length));
    }
    
    static void Run()
    {
      var i = 237;
      using (var writer = new StreamWriter("/home/ben/tmp/a01.txt")) {
        for (var uri = UriBase + UriSuffix; ; ) {
          Console.WriteLine("{0,4}: {1}", i, uri);
          i++;
          string nextUri = null;
          using (var reader = new StreamReader(uri.GetInputStream(), Encode)) {
            for (; ; ) {
              var s = reader.ReadLine();
              if (s == null) break;
              s = s.Trim();
              if (nextUri == null) nextUri = GetNextUri(s);
              Write(writer, s, Prefix1, Suffix1);
              Write(writer, s, Prefix2, Suffix2);
            }
          }
          writer.WriteLine();
          writer.Flush();
          Thread.Sleep(10 * 1000);
          if (nextUri == null) break;
          uri = UriBase + nextUri;
        }
      }
    }

    static void Main()
    {
      try { Run(); }
      catch (Exception ex) { Console.WriteLine("Error: " + ex.Message); }
    }
  }
}
