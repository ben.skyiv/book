using System;
using System.IO;
using System.Text;
using System.Threading;
using Skyiv.Common;

namespace Skyiv.Util
{
  static class Download
  {
    static readonly Encoding Encode = Encoding.GetEncoding("GBK");
    static readonly string MainUri = "https://www.txtnovels.com";
    static readonly string BaseUri = MainUri + "/54_54877/"; // 26653164.html
    static readonly string startUrl = "27796005.html"; // 第1026章
    static readonly string bookName = "不负卿宠";

    static string GetChapterName(string s)
    {
      string key1 = "<title>" + bookName +"全文阅读", key2 = "_txt小说网</title>";
      if (!s.StartsWith(key1) || !s.EndsWith(key2)) return null;
      s = s.Substring(key1.Length);
      s = s.Substring(0, s.Length - key2.Length);
      return s;
    }
    
    static string GetNextUri(string s)
    {
      string key1 = "href=\"", key2 = "\">下一章";
      if (s.IndexOf(key2) < 0) {
        key2 = "\">下一页";
        if (s.IndexOf(key2) < 0) return null;
      }
      var i = s.IndexOf(key1);
      if (i < 0) throw new Exception("GetNextUri()");
      s = s.Substring(i + key1.Length);
      i = s.IndexOf(key2);
      s = s.Substring(0, i);
      return s;
    }
    
    static void Run()
    {
      using (var writer = new StreamWriter("/home/ben/tmp/" + bookName + ".txt")) {
        writer.WriteLine("《{0}》 {1}", bookName, BaseUri);
        for (var uri = BaseUri + startUrl; uri != BaseUri; ) {
          string nextUri = null, chapterName = null;
          var end = false;
          using (var reader = new StreamReader(uri.GetInputStream(), Encode)) {
            var line = 0;
            for (; ; ) {
              var s = reader.ReadLine();
              if (s == null) break;
              s = s.Trim();
              if (chapterName == null) chapterName = GetChapterName(s);
              if (line == 0 &&
                s != "<div class=\"panel-body\" id=\"htmlContent\">") continue;
              if (line > 0 && s == "</div>") { end = true; continue; }
              if (end) {
                if (nextUri == null) nextUri = GetNextUri(s);
                continue; 
              }
              if (line == 0) {
                if (chapterName == null) throw new Exception("chapterName");
                writer.WriteLine("{0}{1}{0}", Environment.NewLine, chapterName);
              }
              line++;
              if (line == 1) continue;
              s = s.Replace("&nbsp;", " ")
                   .Replace("</p>", "")
                   .Replace("<br/><br/>", Environment.NewLine)
                   .Replace("<br/>", "");
              writer.WriteLine(s);
            }
            Console.WriteLine("{0}: {1}: {2}", nextUri, chapterName, line - 1);
          }
          if (nextUri == null) throw new Exception("nextUri");
          uri = MainUri + nextUri;
          writer.Flush();
          //Thread.Sleep(1 * 1000);
        }
      }
    }

    static void Main()
    {
      try { Run(); }
      catch (Exception ex) { Console.WriteLine("Error: " + ex.Message); }
    }
  }
}

