using System;
using System.IO;
using System.Text;
using System.Threading;
using Skyiv.Common;

namespace Skyiv.Util
{
  static class Download
  { // bookName = 三宝来袭总裁夫人美爆全球
    static readonly Encoding Encode = Encoding.GetEncoding("GB18030");//GBK
    static readonly string MainUri = "https://www.ddtxt.cc/";
    static readonly string BaseUri = MainUri + "xhJDSSz7i/48013_";
    static readonly string Prefix1 = "<h1 class=\"readTitle\">";
    static readonly string Prefix2 = "&nbsp;&nbsp;&nbsp;&nbsp;";
    static readonly string Suffix1 = "<small>(1/1)</small> </h1>";
    static readonly string Suffix2 = "<br/>";
    static readonly int n1 = 24786477; //24785952,24784918,24784124;
    static readonly int n2 = 24786655;

    static void Write(StreamWriter writer, string s, string prefix,  string suffix)
    {
      if (s.EndsWith(suffix)) s = s.Substring(0, s.Length - suffix.Length);
      if (s.StartsWith(prefix)) writer.WriteLine(s.Substring(prefix.Length));
    }

    static void Run()
    {
      var i = 1;
      using (var writer = new StreamWriter("/home/ben/tmp/a01.txt")) {
        for (var n = n1; n <= n2; n++) {
          var uri = BaseUri + n.ToString() + ".html";
          Console.WriteLine("{0,4}: {1}", i, uri);
          i++;
          using (var reader = new StreamReader(uri.GetInputStream(), Encode)) {
            var line = 0;
            for (; ; ) {
              var s = reader.ReadLine();
              if (s == null) break;
              s = s.Trim();
              line++;
              Write(writer, s, Prefix1, Suffix1);
              Write(writer, s, Prefix2, Suffix2);
            }
          }
          writer.WriteLine();
          writer.Flush();
          Thread.Sleep(10 * 1000);
        }
      }
    }

    static void Main()
    {
      try { Run(); }
      catch (Exception ex) { Console.WriteLine("Error: " + ex.Message); }
    }
  }
}
