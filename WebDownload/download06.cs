using System;
using System.IO;
using System.Text;
using System.Threading;
using Skyiv.Common;

namespace Skyiv.Util
{
  static class Download
  { // bookName = 第一神医 陈天选方糖
    static readonly Encoding Encode = Encoding.GetEncoding("GB18030");//GBK
    static readonly string UriBase = "https://www.zongcaidarentilihaoya.com";
    static readonly string UriSuffix = "/read/5447_15487332.html";//2818407
    static readonly string NextPageSuffix = "下一篇</a></li>";
    static readonly string Prefix1 = "<title>"; // 章节目录
    static readonly string Suffix1 = "</title>";
    static readonly string Prefix2 = "&nbsp;&nbsp;&nbsp;&nbsp;";
    static readonly string Suffix2 = "<br />";
    
    static string GetNextUri(string s)
    {
      if (!s.EndsWith(NextPageSuffix)) return null;
      var prefix = "href=\"";
      var i = s.IndexOf(prefix); if (i < 0) return null;
      s = s.Substring(i + prefix.Length);
      i = s.IndexOf('"'); if (i < 0) return null;
      return s.Substring(0, i);
    }

    static void Write(StreamWriter writer, string s, string prefix,  string suffix)
    {
      if (s.EndsWith(suffix)) s = s.Substring(0, s.Length - suffix.Length);
      if (s.StartsWith(prefix)) writer.WriteLine(s.Substring(prefix.Length));
    }

    static void Run()
    {
      var i = 1452;
      using (var writer = new StreamWriter("/home/ben/tmp/a01.txt")) {
        for (var uri = UriBase + UriSuffix; ; ) {
          Console.WriteLine("{0,4}: {1}", i, uri);
          i++;
          string nextUri = null;
          using (var reader = new StreamReader(uri.GetInputStream(), Encode)) {
            for (; ; ) {
              var s = reader.ReadLine();
              if (s == null) break;
              s = s.Trim();
              if (nextUri == null) nextUri = GetNextUri(s);
              Write(writer, s, Prefix1, Suffix1);
              Write(writer, s, Prefix2, Suffix2);
            }
          }
          writer.WriteLine();
          writer.Flush();
          Thread.Sleep(10 * 1000);
          if (nextUri == null) break;
          uri = UriBase + nextUri;
        }
      }
    }

    static void Main()
    {
      try { Run(); }
      catch (Exception ex) { Console.WriteLine("Error: " + ex.Message); }
    }
  }
}
