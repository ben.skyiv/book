using System;
using System.IO;
using System.Text;
using System.Threading;
using Skyiv.Common;

namespace Skyiv.Util
{
  static class Download
  { // bookName = 战神江辰 Chap.847--1818
    static readonly Encoding Encode = Encoding.GetEncoding("GB18030");//GBK
    static readonly string BaseUri = "https://www.yuedyue.com/303_303952/";
    static readonly string Prefix1 = "<h1>";
    static readonly string Prefix2 = "&nbsp;&nbsp;&nbsp;&nbsp;";
    static readonly string Suffix1 = "</h1>";
    static readonly string Suffix2 = "<br />";
    static readonly string Prefix0 = "var url_next = \"";
    static readonly string Suffix0 = "\";";
    static readonly int n1 = 139603964;
    static readonly int chap = 1650;

    static void Write(StreamWriter writer, string s, string prefix,  string suffix)
    {
      if (s.EndsWith(suffix)) s = s.Substring(0, s.Length - suffix.Length);
      if (s.StartsWith(prefix)) writer.WriteLine(s.Substring(prefix.Length));
    }
    
    static void Run()
    {
      var uri = BaseUri + n1.ToString() + ".html";
      using (var writer = new StreamWriter("/home/ben/tmp/a01.txt")) {
        for (var i = chap; uri != null; i++) {
          Console.WriteLine("{0,4}: {1}", i, uri);
          using (var reader = new StreamReader(uri.GetInputStream(), Encode)) {
            uri = null;
            for (; ; ) {
              var s = reader.ReadLine();
              if (s == null) break;
              s = s.Trim();
              if (s.StartsWith(Prefix0)) {
                if (s.EndsWith(Suffix0)) s = s.Substring(0, s.Length - Suffix0.Length);
                uri = s.Substring(Prefix0.Length);
                continue;
              }
              Write(writer, s, Prefix1, Suffix1);
              Write(writer, s, Prefix2, Suffix2);
            }
          }
          writer.WriteLine();
          writer.Flush();
          Thread.Sleep(10 * 1000);
        }
      }
    }

    static void Main()
    {
      try { Run(); }
      catch (Exception ex) { Console.WriteLine("Error: " + ex.Message); }
    }
  }
}
