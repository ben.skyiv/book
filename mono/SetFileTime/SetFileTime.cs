// $ mcs SetFileTime.cs && ./SetFileTime.exe

using System;
using System.IO;
using System.Diagnostics;

static class SetFileTime
{
    static void Main()
    {
        char[] sep = { '_' };
        var dir = Directory.GetCurrentDirectory();
        Console.WriteLine("根据文件名修改文件时间程序 1.7");
        Console.WriteLine("当前目录：{0}", dir);
        Console.WriteLine("每处理n个文件显示当前进度");
        Console.WriteLine("按回车默认n=100，输入0退出本程序");
        Console.Write("请输入：");
        int num = 100;
        var key = Console.ReadLine().Trim();
        if (key != "") int.TryParse(key, out num);
        if (num <= 0) return;
        var timer = Stopwatch.StartNew();
        var n = 0;
        Console.Write("{0,11:N0}", n);
        foreach (var s in Directory.EnumerateFiles(dir, "*.mp3")) {
            n++;
            var tt = Path.GetFileNameWithoutExtension(s).Split(sep)[1];
            long t;
            if (!long.TryParse(tt, out t))
                throw new Exception(string.Format("[{0}][{1}]", tt, s));
            var dt = new DateTime(
                (int)(t / 10000000000L),      // yyyy
                (int)(t / 100000000L % 100L), // MM
                (int)(t /   1000000L % 100L), // dd
                (int)(t /     10000L % 100L), // hh
                (int)(t /       100L % 100L), // mm
                (int)(t              % 100L)  // ss
            );
            if (n % num == 0)
              Console.Write("\b\b\b\b\b\b\b\b\b\b\b{0,11:N0}", n);
            File.SetLastWriteTime(s, dt);
            // File.SetCreationTime(s, dt);
            // File.SetLastAccessTime(s, dt);
        }
        timer.Stop();
        Console.Write("\b\b\b\b\b\b\b\b\b\b\b{0,11:N0}", n);
        Console.WriteLine();
        Console.WriteLine("共处理 {0:N0} 个文件，耗时：{1}",
            n, timer.Elapsed);
        Console.Write("按回车键退出 ");
        Console.ReadLine();
    }
}
