using System.Collections.Generic;
using MiniExcelLibs;
using MiniExcelLibs.OpenXml;

static class MiniExcelTest
{
  static void Main()
  {
    var config = new OpenXmlConfiguration() {
      // TableStyles = TableStyles.None,
      AutoFilter = false
    };
    var values = new List<Dictionary<string, object>>()
    {
      new Dictionary<string,object>{{ "姓名", "任盈盈" }, { "数量", 1 } },
      new Dictionary<string,object>{{ "姓名", "韦小宝" }, { "数量", 2 } }
    };
    var path = "/home/ben/tmp/a.xlsx";
    MiniExcel.SaveAs(path, values, configuration:config, overwriteFile:true);
  }
}
