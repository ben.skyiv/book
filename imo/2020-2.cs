using System;

static class Imo
{
  static void Main(string[] args)
  {
    var m = (args.Length == 0) ? 5 : int.Parse(args[0]);
    var n = (long)(Math.Pow(10, m));
    Console.WriteLine("n = 10^{0} = {1:N0}", m, n);
    var rnd = new Random();
    var s  =new double[4];
    var x = new double[4];
    var y = new double[4];
    double min = 10, max = -10;
    for (long i = 0; i < n; i++) {
      double q = 1.0, p = q / 4, r;
      s[0] = rnd.NextDouble()*(q-p)+p; q = Math.Min(s[0],r=1-s[0]); p = r/3;
      s[1] = rnd.NextDouble()*(q-p)+p; q=Math.Min(s[1],r=1-s[0]-s[1]);p=r/2;
      s[2] = rnd.NextDouble()*(q-p)+p; s[3] = 1 - s[0] - s[1] - s[2];
      var z =  Math.Log(s[0]+ 2*s[1]+ 3*s[2]+ 4*s[3]) +
        s[0] * Math.Log(s[0]) + s[1] * Math.Log(s[1]) +
        s[2] * Math.Log(s[2]) + s[3] * Math.Log(s[3]);
      if (z < min) { min = z; for (var j = 0; j < 4; j++) x[j] = s[j]; }
      if (z > max) { max = z; for (var j = 0; j < 4; j++) y[j] = s[j]; }
    }
    Console.WriteLine("{0} {1} {2} {3} {4}",Math.Exp(min),x[0],x[1],x[2],x[3]);
    Console.WriteLine("{0} {1} {2} {3} {4}",Math.Exp(max),y[0],y[1],y[2],y[3]);
  }
}

/*
n = 10^10 (runtime:23m22s)
0.551312036488319 : inf
0.46775643956743  : a
0.275211987369319 : b
0.161857284460316 : c
0.0951742886029353: d
*/
