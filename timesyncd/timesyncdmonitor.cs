using System;
using System.IO;
using System.Threading;

sealed class TimesyncdMonitor
{
  static void Main()
  {
    var t0 = DateTime.MinValue;
    var watcher = new FileSystemWatcher("/run/systemd/timesync", "synchronized");
    watcher.Changed += (_, e) => {
      var t1 = DateTime.Now; if (t0 == DateTime.MinValue) t0 = t1;
      Console.WriteLine("{0:dd HH:mm:ss}: {1,8:F3}",
        t1, (t1 - t0).TotalSeconds); t0 = t1; };
    watcher.EnableRaisingEvents = true;
    Thread.Sleep(Timeout.Infinite);
  }
}
