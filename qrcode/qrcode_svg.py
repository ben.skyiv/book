import qrcode
import qrcode.image.svg

d = 'http://www.fjadd.com/addr?id=010BAE7C-0FAF-546F-E064-90E2BA510A0C'
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_L,
    box_size=15,
    border=4,
)
qr.add_data(d)
qr.make(fit=True)
img = qr.make_image(
    fill_color="black",
    back_color="white",
    image_factory=qrcode.image.svg.SvgPathImage
)
with open("qrcode.svg", "wb") as f:
    img.save(f)
