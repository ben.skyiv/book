using System;
using System.IO;
using System.Text;
using Skyiv.Common;
using Skyiv.Utils;

namespace Skyiv.App
{
  sealed class GetData
  {
    static readonly Encoding WebEncode = Encoding.UTF8;
    static readonly Encoding FileEncode = Encoding.Default;
    static readonly DateTime now = DateTime.Now;
    static readonly string BaseUri = "http://119.1.195.192:8000/Client/Nanjiang/";

    string GetFileName(string dateType)
    {
      return string.Format("{0}/{1}-{2:D4}{3}.csv",
        dateType, dateType[0], now.Year,
        (dateType == "Year") ? "" :
        (dateType == "Month") ? now.ToString("-MM") :
        (dateType == "Day") ? now.ToString("-MM-dd") :
        ("-" + now.GetWeekOfYear().ToString("D2")));
    }

    string Trim(string s, string s1, string s2, bool isLast = false)
    {
      var left = s.IndexOf(s1);
      if (left < 0) throw new Exception("Left (" + s1 + ") invalid: " + s);
      s = s.Substring(left + s1.Length);
      var right = isLast ? s.LastIndexOf(s2) : s.IndexOf(s2);
      if (right < 0) throw new Exception("Right (" + s2 + ") invalid: " + s);
      return s.Substring(0, right);
    }
    
    (string, int) WeekData(string dateType)
    {
      var url = "Module/Index_ProjectTradeRank.aspx";
      Directory.CreateDirectory(dateType);
      var fileName = GetFileName(dateType);
      var cnt = 0;
      using (var writer = new StreamWriter(fileName, false, FileEncode))
      using (var reader = new StreamReader((BaseUri + url).GetInputStream(), WebEncode)) {
        writer.WriteLine("项目名称,成交套数");
        for (; ; ) {
          var s1 = reader.ReadLine();
          if (s1 == null) break;
          if (!s1.Contains("</nobr>")) continue;
          var s2 = reader.ReadLine();
          if (s2 == null) throw new Exception("Unexpected EOF");
          writer.WriteLine(Trim(s1, "\">", "</a>") + "," + Trim(s2, "\">", "</div>"));
          cnt++;
        }
      }
      return (fileName, cnt);
    }

    string GetDayMonthYearData(string dateType)
    {
      var sb = new StringBuilder();
      var url = string.Format("Scripts/Paging/PagingHandler.ashx" +
        "?act={0}EntSaleRank&columnID=1&curPage=1&pageSize={1}&rnd={2}",
        dateType, 1024, new Random().Next(12, 3456));
      using (var reader = new StreamReader((BaseUri + url).GetInputStream(), WebEncode)) {
        for (; ; ) {
          var s = reader.ReadLine();
          if (s == null) break;
          sb.AppendLine(s);
        }
      }
      return sb.ToString();
    }

    string[] Split(string s)
    {
      var sep = "\x7F";
      if (sep.Length != 1) throw new Exception("Invalid sep.Length");
      if (s.Contains("[")) s = Trim(s, "[", "]", true);
      else return new string[0];
      if (s.Contains(sep)) throw new Exception("Invalid sep");
      return s.Replace("},", "}" + sep).Split(sep.ToCharArray());
    }

    string GetFieldValue(string s, string name)
    {
      var ss = s.Split(":".ToCharArray());
      name = "\"" + name + "\"";
      if (ss[0] != name) throw new Exception("(" + ss[0] + ") must be (" + name + ")");
      return ss[1];
    }

    string[] Parse(string[] ss)
    {
      var sep = ",".ToCharArray();
      var t  = "\"".ToCharArray();
      var ts = new string[ss.Length];
      for (var i = 0; i < ss.Length; i++) {
        var fs = Trim(ss[i], "{", "}", true).Split(sep);
        if (fs.Length != 8) throw new Exception("Invalid fs.Length: " + fs.Length);
        var j = int.Parse(GetFieldValue(fs[7], "OrderNum"));
        if (j < 1 || j > ss.Length) throw new Exception("Invalid j: " + j);
        if (ts[j - 1] != null) throw new Exception("Dup j: " + j);
        ts[j - 1] = j + "," +
          GetFieldValue(fs[3], "EnterpriseName").Trim(t) + "," +
          GetFieldValue(fs[4], "HouseNumber") + "," +
          GetFieldValue(fs[5], "BuildArea") + "," +
          GetFieldValue(fs[6], "BuildAvgPrice");
      }
      return ts;
    }

    (string, int) DayMonthYearData(string dateType)
    {
      var ss = Parse(Split(GetDayMonthYearData(dateType)));
      Directory.CreateDirectory(dateType);
      var fileName = GetFileName(dateType);
      using (var writer = new StreamWriter(fileName, false, FileEncode)) {
        writer.WriteLine("序号,企业名称,成交套数,成交面积,成交均价");
        foreach (var s in ss) writer.WriteLine(s);
      }
      return (fileName, ss.Length);
    }

    static void Main(string[] args)
    {
      if (args.Length != 1 || (
          args[0] != "Week" &&
          args[0] != "Day" &&
          args[0] != "Month" &&
          args[0] != "Year"))
      {
        Console.WriteLine("{0} ({1}, CLR {2}, {3})",
          MyUtils.VersionStr, Environment.OSVersion, Environment.Version, FileEncode);
        return;
      }
      using (var log = new StreamWriter("log-" + args[0] + ".txt", true, FileEncode))
      try {
        log.Write("{0:yyyy-MM-dd HH:mm:ss}|{1}|", now, MyUtils.VersionStr);
        var gd = new GetData();
        var (s, n) = (args[0] == "Week")
          ? gd.WeekData(args[0])
          : gd.DayMonthYearData(args[0]);
        log.WriteLine("{0:D2}|{1}", n, s);
      }
      catch (Exception ex) { log.WriteLine("Error: " + ex.GetMessage()); }
    }
  }
}
