运行 getdata.exe 显示版本号，不写任何文件。

运行
getdata.exe Week
getdata.exe Day
getdata.exe Month
getdata.exe Year
分别生成
Week, Day, Month, Year
目录下的 csv 文件，
并写以下日志文件
log-Week.txt
log-Day.txt
log-Month.txt
log-Year.txt

建议计划任务运行本程序时错开运行，例如，
分别于 22:01, 22:02, 22:03, 22:04
生成 Week, Day, Month, Year 数据。

数量来源：
http://119.1.195.192:8000/Client/Nanjiang/Second/SubPage_SellRankList.aspx
