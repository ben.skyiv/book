using System;
using System.Reflection;

namespace Skyiv.App
{
  public static class MyUtils
  {
    public static readonly Assembly ThisAssembly = Assembly.GetExecutingAssembly();
    public static Version Version = ThisAssembly.GetName().Version;
    public static readonly string VersionStr = "v" + Version.ToString(2);

    public static string GetMessage(this Exception ex)
    {
      return ex.ToString().Replace(Environment.NewLine, " ");
    }
  }
}
