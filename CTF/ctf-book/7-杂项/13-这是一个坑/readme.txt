pp.335--336，题目3-13：这是一个坑，明文爆破
题目描述：密码是十位大小写字母、数字、特殊符号组成的，你爆破的开么？！

$ unzip no.zip
Archive:  no.zip
  inflating: flag.zip
  inflating: tips.txt

flag.zip 中包含加密的 flag.txt 和 tips.txt
而且两个 tips.txt 的 CRC32 相同，可见其内容相同，
在 Windows 10 上，使用 WinRAR 压缩 tips.txt 为 tips.zip
然后用 ARCHPR 进行明文爆破，得到 flag_decrypted.zip
解压后得到的 flag.txt 的内容是：
这里并没有flag，flag格式为flag{解压密码}

TODO
