pp.330--333，题目7-11：你知道他是谁蚂，NTFS数据流
题目描述：别忘了WinRAR，答案格式为flag{XXXXX}。

$ file NF.rar
NF.rar: RAR archive data, v4, os: Win32

在 Windows 10 上，用 WinRAR 解压 NF.rar

C:\bin\NTFS_ADS>lads.exe \unsafe

LADS - Freeware version 4.10
(C) Copyright 1998-2007 Frank Heyne Software (http://www.heysoft.de)
This program lists files with alternate data streams (ADS)
Use LADS on your own risk!

Scanning directory C:\unsafe\

      size  ADS in file
----------  ---------------------------------
       531  C:\unsafe\20140328144416-985375150.jpg:flag

       531 bytes in 1 ADS listed

C:\unsafe>notepad.exe 20140328144416-985375150.jpg:flag
后续按书上的步骤进行。
