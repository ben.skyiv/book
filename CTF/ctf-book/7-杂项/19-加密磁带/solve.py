from sys import stdin
s = stdin.read().split()
s = [c.replace('o', '1').replace('_', "0") for c in s]
s = [chr(int(c, 2)) for c in s]
print(''.join(s))
