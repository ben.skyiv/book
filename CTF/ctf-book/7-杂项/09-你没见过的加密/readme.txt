pp.324--325，题目7-9：你没见过的加密，通过加密方式写出解密代码
题目描述：MDEzMjE5MDAyMTg0MTUzMjQwMTQ0MDc3MjUzMDk2MTc1MTUzMTE4MTg4MDEwMDA2MTg4MDA0MjM4MDI1MTA3MTU4，喜欢Linux的你，自己动手写出解密代码吧。

$ unrar x test.rar
根据 test.php 的内容，逆向编写 decrypt 函数：
$ php56 solve.php < a.txt
flag{you_are_successfu

注意，由于 srand 和 rand 函数的版本问题，需要使用 php 5.6
PHP 5.6.40 (cli) (built: Sep 25 2023 04:07:56)
PHP 7.1.33 (cli) (built: Sep 25 2023 06:21:14) ( NTS )
PHP 7.4.33 (cli) (built: Sep 25 2023 05:59:26) ( NTS )
PHP 8.2.10 (cli) (built: Sep  1 2023 11:07:36) (NTS)
