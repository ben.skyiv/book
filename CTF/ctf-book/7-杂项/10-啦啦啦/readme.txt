pp.325--330，题目7-10：啦啦啦，流量分析、隐写、伪加密和脚本编写
题目描述：隐藏在数据包中的密码

$ cp ~/repo/ctf-book/chapter7-杂项/题目-10/LOL.pcapng /tmp
$ wireshark /tmp/LOL.pcapng &
Ctrl-F 查找 lol，或者浏览数据，在第 66 条数据处
追踪 TCP 流，以“原始数据”显示，另存为 LOL.zip
$ bless LOL.zip &  # 删除 PK 之前的部分
$ 7z x LOL.zip
$ hexstr2bytes.exe < LOL/11.txt && mv /tmp/a 1.png
$ hexstr2bytes.exe < LOL/22.txt && mv /tmp/a 2.png
$ hexstr2bytes.exe < LOL/33.txt && mv /tmp/a 3.png
$ hexstr2bytes.exe < LOL/44.txt && mv /tmp/a 4.png
得到4张二维码碎片，拼接它们，得到 a.png
$ zbarimg a.png
QR-Code:flag{NP3j4ZjF&syk9$5h@x9Pqac}
