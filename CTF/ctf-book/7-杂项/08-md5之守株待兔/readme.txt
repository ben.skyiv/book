pp.323--324，题目7-8：md5之守株待兔，python脚本的编写
题目描述：从系统密钥入手，通过GET方式提交答案，直到你的密钥与系统密钥相等即成功。
请参考网址进行答题
http://ctf5.shiyanbar.com/misc/keys/keys.php

访问以上网址得到：
false
系统的密钥=efd1280f38bb5186cf6ea7926e1fec5a
您的密钥=d41d8cd98f00b204e9800998ecf8427e

系统密钥很可能是时间戳，编写以下Python程序得到答案：
$ python solve.py
http://ctf5.shiyanbar.com/misc/keys/keys.php?key=1695679793


<script>alert('c04ffec18156c696')</script>
