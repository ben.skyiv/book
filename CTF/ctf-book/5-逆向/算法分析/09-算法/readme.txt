p.265--267，题目5-9：算法，复杂的反汇编代码分析
https://www.cnblogs.com/QKSword/p/9095242.html
在 Windows 10 上用 PEiD 查看 suanfa.exe 得知 UPX 壳，用以下命令脱壳：
C:\unsafe>\bin\upx\upx.exe -d suanfa.exe
              Ultimate Packer for eXecutables
                 Copyright (C) 1996 - 2023
UPX 4.1.0 Markus Oberhumer, Laszlo Molnar & John Reiser Aug 8th 2023

        File size         Ratio      Format      Name
   --------------------   ------   -----------   -----------
      8192 <-      5632   68.75%    win32/pe     suanfa.exe

Unpacked 1 file.
$ file *.exe
suan.exe:   PE32 executable (console) ..., 5 sections
suanfa.exe: PE32 executable (console) ..., UPX compressed, 3 sections
$ ida64 suan.exe &
$ python solve.py
flag{this_is_a_easy_suanfa}
TODO
