flag, v11 = [], [
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
    "+/abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+/",
    "0123456789+/ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"]
v12, v00 = "QASWZXDECVFRBNGTHYJUMKIOLP", "tfoQsckkwhX51HYpxAjkMQYTAp5"
for m in range(26):
    for i in range(33, 127):
        v4 = chr(i)
        v5 = ord(v4) - ord(v12[m])
        if v11[v5 % 4][v5] == v00[m]:
            print(v4, end='')
            flag.append(v4)
    print(' ', end='')
print()
flag = ''.join(flag)
print(f'{flag} {len(flag)}')  # len(flag) <= 27
