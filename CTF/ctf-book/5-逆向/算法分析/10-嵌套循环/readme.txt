pp.268--269，题目5-10：嵌套循环，双层循环分析
$ ida64 crackme2.exe &
以下是关键代码：
scanf("%20s", v5);
s = "Ygtf00h1uT34___g";
for (i = 0; i <= 15; i += 4)
  for (j = 0; j <= 3; ++j)
    if (v5[i + j] != s[4 * j + i / 4])
      return 0;
编写程序求解：
$ python solve.py
Y0u_g0T_th3_f14g
在 Windows 10 上运行：
C:\unsafe>crackme2.exe
Please give your passcode: Y0u_g0T_th3_f14g
flag{8d426104da47d259aa13cf27e6a609f3}
