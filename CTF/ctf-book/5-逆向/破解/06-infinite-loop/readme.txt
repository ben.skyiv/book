pp.258--261，题目5-6：infinite-loop，ELF文件逆向与调试
$ ida64 infinite-loop &
发现以下关键函数：
__int64 __fastcall sub_4006FD(__int64 a1)
{
  int i; // [rsp+14h] [rbp-24h]
  __int64 v3[4]; // [rsp+18h] [rbp-20h]

  v3[0] = (__int64)"Dufhbmf";
  v3[1] = (__int64)"pG`imos";
  v3[2] = (__int64)"ewUglpt";
  for ( i = 0; i <= 11; ++i )
  {
    if ( *(char *)(v3[i % 3] + 2 * (i / 3)) - *(char *)(i + a1) != 1 )
      return 1LL;
  }
  return 0LL;
}
据此求解得到答案：
$ python solve.py
Code_Talkers
$ python solve.py | ./infinite-loop
Enter the password: Nice!
