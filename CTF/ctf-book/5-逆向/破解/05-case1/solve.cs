using System;
using System.Security.Cryptography;
using System.Text;

string text = "key:";
char c = 'a';
for (int i = 0; i < 16; i += 2) text += (char)(c + i);
MD5 mD = new MD5CryptoServiceProvider();
byte[] bytes = Encoding.Default.GetBytes(text.Trim());
byte[] array = mD.ComputeHash(bytes);
text = BitConverter.ToString(array).Replace("-", "");
Console.WriteLine(text);
