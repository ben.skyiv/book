pp.256--258，题目5-5：修改判断，.NET逆向
$ file key.exe
key.exe: PE32 executable (GUI) Intel 80386 Mono/.Net assembly, for MS Windows, 3 sections
$ mono key.exe
$ ilspy  # 打开 key.exe
定位到 tst.net/Form1/button1_Click，复制其中的代码，
删除 if (text.Length == 0) 语句，重新编译并运行：
$ csc -nologo solve.cs && mono solve.exe
4B342032D4EC13F7485255E1E9C21FF7
