pp.160--163 题目4-15：GIF2
$ cp ~/repo/CTF-book/chapter4-隐写/图片隐写/18\ GIF2\ 时间域/100_KHf05OI.gif /tmp/a.gif
$ file /tmp/a.gif
/tmp/a.gif: data
$ seektail.py /tmp/a.gif
0025C65D: gif89a  (00 3B Tail) (47 49 46 38 39 61: None)
0025C65D: gif87a  (00 3B Tail) (47 49 46 38 37 61: None)
$ echo -n GIF89a > /tmp/b
$ cat /tmp/b /tmp/a.gif > /tmp/c.gif
$ file /tmp/c.gif
/tmp/c.gif: GIF image data, version 89a, 970 x 172
$ identify -format "%s %T\n" /tmp/c.gif
$ identify -format "%T" /tmp/c.gif > /tmp/d
$ python solve.py < /tmp/d
b'XMAN{96575beed4dea18ded4735643aecfa35}'

file:///usr/share/doc/ImageMagick-7/www/escape.html
%s 	scene number (from input unless re-assigned)
%T 	image time delay (in centi-seconds)
