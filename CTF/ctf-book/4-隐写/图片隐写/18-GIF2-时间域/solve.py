from libnum import n2s
s = int(input().lstrip('6').replace('20', '0').replace('10', '1'), 2)
print(n2s(s))
