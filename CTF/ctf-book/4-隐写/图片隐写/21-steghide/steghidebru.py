from subprocess_tee import *


stegoFile = 'rose.jpg'
extractFile = 'hide.txt'
passFile = '/home/ben/src/book/CTF/tools/passwd10k.txt'

errors = ['could not extract', 'steghide --help', 'Syntax error']
cmdFormat = "steghide extract -sf %s -xf %s -p %s"
with open(passFile, 'r') as f:
    for line in f.readlines():
        cmd = cmdFormat % (stegoFile, extractFile, line.strip())
        p = Popen(cmd, shell=True, stdout=PIPE, stderr=STDOUT)
        content = unicode(p.stdout.read(), 'gbk')
        for err in errors:
            if err in content:
                break
        else:
            print(content,)
            print('the passphrase is %s' % line.strip())
            exit()
