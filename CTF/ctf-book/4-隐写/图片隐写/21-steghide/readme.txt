pp.167--168 题目4-18：rose.jpg

方法1：
$ stegseek rose.jpg
StegSeek 0.6 - https://github.com/RickdeJager/StegSeek
[i] Found passphrase: "123456"
[i] Original filename: "test.txt".
[i] Extracting to "rose.jpg.out".
$ iconv -f gbk -t utf8 rose.jpg.out; echo
KEY：D77F5E4385291BCB

方法2：
$ steghide extract -sf rose.jpg -p 123456
$ iconv -f gbk -t utf8 test.txt; echo
KEY：D77F5E4385291BCB
