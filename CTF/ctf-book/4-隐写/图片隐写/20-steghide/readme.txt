$ cp ~/repo/ctf-book/chapter4-隐写/图片隐写/20\ steghide隐写01/Misc.png /tmp/a
$ foremost /tmp/a
$ stegseek output/jpg/00001213.jpg
StegSeek 0.6 - https://github.com/RickdeJager/StegSeek
[i] Found passphrase: ""
[i] Original filename: "secret.txt".
[i] Extracting to "00001213.jpg.out".
$ cat 00001213.jpg.out; echo
the password  for PDF is qazwsx123
$ xdg-open output/pdf/00001175.pdf
flag{Misc_iS_S0eAsy!}
