$ 7z l ccrack.zip | grep txt
2018-11-18 12:58:07 ....A            6           20  3.txt
2018-11-18 12:58:16 ....A            6           20  4.txt
2018-11-18 12:58:25 ....A            6           20  5.txt
2018-11-18 12:51:06 ....A           64           74  flag.txt
2018-11-18 12:57:47 ....A            6           20  1.txt
2018-11-18 12:57:58 ....A            6           20  2.txt
$ 7z l -slt ccrack.zip | grep CRC
CRC = 50546A3E  3.txt
CRC = 23EE148C  4.txt
CRC = 1EC43B00  5.txt
CRC = 88FE1F5C  flag.txt
CRC = 03BBA369  1.txt
CRC = 38D36816  2.txt
$ crc32.py r 0x03BBA369  # 或者 reversecrc32.exe 03BBA369
4 bytes: {0x3d, 0x5c, 0x7b, 0x99}
verification checksum: 0x03bba369 (OK)
...
6 bytes: keyisc (OK)
...
依次对其他文件进行，得到：
keyisc32cryptonotverydifficult
$ unzip -Pc32cryptonotverydifficult ccrack.zip
$ cat flag.txt ;echo
MFXEE3DBGN2G4ZDNMN5E23KWPFQUOZDXLJMGIM3CK5SHOYJTJZ5GCSBQHU======
$ cyberchef  # 依次 base32, base64 解密后：
jpek{gvg32erhgpewwmgpkssh}
使用凯撒解密得到答案：
$ echo jpek{gvg32erhgpewwmgpkssh} | Caesar.exe | grep -i -e ctf -e flag
flag{crc32andclassiclgood}
