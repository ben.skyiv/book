$ iconv -f gbk -t utf8 tip.txt ;echo
粗心的小V忘记了压缩包的密码，她只依稀记得是一个十位数的密码，中间有串venus字符，其余忘记了，你能帮她解出压缩包吗？
$ zip2john flag.zip >/tmp/a
$ john --mask:venus?1?1?1?1?1 /tmp/a
$ john --mask:?1venus?1?1?1?1 /tmp/a
$ john --mask:?1?1venus?1?1?1 /tmp/a
$ john --mask:?1?1?1venus?1?1 /tmp/a
whjvenus76       (flag.zip/flag.txt)
$ unzip -Pwhjvenus76 flag.zip
$ cat flag.txt ;echo
flag{zip_mask_me}
