pp.179--181，题目4-27：read me，明文攻击
$ mkdir a && cd a
$ unzip ../readme.zip
$ crc32 readme.txt
53509eac
$ 7z l -slt new.zip | grep CRC
CRC = 53509EAC
CRC = BD4A2557
在 Windows 10（C盘）上将 readme.txt 发送到压缩的文件夹，
然后 ARCHPR 明文攻击，搜索密钥成功后（约6分钟）就可以停止，
在弹出的“加密密钥已成功恢复！”对话框中点击“确定”按钮，
得到解密后的压缩文件
$ mkdir b && cd b
$ unzip ../new_decrypted.zip
$ vim new.txt  # 删除前23个字符
$ base64 -d new.txt > a
$ file a
a: JPEG image data, JFIF standard 1.01, resolution (DPI), density 72x72, segment length 16, progressive, precision 8, 860x430, components 3
$ binwalk -e a
$ cat _a.extracted/hint.txt ;echo
33326b6c6d763bf61129bf9b385420a5=mk?**ijnb??*7yg?
$ cd .. && kate solve.py
$ python solve.py
$ time python solve.py  # 4m3.517s
mko09ijnbhu87ygv

以下方法行不通：
$ zip2john -a readme.txt new.zip >/tmp/a
Using file readme.txt as an 'ASCII' quick check file
ver 2.0 new.zip/readme.txt PKZIP Encr: cmplen=70, decmplen=62, crc=53509EAC
ver 2.0 new.zip/new.txt PKZIP Encr: cmplen=345537, decmplen=457455, crc=BD4A2557
$ john /tmp/a
