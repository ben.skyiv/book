$ file test.cpython-36-stegosaurus.pyc
test.cpython-36-stegosaurus.pyc: python 3.6 byte-compiled
$ pycdc test.cpython-36-stegosaurus.pyc >/tmp/a.py
$ stegosaurus -x test.cpython-36-stegosaurus.pyc
Extracted payload: flag{pyc_payload}
