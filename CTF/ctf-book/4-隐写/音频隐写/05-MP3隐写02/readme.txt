pp.188--189，题目4-33：venus，通过关键字符搜索寻找密码
$ cp ~/repo/ctf-book/chapter4-隐写/音频隐写/05\ MP3隐写02/venus.mp3 .
$ strings -15 venus.mp3  # 或者
$ strings venus.mp3 | grep -i -e pass -e venus
pass:hivenus2018
$ mv venus.mp3 ~/repo/tmp

C:\bin\MP3Stego>Decode.exe -X -P hivenus2018 Z:\tmp\venus.mp3
MP3StegoEncoder 1.1.19
See README file for copyright info
Input file = 'Z:\tmp\venus.mp3'  output file = 'Z:\tmp\venus.mp3.pcm'
Will attempt to extract hidden information. Output: Z:\tmp\venus.mp3.txt
the bit stream file Z:\tmp\venus.mp3 is a BINARY file
HDR: s=FFF, id=1, l=3, ep=off, br=9, sf=0, pd=1, pr=0, m=3, js=0, c=0, o=0, e=0
alg.=MPEG-1, layer=III, tot bitrate=128, sfrq=44.1
mode=single-ch, sblim=32, jsbd=32, ch=1
[Frame  791]Avg slots/frame = 417.459; b/smp = 2.90; br = 127.847 kbps
Decoding of "Z:\tmp\venus.mp3" is finished
The decoded PCM output file name is "Z:\tmp\venus.mp3.pcm"
C:\bin\MP3Stego>type Z:\tmp\venus.mp3.txt
flag{venus_good}
