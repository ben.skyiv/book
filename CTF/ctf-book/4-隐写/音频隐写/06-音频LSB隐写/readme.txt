pp.189--191，题目4-34：音频LSB隐写，SilentEye的使用
$ cp ~/repo/ctf-book/chapter4-隐写/音频隐写/06\ 音频LSB隐写/love.zip .
$ strings love.zip | tail -1
k3y:iloveu
$ unzip love.zip
$ strings 初恋.wav | tail -1
love is silent.
$ mv 初恋.wav ~/repo/tmp

在 Windows 10 上运行 SilentEye
Decode，勾选 Encrypted data，Key：iloveu
Decode 得到以下内容的 flag.txt：
aGJjdGZ7cHVwcHlfbG92ZX0=

$ echo aGJjdGZ7cHVwcHlfbG92ZX0= | base64 -d; echo
hbctf{puppy_love}
