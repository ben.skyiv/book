pp.187--188，题目4-32：葫芦娃，MP3隐写
$ 7z x ~/repo/ctf-book/chapter4-隐写/音频隐写/04\ MP3隐写01/葫芦娃.zip
$ cd 葫芦娃
$ file xaa
xaa: JPEG image data, JFIF standard 1.01, resolution (DPI), density 96x96, segment length 16, progressive, precision 8, 632x480, components 3
Tips: 葫芦小金刚的英文名称就是他唱的歌中的密码噢！（去除空格，有大小写区分）
$ cat xa? > 1.mp3
$ file 1.mp3
1.mp3: JPEG image data, JFIF standard 1.01, resolution (DPI), density 96x96, segment length 16, progressive, precision 8, 632x480, components 3

在 Windows 10 上运行：
C:\bin\MP3Stego> Decode -X -P GourdSmallDiamond Z:\tmp\1.mp3
MP3StegoEncoder 1.1.19
See README file for copyright info
Input file = 'Z:\tmp\1.mp3'  output file = 'Z:\tmp\1.mp3.pcm'
Will attempt to extract hidden information. Output: Z:\tmp\1.mp3.txt
the bit stream file Z:\tmp\1.mp3 is a BINARY file
HDR: s=FFF, id=1, l=3, ep=off, br=9, sf=0, pd=1, pr=0, m=0, js=0, c=0, o=0, e=0
alg.=MPEG-1, layer=III, tot bitrate=128, sfrq=44.1
mode=stereo, sblim=32, jsbd=32, ch=2
[Frame 1563]Frame cannot be located
Input stream may be empty
Avg slots/frame = 441.804; b/smp = 3.07; br = 135.302 kbps
Decoding of "Z:\tmp\1.mp3" is finished
The decoded PCM output file name is "Z:\tmp\1.mp3.pcm"
C:\bin\MP3Stego> type Z:\tmp\1.mp3.txt
解压密码:j7v@8@8QUWG0FWU^

$ foremost 1.mp3
$ unzip -P j7v@8@8QUWG0FWU^ output/zip/00001349.zip
$ cat flag.txt ;echo
MSTSEC{MSTSEC_DINGANN_KEY_IS_GSD}
