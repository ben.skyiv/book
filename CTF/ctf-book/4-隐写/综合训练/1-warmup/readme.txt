pp.198--200，题目4-39：warmup，LSB隐写
https://ciphersaw.me/2018/07/29/peak-geek-2018-misc-warmup/
$ cp ~/repo/ctf-book/chapter4-隐写/综合训练/题目一/warmup/warmup.bmp .
$ stegsolve &
Analyse, Data Extract
- Red   bit-0, Save bin, r0.txt
- Green bit-0, Save Bin, g0.txt
- Blue  bit-0, Save Bin, b0.txt

https://www.splitbrain.org/services/ook
将 r0.txt 的内容复制到文本框中 [Ook! to Text]
flag{db640436-

$ ook.sh g0.txt && bf < b0.txt
7839-4050-8339
-75a972fc553c}

flag{db640436-7839-4050-8339-75a972fc553c}

$ zsteg warmup.bmp --bits 1 --channel r --lsb --order xy --limit 2048
$ zsteg warmup.bmp --bits 1 --channel g --lsb --order xy --limit 2048
$ zsteg warmup.bmp --bits 1 --channel b --lsb --order xy --limit 2048
--bits 1：    每次只摘取颜色通道中的第 1 个比特。
--channel r： 只摘取红色通道的比特位。
--lsb：       按最低有效位优先的顺序进行摘取。
--order xy：  按照从左至右、从上至下的顺序对图像素点进行摘取。
--limit 2048：最多摘取输出 2048 字节。
