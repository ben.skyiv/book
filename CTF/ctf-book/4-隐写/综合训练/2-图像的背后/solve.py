#!/usr/bin/env python
from PIL import Image
size, i, s = 59, 0, input()
img = Image.new("RGB", (size, size))
for y in range(size):
    for x in range(size):
        p = (255, 255, 255) if s[i] == '0' else (0, 0, 0)
        img.putpixel([x, y], p)
        i += 1
img.save("/tmp/flag.png")
