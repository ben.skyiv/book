$ cp ~/repo/ctf-book/chapter4-隐写/综合训练/题目二/图像的背后/s7.jpg /tmp
$ seektail.py /tmp/s7.jpg
00018966: rar4    (C4 3D 7B 00 40 07 00 Tail) (52 61 72 21 1A 07 00: None)
000161B7: jpeg    (FF D9 Tail) (FF D8 FF: 0000)
00007286: gif89a  (00 3B Tail) (47 49 46 38 39 61: None)
00007286: gif87a  (00 3B Tail) (47 49 46 38 37 61: None)
注意到 jpeg 尾部（0x161B7 = 90551）之后还有数据
$ dd bs=1 skip=90551 if=/tmp/s7.jpg of=/tmp/1.rar
$ bless /tmp/1.rar  # 将前几个字节修改为 Rar 头部
$ file /tmp/1.rar
/tmp/1.rar: RAR archive data, v4, os: Win32
$ unrar x /tmp/1.rar
$ find -name _ |wc -l
59
$ find -name _ | head -1 | xargs cat| wc -c
59
可见这里有59个文件，每个文件59个字符，共 59*59=3481 个字符
$ find -name _ | xargs cat > /tmp/1.txt
$ python solve.py < /tmp/1.txt
$ zbarimg /tmp/flag.png
QR-Code:Welcome, Key: 2339649336ce442c
