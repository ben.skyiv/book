$ cp ~/repo/ctf-book/chapter4-隐写/综合训练/题目三/hack/hack.jpg /tmp
$ seektail.py /tmp/hack.jpg
00002A39: jpeg    (FF D9 Tail) (FF D8 FF: 0000)
在 jpg 文件尾部（0x2A39 = 10809）之后还有数据
$ dd bs=1 skip=10811 if=/tmp/hack.jpg of=/tmp/b
$ hexstr2bytes.exe < /tmp/b
/tmp/a: 82,038 bytes
$ mv /tmp/a /tmp/b && hexstr2bytes.exe < /tmp/b
/tmp/a: 41,019 bytes
$ file /tmp/a
/tmp/a: Zip archive data, at least v2.0 to extract, compression method=deflate
$ unzip /tmp/a  # 得到 1.docx 和 2.zip
$ mkdir a; cd a
$ 7z x ../1.docx
$ seektail.py word/media/image1.jpeg
00006E73: png     (AE 42 60 82 Tail) (89 50 4E 47: None)
0000697A: jpeg    (FF D9 Tail) (FF D8 FF: 0000)
在 jpeg 图像末尾之后（0x697A = 27002）有一幅缺少 png 头的 png 图像
$ dd bs=1 skip=27002 if=word/media/image1.jpeg of=/tmp/1.png
$ bless /tmp/1.png &  # 将 FF D9 改为 89 50 4E 47
$ file /tmp/1.png
/tmp/1.png: PNG image data, 133 x 260, 8-bit/color RGBA, non-interlaced
得到左半部二维码
$ cat word/tip.xml ;echo
the  password next zip  is  Thesecond2
$ cd .. && unzip -P Thesecond2 2.zip  # 得到 2.png
将 /tmp/1.png 颜色反相（在 KolourPaint 中 Ctrl-I）
与 2.png 拼接在一起，得到完整的二维码
$ zbarimg /tmp/1.png
QR-Code:flag{24asf5#&efg!@eretyghjrhj}
