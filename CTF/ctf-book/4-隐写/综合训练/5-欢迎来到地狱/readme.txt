https://www.cnblogs.com/linlei1234/p/10400799.html
$ unzip ~/repo/ctf-book/chapter4-隐写/综合训练/题目五/欢迎来到地狱/欢迎来到地狱.zip
$ cd 欢迎来到地狱/欢迎来到地狱
$ bless 地狱伊始.jpg &  # 添加文件头 FF D8 FF E0
$ file 地狱伊始.jpg
地狱伊始.jpg: JPEG image data, JFIF standard 1.01, resolution (DPI), density 96x96, segment length 16, Exif Standard: [TIFF image data, big-endian, direntries=1, orientation=upper-left], baseline, precision 8, 837x125, components 3
$ xdg-open 地狱伊始.jpg  # 到 http://pan.baidu.com/s/1i49Jhlj 下载资源
$ audacity ~/下载/地狱之声.wav  # morse code 解码得到 keyletusgo
$ xdg-open 第二层地狱.docx  # 密码：letusgo
（哈士奇 图像）  # 另存为 ~/tmp/a.png
你现在在第二层地狱中，凶猛的。。。。
额。。。。哈士奇。。。。。把守着通向第
三层地狱的钥匙，那么。。。。。。。。。。
你要用你手中的剑（握草，老子剑呢，
image steganography，，，是不是掉
在第二层地狱的哪里了）

https://sekao.net/pixeljihad/
key{you are in finally hell now}

使用以上密码解压缩 快到终点了.zip，得到 地狱大门.jpg 和 最后一层地狱.txt

$ iconv -f gbk -t utf8 最后一层地狱.txt ;echo
这里有一个大门，勇士把它轰开吧。
（tips：01110010011101010110111101101011011011110111010101101100011010010110111001100111）

$ cd ../.. && kate step1.py
$ python step1.py
ruokouling （弱口令）
$ foremost 欢迎来到地狱/欢迎来到地狱/地狱大门.jpg
$ zip2john output/zip/00000121.zip >/tmp/a
$ john /tmp/a
Password         (00000121.zip/小姐姐诶.txt)
$ unzip -P Password output/zip/00000121.zip
$ iconv -f gbk -t utf8 小姐姐诶.txt
呀呀呀呀！小姐姐（flag）已经被贝斯家族的人劫持了，你找到凯撒家族发现小姐姐被这个家族的仆人带向了地狱的另一个名门望族，贝斯家族。中途他们还经历了兔子洞穴。最后你从那个家族的sixfour手里拿到了面目全非的小姐姐（VTJGc2RHVmtYMTlwRG9yWjJoVFArNXcwelNBOWJYaFZlekp5MnVtRlRTcDZQZE42elBLQ01BPT0=）。你能帮小姐姐恢复容貌吗？

$ echo VTJGc2RHVmtYMTlwRG9yWjJoVFArNXcwelNBOWJYaFZlekp5MnVtRlRTcDZQZE42elBLQ01BPT0= | base64 -d; echo
U2FsdGVkX19pDorZ2hTP+5w0zSA9bXhVezJy2umFTSp6PdN6zPKCMA==

https://www.woodmanzhang.com/webkit/rabbitencrypt/
fxbqrwrvnwmngrjxsrnsrnhx

$ echo fxbqrwrvnwmngrjxsrnsrnhx | Caesar.exe
...
woshinimendexiaojiejieyo
...

最终得到（我是你们的小姐姐哟）
flag{woshinimendexiaojiejieyo}
