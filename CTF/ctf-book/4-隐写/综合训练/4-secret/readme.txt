$ 7z l secret.zip | grep txt
2017-11-15 13:55:10 ....A            6           20  4.txt
2017-11-15 13:55:14 ....A            6           20  5.txt
2017-11-15 13:55:18 ....A            6           20  6.txt
2017-11-15 13:37:54 ....A           31           45  flag.txt
2017-11-15 13:40:47 ....A            6           20  1.txt
2017-11-15 13:54:59 ....A            6           20  2.txt
2017-11-15 13:55:04 ....A            6           20  3.txt
$ 7z l -slt secret.zip | grep CRC
CRC = 3A9EB11A  4.txt
CRC = 7C07412C  5.txt
CRC = 15B89588  6.txt
CRC = 1D7CB767  flag.txt
CRC = 8CFCB81C  1.txt
CRC = 145383D1  2.txt
CRC = 86F17134  3.txt
$ crc32.py r 0x8CFCB81C  # 或者 reversecrc32.exe 8CFCB81C
...
6 bytes: passwo (OK)
...
依次对其他文件进行，得到：
passwordisClassicalencryptionishint6
$ unzip -PClassicalencryptionishint6 secret.zip
$ cat flag.txt; echo; python Caesar.py < flag.txt
]cX^r:X\jXiV`jVm\ipV`ek\ijk`e^t
flag{Caesar_is_very_intersting}
