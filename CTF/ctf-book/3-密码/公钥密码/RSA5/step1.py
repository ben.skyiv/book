from libnum import s2n
p = 26440615366395242196516853423447
q = 27038194053540661979045656526063
r = 32581479300404876772405716877547
with open('flag.enc', 'rb') as fp: c = s2n(fp.read())
print(f'x^3 = {c % p}(mod {p})')
print(f'y^3 = {c % q}(mod {q})')
print(f'z^3 = {c % r}(mod {r})')
