from numpy import array, matrix, linalg, around
from gmpy2 import invert


def decode(pwd, mat):
    # while len(pwd) % nc != 0: pwd += pwd[-1]
    n, t = len(pwd) // nc, array([dic[c] for c in pwd])
    t = array(matrix(t.reshape(n, nc)) * mat).reshape(n * nc)
    return ''.join(bet[c % nb] for c in t)


mat = matrix(array([[1, 2, 3], [4, 5, 6], [7, 8, 10]]))
dic, bet = {}, "abcdefghijklmnopqrstuvwxyz"
nb, nc = len(bet), mat.shape[1]
for i in range(len(bet)): dic[bet[i]] = i
pwd = "wjamdbkdeibr"
det = int(around(linalg.det(mat)))
inv = around(det * mat.I).astype(int) * invert(det, nb)
print(decode(pwd, inv))
