from Crypto.Cipher import DES
IV = '13245678'.encode()
with open('ciphertext', 'rb') as fp: cipher = fp.read()
keys = [
    b'\x00\x00\x00\x00\x00\x00\x00\x00',
    b'\x1E\x1E\x1E\x1E\x0F\x0F\x0F\x0F',
    b'\xE1\xE1\xE1\xE1\xF0\xF0\xF0\xF0',
    b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF']
for KEY in keys:
    a = DES.new(KEY, DES.MODE_OFB, IV)
    flag = a.decrypt(cipher)
    if flag.find(b'CTF{') >= 0: print(flag)
