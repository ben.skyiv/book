from Crypto.Cipher import AES
from string import printable
from itertools import product
from binascii import hexlify, unhexlify


def decrypt(cipher, passphrase):
    aes = AES.new(passphrase, AES.MODE_CBC, IV)
    return aes.decrypt(cipher)


bKEY = b"5d6I9pfR7C1JQt"  # given
IV = b"\x00" * 16  # use null bytes to minimize effect on output
pt = b"The message is protected by AES!"
ct = b"fe000000000000000000000000009ec3307df037c689300bbf2812ff89bc0b49"
# find the key using the plaintext and ciphertext we know,
# since the IV has no effect on the decryption of the second block
for i in product(printable, repeat=2):
    eKEY = ''.join(i).encode()
    KEY = bKEY + eKEY
    ptc = decrypt(unhexlify(ct), KEY)
    if ptc[16] == pt[16] and ptc[30] == pt[30] and ptc[31] == pt[31]:
        print("Got KEY: " + str(KEY))
        fKEY = KEY
        pt2 = hexlify(decrypt(unhexlify(ct), fKEY))[32:]
        print(f"Decrypting with CT mostly zeroes gives: {pt2}")
        print(f"Should be: {hexlify(pt[16:])}")
# we can now recover the rest of the ciphertext ct by XOR(pt[i], decrypted[i],
# since we chose ct 00 in all the positions we are going to recover
        answer = b""
        for i in range(13):
            pi = pt[17 + i]        # letters from the plaintext
            ti = pt2[2*i+2:2*i+4]  # 2 hex from decryption of second block
            answer += b"%02X" % (pi ^ int(ti, 16))
        rct = ct[0:2] + answer.lower() + ct[28:]
        print(f"Which means CT was: {rct}")
# now we can decrypt the recovered ct and xor against the pt to recover the IV
wp = decrypt(unhexlify(rct), fKEY)
IV = b''.join(b"%02X" % (pt[i] ^ wp[i]) for i in range(16))
IV = unhexlify(IV)
aes = AES.new(fKEY, AES.MODE_CBC, IV)  # sanity check:
print(f"Sanity check: {aes.decrypt(unhexlify(rct))}")
print(f"The IV is: {IV}")  # We won!
