from base64 import b64decode
from Crypto.Cipher import AES
key = "6v3TyEgjUcQRnWuIhjdTBA=="
txt = "rW4q3swEuIOEy8RTIp/DCMdNPtdYopSRXKSLYnX9NQe8z+LMsZ6Mx/x8pwGwofdZ"
crypter = AES.new(b64decode(key), AES.MODE_ECB)
print(crypter.decrypt(b64decode(txt)).decode())
