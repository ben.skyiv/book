from base64 import b64decode
from Crypto.Cipher import DES

cipher = 'U2FsdGVkX18fll8vjD2eBsbj7n77+YDHfY8mA9/B5fV7B6huFdkqlH4yqzAU/hCiHaOLt3kKgCuBMv+9nzN5Eg=='
cipher = b64decode(cipher)
KEY = b'\x00\x00\x00\x00\x00\x00\x00\x00'
a = DES.new(KEY, DES.MODE_ECB)
flag = a.decrypt(cipher)
print(flag)
