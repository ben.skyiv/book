from html import unescape
from base64 import b64decode
s = b64decode(unescape(b64decode(input()).decode())).decode()[1:].split('/')
print(''.join(chr(int(c)) for c in s))
