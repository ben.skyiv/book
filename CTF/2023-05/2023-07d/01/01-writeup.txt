题目给出 01.txt，是 base64 编码的信息，
解码后得到 HTML 编码的信息，
解码后得到 base64 编码的信息，
解码后得到 / 分隔的十进制数，
将这些十进制数视为 ASCII 码，得到答案：
$ python solve.py < 01.txt
welcometoattackanddefenceworld
所以答案是：
flag{welcometoattackanddefenceworld}
