from base64 import b32decode
from base58 import b58decode
print(b58decode(b32decode(input().upper())).decode())
