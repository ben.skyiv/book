from base64 import b32decode
from base58 import b58decode
s1 = 'abcdefgHIJKLMNOPQRSTUVWXYZ234567='
s2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567='
d = dict(zip(s1, s2))
s = ''.join(d[c] for c in input())
print(b58decode(b32decode(s)).decode())
