from ctypes import c_uint32
from itertools import chain
from libnum import n2s


def decrypt(V0, V1, key):
    v0, v1, total = c_uint32(V0), c_uint32(V1), c_uint32(delta * 64)
    for _ in range(64):
        v1.value -= (v0.value << 3 ^ v0.value >> 6) + v0.value ^ total.value + key[total.value >> 11 & 3]
        total.value -= delta
        v0.value -= (v1.value << 3 ^ v1.value >> 6) + v1.value ^ total.value + key[total.value & 3]
    return (v0.value, v1.value)


data = [(0xB2A7C36E, 107964013), (953859025, 0xB9B12F55), (1132264566, 1970557889)]
key, delta = [102, 108, 97, 103], 1415865428
value = chain(*(decrypt(v[0], v[1], key) for v in data))
print(''.join(n2s(c).decode() for c in value))
