# Source Generated with Decompyle++
# File: tmp7.pyc (Python 3.10)

from ctypes import *
data = [
    (0xB2A7C36EL, 107964013),
    (953859025, 0xB9B12F55L),
    (1132264566, 1970557889)]

def encrypt(V0, V1, key):
    v0 = c_uint32(V0)
    v1 = c_uint32(V1)
    delta = 1415865428
    total = c_uint32(0)
    for i in range(64):
        v0.value += (v1.value << 3 ^ v1.value >> 6) + v1.value ^ total.value + key[total.value & 3]
        total.value += delta
        v1.value += (v0.value << 3 ^ v0.value >> 6) + v0.value ^ total.value + key[total.value >> 11 & 3]
    return (v0.value, v1.value)

if __name__ == '__main__':
    flag = input('请输入flag:')
    flag1 = list(flag)
    value = []
    b = 0
    check = 0
    if len(flag1) == 24:
        for i in range(0, len(flag1), 4):
            b = ord(flag1[i]) << 24 | ord(flag1[i + 1]) << 16 | ord(flag1[i + 2]) << 8 | ord(flag1[i + 3])
            value.append(b)
        key = [
            102,
            108,
            97,
            103]
        flag_encrypt = []
        for i in range(0, 6, 2):
            res = encrypt(value[i], value[i + 1], key)
            flag_encrypt.append(res)
        for i in range(len(data)):
            if flag_encrypt[i] == data[i]:
                check += 1
                continue
            print('wrong!!!')
        if check == 3:
            print('yes!!!,you get right flag')
            return None
        return None
    None('wrong!!!')
    return None
