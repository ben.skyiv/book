from libnum import n2s
from gmpy2 import gcd, isqrt, invert

with open('RSA.py') as fp:
    for _ in range(19): fp.readline()
    c = int(fp.readline().strip()[2:])
    n = int(fp.readline().strip()[2:])
    t = int(fp.readline().strip()[2:])
e = 0x10001
p2 = gcd(n, t)
p = isqrt(p2)
q = n // p2
phi = p*(p-1)*(q-1)
d = invert(e, phi)
m = pow(c, d, n)
print(n2s(int(m)).decode())
