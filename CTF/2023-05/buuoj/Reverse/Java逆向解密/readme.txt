https://buuoj.cn/challenges#Java逆向解密
程序员小张不小心弄丢了加密文件用的秘钥，已知还好小张曾经编写了一个秘钥验证算法，聪明的你能帮小张找到秘钥吗？ 注意：得到的 flag 请包上 flag{} 提交
[818a93f3-78...]

$ unzip 818a93f3-78b6-4e8d-a5b5-4c492695cef4.zip
$ jadx-gui Reverse.class &
$ python solve.py
This_is_the_flag_!
$ python solve.py | java Reverse
Please input the flag ：
Your input is ：
This_is_the_flag_!
Result:
Congratulations！
