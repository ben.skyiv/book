from base64 import b64decode
from collections import Counter

with open('read/11.txt', encoding='iso-8859-1') as fp: t = fp.read()
s = Counter()
s.update(t)
s = ''.join(c for c, _ in s.most_common())
s = ''.join(c for c in s if 'a' <= c <= 'z')[:15]
print(s)
with open('decode.txt') as fp: d = fp.read().rstrip().split('\n')
d = dict((c[0], c[4:]) for c in d)
s = ''.join(d[c] for c in s)
print(s)
s = b64decode(s)
print(s)
