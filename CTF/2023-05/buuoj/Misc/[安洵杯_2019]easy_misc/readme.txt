https://buuoj.cn/challenges#[安洵杯 2019]easy misc
得到的 flag 请包上 flag{} 提交。
[attachment.rar]

$ unrar x attachment.rar
在 decode.zip 中发现：
注释: FLAG IN ((√2524921X85÷5+2)÷15-1794)+NNULLULL,
$ python
>>> from math import sqrt
>>> (int(sqrt(2524921))*85//5+2)//15-1794
7
$ zip2john decode.zip > /tmp/a
$ john --mask=?d?d?d?d?d?d?dNNULLULL, /tmp/a
2019456NNULLULL, (decode.zip/decode.txt)
$ unzip -P2019456NNULLULL, decode.zip
$ foremost 小姐姐.png
$ bwm.py decode output/png/00000000.png output/png/00000232.png 1.png
$ xdg-open 1.png  # 得到 in 11.txt
$ file read/11.txt 
read/11.txt: ISO-8859 text, with very long lines (1003), with CRLF line terminators
$ iconv -f GB18030 -t UTF8 read/hint.txt ;echo
hint:取前16个字符
$ python solve.py 
etaonrhisdluygw
QW8obWdIWT9pMkF-sd5REtRQSQWjVfXiE/WSFTajBtcw=
b'Ao(mgHY?i2Alw\x94D\xb5\x14\x12Ah\xd5}x\x84\xfdd\x85M\xa8\xc1\xb5\xcc'
