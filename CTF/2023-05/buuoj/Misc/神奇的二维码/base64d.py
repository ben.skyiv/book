from base64 import b64decode, binascii
s = input()
try:
    for _ in range(50): s = b64decode(s)
except binascii.Error: pass
print(s.decode())
