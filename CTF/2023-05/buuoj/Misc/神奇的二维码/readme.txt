https://buuoj.cn/challenges#[SWPU2019]神奇的二维码
得到的 flag 请包上 flag{} 提交。
[attachment.rar]

$ unrar x attachment.rar
$ cd 神奇的二维码 && zbarimg BitcoinPay.png
QR-Code:swpuctf{flag_is_not_here}
$ binwalk -e BitcoinPay.png
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 400 x 400, 8-bit/color RGBA, non-interlaced
28932         0x7104          RAR archive data, version 5.x
29034         0x716A          RAR archive data, version 5.x
94226         0x17012         RAR archive data, version 5.x
99220         0x18394         RAR archive data, version 5.x
$ cd _BitcoinPay.png.extracted
$ base64 -d encode.txt; echo
asdfghjkl1234567890
$ unrar x -pasdfghjkl1234567890 看看flag在不在里面^_^.rar
$ file flag.doc
flag.doc: ASCII text, with very long lines (8272), with no line terminators
$ python ../../base64d.py < flag.doc
comEON_YOuAreSOSoS0great
$ unrar x -pcomEON_YOuAreSOSoS0great 18394.rar
$ file good/good.mp3
good/good.mp3: MPEG ADTS, layer III, v1, 64 kbps, 44.1 kHz, Monaural
$ audacity good/good.mp3 &  # 由此看出是 morse code
https://morsecode.world/international/decoder/audio-decoder-adaptive.html
使用以上网站得到：  MORSEISVERYVERYEASY
$ python -c 'print("MORSEISVERYVERYEASY".lower())'
morseisveryveryeasy
提交 flag{morseisveryveryeasy}，正确！
