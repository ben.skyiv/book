https://buuoj.cn/challenges#[WUSTCTF2020]find_me
得到的 flag 请包上 flag{} 提交。
感谢 Iven Huang 师傅供题。
比赛平台：https://ctfgame.w-ais.cn/
[attachment.jpg]

$ exiftool attachment.jpg  # 在图片的属性中发现盲文
$ exiftool attachment.jpg | grep XP | cut -d: -f2 | tee /tmp/a
 ⡇⡓⡄⡖⠂⠀⠂⠀⡋⡉⠔⠀⠔⡅⡯⡖⠔⠁⠔⡞⠔⡔⠔⡯⡽⠔⡕⠔⡕⠔⡕⠔⡕⠔⡕⡍=
$ braille.py < /tmp/a
b' wctf2020{y$0$u_f$1$n$d$_M$e$e$e$e$e}='
提交   flag{y$0$u_f$1$n$d$_M$e$e$e$e$e}，正确！
