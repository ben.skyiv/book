using Skyiv.Utils;
var ascii = new System.Text.ASCIIEncoding();
var z = System.Convert.ToUInt32(args[0], 16);
var bs = new byte[4]; bs.ReverseCrc32(z, 0);
System.Console.WriteLine("{0}", ascii.GetString(bs));
