from base64 import b64decode
with open('/tmp/a', 'r') as f: s = f.read()
with open('/tmp/b', 'wb') as f: f.write(b64decode(''.join(s.split())))
