https://buuoj.cn/challenges#[GXYCTF2019]gakki
得到的 flag 请包上 flag{} 提交。
[attachment.rar]

$ unrar x attachment.rar
$ foremost wolaopo.jpg
$ rar2john output/rar/00000268.rar >/tmp/a
! file name: flag.txt
$ john --mask:?d?d?d?d /tmp/a
8864             (00000268.rar)
$ unrar x -p8864 output/rar/00000268.rar
$ counter.py < flag.txt
GXY{gaki_IsMyw1fe}
