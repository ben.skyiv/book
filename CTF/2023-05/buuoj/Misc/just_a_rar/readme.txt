https://buuoj.cn/challenges#[BJDCTF2020]just_a_rar
buuoj, Misc, [BJDCTF2020]just_a_rar, 1分
得到的 flag 请包上 flag{} 提交。来源：https://github.com/BjdsecCA/BJDCTF2020
[attachment.rar]

$ unrar x attachment.rar
$ rar2john 23/4位数.rar > /tmp/a
! file name: flag.jpg
$ john --mask:?d?d?d?d /tmp/a
2016             (4位数.rar)
$ unrar x -p2016 23/4位数.rar

方法1：
$ strings -el flag.jpg
flag{Wadf_123}

方法2：
$ jhead -v flag.jpg | grep -ia flag
    Windows-XP comment = flag{Wadf_123}
File name    : flag.jpg

方法3：
$ exiftool flag.jpg | grep -i flag
File Name                       : flag.jpg
XP Comment                      : flag{Wadf_123}

方法4：
$ cyberchef  # 使用下列 recipe= ，拖入 flag.jpg，得到 flag{Wadf_123}
Extract_EXIF()Split(':',',')From_Decimal('Comma',false)Remove_null_bytes()

方法5：
$ bless flag.jpg &
找到 GB18030 编码的 flag

方法6：
在 Windows 操作系统下，选中 flag.jpg，查看“属性，详细信息”，在“备注”中找到 flag
