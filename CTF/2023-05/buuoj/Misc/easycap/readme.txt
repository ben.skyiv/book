https://buuoj.cn/challenges#easycap
注意：得到的 flag 请包上 flag{} 提交
[876932b1-5...]

$ unzip 876932b1-57fc-4766-86cc-8031b0efba99.zip
$ file easycap.pcap
easycap.pcap: pcap capture file, microsecond ts (little-endian) - version 2.4 (Ethernet, capture length 262144)
$ wireshark easycap.pcap &
按 Ctrl-Alt-Shift-T 追踪 TCP 流，得到：
FLAG:385b87afc8671dee07550290d16a8071
