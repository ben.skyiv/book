https://buuoj.cn/challenges#[ACTF新生赛2020]base64隐写
得到的 flag 请包上 flag{} 提交。
[attachment.tar]

$ tar xf attachment.tar
$ unzip tmp/近在眼前.zip
$ base64stego.py < ComeOn\!.txt
ACTF{6aseb4_f33!}
提交 flag{6aseb4_f33!}，正确！
