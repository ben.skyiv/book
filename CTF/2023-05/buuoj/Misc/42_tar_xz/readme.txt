https://buuoj.cn/challenges#[INSHack2018]42.tar.xz
buuoj, Misc, [INSHack2018]42.tar.xz, 1分
得到的 flag 请包上 flag{} 提交。
[attachment.tar]

题目给出 attachment.tar（48.5 Kib），
解压后得到 tmp 目录下的 42.tar.xz，继续解压，得到
1.tar.xz, 2.tar.xz, ..., 42.tar.xz
以上文件内容全部相同，继续解压，依旧如此。
使用以下 Ptyho 程序求解（得到 flag, flag1, ..., flag10，每个都是 500MiB）
$ python solve.py
0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,...,46,
INSA{04ebb0d6a87f9771f2eea4dce5b91a85e7623c13301a8007914085a91b3ca6d9}
