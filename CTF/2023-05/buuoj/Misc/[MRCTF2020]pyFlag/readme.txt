https://buuoj.cn/challenges#[MRCTF2020]pyFlag
得到的 flag 请包上 flag{} 提交。
感谢天璇战队供题。
[attachment.zip]

$ unzip attachment.zip
$ strings *.jpg | grep -i flag
SecretFile/flag.txt
SecretFile/flag.txt
$ strings *.jpg | grep -i 'file part'
[Secret File Part 2:]
[Secret File Part 3:]
[Secret File Part 1:]PK
$ bless *.jpg  # 删除 [Secret File Part ?:] 以及之前的内容
$ cat Setsuna.jpg Furan.jpg Miku.jpg > a.zip
$ zip2john a.zip > /tmp/a
$ john /tmp/a
1234             (a.zip)
$ 7z x -p1234 a.zip
$ cat SecretFile/.hint.txt ;echo
我用各种baseXX编码把flag套娃加密了，你应该也有看出来。
但我只用了一些常用的base编码哦，毕竟我的智力水平你也知道...像什么base36base58听都没听过
提示：0x10,0x20,0x30,0x55
$ cat SecretFile/flag.txt ;echo
G&eOhGcq(ZG(t2*H8M3dG&wXiGcq(ZG&wXyG(j~tG&eOdGcq+aG(t5oG(j~qG&eIeGcq+aG)6Q<G(j~rG&eOdH9<5qG&eLvG(j~sG&nRdH9<8rG%++qG%__eG&eIeGc+|cG(t5oG(j~sG&eOlH9<8rH8C_qH9<8oG&eOhGc+_bG&eLvH9<8sG&eLgGcz?cG&3|sH8M3cG&eOtG%_?aG(t5oG(j~tG&wXxGcq+aH8V6sH9<8rG&eOhH9<5qG(<E-H8M3eG&wXiGcq(ZG)6Q<G(j~tG&eOtG%+<aG&wagG%__cG&eIeGcq+aG&M9uH8V6cG&eOlH9<8rG(<HrG(j~qG&eLcH9<8sG&wUwGek2)
$ python solve.py < SecretFile/flag.txt
MRCTF{Y0u_Are_4_p3rFect_dec0der}
