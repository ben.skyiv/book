from re import match
from base64 import b16decode, b32decode, b64decode, b85decode


def detect(text):
    if match("^[0-9A-F=]+$",      text): return b16decode
    if match("^[A-Z2-7=]+$",      text): return b32decode
    if match("^[A-Za-z0-9+/=]+$", text): return b64decode
    return                                      b85decode


text = input()
while True:
    if 'MRCTF{' in text: print(text); break
    text = detect(text)(text).decode()
