https://buuoj.cn/challenges#九连环
buuoj, Misc, 九连环, 1分
注意：得到的 flag 请包上 flag{} 提交。
[389a0c11-d...]

$ unzip 389a0c11-d0df-4180-829a-b529e6b0a1bc.zip
$ foremost 123456cry.jpg
$ zipclean.exe output/zip/00000038.zip
00000000: 50-4B-03-04 0A-00 00-08
0000714F: 50-4B-01-02 3F-00 0A-00 01-08
000071BF: 50-4B-01-02 3F-00 0A-00 00-08
00007215: 50-4B-01-02 3F-00 14-00 01-08
0000727D: 50-4B-01-02 3F-00 0A-00 00-08
/tmp/a.zip: 29,425 bytes
$ unzip /tmp/a.zip
$ steghide extract -sf asd/good-已合并.jpg
Enter passphrase:
wrote extracted data to "ko.txt".
$ kate ko.txt                    # 或者
$ iconv -f GB18030 -t UTF8 ko.txt ;echo
看到这个图片就是压缩包的密码：
bV1g6t5wZDJif^J7
$ 7z x -pbV1g6t5wZDJif^J7 asd/qwe.zip
$ cat flag.txt ;echo
flag{1RTo8w@&4nK@z*XL}
