https://buuoj.cn/challenges#假如给我三天光明

$ unzip c103bd46-5ada-4821-8664-d7bc26be142a.zip
$ xdg-open 假如给我三天光明/pic.jpg &
识别上图中的盲文，得到解压密码 kmdonowg
$ file 假如给我三天光明/music.zip  # 注意，是 RAR，不是 zip
假如给我三天光明/music.zip: RAR archive data, v4, os: Win32
$ unrar x -pkmdonowg 假如给我三天光明/music.zip
$ file music.wav
music.wav: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 8000 Hz
$ audacity music.wav &  # 由此看出是 morse code
https://morsecode.world/international/decoder/audio-decoder-adaptive.html
使用以上网站得到：  CTFWPEI08732?23DZ
$ python -c "print('CTFWPEI08732?23DZ'.lower())"
ctfwpei08732?23dz
提交 flag{wpei08732?23dz}，正确！
