https://buuoj.cn/challenges#[UTCTF2020]zero
得到的 flag 请包上 flag{} 提交。
[attachment.txt]

$ vim attachment.txt
发现很多 zero-width 字符，使用以下网站解码得到：
https://330k.github.io/misc_tools/unicode_steganography.html
utflag{whyNOT@sc11_4927aajbqk14}
