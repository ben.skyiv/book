https://buuoj.cn/challenges#N种方法解决
buuoj, Misc, N种方法解决, 1分
注意：得到的 flag 请包上 flag{} 提交
[f64ca6fa-11...]

$ unzip f64ca6fa-1113-4ebe-8dbe-5e2d2db41ae1.zip
$ file KEY.exe
KEY.exe: ASCII text, with very long lines (3870), with no line terminators
$ cut -c23- KEY.exe | base64 -d > /tmp/a
$ file /tmp/a
/tmp/a: PNG image data, 133 x 133, 8-bit/color RGBA, non-interlaced
$ zbarimg /tmp/a
QR-Code:KEY{dca57f966e4e4e31fd5b15417da63269}
提交   flag{dca57f966e4e4e31fd5b15417da63269}
正确！
