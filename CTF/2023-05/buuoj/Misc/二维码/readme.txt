https://buuoj.cn/challenges#二维码
buuoj, Misc, 二维码, 1分
注意：得到的 flag 请包上 flag{} 提交
[f4571698-e6...]

$ unzip f4571698-e6e4-41b6-8853-2aab17cef02a.zip
$ zbarimg QR_code.png
QR-Code:secret is here
$ binwalk QR_code.png
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 280 x 280, 1-bit colormap, non-interlaced
471           0x1D7           Zip archive data, encrypted at least v2.0 to extract, compressed size: 29, uncompressed size: 15, name: 4number.txt
650           0x28A           End of Zip archive, footer length: 22
$ foremost QR_code.png
$ 7z l output/zip/00000000.zip
...
   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
2016-01-15 10:04:23 ....A           15           29  4number.txt
------------------- ----- ------------ ------------  ------------------------
2016-01-15 10:04:23                 15           29  1 files

解压时发现需要密码，用 john 暴力破解
$ zip2john output/zip/00000000.zip > /tmp/a
ver 2.0 00000000.zip/4number.txt PKZIP Encr: cmplen=29, decmplen=15, crc=AE4C3446
$ john /tmp/a
...
Proceeding with incremental:ASCII
7639             (00000000.zip/4number.txt)
1g 0:00:00:06 DONE 3/3 (2023-06-09 06:59) 0.1658g/s 9579Kp/s 9579Kc/s 9579KC/s 08r..sapphine
$ 7z x -p7639 output/zip/00000000.zip
$ cat 4number.txt; echo
CTF{vjpw_wnoei}
注意：根据题目提示，答案是
flag{vjpw_wnoei}
