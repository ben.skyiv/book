from hashlib import md5
s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
m = 'TASC?O3RJMV+WDJKX-ZM'
d = 'E903???4DAB????08?????51?80??8A?'.lower()
for i in s:
    z0 = m.replace('?', i)
    for j in s:
        z1 = z0.replace('+', j)
        for k in s:
            z2 = z1.replace('-', k)
            z3 = md5(z2.encode()).hexdigest()
            if z3[:4] == d[:4] and z3[7:11] == d[7:11]:
                print(d.upper())
                print(z3.upper())
