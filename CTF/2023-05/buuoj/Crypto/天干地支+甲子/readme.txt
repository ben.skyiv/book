https://buuoj.cn/challenges#[MRCTF2020]天干地支+甲子
得到的 flag 请包上 flag{} 提交。
感谢天璇战队供题。
[attachment.rar]

$ unrar x attachment.rar  # 得到 天干地支+甲子.txt
$ tail -n +3 天干地支+甲子.txt | years60.py | python solve.py
Goodjob
