#!/usr/bin/env python3
from sys import stdin
ts, ds = '甲乙丙丁戊己庚辛壬癸', '子丑寅卯辰巳午未申酉戌亥'
td = [ts[i % 10] + ds[i % 12] for i in range(60)]
print(' '.join(str(td.index(c)+1 if c in td else 0) for c in stdin.read().split()))
