from libnum import n2s
from gmpy2 import gcdext, powmod
with open('output.txt') as fp:
    s1 = list(map(int, fp.readline().strip()[1:-1].split(','))); fp.readline()
    s2 = list(map(int, fp.readline().strip()[1:-1].split(','))); fp.readline()
    c1 = int(fp.readline().split('=')[1]); fp.readline()
    c2 = int(fp.readline().split('=')[1])
n, e1, e2 = s1[0], s1[1], s2[1]
_, s1, s2 = gcdext(e1, e2)
m = powmod(c1, s1, n) * powmod(c2, s2, n) % n
print(n2s(int(m)).decode())
