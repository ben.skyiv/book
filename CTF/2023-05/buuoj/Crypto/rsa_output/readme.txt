https://buuoj.cn/challenges#[BJDCTF2020]rsa_output
得到的 flag 请包上 flag{} 提交。来源：https://github.com/BjdsecCA/BJDCTF2020
[attachment.zip]

$ unzip attachment.zip
$ python  rsa_common_mod.py
BJD{r3a_C0mmoN_moD@_4ttack}
