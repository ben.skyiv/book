from libnum import n2s
from base64 import b64decode
from gmpy2 import gcd, invert, powmod
with open('enc') as fp:
    p = int(fp.readline().split('=')[1])
    n = int(fp.readline().split('=')[1])
    c = int(b64decode(fp.readline()[4::][::-1]).decode())
q = n // p
phi = (p-1)*(q-1)
e0 = 0
for e in range(50000, 70000):
    while True:
        if gcd(e, phi) == 1: break
        else: e -= 1
    if e0 == e: continue
    else: e0 = e
    d = invert(e, phi)
    m = powmod(c, d, n)
    flag = n2s(int(m))
    if flag[:4] == b'flag':
        print(flag, e)
