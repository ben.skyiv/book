from gmpy2 import invert
from libnum import n2s
with open('/tmp/pq.txt') as fp:
    p = int(fp.readline().split('=')[1].strip())
    q = int(fp.readline().split('=')[1].strip())
with open('task.py') as fp:
    for _ in range(20): fp.readline()
    c = int(fp.readline().split('=')[1].strip())
e = 0x10001
n = p*q
phi = (p-1)*(q-1)
d = invert(e, phi)
m = pow(c, d, n)
print(n2s(int(m)).decode())
