from libnum import n2s
from operator import mul
from functools import reduce
from gmpy2 import gcd, invert, powmod
from Crypto.Util.number import sieve_base
with open('task.py') as fp:
    for _ in range(19): fp.readline()
    n = int(fp.readline().split('=')[1].strip())
    c = int(fp.readline().split('=')[1].strip())
e = 0x10001
p = gcd(powmod(2, reduce(mul, sieve_base), n) - 1, n)
q = n // p
d = invert(e, (p-1)*(q-1))
m = powmod(c, d, n)
print(n2s(int(m)).decode())
