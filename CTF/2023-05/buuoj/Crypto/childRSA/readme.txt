https://buuoj.cn/challenges#[NCTF2019]childRSA
得到的 flag 请包上 flag{} 提交。
[attachment.zip]

$ unzip attachment.zip

方法1：
$ grep '# n = ' task.py | cut -c7- > /tmp/n.txt
$ (cd /tmp; yafu "factor(@)" -batchfile n.txt > out.txt)
$ grep PRP /tmp/out.txt > /tmp/pq.txt
$ python solve1.py

方法2：
https://blog.csdn.net/xiao_han_a/article/details/118670716
$ python solve2.py

最终得到 flag：
NCTF{Th3r3_ar3_1ns3cure_RSA_m0duli_7hat_at_f1rst_gl4nce_appe4r_t0_be_s3cur3}
