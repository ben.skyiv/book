from gmpy2 import invert
from libnum import n2s

with open('题目.txt', 'r') as f:
    p = int(f.readline().split('=')[1].rstrip())
    q = int(f.readline().split('=')[1].rstrip())
    dp = int(f.readline().split('=')[1].rstrip())
    dq = int(f.readline().split('=')[1].rstrip())
    c = int(f.readline().split('=')[1].rstrip())
invp = invert(p, q)
m1 = pow(c, dp, p)
m2 = pow(c, dq, q)
m = (m2 - m1) * invp % q * p + m1
print(n2s(int(m)).decode())
