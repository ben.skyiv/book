from gmpy2 import gcd, invert
from libnum import n2s


def seek_i_j():
    for i in range(len(ns) - 1):
        for j in range(i + 1, len(ns)):
            if gcd(ns[i], ns[j]) > 1: return (i, j)


ns, cs = [], []
with open('1.txt', 'r') as f:
    f.readline()
    e = int(f.readline().split('=')[1])
    f.readline()
    while True:
        s = f.readline().split('=')
        if len(s) < 2: break
        ns.append(int(s[1]))
        cs.append(int(f.readline().split('=')[1]))
        f.readline()
i, j = seek_i_j()
n = ns[i]
p = gcd(n, ns[j])
q = n // p
c = cs[i]
phi = (p-1)*(q-1)
d = invert(e, phi)
m = pow(c, d, n)
print(n2s(int(m)).decode())
