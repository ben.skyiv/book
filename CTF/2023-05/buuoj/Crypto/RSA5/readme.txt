https://buuoj.cn/challenges#RSA5
buuoj, Crypto, RSA5, 1分
得到的 flag 请包上 flag{} 提交。
[a60bcf65-35..]

$ unzip a60bcf65-3587-46cc-a595-0be5aaf2326f.zip
$ head -3 1.txt
m = xxxxxxxx
e = 65537
========== n c ==========
题目给出了指数 e，然后是 20 组 n 和 c（~617 digits）
用以下 Python 程序求解（寻找 gcd(n1, n2) > 1）：
$ python solve.py
flag{abdcbe5fd94e23b3de429223ab9c2fdf}
