from libnum import n2s
from gmpy2 import gcd, powmod, invert
with open('task.py') as fp:
    for _ in range(21): fp.readline()
    s1 = list(map(int, fp.readline().split()))
    c0 = int(fp.readline())  # pow(294, e, n1)
    s2 = list(map(int, fp.readline().split()))
c1, n1, n2 = s1[0], s1[1], s2[1]
q = gcd(n1, n2)
p = n1 // q
phi = (p-1)*(q-1)
for e in range(100_000):
    if c0 == powmod(294, e, n1): break
d = invert(e, phi)
m = powmod(c1, d, n1)
print(n2s(int(m)).decode())
