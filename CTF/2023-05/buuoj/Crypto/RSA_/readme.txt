https://buuoj.cn/challenges#[BJDCTF2020]RSA
得到的 flag 请包上 flag{} 提交。来源：https://github.com/BjdsecCA/BJDCTF2020
[attachment.zip]

$ unzip attachment.zip
$ python solve.py
BJD{p_is_common_divisor}
