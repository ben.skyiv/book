from os import system

with open('rsa_task.py', 'r') as f:
    for _ in range(7): f.readline()
    e = f.readline().split('=')[1].strip()
    for _ in range(18 - 9): f.readline()
    n = f.readline().strip()
system(f'RsaCtfTool.py --private --createpub -n {n} -e {e}')
