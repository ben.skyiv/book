https://buuoj.cn/challenges#[BJDCTF2020]easyrsa
得到的 flag 请包上 flag{} 提交。来源：https://github.com/BjdsecCA/BJDCTF2020
[attachment.zip]

方法1：
$ unzip attachment.zip
$ python rsa_create_pub.py > /tmp/pub
$ RsaCtfTool.py --private --publickey /tmp/pub --output /tmp/pri
[*] Testing key /tmp/pub.
[*] Performing lucas_gcd attack on /tmp/pub.
[*] Performing mersenne_primes attack on /tmp/pub.
[*] Performing factordb attack on /tmp/pub.
[*] Attack success with factordb method !
[+] Total time elapsed min,max,avg: 0.0006/0.1840/0.0923 sec.
$ python rsa_private_txt.py
BJD{Advanced_mathematics_is_too_hard!!!}

方法2：
https://blog.csdn.net/weixin_44017838/article/details/104989678
z = Fraction(1,Derivative(arctan(p),p))-Fraction(1,Derivative(arth(q),q))
arctan(x)' = 1/(1+x**2)
arth(x)'   = 1/(1-x**2)
arth(x)    = ln[(1+x)/(1-x)]/2  # arctanh，反双曲正切
z = p**2 + q**2
n = p*q
令 a = isqrt(z+2*n), b = isqrt(z-2*n)，则
则 p = (a+b)//2, q = (a-b)//2
$ python solve.py
BJD{Advanced_mathematics_is_too_hard!!!}
