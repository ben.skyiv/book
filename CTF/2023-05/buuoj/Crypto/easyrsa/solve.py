from gmpy2 import invert, isqrt
from libnum import n2s

with open('rsa_task.py', 'r') as f:
    for _ in range(7): f.readline()
    e = int(f.readline().split('=')[1].strip())
    for _ in range(16 - 9): f.readline()
    c = int(f.readline().strip())
    z = int(f.readline().strip())
    n = int(f.readline().strip())
a, b = isqrt(z+2*n), isqrt(z-2*n)
p, q = (a+b) // 2, (a-b) // 2
phi = (p-1)*(q-1)
d = invert(e, phi)
m = pow(c, d, n)
print(n2s(int(m)).decode())
