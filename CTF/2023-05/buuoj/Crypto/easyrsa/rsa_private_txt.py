import libnum
import rsa

with open('/tmp/pri', mode='rb') as privatefile:
    keydata = privatefile.read()
key = rsa.PrivateKey.load_pkcs1(keydata)
with open('rsa_task.py', 'r') as f:
    for i in range(15): f.readline()
    c = int(f.readline().strip())
m = pow(c, key.d, key.n)
print(libnum.n2s(int(m)).decode())
