https://buuoj.cn/challenges#异性相吸
最近出现了一个奇葩观点，说性别都不一样，怎么能谈恋爱？为了证明这个观点错误，请大家证明异性是相吸的。 注意：得到的 flag 请包上 flag{} 提交
[b8c1caee-43..]

$ unzip b8c1caee-43d6-42ee-aecc-d72502a5ade2.zip
$ python solve.py
flag{ea1bc0988992276b7f95b54a7435e89e}
