https://buuoj.cn/challenges#RSA3
注意：得到的 flag 请包上 flag{} 提交
[92e0d936-c...]

$ unzip 92e0d936-c5ea-43c0-8d8c-d66effec3456.zip
$ cat 题目.txt
c1=2232203527566323704164689377045...361  # 617 digits
n=22708078815885011462462049064339...801  # 617 digits
e1=11187289
c2=1870201004518701555654869164239...397  # 617 digits
e2=9647291
$ python rsa_common_mod.py
flag{49d91077a1abcb14f1a9d546c80be9ef}
