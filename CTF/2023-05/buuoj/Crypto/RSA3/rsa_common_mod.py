from gmpy2 import gcdext, powmod
from libnum import n2s

with open('题目.txt', 'r') as f:
    c1 = int(f.readline().split('=')[1])
    n  = int(f.readline().split('=')[1])
    e1 = int(f.readline().split('=')[1])
    c2 = int(f.readline().split('=')[1])
    e2 = int(f.readline().split('=')[1])
_, s1, s2 = gcdext(e1, e2)
m = powmod(c1, s1, n) * powmod(c2, s2, n) % n
print(n2s(int(m)).decode())
