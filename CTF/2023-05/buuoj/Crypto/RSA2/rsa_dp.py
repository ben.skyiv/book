from gmpy2 import invert
from libnum import n2s


def seek_p():
    for x in range(1, e):
        if (e * dp - 1) % x != 0: continue
        p = (e * dp - 1) // x + 1
        if n % p == 0: return p


with open('题目.txt', 'r') as f:
    e = int(f.readline().split('=')[1].rstrip())
    n = int(f.readline().split('=')[1].rstrip())
    dp = int(f.readline().split('=')[1].rstrip())
    f.readline()
    c = int(f.readline().split('=')[1].rstrip())
p = seek_p()
q = n // p
phi = (p-1)*(q-1)
d = invert(e, phi)
m = pow(c, d, n)
print(n2s(int(m)).decode())
