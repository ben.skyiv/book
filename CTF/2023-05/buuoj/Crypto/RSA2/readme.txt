https://buuoj.cn/challenges#RSA2
注意：得到的 flag 请包上 flag{} 提交
[3e6857a6-1...]

$ unzip 3e6857a6-18f1-4f9e-baa2-f61dfc8d110e.zip
$ cat 题目.txt
e = 65537
n = 248254007851526241177721526698901802985832...113  # 309 digits
dp = 90507449805234690464302513287951833069192...657  # 153 digits

c = 140423670976252696807533673586209400575664...751  # 309 digits
根据定义，dp = d % (p-1)，用以下 Python 程序求解：
$ python rsa_dp.py
flag{wow_leaking_dp_breaks_rsa?_98924743502}
