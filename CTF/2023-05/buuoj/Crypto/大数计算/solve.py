from functools import reduce
from operator import mul
from gmpy2 import mpz
a = [-80538738812075974, 80435758145817515, 12602123297335631]
flag = [str(mpz(reduce(mul, range(1, 2020+1))))[:8]]  # Part1
flag.append(str(520**1314 + 2333**666)[:8])           # Part2
flag.append(str(sum(map(abs, a)))[:8])                # Part3
flag.append((22**2 + 36) * 1314)                      # Part4
print('flag{' + '-'.join(map(lambda x: hex(int(x))[2:], flag)) + '}')
