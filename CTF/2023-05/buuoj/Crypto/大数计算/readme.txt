https://buuoj.cn/challenges#[WUSTCTF2020]大数计算
得到的 flag 请包上 flag{} 提交。
感谢 Iven Huang 师傅供题。
比赛平台：https://ctfgame.w-ais.cn/
[attachment.zip]

$ unzip attachment.zip
得到 题目描述.txt 和 图片附件.zip
Part1 = 2020*2019*2018* ... *3*2*1 的前8位
Part2 = 520^1314 + 2333^666 的前8位
Part3 = 宇宙终极问题的答案 x,y,z绝对值和的前8位
解不定方程：$x^3+y^3+z^3=42$
Part4 = 见图片附件，计算结果乘上1314
图片为：$\int_0^{22}2x dx+36$
flag等于 wctf2020{Part1-Part2-Part3-Part4} 每一Part都为数的十六进制形式（不需要0x)，并用 '-' 连接
$ python solve.py
flag{24d231f-403cfd3-108db5e-a6d10}
