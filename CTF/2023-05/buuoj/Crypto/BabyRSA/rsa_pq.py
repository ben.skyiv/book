from libnum import n2s
with open('BabyRsa') as fp:
    w = int(fp.readline().split(':')[1], 16)  # p+q
    z = int(fp.readline().split(':')[1], 16)  # (p+1)(q+1)
    e = int(fp.readline().split(':')[1], 16)
    d = int(fp.readline().split(':')[1], 16)
    c = int(fp.readline().split(':')[1], 16)
print(n2s(pow(c, d, z-w-1)).decode())
