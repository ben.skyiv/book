https://buuoj.cn/challenges#[GUET-CTF2019]BabyRSA
得到的 flag 请包上 flag{} 提交。
[BabyRsa]

$ cat BabyRsa
p+q : 0x1232fecb92adead91613e7d9ae5e36fe6bb765317d6ed38a...2ea
(p+1)(q+1) : 0x5248becef1d925d45705a7302700d6a0ffe5877fd...740
e : 0xe6b1bee47bd63f615c7d0a43c529d219
d : 0x2dde7fbaed477f6d62838d55b0d0964868cf6efb2c282a5f13...1a5
enc_flag : 0x50ae00623211ba6089ddfae21e204ab616f6c9d294e...a9a

令 z = (p+1)(q+1) = pq + (p+q) + 1
则 n = pq = z - (p+q) -1
$ python rsa_pq.py  # flag = pow(c, d, n)
flag{cc7490e-78ab-11e9-b422-8ba97e5da1fd}
