https://buuoj.cn/challenges#rsa2
听说这题是rsa的续集 注意：得到的 flag 请包上 flag{} 提交
[34f80ba9-8d...]

$ cat 34f80ba9-8d9a-43cb-b3e3-8283c50e62b3.py

N = 1019918097775532534702767513992647401311576823...471  # 309 digits
e = 4673191956326572130710518041030251867667613550...85   # 308 digits

import hashlib
flag = "flag{" + hashlib.md5(hex(d)).hexdigest() + "}"

$ python rsa_create_pub.py > /tmp/pub  # 创建公钥
$ RsaCtfTool.py --private --publickey /tmp/pub --dumpkey
...
[*] Attack success with wiener method !
[+] Total time elapsed min,max,avg: 0.0003/60.0003/6.7770 sec.
...
d: 8920758995414587152829426558580025657357328745839747693739591820283538307445
...
$ python2 solve.py  # 这题要求 python2，因为对于大的 d，hex(d) 最后有 'L'
$ python3 solve.py  # 但是 python3 也可以手动加上 'L'，并加上 .encode()
flag{47bf28da384590448e0b0d23909a25a4}
