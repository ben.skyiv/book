import hashlib
d = 8920758995414587152829426558580025657357328745839747693739591820283538307445
s = hex(d)
s += 'L' if s[-1] != 'L' else ''
print("flag{" + hashlib.md5(s.encode()).hexdigest() + "}")
