words = [s.strip() for s in open('words.txt').readlines()]
text = input()
flag = sorted((text.count(word), word) for word in words)
print(''.join(flag[i-1][1] for i in (4, 8, 11, 15, 16)))
