https://buuoj.cn/challenges#变异凯撒
注意：得到的 flag 请包上 flag{} 提交
[e072b2d9-7...]

$ unzip e072b2d9-73a5-49fe-b50c-54ac6dab310a.zip
$ cat 变异凯撒/变异凯撒.txt
加密密文：afZ_r9VYfScOeO_UL^RWUc
格式：flag{ }
$ head -1 变异凯撒/变异凯撒.txt | cut -c16- | tee /tmp/a
afZ_r9VYfScOeO_UL^RWUc
$ cat solve.py
print(''.join((chr(ord(c)+i+5)) for i, c in enumerate(input())))
$ python solve.py < /tmp/a
flag{Caesar_variation}
