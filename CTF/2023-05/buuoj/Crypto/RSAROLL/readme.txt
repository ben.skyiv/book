https://buuoj.cn/challenges#RSAROLL
注意：得到的 flag 请包上 flag{} 提交
[02c01a13-3a...]

$ 7z x 02c01a13-3a86-47de-8648-f03328a5e5d8.zip
$ iconv -f GB18030 -t utf8  RsaRoll/题目.txt; echo
RSA roll！roll！roll！
Only number and a-z
（don't use editor
which MS provide）
$ cat RsaRoll/data.txt
{920139713,19}

704796792
752211152
...
306220148
由此，n = 920139713, e = 19, c1 = 704796792, c2 = 752211152, ...
$ factor 920139713
920139713: 18443 49891
因此，p = 18443, q = 49891, d = invert(e, (p-1)*(q-1))
$ python rsa_roll.py  # 根据各个 c 解出各个 m，拼接成 flag
flag{13212je2ue28fy71w8u87y31r78eu1e2}
