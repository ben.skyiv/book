from gmpy2 import invert
p, q, e = 18443, 49891, 19
n, d = p*q, invert(e, (p-1)*(q-1))
with open('RsaRoll/data.txt') as fp: s = fp.read()
flag = ''.join(chr(pow(c, d, n)) for c in map(int, s.split()[1:]))
print(flag)
