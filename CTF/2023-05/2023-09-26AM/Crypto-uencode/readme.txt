小智在告警设备中关注到一条加密数据，请协助他进行解密分析。

$ unzip uencode.zip
Archive:  uencode.zip
  inflating: uencode.txt
$ cyberchef  # HTML
sqy1nsts{3s0srn2r36nosq053s406n945p8p}

$ echo -n sqy1nsts{3s0srn2r36nosq053s406n945p8p} | fence.exe
synt{ssnr6oq5s0n4ppq1ss30r23ns0346958}

$ cyberchef  # rot13:
flag{ffae6bd5f0a4ccd1ff30e23af0346958}
