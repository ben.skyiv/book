小智在凌晨收到单位通知，通知中说明单位数据库文件被不法分子暴露在暗网贩卖。目前已捕捉到相关行为流量，请分析该流量并找到flag。

$ unzip hack.zip
Archive:  hack.zip
  inflating: hack.pcapng

方法1：
$ strings hack.pcapng | grep -i flag
Content-Disposition: form-data; name="uploaded"; filename="flag.jpg"
flag.txtK
flag.txt
$ wireshark hack.pcapng &  #查找 flag.jpg，追踪 TCP 流，另存为 flag.jpg
$ bless flag.jpg &  # 删除头部多余数据
$ foremost flag.jpg
$ 7z x output/zip/00000982.zip

方法2：
$ tcpxtract -f hack.pcapng
$ 7z x 00000017.zip

$ cat flag.txt
flag{10ecc8e63060890f8b36198faacf61cc}
