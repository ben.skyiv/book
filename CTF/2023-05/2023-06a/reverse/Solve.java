public class Solve {
  public static void main(String[] args) {
    var mask = "zxcvb,nmas_dfgh{}j@kl*qwe(rtyu)io!p+-1029384756".toCharArray();
    int c[] = {12,20,8,13,15,21,28,38,29,10,12,31,6,11,10,37,27,21,16};
    for (var i : c) System.out.print(mask[i]);
    System.out.println();
  }
}
