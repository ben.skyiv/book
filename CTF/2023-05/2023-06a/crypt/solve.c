#include <stdio.h>

int main(void)
{
  char s[BUFSIZ];
  fgets(s, BUFSIZ, stdin);
  for (int i = 0; s[i]; i++) s[i] += i;
  printf("%s\n", s);
}
