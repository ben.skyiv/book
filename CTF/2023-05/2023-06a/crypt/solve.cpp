#include <iostream>

int main()
{
    std::string s;
    std::cin >> s;
    for (int i = 0; s[i]; i++) std::cout << char(s[i] + i);
    std::cout << std::endl;
}
