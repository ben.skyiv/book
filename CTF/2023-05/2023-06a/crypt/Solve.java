import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solve {
  public static void main(String[] args) throws IOException {
    var br = new BufferedReader(new InputStreamReader(System.in));
    var s = br.readLine();
    for (var i = 0; i < s.length(); i++)
      System.out.print((char)(s.charAt(i) + i));
    System.out.println();
  }
}
