sc-unagi
unagi
http://39.100.87.38:20166

打开上述URL，得到：
[Home*] [User] [Upload] [About]
Welcome to the challenge

[Home] [User*] [Upload] [About]
Name: Alice
Email: alice@fakesite.com
Group: CSAW2019
Intro: Alice is cool

Name: Bob
Email: bob@fakesite.com
Group: CSAW2019
Intro: Bob is cool too

[Home] [User] [Upload*] [About]
Upload new users to the system
You can check out the format example [here]
[浏览...]未选择文件            [Upload]

[Home] [User] [Upload] [About*]
Flag is located at /flag, come get it

$ cp sample.xml bob8.xml
$ kate bob8.xml
上传 bob8.xml 失败：
WAF blocked uploaded file. Please try again
$ iconv -f utf8 -t utf16 bob8.xml > bob16.xml
上传 bob16.xml 成功：
Successfully uploaded user profiles.
得到 flag：
Name: Bob
Email: bob@fakesite.com
Group: CSAW2019
Intro: flag{1ha4513eonreva02884f1s0nschs2jva}

https://zhuanlan.zhihu.com/p/164861768
