from sys import stdin
from PIL import Image
ss = [s.strip()[1:-1].split(',') for s in stdin.readlines()]
ss = set((int(x), int(y)) for x, y in ss[:-1])
size = 280
img = Image.new("RGBA", (size, size))
for x in range(size):
    for y in range(size):
        white, black = (255, 255, 255), (0, 0, 0)
        img.putpixel((x, y), black if (x, y) in ss else white)
img.save("/tmp/a.bmp")
