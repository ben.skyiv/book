from sys import stdin
from zlib import crc32


def seek(z):
    for a in s:
        for b in s:
            for c in s:
                for d in s:
                    t = f'{a}{b}{c}{d}'
                    if crc32(t.encode()) == z:
                        return t


s = 'abcdefghijklmnopqrstuvwxyz_0123456789{}'
m = [int(c.split(':')[1].strip(), 16) for c in stdin.readlines()]
print(''.join(seek(c) for c in m))
