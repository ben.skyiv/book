using System;
using System.Text;
using Skyiv.Utils;

static class ReverseCrc32
{
  static void Main(string[] args)
  {
    var bs = new byte[4];
    foreach (var arg in args) {
      bs.ReverseCrc32(Convert.ToUInt32(arg, 16), 0);
      Console.Write(new ASCIIEncoding().GetString(bs));
    }
    Console.WriteLine();
  }
}
