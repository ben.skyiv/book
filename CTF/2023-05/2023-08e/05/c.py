s1 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+'
s2 = 'hP&p0!5L^#3NXLs@*QR%L&UN!L)0%Q^'
print(''.join(chr(s1.index(c) + 48) for c in s2))
