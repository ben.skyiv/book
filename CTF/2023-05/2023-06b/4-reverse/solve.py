with open('a.txt', 'r') as f: flag = f.read().rstrip()
print(''.join(chr(int(s[-3:-1], 16)) for s in flag.split('\n')))
