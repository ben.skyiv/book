BugKu, MISC, 这是一张单纯的图片
分数: 10 金币: 1
一血奖励: 1金币
描　　述: key{}
------------------------------

解法1：
$ file file.jpg
file.jpg: JPEG image data, JFIF standard 1.01, resolution (DPI), density 96x96, segment length 16, baseline, precision 8, 136x152, components 3
$ binwalk file.jpg
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
$ bless file.jpg &
发现文件最后有一大串字符串，用 strings 命令抽取：
$ strings -100 file.jpg | tee /tmp/a.html
&#107;&#101;&#121;&#123;&#121;&#111;&#117;&#32;&#97;&#114;&#101;&#32;&#114;&#105;&#103;&#104;&#116;&#125;
$ firefox /tmp/a.hmtl &
显示：key{you are right}
或者使用 Python 解码：
$ python
>>> import html
>>> with open('/tmp/a.html', 'r') as f:
...   m = f.readline().strip()
...
>>> html.unescape(m)
'key{you are right}'

解法2：
$ cp file.jgp /tmp/a.html
在浏览器地址栏输入 file:///tmp/a.html
也可以在页面的最后看到：(¢Šÿkey{you are right}ÙÙ
