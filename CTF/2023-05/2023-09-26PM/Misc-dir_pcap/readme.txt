用户流量被劫持，并且使用了通用口令导致数据泄露，寻找在流量传输过程中压缩包内的flag。（10分）

$ unzip dir_pcap.zip
Archive:  dir_pcap.zip
  inflating: dir_pcap.pcapng
$ strings dir_pcap.pcapng | grep -e flag -e pass
...
-rw-r--r-- 1 ftp ftp  256 Sep 04 15:50 flag.zip
-rw-r--r-- 1 ftp ftp  140 Sep 04 15:44 zip_pass.txt
$ wireshark dir_pcap.pcapng &
文件，导出对象，FTP-DATA，导出 flag.zip 和 zip_pass.txt
$ zip2john flag.zip >/tmp/a
$ john --wordlist=zip_pass.txt /tmp/a
3.6*3.6          (flag.zip/flag.txt)
$ 7z x -p3.6*3.6 flag.zip
$ cat flag.txt; echo
flag{7a704019fae7d5fc0c925795c5527c5f}
