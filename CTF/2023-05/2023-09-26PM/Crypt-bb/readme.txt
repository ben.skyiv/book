请分析加密数据，找到攻击者窃取的数据。（8分）

$ unzip bb.zip
Archive:  bb.zip
  inflating: bb.txt
$ python solve.py < bb.txt
flag{64c9f1b7f2f1e4170a35012b1039b6b8}
也可以使用 cyberchef 解码（base32, hex, base32, base64）
