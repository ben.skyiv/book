from base64 import b32decode, b64decode
s = b32decode(input()).decode()
s = bytearray.fromhex(s)
s = b64decode(b32decode(s))
print(s.decode())
