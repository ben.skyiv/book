from libnum import n2s
from gmpy2 import invert
with open('task.py') as fp:
    for _ in range(12): fp.readline()
    c = int(fp.readline()[1:])
    n = int(fp.readline()[1:])  # n = p*q
    z = int(fp.readline()[1:])  # z = p+q
e = 65537
phi = n - z + 1  # phi = (p-1)(q-1)
d = invert(e, phi)
m = pow(c, d, n)
print(n2s(int(m)).decode())
