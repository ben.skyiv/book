办公文档安全在平时工作中，是很容易忽视的安全隐患。（10分）

$ unzip docz.zip
Archive:  docz.zip
  inflating: docz.docx
用 WPS 打开 doz.docx，点击 WPS 的“文件，选项”菜单项，
在弹出的“选项”对话框（“视图”选项卡）中勾选格式标记区域的“隐藏文字”复选框。
得到 flag 的后半段：fe7e96baabe2eb483}
$ mkdir a; cd a
$ 7z x ../docz.docx
$ kate word/document.xml
在该文件的尾部发现一大串二进制字符串，将它复制到 a.txt 中，
编写以下程序将它转换为字符串（140 bits，每个字符 7 bits）
$ python solve.py < a.txt
flag{21b2382d07f5e50
flag{21b2382d07f5e50fe7e96baabe2eb483}
