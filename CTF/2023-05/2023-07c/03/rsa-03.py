import gmpy2
import Crypto.Util.number
with open('新建文本文档.txt') as fp:
    s = fp.read().split()
p =  int(s[0].split('=')[1], 16)
q =  int(s[1].split('=')[1], 16)
e =  int(s[2].split('=')[1], 16)
c =  int(s[3].split('=')[1], 16)
n = p*q
phi = (p-1)*(q-1)
d = gmpy2.invert(e, phi)
m = pow(c, d, n)
print(Crypto.Util.number.long_to_bytes(m))
