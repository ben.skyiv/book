from gmpy2 import invert
from libnum import n2s
s = input().split()
p =  int(s[0].split('=')[1], 16)
q =  int(s[1].split('=')[1], 16)
e =  int(s[2].split('=')[1], 16)
c =  int(s[3].split('=')[1], 16)
n = p*q
phi = (p-1)*(q-1)
d = invert(e, phi)
m = pow(c, d, n)
print(n2s(int(m)).decode())
