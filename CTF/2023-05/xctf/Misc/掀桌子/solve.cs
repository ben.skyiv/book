using System;
var s = Console.ReadLine();
var bs = new byte[s.Length / 2];
for (int i = 0; i < bs.Length; i++)
    bs[i] = (byte)(Convert.ToByte(s.Substring(i * 2, 2), 16) & 0x7F);
Console.WriteLine(System.Text.Encoding.UTF8.GetString(bs));
