#include <stdio.h>

int main(void)
{
  char s[BUFSIZ], flag[BUFSIZ / 2]; scanf("%s", s);
  for (int t, i = 0; s[i]; i += 2)
    sscanf(s + i, "%02x", &t), flag[i / 2] = t & 0x7F;
  printf("%s\n", flag);
}
