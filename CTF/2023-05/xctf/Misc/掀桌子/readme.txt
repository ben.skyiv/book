[GFSJ5018] xctf, Misc, 难度:4, 掀桌子
题目来源: DDCTF
题目描述: 菜狗截获了一份报文如下c8e9aca0c6f2e5f3e8c4efe7a1a0d4e8e5a0e6ece1e7a0e9f3baa0e8eafae3f9e4eafae2eae4e3eaebfaebe3f5e7e9f3e4e3e8eaf9eaf3e2e4e6f2，生气地掀翻了桌子(╯°□°）╯︵ ┻━┻

我们将题目描述中的报文存放在 a.txt 中。
报文是以16进制数表示字节串，去除高位后就得到 flag：
$ python solve.py < a.txt                     # Python 语言
$ gcc solve.c && ./a.out < a.txt                  # C  语言
$ csc -nologo solve.cs && mono solve.exe < a.txt  # C# 语言
Hi, FreshDog! The flag is: hjzcydjzbjdcjkzkcugisdchjyjsbdfr
