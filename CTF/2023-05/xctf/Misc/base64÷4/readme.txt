[GFSJ0731] xctf, Misc, 难度:2, base64÷4
题目来源:
题目描述: 暂无
题目附件: [下载附件]

附件是一个文本文件，内容为16进制数：
666C61677B45333342374644384133423834314341393639394544444241323442363041417D
$ python solve1.py   # libnum.n2s(int(m, 16))
$ python solve2.py   # base64.b16decode(m)
flag{E33B7FD8A3B841CA9699EDDBA24B60AA}
