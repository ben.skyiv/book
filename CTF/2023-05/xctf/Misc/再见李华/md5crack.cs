static class Md5crack
{
  static void Main(string[] args)
  {
    using (var h = System.Security.Cryptography.MD5.Create())
    for (; ; ) {
      var a = System.Console.ReadLine();
      if (a == null) break;
      if (System.BitConverter.ToString(h.ComputeHash
        (System.Text.Encoding.UTF8.GetBytes(a))).Replace
        ("-", "").ToLower().StartsWith(args[0]))
      { System.Console.WriteLine(a); return; }
    }
  }
}
