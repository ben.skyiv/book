using System;
using System.Text;

static class Mask
{
  static readonly int max = 4;
  static readonly string mask = "0123456789" +
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  static void GetWords(StringBuilder t)
  {
    var s = new StringBuilder(t.ToString());
    if (s.Length  > max) return;
    if (s.Length == max) Console.WriteLine(s + "LiHua");
    s.Append('!');
    foreach (var c in mask) { s[s.Length - 1] = c; GetWords(s); }
  }

  static void Main()
  {
    try { GetWords(new StringBuilder()); }
    catch (System.IO.IOException) { }
  }
}
