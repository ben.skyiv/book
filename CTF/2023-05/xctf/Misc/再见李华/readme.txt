[GFSJ0340] xctf, Misc, 难度:2, 再见李华
题目来源: ISCC
题目描述: 假如你是李华（LiHua)，收到乔帮主一封密信，没有任何特殊字符，请输入密码，不少于1000个字。同学，记得署名哦～
题目附件: [下载附件]

$ 7z x 7ab3e456b35945a4afed08050cd8859e.zip
$ binwalk mail2LiHua.jpg
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
30            0x1E            TIFF image data, little-endian offset of first image directory: 8
19273         0x4B49          Zip archive data, encrypted at least v2.0 to extract, compressed size: 37, uncompressed size: 26, name: key.txt
19436         0x4BEC          End of Zip archive, footer length: 22
$ foremost mail2LiHua.jpg
得到 output/zip/00000037.zip，解压需要密码。
根据题目描述，密码应该是4位字符（不包括特殊字符）后跟LiHua

方法1：
根据 mail2LiHua.jpg，密码的 md5sum 应该是以 1a4fb3fb5ee12307 开头的
用 C# 程序暴力破解：
$ mcs md5crack.cs
$ mcs mask.cs && ./mask.exe | ./md5crack.exe 1a4fb3fb5ee12307  # 或者
$ john --stdout --mask=?1?1?1?1LiHua | xargs sh md5crack.sh  # 非常慢
$ john --stdout --mask=?1?1?1?1LiHua | ./md5crack.exe 1a4fb3fb5ee12307
15CCLiHua
$ echo -n 15CCLiHua | md5sum
1a4fb3fb5ee1230710f97e8fa2716916  -

方法2：
用 john 暴力破解，1秒钟破解成功
$ zip2john output/zip/00000037.zip >/tmp/a
ver 2.0 00000037.zip/key.txt PKZIP Encr: cmplen=37, decmplen=26, crc=1B710205
$ john --mask=?1?1?1?1LiHua /tmp/a  # 或者
$ john --inc --max-length=9 --mask=?wLiHua /tmp/a
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 8 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
15CCLiHua        (00000037.zip/key.txt)

继续：
$ 7z x -p15CCLiHua output/zip/00000037.zip
$ cat key.txt; echo
Stay hungry, Stay foolish.
注意：不要加上 flag{...}，直接提交。
