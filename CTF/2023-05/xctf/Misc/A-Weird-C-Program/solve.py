flag = []
try:
    while True:
        s = ''
        for c in input():
            if c == ' ' or c == '\t':
                s += '0' if c == ' ' else '1'
        if len(s) >= 8: flag.append(s)
except EOFError: pass
print(''.join(map(lambda x: chr(int(x, 2)), flag)), end='')
