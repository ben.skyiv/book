[GFSJ1208] xctf, Misc, 难度:5, ext3
题目来源: bugku
题目描述: 今天是菜狗的生日，他收到了一个linux系统光盘
题目附件: [下载附件]

$  file f1fc23f5c743425d9e0073887c846d23
Linux rev 1.0 ext3 filesystem data, UUID=...
$ mkdir mnt

方法1：
$ cd mnt
$ 7z x ../f1fc23f5c743425d9e0073887c846d23

方法2：
$ sudo mount -o loop f1fc23f5c743425d9e0073887c846d23 mnt
$ cd mnt

继续：
$ find -name 'flag*'
./O7avZhikgKgbF/flag.txt
$ find -name 'flag*' | xargs cat
ZmxhZ3tzYWpiY2lienNrampjbmJoc2J2Y2pianN6Y3N6Ymt6an0=
$ find -name 'flag*' | xargs cat | base64 -d; echo
flag{sajbcibzskjjcnbhsbvcjbjszcszbkzj}
$ cd ..

清理：rm -rf mnt 或者 sudo umount mnt && rmdir mnt
