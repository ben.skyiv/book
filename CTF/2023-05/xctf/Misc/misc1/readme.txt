[GFSJ0018] xctf, Misc, 难度:2, misc1
题目来源: DDCTF
题目描述: d4e8e1f4a0f7e1f3a0e6e1f3f4a1a0d4e8e5a0e6ece1e7a0e9f3baa0c4c4c3d4c6fbb9e1e6b3e3b9e4b3b7b7e2b6b1e4b2b6b9e2b1b1b3b3b7e6b3b3b0e3b9b3b5e6fd

我们将题目描述的内容 a.txt 中，它是以16进制数表示字节串，
去除高位后就得到 flag（见 Misc/掀桌子）：
$ python solve.py < a.txt
Hi, FreshDog! The flag is: hjzcydjzbjdcjkzkcugisdchjyjsbdfr
That was fast! The flag is: DDCTF{9af3c9d377b61d269b11337f330c935f}
