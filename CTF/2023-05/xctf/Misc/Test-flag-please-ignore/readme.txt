[GFSJ0171] xctf, Misc, 难度:1, Test-flag-please-ignore
题目来源: CTF
题目描述: 暂无
题目附件: [下载附件]

$ 7z x 2f572adcaa5447feb8cc8c50969cd57d.zip
$ cat misc10; echo
666c61677b68656c6c6f5f776f726c647d
$ python solve.py
flag{hello_world}
注：这题与 Misc/base64÷4 相同
