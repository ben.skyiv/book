import libnum

with open('misc10', 'r') as f:
    m = f.readline().strip()
print(libnum.n2s(int(m, 16)).decode())
