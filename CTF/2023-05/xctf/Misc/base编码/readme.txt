[GFSJ1049] xctf, Misc, 难度:5, base编码
题目来源: 贵州贵阳
题目描述: b’GE3DKOJZGI3DINRRFY2TGMBTGE3TG===’
ran_number: 4
b_type: 32 0
b_type: 32 1
b_type: 16 2
b_type: 16 3
小云是一个喜欢捣蛋的家伙，他把自己最爱的flag加密保存后混淆在一堆无用数据中找不回来了，你能帮帮他吗？
题目附件: [下载附件]

根据题目描述，解压出 GE3DKOJZGI3DINRRFY2TGMBTGE3TG===
$ unzip 00f3062*.zip GE3DKOJZGI3DINRRFY2TGMBTGE3TG===
$ python solve.py  < GE3DKOJZGI3DINRRFY2TGMBTGE3TG===
This is your flag: flag{ff02af1f28f5c275f2449ffe37ab6b86}
