[GFSJ0147] xctf, Misc, 难度:6, 神奇的压缩文件
题目来源: CTF
题目描述: 为什么文件大小和说好的不一样呢？无限的命运石之门啊！里面并没有Flag。
题目附件: [下载附件]

https://blog.csdn.net/weixin_44604541/article/details/112427198
$ unrar x 54cb0e20b4964956a72068ecfec20120.rar
解压后得到 Misc150.txt，里面并没有Flag。
$ cat Misc150.txt; echo
Flag.zip behind me.

用 unrar lta,b 查看，发现 Flag.zip：
$ unrar lta,b 54cb0e20b4964956a72068ecfec20120.rar
...
        Name: STM
        Type: NTFS alternate data stream
      Target: :Flag.zip
        Size: 436633
 Packed size: 437359
       Ratio: 100%
       mtime: 2016-10-01 01:26:26,000000000
  Attributes: .B
       CRC32: ECD1786E
     Host OS: Windows
 Compression: RAR 1.5(v29) -m3 -md=64K

TODO 最终得到：
lctf{6d3677dd}
