[GFSJ1203] xctf, Misc, 难度:1, CatCatCat
题目来源: CATCTF
题目描述: (得到的flag内容用CatCTF{}包上提交)
题目附件: [下载附件]

https://blog.csdn.net/qq_53229503/article/details/128813882
$ unzip -O GBK 1acc6eb9-446e-49d8-be4e-e02fa9753a43.zip
$ cd cat; file *
猫猫.jpg: JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment length 16, baseline, precision 8, 264x300, components 3
我养了一只叫兔子的91岁的猫猫.txt: openssl enc'd data with salted password, base64 encoded

txt 文件开头是 U2FsdGVkX1+，base64 解码后是 Salted__
根据“我养了一只叫兔子的91岁的猫猫.txt”中的“兔子”，
猜测它是用 Rabbit Stream Cipher Algorithm 加密的。
由以下命令可知密钥是 catflag
$ strings -20 *.jpg
"!&+7/&)4)!"0A149;>>>%.DIC<H7=>;
;("(;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
passwordis..catflag..]

使用以下网站在线解密
https://www.woodmanzhang.com/webkit/rabbitencrypt
]DW35W/HqUYt3P_ow"3P_ow"3P_ow"3P_ow"3P...

根据“我养了一只叫兔子的91岁的猫猫.txt”和以上字符串，
猜测它是用 base91 编码的，使用以下 Python 程序解码
$ python ../base91decode.py < a01 > a02
得到：cat. cat. cat. cat. cat...cat! cat. cat? cat.
猜测它是 Ook 语言源程序

方法1：
$ sed 's/cat/Ook/g' a02 > a03.ook  # 将 cat 替换为 Ook
$ ook.sh a03.ook        # 使用本机 Ook 解释器得到 flag

方法2：
使用以下网站在线解密得到 flag
https://www.splitbrain.org/services/ook

继续：
CATCTF{Th1s_V3ry_cute_catcat!!!}
注意：提交时要把 CATCTF 改为 CatCTF
