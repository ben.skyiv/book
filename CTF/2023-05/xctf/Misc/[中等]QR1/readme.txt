[GFSJ1088] xctf, Misc, 难度:3, [中等]QR1
题目来源: XCTF联合招新赛
题目描述: 一张空白的图片？
题目附件: [下载附件]

$ unzip 6e732dad-c6c5-4daa-8432-cd18b660da62.zip
$ file enc.png
enc.png: PNG image data, 5330 x 5330, 1-bit grayscale, non-interlaced
$ kolourpaint enc.png &  # Ctrl-E，缩放为原来的 1/10，Ctrl-S 保存
$ file enc.png
enc.png: PNG image data, 533 x 533, 1-bit colormap, non-interlaced
$ stegsolve &  # 在 Red plane 1 发现一幅二维码，File -> Sava As
$ file solved.bmp
solved.bmp: PC bitmap, Windows 3.x format, 533 x 533 x 24, ...
$ zbarimg solved.bmp
scanned 0 barcode symbols from 1 images in 0.01 seconds
启动 VirtualBox，在 Windows 10 中运行 QR Reseach，得到：
flag{AHA_U_Kn0w_QR_c0d3_312468ed-ea59-4fe9-9cd2-b5e9d8b39498}
