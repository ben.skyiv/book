#!/usr/bin/env python3
from sys import argv
from pathlib import Path
from PIL import Image


def get_gif_frame(file_name):
    im = Image.open(file_name)
    for i in range(im.n_frames):
        im.seek(i)
        im.save(figs / f'{i:03}.png')
    return im


def image_compose(frames, width, height):
    image = Image.open(figs / '000.png').convert('RGBA')
    for i in range(1, frames):
        img = Image.open(figs / f'{i:03}.png').convert('RGBA')
        image.paste(img, (0, 0), mask=img.getchannel('A'))
    image.save('out.png')


figs = Path('figs')
figs.mkdir(exist_ok=True)
im = get_gif_frame(argv[1])
image_compose(im.n_frames, im.size[0], im.size[1])
