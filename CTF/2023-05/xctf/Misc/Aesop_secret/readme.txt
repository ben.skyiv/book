[GFSJ0147] xctf, Misc, 难度:1, Aesop_secret
题目来源: ISCC
题目附件: [下载附件]

$ unzip f732347c6bad47f1ac715cf67a3f4532.zip
$ file Aesop.gif 
Aesop.gif: GIF image data, version 89a, 124 x 70
$ ./gif2over.py Aesop.gif
将 Aesoo.gif 分解为 000.png--008.png，然后叠加在一起，得到：ISCC
$ strings Aesop.gif | tail -1 | cut -c2- | base64 -d > /tmp/a
$ file /tmp/a
/tmp/a: openssl enc'd data with salted password
这是 AES 加密的数据（http://www.jsons.cn/aesencrypt/）
$ strings Aesop.gif | tail -1 | cut -c2-
U2FsdGVkX19QwGkcgD0fTjZxgijRzQOGbCWALh4sRDec2w6xsY/ux53Vuj/AMZBDJ87qyZL5kAf1fmAH4Oe13Iu435bfRBuZgHpnRjTBn5+xsDHONiR3t0+Oa8yG/tOKJMNUauedvMyN4v4QKiFunw==
在上述网站二次解密（密码：ISCC）上述 base64 数据得到：
flag{DugUpADiamondADeepDarkMine}
