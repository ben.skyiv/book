#!/usr/bin/env python3
from sys import argv
from pathlib import Path
from PIL import Image


def get_gif_frame(file_name):
    im = Image.open(file_name)
    for i in range(im.n_frames):
        im.seek(i)
        im.save(figs / f'{i:03}.png')
    return im


def image_compose(frames, width, height):
    image = Image.new('RGB', (width*frames, height))
    for i in range(frames):
        image.paste(Image.open(figs / f'{i:03}.png'), (i*width, 0))
    image.save('out.png')


figs = Path('figs')
figs.mkdir(exist_ok=True)
im = get_gif_frame(argv[1])
image_compose(im.n_frames, im.size[0], im.size[1])
