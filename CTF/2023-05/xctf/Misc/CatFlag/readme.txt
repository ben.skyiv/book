[GFSJ1208] xctf, Misc, 难度:1, CatFlag
题目来源: CATCTF
题目描述: Just “cat flag” or “type flag”
题目附件: [下载附件]

$ 7z x e56415b7-a6e9-459a-bc0d-db59ebfe41c2.zip
解压后得到一个 88.8 MiB 的文件
$ file flag
flag: ASCII text, with very long lines (65535), with escape sequences
按照题目描述，使用 cat flag 直接得到 flag：
$ cat flag

 CatCTF{!2023_Will_Be_Special,2022_Was_Not!}
