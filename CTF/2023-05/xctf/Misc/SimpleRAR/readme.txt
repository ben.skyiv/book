[GFSJ0515] xctf, Misc, 难度:5, SimpleRAR
题目来源: CTF
题目描述: 菜狗最近学会了拼图，这是他刚拼好的，可是却搞错了一块(ps:双图层)
题目附件: [下载附件]

$ cp 18c5326aada0499eafbe03ad8a52e40c.rar a.rar
$ file a.rar          # 下载得到的附件是 RAR 文件
a.rar: RAR archive data, v4, os: Win32
$ unrar t a.rar # 测试 RAR 文件，发现第2个 file header 损坏
Testing     flag.txt                 OK
secret.png - the file header is corrupt
根据 CTF/doc/rar.pdf，用 bless 修改 RAR 文件：
$ bless a.rar &       # 把 0x53 处的 7A 改为 74
$ unrar x a.rar       # 解压得到 secret.png 和 flag.txt
$ cat flag.txt; echo  # 假的 flag :(
flag is not here      # flag 不在这里
$ file secret.png     # flag 在这里
secret.png: GIF image data, version 89a, 280 x 280
$ mv secret.png a.gif # 实际上是 gif 文件
$ gifspliter.py a.gif # 分解得到 2 个 png 文件
$ file z0?.png        # 题目描述中已经提示：双图层
z00.png: PNG image data, 280 x 280, 2-bit colormap, non-interlaced
z01.png: PNG image data, 280 x 280, 8-bit/color RGBA, non-interlaced
用 stegsolve 打开 z00.png，一直按“>”，
在 Red plane 0 发现二维码的下半部，File, Save As, 2.bmp
用同样的方法从 z01.png 中得到二维码的上半部：     1.bmp
$ kolourpaint 1.bmp 2.bmp &
将 1.bmp 的上半部复制到 2.bmp 的上半部，
并且把图像左下角的定位块复制到左上角和右上角。
$ zbarimg 2.bmp       # 识别二维码得到 flag
QR-Code:flag{yanji4n_bu_we1shi}
