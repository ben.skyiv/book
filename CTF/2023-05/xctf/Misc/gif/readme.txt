[GFSJ0517] xctf, Misc, 难度:4, gif
题目描述: 菜狗截获了一张菜鸡发给菜猫的动态图，却发现另有玄机
题目附件: [下载附件]

$ 7z x dbbc971bf4da461fb8939ed8fc9c4c9d.zip
解压后得到 gif 目录下的 104 个 jpg 文件，每个文件都是全白或全黑的方块。
这 104 个文件可分为 13 组，每组 8 个，8 bits = 1 byte
使用以下程序将黑方块转换为1、白方块转换为0（也可目视手动转换）：
$ python jpg2bin0.py  # 使用 PIL 库，获取占主导地位的颜色，或者
$ python jpg2bin1.py  # 使用 PIL 库，获取左上角的颜色，或者
$ python jpg2bin2.py  # 直接判断图像文件中倒数第3个字节的差异
01100110 01101100 01100001 01100111 01111011 01000110 ... 01111101
按照 ASCII 编码解读这个字节序列，得到 flag：
$ python jpg2bin1.py | python solve.py
flag{FuN_giF}
