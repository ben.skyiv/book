from PIL import Image

flag = ''
for i in range(104):
    image_file = 'gif/' + str(i) + '.jpg'
    img = Image.open(image_file).convert("RGB")
    r, _, _ = img.load()[0, 0]
    flag += '0' if r == 255 else '1'
    flag += ' ' if (i+1) % 8 == 0 else ''
print(flag)
