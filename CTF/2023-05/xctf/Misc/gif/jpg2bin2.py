flag = ''
for i in range(104):
    image_file = 'gif/' + str(i) + '.jpg'
    with open(image_file, 'rb') as f: a = f.read()
    flag += '1' if a[-3] == 1 else '0'
    flag += ' ' if (i+1) % 8 == 0 else ''
print(flag)
