[GFSJ0513] xctf, Misc, 难度:3, pdf
题目来源：csaw
题目描述: 菜猫给了菜狗一张图，说图下面什么都没有
题目附件: [下载附件]

方法1：
备注：pdftotext 包含在 poppler 软件包中。
$ pdftotext ad00be3652ac4301a71dedd2708f78b8.pdf
$ less ad00be3652ac4301a71dedd2708f78b8.txt
flag{security_through_obscurity}

方法2：
$ okular ad00be3652ac4301a71dedd2708f78b8.pdf &
点击右上角的“选择文本”按钮，按 Ctrl-A，Ctrl-C，
在文本编辑器里按 Ctrl-V，得到（要删除很多空行）：
flag{security_through_obscurity}
