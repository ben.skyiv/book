[GFSJ0278] xctf, Misc, 难度:4, picture2
题目来源：CISCN
题目附件: [下载附件]

$ binwalk e4103617b4a6476fb7aa8f862f2ee400.png
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
38884         0x97E4          Zlib compressed data, default compression
$ binwalk -e e4103617b4a6476fb7aa8f862f2ee400.png
注意：foremost 无法分离这个文件
$ base64 -d _e4103617b4a6476fb7aa8f862f2ee400.png.extracted/97E4 > /tmp/a
$ seektail.py /tmp/a
0000007C: zip     (50 4B 01 02  Mid) (50 4B 03 04: None)
000000D2: zip     (50 4B 05 06 Tail) (50 4B 03 04: None)
$ bless /tmp/a &  # 修改开头 4B 50 为 50 4B
$ file /tmp/a
/tmp/a: Zip archive data, at least v2.0 to extract, compression method=store
$ 7z l /tmp/a
...
Comment = [Python 2.7]
>>> ¨}¨}¨}

Traceback (most recent call last):
  File "<pyshell#0>", line 1, in <module>
    ¨}¨}¨}
ZeroDivisionError: ¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨}¨} <- password ;)
>>>

   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
2018-04-23 06:01:50 ....A           78           90  code
------------------- ----- ------------ ------------  ------------------------
2018-04-23 06:01:50                 78           90  1 files

根据提示，在 Kali Linux 中运行：
$ python2
>>> 1/0
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: integer division or modulo by zero
$ unzip -P'integer division or modulo by zero' /tmp/a
$ file code
code: uuencoded or xxencoded text, file name "key.txt", ASCII text
$ cat code
begin 644 key.txt
G0TE30TY[,C,X.$%&,C@Y,T5".#5%0C%"-#,Y04)&1C8Q-S,Q.49]
`
end
$ uudecode code && cat key.txt; echo
CISCN{2388AF2893EB85EB1B439ABFF617319F}
