[GFSJ0516] xctf, Misc, 难度:4, 坚持60s
题目来源: CTF
题目描述: 菜狗发现最近菜猫不爱理他，反而迷上了菜鸡
题目附件: [下载附件]

附件是一个 .jar 文件，运行结果是一个 Java 小游戏，对解题没有什么帮助
$ java -jar 9dc125bf1b84478cb14813d9bed6470c.jar  # 运行之，是个小游戏
$ jadx 9dc125bf1b84478cb14813d9bed6470c.jar     # 反编译成 Java 源文件
$ find -name *.java | xargs grep -i flag   # 在 Java 源文件中查找 flag
./9dc125bf1b84478cb14813d9bed6470c/sources/cn/bjsxt/plane/PlaneGameFrame.java:                    printInfo(g, "flag{RGFqaURhbGlfSmlud2FuQ2hpamk=}", 50, 150, 300);
$ echo -n RGFqaURhbGlfSmlud2FuQ2hpamk= | base64 -d; echo
DajiDali_JinwanChiji
提交 flag{DajiDali_JinwanChiji}，成功！
