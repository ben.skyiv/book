[GFSJ1050] xctf, Misc, 难度:3, Misc文件类型
题目来源: 贵州贵阳
题目描述: 最重要的还是要知道这是什么文件
题目附件: [下载附件]

$ unzip 3ea702ca-85fd-49da-9997-4e513310851c.zip
$ hexstr2bytes.exe < cipher.txt; cat /tmp/a; echo
/tmp/a: 261 bytes
46ESAB_UEsDBBQAAAAIAAldCFXqOw7cKAAAACYAAAAIAAAAZmxhZy50eHRLy0l...AAA
$ vim /tmp/a  # 删除开头的 46ESAB_ 并在末尾加上 ==
$ base64 -d /tmp/a > /tmp/b  # 吐槽：为什么不要 $ rev /tmp/a ?
$ file /tmp/b
/tmp/b: Zip archive data, at least v2.0 to extract, compression method=deflate
$ unzip /tmp/b
cat flag.txt; echo
flag{0bec0ad3da2113c70e50fd5617b8e7f9}
