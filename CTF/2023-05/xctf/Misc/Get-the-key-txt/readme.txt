[GFSJ0360] xctf, Misc, 难度:2, Get-the-key.txt
题目来源: SECON
题目描述: 暂无
题目附件: [下载附件]

$ 7z x 256cb07f5dbd493f81ad5b199f2b248a.zip
$ file forensic100
forensic100: Linux rev 1.0 ext2 filesystem data, ...
发现 forensic100 是 Linux filesystem data，
可以用 mount 命令挂载，也可以直接用 7z 命令解压：
$ mkdir a && cd $_ && 7z x ../forensic100 && ls
发现很多文件，根据题目名称“Get-the-key.txt”，寻找 key.txt
$ grep key.txt *             # 方法1
grep: 1: 匹配到二进制文件
grep: lost+found: 是一个目录
$ file * | grep key.txt      # 方法2
1: gzip compressed data, was "key.txt", ...
找到名称为“1”的文件，它包含 key.txt 的压缩数据，解压之：
$ gunzip < 1     # 或者
$ 7z x 1 && cat key.txt
SECCON{@]NL7n+-s75FrET]vU=7Z}
