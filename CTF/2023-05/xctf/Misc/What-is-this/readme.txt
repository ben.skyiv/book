[GFSJ0320] xctf, Misc, 难度:2, What-is-this
题目来源: CTF
题目描述: 找到FLAG
题目附件: [下载附件]

$ 7z x e66ea8344f034964ba0b3cb9879996ff.gz
$ file e66ea8344f034964ba0b3cb9879996ff
e66ea8344f034964ba0b3cb9879996ff: POSIX tar archive (GNU)
$ tar xf e66ea8344f034964ba0b3cb9879996ff
$ file *.jpg
pic1.jpg: JPEG image data, ..., 300x300, components 1
pic2.jpg: JPEG image data, ..., 300x300, components 1

方法1：
$ compare pic1.jpg pic2.jpg a.png
打开 a.png 得到：AZADI TOWER，直接提交，正确！

方法2：
$ stegsolve &
File    -> Open          : pic1.jpg
Analyse -> Image Combiner: pic2.jpg
第1幅 XOR 图像就得到 flag
