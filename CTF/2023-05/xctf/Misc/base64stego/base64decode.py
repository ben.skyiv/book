#!/usr/bin/env python3
from sys import stdin
from base64 import b64decode
ss = stdin.readlines()
print(''.join(b64decode(s).decode(encoding='iso 8859-1') for s in ss))
