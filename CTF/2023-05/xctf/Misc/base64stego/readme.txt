[GFSJ0520] xctf, Misc, 难度:5, base64stego
题目来源：CTF
题目描述: 菜狗经过几天的学习，终于发现了如来十三掌最后一步的精髓
题目附件: [下载附件]

$ zipclean.exe a2eb7ceaf5ab49f7acb33de2e7eed74a.zip
00000000: 50-4B-03-04 14-03 00-00
00000E10: 50-4B-01-02 3F-03 14-03 09-00
/tmp/a.zip: 3,713 bytes       # 清除伪加密标志得到 /tmp/a.zip
$ unzip /tmp/a.zip            # 解压得到 stego.txt
$ base64stego.py < stego.txt  # 利用 base64 串尾部冗余信息
Base_sixty_four_point_five

base64 编码是把每 6-bit（共2**6 == 64种）编码成 8-bits 的可打印字符，
宏观上等价于把每 3 个字节（3*8 == 24）编码成 4 个可打印字符（4*6 == 24）。
如果编码后的字符串长度不是 4 的倍数，则用 = 字符填充。
如果编码前的内容的长度（以 bit 为单位）不是6的倍数，则用 0 填充。
base64 隐写就是利用这填充 bit，不用 0 填充，代之以要隐写的 bits。
base64 尾部可以有 0--3 个 =，每个 = 可以隐写 2-bit。
