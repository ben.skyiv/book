b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
with open('stego.txt') as fp: ss, flag = fp.readlines(), ''
for s in ss:
    if s.count('='):
        a = b64.index(s.rstrip().rstrip('=')[-1])
        flag += f'{a:08b}'[s.count('=') * -2:]
print(''.join(chr(int(flag[i:i+8], 2)) for i in range(0, len(flag), 8)))
