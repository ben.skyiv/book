from base64 import b64encode, b64decode
b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
with open('stego.txt') as fp: ss, flag = fp.readlines(), ''
for s in ss:
    if not s.count('='): continue
    t = b64encode(b64decode(s)).decode()
    a = b64.index(s.rstrip().rstrip('=')[-1])
    b = b64.index(t.rstrip().rstrip('=')[-1])
    flag += bin(a ^ b)[2:].zfill(s.count('=') * 2)
print(''.join(chr(int(flag[i:i+8], 2)) for i in range(0, len(flag), 8)))
