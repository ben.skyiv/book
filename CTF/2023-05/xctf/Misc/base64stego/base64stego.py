#!/usr/bin/env python3
from sys import stdin
b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
ss = stdin.readlines()
flag = ''.join(f"{b64.index(s.rstrip().rstrip('=')[-1]):08b}"
               [s.count('=') * -2:] for s in ss if s.count('='))
print(''.join(chr(int(flag[i:i+8], 2)) for i in range(0, len(flag), 8)))
