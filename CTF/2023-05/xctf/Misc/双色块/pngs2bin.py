from PIL import Image

flag, byte, size = '', '', 24
for i in range(0, size):
    for j in range(0, size):
        img = Image.open(f"a/z{i * size + j:03}.png").convert("RGBA")
        _, g, _, _ = img.load()[j * 10 + 5, i * 10 + 5]
        byte += '0' if g == 255 else '1'
        if len(byte) == 8:
            flag += chr(int(byte, 2))
            byte = ""
print(flag)
