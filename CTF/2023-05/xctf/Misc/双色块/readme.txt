[GFSJ0732] xctf, Misc, 难度:4, 双色块
题目来源: 网鼎杯
题目描述: 分离
题目附件: [下载附件]

https://bbs.huaweicloud.com/blogs/329981
$ mkdir a && cd $_ && 7z x ../456*59.zip
$ gifspliter.py out.gif && cd ..
Total: 576  # 得到 24x24 = 576 个 png 文件
$ file a/out.gif a/z000.png a/z575.png
a/out.gif:  GIF image data, version 89a, 240 x 240
a/z000.png: PNG image data, 240 x 240, 8-bit colormap, non-interlaced
a/z575.png: PNG image data, 240 x 240, 8-bit/color RGB, non-interlaced
每个 png 文件都包含一个 10x10 的色块，颜色为绿色或紫色
$ python pngs2bin.py | tee /tmp/a          # Python 程序，或者
$ csc pngs2bin.cs && mono pngs2bin.exe | tee /tmp/a  # C# 程序
o8DlxK+H8wsiXe/ERFpAMaBPiIcj1sHyGOMmQDkK+uXsVZgre5DSXw==hhhhhhhhhhhhhhhh
$ base64 -d /tmp/a | hexdump -C
00000000  a3 c0 e5 c4 af 87 f3 0b  22 5d ef c4 44 5a 40 31  |........"]..DZ@1|
00000010  a0 4f 88 87 23 d6 c1 f2  18 e3 26 40 39 0a fa e5  |.O..#.....&@9...|
00000020  ec 55 98 2b 7b 90 d2 5f  86 18 61 86 18 61 86 18  |.U.+{.._..a..a..|
00000030  61 86 18 61                                       |a..a|
00000034
是 DES 加密的数据，用 foremost 提取密钥，再用 Cyberchef 解密
$ foremost a/out.gif
$ xdg-open output/png/00001436.png
key:ctfer2333
$ cbyerchef.sh  # 见 cbyerchef.png
flag{2ce3b416457d4380dc9a6149858f71db}
