using System;
using System.Drawing;
for (int k = 1, b = 0, size = 24, i = 0; i < size; i++)
    for (var j = 0; j < size; j++, k++) {
        var img = new Bitmap($"a/z{i * size + j:D3}.png");
        var color = img.GetPixel(j * 10, i * 10);
        b = (b << 1) + ((color.G != 0) ? 0 : 1); // A,R,G,B
        if (k % 8 == 0) { Console.Write((char)b); b = 0; }
    }
Console.WriteLine();
