[GFSJ0006] xctf, Misc, 难度:1, Banmabanma
题目来源: 世安杯
题目描述: flag格式为flag{xxxx}
题目附件: [下载附件]

$ 7z x f018351ca6c64ceb8a98c9da9f29c9cd.rar
得到 斑马斑马.png，直接在网站
https://online-barcode-reader.inliteresearch.com
在线识别图片中斑马身上的条形码，得到
FLAG IS TENSHINE
