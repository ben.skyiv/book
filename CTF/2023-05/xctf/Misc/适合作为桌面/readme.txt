[GFSJ0010] xctf, Misc, 难度:1, 适合作为桌面
题目来源: 世安杯
题目附件: [下载附件]

$ unrar x 1573d940d9bb47a083da6db70ffbffe0.rar 
用 stegsolve 打开 1.png，用一直按 -> 键，在 Read plane 1 发现一幅二维码，保存之。
$ zbarimg solved.bmp > a.txt  # 然后删除开头的“QR-Code:”
$ hexstr2bytes.exe < a.txt
/tmp/a: 417 bytes
$ file /tmp/a
/tmp/a: python 2.7 byte-compiled
$ pycdc /tmp/a > /tmp/a.py
$ echo 'flag()' >> /tmp/a.py
$ python2 /tmp/a.py
flag{38a57032085441e7}
