[GFSJ0390] xctf, Misc, 难度:3, 签到题
题目来源: CTF
题目描述: SSCTF线上选举美男大赛开始了，泰迪拿着他的密码去解密了，提交花括号内内容（Z2dRQGdRMWZxaDBvaHRqcHRfc3d7Z2ZoZ3MjfQ==）

首先进行 base64 解码：
$ echo -n Z2dRQGdRMWZxaDBvaHRqcHRfc3d7Z2ZoZ3MjfQ== | base64 -d | tee /tmp/a; echo
ggQ@gQ1fqh0ohtjpt_sw{gfhgs#}

注意到“}”位于最后，但“{”位于比较靠后的位置。
正常情况下，“}”位于最后，“{”应该位于比较靠前的位置，如：
CTF{...}
flag{...}
ssctf{...}
所以这是栅栏密码，用以下 C# 程序解密：
$ mcs fence.cs
$ ./fence.exe < /tmp/a
gjgpQt@_gsQw1{fgqfhh0gosh#t}
gfjggqpfQhth@0_ggossQhw#1t{}
ggqht{ggQht_gsQ10jsf#@fopwh}
gQg1q0hjts{fg#g@Qfhotp_wghs}

第3个符合要求：
$ ./fence.exe < /tmp/a | head -3 | tail -1 | tee /tmp/b
ggqht{ggQht_gsQ10jsf#@fopwh}

最后用以下 C# 程序进行凯撒密码解密，
其中 C# 程序给出所有26种可能的结果，
再用 grep 挑选出包含 ctf 或 flag（不区分大小写）的结果。
$ mcs Caesar.cs && ./Caesar.exe < /tmp/b | grep -i -e ctf -e flag
ssctf{ssCtf_seC10ver#@rabit}
