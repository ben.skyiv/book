[GFSJ0147] xctf, Misc, 难度:3, 如来十三掌。
题目描述: 菜狗为了打败菜猫，学了一套如来十三掌。
题目附件: [下载附件]

下载附件得到一个 docx 文档，使用以下“与佛论禅”网站解密文档内容：

https://ctf.bugku.com/tool/todousharp   # 或者
https://www.keyfc.net/bbs/tools/tudoucode.aspx
点击“普度众生”按钮，提示：将需要解码的文字输入在下面的文本框里，记得带上『佛曰：』或『如是我闻：』的文字，点击『参悟佛所言的真意』按钮，就能在上面的文本框里得到解码后的文字。

按照提示，得到：MzkuM3gvMUAwnzuvn3cgozMlMTuvqzAenJchMUAeqzWenzEmLJW9
根据题目“如来十三掌”，用 ROT13 解码，得到：
$ rot13.sh MzkuM3gvMUAwnzuvn3cgozMlMTuvqzAenJchMUAeqzWenzEmLJW9; echo
ZmxhZ3tiZHNjamhia3ptbmZyZGhidmNraWpuZHNrdmJramRzYWJ9
再用 base64 解码得到：
$ rot13.sh MzkuM3gvMUAwnzuvn3cgozMlMTuvqzAenJchMUAeqzWenzEmLJW9 | base64 -d; echo
flag{bdscjhbkzmnfrdhbvckijndskvbkjdsab}
