[GFSJ0147] xctf, Misc, 难度:2, 小小的PDF
题目来源: CTF
题目描述: 暂无
题目附件: [下载附件]

$ binwalk a4f37ec070974eadab9b96abd5ddffed.pdf
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PDF document, version: "1.4"
452           0x1C4           JPEG image data, JFIF standard 1.01
73254         0x11E26         JPEG image data, JFIF standard 1.01
81606         0x13EC6         Zlib compressed data, default compression
82150         0x140E6         JPEG image data, JFIF standard 1.01
104469        0x19815         Zlib compressed data, default compression
105134        0x19AAE         Zlib compressed data, default compression
用 binwalk 发现 3 个 JPEG 图像，用 foremost 分离：
$ foremost a4f37ec070974eadab9b96abd5ddffed.pdf
在 output/jpg/00000160.jpg 中发现 flag:
SYC{so_so_so_easy}
