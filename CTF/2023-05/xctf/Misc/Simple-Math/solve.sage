from hashlib import sha1
a = ('1 1 0 1 1 0 1 1 1 1'
     '0 0 1 1 0 0 0 1 1 0'
     '1 0 1 1 0 0 1 1 1 0'
     '0 1 0 1 0 1 1 1 0 0'
     '0 0 0 0 1 1 0 1 1 1'
     '1 1 1 1 1 0 0 0 1 0'
     '1 0 0 0 0 1 0 1 1 1'
     '0 0 0 1 0 1 0 0 0 1'
     '0 1 1 0 0 0 1 1 1 0'
     '0 0 0 1 0 0 1 1 0 1')
b = ('1 0 1 1 0 0 0 1 0 1')
a = matrix(ZZ, 10, 10, list(a.replace(' ', '')))
b = matrix(ZZ, 10,  1, (int(i) for i in b.replace(' ', '')))
for i in range(2 ** 10):
    x = matrix(ZZ, 10, 1, (int(i) for i in f'{i:010b}'))
    m = matrix(ZZ, 10, 1, (i % 2 for i in a * x))
    if m == b: break
s = ''.join(str(i)[1:-1] for i in x)
print(f'XSCTF{{{sha1(s.encode()).hexdigest()}}}')
