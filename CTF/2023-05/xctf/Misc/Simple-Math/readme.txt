[GFSJ1113] xctf, Misc, 难度:2, Simple-Math
题目来源: XSCTF联合招新赛
题目描述: Simple! Simple! Simple! 中国人不骗中国人！
题目附件: [下载附件]

$ unzip 450b37cb-10ae-43a3-9c23-8a091765dc7c.zip
$ xdg-open challenge.pdf
$ sage solve.sage
XSCTF{103c8041593b4b8e38971db283a7a773b0ffc2ee}
