[GFSJ0512] xctf, Misc, 难度:4, give_you_flag
题目描述: 菜狗找到了文件中的彩蛋很开心，给菜猫发了个表情包
题目附件: [下载附件]

$ gifspliter.py 4b0799f9a4d649f09a882b6b1130bb70.gif
Total: 53
在 z49.png 中发现缺少定位器的二维码，将二维码移到左上角，
将尺寸 72x72 缩放为 360x360，补全3个定位器。
$ kolourpaint z49.png ~/src/book/CTF/test/qr.png &
$ zbarimg z49.png        # 识别二维码得到 flag
QR-Code:flag{e7d478cf6b915f50ab1277f78502a2c5}
