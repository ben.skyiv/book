[GFSJ0975] xctf, Misc, 难度:5, easy_misc
题目来源: CTF
题目描述: 这是一个藏了秘密图片的压缩包
题目附件: [下载附件]

解压所下载的附件，发现需要密码，使用 john 暴力破解：
$ zip2john 4252bf8a-c5a2-4cc8-a0c3-e8c06615dd57.zip >/tmp/a
$ john /tmp/a
...
qwer             (4252bf8a-c5a2-4cc8-a0c3-e8c06615dd57.zip/1.png)
$ 7z x -pqwer 4252bf8a-c5a2-4cc8-a0c3-e8c06615dd57.zip
发现解压后得到的 1.png 损坏，检查一下：
$ pngcheck.exe 1.png
CRC(computed): 9DF303D8
CRC(expected): C61C9FAA
948x170x8, 6 0 0 0
00-00-03-B4-00-00-00-DF
用 pngcrc32.py 修复：
$ pngcrc32.py 1.png
1.png.png : 948 x 223
$ file *.png
1.png:     PNG image data, 948 x 170, 8-bit/color RGBA, non-interlaced
1.png.png: PNG image data, 948 x 223, 8-bit/color RGBA, non-interlaced
打开 1.png.png，得到：
# 题解
```
链接：https://pan.baidu.com/s/1cG2QvYy3khpQGLfjfbYevg
提取码：cavb
按照提示，下载得到一个 6 MiB 的 file.pcap
$ file file.pcap
file.pcap: pcap capture file, microsecond ts (little-endian) - version 2.4 (Ethernet, capture length 262144)
用 wireshark 查看，看起来是 SQL 延时注入。

方法1：使用 tshark 提取数据：
$ tshark -r file.pcap -T fields -e urlencoded-form.value | grep flag > /tmp/a

方法2：使用 strings 提取数据：
$ strings file.pcap | grep flag > /tmp/b
$ python url_decode.py < /tmp/b > /tmp/a

继续：
得到的数据具有以下格式：
... flag limit 0,1),1,1))=30,sleep(5),1) ...
......
... flag limit 0,1),1,1))=102,sleep(5),1) ...
... flag limit 0,1),2,1))=30,sleep(5),1) ...
......
... flag limit 0,1),39,1))=126,sleep(5),1) ...
用以下程序提取 flag：
$ python solve.py       # Python 程序，或者
$ csc solve.cs && mono solve.exe  # C# 程序
flag{cd2c3e2fea463ded9af800d7155be7aq}~
