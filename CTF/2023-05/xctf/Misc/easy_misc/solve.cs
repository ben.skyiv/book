using System;
using System.IO;
using System.Text.RegularExpressions;

var n0 = 0;
var r = new Regex(@"=(\d+),sleep");
foreach (var s in File.ReadLines("/tmp/a")) {
    var m = r.Match(s);
    if (!m.Success) throw new Exception("Match fail");
    var n = int.Parse(m.Groups[1].ToString());
    if (n < n0) Console.Write((char)n0);
    n0 = n;
}
Console.WriteLine((char)n0);
