[GFSJ0493] xctf, Reverse, 难度:4, no-strings-attached
题目来源: CTF
题目描述: 菜鸡听说有的程序运行就能拿Flag？
题目附件: [下载附件]

$ file 554e0986d6db4c19b56cfdb22f13c834
554e0986d6db4c19b56cfdb22f13c834: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 2.6.24, BuildID[sha1]=c8d273ed1363a1878f348d6c506048f2354849d0, not stripped
$ chmod +x 554e0986d6db4c19b56cfdb22f13c834
$ ida64 554e0986d6db4c19b56cfdb22f13c834 &
$ gdb 554e0986d6db4c19b56cfdb22f13c834
pwndbg> disass main
pwndbg> disass authenticate
...
x08048719 <+17>:	mov    DWORD PTR [esp],0x8048aa8
0x08048720 <+24>:	call   0x8048658 <decrypt>
0x08048725 <+29>:	mov    DWORD PTR [ebp-0xc],eax
...
pwndbg> b *0x8048725
pwndbg> r
pwndbg> x/sw $eax
0x804fd00:	U"9447{you_are_an_international_mystery}"
