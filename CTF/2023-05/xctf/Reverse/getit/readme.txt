[GFSJ0495] xctf, Reverse, 难度:4, getit
题目来源: CTF
题目描述: 菜鸡发现这个程序偷偷摸摸在自己的机器上搞事情，它决定一探究竟
题目附件: [下载附件]

$ cp e3dd9674429f4ce1a25c08ea799fc027 getit
$ file getit
getit: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 2.6.24, BuildID[sha1]=e389cd7a4b9272ba80f85d7eb604176f6106c61e, not stripped
$ chmod +x getit
$ ida64 getit &

方法1：
$ gdb getit
pwndbg> disass main
pwndbg> b *0x400824
pwndbg> r
...
*RDI  0x6010e0 (t) ◂— 'SharifCTF{b70c59275fcfa8aebf2d5911223c6589}'
pwndbg> x/s $rdi
0x6010e0 <t>:	"SharifCTF{b70c59275fcfa8aebf2d5911223c6589}"
pwndbg> p (char *)&t
$2 = 0x6010e0 <t> "SharifCTF{b70c59275fcfa8aebf2d5911223c6589}"


方法2：
$ gdb --write getit
pwndbg> disass main
0x00000000004008a2 <+332>:	mov    eax,0x0
0x00000000004008a7 <+337>:	call   0x400620 <fprintf@plt>
0x00000000004008ac <+342>:	add    DWORD PTR [rbp-0x3c],0x1
...
0x00000000004008c5 <+367>:	mov    rdi,rax
0x00000000004008c8 <+370>:	call   0x4005c0 <remove@plt>
0x00000000004008cd <+375>:	mov    eax,0x0
...
pwndbg> set *(unsigned*)(0x4008a7)=0x90909090
pwndbg> set *(char*)(0x4008ab)=0x90
pwndbg> set *(unsigned*)(0x4008c8)=0x90909090
pwndbg> set *(char*)(0x4008cc)=0x90
pwndbg> q
$ ./getit
$ cat /tmp/flag.txt
SharifCTF{b70c59275fcfa8aebf2d5911223c6589}
