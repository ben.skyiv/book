bVar1, enflag = 18, "izwhroz\"\"w\"v.K\".Ni"
s1 = (chr((bVar1 ^ ord(c)) - 6) for c in enflag[0::3])
s2 = (chr((bVar1 ^ ord(c)) + 6) for c in enflag[1::3])
s3 = (chr((bVar1 ^ ord(c)) ^ 6) for c in enflag[2::3])
print(''.join(''.join(c) for c in zip(s1, s2, s3)))
