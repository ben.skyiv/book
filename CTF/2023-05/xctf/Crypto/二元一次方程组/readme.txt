[GFSJ1112] xctf，Crypto，难度:2，二元一次方程组
题目来源: XSCTF联合招新赛
题目描述: 签到题
题目附件: [下载附件]

$ 7z x 0b4bc32e-9d34-4b38-8560-144e949cceba.zip
得到 avgrsa.py 和 out.txt，由此可知本题是 RSA 加密，
已知 e（指数），c（密文），n = p*q（模），还有 avg = (p+q)/2
因此 phi = (p-1)(q-1) = p*q - p - q + 1 = n - 2*avg + 1
以下按照标准的 RSA 套路就行了：
$ python solve.py
hsctf{Dl3F4TH3rRR4iin_AvAvA}
