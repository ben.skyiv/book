import gmpy2
import libnum

with open('out.txt', 'r') as f:
    n = int(f.readline().split('=')[1].strip())
    avg = int(f.readline().split('=')[1].strip())
    c = int(f.readline().split('=')[1].strip())
e = 65537
phi = n - 2*avg + 1
d = gmpy2.invert(e, phi)
m = pow(c, d, n)
print(libnum.n2s(int(m)).decode())
