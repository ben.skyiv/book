with open('task.py', 'r') as f:
    for _ in range(17): f.readline()
    enc = bytearray.fromhex(f.readline()[2:])
flag, flag0, n = b'', b'XMan{', 16
key = list(flag0 + enc[len(flag0) + n:n * 2])
for i in range(len(flag0)): key[i] ^= enc[i]
for i in range(n * 2):
    flag += (key[i % n] ^ enc[i]).to_bytes()
print(flag.rstrip(b'\0').decode())
