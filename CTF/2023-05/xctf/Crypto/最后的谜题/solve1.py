from gmpy2 import invert


def encrypt(m, a, b, kt):
    c = ''
    for mt in m:
        tmp = key.find(mt)
        k = kt
        for _ in range(len(c0)):
            tmp = tmp * a if k % 2 == 0 else tmp + b
            k = k // 2
        c += key[tmp % len(key)]
    return c


def decrypt(c, inv_a, b, kt):
    m = ''
    for ct in c:
        tmp = key.find(ct)
        k = kt
        for _ in range(len(c0)):
            tmp = tmp * inv_a if k % 2 == 0 else tmp - b
            k = k // 2
        m += key[tmp % len(key)]
    return m


def get_params(limit):
    m = flag[4:4+2]
    for a in range(limit):
        for b in range(limit):
            for kt in range(limit):
                if (encrypt(m, a, b, kt) == c0[:2]):
                    return a, b, kt


with open('easyCaesar.py', 'r') as f:
    for i in range(3): f.readline()
    flag = f.readline().strip()
    for i in range(7 - 4): f.readline()
    key = f.readline().split('=')[1].strip()[1:-1]
    for i in range(23 - 8): f.readline()
    c0 = f.readline()[1:].strip()
flag = flag[flag.find("'")+1:]
a, b, kt = get_params(len(key))
print(flag[:4] + decrypt(c0, invert(a, len(key)), b, kt) + '}')
