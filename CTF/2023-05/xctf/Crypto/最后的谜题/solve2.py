from gmpy2 import invert


def encrypt2(m, a2, b2):
    c = ''
    for mt in m:
        c += key[(a2 * key.find(mt) + b2) % len(key)]
    return c


def decrypt2(c, inv_a2, b2):
    m = ''
    for ct in c:
        m += key[inv_a2 * (key.find(ct) - b2) % len(key)]
    return m


def get_params(limit):
    m = flag[4:4+2]
    for a2 in range(limit):
        for b2 in range(limit):
            if (encrypt2(m, a2, b2) == c0[:2]):
                return a2, b2


with open('easyCaesar.py', 'r') as f:
    for i in range(3): f.readline()
    flag = f.readline().strip()
    for i in range(7 - 4): f.readline()
    key = f.readline().split('=')[1].strip()[1:-1]
    for i in range(23 - 8): f.readline()
    c0 = f.readline()[1:].strip()
flag = flag[flag.find("'")+1:]
a2, b2 = get_params(len(key))
print(flag[:4] + decrypt2(c0, invert(a2, len(key)), b2) + '}')
