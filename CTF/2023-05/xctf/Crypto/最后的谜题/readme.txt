[GFSJ1052] xctf，Crypto，难度:3，最后的谜题
题目来源: XSCTF联合招新赛
题目描述: 在古老的东方，有着一个神秘的洞穴，一群考古学家深入其中，发现了超过人类智慧的器具，但奇怪的是洞穴中并没有生命迹象。考古学家发现洞穴上面的壁画隐隐约约刻着一串串字，将其翻译结果是“穷尽一生，寻此真理，来者务必将其好好保管…”，后半段的乱码阻碍了翻译…
题目附件: [下载附件]

$ 7z x 0c70d53b-91fe-45b0-a3b4-f545c3ca35c0.zip
解压后得到 easyCaeser.py，阅读这个程序可知
这是一种变形的凯撒密码，已知它将 'f{' 加密为 'Wh'。

方法1：
因为加密运算是 mod len(key) 的，
我们可以假设 a 和 b 都小于 len(key)，
不要被题目中巨大的 2**128 吓到。
至于 kt，它的最大值虽然可达 2**39 - 1，
但是我们还是猜测 kt 也小于 len(key)。
已知 'f{}' -> 'Wh'，可暴力破解出 a, b, kt。
$ python solve1.py
hsctf{th3_l4st_s3c5et_0f_4he_un1ve2se_1s_42}

方法2：
题目给出的 easyCaesar.py 中加密函数 encrypt 的最内层循环
可归结为 ct = a2 * key.find(mt) + b2，
易知明文 mt = inv_a2 * (key.find(ct) - b2)。
已知 'f{}' -> 'Wh'，可暴力破解出 a2 和 b2。
$ python solve2.py
hsctf{th3_l4st_s3c5et_0f_4he_un1ve2se_1s_42}

备注：可以用以下方式调试 easyCaeser.py：
$ vim easyCaesar.py  # 将第 1 行修改为：
flag = 'hsctf{***1234567890_abcdef***}'  # from secret import flag
然后以交互模式“-i”运行它，可以查看变量，调用函数：
$ python -i easyCaesar.py
l6QQQOK10rdcTFVo7Hbp2lQQQ
>>> m1
'f{***1234567890_abcdef***'
