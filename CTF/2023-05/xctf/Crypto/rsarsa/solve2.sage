p1= 96299565102878405622896721118929860350160024776145...733
ct= 53692868731001626333440793541878974197039413877018...751
n= 135848902202512642181457751708566394906505171283376...691
c= 359429273964996296239923716283596908671709201263407...441

def solveit(p1, ct, n, c):
    mod = pow(2,265)
    pbar = (p1 << 724) + ct
    PR.<x> = PolynomialRing(Zmod(n))
    for i in range(32):
        f = pbar + x * mod * 32
        f = f.monic()
        pp = f.small_roots(X=2^454, beta=0.4)
        if (pp): break
        pbar += mod
    assert pp
    p = pbar + pp[0] * 32 * mod
    q = n // Integer(p)
    e = 65537
    phi = (p - 1) * (q - 1)
    d = inverse_mod(e, phi)
    m = pow(c, d, n)
    return m

print(solveit(p1,ct,n,c))
