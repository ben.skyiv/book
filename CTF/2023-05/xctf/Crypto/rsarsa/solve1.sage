from pwn import *

context.log_level = "DEBUG"
conn = remote("61.147.171.105", int(51916))

def solveit(p1, ct, n, c):
    mod = pow(2,265)
    pbar = (p1 << 724) + ct
    PR.<x> = PolynomialRing(Zmod(n))
    for i in range(32):
        f = pbar + x * mod * 32
        f = f.monic()
        pp = f.small_roots(X=2^454, beta=0.4)
        if (pp): break
        pbar += mod
    assert pp
    p = pbar + pp[0] * 32 * mod
    q = n // Integer(p)
    e = 65537
    phi = (p - 1) * (q - 1)
    d = inverse_mod(e, phi)
    m = pow(c, d, n)
    return m

for _ in range(3):
    conn.recvuntil(b'p1=')
    params = conn.recvuntil(b'm=')
    params=params.split(b'\n')
    p1 = int(params[0][1:])
    ct = int(params[1][4:])
    n  = int(params[2][3:])
    c  = int(params[3][3:])
    m  = solveit(p1,ct,n,c)
    conn.sendline(str(m).encode())

conn.recvall()
