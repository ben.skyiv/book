import gmpy2
import libnum
from operator import mul
from functools import reduce

with open('release/out', 'r') as f:
    n = int(f.readline().strip()[4:])
    c = int(f.readline().strip()[4:])
ps = [
  9401433281508038261,
  10252499084912054759,
  13716847112310466417,
  11215197893925590897,
  11855687732085186571
  ]
e = 65537
assert reduce(mul, ps) == n
phi = reduce(mul, map(lambda x: x-1, ps))
d = gmpy2.invert(e, phi)
m = pow(c, d, n)
print(libnum.n2s(int(m)).decode())
