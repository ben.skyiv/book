[GFSJ1117] xctf，Crypto，难度:2，baigeiRSA2
题目来源: XSCTF联合招新赛
题目描述: 既然白给，就多给一点吧！
题目附件: [下载附件]

$ 7z x 79933ca5-a4ab-45eb-ab47-4f1f44e690a5.zip
得到 release/baigei2.py 和 release/out
由 release/baigei2.py 可知这题是 RSA 的变种，
而 release/out 给出了 n（模）和 c（密文），
其中 n 是5个（标准的 RSA 是2个）不同素数的乘积。
先用 yafu 分解出这5个素数：
$ head -1 release/out | cut -c5- | tee /tmp/n.txt
1757971372765174000241708611981920890212539...921
$ (cd /tmp; yafu "factor(@)" -batchfile n.txt)
...
P19 = 9401433281508038261
P20 = 10252499084912054759
P20 = 13716847112310466417
P20 = 11215197893925590897
P20 = 11855687732085186571
显而易见，phi = (p1-1)(p2-1)(p3-1)(p4-1)(p5-1)
注意：如果有素数相同，比如 n=p^k，则 phi(n)=(p^(k-1))(p-1)
于是我们有：
$ python solve.py
HSCTF{@Tv0_br3ad5_c1ip_cHe3se_!@}
