from Crypto.Util.number import bytes_to_long, long_to_bytes
from pubkey import P, n, e

R.<a> = GF(2^2049)
with open('flag.enc', 'rb') as f:
    c = R.from_integer(bytes_to_long(f.read()))
p, q = n.factor()
s = (pow(2, p[0].degree()) - 1) * (pow(2, q[0].degree()) - 1)
d = inverse_mod(e, s)
flag = long_to_bytes(R(pow(P(c), d, n)).to_integer())
print(flag[:flag.find(b'}') + 1].decode())
