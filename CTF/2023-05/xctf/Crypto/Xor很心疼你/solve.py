from base64 import b64decode
from random import seed, getrandbits


def f(x):
    seed(x)
    return getrandbits(8)


def core_encrypt(plaintext, key, r):
    for i in range(len(plaintext)):
        key = f(key)
        tmp = (key * r) % 251
        plaintext[i] ^= tmp
    return bytes(plaintext)


def seek(c, flag):
    m = list(map(ord, flag))
    for key in range(1, 256):    # key = random.getrandbits(8)
        for r in range(2, 251):  # r   = random.randint(2, 250)
            t = core_encrypt(m[:], key, r)
            if (t in c): return key, r, c.index(t)


with open('easyXor.py', 'r') as f1:
    for i in range(36): f1.readline()
    c = b64decode(f1.readline().strip()[4:-1].encode())
key, r, p = seek(c, 'hsctf{')
c = list(c[p:])
m = core_encrypt(c, key, r)
print(m.decode()[:m.index(b'}')+1])
