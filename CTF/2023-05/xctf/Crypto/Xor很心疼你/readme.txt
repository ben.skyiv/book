[GFSJ1114] xctf，Crypto，难度:2，Xor很心疼你
题目来源: XSCTF联合招新赛
题目描述: 看了这么久题目了，Xor很心疼你，并打算考考你
题目附件: [下载附件]

https://blog.csdn.net/figfig55/article/details/128646558
https://docs.python.org/3.11/library/random.html

$ 7z x b5582b14-ccd3-41c2-8f8d-8ba01312e310.zip
解压缩得到 easyXor.py，阅读这个 Python 程序得知这是使用异或的加密题。

可以用以下方式调试 easyXor.py
$ vim easyXor.py  # 将第 2 行修改为：
flag = 'hsctf{***1234567890_abcdef***}'  # from secret import flag
然后以交互模式“-i”运行它，可以查看变量，调用函数：
$ python -i easyXor.py
b'uic++w51vDxItQpDlhZ1oy8xgn1sjXxp/TZ...qjTY='
>>> m
'vk8...hsctf{***1234567890_abcdef***}...sQlAz'
>>> generate(48)
'cJTGlYiXSybw4EbPjZaDxlqggfCpyYw3ypekTgy3nsj7WIRY'
>>> key = random.getrandbits(128); key, f(key), r
(255562324559551008479662999018937236850, 150, 227)

这个 Ptyhon 程序（easyXor.py）的要点如下：
1. 旗帜 flag 是以 'hsctf{' 打头的字符串。（第2行和第8行）
2. 明文 m 是 flag 前后各加 200 多个随机字符生成的字符串。（第34行）
3. 变量 1 < r < 251 是给定的随机数（不会在加密过程改变）。（第7行）
4. 函数 f(x) 的算法是确定的，尽管其中使用了 random.getrandbits(8)，
但是由于给定了 random.seed(x)，所以不存在随机性。（第15-17行）
5. 使用 encrypot(plaintext, key) 函数加密。（第20-31行）
6. 调用加密函数时（第35行），第2个参数（key）虽然是高达 2**128 的随机数，
但是在加密函数内部（第22-24行）马上将其退化为 2**8 的随机数。

加密函数 encrypt(plaintext, key) 的工作流程（第20-31行）：
1. 把字符串类型明文 m 转为列表，列表元素即明文字符的 ASCII 值。（第21行）
2. 调用 20 次 key = f(key) 得到一个新的字节。（第22-24行）
3. 按以下方法遍历每个明文字符：（第25-29行）
(1) 调用 key = f(key) 得到一个新的字节。（第26行）
(2) 计算 tmp = (key * r) % 251。（第27行）
(3) 当前明文字符与 tmp 进行一次异或。（第29行）

本题目中的 key 和 r 是随机的未知变量，好在都不超过 2**8，
可以直接用双重循环直接遍历爆破，寻找 'hsctf{' 加密后的密文。
由于本题中加密函数的核心算法是异或，异或两次会还原，
所以加密函数也是解密函数。最终，使用以下 Python 程序破解：
$ python solve.py
hsctf{x0r_i5_v4ry@eAsy_1oakn29gm3m3k93}
