[GFSJ1052] xctf，Crypto，难度:2，crypto垃圾邮件
题目来源: 贵州贵阳
题目描述: 常常收到垃圾邮件都不知道是什么东西，这次我一定要搞清楚
题目附件: [下载附件]

https://blog.csdn.net/m0_48780534/article/details/127353906
$ 7z x 69978f32-c02e-4b02-8072-2de0d1fe1958.zip
$ file mail.txt
mail.txt: ASCII text, with CRLF line terminators
将这封邮件复制到以下解垃圾邮件的网站解码
https://www.spammimic.com/decode.shtml
flag{bd826fcdc637864d3ccc1c1e0360ff6d}
