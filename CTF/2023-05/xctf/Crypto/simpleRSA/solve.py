from libnum import n2s
from gmpy2 import iroot, is_prime, is_square, isqrt, invert, gcd
from number_theory import chinese_remainder  # ~/python-site-packages


def force_compute_p():
    for a in range(2**15, 2**16):
        if not is_prime(a): continue
        for b in range(38219, 38219+1000):
            if is_square(b*b+4*a*n):
                p = (-b+isqrt(b*b+4*a*n))//2//a
                if is_prime(p): return p


with open('task.py', 'r') as f:
    for _ in range(20): f.readline()
    n = int(f.readline().split('=')[1].strip())
    c = int(f.readline().split('=')[1].strip())
    for _ in range(28 - 22): f.readline()
    ns = list(map(int, f.readline().split('=')[1].strip()[1:-1].split(',')))
    cs = list(map(int, f.readline().split('=')[1].strip()[1:-1].split(',')))
    for _ in range(48 - 30): f.readline()
    n1 = int(f.readline().split('=')[1].strip())
    n2 = int(f.readline().split('=')[1].strip())
    c1 = int(f.readline().split('=')[1].strip())
    c2 = int(f.readline().split('=')[1].strip())

p = force_compute_p()
E1 = pow(c, invert(65537, (p-1)*(n//p-1)), n)
print(f'E1 = {E1}')

x = chinese_remainder(ns, cs)
E2 = iroot(x, 89)[0]
print(f'E2 = {E2}')

P = gcd(n1, n2)
Q1, Q2 = n1//P, n2//P
phi1 = (P-1)*(Q1-1)
phi2 = (P-1)*(Q2-1)
vv = gcd(E1, phi1)
m1 = pow(c1, invert(E1//vv, phi1), n1)
m2 = pow(c2, invert(E2//vv, phi2), n2)
m3 = m1 % P
m2 = m2 % Q2
m1 = m1 % Q1
m4 = chinese_remainder([Q1, Q2, P], [m1, m2, m3])
n3 = Q1 * Q2
phi3 = (Q1-1)*(Q2-1)
m5 = pow(m4, invert(7, phi3), n3)
m6 = iroot(m5, 5)[0]
flag = n2s(int(m6))
print(flag.decode())
