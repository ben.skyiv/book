[GFSJ1058] xctf，Crypto，难度:3，simpleRSA
题目来源: XMan
题目描述: simpleRSA
题目附件: [下载附件]

https://blog.csdn.net/weixin_52640415/article/details/127817186
https://blog.csdn.net/luochen2436/article/details/128012748

$ 7z x 6ce0f71c-5aee-4560-ab04-5869506f60af.zip
解压后得到 task.py，这是 RSA 加密题，分为4个部分

一、求解 E1：
已知 a = getPrime(16) 是不超过 16bit 的素数，b = 38219 + delta
且 q = a*p + b 是素数，则：a*p + b - q = 0，两边同时乘以 p 得到：
a*p**2 + b*p - p*q = 0，即 a*p**2 + b*p - n = 0，直接爆破求解 E1

二、求解 E2：
已知一组 n 和 c，利用中国剩余定理求解 E2

四、得到 flag：
$ python solve.py
E1 = 377312346502536339265
E2 = 561236991551738188085
flag{27dab675-9e9b-4c1f-99ab-dd9fe49c190a}
