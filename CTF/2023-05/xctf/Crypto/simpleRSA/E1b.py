from gmpy2 import is_prime, isqrt, invert, next_prime

with open('task.py', 'r') as f:
    for _ in range(20): f.readline()
    n = int(f.readline().split('=')[1].strip())
    c = int(f.readline().split('=')[1].strip())

for i in range(2**15, 2**16):
    if not is_prime(i): continue
    q = next_prime(i*isqrt(n//i) + 38219)
    if n % q == 0: break
p = n // q
E1 = pow(c, invert(65537, (p-1)*(n//p-1)), n)
print(f'E1 = {E1}')
