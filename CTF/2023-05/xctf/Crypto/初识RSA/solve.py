import gmpy2
import libnum

with open('newcomer.py', 'r') as f:
    for i in range(9): f.readline()
    e = int(f.readline().split('=')[1].strip())
    for i in range(22 - 10): f.readline()
    c = int(f.readline().split('=')[1].strip())
    n = int(f.readline().split('=')[1].strip())
    pq = int(f.readline().split('=')[1].strip())
    qp = int(f.readline().split('=')[1].strip())
phi = pq * qp // n
d = gmpy2.invert(e, phi)
m = pow(c, d, n)
print(libnum.n2s(int(m)))
