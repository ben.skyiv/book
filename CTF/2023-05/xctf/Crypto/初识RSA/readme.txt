[GFSJ1078] xctf，Crypto，难度:1，[简单]初识RSA
题目来源: XSCTF联合招新赛
题目描述: p和q藏起来了，你能帮我找到它们吗
题目附件: [下载附件]

$ 7z x 769d0403-1be9-47ff-9960-b7e6010cdab1.zip
得到 newcomer.py，由此可知本题是 RSA 加密，
已知 e（指数），c（密文），n = p*q（模），
还有 pq = p*(q-1) 和 qp = q*(p-1)
因此 phi = (p-1)(q-1) = pq * qp // n
以下按照标准的 RSA 套路就行了：
$ python solve.py
b'flag{719014b3-c4e1-4f81-a7be-b4f0d65c9e10}'
