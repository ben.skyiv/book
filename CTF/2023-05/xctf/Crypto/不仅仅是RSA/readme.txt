[GFSJ0832] xctf，Crypto，难度:7，不仅仅是RSA
题目来源: ISCC
题目描述: 暂无
题目附件: [下载附件]

$ unzip e870c9584db94daaa3274996c2c0a73a.zip
阅读 RSA.py 得知有两组 RSA，并且有公因数 q，因此可以使用 gcd，
使用 RsaCtfTool.py 从两组 pubkey 得到 n1, n2, e
$ RsaCtfTool.py --public '附件/pubkey?.pem' --dumpkey | tee /tmp/a
使用以下网站从 C1.wav 和 C2.wav 得到 c1 和 c2 存入 /tmp/b
https://morsecode.world/international/decoder/audio-decoder-adaptive.html
然后用 tr 命令删除 /tmp/b 中的空格
$ cat /tmp/b | tr -d ' ' | tee /tmp/c
最后用标准的 RSA 算法得到 flag
$ python solve.py
UNCTF{ac01dff95336aa470e3b55d3fe43e9f6}
