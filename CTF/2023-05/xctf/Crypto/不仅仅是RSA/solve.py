from gmpy2 import gcd, invert
from libnum import n2s

with open('/tmp/a', 'r') as f:
    n1 = int(f.readline().strip()[3:])
    e  = int(f.readline().strip()[3:])
    n2 = int(f.readline().strip()[3:])
q = gcd(n1, n2)
p1, p2 = n1 // q, n2 // q
with open('/tmp/c', 'r') as f:
    c1 = int(f.readline().strip().split(':')[1])
    c2 = int(f.readline().strip().split(':')[1])
phi1, phi2 = (p1 - 1) * (q - 1), (p2 - 1) * (q - 1)
d1, d2 = invert(e, phi1), invert(e, phi2)
m1, m2 = pow(c1, d1, n1), pow(c2, d2, n2)
print(f'{n2s(int(m1)).decode()}{n2s(int(m2)).decode()}')
