[GFSJ0481] xctf，Web，难度:2，xff_referer
题目来源: Cyberpeace-n3k0
题目描述: X老师告诉小宁其实xff和referer是可以伪造的。
题目场景: http://61.147.171.105:50413

打开场景，显示以下网页：
ip地址必须为123.123.123.123

打开 Burp Suite，Proxy, Intercept is on
Burp Suite，Proxy, Proxy settings，记住：127.0.0.1:8080
Firefox，设置，网络设置，手动配置代理
Firefox，刷新页面
Burp Suite，Proxy，Action，Send to Repeater
Burp Suite，Repeater，Send，得到：
ip地址必须为123.123.123.123
在 Request Headers 中增加：
X-Forwarded-For: 123.123.123.123
再次 Send，得到：
document.getElementById("demo").innerHTML="必须来自https://www.google.com";
在 Request Headers 中增加：
Referer: https://www.google.com
再次 Send，得到：
cyberpeace{57a5102d93552ba6c3a31c5a37291930}
常见的 Http Headers 见 CTF/doc/ctf.txt
