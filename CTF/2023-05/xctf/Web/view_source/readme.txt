[GFSJ0474] xctf，Web，难度:1，view_source
题目来源: Cyberpeace-n3k0
题目描述: X老师让小宁同学查看一个网页的源代码，但小宁同学发现鼠标右键好像不管用了。
题目场景: http://61.147.171.105:61505

打开场景，得到以下网页：
FLAG is not here

直接按 Ctrl-U 键（或者 <F12> 键）查看源代码：
cyberpeace{b3001de17419c5562cc475bd0eb49c0b}
