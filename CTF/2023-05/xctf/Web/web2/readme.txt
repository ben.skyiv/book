[GFSJ0627] xctf，Web，难度:2，web2
题目来源: CTF
题目描述: 解密
题目场景: http://61.147.171.105:56917

打开场景，页面显示：
<?php
$miwen="a1zLbgQsCESEIqRLwuQAyMwLyq2L5VwBxqGA3RQAyumZ0tmMvSGM2ZwB4tws";

function encode($str){
    $_o=strrev($str);
    // echo $_o;

    for($_0=0;$_0<strlen($_o);$_0++){

        $_c=substr($_o,$_0,1);
        $__=ord($_c)+1;
        $_c=chr($__);
        $_=$_.$_c;
    }
    return str_rot13(strrev(base64_encode($_)));
}

highlight_file(__FILE__);
/*
   逆向加密算法，解密$miwen就是flag
*/
?>

使用以下程序逆向加密算法，解密得到 flag
$ php solve.php; echo   # PHP 程序 或者
$ python solve.py       # Python 程序
flag:{NSCTF_b73d5adfb819c64603d7237fa0d52977}
