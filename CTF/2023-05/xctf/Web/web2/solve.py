from codecs import encode
from base64 import b64decode


c = 'a1zLbgQsCESEIqRLwuQAyMwLyq2L5VwBxqGA3RQAyumZ0tmMvSGM2ZwB4tws'
c = b64decode(encode(c, 'rot13')[::-1]).decode()
print(''.join(list(map(lambda x: chr(ord(x)-1), c)))[::-1])
