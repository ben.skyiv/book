[GFSJ0485] xctf，Web，难度:1，simple_php
题目来源: Cyberpeace-n3k0
题目描述: 小宁听说php是最好的语言,于是她简单学习之后写了几行php代码。
题目场景: http://61.147.171.105:63466

打开场景，页面显示：
<?php
show_source(__FILE__);
include("config.php");
$a=@$_GET['a'];
$b=@$_GET['b'];
if($a==0 and $a){
    echo $flag1;
}
if(is_numeric($b)){
    exit();
}
if($b>1234){
    echo $flag2;
}
?>

原理：php中有两种比较符号
=== 会同时比较字符串的值和类型
==  会先将字符串换成相同类型，再作比较，属于弱类型比较
is_numeric($b) 函数当 b 不是纯数字是返回 false，
但是“>”将 4321b 视为 4321。
于是，我们构造 ?a=x&b=4321b，得到 flag：
Cyberpeace{647E37C7627CC3E4019EC69324F66C7C}
