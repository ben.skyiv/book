福建分行_小黑，Misc，100
描述：我们在网络边界截取了一封黑客沟通的邮件，发现是一张图片，猜测黑客利用隐写术在传递下一步的行动计划，请分析图片尽快破解黑客传递的信息，阻止黑客下一步的攻击。
附件：misc2.zip

$ unzip misc2.zip 
Archive:  misc2.zip
  inflating: 3.jpg
$ foremost 3.jpg
$ strings -20 3.jpg  | tail -1 | base64 -d
flag{key=FFD8FFE0}
$ 7z x -pFFD8FFE0 output/zip/00000020.zip
$ echo -n FFD8FFE0 > /tmp/a
$ cat /tmp/a flag | hexdecoder.py > 1.jpg
$ zbarimg 1.jpg 
QR-Code:flag{lo9dwzsgfae437m65ukhtqrijvxb802n}
