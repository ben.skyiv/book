福建分行_ezDoc，Misc，100
描述：发现黑客从某名业务主管电脑中窃取了一份文件，但是黑客利用了一些手段对敏感数据做了处理，为了减少损失，请帮忙分析黑客拿走了什么敏感信息。
附件：misc1.zip

$ unzip misc1.zip 
Archive:  misc1.zip
  inflating: attachments.docx
$ mkdir a; cd $_
$ 7z x ../attachments.docx
$ cut -c11- _rels/error.xml | base64 -d > a.jpg
$ zbarimg a.jpg
QR-Code:flag{40ccf25d8244893c2eb9013265da1842}
