import sympy
import gmpy2
import libnum

e = 0x10001
fp = open('Eazyrsa/output.txt.txt')
fp.readline()
c = int(fp.readline().split('=')[1])
p = int(fp.readline().split('=')[1])
q = int(fp.readline().split('=')[1])
n = p * q
phi = (p - 1) * (q - 1)
d = gmpy2.invert(e, phi)
m = pow(c, d, n)
print(libnum.n2s(int(m)).decode(), end='')

fp.readline()
c = int(fp.readline().split('=')[1])
n = int(fp.readline().split('=')[1])
q = sympy.nextprime(gmpy2.isqrt(n))
p = n // q
phi = (p - 1) * (q - 1)
d = gmpy2.invert(e, phi)
m = pow(c, d, n)
print(libnum.n2s(int(m)).decode(), end='')

e = 3
fp.readline()
c = int(fp.readline().split('=')[1])
n = int(fp.readline().split('=')[1])
for k in range(1024):
    m, b = gmpy2.iroot(c+k*n, e)
    if b:
        print(libnum.n2s(int(m)).decode())
        break
fp.close()
