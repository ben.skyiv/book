福建分行_数据库泄露，Misc，100
描述：在运维人员的机器上发现了黑客入侵的痕迹，利用流量分析工具截取了这台机器的流量，请分析黑客从这台机器上拿走了什么敏感信息
附件：misc3.zip

$ unzip misc3.zip 
Archive:  misc3.zip
  inflating: 1.pcapng
$ strings 1.pcapng | grep -i flag{
.c-icon-flag{background-position:0 -144px}
)flag{dc399e24-6771-11ed-a5a3-00155db770d}
