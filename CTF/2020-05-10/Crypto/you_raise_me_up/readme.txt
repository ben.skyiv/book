c = m ^ flag mod n
这题是求离散对数，即求 log(c, m) mod n
在 sage 中有 discrete_log(c, mod(m, n))
在 python 里有 sympy.discrete_log(n, c, m)

$ python answer.py && sage solve.sage
flag{5f95ca93-1594-762d-ed0b-a9139692cb4a}
