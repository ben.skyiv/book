from sympy import discrete_log
from libnum import n2s

with open('you_raise_me_up.py') as fp:
    for _ in range(11): fp.readline()
    m = int(fp.readline().split('=')[1])
    c = int(fp.readline().split('=')[1])
n = 2**512
print(n2s(int(discrete_log(n, c, m))).decode())
