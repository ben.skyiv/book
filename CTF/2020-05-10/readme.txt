战队名称：skyiv
战队排名：1521
战队ID：T329687
解题数量：1
得分：7

提交答案人数：3773名
被攻克题数：16题/共18题
报名队伍数：6701队
解题队伍数：2488队
登录人数：11699名

题目：  攻克的队伍数,分值
Misc/签到:      2427,   5
Misc/Teslaaaaa:    3, 455
Misc/虚幻2:       26, 223
Misc/未完成的书：  0, 500
Web/AreUSerialz: 713,  14
Web/trace:        37, 179
Web/notes:        82, 100
Web/filejava:    199,  46
Reverse/bang:    450,  22
Reverse/rev01:     0, 500
Reverse/jocker:  214,  43
Reverse/signal:  464,  21
Crypto/boom: *  1418,   7
Crypto/your rai: 547,  18
Crypto/easy_ya:   56, 134
PWN/boom1:       237,  40
PWN/boom2:        82, 100
PWN/faster0:      21, 250
