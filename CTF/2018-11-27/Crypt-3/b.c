#include <stdio.h>
#include <stdlib.h>

int char2num(int c)
{
  if (c >= 'a' && c <= 'z') return c - 'a';
  puts("error: char2num()"); exit(1);
}

int num2char(int n) { return n + 'a'; }

int inv(int y)
{
  static int m = 26;
  for (int x = 0; x < m; x++)
    if ((5*x + 12) % m == y) return x;
  puts("error: inv()"); exit(2);
}
// 此密文是通过函数y=5x+12得到的，请解密。key为明文的MD5值。RgYDMllaKzGC
int main(void)
{
  char *s = "rgydmllakzgc";
  for (int i = 0; i < 12; i++) printf("%c", num2char(inv(char2num(s[i]))));
  printf("\n");
}

