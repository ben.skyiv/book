简单，Flag格式flag{xxx}.
密文：synt{on19715rqr5s11qn3qo8qsn2o9950s95}

方法1：
$ rot13.sh synt{on19715rqr5s11qn3qo8qsn2o9950s95}; echo
flag{ba19715ede5f11da3db8dfa2b9950f95}

方法2：
$ cyberchef.sh
在 Operations 的搜索框里输入 rot，将出现的 ROT13 拖入 Recipe
在 Input  框中粘贴 synt{on19715rqr5s11qn3qo8qsn2o9950s95}
在 Output 框中得到 flag{ba19715ede5f11da3db8dfa2b9950f95}
