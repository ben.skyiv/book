using System;
using System.IO;

static class A
{
    static void Main()
    {
        var a = new char[20];
        foreach (var s in File.ReadLines
            ("1/ReverseEngineeringChallenge.java"))
        {
            var t = s.Trim();
            if (!t.StartsWith("password")) continue;
            t = t.Substring(t.IndexOf('(') + 1);
            var n = int.Parse(t.Substring(0, t.IndexOf(')')));
            a[n] = t[t.IndexOf('\'')+1];
        }
        Console.WriteLine(new String(a));
    }
}
