#include <stdio.h>
#include <string.h>
#include "data_type.h"
#include "crypto_func.h"

const char *trim(char *buf)
{
  int len = strlen(buf);
  if (len > 0 && buf[len - 1] == '\n') buf[len - 1] = '\0';
  return buf;
}

int main(void)
{
  static UINT32 buf[BUFSIZ];
  static char name[BUFSIZ], path[BUFSIZ];
  static char prefix[] = "../sbinbackup/";
  int offset = strlen(prefix);
  strcpy(path, prefix);
  FILE *fp = fopen("../name_list", "r");
  while (fgets(name, BUFSIZ, fp) != NULL) {
    strcpy(path + offset, trim(name));
    int result = calculate_sm3(path, buf);
    for (int i = 0; i < 8; i++) printf("%08x", buf[i]);
    printf(" %d %s\n", result, name);
  }
  fclose(fp);
}

