using System;
using System.IO;

static class B
{
  static void Main()
  {
    using (var sw = new StreamWriter   ("../digest3_list"))
      foreach (var s2 in File.ReadLines("../digest2_list")) {
        var s3 = s2.ToCharArray();
        for (var i = 0; i < 64; i += 8) {
          s3[i + 0] = s2[i + 6];
          s3[i + 1] = s2[i + 7];
          s3[i + 2] = s2[i + 4];
          s3[i + 3] = s2[i + 5];
          s3[i + 4] = s2[i + 2];
          s3[i + 5] = s2[i + 3];
          s3[i + 6] = s2[i + 0];
          s3[i + 7] = s2[i + 1];
        }
        sw.WriteLine(new string(s3));
      }
  }
}

