using System;
using System.IO;
using System.Collections.Generic;

static class A
{
  static void Run(string file, int seq)
  {
    var dict = new Dictionary<string, List<int>>();
    var i = 0;
    foreach (var s in File.ReadLines(file)) {
      i++;
      var key = s.Split()[0];
      List<int> value;
      if (!dict.TryGetValue(key, out value)) dict.Add(key, value = new List<int>());
      value.Add(i);
    }
    if (seq == 1) {
      var a = new int[100];
      foreach (var kvp in dict) a[kvp.Value.Count]++;
      for (i = 0; i < a.Length; i++) if (a[i] != 0) Console.Write("{0}:{1} ", i, a[i]);
    }
    else 
      foreach (var kvp in dict)
        if (kvp.Value.Count == 3)
          foreach (var n in kvp.Value)
            Console.Write(n + " ");
    Console.WriteLine();
  }
  
  static void Main(string[] args)
  {
    var seq = (args.Length == 0) ? 1 : int.Parse(args[0]);
    Run("../digest_list" , seq);
    Run("../digest2_list", seq);
  }
}

