using System;
using System.IO;
using System.Collections.Generic;

static class C
{
  static void Main()
  {
    var set = new HashSet<string>();
    foreach (var s in File.ReadLines("../digest_list")) set.Add(s);
    foreach (var s in File.ReadLines("../digest3_list"))
      if (!set.Contains(s.Split()[0])) Console.WriteLine(s);
  }
}

