#ifndef  CRYPTO_FUNC_H
#define  CRYPTO_FUNC_H
#define DIGEST_SIZE 32

int digest_to_uuid(BYTE *digest,char *uuid);
int uuid_to_digest(char * uuid,BYTE * digest);
int calculate_sm3(char* filename, UINT32 *SM3_hash);
int calculate_context_sm3(char* context, int context_size, UINT32 *SM3_hash);

#endif
