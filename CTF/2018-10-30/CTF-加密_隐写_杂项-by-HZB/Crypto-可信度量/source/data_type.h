#ifndef _OS210_DATA_TYPE_H
#define _OS210_DATA_TYPE_H

typedef unsigned char         BYTE;
typedef unsigned short int  UINT16;
typedef unsigned int        UINT32;
typedef unsigned long int   UINT64;

typedef unsigned short int    WORD;
typedef unsigned int         DWORD;


#define DIGEST_SIZE	32
#endif
