$ pacman -Ss apktool: archlinuxcn/android-apktool 2.3.4-1
$ pacman -Ss jadx   : community/jadx 0.8.0-1
$ uncompyle6 -h

/home/ben/WindowsData/iCQ/01a/重庆分行竞赛/银监2018/密码学/

题目1：调皮的凯撒
e6ece1e7fbb8b4b5e2e6b9e5b7e3e6e4e4b8b5e5b8b7e3b3b5b8b6b7b3b5e1e1e4e5e6e4b1fd

$ python2 caesar.py
flag{845bf9e7cfdd85e87c3586735aadefd1}

题目2：喵星誓言
古时候一喵星人要向喵星发送一段不知道干什么用的密码“BLOCKCIPHERDESIGNPRINCIPLE”，但是它忘记了密钥是什么，手头（爪头）只有它自己加密过的密钥“HTRUZYJW”，而且它还知道原密钥是一个单词，你可以帮助它传递信息，早日攻克蓝星，征服人类吗？
提交flag格式：flag{xxxx}。

百度有第二题的原题，百度的题目虽然一样，但是。。。
百度中显示   “BLOCKCIPHERDESIGNPRINCIPLE”为明文，用密钥对其加密后，为flag！
实际考试时应将“BLOCKCIPHERDESIGNPRINCIPLE”作为密文对其解密，然后用密钥解密得出flag

https://blog.csdn.net/u010379510/article/details/50913016
https://blog.csdn.net/dongyanwen6036/article/details/76716373
$ mcs cat1.cs && ./cat1.exe
HTRUZYJW
COMPUTER

$ python vigenere.py
BLOCKCIPHERDESIGNPRINCIPLE

