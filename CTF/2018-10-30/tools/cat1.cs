static class Cat1
{
  static void Main()
  {
    var s = "HTRUZYJW";
    for (var i = 0; i < 26; i++) {
      System.Console.Write("{0:D2}: ", i);
      foreach (int c in s) {
        var d = c - i; if (d < 'A') d += 26;
        System.Console.Write((char)d);
      }
      System.Console.WriteLine();
    }
  }
}

