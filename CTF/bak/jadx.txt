Kali Linux 的 jadx 命令行程序“当前目录不正确问题”的规避办法：

$ sudo apt install jadx  # 安装 jadx，如果已经安装了则跳过这一步
$ which jadx
/usr/bin/jadx
上面这个 jadx 有问题，我写了一个正确的替换它

确信已经建立 ~/bin 目录且位于 PATH 环境变量最前面
$ cd ~/bin && echo '/usr/share/jadx/bin/jadx "$@"' >jadx && chmod +x jadx
$ exit

然后重新打开一个终端
$ which jadx
/home/<username>/bin/jadx
