在 Kali Linux 中安装 stegsolve 的步骤：
$ mkdir -p ~/opt/stegsolve && cd $_
$ wget http://www.caesum.com/handbook/Stegsolve.jar
$ echo 'java -jar ~/opt/stegsolve/Stegsolve.jar "$@"' > ~/bin/stegsolve
$ chmod +x ~/bin/stegsolve
