$ pngcheck.exe a.png
CRC(computed): 999DBA18
CRC(expected): 409C5369
580x257x8, 6 0 0 0
00-00-02-44-00-00-01-47
用以下 Ptyhon 程序暴力破解：
$ pngcrc32.py a.png
a.png.png : 580 x 327
$ file *.png
a.png:     PNG image data, 580 x 257, 8-bit/color RGBA, non-interlaced
a.png.png: PNG image data, 580 x 327, 8-bit/color RGBA, non-interlaced
$ xdg-open a.png.png
得到：md5(single_dog)
$ echo -n single_dog | md5sum
356ec37f8ebfd0706c19ebf0e2033c75  -
所以答案是：
flag{356ec37f8ebfd0706c19ebf0e2033c75}
