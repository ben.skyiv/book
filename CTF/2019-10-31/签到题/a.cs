using System;
using System.Text;
using System.Collections.Generic;

static class A
{
  static void Main()
  {
    var list = new List<byte>();
    foreach (var s in Console.In.ReadToEnd().Trim().Split())
      list.Add((byte)(byte.Parse(s) - 13));
    var bs = list.ToArray();
    Console.WriteLine(Encoding.UTF8.GetString(bs));
  }
}
