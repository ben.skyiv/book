邮件监控平台截获一位已经提离职申请的员工近期频繁外发邮件，
且邮件中有一个附件，可是DLP并未检测到敏感信息，你能帮忙分析这个附件是什么嘛？
提交flag格式：flag{xxxx}
--------------------------------------------------------------
$ zipclean.exe flag.zip
00000000: 50-4B-03-04 0A-00 00-08
0000006E: 50-4B-01-02 3F-00 0A-00 00-08
000000C5: 50-4B-01-02 3F-00 14-00 09-00
/tmp/a.zip: 310 bytes
$ 7z x /tmp/a.zip
$ cat flag/flag; echo
flag{Slmple_Zip_Pseud0_En4rypt^_^}


或者
$ cat ~/bin/zipclean.sh
java -jar ~/bin/ZipCenOp.jar r "$@" 2>/dev/null; echo
$ zipclean.sh /tmp/a.zip
success 2 flag(s) found

或者
$ bless /tmp/a.zip &
把第4个“50 4B”之后的第7个字节从 09 改为 00
50 4B 01 02 3F 00 14 00 09 00
-- -- 1  2  3  4  5  6  00 00
