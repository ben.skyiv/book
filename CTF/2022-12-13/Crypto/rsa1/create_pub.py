import os

with open('rsa.txt', 'r') as f:
    for i in range(4): f.readline()
    e = int(f.readline().split('=')[1].strip())
    f.readline()
    n = int(f.readline().split('=')[1].strip())
os.system(f'RsaCtfTool.py --private --createpub -n {n} -e {e}')
