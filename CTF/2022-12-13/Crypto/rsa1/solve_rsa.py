from rsa import PrivateKey
from gmpy2 import invert
from libnum import n2s

with open('/tmp/pem', mode='rb') as fp: data = fp.read()
key = PrivateKey.load_pkcs1(data)
with open('rsa.txt') as fp:
  for i in range(2): fp.readline()
  c = int(fp.readline().split('=')[1].strip())
d = invert(key.e, (key.p - 1) * (key.q - 1))
m = pow(c, d, key.p * key.q)
print(n2s(int(m)))
