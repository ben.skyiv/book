2020年中国工商银行股份有限公司全行攻防比赛：MISC/学习使我快乐
题目中给出一幅 jpg 图像，经分析，这幅图像后半部还包含一幅 png 图像。
分离这两幅图像，使用图像比对工具，得到一幅二维码，识别后得到 flag。
具体步骤如下：

$ binwalk secret.jpg          # 分析图像
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
14225         0x3791          PNG image, 260 x 260, 8-bit/color RGB, non-interlaced
14266         0x37BA          Zlib compressed data, default compression
$ dd if=secret.jpg of=1.jpg bs=1 count=14225        # 分离第一幅图像
$ dd if=secret.jpg of=2.png bs=1  skip=14225        # 分离第二幅图像
$ compare 1.jpg 2.png 3.png    # 比对图像

或者：

$ foremost secret.jpg          # 直接分离所有图像
$ compare output/jpg/00000000.jpg output/png/00000027.png 3.png  # 比对图像

最后：

用微信扫一扫（或者用 QR Research 打开）3.png 得到
flag{90928e57baff6ab5a3725d3a14784d28}
