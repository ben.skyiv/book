$ file flag.doc
flag.doc: Zip archive data, at least v1.0 to extract, compression method=store
$ 7z x flag.doc
$ find flag -type f
在解压后的目录中（共13个文件）发现：
$ iconv -f GB18030 -t utf8 flag/flag/word/theme/theme2.txt | tee /tmp/a.txt; echo
256743339412249911173022251870967118393333125923200059962235319964192511336203803263137295487140994979509731248832824184533125059339333(被你发现了，嘻嘻)
使用以下 Python 程序解码：
$ python solve.py
b'ZmxhZyU3QjRlMWMwOWU4ODQzNDVkODAxMTczZTE2NDdhZTU0ODYzJTdE'
b'flag%7B4e1c09e884345d801173e1647ae54863%7D'
flag{4e1c09e884345d801173e1647ae54863}
