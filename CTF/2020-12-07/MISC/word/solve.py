import libnum
import base64
import urllib.parse

with open('/tmp/a.txt', 'r') as f:
  m = int(f.readline().split('(')[0])
s = libnum.n2s(m); print(s)
s = base64.b64decode(s); print(s)
s = urllib.parse.unquote(s); print(s)
