from Crypto.Util.number import *
from Crypto.Random.random import *

def gcd(x, y):
    while y: x, y = y, x % y
    return x

def powerMod(x, y, m):
    z = 1
    while y != 0:
        if (y & 1) == 1: z = (z * x) % m
        y >>= 1
        x = (x * x) % m
    return z

v2 = getStrongPrime(1024)
v4 = getStrongPrime(1024)
while True:
    r = randint(1, 2 ** 512)
    if gcd(r, v4 - 1) == 1: break
wowo = bytes_to_long(b"WoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWo")
flag = bytes_to_long(b"flag{This_is_test_for_ICBC_FJ_Nanping!}")
pow1 = powerMod(powerMod(v2, getStrongPrime(1024), v4), r, v4)
with open('cipher-test.txt', 'w') as f:
    f.write("v1 = " + str(powerMod(v2, r, v4)) + "\n")
    f.write("v2 = " + str(v2) + "\n")
    f.write("v3 = " + str((wowo * pow1) % v4) + "\n")
    f.write("v4 = " + str(v4) + "\n")
    f.write("v5 = " + str((flag * pow1) % v4) + "\n")
