from Crypto.Util.number import *
from Crypto.Random.random import *
from flag import flag

Wo = getStrongPrime(1024)
WoWo = getStrongPrime(1024)
WoWoWo = getStrongPrime(1024)


def WoWoWoWo(Wo, WoWo):
    while WoWo: Wo, WoWo = WoWo, Wo % WoWo
    return Wo


def WoWoWoWoWo(Wo, WoWo, WoWoWo):
    WoWoWoWo = 1
    while WoWo != 0:
        if (WoWo & 1) == 1:
            WoWoWoWo = (WoWoWoWo * Wo) % WoWoWo
        WoWo >>= 1
        Wo = (Wo * Wo) % WoWoWo
    return WoWoWoWo


WoWoWoWoWoWoWoWo = WoWoWoWoWo(WoWoWo, Wo, WoWo)
while True:
    WoWoWoWoWoWoWoWoWo = randint(1, 2 ** 512)
    if WoWoWoWo(WoWoWoWoWoWoWoWoWo, WoWo - 1) == 1:
        break
WoWoWoWoWoWo = bytes_to_long("WoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWo")
WoWoWoWoWoWoWoWoWoWo = WoWoWoWoWo(WoWoWo, WoWoWoWoWoWoWoWoWo, WoWo)
WoWoWoWoWoWoWoWoWoWoWo = (WoWoWoWoWoWo * WoWoWoWoWo(WoWoWoWoWoWoWoWo, WoWoWoWoWoWoWoWoWo, WoWo)) % WoWo
WoWoWoWoWoWoWo = bytes_to_long(flag)
WoWoWoWoWoWoWoWoWoWoWoWo = (WoWoWoWoWoWoWo * WoWoWoWoWo(WoWoWoWoWoWoWoWo, WoWoWoWoWoWoWoWoWo, WoWo)) % WoWo
with open('cipher.txt', 'w') as f:
    f.write("WoWoWoWoWoWoWoWoWoWo = " + str(WoWoWoWoWoWoWoWoWoWo) + "\n")
    f.write("WoWoWo = " + str(WoWoWo) + "\n")
    f.write("WoWoWoWoWoWoWoWoWoWoWo = " + str(WoWoWoWoWoWoWoWoWoWoWo) + "\n")
    f.write("WoWo = " + str(WoWo) + "\n")
    f.write("WoWoWoWoWoWoWoWoWoWoWoWo = " + str(WoWoWoWoWoWoWoWoWoWoWoWo) + "\n")
