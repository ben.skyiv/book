2020年中国工商银行股份有限公司全行攻防比赛：Crypto/Wocipher_1
题目给出 cipher.py，并且给出该程序生成的 cipher.txt
cipher.py 的第 30 行是
WoWoWoWoWoWoWo = bytes_to_long(flag)
我们就是要找出这个 flag
cipher.py 的函数名称和变量名不利于分析程序，
我们把 cipher.py 复制到 cipher-test.py
然后修改 cipher-test.py 为 Python3 格式
（原来为 Python2，就是把 bytes_to_long 的参数改为 b"..."）
并且修改 cipher-test.py 的函数名称和变量名
（比如把 WoWoWoWo 改为 gcd，把 WoWoWoWoWo 改为 power ）
运行 cipher-test.py 可以生成 cipher-test.txt。

然后根据 cipher-test.py 的流程写出 solve.py：
wowo, v3, v4 已知，由 v3 = wowo * pow1 % v4 可得：
pow1 = (1/wowo % v4 * U) % v4
v5 已知，由 v5 = flag * pow1 % v4 可得：
flag = (1/pow1 % v4 * v5) % v4，从而得到：
flag{dbf589cad823db4a2fdca376514e0875}

走过的弯路：
v1 = v2^r % v4 已知
用2020年网鼎杯 2020-05-10/Crypto/you_raise_me_up 的方法可得：
r = sympy.discrete_log(v4, v1, v2)
由于 r, v1, v2, v4 非常大，计算离散对数非常慢，得不到 r。
实际上 r 和 v1, v2 都是干扰项，解 flag 用不到这些变量。
