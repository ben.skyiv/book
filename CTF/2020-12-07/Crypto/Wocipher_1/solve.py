from Crypto.Util.number import inverse,bytes_to_long
from libnum import n2s

with open('cipher.txt', 'r') as f:
  f.readline()
  f.readline()
  v3 = int(f.readline().split('=')[1].strip())
  v4 = int(f.readline().split('=')[1].strip())
  v5 = int(f.readline().split('=')[1].strip())
wowo = bytes_to_long(b"WoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWoWo")
pow1 = inverse(wowo, v4) * v3 % v4
flag = inverse(pow1, v4) * v5 % v4
print(n2s(flag))
