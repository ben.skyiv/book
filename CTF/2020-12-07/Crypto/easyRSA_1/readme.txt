Crypto/easyRSA_1

$ 7z x easyRSA_1.zip
解压得到 cipher.py
查看该 Python 源文件，得知本题是 RSA 加密，
已知 e（指数）, c（密文）, n = p*q（模）和 z = p+q
因此 phi = (p-1)(q-1) = p*q - (p+q) + 1 = n - z + 1
以下按照标准的 RSA 套路就行了：
$ python a.py
b'flag{f4c9e78c471f05f25ed929b32d74757d}'
