Crypto/RSA_2

$ 7z x comRSA_2.zip
解压得到 3 个 RSA 数据文件，各包含 (n,e) 和 c。
利用 gp 计算发现这3个 n 有公因数。
$ gp -q a.gp
? p1=gcd(n1,n2)
122...037
? p3=gcd(n2,n3)
136...777
攻击成功，得到 3 个私钥。下面就是标准的 RSA 套路了：
$ python rsa_1.py; python rsa_2.py; python rsa_3.py 
b'flag{212e00f5'
b'44ac1b5e34c02'
b'f44d90deb85}'
拼合得到：
flag{212e00f544ac1b5e34c02f44d90deb85}


首先利用 (n,e) 得到 pub?.pem：
$ RsaCtfTool.py --createpub -n 169...513 -e 73357 > pub1.pem
$ RsaCtfTool.py --createpub -n 167...749 -e 65537 > pub2.pem
$ RsaCtfTool.py --createpub -n 186...443 -e 67735 > pub3.pem
然后使用这 3 个公钥攻击：
$ RsaCtfTool.py --publickey "pub?.pem" --private --dumpkey
