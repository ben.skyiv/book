using System;
using System.IO;

static class Xor
{
  static byte[] GetBytes(string filename)
  {
    byte[] bs;
    using (var br = new BinaryReader(File.OpenRead(filename)))
      bs = br.ReadBytes(int.MaxValue);
    return bs;
  }
  
  static void Main()
  {
    var bs0 = GetBytes("B0.bin");
    var bs1 = GetBytes("B1.bin");
    var bs = new byte[bs0.Length];
    for (var i = 0; i < bs.Length; i++)
      bs[i] = (byte)(bs0[i] ^ bs1[i]);
    using (var bw = new BinaryWriter(File.OpenWrite("a.bin")))
      bw.Write(bs);
  }
}
