#!/usr/bin/env python3
from sys import argv
from pathlib import Path


def seek(bs, start, target):
    for i in range(start, len(bs)):
        if bs[i:i+len(target)] == target: return i
    return -1


def hex_str(bs):
    return '-'.join(f'{c:02X}' for c in bs)


def out_and_modify_head(bs, i):
    i0 = 0 if i == 0 else 2
    s1 = hex_str(bs[i:i+4])
    s2 = hex_str(bs[i+4:i+6])
    s3 = '' if i0 == 0 else ' ' + hex_str(bs[i+6:i+8])
    s4 = hex_str(bs[i+i0+6:i+i0+8])
    print(f'{i:08X}: {s1} {s2}{s3} {s4}')
    if i0 == 0 and s1 != '50-4B-03-04':
        raise ValueError('Invalid file head')
    bs[i+i0+6] = 0


with open(argv[1], 'rb') as fp: bs = bytearray(fp.read())
i, h, t = 0, bytearray([0x50, 0x4B, 0x01, 0x02]), '/tmp/'
while i >= 0:
    out_and_modify_head(bs, i)
    i = seek(bs, i + 10, h)
file_name = (t if Path(t).exists() else '') + 'a.zip'
if argv[1] != file_name:
    print(f'{file_name}: {len(bs)} bytes')
    with open(file_name, 'wb') as fp: fp.write(bs)
