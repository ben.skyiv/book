import gmpy2
import libnum

with open('cipher.py', 'r') as f:
    for i in range(7): f.readline()
    e = int(f.readline().split('=')[1].strip())
    for i in range(14 - 8): f.readline()
    n = int(f.readline().strip()[1:])
    c = int(f.readline().strip()[1:])
    z = int(f.readline().strip()[1:])
phi = n - z + 1
d = gmpy2.invert(e, phi)
m = pow(c, d, n)
print(libnum.n2s(int(m)))
