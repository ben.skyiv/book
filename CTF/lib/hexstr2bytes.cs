string s = "";
for (string t; (t = System.Console.ReadLine()) != null; )
  s += t.Replace(" ", "").Replace("\t", "");
if (s.ToLower().StartsWith("0x")) s = s.Substring(2);
var bs = new byte[s.Length / 2];
for (int i = 0; i < bs.Length; i++)
  bs[i] = System.Convert.ToByte(s.Substring(i * 2, 2), 16);
var name = (args.Length == 1) ? args[0] : "/tmp/a";
System.IO.File.WriteAllBytes(name, bs);
System.Console.WriteLine("{0}: {1:N0} bytes", name, bs.Length);
