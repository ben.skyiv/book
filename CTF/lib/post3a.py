import requests
import base64
import re
 
s=requests.Session()
u="http://123.206.87.240:8002/web6/"
headers =s.get(u).headers
str1 = str(base64.b64decode(headers['flag']),'utf-8')
# 打印头部中的flag信息，base64解码，将bytes类型转换成string
str2 = re.findall('\w*', str1) # 匹配非特殊字符
str3 = str2[5:6] # 截取编码
str4 = "".join(str3) # 转换成字符串
str5 = str(base64.b64decode(str4), 'utf-8') # 解码
post = {'margin':str5}
flag = s.post(u, data=post)
print(flag.text)
