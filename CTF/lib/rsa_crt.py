from gmpy2 import iroot
from libnum import n2s
from number_theory import chinese_remainder

cs, ns = [], []
with open('题目.txt', 'r') as f:
    while True:
        s = f.readline()
        if len(s) == 0: break
        ns.append(int(s.split('=')[1], 5))
        cs.append(int(f.readline().split('=')[1], 5))
        f.readline()
me = chinese_remainder(ns, cs)
for e in range(2, 1024):
    m, b = iroot(me, e)
    if b:
        print(n2s(int(m)).decode())
        break
