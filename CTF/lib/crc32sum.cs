using System;
using System.IO;
using System.Collections.Generic;
using Skyiv.Utils;

static class Crc32sum
{
  static Stream r = Console.OpenStandardInput();

  static IEnumerable<byte> Read()
  {
    for (int z; (z = r.ReadByte()) != -1; )
      yield return (byte)z;
  }
  
  static void Main(string[] args)
  {
    var z = Read().GetCrc32();
    Console.WriteLine("0x{0:X8} = {0}", z);
  }
}
