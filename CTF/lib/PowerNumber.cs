using System;

static class A
{
  static int DigitSum(int n)
  {
    var z = 0;
    for (; n > 0; n /= 10) z += n % 10;
    return z;
  }
  
  static void Main()
  {
    var s = Console.ReadLine();
    Console.WriteLine(s);
    var ss = s.Split('0');
    foreach (var i in ss)
      Console.Write((char)('A' - 1 + DigitSum(int.Parse(i))));
    Console.WriteLine();
  }
}
