import colorsys
from PIL import Image


def get_dominant_color(image):
    image = image.convert('RGBA')
    max_score, dominant_color = 0, 0
    for count, (r, g, b, a) in image.getcolors(image.size[0] * image.size[1]):
        if a == 0: continue
        saturation = colorsys.rgb_to_hsv(r / 255, g / 255, b / 255)[1]
        y = min(abs(r * 2104 + g * 4130 + b * 802 + 4096 + 131072) >> 13, 235)
        y = (y - 16) / (235 - 16)
        if y > 0.9: continue
        score = (saturation + 0.1) * count
        if score > max_score:
            max_score = score
            dominant_color = (r, g, b)
        return dominant_color


flag = ''
for i in range(104):
    image_file = 'gif/' + str(i) + '.jpg'
    f = get_dominant_color(Image.open(image_file))
    flag += '1' if f else '0'
    flag += ' ' if (i+1) % 8 == 0 else ''
print(flag)
