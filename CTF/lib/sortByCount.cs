using System;
using System.Linq;
using System.Collections.Generic;

static class Test
{
  static void Main()
  {
    var dict = new Dictionary<char, int>();
    foreach (var c in Console.ReadLine()) {
      int v;
      if (!dict.TryGetValue(c, out v)) dict.Add(c, 1);
      else dict[c] = v + 1;
    }
    var keys = dict.Keys.ToArray();
    var values = dict.Values.ToArray();
    Array.Sort(values, keys);
    foreach (var c in keys) Console.Write(c);
    Console.WriteLine();
  }
}
