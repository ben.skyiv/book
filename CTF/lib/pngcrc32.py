#!/usr/bin/env python3
from sys import argv
from zlib import crc32
from struct import pack
from Crypto.Util.number import bytes_to_long

file_name = argv[1]
fr = open(file_name, 'rb').read()
data, crc32key = bytearray(fr[12:29]), bytes_to_long(fr[29:33])
limit = 4095  # 理论上 0xFFFFFFFF, 但考虑到屏幕实际，0x0FFF 就差不多了
for w in (bytes_to_long(fr[16:20]), *range(limit)):  # 原宽度优先爆破
    width = bytearray(pack('>i', w))  # >:big-endian, i:int(4bytes)
    for h in range(limit):
        height = bytearray(pack('>i', h))
        for x in range(4):
            data[x + 4] = width[x]
            data[x + 8] = height[x]
        if crc32(data) == crc32key:
            file2 = file_name + '.png'
            print(f'{file2}: {bytes_to_long(width)}x{bytes_to_long(height)}')
            img = bytearray(fr)
            for x in range(4):
                img[x + 16] = width[x]
                img[x + 20] = height[x]
            with open(file2, 'wb') as fw: fw.write(img)
            exit()
