import gmpy2
import libnum

p = 780900790334269659443297956843
q = 1034526559407993507734818408829
e = 0x10001
c = 0x534280240c65bb1104ce3000bc8181363806e7173418d15762
n = p*q

phi = (p-1)*(q-1)
d = gmpy2.invert(e, phi)
m = pow(c, d, n)
print(libnum.n2s(int(m)))
