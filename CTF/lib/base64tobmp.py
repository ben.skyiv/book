import base64
from PIL import Image

f = open("WTF.txt", "r")

data = f.read()

data = base64.b64decode(data)

print(len(data), type(data))
# len(data)=65536=256*256
print(data[0], data[1])

newImg = Image.new("RGBA", (256, 256))

for i in range(0, 256):
    for j in range(0, 256):
        color = data[i*256+j]
        if color == 0x30:  # '0'
            newImg.putpixel((j, i), (255, 255, 255))
        else:
            newImg.putpixel((j, i), (0, 0, 0))

newImg.save("out.bmp")
