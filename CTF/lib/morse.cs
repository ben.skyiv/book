using System;
using System.Collections.Generic;

static class Morse
{
  static readonly Dictionary<string, char> dict;

  static readonly string[] digits = {
    //   0        1       2         3        4
    "-----", ".----", "..---", "...--", "....-",
    //   5        6        7        8        9
    ".....", "-....", "--...", "---..", "----."
  };
  static readonly string[] letters = {
    // a      b       c      d    e       f      g       h     i       j
    ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---",
    // k       l     m     n      o       p       q      r      s    t
    "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-",
    // u       v      w       x       y       z
    "..-", "...-", ".--", "-..-", "-.--", "--.."
  };

  static Morse()
  {
    dict = new Dictionary<string, char>();
    dict.Add(".--.-", '\\');
    for (var i = 0; i < digits.Length; i++) dict.Add(digits[i], (char)('0' + i));
    for (var i = 0; i < letters.Length; i++) dict.Add(letters[i], (char)('a' + i));
  }
  
  static char GetChar(string s)
  {
    char c;
    if (!dict.TryGetValue(s, out c)) c = '?';
    return c;
  }

  static void Main()
  {
    foreach (var s in Console.In.ReadToEnd().Split()) Console.Write(GetChar(s));
    Console.WriteLine();
  }
}
