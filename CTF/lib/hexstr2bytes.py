#!/usr/bin/env python3
from sys import argv, stdin
from Crypto.Util.number import long_to_bytes
s = stdin.read()
for c in ' \t\n': s = s.replace(c, '')
bs = long_to_bytes(int(s, 16))
name = argv[1] if len(argv) > 1 else '/tmp/a'
with open(name, 'wb') as fp: fp.write(bs)
print(f'{name}: {len(bs)} bytes')
