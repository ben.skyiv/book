F, s = GF(31337), []
g, y = F(5), F(15676)  # y = g^x (mod F)
# s.append(discrete_log_rho(y, g, operation='*'))
s.append(discrete_log(y, g, operation='*'))
s.append(discrete_log_lambda(y, g, (5000, 6000)))

k.<a> = GF(31337)  # x * P = Q
E = EllipticCurve(k, [123, 234])
P, Q = E([233, 18927]), E([1926, 3590])
s.append(discrete_log(Q, P, operation='+'))

print(s)
