using System;
using System.Text;

static class Decstr2Ascii
{
  static void Main()
  {
    var encode = Encoding.ASCII;
    var bs = new byte[1];
    for (; ; ) {
      var s = Console.ReadLine();
      if (s == null) break;
      foreach (var t in s.Split()) {
        if (t.Length == 0) continue;
        bs[0] = Convert.ToByte(t, 10);
        Console.Write(encode.GetString(bs));
      }
    }
    Console.WriteLine();
  }
}
