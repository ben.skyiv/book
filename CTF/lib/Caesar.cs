using System;
using System.Text;

static class Caesar
{
  static readonly int len = 26;

  static char Add(char c, int n, int low, int len)
  {
    if ((int)c < low || (int)c >= low + len) return c;
    return (char)(((int)c - low + n) % len + low);
  }

  static StringBuilder Transform(StringBuilder sb, int n)
  {
    for (var i = 0; i < sb.Length; i++) {
      sb[i] = Add(sb[i], n, 0x41, len);
      sb[i] = Add(sb[i], n, 0x61, len);
    }
    return sb;
  }

  static void Main()
  {
    var sb = new StringBuilder(Console.ReadLine());
    for (var i = 0; i < len; i++) {
      var s = Transform(sb, 1).ToString();
      if (s.StartsWith("cyberpeace"))
        Console.WriteLine(s);
    }
  }
}
