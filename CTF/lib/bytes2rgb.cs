using System;
using System.IO;

static class Bytes2rgb
{
  static void Main()
  {
    byte[] bs;
    using (var br = new  BinaryReader(File.OpenRead("a.bin")))
      bs = br.ReadBytes(int.MaxValue);
    Console.WriteLine(bs.Length);
    using (var sw = new StreamWriter(File.OpenWrite("1.txt")))
      for (var i = 0; i < bs.Length / 3; i++) {
        var k = i * 3;
        sw.WriteLine("{0},{1},{2}", bs[k], bs[k+1], bs[k+2]);
      }
  }
}
