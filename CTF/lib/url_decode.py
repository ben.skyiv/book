#!/usr/bin/python
from sys import stdin
from urllib.parse import unquote

while True:
    line = stdin.readline()
    if not line: break
    print(unquote(line).strip())
