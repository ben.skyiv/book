import z3

x, y = z3.Int('x'), z3.Int('y')
s = z3.Solver()
s.add(57*x + 33*y ==  69096)
s.add(71*x + 68*y == 110837)
print(s.check(), s.model())
