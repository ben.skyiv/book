from gmpy2 import iroot
from libnum import n2s

with open('../test/rsa_small_exp.txt') as fp:
    n = int(fp.readline().split('=')[1], 16)
    c = int(fp.readline().split('=')[1], 16)
    e = int(fp.readline().split('=')[1], 16)
for k in range(1024):
    m, b = iroot(c+k*n, e)
    if b:
        print(n2s(int(m)))
        break
