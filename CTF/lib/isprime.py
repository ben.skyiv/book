#!/usr/bin/env python3
from gmpy2 import is_prime

s = input()
b = 16 if s[:2].lower() == '0x' else 10
n = int(s, b)
print(f'{is_prime(n)}: {s}')
