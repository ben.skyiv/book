using System;
using System.IO;

static class ZipClean
{
  static readonly byte[] Head = { 0x50,0x4B,0x01,0x02 };

  static bool Equal(byte[] bs, int start, byte[] target)
  {
    var j = 0;
    for (var i = start; i < bs.Length && j < target.Length; i++, j++)
      if (bs[i] != target[j]) return false;
    return j == target.Length;
  }

  static int Seek(byte[] bs, int start, byte[] target)
  {
    for (var i = start; i < bs.Length; i++)
      if (Equal(bs, i, target)) return i;
    return -1;
  }

  static void OutAndModifyHead(byte[] bs, int start)
  {
    var i0 = (start == 0) ? 0 : 2;
    var s1 = BitConverter.ToString(bs, start, 4);
    var s2 = BitConverter.ToString(bs, start + 4, 2);
    var s3 = (i0 == 0) ? "" : (" " + BitConverter.ToString(bs, start + 6, 2));
    var s4 = BitConverter.ToString(bs, start + 6 + i0, 2);
    Console.WriteLine("{0:X8}: {1} {2}{3} {4}", start, s1, s2, s3, s4);
    if (i0 == 0 && s1 != "50-4B-03-04") throw new Exception("Invalid file head");
    bs[start + 6 + i0] = /* bs[start + 7 + i0] = */ 0;
  }

  static void Main(string[] args)
  {
    try {
      if (args.Length != 1) throw new Exception("Usage: zipclean.exe zip_file_name");
      var bs = File.ReadAllBytes(args[0]);
      OutAndModifyHead(bs, 0);
      for (var i = 0; ; ) {
        i = Seek(bs, i + 10, Head);
        if (i < 0) break;
        OutAndModifyHead(bs, i);
      }
      string tmp = "/tmp", fileName = "a.zip";
      if (Directory.Exists(tmp)) fileName = Path.Combine(tmp, fileName);
      if (args[0] == fileName) return;
      Console.WriteLine("{0}: {1:N0} bytes", fileName, bs.Length);
      File.WriteAllBytes(fileName, bs);
    }
    catch (Exception ex) { Console.WriteLine(ex.Message); }
  }
}
