#!/usr/bin/env python
from base64 import b64decode
from libnum import s2n
from sys import stdin


def get_head(i):
    d, i = dat[i], i + 1
    if d & 0x80 == 0:
        return d, i
    j = i + (d & 0x7F)
    return s2n(dat[i:j]), j


m = ['version', 'n', 'e', 'd', 'p', 'q', 'dp', 'dq', 'qinv', 'other']
k, dat = 0, b64decode(''.join(stdin.readlines()[1:-1]))
d, i = get_head(1)
while i < len(dat):
    d, i = get_head(i + 1)
    v = s2n(dat[i:i+d])
    b1, b2 = len(bin(v))-2, len(bin(v).rstrip("0"))-2
    print(f'0x{v:x} {b1} {b1-b2} {m[k]}')
    i, k = i + d, k + 1 if k < len(m) - 1 else k
