#!/usr/bin/env python3
import sys
from PIL import Image
im = Image.open(sys.argv[1])
try:
    for i in range(10**20):
        im.seek(i)
        im.save('z' + str(i).zfill(3) + '.png')
except EOFError: pass
print('Total: ' + str(i))
