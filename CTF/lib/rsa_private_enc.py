from libnum import n2s
from rsa import PrivateKey

with open('private.pem', mode='rb') as privatefile:
    keydata = privatefile.read()
key = PrivateKey.load_pkcs1(keydata)
c = int(open("flag.enc", 'rb').read().hex(), 16)
m = pow(c, key.d, key.n)
print(n2s(m).decode())
