using System;
using System.Text;

static class Hexstr2bytes
{
  static void Main()
  {
    var encode = Encoding.GetEncoding("GB18030");
    var s = "43A61F0F5F0EB979F13B7F1325DDC7";
    var bs = new byte[s.Length / 2];
    for (int i = 0; i < bs.Length; i++)
      bs[i] = Convert.ToByte(s.Substring(i * 2, 2), 16);
    Console.WriteLine(encode);
    Console.WriteLine(encode.GetString(bs));
  }
}
