#!/usr/bin/env python3
t = [None] * (z := len(s := input()))
for n in range(2, z):
    if z % n != 0: continue
    for i in range(n):
        for j in range(m := z // n):
            t[j * n + i] = s[i * m + j]
    print(''.join(t))
