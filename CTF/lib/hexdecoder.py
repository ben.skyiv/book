#!/usr/bin/env python3
from sys import argv, stdin, stdout
from Crypto.Util.number import long_to_bytes
s, n = stdin.read(), int(argv[1]) if len(argv) > 1 else 16
if len(argv) <= 2:
    for c in ' \t\n': s = s.replace(c, '')
    bs = long_to_bytes(int(s, n))
else:
    bs = bytearray(int(c, n) for c in s.split())
stdout.buffer.write(bs)
