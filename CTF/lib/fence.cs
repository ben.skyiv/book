using System;
using System.Collections.Generic;

static class Fence
{
  static int[] GetProperFactor(int n)
  {
    var list = new List<int>();
    for (var i = 2; i < n; i++)
      if (n % i == 0) list.Add(i);
    return list.ToArray();
  }

  static void Main()
  {
    var s = Console.ReadLine();
    var t = new char[s.Length];
    foreach (var n in GetProperFactor(s.Length))
    {
      for (var i = 0; i < n; i++)
        for (int k = s.Length / n, j = 0; j < k; j++)
          t[j * n + i] = s[i * k + j];
      Console.WriteLine(new string(t));
    }
  }
}
