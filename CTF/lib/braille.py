#!/usr/bin/env python3
def tran(c):
    c = ord(c)
    if not 0x2800 <= c <= 0x28FF: return c
    c -= 0x2800
    return mask[c // 16] * 16 + c % 16


mask = [int(c, 16) for c in '32107654BA98FEDC']
print(bytes(map(tran, input())))
