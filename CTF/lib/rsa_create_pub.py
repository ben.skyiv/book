from os import system

with open('rsa.txt', 'r') as f:
    for _ in range(4): f.readline()
    e = f.readline().split('=')[1].strip()
    f.readline()
    n = f.readline().split('=')[1].strip()
system(f'RsaCtfTool.py --private --createpub -n {n} -e {e}')
