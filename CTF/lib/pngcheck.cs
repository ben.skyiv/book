// csc pngcheck.cs ../lib/Crc32.cs
using System;
using System.IO;
using System.Linq;
using Skyiv.Utils;

static class PngCheck
{
  static uint GetUInt32(this byte[] bs, int start)
  {
    var bs2 = bs.Skip(start).Take(4).ToArray();
    if (BitConverter.IsLittleEndian) Array.Reverse(bs2);
    return BitConverter.ToUInt32(bs2);
  }

  static byte[] ReadBytes(string path, int n)
  {
    using (var reader = new BinaryReader(File.OpenRead(path)))
      return reader.ReadBytes(n);
  }
  
  static bool Rebuild(uint crc, byte[] bs, bool isHigh)
  {
    var k = isHigh ? 10 : 6;
    for (var i = 0; i < 0x10; i++)
      for (var j = 0; j < 0x100; j++) {
        bs[k] = (byte)i; bs[k+1] = (byte)j;
        if (bs.GetCrc32() == crc) {
          Console.WriteLine(BitConverter.ToString(bs.Skip(4).Take(8).ToArray()));
          return true;
        }
      }
    return false;
  }

  static void Main(string[] args)
  {
    if (args.Length != 1)
    { Console.WriteLine("Usage: pngcheck.exe <png_file_name>"); return; }
    var head = "89-50-4E-47-0D-0A-1A-0A-00-00-00-0D-49-48-44-52";
    var bs = ReadBytes(args[0], 33);
    if (!BitConverter.ToString(bs).StartsWith(head))
    { Console.WriteLine("Invalid file head"); return; }
    var cs = bs.Skip(12).Take(17).ToArray();
    uint crc1 = cs.GetCrc32(), crc2 = bs.GetUInt32(29);
    Console.WriteLine("CRC(computed): {0:X8}", crc1);
    Console.WriteLine("CRC(expected): {0:X8}", crc2);
    Console.Write("{0}x{1}x{2}, ", bs.GetUInt32(16), bs.GetUInt32(20), bs[24]);
    Console.WriteLine("{0} {1} {2} {3}", bs[25], bs[26], bs[27], bs[28]);
    if (crc1 == crc2) return;
    if (!Rebuild(crc2, cs, true)) Rebuild(crc2, cs, false);
  }
}
