from PIL import Image

HEIGHT, WIDTH = 122, 503

pic = Image.new("RGB",(WIDTH, HEIGHT))
f = open("1.txt", "r")

for y in range (0,WIDTH):
	for x in range (0,HEIGHT):
		line = f.readline()
		r,g,b = line.split(",")
		pic.putpixel([y,x],(int(r),int(g),int(b)))	

f.close()
# pic.save("flag.jpg")	    	
pic.show()
