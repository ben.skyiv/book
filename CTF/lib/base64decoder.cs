using System;
using System.Text;

static class Base64Decoder
{
  static void Main()
  {
    for (string s; (s = Console.ReadLine()) != null; )
      Console.WriteLine(Encoding.UTF8.GetString(Convert.FromBase64String(s)));
  }
}
