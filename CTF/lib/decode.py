import base64

a = "dGVzdDEyMw=="
print(base64.b64decode(a))
# base64.b32decode(a)
# a.decode('hex')
# b'\344\270\255\345\233\275'.decode()
# bytearray.fromhex("66 6c 61 67")
# bytearray(map(int, "102 108 97 103".split())).decode()

# import urllib.parse
# urllib.parse.unquote(a)
# urllib.parse.quote(a)
# urllib.parse.urlencode(a)

# import html
# html.unescape('&#102;&#108;&#97;&#103;&#123;&#112;&#69;&#125;')
