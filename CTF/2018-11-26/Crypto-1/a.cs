using System;

static class A
{
  static void Main()
  {
    var s = "1101 1011100010000010111110101111001111111011011101011101011 10";
    foreach (var c in s) Console.Write(c == '1' ? '-' : (c == '0' ? '.' : c));
    Console.WriteLine();
  }
}

