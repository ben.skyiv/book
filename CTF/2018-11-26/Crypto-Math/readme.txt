据说黑客的高数都很厉害？！而且编程遵循骆驼命名法？这里有道小学生的题目，
你能做出来么？已知函数y=11x+19,密文：TwwdglDjGdpl，请解密，flag为明文的MD5值。
答案格式：flag{xxx}。
答案就是：flag{cabf7c1caee318024d3ef8182092f01b}

思路：因为密文只包含大小写英文字母，共52个，所以采用 mod 52 算术。

$ gcc a.c && ./a.out && echo TwwdglDjGdpl | python affine.py
AffineIsNiCe
$ echo -n 'AffineIsNiCe' | md5sum
cabf7c1caee318024d3ef8182092f01b  -

注意：这里的 echo 命令的 -n 参数不可缺少。
如果删除 a.c 最后的 printf("\n"); 则可以：
$ gcc a.c && ./a.out | md5sum
