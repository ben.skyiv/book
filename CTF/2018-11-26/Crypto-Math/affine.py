from gmpy2 import invert
t = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
a, b, m = 11, 19, len(t)
d = invert(a, m)
print(''.join(t[d * (m + t.index(c) - b) % m] for c in input()))
