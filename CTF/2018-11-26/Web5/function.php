<?php

$xkey='php_is_nice';

class Neo{
    public $cmd;
    public $key;   
    
    function __construct($cmd,$key){
        $this->cmd=$cmd;
        $this->key=$key;
    }

    function __wakeup(){        
        $this->run();
    }
    
    function run(){
        $this->waf();
        global $xkey;
        if ($this->key === $xkey){
            system($this->cmd);
        }
    }
    
    
    function waf(){        
        $black = array(' ','cat');
        foreach ($black as $key => $value) {
            if(stripos($this->cmd,$value)){
                die("Attack!");
            }
        }
    }   
    
}