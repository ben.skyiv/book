import libnum
import rsa

with open('/tmp/flag.pem', mode='rb') as privatefile:
    keydata = privatefile.read()
key = rsa.PrivateKey.load_pkcs1(keydata)
with open('it_may_contain_flag.txt', 'r') as f:
    for i in range(2): f.readline()
    c = int(f.readline().split('=')[1].strip(), 16)
m = pow(c, key.d, key.n)
print(libnum.n2s(int(m)))
