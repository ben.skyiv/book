$ python create_pub.py > /tmp/flag.pub  # 创建公钥
使用 RsaCtfTool.py 暴力破解，运行1分钟后使用 wiener 方法得到私钥
$ time RsaCtfTool.py --private --publickey /tmp/flag.pub --output /tmp/flag.pem
...
[*] Attack success with wiener method !
[+] Total time elapsed min,max,avg: 0.0005/60.0002/6.7826 sec.
...
然后使用以下 Python 程序得到 flag：
$ python rsa_private_txt.py
b'flag1sH3r3_d_ist0sma1l'
