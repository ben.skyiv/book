import rsa
import base64

with open('private.pem', mode='rb') as privatefile:
	keydata = privatefile.read()
privkey = rsa.PrivateKey.load_pkcs1(keydata)

f = open("flag.b64",'r').read()
c = int(base64.b64decode(f).hex(), 16)
for i in range(20):
  m = pow(c, privkey.d, privkey.n)
  c = m
flag = base64.b16decode(hex(c)[2:].upper())
# flag = rsa.decrypt(c,privkey)
print(flag)
