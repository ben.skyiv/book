https://blog.csdn.net/jianpanliu/article/details/106501709
https://blog.csdn.net/qq_41621362/article/details/102105642

题目来源： ASIS-CTF-Finals-2017
题目描述：有人正在他老房子的地下室里开发自己的RSA系统。 证明他这个RSA系统只在他的地下室有效！

-----------------------------------------------
将 output.txt 拆分为 public.pem 和 flag.b64
$ ~/git/RsaCtfTool/RsaCtfTool.py --publickey public.pem --attack factordb --dumpkey --private
根据这个运行结果得到 private.pem，然后：
$ python rsa_private.py 
b'the flag is: ASIS{n0t_5O_e4sy___RSA___in_ASIS!!!}'
