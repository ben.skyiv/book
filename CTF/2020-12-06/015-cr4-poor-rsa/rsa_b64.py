from Crypto.PublicKey import RSA
from gmpy2 import invert
import rsa
from base64 import b64decode

f = open("key.pub","rb").read()
pub = RSA.importKey(f)
n = pub.n
e = pub.e

p = 863653476616376575308866344984576466644942572246900013156919
q = 965445304326998194798282228842484732438457170595999523426901
d = int(invert(e, (p-1)*(q-1)))

key = rsa.PrivateKey(n,e,d,p,q)
f = open("flag.b64",'r').read()
c = b64decode(f)
flag = rsa.decrypt(c,key)
print(flag)
