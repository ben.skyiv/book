https://www.cnblogs.com/vict0r/p/13445509.html

$ python rsa_b64.py 
b'ALEXCTF{SMALL_PRIMES_ARE_BAD}\n'

或者：

$ ~/git/RsaCtfTool/RsaCtfTool.py --publickey key.pub --attack factordb --private
得到 private.pem，然后：
$ python rsa_private.py 
b'ALEXCTF{SMALL_PRIMES_ARE_BAD}\n'
