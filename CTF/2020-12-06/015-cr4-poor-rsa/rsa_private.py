import rsa
from base64 import b64decode

with open('private.pem', mode='rb') as privatefile:
	keydata = privatefile.read()
privkey = rsa.PrivateKey.load_pkcs1(keydata)

f = open("flag.b64",'r').read()
c = b64decode(f)
flag = rsa.decrypt(c,privkey)
print(flag)
