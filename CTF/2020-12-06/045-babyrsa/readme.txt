https://blog.csdn.net/weixin_44110537/article/details/108956759
https://www.cnblogs.com/semishigure/p/9318258.html
http://dyf.ink/crypto/asymmetric/rsa/rsa_chosen_cipher/

045: babyrsa
难度系数：7.0
题目来源：XCTF 4th-QCTF-2018
题目描述：暂无
题目场景：点击获取在线场景
题目附件：附件1

$ nc 220.249.52.133 37037
----------------------------- baby rsa -----------------------------
Come and Decode your data
If you give me ciphertext, I can tell you whether decoded data is even or odd
You can input ciphertext(hexdecimal) now
1CFF
odd

$ python rsa_parity_oracle_socket.py
...
560856645743734814774953158390773525781916094468093308691660509501812320
560856645743734814774953158390773525781916094468093308691660509501812349
b'QCTF{RSA_parity_oracle_is_fun}'

RSA parity oracle
对密文乘(2^e%n)操作, 解密的时候，
如果为偶数，说明明文再(0, n/2)之间，
否则在(n/2, n)之间
以此操作，只需要 log n 的次数就可以知道明文
