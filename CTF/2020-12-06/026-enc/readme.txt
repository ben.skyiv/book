题目描述：Fady不是很理解加密与编码的区别 所以他使用编码而不是加密，给他的朋友传了一些秘密的消息。

$ cp cca1ce4b15ba4ac7950f6d03f8fa6ad1 /tmp/a
$ sed -i 's/ONE/1/g'  /tmp/a
$ sed -i 's/ZERO/0/g' /tmp/a
$ hexdecoder.py 2 < /tmp/a | base64 -d > b.bin
用摩尔斯电码解码得：
 ALEXCTFTH15O1SO5UP3RO5ECR3TOTXT
ALEXCTF{TH15_1S_5UP3R_5ECR3T_TXT}
