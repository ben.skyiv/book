https://adworld.xctf.org.cn/task/task_list?type=crypto&number=5&grade=1

.008-Decrypt-the-Message: Poem codes
.009-OldDriver: 低加密指数广播攻击
.011-streamgame1: // TODO: 需进一步学习
*014-fanfie: 仿射密码
.016-banana-princess: ROT13 PDF 文件，用 Adobe reader 打开后复制图像
.017-shanghai: 维吉尼亚密码
.028-best_rsa: 共模攻击
*030-Easy-one: know-plaintext attack (C lang)
*031-sleeping-guard: XORed .png (C#)
.032-简单流量分析: 利用 ICMP 包的数据长度
*033-Decode_The_File: 利用 base64 的填充传递信息 (python2, C#)
.038-xor_game: 加密的英文诗（长诗与短 flag 重复 xor）
.040-beginners-luck: 加密的 png，使用 xortool 破解
.045-babyrsa: RSA parity oracle
.079-简单的rsa: (new_p, new_q) 与 (p, q) 的前 b 位相同
.082-RSA: 构造不断增大的 p,q,e
