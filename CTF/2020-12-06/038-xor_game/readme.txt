https://www.cnblogs.com/coming1890/p/13571982.html

题目来源： hctf-2018
题目描述：这是一首英文诗，但它是加密的。找到标志，并恢复它

$ python a.py
21 : hctf{xor_is_interesting!@#}
10 : hctf{trooetiYcn}
39 : hctf{tot_ieiDois_is_srxetdiMr@b_er_ip_isCnr}
16 : hctf{netRisrRtonuidXo}
19 : hctf{oinnxoisebCtrorsonn}
