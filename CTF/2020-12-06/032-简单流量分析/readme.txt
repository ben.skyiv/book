https://www.cnblogs.com/coming1890/p/13526532.html

题目来源： 2019工业信息安全技能大赛个人线上赛第二场
题目描述：不久前，运维人员在日常安全检查的时候发现现场某设备会不时向某不知名ip发出非正常的ICMP PING包。这引起了运维人员的注意，他在过滤出ICMP包分析并马上开始做应急处理很可能已被攻击的设备。运维人员到底发现了什么? flag形式为 flag{}

$ 7z x ce801b8150ef424699ab977c278846aa.zip
$ python a.py
flag{xx2b8a_6mm64c_fsociety}
