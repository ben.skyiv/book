from Crypto.PublicKey import RSA
import gmpy2
import base64
#Alice's
A_n = 1696206139052948924304948333474767
A_p = 38456719616722997
A_q = 44106885765559411
#Bob's
B_n = 3104649130901425335933838103517383
B_p = 49662237675630289
B_q = 62515288803124247

A_phi = (A_p - 1) * (A_q - 1)
B_phi = (B_p - 1) * (B_q - 1)
  
e = 65537
  
A_d = int(gmpy2.invert(e, A_phi))
B_d = int(gmpy2.invert(e, B_phi))
  
A_rsa = RSA.construct( (A_n, e, A_d) )
B_rsa = RSA.construct( (B_n, e, B_d) )
  
flag = {}
with open('a.txt') as f:
    for s in f.readlines():
        line = str(base64.b64decode(s), encoding = 'utf8')
        seq = int(line.split(';')[0].split(' ')[2])
        data = int(line.split('0x')[1].split('L;')[0], 16)
        sig = int(line.split('0x')[2].rstrip('L;\n'), 16)
        decry = B_rsa.decrypt(data)
        signcheck = A_rsa.sign(decry, '')[0]
  
        if signcheck == sig:
            flag[seq] = chr(decry)
dic = sorted(flag.items(), key = lambda item:item[0])  #对字典按键值进行排序，返回值为列表
print(dic)
f = ''
for i in dic:
    f += i[1]
print(f)
