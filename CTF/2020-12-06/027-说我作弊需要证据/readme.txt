https://blog.csdn.net/zz_Caleb/article/details/89316034
https://www.jianshu.com/p/c4c0802317e6

题目来源： ISCC-2017
题目描述：X老师怀疑一些调皮的学生在一次自动化计算机测试中作弊，他使用抓包工具捕获到了Alice和Bob的通信流量。狡猾的Alice和Bob同学好像使用某些加密方式隐藏通信内容，使得X老师无法破解它，也许你有办法帮助X老师。X老师知道
Alice的RSA密钥为(n, e) = (0x53a121a11e36d7a84dde3f5d73cf, 0x10001) (192.168.0.13)?,
Bob   的RSA密钥为(n, e) =(0x99122e61dc7bede74711185598c7, 0x10001) (192.168.0.37)

flag{n0th1ng_t0_533_h3r3_m0v3_0n}
