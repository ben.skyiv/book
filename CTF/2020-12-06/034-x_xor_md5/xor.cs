using System;
using System.IO;

static class Xor
{
  static void Main()
  {
    using (var br = new BinaryReader(File.OpenRead("6c0c57fa88eb44f3b179a6e9798fc7b6")))
    using (var bw = new BinaryWriter(File.OpenWrite("01")))
    {
      var mask = new byte[16];
      br.BaseStream.Seek(5 * mask.Length, SeekOrigin.Begin);
      br.Read(mask, 0, mask.Length);
      Console.WriteLine(BitConverter.ToString(mask));
      for (var i = 0; i < mask.Length; i++) mask[i] ^= 0x20;
      Console.WriteLine(BitConverter.ToString(mask).Replace("-", ""));
      br.BaseStream.Seek(0, SeekOrigin.Begin);
      var bs = new byte[mask.Length];
      for (int n; (n = br.Read(bs, 0, bs.Length)) > 0; bw.Write(bs, 0, n))
        for (var i = 0; i < n; i++) bs[i] ^= (byte)(mask[i]);
    }
  }
}
