https://blog.csdn.net/qq_42967398/article/details/91138366

题目来源： RCTF-2015
题目描述：key 不存在

$ xxd -g 1 6c0c57fa88eb44f3b179a6e9798fc7b6 
00000000: 69 35 41 01 1c 9e 75 78 5d 48 fb f0 84 cd 66 79  i5A...ux]H....fy
00000010: 55 30 49 4c 56 d2 73 70 12 45 a8 ba 85 c0 3e 53  U0ILV.sp.E....>S
00000020: 73 1b 78 2a 4b e9 77 26 5e 73 bf aa 85 9c 15 6f  s.x*K.w&^s.....o
00000030: 54 2c 73 1b 58 8a 66 48 5b 19 84 b0 80 ca 33 73  T,s.X.fH[.....3s
00000040: 5c 52 0c 4c 10 9e 32 37 12 0c fb ba cb 8f 6a 53  \R.L..27......jS
00000050: 01 78 0c 4c 10 9e 32 37 12 0c fb ba cb 8f 6a 53  .x.L..27......jS
00000060: 01 78 0c 4c 10 9e 32 37 12 0c fb ba cb 8f 6a 53  .x.L..27......jS
00000070: 01 78 0c 4c 10 9e 32 37 12 0c 89 d5 a2 fc        .x.L..27......
$ mcs xor.cs && ./xor.exe 
01-78-0C-4C-10-9E-32-37-12-0C-FB-BA-CB-8F-6A-53
21582C6C30BE1217322CDB9AEBAF4A73
034-x_xor_md5$ xxd -g 1 01
00000000: 48 6d 6d 6d 2c 20 67 6f 6f 64 20 6a 6f 62 2c 0a  Hmmm, good job,.
00000010: 74 68 65 20 66 6c 61 67 20 69 73 20 6e 6f 74 20  the flag is not 
00000020: 52 43 54 46 7b 57 65 31 6c 5f 64 30 6e 33 5f 1c  RCTF{We1l_d0n3_.
00000030: 75 74 5f 77 68 34 74 5f 69 35 5f 2a 6b 65 79 00  ut_wh4t_i5_*key.
00000040: 7d 0a 20 20 20 20 20 20 20 20 20 20 20 20 20 20  }.              
00000050: 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20                  
00000060: 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20                  
00000070: 20 20 20 20 20 20 20 20 20 20 52 4f 49 53                  ROIS
调整最后一列：00^2a=2a, 1c^2a=36='6'.
在 https://www.cmd5.com 查询 21582C6C30BE1217322CDB9AEBAF4A73 得到“that”
RCTF{We1l_d0n3_6ut_wh4t_i5_that}
