https://www.cnblogs.com/coming1890/p/13498762.html
https://blog.csdn.net/dchua123/article/details/105185194

题目给出 9e45191069704531accd66f1ee1d5b2b.pdf，使用 ROT13 进行转换
$ file 9e*2b.pdf
9e45191069704531accd66f1ee1d5b2b.pdf: data
$ cat 9e*2b.pdf | tr A-Za-z N-ZA-Mn-za-m >/tmp/a.pdf

方法1：
$ pdfimages -all /tmp/a.pdf /tmp/b
$ file /tmp/b-*
/tmp/b-000.jpg: JPEG image data, baseline, precision 8, 690x469, components 3
/tmp/b-001.jpg: JPEG image data, baseline, precision 8, 690x469, components 3
$ xdg-open /tmp/b-000.jpg

方法2：
$ seektail.py /tmp/a.pdf
00008EEB: jpeg   (FF D9 Tail) (FF D8 FF: 06CA)
0002D9D1: gif89a (00 3B Tail) (47 49 46 38 39 61: None)
0002D9D1: gif87a (00 3B Tail) (47 49 46 38 37 61: None)
$ python -c 'print(0x06ca, 0x8eeb+2-0x06ca)'
1738 34851
$ dd bs=1 skip=1738 count=34851 if=/tmp/a.pdf of=/tmp/a.jpeg
$ xdg-open /tmp/a.jpeg

最终得到：
BITSCTF{save_the_kid}
