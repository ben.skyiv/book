https://ddaa.tw/bctf_crypto_200_special_rsa.html

难度系数：8.0
题目描述：在学习RSA算法时，我发现了一种和RSA具有同等安全性的算法。
对msg.txt加密得到了msg.enc。
$ python special_rsa.py enc msg.txt msg.enc
你能从flag.enc中恢复flag.txt么？

BCTF{q0000000000b3333333333-ju57-w0n-pwn20wn!!!!!!!!!!!!}
