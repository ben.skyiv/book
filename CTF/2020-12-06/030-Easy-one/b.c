#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
	if (argc != 2) { printf("[Error1]\n"); return 0; }
	FILE* input  = fopen(argv[1], "rb");
	if (!input) { printf("[Error2]\n"); return 0; }
	char k[] = "VeryLongKeyYouWillNeverGuess";
  int len = strlen(k);
	char c, z, t = 0;
	int i = 0;
	while ((z = fgetc(input)) != EOF) {
    int p = 0x0;
    for (; p < 0x7f; p++) {
		  c = (p + (k[i % len] ^ t) + i*i) & 0xff;
      if (c != z) continue;
      fputc(p, stdout);
      break;
    }
    if (p == 0x7f) { printf("[Error3]\n"); return 0; }
		t = p;
		i++;
	}
	printf("\n");
	return 0;
}
