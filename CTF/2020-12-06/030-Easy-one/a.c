#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
	if (argc != 2)  { printf("[Error1]\n"); return 0; }
	FILE* input  = fopen(argv[1], "rb");
	if (!input)  { printf("[Error2]\n"); return 0; }
	char w[] = "Hi! This is only test message\n";
	char c, p, t = 0;
	for (int i = 0; w[i]; i++) {
    p = fgetc(input);
    int k = 0x20;
    for (; k < 0x7f; k++) {
		  c = (w[i] + (k ^ t) + i*i) & 0xff;
      if (c != p) continue;
      fputc(k, stdout);
      break;
    }
    if (k == 0x7f) { printf("[Error3]\n"); return 0; }
		t = w[i];
	}
	printf("\n");
	return 0;
}
