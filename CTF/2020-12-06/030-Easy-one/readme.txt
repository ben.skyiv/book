https://blog.csdn.net/zz_Caleb/article/details/89575430

题目来源：Hack-you-2014
题目描述：破解密文，解密msg002.enc文件

$ gcc a.c && ./a.out msg001.enc
VeryLongKeyYouWillNeverGuessVe
$ gcc b.c && ./a.out msg002.enc 
The known-plaintext attack (KPA) is an attack model for cryptanalysis where the attacker has samples of both the plaintext (called a crib), and its encrypted version (ciphertext). These can be used to reveal further secret information such as secret keys and code books. The term "crib" originated at Bletchley Park, the British World War II decryption operation. 
The flag is CTF{6d5eba48508efb13dc87220879306619}
