from gmpy2 import invert
import rsa

p=273821108020968288372911424519201044333
q =280385007186315115828483000867559983517
e = 65537

n = p * q
d = int(invert(e, (p-1)*(q-1)))

key = rsa.PrivateKey(n,e,d,p,q)
c = open("RSA256/fllllllag.txt",'rb').read()
flag = rsa.decrypt(c,key)
print(flag)
