// mcs affine.cs ~/src/book/lib/ModPow.cs
using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class Affine
{
  static readonly Dictionary<char, byte> dict = new Dictionary<char, byte>();
  static readonly string t = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
  static readonly int m = t.Length;

  static Affine() { for (var i = 0; i < m; i++) dict.Add(t[i], (byte)i); }

  static void Main()
  {
    var s = Console.ReadLine();
    int a = 13, b = 4, d = a.ModInv(m);
    foreach (var c in s) Console.Write(t[d * (m + dict[c] - b) % m]);
    Console.WriteLine();
  }
}
