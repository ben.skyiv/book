题目来源：BITSCTF-2017
题目：MZYVMIWLGBL7CIJOGJQVOA3IN5BLYC3NHI
答案格式：BITSCTF{...}

本题使用了仿射密码和 bases32 (A-Z2-7=)

仿射密码是一种表单代换密码，
字母表的每个字母相应的值
使用一个简单的数学函数对应一个数值，
再把对应数值转换成字母。

加密函数：E(x) = (ax + b) (mod m)
解密函数：D(x) = a^{-1}(x - b) (mod m)
其中 a 与 m 互素

0123456789.123456789.123456789.1
ABCDEFGHIJKLMNOPQRSTUVWXYZ234567

由于题目中只含大写英文字母和数字 2-7，
所以猜测是 base32 编码，
直接进行 base32 解码，失败！
由于答案格式是 BITSCIT{...}，所以：
$ echo -n BITSCTF | base32
IJEVIU2DKRDA====
MZYVMIWLGBL7CIJOGJQVOA3IN5BLYC3NHI
由于
两个 I 都对应到 M，
两个 D 都对应到 L，
所以猜测是仿射密码。

I -> M 即 8 -> 12
J -> Z 即 9 -> 25

12 = 8a + b
25 = 9a + b
后者减前者得：a = 13
代入前者  得：b = 12 - 8a = -92 = 4 (mod 32)
验证一下 E -> Y，正确：
13*4 + 4 = 56 = 24 (mod 32) 

则密文进行仿射解密得：
$ python affine.py < a.txt  # 或者
$ mcs affine.cs ~/src/book/lib/ModPow.cs && ./affine.exe < a.txt
IJEVIU2DKRDHWUZSKZ4VSMTUN5RDEWTNPU
$ echo -n IJEVIU2DKRDHWUZSKZ4VSMTUN5RDEWTNPU====== | base32 -d; echo
BITSCTF{S2VyY2tob2Zm}
