from gmpy2 import invert
t = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'
a, b, m = 13, 4, len(t)
d = invert(a, m)
print(''.join(t[d * (m + t.index(c) - b) % m] for c in input()))
