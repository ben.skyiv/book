bitsctf-2017 beginners-luck

$ xortool -c 00 BITSCTFfullhd.png
$ cd xortool_out
$ file 0.out 
0.out: PNG image data, 1920 x 1080, 8-bit/color RGB, non-interlaced
$ mv 0.out a.png
$ xdg-open a.png
BITSCTF{p_en_gee}

https://github.com/hellman/xortool
