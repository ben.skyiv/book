from Crypto.PublicKey import RSA
from gmpy2 import invert
import rsa

f = open("pubkey.pem","rb").read()
pub = RSA.importKey(f)
n = pub.n
e = pub.e

p = 275127860351348928173285174381581152299
q = 319576316814478949870590164193048041239
d = int(invert(e, (p-1)*(q-1)))

key = rsa.PrivateKey(n,e,d,p,q)
c = open("flag.enc",'rb').read()
flag = rsa.decrypt(c,key)
print(flag)
