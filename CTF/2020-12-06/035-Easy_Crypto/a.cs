using System;
using System.IO;
using System.Text;
using System.Linq;

static class EasyCrypto
{
  static void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }
  
  static void Main()
  {
    var key = Encoding.UTF8.GetBytes("hello world");
    var count = 256;
    var s = Enumerable.Range(0, count).Select(i => (byte)i).ToArray();
    var t = Enumerable.Range(0, count).Select(i => key[i % key.Length]).ToArray();
    for (int j = 0, i = 0; i < count; i++) {
      j = (j + s[i] + t[i]) % count;
      Swap(ref s[i], ref s[j]);
    }
    byte[] flag;
    using (var br = new BinaryReader(File.OpenRead("enc.txt")))
      flag = br.ReadBytes(1024);
    for (int i = 0, j = 0, m = 0; m < flag.Length; m++) {
      i = (i + 1) % 256;
      j = (j + s[i]) % 256;
      Swap(ref s[i], ref s[j]);
      var x = (s[i] + s[j] % 256) % 256;
      flag[m] ^= s[x];
    }
    Console.WriteLine(Encoding.UTF8.GetString(flag));
  }
}
