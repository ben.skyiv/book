j = 0
s = list(range(256))
key = "hello world"
for i in range(256):
	j = (j + s[i] + ord(key[i % len(key)])) % 256
	s[i],s[j] = s[j],s[i]
strings = ""
i = j = 0 
flag = open('enc.txt','r',encoding='ISO-8859-1').read()
for m in flag:
	i = (i + 1) % 256
	j = (j + s[i]) % 256
	s[i],s[j] = s[j],s[i]
	x = (s[i] + (s[j] % 256)) % 256
	strings += chr(ord(m) ^ s[x])
print(strings)
