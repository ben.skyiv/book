https://blog.csdn.net/qq_42967398/article/details/91346318

题目来源： 高校网络信息安全运维挑战赛
题目描述：小明在密码学课上新学了一种加密算法，你能帮他看看么

$ python a.py
EIS{55a0a84f86a6ad40006f014619577ad3}
$ mcs a.cs && ./a.exe 
EIS{55a0a84f86a6ad40006f014619577ad3}
