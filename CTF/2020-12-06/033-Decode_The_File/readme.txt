https://www.cnblogs.com/coming1890/p/13540370.html

033. Decode The File

利用 base64 的填充传递信息

$ python2 a.py
ROIS{base_GA_caN_b3_d1ffeR3nT}

或者

$ mcs base64padding.cs
$ ./base64padding.exe < 1a351e90fb2b476a929d1e2666d7c511 | bindecoder.exe; echo
ROIS{base_GA_caN_b3_d1ffeR3nT}
