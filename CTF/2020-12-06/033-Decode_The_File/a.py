from string import uppercase,lowercase,digits

def solve(filename):
    with open(filename,'r') as f:
        codes=f.read()
    Lc=codes.split('\n')[:-1]
    base=uppercase+lowercase+digits+'+/'
    re2=[]
    for code in Lc:
        if '==' in code:
            re2.append(bin(base.find(code[-3]))[2:].rjust(6,'0')[2:])
        elif '=' in code:
            re2.append(bin(base.find(code[-2]))[2:].rjust(6,'0')[4:])
    ret=''.join(re2)
    return hex(long(ret[:ret.rfind('1')+1],2))[2:-1].decode('hex') # right 0 error

if __name__=='__main__':
    print solve('1a351e90fb2b476a929d1e2666d7c511')
