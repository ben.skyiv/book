using System;
using System.Text;

static class Base64Padding
{
  static readonly string b64 =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
    "abcdefghijklmnopqrstuvwxyz" +
    "0123456789+/";

  static string GetPadding(string s, int a, int b)
  {
    var i = b64.IndexOf(s[s.Length - a]);
    if (i < 0) throw new Exception("Error1");
    var t = Convert.ToString(i, 2).PadLeft(6, '0').Substring(b);
    return t + "|";
  }
  
  static StringBuilder TrimEndZero(StringBuilder sb)
  {
    for (; ; ) {
      if      (sb.ToString().EndsWith("0000|")) sb.Length -= 5;
      else if (sb.ToString().EndsWith(  "00|")) sb.Length -= 3;
      else break;
    }
    return sb;
  }

  static void Main()
  {
    string s; var sb = new StringBuilder();
    while ((s = Console.ReadLine()) != null)
      if (s.EndsWith("==")) sb.Append(GetPadding(s, 3, 2));
      else if (s.EndsWith("=")) sb.Append(GetPadding(s, 2, 4));
    s = TrimEndZero(sb).ToString().Replace("|", "");
    if (s.Length % 8 != 0) throw new Exception("Error2");
    Console.WriteLine(s);
  }
}
