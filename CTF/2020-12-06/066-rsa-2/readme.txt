http://mslc.ctf.su/wp/0ctf-2016-quals-rsa-crypto-2-pts/

难度系数：8.0
题目来源：0CTF-2016
题目描述：It seems easy, right?
Tip: openssl rsautl -encrypt -in FLAG -inkey public.pem -pubin -out flag.enc

$ ~/git/RsaCtfTool/RsaCtfTool.py --publickey public.pem --dumpkey
n: 23292710978670380403641273270002884747060006568046290011918413375473934024039715180540887338067
e: 3
$ yafu
>> factor(232...067)
P32 = 26440615366395242196516853423447
P32 = 27038194053540661979045656526063
P32 = 32581479300404876772405716877547
查看该题的 Writeup 得：
0ctf{HahA!Thi5_1s_n0T_rSa~}
