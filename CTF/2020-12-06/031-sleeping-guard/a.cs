using System;
using System.IO;
using System.Text;
using System.Linq;

static class A
{
  static byte[] GetBytes(string s)
  {
    if (s.Length % 2 != 0) throw new Exception("s.Length must be even number");
    var bs = new byte[s.Length / 2];
    for (var i = 0; i < bs.Length; i++)
      bs[i] = Convert.ToByte(s.Substring(2 * i, 2), 16);
    return bs;
  }
  
  static byte[] GetMask()
  {
    var hdr = GetBytes("89504e470d0a1a0a0000000d49484452"); // png header
    Console.WriteLine(BitConverter.ToString(hdr));
    using (var br = new BinaryReader(File.OpenRead("data"))) {
      var bs = br.ReadBytes(hdr.Length);
      for (var i = 0; i < bs.Length; i++) hdr[i] ^= bs[i];
    }
    Console.WriteLine(BitConverter.ToString(hdr));
    var key = Encoding.ASCII.GetString(hdr);
    Console.WriteLine(key);
    var len = 12;
    key = key.Substring(0, len);
    Console.WriteLine(key);
    var mask = hdr.Take(len).ToArray();
    Console.WriteLine(BitConverter.ToString(mask));
    return mask;
  }
  
  static void Write(byte[] mask)
  {
    var len = 0;
    using (var br = new BinaryReader(File.OpenRead("data")))
    using (var bw = new BinaryWriter(File.OpenWrite("a.png"))) {
      for (; ; ) {
        var bs = br.ReadBytes(mask.Length);
        len += bs.Length;
        for (var i = 0; i < bs.Length; i++)
          bw.Write((byte)(mask[i] ^ bs[i]));
        if (bs.Length < mask.Length) break;
      }
    }
    Console.WriteLine("Write: {0} bytes", len);
  }

  static void Main()
  {
    var mask = GetMask();
    Write(mask);
  }
}
