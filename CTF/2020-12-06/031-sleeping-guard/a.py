from base64 import b64decode
from binascii import unhexlify
from Crypto.Util.strxor import strxor

def enc(data,key):
    key=(key*(len(data)/len(key)+1))[:len(data)]
    return strxor(data,key)

def solve(data):
    head=unhexlify('89504e470d0a1a0a0000000d49484452')
    key=strxor(head,data[:16])
    with open('sleeping-guard.png','wb') as f:
        f.write(enc(data,key[:12]))

if __name__=='__main__':
    with open('data','rb') as f:
        solve(f.read())
