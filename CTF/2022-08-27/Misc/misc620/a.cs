using System;
using System.Text;

static class Tester
{
  static void Main()
  {
    var bs = Encoding.Unicode.GetBytes(Console.ReadLine());
    for (var i = 0; i < bs.Length; i += 2)
    {  var t = bs[i]; bs[i] = bs[i+1]; bs[i+1] = t; }
    Console.WriteLine(Encoding.GetEncoding("GB2312").GetString(bs));
  }
}
