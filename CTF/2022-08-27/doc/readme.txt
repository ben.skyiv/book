https://www.wangdingcup.com
2022年第三届“网鼎杯”网络安全大赛
福建分行网鼎分队/T1005781, U5729281
token：icq0b31a57f6c542748e7ad607501138
2022-08-27 9:00--17:00
共16道题，解出1道（签到题）。

01. Misc/签到，      1644支队伍攻克，  7pt
02. Misc/misc620,     600支队伍攻克， 17pt
03. Misc/misc830，     39支队伍攻克，173pt
04. Misc/misc034,      29支队伍攻克，209pt
05. Crypto/crypto582，354支队伍攻克， 27pt
06. Crypto/crypto581， 60支队伍攻克，127pt
07. Crypto/crypto583，  0支队伍攻克，500pt
08. PWN/pwn982,        68支队伍攻克，115pt
09. PWN/pwn577，        5支队伍攻克，417pt
10. PWN/pwn947,         0支队伍攻克，500pt
11. Reverse/re790，    57支队伍攻克，132pt
12. Reverse/re911，    25支队伍攻克，228pt
13. Reverse/re031，     2支队伍攻克，477pt
14. Web/web923,       251支队伍攻克， 38pt
15. Web/web925,        67支队伍攻克，117pt
16. Web/web262,        62支队伍攻克，124pt

https://blog.csdn.net/weixin_45770420/article/details/126554769
