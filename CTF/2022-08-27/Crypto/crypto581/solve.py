from crypt import crypt
from gmpy2 import *
from functools import reduce

def crack_unknown_increment(states, modulus, multiplier):
    increment = (states[1] - states[0]*multiplier) % modulus
    return modulus, multiplier, increment

def crack_unknown_multiplier(states, modulus):
    multiplier = (states[2] - states[1]) * invert(states[1] - states[0], modulus) % modulus
    return crack_unknown_increment(states, modulus, multiplier)

def crack_unknown_modulus(states):
    diffs = [s1 - s0 for s0, s1 in zip(states, states[1:])]
    zeroes = [t2*t0 - t1*t1 for t0, t1, t2 in zip(diffs, diffs[1:], diffs[2:])]
    modulus = abs(reduce(gcd, zeroes))
    return crack_unknown_multiplier(states, modulus)

s=[150532854791355748039117763516755705063,
335246949167877025932432065299887980427,
186623163520020374273300614035532913241,
215621842477244010690624570814660992556,
220694532805562822940506614120520015819,
17868778653481346517880312348382129728,
160572327041397126918110376968541265339]
n,a,b=crack_unknown_modulus(s)
c = 114514
e = int(2e8)

mod=10 ** 10000

M=matrix(Zmod(mod),[[b,a*c,1],[0,c,0],[0,0,1]])
C=matrix(Zmod(mod),[[1,1,n]])
C=C.T
s=(M^e)*C
sol = str(s[0])[1:10001]
from hashlib import md5
enc=b'UUV\x04H\x01T\x01P\x03\t\x04\t\x1fW\x00T\x02LRSPT\x1d\x02\x02^\x01N[\\R\x02\tSV\x07\x06P\x01QK'
sol_md5 = md5(sol.encode()).hexdigest()
print(len(enc))
print(len(sol_md5))
from Crypto.Util.strxor import *
print(strxor((2*sol_md5.encode())[:42],enc))
