from math import gcd
import hashlib

def compute_m1_m2(xy, c12):
  m12 = (xy - e) % m
  if c12 != pow(m+m12, e, m*m12): m12 += m
  assert c12 == pow(m+m12, e, m*m12)
  return m12

with open('test1.py', 'r') as f:
  for i in range(3): f.readline()
  e = int(f.readline().split('=')[1].strip())
  for i in range(23): f.readline()
  c1 = int(f.readline().split('=')[1].strip())
  c2 = int(f.readline().split('=')[1].strip())
  f.readline()
  x = int(f.readline().split('=')[1].strip())
  y = int(f.readline().split('=')[1].strip())
m  = gcd((x - e) ** e - c1, (y - e) ** e - c2)
m1 = compute_m1_m2(x, c1)
m2 = compute_m1_m2(y, c2)
flag = m + m1 + m2
flag = hashlib.md5(str(flag).encode('utf-8')).hexdigest()
print("flag{" + flag + "}")
