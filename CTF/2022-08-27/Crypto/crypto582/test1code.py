from Crypto.Util.number import getPrime
import hashlib

e = 2022
m = getPrime(512)
m1 = getPrime(512)
m2 = getPrime(512)
flag = m + m1 + m2
flag = hashlib.md5(str(flag).encode('utf-8')).hexdigest()

c1 = pow(m+m1,e,m*m1)
c2 = pow(m+m2,e,m*m2)
c3 = pow(m1+m2,e,m1*m2)

x = pow(m1+2022,m,m*m1)
y = pow(m2+2022,m,m*m2)
z = pow(m+2022,m1,m*m1)

print('c1 =',c1)
print('c2 =',c2)
print('c3 =',c3)
print('x =',x)
print('y =',y)
print('z =',z)


'''
c1 =   851394343292721235190941842862760703...687 # 308 digits
c2 =  1045548083807216458400322693365795490...144 # 309 digits
c3 =   947716258454491288120813452912189733...330 # 308 digits
x =    782373294083519554659270928059950769...765 # 308 digits
y =   1004421666336323196334944505954181676...029 # 309 digits
z =   1047126619859001157500116287272709345...452 # 309 digits
'''
