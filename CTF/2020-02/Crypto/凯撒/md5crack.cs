static class Md5crack
{
  static void Main()
  {
    var s = "e57b9e18b08bff0d05a3c59900b10";
    var t = "742ea8152ff11b1f6f9314c3c4eff93e";
    using (var h = System.Security.Cryptography.MD5.Create())
    for (var i = 0; i <= 0xfff; i++) {
      var a = "flag{" + s + i.ToString("x3") + "}";
      if (System.BitConverter.ToString(h.ComputeHash
        (System.Text.Encoding.UTF8.GetBytes(a)))
        .Replace("-", "").ToLower() == t)
        System.Console.WriteLine(a);
    }
  }
}
