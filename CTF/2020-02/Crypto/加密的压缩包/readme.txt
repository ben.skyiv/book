Crypot/加密的压缩包
加密的压缩包，提交flag格式。flag{xxx}
flag.zip
----------------------------------
$ 7z l -slt flag.zip | grep CRC
CRC = 4B10DEBA
CRC = 1FD8A07A
CRC = E7F7E18C
CRC = 88940FDE

$ csc crackcrc32.cs ~/src/apps/lib/Crc32.cs
$ date; time mono crackcrc32.exe 4B10DEBA
$ date; time mono crackcrc32.exe 1FD8A07A
$ date; time mono crackcrc32.exe E7F7E18C

$ csc reversecrc32.cs ~/src/book/lib/Crc32.cs
$ mono reversecrc32.exe 4B10DEBA
$ mono reversecrc32.exe 1FD8A07A
$ mono reversecrc32.exe E7F7E18C

https://www.ituring.com.cn/article/509561
计算 CRC32 的逆函数
我们把明文存储在一个长度为 6 的 byte 数组中，
该数组的前 2 个字节遍历所有可能的明文字符，
然后根据给定的 CRC32 值（以及该数组的前 2 个字节）计算逆函数，
填入该数组的后 4 个字节，然后判断它们是否在明文字符范围内。

reversecrc32.cs 的 Main 函数的内循环
bs[0] = i; bs[1] = j; bs.ReverseCrc32(z, 2); 给定前  2个字节，填充后  4个字节
bs[0] = i; bs[5] = j; bs.ReverseCrc32(z, 1); 给定头尾2个字节，填充中间4个字节
bs[4] = i; bs[5] = j; bs.ReverseCrc32(z, 0); 给定后  2个字节，填充前  4个字节

https://github.com/theonlypwner/crc32
$ python crc32.py reverse '0x4B10DEBA'
$ python crc32.py reverse '0x1FD8A07A'
$ python crc32.py reverse '0xE7F7E18C'

$ 7z x -pWe1c0meT0CTF flag.zip
$ cat flag.txt
flag{592b7e16bb42d046e1e85fecb9c9e6e5}

1.txt: 4B10DEBA 6hour6min
50-61-73-73-69-73 Passis *

2.txt: 1FD8A07A 5hour33min
57-65-31-63-30-6D We1c0m *

3.txt: E7F7E18C 5hour2min
65-54-30-43-54-46 eT0CTF *
