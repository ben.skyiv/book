using System;
using System.Text;
using System.Collections.Generic;
using Skyiv.Utils;

static class CrackCrc32
{
  static uint z;
  static byte[] a;
  static byte[] bytes;
  static Encoding ascii;

  static CrackCrc32()
  {
    var s = new List<byte>();
    for (var i = '0'; i <= '9'; i++) s.Add((byte)i);
    for (var i = 'A'; i <= 'Z'; i++) s.Add((byte)i);
    for (var i = 'a'; i <= 'z'; i++) s.Add((byte)i);
    s.Add((byte)'+');
    s.Add((byte)'/');
    s.Add((byte)'=');
    a = s.ToArray();
    bytes = new byte[6];
    ascii = new ASCIIEncoding();
  }

  static void Make(int i)
  {
    if (i < 0) {
      if (z == bytes.GetCrc32())
        Console.WriteLine("{0} {1}",
          BitConverter.ToString(bytes),
          ascii.GetString(bytes));
      return;
    }
    foreach (var j in a) {
      bytes[i] = (byte)j;
      Make(i - 1);
    }
  }

  static void Main(string[] args)
  {
    z = Convert.ToUInt32(args[0], 16);
    Console.WriteLine("Crack Crc32 (Base64): {0:X}", z);
    Make(bytes.Length - 1);
  }
}
