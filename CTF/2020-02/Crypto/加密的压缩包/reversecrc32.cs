using System;
using System.Text;
using System.Collections.Generic;
using Skyiv.Utils;

static class ReverseCrc32
{
  static readonly Encoding ascii = new ASCIIEncoding();
  static readonly HashSet<byte> a = new HashSet<byte>();

  static ReverseCrc32()
  {
    for (var i = '0'; i <= '9'; i++) a.Add((byte)i);
    for (var i = 'A'; i <= 'Z'; i++) a.Add((byte)i);
    for (var i = 'a'; i <= 'z'; i++) a.Add((byte)i);
    a.Add((byte)'+'); a.Add((byte)'/'); a.Add((byte)'=');
  }

  static bool Valid(byte[] bs)
  {
    foreach (var b in bs) if (!a.Contains(b)) return false;
    return true;
  }

  static void Main(string[] args)
  {
    var z = Convert.ToUInt32(args[0], 16);
    var bs = new byte[6];
    foreach (var i in a)
      foreach (var j in a) {
        bs[0] = i; bs[5] = j;
        bs.ReverseCrc32(z, 1);
        if (Valid(bs)) Console.WriteLine("{0}", ascii.GetString(bs));
      }
  }
}
