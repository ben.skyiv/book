Crypto/一般难度的加解密
豆豆说这是一般难度的RSA了，你能解密成功吗？提交flag格式：flag{xxxx}。
hint1: 617
hint2: http://factordb.com/
-------------------------------
题目给定 n,e,c
一般来说，n=p*q，其中p和q都是素数。
但是本题中n是617位的素数。
RSA算法的本质是 m^e = c (mod n)
计算出明文  m = c^(1/e) (mod n)
$ gp -q rsa1.gp | tee /tmp/a.txt
313330...373039 (184 digits)
我们知道 0x30:'0',0x31:'1',0x32:'2',...,0x39:'9'，
所以先按十六进制解码得到一个十进制数，再转换为字符串：
$ cat solve.py
import libnum
with open('/tmp/a.txt', 'r') as f:
  m = int(f.readline(), 16)
print(libnum.n2s(int(libnum.n2s(m))))
$ python solve.py
b'flag{630b1953a612a8392af021393e36538b}'
