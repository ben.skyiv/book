using System;

static class Base64Decode
{
  static void Main()
  {
    foreach (var s in Console.In.ReadToEnd().Trim().Split('\n')) {
      var outputb = Convert.FromBase64String(s);
      var t = System.Text.Encoding.Default.GetString(outputb);
      if (t.StartsWith("flag")) Console.WriteLine(t);
    }
  }
}
