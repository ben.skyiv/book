Misc/流量分析
$ 7z x 1.zip 得到 1.pacpng
用 Wireshark 打开 1.pacpng
文件 -> 导出对象 -> HTTP
在弹出的“Wireshark·导出·HTTP 对象列表”
对话框的“文本过滤器”文本框中输入“zip”，
选中 application/zip: 666.zip
点击 Save 按钮
得到的 666.zip 是伪加密的压缩文件
$ zipclean.exe 666.zip
00000000: 50-4B-03-04 14-00 09-00
00001476: 50-4B-01-02 3F-00 14-00 00-00
000014D1: 50-4B-01-02 3F-00 14-00 00-00
0000152C: 50-4B-01-02 3F-00 14-00 00-00
00001587: 50-4B-01-02 3F-00 14-00 00-00
000015E2: 50-4B-01-02 3F-00 14-00 00-00
0000163D: 50-4B-01-02 3F-00 14-00 00-00
00001698: 50-4B-01-02 3F-00 14-00 00-00
/tmp/a.zip: 5,892 bytes
$ 7z x /tmp/a.zip
得到 666 目录下的 1.txt, 2.txt, ..., 6.txt
$ cat 666/?.txt > a.txt     # 合并为 a.txt
$ hexstr2bytes.exe < a.txt  # 转换为二进制文件
/tmp/a: 8,627 bytes
$ file /tmp/a               # 该文件是二维码图像
/tmp/a: PNG image data, 300 x 300, 8-bit/color RGBA, non-interlaced
$ zbarimg /tmp/a            # 识别该二维码得到 flag
QR-Code:flag{3819169573b7a37786d2ea39c6daef76}
