用 archpr 暴力破解，或者
用 zip2john 暴力破解（字母数据的混合）
$ zip2john zip > a.txt
ver 2.0 zip/flag.txt PKZIP Encr: cmplen=52, decmplen=38, crc=7E065163
$ john a.txt
...
d01nt            (zip/flag.txt)
...
$ 7z x -pd01nt zip
$ cat flag.txt; echo
flag{e3dc96f2cdcae4c4c72f5a6a0f00782a}
