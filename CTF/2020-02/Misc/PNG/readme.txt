$ 7z x timg.zip
$ pngcheck.exe timg.png
CRC(computed): EEE0D765
CRC(expected): 0EBD0637
2560x1500x8, 6 0 0 0
00-00-0A-00-00-00-07-D0
$ pngcrc32.py timg.png
timg.png.png : 2560 x 2000
$ file *.png
timg.png:     PNG image data, 2560 x 1500, 8-bit/color RGBA, non-interlaced
timg.png.png: PNG image data, 2560 x 2000, 8-bit/color RGBA, non-interlaced
打开 timg.png.png 得到 flag：
flag{4de1b95e68be64ad43d3959c7f4b0505}
