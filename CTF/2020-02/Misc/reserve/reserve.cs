using System;
using System.IO;

static class Reserve
{
  static void Main()
  {
    var bs = File.ReadAllBytes("flag.png");
    Array.Reverse(bs);
    File.WriteAllBytes("answer.png", bs);
  }
}
