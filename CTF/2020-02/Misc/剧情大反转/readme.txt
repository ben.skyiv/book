Misc/剧情大反转
$ 7z x esrever.zip          # 得到 esrever.png
$ strings -100 esrever.png  # 包含倒序的16进制字符串
$ strings -100 esrever.png | rev | hexstr2bytes.exe
/tmp/a: 26,908 bytes
$ file /tmp/a  # 伪加密的压缩文件
/tmp/a: Zip archive data, at least v2.0 to extract, compression method=deflate
$ zipclean.exe /tmp/a
00000000: 50-4B-03-04 14-00 09-00
000068AC: 50-4B-01-02 3F-00 14-00 00-00
/tmp/a.zip: 26,908 bytes
$ 7z x /tmp/a.zip    # 得到 flag.png
$ xdg-open flag.png  # 上下巅倒的 flag
flag{99f0fd18f89e6b73f05c139b0a9e4c98}
