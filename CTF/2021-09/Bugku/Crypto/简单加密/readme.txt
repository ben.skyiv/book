简单加密 Crypto
分数: 20 金币: 1
题目作者: harry
一　　血: H3rmesk1t
一血奖励: 1金币
解　　决: 1049
提　　示: key{}
描　　述: e6Z9i~]8R~U~QHE{RnY{QXg~QnQ{^XVlRXlp^XI5Q6Q6SKY8jUAA 
--------------------------------------------------------------
$ mcs string_shift.cs
$ mono string_shift.exe -4 < input.txt
a2V5ezY4NzQzMDAwNjUwMTczMjMwZTRhNThlZTE1M2M2OGU4fQ==
$ mono string_shift.exe -4 < input.txt | base64 -d; echo
key{68743000650173230e4a58ee153c68e8}
