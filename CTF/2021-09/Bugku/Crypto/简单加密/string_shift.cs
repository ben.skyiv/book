using System;
using System.Text;

static class StringShift
{
  static void Main(string[] args)
  {
    var diff = (args.Length > 0) ? int.Parse(args[0]) : 0;
    var s = Console.ReadLine();
    var sb = new StringBuilder();
    foreach (var c in s) sb.Append((char)(c + diff));
    Console.WriteLine(sb);
  }
}
