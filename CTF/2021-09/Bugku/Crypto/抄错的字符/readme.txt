题目信息给出：“部分数字抄成字母”、“把所有字母都换成大写”。
可以解读为:
a.flag中包含数字，大写字母，小写字母
b.所有的大写字母均有可能为小写字母
c.可以找出与数字形状相近的字母：I=>1;b=>6;g=>9;Z=>2;S=>5

将 QWIHBLGZZXJSXZNVBZW
4个一组base64解码，大写、小写、数字均尝试一遍，

QWIHBLGZZXJSXZNVBZW

QW1h base64解码为 Ama
BL92 base64解码为 n_v
ZXJ5 base64解码为 ery
X2Nv base64解码为 _co
b2w= base64解码为 ol

由描述知给出的字符串中的每个字符原来可能是大小写字母或数字，
应该先暴力枚举，并且优先级：数字 > 小写字母 > 大写字母，
然后将枚举出来的字符进行解密。

$ python a.py
Aman_very_cool
flag{Aman_very_cool}
