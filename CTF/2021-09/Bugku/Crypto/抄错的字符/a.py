import base64
s = "QWIHBLGZZXJSXZNVBZW="
dic = {'I': '1', 'B': '6', 'S':'5','G': '9','Z':'2'}
res = ''

def check(s):
    f = True
    s = str(s)
    for i in range(len(s)):
        if ((s[i] in list('_+=')) or s[i].isdigit() or s[i].isalpha()):
            continue
        else:
            f = False
            break
    return f


def strcon_decode(s,i):
    global res
    if(i==4):
        ss = ''.join(s)
        sss = base64.b64decode(ss)
        sss = str(sss)[2:-1]
        if check(sss):
            # print(ss+' decode: ' + sss)
            ss = res + sss
            res = ss
            return True
        else:
            return False
    else:
        if s[i] in dic.keys():
            ss = s[i]
            s[i] = dic[s[i]]
            f = strcon_decode(s,i+1)
            s[i] = ss
            if f:
                return True
        s[i] = s[i].lower()
        f = strcon_decode(s,i+1)
        s[i] = s[i].upper()
        if f:
            return f
        return strcon_decode(s,i+1)
        
        
for i in range((len(s)//4)):
    ss = s[i*4:(i+1)*4]
#     print(ss+' decode: ')
    ss = list(ss)
    strcon_decode(ss,0)

print(res)
