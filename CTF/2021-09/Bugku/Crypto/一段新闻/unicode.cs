using System;
using System.Text;
using System.Globalization;
using System.Collections.Generic;

static class Test
{
  static void Main()
  {
    var bs = new List<byte>();
    foreach (var s in Console.In.ReadToEnd().Trim().Substring(1).Split('\\')) {
      if (s.Length != 5) throw new Exception("s.Length != 5");
      if (s[0] != 'u') throw new Exception("s[0] != 'u'");
      bs.Add(byte.Parse(s.Substring(3, 2), NumberStyles.HexNumber));
      bs.Add(byte.Parse(s.Substring(1, 2), NumberStyles.HexNumber));
    }
    Console.WriteLine(Encoding.Unicode.GetString(bs.ToArray()));
  }
}
