using System;
using System.Collections.Generic;

static class Morse
{
  static readonly Dictionary<string, char> dict;

  static readonly string[] digits = {
    //   0        1       2         3        4
    "11111", "01111", "00111", "00011", "00001",
    //   5        6        7        8        9
    "00000", "10000", "11000", "11100", "11110"
  };
  static readonly string[] letters = {
    // a      b       c      d    e       f      g       h     i       j
    "01", "1000", "1010", "100", "0", "0010", "110", "0000", "00", "0111",
    // k       l     m     n      o       p       q      r      s    t
    "101", "0100", "11", "10", "111", "0110", "1101", "010", "000", "1",
    // u       v      w       x       y       z
    "001", "0001", "011", "1001", "1011", "1100"
  };

  static Morse()
  {
    dict = new Dictionary<string, char>();
    dict.Add("01101", '\\');
    for (var i = 0; i < digits.Length; i++) dict.Add(digits[i], (char)('0' + i));
    for (var i = 0; i < letters.Length; i++) dict.Add(letters[i], (char)('a' + i));
  }
  
  static char GetChar(string s)
  {
    char c;
    if (!dict.TryGetValue(s, out c)) c = '?';
    return c;
  }

  static void Main()
  {
    foreach (var s in Console.In.ReadToEnd().Split()) Console.Write(GetChar(s));
    Console.WriteLine();
  }
}
