using System;

static class Test
{
  static void Main()
  {
    int a3 = 8203, a4 = 8204, a5 = 8205;
    var s = Console.In.ReadToEnd();
    var n = 0;
    foreach (var c in s) {
      if (c < a3 || c > a5) continue;
      Console.Write((c == a3) ? ((++n % 6 == 0) ? '\n' : ' ') : (c == a4) ? '0' : '1');
    }
  }
}
