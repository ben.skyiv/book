using System;
using System.Text;

static class Test
{
  static Encoding[] encodes = {
    //Encoding.UTF8,
    Encoding.Unicode,
    //Encoding.GetEncoding("GB18030")
  };

  static void Main()
  {
    foreach (var encode in encodes) {
      foreach (var c in "公正友善诚信") {
        var bs = encode.GetBytes(c.ToString());
        foreach (var b in bs) Console.Write("{0,2} ", Convert.ToString(b, 16));
        // foreach (var b in bs) Console.Write("{0,8} ", Convert.ToString(b, 2));
        Console.Write("{0}| ", c);
      }
      Console.WriteLine(" {0}", encode);
    }
  }
}
