https://ctf.bugku.com/challenges/detail/id/286.html

Bugku -> Crypto -> 一段新闻
分数: 20 金币: 2
题目作者: STck0vERfl0W
一　　血: FDgege
一血奖励: 2金币
解　　决: 225
描　　述: flag{} 
-----------------------
网上的解答都是基于在线工具的，我这里给出离线工具：
$ zerowidth.exe  < 一段新闻.txt | morse.exe | unicode.exe | python core-value.py
flag{why_d0nt_you_try_t0_understand_socia1ism?}

这题目考核以下方面的能力：
1. 零宽字符（不可见字符）
2. 摩尔斯码
3. Unicode 码（UTF16）
4. 社会主义核心价值观编码

如果需要看中间结果，也可以分步进行：
$ mcs zerowidth.cs && ./zerowidth.exe  < 一段新闻.txt > c1.txt
$ mcs morse.cs && ./morse.exe < c1.txt > c2.txt
$ mcs unicode.cs && ./unicode.exe < c2.txt > c3.txt
$ python core-value.py < c3.txt

http://www.atoolbox.net/Tool.php?Id=829
https://zhuanlan.zhihu.com/p/75992161
https://github.com/sym233/core-values-encoder

0x8203 / 零宽空格
0x8204 . 零宽不连字
0x8205 - 零宽连字
