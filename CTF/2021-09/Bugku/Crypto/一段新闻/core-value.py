import sys
from itertools import islice

VALUES = '富强民主文明和谐自由平等公正法治爱国敬业诚信友善'

def str2utf8(Str):
    utfStr = ''.join([i.encode('utf-8').hex().upper() for i in Str])
    return utfStr

def utf82str(utfStr):
    Str = bytearray.fromhex(utfStr).decode('utf-8')
    return Str

def hex2duo(hexStr):
    duo = []
    for h in hexStr:
        numH = int(h, 16)
        if numH < 10:
            duo.append(numH)
        elif random.random() < 0.5:
            duo.append(10)
            duo.append(numH - 10)
        else:
            duo.append(11)
            duo.append(numH - 6)
    return duo

def duo2hex(duo):
    hexList = []
    if duo[-1] >= 10:
        duo = duo[:-1]
    lit = iter(enumerate(duo))
    for i, d in lit:
        if d < 10:
            hexList.append('{:X}'.format(d))
        elif d == 10:
            hexList.append('{:X}'.format(duo[i + 1] + 10))
            next(islice(lit, 1, 1), None)
        else:
            hexList.append('{:X}'.format(duo[i + 1] + 6))
            next(islice(lit, 1, 1), None)
    hexStr = ''.join(hexList)
    return hexStr

def duo2values(duo):
    value = ''.join([VALUES[2 * i] + VALUES[2 * i + 1] for i in duo])
    return value

def values2duo(value):
    duo = []
    pureValue = [v for v in value if v in VALUES]
    for i, v in enumerate(pureValue[::2]):
        index = VALUES.index(v)
        if index % 2 == 0:
            duo.append(index // 2)
    return duo

def valueEncode(s):
    return duo2values(hex2duo(str2utf8(s)))

def valueDecode(value):
    return utf82str(duo2hex(values2duo(value)))

s = sys.stdin.read()
print(valueDecode(s))
