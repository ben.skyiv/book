快递员在快递上记录信息用 IPhone 拍下来了，但是手机出现问题，
恢复的文件内容不正常，你能帮他找出他记录的信息么？
是不是高度和宽度有问题呢？Flag格式flag{xxx}。
附件：1.JPG
------------------------------------------
$ cp 1.JPG 2.jpg
$ file 2.jpg                 宽度 x 高度
2.jpg: JPEG image data, ..., 2447 x 2496, components 3
$ bless 2.jpg                098F x 09C0
查找 0xffc0（jpg 文件图片数据的开始位置），
之后第4个字节开始是高度和宽度（各2字节）
修改高度：09 C0 -> 0B C0
FF C0 00 11 08 09 C0 09 8F
----- ----- -- 高度- 宽度-
$ file 2.jpg
2.jpg: JPEG image data, ..., 2447 x 3008, components 3
$ xdg-open 2.jpg
得到：flag{64e2844233108bce70d4e214e1f99636}

~/pub/Misc-doc/JPEG文件格式.html：
https://www.cnblogs.com/Dreaming-in-Gottingen/p/14285605.html
