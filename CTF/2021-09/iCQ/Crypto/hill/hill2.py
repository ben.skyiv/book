from numpy import array, matrix, linalg, around
from gmpy2 import invert
from sys import argv


def decode(txt, mat):
    n, t = len(txt) // nc, array([dic[c] for c in txt])
    t = array(matrix(t.reshape(n, nc)) * mat).reshape(n * nc)
    return ''.join(bet[c % nb] for c in t)


mat = matrix(array([
    [54, 53, 28, 20, 54, 15, 12,  7],
    [32, 14, 24,  5, 63, 12, 50, 52],
    [63, 59, 40, 18, 55, 33, 17,  3],
    [63, 34,  5,  4, 56, 10, 53, 16],
    [35, 43, 45, 53, 12, 42, 35, 37],
    [20, 59, 42, 10, 46, 56, 12, 61],
    [26, 39, 27, 59, 44, 54, 23, 56],
    [32, 31, 56, 47, 31,  2, 29, 41]])).T
bet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789_{}"
dic, nb, nc = {}, len(bet), mat.shape[1]
for i in range(len(bet)): dic[bet[i]] = i
det = int(around(linalg.det(mat)))
inv = around(det * mat.I).astype(int) * invert(det, nb)
print(decode(input(), mat if len(argv) > 1 else inv))
