from numpy import array, matrix, linalg, around
from gmpy2 import invert
from sys import argv


def decode(txt, mat):
    n, t = len(txt) // nc, array([dic[c] for c in txt])
    t = array(matrix(t.reshape(n, nc)) * mat).reshape(n * nc)
    return ''.join(bet[c % nb] for c in t)


mat = matrix(array([[1, 2, 3], [4, 5, 6], [7, 8, 10]]))
bet = "abcdefghijklmnopqrstuvwxyz"
dic, nb, nc = {}, len(bet), mat.shape[1]
for i in range(len(bet)): dic[bet[i]] = i
det = int(around(linalg.det(mat)))
inv = around(det * mat.I).astype(int) * invert(det, nb)
print(decode(input(), mat if len(argv) > 1 else inv))
