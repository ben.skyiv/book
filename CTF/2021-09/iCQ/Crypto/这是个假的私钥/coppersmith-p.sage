with open('/tmp/a') as fp:
    fp.readline()
    n = int(fp.readline().split()[0], 16)
    for _ in range(3): fp.readline()
    s = fp.readline().split()
pbar, pbits, kbits = int(s[0], 16), int(s[1]), int(s[2])
PR.<x> = PolynomialRing(Zmod(n))
f = x + pbar
# find root < 2^kbits with factor >= n^0.3
x0 = f.small_roots(X=2^kbits, beta=0.4)[0]
p = int(x0 + pbar)
print(hex(p))
assert n % p == 0
