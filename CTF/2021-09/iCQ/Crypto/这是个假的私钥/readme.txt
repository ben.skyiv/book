第三届“百越杯”福建省高校网络空间安全大赛
分值：200分
类型：Crypto
题目名称：这是个假的私钥
--------------------------------------------------
$ unzip rsa_d8d8a0ba8a4a0d069296ba05126e63d7.zip
$ rsa_key_info.py < privatekey.pem | tee /tmp/a
在这个假的私钥中，n: 2048 bits，e = 0x10001，跟公钥一致
而 q: 1024 bits，但低 384 bits 被置 0
其他参数（d, p, dp, dq, invq）都被置 0
于是，使用 LLL-acttack 恢复 q
$ sage coppersmith-p.sage > /tmp/p
$ python step-2.py
b'...flag{Nes5un0_puo_emend4rs1_d4l_p3ccato_che_sc0rre_nele_vene}'

https://github.com/mimoo/RSA-and-LLL-attacks
~/git/RSA-and-LLL-attacks/coppersmith.sage
public.pem：base64
n: 0xd12b639df759a99c9adb57500bbd..76ff5967 (256 bytes)
e: 0x010001                                 (  3 bytes)

privatekey.pem：base64
privatekey.dat        | file length: 1190 bytes
000b: 00 D1 ... 59 67 | 257 bytes: n
010e: 01 00 01        |   3 bytes: e
029c: 00 EE ... 80 5D |  81 bytes: q
02ed: 00 00 ... 00 00 |  48 bytes: q
