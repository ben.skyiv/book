from libnum import n2s, s2n
from base64 import b64decode
from gmpy2 import invert

with open('/tmp/a') as fp:
    fp.readline()
    n = int(fp.readline().split()[0], 16)
    e = int(fp.readline().split()[0], 16)
with open('/tmp/p') as fp:
    p = int(fp.readline(), 16)
with open('rsacrypto.py') as fp:
    for _ in range(11):
        fp.readline()
    c = s2n(b64decode(fp.readline().split()[2]))
q = n // p
phi = (p-1)*(q-1)
d = invert(e, phi)
m = pow(c, d, n)
print(n2s(int(m)))
