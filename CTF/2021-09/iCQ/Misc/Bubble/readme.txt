2017第二届广东省强网杯线上赛
分值：50分
类型：Misc
题目名称：Bubble
------------------------------------------
$ 7z x ubble_716df0cb9e80e548269f85808ae6fb69.zip
$ cat flag.enc ;echo
xinik-samak-luvag-hutaf-fysil-notok-mepek-vanyh-zipef-hilok-detok-damif-cusol-fezyx
$ python bubble.py 
b'flag{Ev3ry7hing_i5_bubb13s}'
这是 Bubble 加密算法，见
https://www.cnblogs.com/mke2fs/p/10616588.html
