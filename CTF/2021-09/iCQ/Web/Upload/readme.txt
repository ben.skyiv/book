iCQ, Web, Upload
第一届“百度杯”信息安全攻防总决赛 线上选拔赛
分值：50分 类型：Web题目名称：Upload
题目内容：来来来，都是套路，贼简单。
---------------------------------------
打开网页后显示：Hi,CTFer!u should be a fast man:)
网页源代码是：
</br>Hi,CTFer!u should be a fast man:)<!-- Please post the ichunqiu what you find -->
用 Postman 打开：
其中 Headers 中有：
flag: ZmxhZ19pc19oZXJlOiBOVEF5T1RNeg==
$ echo -n ZmxhZ19pc19oZXJlOiBOVEF5T1RNeg== | base64 -d; echo
flag_is_here: NTAyOTMz
$ echo -n NTAyOTMz | base64 -d; echo
502933

再次 Send 又变成：
flag: ZmxhZ19pc19oZXJlOiBOVFEwTnprMQ==
用 base64 解码：flag_is_here: NTQ0Nzk1
再次用 base64 解码：544795

写个 Python 程序：
$ ptyhon post6.py
Path:3712901a08bb58557943ca31f3487b7d
然后访问这个路径，进入一个登陆界面：
...
（后面的步骤略去）
