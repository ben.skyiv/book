i春秋-CTF工具箱，Web，p_Test001
题目描述：tst
题目类型：Web
题目分值：500分
-----------------------------
打开网页，自动跳转到“http://m.simplexue.com/”，显示：
呃…找不到此网站。

用 Burp Suite，在 Respone 中找到：
<dt>看格式的话，这个图片好像是GIF的
<a target="_blank" href="./gif.gif" style="color: red">解题链接</a></p></dt>
用以下命令下载：
$ wget http://eci-2ze4kt5q9u5rvqr8tuag.cloudeci1.ichunqiu.com/gif.gif
$ bless gif.gif
添加文件头“GIF8”。
$ gifspliter.py gif.gif
Total: 8
$ xdg-open z00.png
得到：
PASSWORD
is
Y2F0Y2hfd
Gh1X2R5bm
FtaWNfZm
xhZ19pc19
xdW10ZV9z
aW1wbGU=

$ echo -n Y2F0Y2hfdGh1X2R5bmFtaWNfZmxhZ19pc19xdW10ZV9zaW1wbGU= | base64 -d;echo
catch_thu_dynamic_flag_is_qumte_simple

在浏览器地址栏输入：
http://eci-2ze4kt5q9u5rvqr8tuag.cloudeci1.ichunqiu.com/ctf/submitkey
得到：
看格式的话，这个图片好像是GIF的 解题链接
key：
[  ] <提交>
在对话框中输入“catch_thu_dynamic_flag_is_qumte_simple”，
点击<提交>按钮，得到：
404 Not Found

在浏览器地址栏输入：
http://eci-2ze4kt5q9u5rvqr8tuag.cloudeci1.ichunqiu.com/me.php
得到：
你不是我的主人

