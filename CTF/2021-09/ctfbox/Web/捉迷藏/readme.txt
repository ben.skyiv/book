i春秋-CTF工具箱，Web，捉迷藏
题目描述：找呀找呀找flag，你在哪里呢？登录题目地址，得到密码
题目类型：Web
题目分值：10分
--------------------------------
打开网页，得到一幅捉迷藏的图画，下端写着：
找呀找呀找flag，你在哪里呢？

查看源代码，得到：
<script language="JavaScript" type="text/javascript"> 
eval(function(p,a,c,k,e,d){e=function(c){return c};if(!''.replace(/^/,String)){while(c--){d[c]=k[c]||c}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('1.2="3=0";',4,4,'|document|cookie|check'.split('|'),0,{}))
</script>
使用 Burp Suite 得到 Request 的 Cookie：
Cookie: pgv_pvi=1414577152;...;check=0;...

