i春秋-CTF工具箱，Web，回旋13踢
题目描述：看我回旋13踢~~答案格式：flag{xxxxxx}
题目类型：Web
题目分值：10分
----------------------------------------
打开网页，得到：
看我回旋13踢~~
xw4n73qm23z
[           ]
用 ROT13 解码：
$ echo -n xw4n73qm23z | tr A-Za-z N-ZA-Mn-za-m;echo
kj4a73dz23m
输入后回车，得到：
flag{efaaef5d-19c4-43ab-b806-1d250771b88c}
提交答案，正确！
