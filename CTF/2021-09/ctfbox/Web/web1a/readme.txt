i春秋-CTF工具箱，Web，web1
题目描述：find the flag.
题目类型：Web
题目分值：60分
-------------------------------
打开网页，得到：
flag在哪里？
查看源代码，得到：
<a style="display:none" href="There_is_no_flag_here.php"></a>
访问该页面，还是：
flag在哪里？
但是查看源代码，得到：
flag在哪里？<!-- 该页面只允许本地访问。 -->
用 Burp Suite，增加以下 Request Header：
Client-ip: 127.0.0.1
再次 Send，得到：
flag{864a2a35-a6c6-4041-b597-b449de3c5142}
提交答案，正确！
