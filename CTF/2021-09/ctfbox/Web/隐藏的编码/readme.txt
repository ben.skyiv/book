i春秋-CTF工具箱，Web，隐藏的编码
题目描述：请用你的火眼金睛找出这段密文中所包含的密码，
我相信你行的！答案格式：flag{xxxxxx}
题目类型：Web
题目分值：10分
---------------------------------
打开网页，得到：
请用你的火眼金睛找出这段密文中所包含的密码，我相信你行的！
adslkjadsl a ,zmxnc,zmc,zxcopaweqwk;l;l;l;l;l;l;l;l;l;l;l;ldkm,ZGY1czRh==.zxmczxkchzxkhiqewupipsad;l;l;l;l;l;l;l;l;l;l;l;l
请输入密码:[             ]
---------------------------------
在文本框中随便输入，得到：回答错误!
在 Response 的 Headers 中发现以下内容，不知有没有用处。
X-Via-JSL: 2273f52,-

adslkjadsl a ,zmxnc,zmc,zxcopaweqwk;l;l;l;l;l;l;l;l;l;l;l;ldkm,
ZGY1czRh==.zxmczxkchzxkhiqewupipsad;l;l;l;l;l;l;l;l;l;l;l;l
