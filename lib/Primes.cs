using System;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public static class Primes
  {
    public static int[] GetPrimes(int low, int high, bool hasSentinel = false)
    {
      var primes = new List<int>();
      var composite = GetCompositeArray(high);
      for (var i = 2; i < composite.Length; i++)
        if (!composite[i] && i >= low) primes.Add(i);
      if (hasSentinel) primes.Add(high + 1);
      return primes.ToArray();
    }

    public static bool[] GetCompositeArray(int max)
    {
      int limit = (int)Math.Sqrt(max);
      var composite = new bool[max + 1];
      for (int i = 2; i <= limit; i++)
        if (!composite[i])
          for (int j = i * i; j <= max; j += i)
            composite[j] = true;
      return composite;
    }
    
    public static bool IsPrime(this ulong n)
    {
      if (n < 2) return false;
      if (n == 2 || n == 3) return true;
      if (n % 2 == 0 || n % 3 == 0) return false;
      for (ulong i = 6, i9 = 1 + (ulong)Math.Sqrt(n); i <= i9; i += 6)
        if (n % (i-1) == 0 || n % (i+1) == 0) return false;
      return true;
    }
  }
}

