namespace Skyiv.Utils
{
  public static class ModPowExtensions
  {
    public static long Pow(this long a, long b)
    {
      long v = 1;
      for (long c = a; b != 0; b >>= 1, c *= c)
        if ((b & 1) != 0) v = v * c;
      return v;
    }

    public static int ModPow(this int a, int b, int m)
    {
      long v = 1;
      for (long c = a; b != 0; b >>= 1, c = c * c % m)
        if ((b & 1) != 0) v = v * c % m;
      return (int)v;
    }

    public static int ModInv(this int a0, int m)
    {
      long z = 1, a = a0;
      for (long c, q, x, y = 0, b = m; b != 0; z = y, y = x)
      { c = a - (q = a/b) * b; a = b; b = c; x = z - q*y; }
      return (int)((z < 0) ? (z+m) : z);
    }
  }
}

