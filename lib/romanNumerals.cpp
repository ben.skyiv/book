#include <string>

std::string to_roman(int n)
{
  std::string s = "";
  for (; n >= 1000; n -= 1000) s += 'M';
  if      (n / 100 == 9) s += "CM";
  else if (n / 100 == 4) s += "CD";
  else {
    if (n >= 500) s += 'D';
    for (int i = n / 100 % 5; i > 0; i--) s += 'C';
  }
  if ((n %= 100) / 10 == 9) s += "XC";
  else if     (n / 10 == 4) s += "XL";
  else {
    if (n >= 50) s += 'L';
    for (int i = n / 10 % 5; i > 0; i--) s += 'X';
  }
  if ((n %= 10) == 9) s += "IX";
  else if    (n == 4) s += "IV";
  else { 
    if (n >= 5) s += 'V';
    for (int i = n % 5; i > 0; i--) s += 'I';
  }
  return s;
}

int from_roman(std::string s)
{
  int z = 0; char c = '0';
  for (int i = 0; i < s.size(); c = s[i++])
    switch(s[i]) {
    case 'M': z += (c == 'C') ? 800 : 1000; break;
    case 'D': z += (c == 'C') ? 300 :  500; break;
    case 'C': z += (c == 'X') ?  80 :  100; break;
    case 'L': z += (c == 'X') ?  30 :   50; break;
    case 'X': z += (c == 'I') ?   8 :   10; break;
    case 'V': z += (c == 'I') ?   3 :    5; break;
    case 'I': z++;                          break;
    }
  return z;
}

