using System;
using System.Linq;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public sealed class MultisetPermutationCount
  {
    int[] ns;
    
    public MultisetPermutationCount(int[] ns) { this.ns = ns.Where(x => x > 0).ToArray(); }
  
    public long All()
    {
      if (ns.Length < 2) return 1;
      Array.Sort(ns);
      long v1 = 1, v2 = 1;
      for (int n = ns.Sum(), i = 0; i < ns.Length - 1; i++)
        for (var j = ns[i]; j > 0; j--, n--)
        { v1 *= n; v2 *= j; }
      return v1 / v2;
    }

    public long Value(int k)
    {
      var n = ns.Sum();
      if (k > n || k < 0) return 0;
      if (k == 0 || n == 0) return 1;
      if (k == 1) return ns.Length;
      if (n == k) return All();
      var a = From0To(k);
      return a[a.Length - 1];
    }
    
    public long[] From0To(int k)
    {
      var n = ns.Sum();
      if (k > n || k < 0) return Enumerable.Empty<long>().ToArray();
      if (k == 0 || n == 0) return Enumerable.Repeat(1L, 1).ToArray();
      if (k == 1) return  new long[] { 1, ns.Length };
      var mset = GetMultiset(ns);
      var b = Coefficient.Func(0, k);
      foreach (var kvp in mset)
        b = Coefficient.Product(b, Coefficient.Power(kvp.Key, kvp.Value, k), k);
      return b.Select(x => x.Coeff).ToArray();
    }

    Dictionary<int, int> GetMultiset(int[] ns)
    {
      var mset = new Dictionary<int, int>();
      for (int k, i = 0; i < ns.Length; mset[ns[i++]] = k + 1)
        mset.TryGetValue(ns[i], out k);
      return mset;
    }
  }
  
  struct Coefficient
  {
    public long Coeff { get; private set; }
    int index;
    
    void Add(Coefficient a, Coefficient b)
    { // this += a * b
      Coeff += a.Coeff * b.Coeff * (a.index + b.index).Combination(a.index);
    }
    
    void Set(long coeff, int index) { Coeff = coeff; this.index = index; }
    
    public static Coefficient[] Func(int b, int k)
    {
      var a = new Coefficient[k + 1];
      var i = 0;
      for (; i <= b && i <= k; i++) a[i].Set(1, i);
      for (; i <= k; i++) a[i].Set(0, i);
      return a;
    }
    
    public static Coefficient[] Product(Coefficient[] a, Coefficient[] b, int k)
    { // return: a * b
      var v = Coefficient.Func(-1, k);
      for (var i = 0; i < a.Length; i++)
        for (var j = 0; j < b.Length; j++)
          if (i + j <= k) v[i + j].Add(a[i], b[j]);
      return v;
    }
    
    public static Coefficient[] Power(int b, int e, int k)
    { // return: (1+x+...+x^b/b!)^e, items:k, b >= 1, e >= 1, k > 1
      var a = Coefficient.Func(b, k);
      if (e == 1) return a;
      var v = Coefficient.Func(0, k); // TODO: use multinomial theorem.
      for (; e > 0; e /= 2, a = Product(a, a, k))
        if (e % 2 != 0) v = Product(v, a, k);
      return v;
    }
  }
}

