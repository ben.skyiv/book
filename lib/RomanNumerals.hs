module Skyiv.RomanNumerals where
import Data.Map ( (!),fromList )
toRoman = concat . (map ((fromList $ zip z s)!)) . f where
  z = [1000,900,500,400,100,90,50,40,10,9,5,4,1]
  s = ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"]
  f 0 = []; f n = let m = head $ dropWhile (>n) z in m : f (n-m)
fromRoman = sum . f . (map ((fromList $
  zip "MDCLXVI" [1000,500,100,50,10,5,1])!)) where
  f [] = []; f [x] = [x]; f (a:b:c) | a < b = (b-a) : f c | True = a : f (b:c)

