namespace Skyiv.Numerics
{
  using System;

  public struct Quadratic : IEquatable<Quadratic>, IComparable<Quadratic>
  { // (b+c√d)/a, a > 0, d > 0, d is square-free, d = 1 when c = 0, a = 1 when b,c = 0.
    Int32 a, b, c, d;
    static Int32 Gcd(Int32 a, Int32 b) { return (b == 0) ? a : Gcd(b, a % b); }
    public static implicit operator Quadratic(Int32 b) { return new Quadratic(1,b,0,1); }
    public static bool operator ==(Quadratic x, Quadratic y) { return x.Equals(y); }
    public static bool operator !=(Quadratic x, Quadratic y) { return !x.Equals(y); }
    public static bool operator <(Quadratic x, Quadratic y) { return x.CompareTo(y) < 0; }
    public static bool operator >(Quadratic x, Quadratic y) { return x.CompareTo(y) > 0; }
    public static bool operator <=(Quadratic x, Quadratic y) { return x.CompareTo(y) <= 0; }
    public static bool operator >=(Quadratic x, Quadratic y) { return x.CompareTo(y) >= 0; }
    public bool Equals(Quadratic other) { return CompareTo(other) == 0; }

    public override int GetHashCode()
    { return a.GetHashCode() ^ b.GetHashCode() ^ c.GetHashCode() ^ d.GetHashCode(); }

    Quadratic(Int32 a, Int32 b, Int32 c, Int32 d) : this()
    {
      var gcd = Math.Abs(Gcd(Gcd(a, b), c)); a /= gcd; b /= gcd; c /= gcd;
      this.a = a; this.b = b; this.c = c; this.d = d;
    }

    public static Quadratic From(Int32 a, Int32 b, Int32 c, Int32 d)
    {
      if (a == 0) throw new DivideByZeroException();
      if (a < 0) { a = -a; b = -b; c = -c; }
      if (d < 0) throw new ArgumentException("d must >= 0");
      if (d == 0) c = 0;
      if (c == 0) { d = 1; if (b == 0) a = 1; }
      for (Int32 i = 2, j = i*i; d >= j; ++i, j = i*i)
        for (; d % j == 0; d /= j) c *= i;
      return new Quadratic(a, b, c, d);
    }

    public static Quadratic operator +(Quadratic x, Quadratic y)
    {
      if (x.c != 0 && y.c != 0 && x.d != y.d) throw new ArgumentException();
      return new Quadratic(x.a*y.a,x.b*y.a+y.b*x.a,x.c*y.a+y.c*x.a,(x.c==0)?y.d:x.d);
    }

    public static Quadratic operator -(Quadratic x, Quadratic y)
    {
      if (x.c != 0 && y.c != 0 && x.d != y.d) throw new ArgumentException();
      return new Quadratic(x.a*y.a,x.b*y.a-y.b*x.a,x.c*y.a-y.c*x.a,(x.c==0)?y.d:x.d);
    }
    
    public int CompareTo(Quadratic other)
    {
      if (c != 0 && other.c != 0 && d != other.d) throw new ArgumentException();
      var u = b * other.a - other.b * a;
      var v = c * other.a - other.c * a;
      var w = (c == 0) ? other.d : d;
      if (v == 0) return u.CompareTo(0);
      if (u == 0) return v.CompareTo(0);
      if (u > 0 && v > 0) return 1;
      if (u < 0 && v < 0) return -1;
      var gcd = Math.Abs(Gcd(u, v)); u /= gcd; v /= gcd;
      return ((u > 0) ? 1 : -1) * ((long)u*u - (long)v*v*w).CompareTo(0);
    }

    public override bool Equals(object other)
    {
      if (other == null || GetType() != other.GetType()) return false;
      return Equals((Quadratic)other);
    }

    public override string ToString()
    {
      if (a == 1) return (c == 0) ? b.ToString() : (b == 0) ?
        string.Format("{0}√{1}", c, d) :
        string.Format("{0}{3}{1}√{2}", b, Math.Abs(c), d, (c>=0)?'+':'-');
      return (c == 0) ?
        string.Format("{1}/{0}", a, b) : (b == 0) ?
        string.Format("{1}√{2}/{0}", a, c, d) :
        string.Format("({1}{4}{2}√{3})/{0}",a,b,Math.Abs(c),d,(c>=0)?'+':'-');
    }
  }
}

