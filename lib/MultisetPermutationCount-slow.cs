using System;
using System.Linq;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public sealed class MultisetPermutationCount
  {
    static readonly ArrayEqualityComparer comparer = new ArrayEqualityComparer();
    
    int[] ns;
    
    public MultisetPermutationCount(int[] ns) { this.ns = ns.Where(x => x > 0).ToArray(); }
  
    public long All()
    {
      if (ns.Length < 2) return 1;
      Array.Sort(ns);
      long v1 = 1, v2 = 1;
      for (int n = ns.Sum(), i = 0; i < ns.Length - 1; i++)
        for (var j = ns[i]; j > 0; j--, n--)
        { v1 *= n; v2 *= j; }
      return v1 / v2;
    }

    public long Value(int k)
    { // This method is slower.
      var n = ns.Sum();
      if (k > n) return 0;
      if (k == 0 || n == 0) return 1;
      if (k == 1) return ns.Length;
      if (n == k) return All();
      long sum = 0;
      foreach (var a in GetPermutationWorkSubset(ns, n - k))
        sum += a.Permutation();
      return sum;
    }
    
    public long[] From0To(int k)
    {
      var a = new long[k + 1];
      for (var i = 0; i <= k; i++) a[i] = Value(i);
      return a;
    }
    
    HashSet<int[]> GetPermutationWorkSubset(int[] ns, int m)
    {
      var set1 = new HashSet<int[]>(comparer);
      var set2 = new HashSet<int[]>(comparer);
      var len = ns.Length;
      for (set1.Add(ns); m > 0; set1.Clear(), Extensions.Swap(ref set1, ref set2), m--)
        foreach (var a in set1)
          for (var i = 0; i < len; i++)
          {
            if (a[i] == 0) continue;
            var b = a.Dup();
            b[i]--;
            set2.Add(b);
          }
      return set1;
    }

    sealed class ArrayEqualityComparer : EqualityComparer<int[]>
    {
      public override bool Equals(int[] a, int[] b)
	    {
	      if (a.Length != b.Length) return false;
	      for (var i = 0; i < a.Length; i++)
	        if (a[i] != b[i]) return false;
	      return true;
	    }
	    
      public override int GetHashCode(int[] a)
      {
        var v = 0;
        for (var i = 0; i < a.Length; i++) v ^= a[i];
        return v;
      }  
    }
  }
}

