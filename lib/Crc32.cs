using System;
using System.Linq;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public static class Crc32
  {
    static readonly uint[] t1 = new uint[256];
    static readonly uint[] t2 = new uint[256];

    static Crc32()
    {
      uint p = 0xEDB88320;
      for (uint f, r, j, i = 0; i < t1.Length; t1[i] = f, t2[i] = r, i++)
        for (f = i, r = i << 24, j = 8; j > 0; j--) {
          f = ((f & 1) == 0) ? (f >> 1) : ((f >> 1) ^ p);
          r = ((r & 0x80000000) == 0) ? (r << 1) : (((r ^ p) << 1) | 1);
        }
    }

    public static void ReverseCrc32(this byte[] bs, uint crc, int n)
    {
      if (bs.Length < n + 4) return;
      uint z = 0xFFFFFFFF;
      for (int i = 0; i < n; i++) z = (z >> 8) ^ t1[(z ^ bs[i]) & 0xFF];
      Array.Copy(BitConverter.GetBytes(z), 0, bs, n, 4);
      z = crc ^ 0xFFFFFFFF;
      for (int i = bs.Length-1; i >= n; i--) z = (z<<8)^t2[z>>24]^bs[i];
      Array.Copy(BitConverter.GetBytes(z), 0, bs, n, 4);
    }

    public static uint GetCrc32<T>(this IEnumerable<T> bs)
    {
      return ~bs.Aggregate(0xFFFFFFFF, (r, b) =>
        (t1[(r & 0xFF) ^ Convert.ToByte(b)] ^ (r >> 8)));
    }
  }
}
