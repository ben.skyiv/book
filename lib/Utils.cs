using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public static class Extensions
  {
    public static T[] Dup<T>(this T[] a) { return (T[])a.Clone(); }
    public static T[] Tail<T>(this T[] a) { return a.Skip(1).ToArray(); }
    public static void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }
    //public static long Combination(this int[] ns) { return Permutation(ns); }

    // 返回 date 位于一年的第几周，可选参数 firstDayOfWeek 用于指定一周的第一天是星期几
    public static int GetWeekOfYear(this DateTime date, DayOfWeek firstDayOfWeek = DayOfWeek.Monday)
    {
      int x = (int)(new DateTime(date.Year, 1, 1).DayOfWeek);
      x += 8 - (int)firstDayOfWeek;
      x %= 7;
      if (x == 0) x = 7;
      return (date.DayOfYear + x + 5) / 7;
    }

    public static string ToArrayString<T>(this T[] a)
    {
      if (a == null) return "(null)";
      var sb = new StringBuilder("[");
      for (var i = 0; i < a.Length; i++) sb.Append(a[i] + " ");
      sb[sb.Length - 1] = ']';
      return sb.ToString();
    }
    
    public static long Combination(this int n, int k)
    {
      if (k > n / 2) k = n - k;
      long v1 = 1, v2 = 1;
      for (; k > 0; k--, n--) { v1 *= n; v2 *= k; }
      return v1 / v2;
    }
    
    public static long Permutation(this int n, int k)
    {
      long v = 1;
      for (; k > 0; k--, n--) v *= n;
      return v;
    }
    
    /*public static long Permutation(this int[] ns)
    {
      return new MultisetPermutationCount(ns).All();
    }
    
    public static long Permutation(this int[] ns, int k)
    {
      return new MultisetPermutationCount(ns).Value(k);
    }
    
    public static long[] PermutationFrom0To(this int[] ns, int k)
    {
      return new MultisetPermutationCount(ns).From0To(k);
    }*/
  }
}

