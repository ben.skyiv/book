// TAOCP vol.3 Chn, Ex 5.2.3-15 Answer, p.504.
using System;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public static class Primes
  {
    class Rev : IComparer<Tuple<long,long,long>>
    {
      public int Compare(Tuple<long,long,long> a, Tuple<long,long,long> b)
      {
        long x = a.Item1, y = b.Item1;
        return (x > y) ? (-1) : 1; // assert: x != y
      }
    }

    static public IEnumerable<long> GetPrimes(long max)
    {
      if (max < 2) yield break;
      var pq = new PriorityQueue<Tuple<long, long, long>>(new Rev());
      var ps = new List<long>();
      var m9 = 9 + (long)Math.Sqrt(max);
      ps.Add(2); yield return 2;
      if (max < 3) yield break;
      ps.Add(3); yield return 3;
      int d = 2; long n = 5, t = 25;
      pq.Push(Tuple.Create(t, 10L, 30L));
      for (long q; n <= max; n += d, d = 6 - d) {
        for (var v = pq.Pop(); ; v = pq.Pop()) {
          q = v.Item1; long q1 = v.Item2, q2 = v.Item3;
          pq.Push(Tuple.Create(q + q1, q2 - q1, q2));
          if (n <= q) break;
        }
        for (; n < q; n += d, d = 6 - d) {
           if (n > max) break;
           if (n <= m9) ps.Add(n);
           yield return n;
         }
        if (n != t) continue;
        var u = ps[pq.Count + 2];
        pq.Push(Tuple.Create(t = u * u, u << ((u % 3 == 2) ? 1 : 2), 6 * u));
      }
    }
  }
}

