using System;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public class IntRevComparer : IComparer<int>
  {
    public int Compare(int x, int y)
    {
      return (x > y) ? (-1) : (x < y) ? 1 : 0;
    }
  }
}

