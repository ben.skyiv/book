using System;

namespace Skyiv.Utils
{
  sealed class PrimeHelper
  {
    long m; int z; int[] a; long[] b;

    public PrimeHelper(long n)
    {
      a = new int[(z = (int)Math.Sqrt(m = n)) + 1];
      b = new long[z + 1];
      for (int i = 1; i <= z; a[i] = i-1, ++i) b[i] = n/i-1;
      for (int p = 2; p <= z; ++p) {
        if (a[p] <= a[p - 1]) continue;
        long d = p, q = (long)p * p;
        int c = a[p - 1], m = (int)Math.Min(z, n / q);
        for (int i = 1; i <= m; ++i, d += p)
          b[i] += c - ((d <= z) ? b[d] : a[n / d]);
        for (int i = z; i >= q; --i) a[i] += c - a[i/p];
      }
    }

    public long π(long n)
    { // 当 n > sqrt(m) 时，仅保证对 n == floor(m/i) 正确
      return (n > z) ? b[m / n] : a[n];
    }

    public int[] GetPrimes(bool hasSentinel = false)
    { // 返回不超过 sqrt(m) 的所有素数
      var primes = new int[a[z] + (hasSentinel ? 1 : 0)];
      for (int j = 0, i = 2; i <= z; ++i)
        if (a[i] > a[i - 1]) primes[j++] = i;
      if (hasSentinel) primes[a[z]] = z + 1;
      return primes;
    }
  }
}

