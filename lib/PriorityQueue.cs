using System;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public class PriorityQueue<T> where T : IComparable<T>
  {
    List<T> data;
    public PriorityQueue() { data = new List<T>(); }
    public T Peek()  { return data[0]; }

    public void Enqueue(T item)
    {
      data.Add(item);
      for (int pi, ci = data.Count - 1; ci > 0 &&
        data[ci].CompareTo(data[pi = (ci - 1) / 2]) < 0; ci = pi)
      { T tmp = data[ci]; data[ci] = data[pi]; data[pi] = tmp; }
    }

    public T Dequeue()
    {
      T item = data[0];
      var li = data.Count - 1; data[0] = data[li]; data.RemoveAt(li--);
      for (int pi = 0, ci, rc; (ci = pi * 2 + 1) <= li; pi = ci)
      {
        if ((rc = ci+1) <= li && data[rc].CompareTo(data[ci]) < 0) ci = rc;
        if (data[pi].CompareTo(data[ci]) <= 0) break;
        T tmp = data[pi]; data[pi] = data[ci]; data[ci] = tmp;
      }
      return item;
    }
  }
}

