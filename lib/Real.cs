namespace Skyiv.Numerics
{
  public struct Real : System.IEquatable<Real>, System.IComparable<Real>
  {
    double v; static readonly int R = 6;
    double V { get { return System.Math.Round(v, R); } }
    Real(double x) { v = x; }
    public static implicit operator Real(double x) { return new Real(x); }
    public static Real operator +(Real x, Real y) { return new Real(x.v + y.v); }
    public static Real operator -(Real x, Real y) { return new Real(x.v - y.v); }
    public static bool operator ==(Real x, Real y) { return x.Equals(y); }
    public static bool operator !=(Real x, Real y) { return !x.Equals(y); }
    public static bool operator <(Real x, Real y) { return x.CompareTo(y) < 0; }
    public static bool operator >(Real x, Real y) { return x.CompareTo(y) > 0; }
    public static bool operator <=(Real x, Real y) { return x.CompareTo(y) <= 0; }
    public static bool operator >=(Real x, Real y) { return x.CompareTo(y) >= 0; }
    public bool Equals(Real other) { return V == other.V; }
    public int CompareTo(Real other) { return V.CompareTo(other.V); }
    public override int GetHashCode() { return V.GetHashCode(); }
    public override string ToString() { return V.ToString("F" + 0/*R*/); }
    public override bool Equals(object other)
    {
      if (other == null || GetType() != other.GetType()) return false;
      return Equals((Real)other);
    }
  }
}

