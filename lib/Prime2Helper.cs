using System;

namespace Skyiv.Utils
{
  sealed class Prime2Helper
  {
    int n; int[] a;

    public Prime2Helper(int n)
    {
      a = new int[(this.n = n) + 1];
      for (int i = 1; i <= n; ++i) a[i] = i - 1;
      for (int p = 2; p <= n; ++p) {
        if (a[p] <= a[p - 1]) continue;
        long q = (long)p * p;
        for (int c = a[p - 1], i = n; i >= q; --i) a[i] += c - a[i/p];
      }
    }

    public long π(int n) { return a[n]; }

    public int[] GetPrimes(bool hasSentinel = false)
    { // 返回不超过 n 的所有素数
      var primes = new int[a[n] + (hasSentinel ? 1 : 0)];
      for (int j = 0, i = 2; i <= n; ++i)
        if (a[i] > a[i - 1]) primes[j++] = i;
      if (hasSentinel) primes[a[n]] = n + 1;
      return primes;
    }
  }
}

