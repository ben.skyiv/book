using System;
using System.IO;
using System.Net;

namespace Skyiv.Common
{
  public static class StringExtensions
  {
    public static Stream GetInputStream(this string fileNameOrUri, string user = null, string passwd = null)
    {
      if (!Uri.IsWellFormedUriString(fileNameOrUri, UriKind.Absolute)) return File.OpenRead(fileNameOrUri);
      var uri = new Uri(fileNameOrUri);
      if (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps) return uri.GetHttpStream();
      if (uri.Scheme == Uri.UriSchemeFtp) return uri.GetFtpStream(user, passwd);
      if (uri.Scheme == Uri.UriSchemeFile) return uri.GetFileStream();
      throw new NotSupportedException("NotSupported uri scheme: " + uri.Scheme);
    }
    
    static Stream GetFtpStream(this Uri uri, string user = null, string passwd = null)
    {
      var ftp = (FtpWebRequest)WebRequest.Create(uri);
      if (user != null) ftp.Credentials = new NetworkCredential(user, passwd);
      ftp.Method = WebRequestMethods.Ftp.DownloadFile;
      try { return ((FtpWebResponse)ftp.GetResponse()).GetResponseStream(); }
      catch (WebException ex) { throw new WebException("[" + uri + "]", ex); }
    }
    
    static Stream GetHttpStream(this Uri uri)
    {
      return ((HttpWebResponse)((HttpWebRequest)WebRequest.Create(uri)).GetResponse()).GetResponseStream();
    }

    static Stream GetFileStream(this Uri uri)
    {
      return ((FileWebResponse)((FileWebRequest)WebRequest.Create(uri)).GetResponse()).GetResponseStream();
    }
  }
}

