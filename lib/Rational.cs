namespace Skyiv.Numerics
{
  using System.Numerics;

  public struct Rational : System.IEquatable<Rational>, System.IComparable<Rational>
  {
    BigInteger denominator;
    public BigInteger Numerator { get; private set; }
    public BigInteger Denominator { get { return (Numerator == 0) ? 1 : denominator; } }
    public static implicit operator Rational(long x) { return new Rational(x, 1); }
    public bool Equals(Rational other) { return CompareTo(other) == 0; }
    public static bool operator ==(Rational x, Rational y) { return x.Equals(y); }
    public static bool operator !=(Rational x, Rational y) { return !x.Equals(y); }
    public static bool operator <(Rational x, Rational y) { return x.CompareTo(y) < 0; }
    public static bool operator >(Rational x, Rational y) { return x.CompareTo(y) > 0; }
    public static bool operator <=(Rational x, Rational y) { return x.CompareTo(y) <= 0; }
    public static bool operator >=(Rational x, Rational y) { return x.CompareTo(y) >= 0; }
    public override int GetHashCode()
    { return Denominator.GetHashCode() ^ Numerator.GetHashCode(); }

    public Rational(BigInteger numerator, BigInteger denominator) : this()
    {
      if (denominator == 0) throw new System.DivideByZeroException();
      if (denominator < 0)
      {
       denominator = -denominator;
       numerator = -numerator;
      }
      var gcd = BigInteger.GreatestCommonDivisor(numerator, denominator);
      Numerator = numerator / gcd;
      this.denominator = denominator / gcd;
    }

    public int CompareTo(Rational other)
    {
      var a = Numerator * other.Denominator;
      var b = Denominator * other.Numerator;
      if (a < b) return -1;
      if (a > b) return 1;
      return 0;
    }

    public static Rational operator +(Rational left, Rational right)
    {
      return new Rational(left.Numerator * right.Denominator
        + left.Denominator * right.Numerator,
        left.Denominator * right.Denominator);
    }

    public static Rational operator -(Rational left, Rational right)
    {
      return new Rational(left.Numerator * right.Denominator
        - left.Denominator * right.Numerator,
        left.Denominator * right.Denominator);
    }

    public override bool Equals(object other)
    {
      if (other == null || GetType() != other.GetType()) return false;
      return Equals((Rational)other);
    }

    public override string ToString()
    {
      return string.Format("{0}/{1}", Numerator, Denominator);
    }
  }
}

