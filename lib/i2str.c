#include <string.h>

char* strrev(char* s)
{
  for (int i = 0, j = strlen(s) - 1; i < j; i++, j--) {
    char t = s[i]; s[i] = s[j]; s[j] = t;
  }
  return s;
}

void i_str(char buf[], __int128 x)
{
  int i = 0, j;
  if (x < 0) { x = -x; buf[i++] = '-'; }
  for (j = i; x > 0; x /= 10) buf[i++] = (x % 10) + '0';
  buf[i] = '\0';
  strrev(buf + j);
}

char* i2str(__int128 x) { static char buf[41]; i_str(buf, x); return buf; }
