using System;
using System.Numerics;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public static class Combination
  {
    public static int Value(int n, int k, int m)
    {
      if (k > n / 2) k = n - k;
      if (k == 0) return 1;
      if (k == 1) return n % m;
      BigInteger v1 = 1, v2 = 1;
      for (; k > 0; k--, n--) { v1 *= n; v2 *= k; }
      return (int)((v1 / v2) % m);
    }

    public static IEnumerable<int[]> Generate(int n, int t)
    { // Generate {0, 1, ..., n-1} t-combination, 0 < t < n
      if (n < 2 || t < 1 || t >= n) throw new ArgumentException();
      var c = new int[t+3]; var a = new int[t];
      var j = 1; for (; j <= t; j++) c[j] = j - 1;
      c[t+1] = n; c[t+2] = 0; j = t;
      T2: Array.Copy(c, 1, a, 0, t); yield return a;
      var x = 0; if (j > 0) { x = j; goto T6; }
      if (c[1] + 1 < c[2]) { c[1]++; goto T2; } else j = 2;
      T4: c[j-1] = j - 2; x = c[j] + 1; if (x == c[j+1]) { j++; goto T4; }
      if (j > t) yield break;
      T6: c[j] = x; j--; goto T2;
    }
  }
}
