from gmpy2 import invert
from operator import mul
from functools import reduce


def chinese_remainder(modulus, remainders):
    p = reduce(mul, modulus)
    return sum(p//m*r*invert(p//m, m) for m, r in zip(modulus, remainders)) % p
