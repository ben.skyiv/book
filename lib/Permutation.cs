using System;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public static class Permutation
  {
    public static IEnumerable<int[]> Generate(int n)
    { // Generate {1, 2, ..., n} permutation
      if (n < 1) throw new ArgumentException();
      var a = new int[n]; for (var j = 0; j < n; j++) a[j] = j+1;
      C2: yield return a; var k = n - 1;
      C3: var t = a[0]; for (var j = 0; j < k; j++) a[j] = a[j+1];
      a[k] = t; if (a[k] != k+1) goto C2;
      if (--k > 0) goto C3;
    }
  }
}
