using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  public static class RomanNumerals
  {
    static readonly int[] ns = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
    static readonly Dictionary<int, string> n2r = new Dictionary<int, string>();
    static readonly Dictionary<char, int>   r2n = new Dictionary<char, int>();

    static RomanNumerals()
    {
      string[] rs = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
      string a1 = "MDCLXVI"; int[] a2 = {1000,500,100,50,10,5,1};
      for (var i = 0; i < ns.Length; i++) n2r.Add(ns[i], rs[i]);
      for (var i = 0; i < a1.Length; i++) r2n.Add(a1[i], a2[i]);
    }

    static List<int> Number2Roman(int n)
    {
      if (n == 0) return new List<int>();
      var i = 0; while (ns[i] > n) i++;
      var s = Number2Roman(n - ns[i]);
      s.Add(ns[i]); return s;
    }
    
    static List<int> Roman2Number(List<int> ns)
    {
      if (ns.Count <= 1) return ns; var b = ns[0] < ns[1];
      var s = Roman2Number(ns.Skip(b ? 2 : 1).ToList());
      s.Add(b ? (ns[1] - ns[0]) : ns[0]); return s;
    }

    public static string ToRoman(this int n)
    {
      var s = Number2Roman(n); var sb = new StringBuilder();
      for (int i = s.Count - 1; i >= 0; i--) sb.Append(n2r[s[i]]);
      return sb.ToString();
    }

    public static int FromRoman(this string s)
    {
      var ns = new List<int>();
      foreach (var c in s) ns.Add(r2n[c]);
      return Roman2Number(ns).Sum();
    }
  }
}
