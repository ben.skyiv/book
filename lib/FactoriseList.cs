using System;
using System.Collections.Generic;

namespace Skyiv.Utils
{
  static class FactoriseList
  {
    static List<int>[] GetPrimeFactors(int max)
    {
      int limit = (int)Math.Sqrt(max);
      var pfs = new List<int>[max + 1];
      for (int i = 2; i <= limit; i++)
        if (pfs[i] == null)
          for (int j = i + i; j <= max; j += i)
          {
            if (pfs[j] == null) pfs[j] = new List<int>();
            pfs[j].Add(i);
          }
      return pfs;
    }
    
    static (int, int)[] Factorise(int n, List<int> primes)
    {
      var list = new List<(int, int)>();
      if (primes != null)
      {
        foreach (var p in primes)
        {
          var k = 0;
          for (; n > 0 && n % p == 0; k++) n /= p;
          list.Add((p, k));
        }
        if (n > 1) list.Add((n, 1));
      }
      else list.Add((n, 1));
      return list.ToArray();
    }
    
    public static IEnumerable<(int,(int, int)[])> Get(int max)
    {
      var pfs = GetPrimeFactors(max);
      for (var i = 2; i < pfs.Length; i++)
        yield return (i, Factorise(i, pfs[i]));
    }
  }
}
