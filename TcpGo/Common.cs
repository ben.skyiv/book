using System;
using System.Text;
using System.Net;

namespace TcpGo
{
  public static class Common
  {
    public static readonly int Port = 9071;

    public static string GetLocalIPAddress()
    {
      var name = Dns.GetHostName();
      var addrs = Dns.GetHostAddresses(name);
      var sb = new StringBuilder();
      foreach (var addr in addrs) { sb.Append(addr); sb.Append(','); }
      if (sb.Length > 0) sb.Length--;
      return sb.ToString();
    }
  }
}
