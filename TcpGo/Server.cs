using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace TcpGo
{
  sealed class Server : ServerBase
  {
    static readonly Version ServerVersion = new Version(1, 5);

    static void Main(string[] args)
    {
      try { new Server().Run(args.Length > 0 && args[0] == "debug"); }
      catch (Exception ex) { Console.Error.WriteLine(ex.Message); }
    }

    void Run(bool debug)
    {
      Out("Server version {0,-5} on {1}:{2} ({3})", 
        ServerVersion, Common.GetLocalIPAddress(),
        Common.Port, debug ? "debug" : "product");
      var listener = new TcpListener(IPAddress.Any, Common.Port);
      listener.Start();
      var clients = new TcpClient[2];
      for (var k = 0; ; k += 2) {
        for (var i = 0; i < 2; i++) {
          clients[i] = listener.AcceptTcpClient();
          Out("{0,-20} on {1} [ID:{2}]",
            GetClientVersion(clients[i]),
            clients[i].Client.RemoteEndPoint, k + i);
        }
        for (var i = 0; i < 2; i++)
          new Thread(new ThreadStart(
          new ServerHandler(k + i, debug, clients).Run)).Start();
      }
      //listener.Stop();
    }

    static string GetClientVersion(TcpClient client)
    {
      if (!client.Connected) return "0.0";
      return new StreamReader(client.GetStream()).ReadLine();
    }
  }
}
