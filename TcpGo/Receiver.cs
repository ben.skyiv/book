using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Concurrent;

namespace TcpGo
{
  sealed class Receiver
  {
    TcpClient client;
    ConcurrentQueue<string> queue;

    public Receiver(TcpClient client, ConcurrentQueue<string> queue)
    {
      this.client = client;
      this.queue = queue;
    }

    public void Run()
    {
      var reader = new StreamReader(client.GetStream());
      try {
        while (client.Connected) {
          var s = reader.ReadLine();
          if (s == null) break;
          queue.Enqueue(s);
        }
      }
      catch (IOException) { }
    }
  }
}
