using System;

namespace TcpGo
{
  class ServerBase
  {
    static readonly string time = "yyyy-MM-dd HH:mm:ss";
    
    protected void Out(string fmt, params object[] args)
    {
      Console.Write("{0}: ", DateTime.Now.ToString(time));
      Console.WriteLine(fmt, args);
    }
  }
}
