using System;
using System.IO;
using System.Text;
using System.Net.Sockets;

namespace TcpGo
{
  sealed class ServerHandler : ServerBase
  {
    int n;
    bool debug;
    TcpClient[] clients;

    public ServerHandler(int n, bool debug, TcpClient[] clients)
    {
      this.n = n;
      this.debug = debug;
      this.clients = clients;
    }

    public void Run()
    {
      var reader = new StreamReader(clients[n % 2].GetStream());
      var writer = clients[1 - (n % 2)].GetStream();
      Out("{0}> Start", n);
      string msg = null;
      for (string s; ; ) {
          try { s = reader.ReadLine(); }
          catch (IOException) { msg = "Read: I/O Exception"; break; }
          if (s == null) { msg = "Read: End"; break; }
          if (debug) Out("{0}> {1}", n, s);
          var bs = Encoding.UTF8.GetBytes(s + "\n");
          try { writer.Write(bs, 0, bs.Length); }
          catch (IOException) { msg = "Write: I/O Exception"; break; }
      }
      Out("{0}> Exit ({1})", n, msg);
    }
  }
}
