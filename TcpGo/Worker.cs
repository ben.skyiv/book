using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;

namespace TcpGo
{
  sealed class Worker : ClientBase
  {
    static readonly string Strs =
      "protocol_version\nname\nversion\nknown_command\nlist_commands\n" +
      "quit\nboardsize\nclear_board\nkomi\nplay\ngenmove\nset_free_handicap";
    static readonly HashSet<string> Cmds = new HashSet<string>(Strs.Split());

    ulong id;
    bool hasId;
    TcpClient client;
    NetworkStream writer;
    ConcurrentQueue<string> queue;

    public Worker(TcpClient client, ConcurrentQueue<string> queue)
    {
      this.queue = queue;
      this.client = client;
      this.writer = client.GetStream();
    }

    public void Run()
    {
      try { while (client.Connected) if (!Run(Console.ReadLine())) break; }
      catch (IOException) { }
      client.Close();
    }

    bool Run(string s)
    {
      var ss = Regex.Split(s.Trim(), "\\s+");
      if (ss.Length == 0) return true;
      var t = ss[0];
      if (hasId = ulong.TryParse(t, out id))
        if (ss.Length == 1) return true;
        else t = (ss = ss.Skip(1).ToArray())[0];
      if (t.Length == 0) return true;
      else if (t == "play")              Play(ss);
      else if (t == "genmove")           Echo(Dequeue());
      else if (t == "protocol_version")  Echo("2");
      else if (t == "name")              Echo("TcpGo");
      else if (t == "version") Echo(ClientVersion.ToString());
      else if (t == "list_commands")     Echo(Strs);
      else if (t == "boardsize")         Echo("");
      else if (t == "clear_board")       Echo("");
      else if (t == "komi")              Echo("");
      else if (t == "set_free_handicap") Echo("");
      else if (t == "quit")              Echo("");
      else if (t == "known_command")     Known(ss);
      else Error("unknown command\n");
      return t != "quit";
    }

    void Known(string[] ss)
    {
      Echo((ss.Length > 1 && Cmds.Contains(ss[1])) ? "true" : "false");
    }

    void Play(string[] ss)
    {
      if (ss.Length < 3) {
        Error("invalid color or coordinate\n");
        return;
      }
      Echo("");
      Write(ss[2]);
    }

    string Dequeue()
    {
      for (string s; ; ) {
        Thread.Sleep(100); // 0.1 seconds
        if (queue.TryDequeue(out s)) return s;
      }
    }

    void Echo(bool ok, string s)
    {
      Console.WriteLine("{0}{1} {2}\n",
        ok ? '=' : '?', hasId ? id.ToString() : "", s);
    }

    void Error(string s) { Echo(false, s); }
    void Echo(string s)  { Echo(true, s); }
    void Write(string s) { Write(writer, s); }
  }
}
