using System;
using System.Threading;
using System.Net.Sockets;
using System.Collections.Concurrent;

namespace TcpGo
{
  sealed class TcpGo : ClientBase
  {
    static void Main(string[] args)
    {
      if (args.Length == 0) {
        Console.WriteLine(ClientId);
        return;
      }
      try
      {
        new TcpGo().Run(new TcpClient(args[0], Common.Port));
      }
      catch (Exception ex)
      {
        Console.Error.WriteLine("Error: {0}", ex.Message);
      }
    }

    void Run(TcpClient client)
    {
      Write(client.GetStream(), ClientId);
      var queue = new ConcurrentQueue<string>();
      new Thread(new ThreadStart(new Worker(client, queue).Run)).Start();
      new Thread(new ThreadStart(new Receiver(client, queue).Run)).Start();
    }
  }
}
