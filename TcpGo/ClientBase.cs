using System;
using System.Text;
using System.Net.Sockets;

namespace TcpGo
{
  class ClientBase
  {
    protected static readonly Version ClientVersion = new Version(1, 9);
    protected static readonly string ClientId =
      "TcpGo  version " + ClientVersion.ToString(2);

    protected void Write(NetworkStream writer, string s)
    {
      var bs = Encoding.UTF8.GetBytes(s + "\n");
      writer.Write(bs, 0, bs.Length);
    }
  }
}
