using System;
using System.Linq;

sealed class ProportionalDistribution
{
  static decimal[] Rates = {0.22m, 0.23m, 0.22m, 0.24m, 0.09m};

  static void Main(string[] args)
  {
    var p = new ProportionalDistribution();
    var n = (args.Length == 0) ? 0 : int.Parse(args[0]);
    for (var i = 0; i <= n; i++) {
      var a = p.GetNums(i, Rates);
      Console.WriteLine("{0} ({1})", string.Join(",",a), i);
    }
  }

  int[] GetNums(int n, decimal[] rates)
  {
    if (n < 0 || rates.Sum() != 1) return null;
    var a = new int[rates.Length];
    var b = new decimal[a.Length];
    for (var i = 0; i < a.Length; i++)
      b[i] = (a[i] = (int)Math.Round(rates[i]*n)) - rates[i]*n;
    var r = a.Sum() - n;
    if (r == 0) return a;
    var c = Enumerable.Range(0, a.Length).ToArray();
    Array.Sort(b, c);
    if (r < 0) for (var i = 0; i < -r; i++) a[c[i]]++;
    else for (var i = 0; i < r; i++) a[c[c.Length - 1 - i]]--;
    return a;
  }
}
