#include <stdio.h>
#include <float.h>
#include <math.h>

double mysincos(double x, double *pcx)
{
    double sx = 0, cx = 1, t = 1; int i;
    for (i = 0; ; ) {
        t *= x; t /= ++i; sx += t;
        t *= x; t /= ++i; cx -= t;
        t *= x; t /= ++i; sx -= t;
        t *= x; t /= ++i; cx += t;
        if (t <= DBL_EPSILON) break;
    }
    printf("%.18lf [%.18lf:%d]\n", x, t, i);
    *pcx = cx;
    return sx;
}

int main(void)
{
    double cx = 0, x = M_PI / 16;
    double sx = mysincos(x, &cx), s = sin(x), c = cos(x);
    printf("%.18lf  %.18lf %.18lf\n", sx, cx, sx*sx+cx*cx);
    printf("%.18lf  %.18lf %.18lf\n", s, c, s*s+c*c);
}
