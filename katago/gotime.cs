using System;
using System.Collections.Generic;

sealed class GoTime
{
  List<TimeSpan>[] times;

  static void Main()
  {
    new GoTime().Run();
  }

  void Run()
  {
    GetTimesList();
    times[0].Add(TimeSpan.Zero);
    Make(times[0]);
    Make(times[1]);
  }

  void Make(List<TimeSpan> list)
  {
    var dict = new SortedDictionary<TimeSpan, int>();
    foreach (var time in list) {
      int count;
      dict.TryGetValue(time, out count);
      dict[time] = count + 1;
    }
    var totalTime = new TimeSpan();
    var totalCount = 0;
    foreach (var kvp in dict) {
      Console.WriteLine("{0}, {1,3}", kvp.Key, kvp.Value);
      totalTime += kvp.Key * kvp.Value;
      totalCount += kvp.Value;
    }
    Console.WriteLine("{0}, {1,3}: Total", totalTime, totalCount);
    Console.WriteLine("{0}: Average", totalTime / totalCount);
    Console.WriteLine();
  }

  void GetTimesList()
  {
    times = new List<TimeSpan>[2];
    times[0] = new List<TimeSpan>();
    times[1] = new List<TimeSpan>();
    DateTime time2, time1 = new DateTime();
    var i = 0;
    for (string s; (s = Console.ReadLine()) != null; time1 = time2) {
      if (!s.Contains("play ")) continue;
      time2 = DateTime.Parse(s.Substring(0, 24));
      if (i++ == 0) continue;
      times[(i + 1) % 2].Add(time2 - time1);
    }
  }
}
