import cv2
import sys

img = cv2.imread(sys.argv[1])
detector = cv2.wechat_qrcode_WeChatQRCode(
    "detect.prototxt", "detect.caffemodel", "sr.prototxt", "sr.caffemodel")
# https://github.com/WeChatCV/opencv_3rdparty.git
res, points = detector.detectAndDecode(img)
print(res, points)
