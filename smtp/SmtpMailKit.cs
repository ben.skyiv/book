using System;
using System.IO;
using System.Web;
using MailKit.Net.Smtp;
using MailKit.Security;
using MailKit;
using MimeKit;

static class Program
{
    public static void Main (string[] args)
    {
        var body = new TextPart("plain") { Text = @"修改运行时版本。" };
        var path = "SmtpMailKit.exe.config";
        var type = MimeMapping.GetMimeMapping(path).Split("/".ToCharArray());
        Console.WriteLine("{0}/{1}", type[0], type[1]);
        var attachment = new MimePart(type[0], type[1]) {
            Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
            ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
            ContentTransferEncoding = ContentEncoding.Base64,
            FileName = Path.GetFileName(path)
        };
        var multipart = new Multipart("mixed");
        multipart.Add(body);
        multipart.Add(attachment);
        var message = new MimeMessage() { Subject = "配置文件", Body = multipart };
        message.From.Add(new MailboxAddress("银河", "skyivben@qq.com"));
        message.To.Add(new MailboxAddress("黄志斌", "skyivben@126.com"));
        // message.Cc.Add(new MailboxAddress("黄志斌", "huangzb_np@fj.icbc.com.cn"));
        using (var client = new SmtpClient()) {
            // client.Connect("smtp.qq.com", 587, false); // not useSsl
            // client.Connect("smtp.qq.com", 465, true); // useSsl
            client.Connect("smtp.qq.com", 25, SecureSocketOptions.StartTls);
            client.Authenticate("skyivben@qq.com", "hzfjxeeujnmkcbce");
            client.Send(message);
            client.Disconnect(true);
        }
    }
}
