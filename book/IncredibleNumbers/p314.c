#include <stdio.h>

double p(int n, int m)
{
  double z = 1;
  for (int i = n - 1; i >= m; i--) z *= i / (double)n;
  return 1 - z;
}

int main(void)
{
  int n = 10477, n1 = 10356, n2 = n1 + 1;
  printf("p(%d, %d) = %lf\n", n, n1, p(n, n1));
  printf("p(%d, %d) = %lf\n", n, n2, p(n, n2));
}
