import Data.Digits ( digitsRev )
isPal [] = True; isPal [_] = True
isPal (x:s) = x == last s && isPal (init s)
main = print $ head $ filter (\n -> and [isPal
  $ digitsRev b n | b <- [10,8,2]]) [11,13..]

