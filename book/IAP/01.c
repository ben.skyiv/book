#include <stdio.h>
#include <string.h>
#include <time.h>

const char *now()
{
  static char buf[32]; static time_t t; time(&t);
  strftime(buf, sizeof(buf), "%F %T| ", localtime(&t));
  return buf;
}

unsigned long p10(int n)
{ 
  unsigned long z = 1;
  while (n-- > 0) z *= 10;
  return z;
}

const char *digitsRev(int b, const char *s)
{
  int i = 0; __int128 n = 0; static char a[256];
  for (int c; c = s[i]; i++) n = n * 10 + c - '0';
  for (i = 0; n > 0; n /= b) a[i++] = '0' + n % b;
  a[i] = '\0';
  return a;
}

int isPalindrome(const char a[])
{
  for (int i = 0, j = strlen(a) - 1; i < j; i++, j--)
    if (a[i] != a[j]) return 0;
  return 1;
}

void reverse(char a[])
{
  for (int i = 0, j = strlen(a) - 1; i < j; i++, j--)
  { char t = a[i]; a[i] = a[j]; a[j] = t; }
}

int main()
{ // http://oeis.org/A029804/b029804.txt
  static char s0[2], s1[32], s2[32], s3[64]; s0[1] = '\0';
  for (int i = 15; i <= 15; i++) {
    printf("%s(%d digits)\n", now(), i);
    for (unsigned long j = p10((i >> 1) - 1), j9 = 10 * j; j < j9; j++) {
      sprintf(s1, "%lu", j); strcpy(s2, s1); reverse(s1);
      for (int k9 = (i % 2) ? 10 : 1, k = 0; k < k9; k++) {
        s0[0] = (i % 2) ? ('0' + k) : 0;
        sprintf(s3, "%s%s%s", s2, s0, s1);
        if (isPalindrome(digitsRev(8, s3))) printf("%s%s\n", now(), s3);
      }
    }
  }
  printf("%sEnd\n", now());
}
/*
int isPalindrome(int b, const char * s)
{
  __int128 n = 0, r = 0;
  for (int c, i = 0; c = s[i]; i++) n = n * 10 + c - '0';
  for (__int128 z = n; z > 0; z /= b) r = r * b + z % b;
  return n == r;
} */

