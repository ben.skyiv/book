import Data.Digits ( digitsRev )
main = print $ concat [filter (\n -> and [(\a -> a == reverse a) $
  digitsRev b n | b <- [2,8]]) [read $ x : s ++ y ++ reverse s ++ [x]
  | x <- "13579", s <- if n /= 0 then [replicate (n - length t) '0'
  ++ t | i <- [0 .. 10^n-1], let t = show i] else [""], y <- if z
  then map show [0..9] else [""]] | n <- [0..3], z <- [False, True]]

