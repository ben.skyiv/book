using System;
using System.Text;
using System.Numerics;

static class I01a
{
  static StringBuilder DigitsRev(int b, BigInteger n)
  {
    var sb = new StringBuilder();
    for (; n > 0; n /= b) sb.Append(n % b);
    return sb;
  }

  static bool IsPal(StringBuilder sb)
  {
    for (int i = 0, j = sb.Length - 1; i < j; i++, j--)
      if (sb[i] != sb[j]) return false;
    return true;
  }

  static string Reverse(string s)
  {
    var a = s.ToCharArray();
    Array.Reverse(a);
    return new string(a);
  }

  static void Main()
  { // http://oeis.org/A029804/b029804.txt
    Console.WriteLine(DateTime.Now);
    for (var n = 6; n <= 10; n++) {
      var fmt = "{0:D" + n + "}";
      for (var z = 0; z < 2; z++)
        for (var x = 1; x < 10; x++)
          for (BigInteger s9 = BigInteger.Pow(10, n), s = 0; s < s9; s++) {
            var s1 = string.Format(fmt, s);
            var s2 = Reverse(s1);
            for (int y9 = (z == 0) ? 1 : 10, y = 0; y < y9; y++) {
              var sb = new StringBuilder();
              sb.Append(x); sb.Append(s1);
              if (z != 0) sb.Append(y);
              sb.Append(s2); sb.Append(x);
              if (IsPal(DigitsRev(8, BigInteger.Parse(sb.ToString()))))
                Console.WriteLine(sb);
            }
          }
    }
    Console.WriteLine(DateTime.Now);
  }
}

