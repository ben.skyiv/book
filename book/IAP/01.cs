using System;
using System.IO;
using System.Net;
using System.Collections.Generic;

static class I01
{
  static readonly string b8 = "http://oeis.org/A029804/b029804.txt";
  static readonly string b2 = "http://oeis.org/A007632/b007632.txt";

  static IEnumerable<string> Data(string file)
  {
    using (var res = WebRequest.Create(file).GetResponse())
      for (var sr = new StreamReader(res.GetResponseStream()); ; ) {
        var s = sr.ReadLine();
        if (s == null) break;
        if (s.Length == 0 || s.StartsWith("#")) continue;
        var ss = s.Trim().Split();
        yield return ss[ss.Length - 1];
      }
  }

  static void Main()
  {
    var set = new HashSet<string>();
    foreach (var s in Data(b8)) set.Add(s);
    foreach (var s in Data(b2))
      if (set.Contains(s)) Console.Write(s + " ");
    Console.WriteLine();
  }
}

