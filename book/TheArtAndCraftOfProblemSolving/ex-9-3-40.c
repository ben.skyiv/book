#include <stdio.h>

int f(int n)
{
  int z = 0;
  for (int i = 1; i <= n; i++)
    z += (int)(2.0*n/i) - 2*(int)((double)n/i);
  return z;
}

int main()
{ // http://oeis.org/A075989
  //for (int n = 1; n < 100; n++) printf("%d,", f(n));
  int n = (int)1e9;
  printf("%f\n", f(n)/(double)n);
}
