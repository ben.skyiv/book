#include <stdio.h>

int main(void)
{
  long m = 10, n = 4;
  for (long d = 2; d <= n; d += 2) {
    long d8 = d*d*d*d*d*d*d*d, e = 987*d8/16, f = 2207*d8/2;
    for (long a = -m; a <= m; a++) {
      long a2 = a*a, a3 = a2*a, a5 = a2*a3, a7 = a2*a5;
      long a4 = a*a3, a6 = a*a5, a8 = a*a7;
      for (long b = -m; b <= m; b++) {
        long b2 = b*b, b3 = b2*b, b5 = b2*b3, b7 = b2*b5;
        long b4 = b*b3, b6 = b*b5, b8 = b*b7;
        long z1 = a7*b + 35*a5*b3 + 175*a3*b5 + 125*a*b7;
        long z2 = a8 + 140*a6*b2+1750*a4*b4+3500*a2*b6+625*b8;
        if (z1 != e || z2 != f) continue;
        printf("%ld %ld %ld\n", a, b, d);
      }
    }
  }
}
