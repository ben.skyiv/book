#include <stdio.h>
#include <math.h>

int main(void)
{
  double a = 1; int n = 40;
  for (int i = 0; i < n; a = 1 + 1 / a, i++)
    if (i < 100 || i > n - 10) printf("%5d: %.18f\n", i, a);
  printf("%5s: %.18f\n", "", (1 + sqrt(5)) / 2);
}

