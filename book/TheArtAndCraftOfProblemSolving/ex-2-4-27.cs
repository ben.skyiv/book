using System;
using System.Collections.Generic;

static class Ex27
{
  static void Main()
  {
    int n = 6, n2 = 2*n+1, m = n2;
    var s = new HashSet<int>();
    for (var k = 0; k <= m; k++) {
      for (var x = 0; x <= k; x++)
        s.Add(n2 * k - (n2-n) * x);
      Console.Write(s.Count + ",");
    }
    Console.WriteLine("F({0},{1},{2}) = {3} ({4})",
      n, n2, m, s.Count, 1+n2*m-s.Count);
    /*for (var i = 0; i <= n2*m; i++) {
      if (s.Contains(i)) continue;
      var j = (i > 8*n*n-4*n-1) ? (i-n2*m) : i;
      Console.Write(j + " ");
    }
    Console.WriteLine();*/
  }
}
