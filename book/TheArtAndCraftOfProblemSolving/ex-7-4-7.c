#include <stdio.h>

int main(void)
{
  const int n = 1000;
  for (int a = 2; a <= n; a++)
    for (int b = a; b <= n; b++)
      for (int c = b; c <= n; c++)
        if (2L*a*b*c == (a+1L)*(b+1)*(c+1))
          printf("%d,%d,%d\n", a, b, c);
}

