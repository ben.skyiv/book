#include <stdio.h>
#include <math.h>

int main(void)
{
  double x = sqrt(2);
  for (int i = 0; i < 100; i++) {
    printf("%lf\n", x);
    x = sqrt(2 + x);
  }
}
