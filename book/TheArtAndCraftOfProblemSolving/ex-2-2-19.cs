using System;
using System.Collections.Generic;

static class Ex19
{
  static void Main()
  {
    int m = 1000, a = 7, b = 11;
    var s = new HashSet<int>();
    for (var k = 0; k <= m; k++)
      for (var x = 0; x <= k; x++)
        s.Add(b * k - (b-a) * x);
    for (var i = 0; i <= b*m; i++) {
      if (s.Contains(i) || i > m) continue;
      Console.Write(i + " ");
    }
    Console.WriteLine();
  }
}
