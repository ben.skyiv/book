#include <stdio.h>

int main(void)
{
  const int n = 100;
  static int u[n + 1] = {1,0,1,0,1,1,1,1};
  for (int i = 8; i <= n; i++) u[i] = u[i-2] + u[i-5] - u[i-7];
  printf("%d\n", u[n]);
}

