using System;

static class Ex16
{
  static void Main()
  {
    int n = 6, max = 1 << n;
    var a = new int[n*(n+1)/2+1];
    for (int i = 0; i < max; i++)
    {
      int z = 0;
      for (int k = 1, j = i; j > 0; j >>= 1, k++)
        if ((j & 1) != 0) z += k;
      a[z]++;
    }
    for (var i = 0; i < a.Length; i++) Console.Write(a[i] + " ");
    Console.WriteLine();
  }
}
