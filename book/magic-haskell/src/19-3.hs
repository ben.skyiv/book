newtype State s a = State { runState :: s -> (a, s) }
instance Functor (State s) where
  fmap f fs = State $ \s ->
    let (a, s') = runState fs s in (f a, s')
instance Applicative (State s) where
  pure a = State $ \s -> (a, s)
  f <*> fa = State $ \s ->
    let (fab, s0) = runState f s
        (a, s1) = runState fa s0
    in (fab a, s1)
instance Monad (State s) where
  return = pure
  fa >>= f = State $ \s ->
    let (a, s') = runState fa s in runState (f a) s'
(~+) x = State $ \s -> ((+x), s + x)
(~-) x = State $ \s -> (((-)x), s - x)
(~*) x = State $ \s -> ((*x), s * x)
(~/) x = State $ \s -> ((/x), s / x)
(~~) f = State $ \s -> (f, f s)
op = do
  (~+) 10
  (~*) 4
  (~-) 2
  (~/) 10
  >>= (~~)
  >>= (~~)
main = let (_, result) = runState op 0 in print result

