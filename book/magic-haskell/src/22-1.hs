import Data.Traversable

data BinaryTree a = Nil | Node a (BinaryTree a) (BinaryTree a) deriving (Show)

instance Functor BinaryTree where
  fmap f Nil = Nil
  fmap f (Node x left right) = Node (f x) (fmap f left) (fmap f right)
  
instance Foldable BinaryTree where
  foldMap f Nil = mempty
  foldMap f (Node x left right)
    = foldMap f left `mappend` f x `mappend` foldMap f right
--  foldr f acc Nil = acc
--  foldr f acc (Node x left right)
--    = (foldr f (f x (foldr f acc right)) left)

instance Traversable BinaryTree where
  traverse f Nil = pure Nil
  traverse f (Node x left right) =
    Node <$> f x <*> traverse f left <*> traverse f right

