import Data.Monoid ( (<>) )
import Control.Applicative ( (<|>) )
import Options.Applicative ( execParser,help,info,long,metavar,strOption,switch )

data Message =
  Greet { host :: String, hello :: String, quiet :: Bool }
  | Farewell { host :: String, bye :: String, quiet :: Bool }

hostParser = strOption (long "host" <> metavar "HOST" <> help "Who is the host")
quietParser = switch (long "quiet" <> help "Whether to be quiet")

greetParser = Greet
  <$> hostParser
  <*> strOption (long "hello" <> metavar "TARGET" <> help "Target for the greeting")
  <*> quietParser

farewellParser = Farewell
  <$> hostParser
  <*> strOption (long "bye" <> metavar "TARGET" <> help "Target for the farewll")
  <*> quietParser

messageParser = greetParser <|> farewellParser

main = do
  greet <- execParser $ info messageParser mempty
  case greet of
    Greet host hello False  -> putStrLn $ "Hello, " ++ hello ++ ", from " ++ host
    Farewell host bye False -> putStrLn $ "Bye, " ++ bye ++ ", from " ++ host
    _                       -> return ()

