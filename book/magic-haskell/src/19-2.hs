import Control.Monad ( replicateM )
import Control.Monad.Random ( getRandom )

main = do
  rs <- replicateM 10 (getRandom :: IO Int);  print rs
  rs <- replicateM 10 (getRandom :: IO Char); print rs
  rs <- replicateM 10 (getRandom :: IO Bool); print rs

