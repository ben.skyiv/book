import Data.Monoid ( (<>) )
import Options.Applicative ( execParser,help,info,long,metavar,strOption,switch )

data Greet = Greet { hello :: String, quiet :: Bool }

greetParser = Greet
  <$> strOption
      ( long "hello"
     <> metavar "TARGET"
     <> help "Target for the greeting" )
  <*> switch
      ( long "quiet"
     <> help "Whether to be quiet" )

main = do
  greet <- execParser $ info greetParser mempty
  case greet of
    Greet h False -> putStrLn $ "Hello, " ++ h
    _             -> return ()

