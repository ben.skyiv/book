safe _ [] _ = True
safe x (x1:xs) n = x /= x1 && x /= x1+n && x /= x1-n && safe x xs (n+1)
queensN n = queens n where
  queens 0 = [[]]
  queens m = [x:y | y <- queens (m-1), x <- [1..n], safe x y 1]
main = mapM_ print $ queensN 6

