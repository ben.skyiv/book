import Control.Monad ( forever )
import Control.Concurrent ( forkIO,threadDelay )

main = do
  tid1 <- forkIO $ forever $ do
    threadDelay 1000000
    putStrLn "hello"
  threadDelay 500000
  tid2 <- forkIO $ forever $ do
    threadDelay 1000000
    putStrLn "world"

  putStrLn $ "Two worlds are running at " ++
    show tid1 ++ " and " ++ show tid2 ++ "."

  threadDelay 1000000

