using System;
using System.IO;
using System.Net;
using System.Text;

static class TestProvidentFund {
  static readonly string ApiVersion = "ProvidentFund-API-1.0|";
  static readonly string Url = "http://103.37.61.29/Admin/Api.aspx";

  static string CallApi(string user, string password) {
    var data = Convert.ToBase64String(Encoding.UTF8.GetBytes(
      string.Format("{0}Q{1}|{2}", ApiVersion, user, password)));
    var req = WebRequest.Create(Url);
    req.Method = "POST";
    req.ContentLength = Encoding.UTF8.GetByteCount(data);
    using (var sw = new StreamWriter(req.GetRequestStream())) sw.Write(data);
    using (var sr = new StreamReader(req.GetResponse().GetResponseStream()))
      return Encoding.UTF8.GetString(Convert.FromBase64String(sr.ReadToEnd()));
  }
  
  static void Main(string[] args) {
    Console.WriteLine("{0}", CallApi(
      (args.Length > 0) ? args[0] : "", (args.Length > 1) ? args[1] : ""));
  }
}

