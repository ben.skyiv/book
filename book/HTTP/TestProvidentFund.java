import java.io.*;
import java.net.*;
import java.util.Base64;

public class TestProvidentFund {
  static final String apiVersion = "ProvidentFund-API-1.0|";
  static final String url = "http://103.37.61.29/Admin/Api.aspx";

  static String callApi(String user, String password) throws Exception {
    byte[] data = Base64.getEncoder().encode((
      apiVersion + "Q" + user + "|" + password).getBytes());
    HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection();
    conn.setDoOutput(true);
    conn.setRequestMethod("POST");
    conn.setRequestProperty("Content-Length", Integer.toString(data.length));
    conn.getOutputStream().write(data);
    Reader in = new InputStreamReader(conn.getInputStream());
    StringBuilder sb = new StringBuilder();
    for (int c; (c = in.read()) >= 0; ) sb.append((char)c);
    return new String(Base64.getDecoder().decode(sb.toString().getBytes()));
  }
  
  public static void main(String[] args) throws Exception {
    System.out.println(callApi(
     (args.length > 0) ? args[0] : "", (args.length > 1) ? args[1] : ""));
  }
}

