f n = let a = 2^n; b = a-1; c = 2^(2*a*(b-n))*b^(2*b)
  in (a*a*c, b*b*c, 2*a*b*c)

