function test(n)
  println("\n性能测试(n = $(n))")
  println("生成器表达式：")
  @timev reduce(*, (x for x=1:n))
  println("\n数组推导式：")
  @timev reduce(*, [x for x=1:n])
end

println("Julia Version $(VERSION)")
test(10)
test(10^8)
