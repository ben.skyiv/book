import Math.NumberTheory.Powers ( integerCubeRoot,isCube )
f n = map (\(a,v) -> (a, integerCubeRoot v)) $ filter (isCube . snd)
  $ takeWhile ((>= div n 2) . snd) [(a, n - a*a*a) | a <- [1..]]

