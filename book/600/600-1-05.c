#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned long u64; typedef long i64;
const i64 SIZE = 320L * 1024 * 1024;
static struct buf_t { u64 n : 2, z : 62; } *buf;
static i64 len;

/* If buf does not contain the specified value, the method returns a negative
   integer. You can apply the bitwise complement operator (~) to the negative
   result to produce an index. If this index is equal to the size of the
   array, there are no elements larger than value in the array. Otherwise,
   it is the index of the first element that is larger than value. */
i64 binary_search(u64 value)
{
  i64 mid = 0; int d = 0;
  for (i64 low = 0, high = len - 1; low <= high; ) {
    u64 z = buf[mid = (low + high) >> 1].z;
    if (value == z) return mid; d = (value > z);
    if (d) low = mid + 1;
    else  high = mid - 1;
  }
  return ~(mid + d);
}

int insert(u64 z, i64 i)
{ // return: 1 for success, 0 for buf overflow.
  if (len >= SIZE) return 0;
  memmove(buf + i + 1, buf + i, (len++ - i) * sizeof(u64));
  buf[i].z = z;
  return 1;
}

int main()
{
  buf = (struct buf_t *)calloc(SIZE, sizeof(struct buf_t));
  if (buf == NULL) { puts("error: out of memory"); exit(1); }
  printf("memory: %lu bytes\n", (u64)SIZE * sizeof(buf)); fflush(stdout);
  for (int c = 2; ; c++)
    for (int a = c >> 1; a > 0; a--) {
      u64 z = ((u64)c * c - 3UL * a * (c - a)) * c;
      i64 i = binary_search(z);
      if (i < 0) { if (!insert(z, ~i)) goto exit; continue; }
      if (buf[i].n < 3) buf[i].n++;
      if (buf[i].n < 2) continue;
      printf("%6d %6d: %15lu (%lu) %ld\n",
        c, a, buf[i].z, buf[i].n+1UL, len); fflush(stdout);
    }
  exit: printf("SIZE: %ld\n", len);
  free(buf);
}
/*
3255   1140:     10942414875 (3) 2642748
3270   1530:      8849601000 (3) 2666795
3303   1269:     10458523413 (3) 2721208
3324   1632:      9190673856 (3) 2755609
3345   1275:     10942414875 (4) 2790917
10,942,414,875: (835,2180) (1140,2115) (1275, 2070)
*/

