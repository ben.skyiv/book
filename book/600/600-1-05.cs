using System;
using System.Collections.Generic;

static class M105
{
  static int CubeRoot(long z)
  { // z > 0
    double a = Math.Pow(z, 1/3.0), b = Math.Round(a);
    if (Math.Abs(a - b) > 1e-9) return -1;
    return (int)b;
  }

  static void Out(long z)
  {
    Console.Write("{0,18:N0}:", z);
    for (int b, a = 1; ; a++) {
      long v = z - (long)a*a*a;
      if (v < z / 2) break;
      if ((b = CubeRoot(v)) < 0) continue;
      Console.Write(" ({0,5},{1,5})", a, b);
    }
    Console.WriteLine();
  }

  static void Main()
  {
    Dictionary<long, byte> dict = null;
    try {
      var capacity = (int)(2.65 * 47995853);
      dict = new Dictionary<long, byte>(capacity);
      for (int c = 2; ; c++)
        for (int a = c >> 1; a > 0; a--) {
          if (dict.Count >= capacity) goto exit;
          long z = ((long)c * c - 3L * a * (c - a)) * c;
          byte n; dict.TryGetValue(z, out n);
          dict[z] = (byte)(++n);
          if (n >= 3) Out(z);
        }
    }
    catch (OutOfMemoryException) { }
    exit: Console.WriteLine("Count: {0:N0}",
      (dict == null) ? 0 : dict.Count);
  }
}

