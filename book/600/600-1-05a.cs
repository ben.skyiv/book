using System;

static class M105a
{
  static int CubeRoot(long z)
  { // z > 0
    double a = Math.Pow(z, 1/3.0), b = Math.Round(a);
    if (Math.Abs(a - b) > 1e-9) return -1;
    return (int)b;
  }

  static int Count(long z, int a)
  {
    int n = 1;
    for (a++; ; a++) {
      long v = z - (long)a*a*a;
      if (v < z / 2) break;
      if (CubeRoot(v) >= 0) n++;
    }
    return n;
  }

  static void Main(string[] args)
  {
    Console.WriteLine(DateTime.Now);
    for (int c = 22568; ; c++)
      for (int a = c >> 1; a > 0; a--) {
        long z = ((long)c * c - 3L * a * (c - a)) * c;
        int n = Count(z, a);
        if (n >= 3) Console.WriteLine("{0,6} {1,6} {2,19:N0}", c, a, z);
        if (n >  3) goto exit;
      }
    exit: Console.WriteLine(DateTime.Now);
  } // 12,625,136,269,928: 
}   // (4275,23237) (7068,23066) (10362,22580) (12939,21869)

