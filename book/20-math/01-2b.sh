gnuplot -p <<!
set samples 100000
unset key
unset border
set zeroaxis
set xtics axis
set ytics axis
fA(x) = (1-abs(x))*(abs(x)-0.75)
fB(x) = (0.75-abs(x))*(abs(x)-0.5)
fC(x) = (1.5-0.5*abs(x))*sqrt(abs(abs(x)-1)/(abs(x)-1))
fD(x) = 6*sqrt(10)/14*sqrt(4-(abs(x)-1)**2)
f1(x) = 9*sqrt(abs(fA(x))/fA(x))-8*abs(x)
f2(x) =  3*sqrt(-sqrt(abs(abs(x)-3)/(abs(x)-3))*((x/7)**2)+1)
f3(x) = -3*sqrt(-sqrt(abs(abs(x)-4)/(abs(x)-4))*((x/7)**2)+1)
f4(x) = abs(x/2)-(3*sqrt(33)-7)/122*x*x+sqrt(1-(abs(abs(x)-2)-1)**2)-3
f5(x) = 3*abs(x)+0.75*sqrt(abs(fB(x))/fB(x))
f6(x) = 2.25*sqrt(abs(0.5-abs(x))/(0.5-abs(x)))
f7(x) = 6*sqrt(10)/7+fC(x)-fD(x)
plot [x=-7:7] f1(x),f2(x),f3(x),f4(x),f5(x),f6(x),f7(x)
!

