gnuplot -p <<!
unset key
unset border
set zeroaxis
set xtics axis
set ytics axis
f(x) = 0.6*sqrt(abs(x))
g(x) = sqrt(0.5*(1-x*x))
plot [x=-1:1] f(x)+g(x),f(x)-g(x)
!

