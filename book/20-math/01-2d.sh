gnuplot -p <<!
set parametric
unset key
unset border
set zeroaxis
set xtics axis
set ytics axis
plot sin(t)*(1+cos(t)),-cos(t)*(1+cos(t))
!

