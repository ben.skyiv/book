using System;
using System.Collections.Generic;

static class R19199
{
  static int DigitsSum(int n)
  {
    var z = 0;
    for (; n > 0; n /= 10) z += n % 10;
    return z;
  }

  static void Main()
  {
    var dict = new SortedDictionary<int, List<int>>();
    for (var i = 1; i <= 1999; i++) {
      var k = DigitsSum(i);
      List<int> v;
      if (!dict.TryGetValue(k, out v)) dict.Add(k, v = new List<int>());
      v.Add(i);
    }
    foreach (var kvp in dict) {
      if (kvp.Value.Count < 19) continue;
      var min = 0;
      for (var i = 0; i < 19; i++) min += kvp.Value[i];
      if (min > 1999) continue;
      Console.Write("{0,2}: {1,3} {2}:", kvp.Key, kvp.Value.Count, min);
      for (var i = 0; i < 19; i++) Console.Write(" " + kvp.Value[i]);
      Console.Write(": ");
      for (var i = 19; i < 25; i++) Console.Write(" " + kvp.Value[i]);
      Console.WriteLine();
    }
  }
}

