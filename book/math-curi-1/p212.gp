f1(n)=x=sum(i=1,n,1.0/i);exp(x)*log(x)+x;
f2(n)=exp(Euler)*n*log(log(n));
g1(n)=[sigma(n),f1(n)];
g2(n)=[sigma(n),f2(n)];
\\ ploth(n=1,1000,f1(n))
\\ vectorv(30,n,g1(n))
\\ apply(g1,[123,1234,12345,123456,1234567,12345678])~

