#include <stdio.h>

const long n = (long)1e9;
const int n2 = (int)3.17e4; // n2 > sqrt(n + n2)

void factor(long i0, char a[], const char c[])
{
  static long t[n2];
  for (int i = 0; i < n2; ++i) t[i] = 1, a[i] = 0;
  for (int p = 2; (long)p * p < i0 + n2; p++)
    if (!c[p])
      for (long q = p; q < i0 + n2; q *= p)
        for (long i = q - 1 - (i0 - 1) % q; i < n2; i += q)
          t[i] *= p, ++a[i];
  for (int i = 0; i < n2; ++i) if (i + i0 != t[i]) ++a[i];
}

int main()
{
  static char a[n2], c[n2];
  for (int i = 2; i * i < n2; ++i)
    if (!c[i]) for (int j = i * i; j < n2; j += i) c[j] = 1;
  for (long z = -1, i0 = -n2, i = 2; i <= n; ++i) {
    if (i >= i0 + n2) factor(i0 = i, a, c);
    if (a[i - i0] & 1) ++z; else --z;
    if (z < 0) { printf("%ld\n", i); break; }
  }
}

