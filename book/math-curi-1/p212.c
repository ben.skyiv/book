#include <stdio.h>
#include <math.h>

typedef struct { long ps[12]; char ks[12], n; } item;

const long n =    (long)1e8;
const int n2 = 1 + (int)1e4; // n2 > sqrt(n + n2)

void factor(long i0, item a[], const char c[])
{
  static long t[n2];
  for (int i = 0; i < n2; i++) t[i] = 1, a[i].n = 0;
  for (int p = 2; (long)p * p < i0 + n2; p++)
    if (!c[p])
      for (long q = p; q < i0 + n2; q *= p)
        for (long i = q-1-(i0-1)%q; i < n2; t[i] *= p, i += q) {
          if (q != p) ++a[i].ks[a[i].n - 1];
          else a[i].ps[a[i].n] = p, a[i].ks[a[i].n++] = 1;
        }
  for (int i = 0; i < n2; i++) {
    if (i + i0 == t[i]) continue;
    a[i].ps[a[i].n] = (i + i0) / t[i];
    a[i].ks[a[i].n++] = 1;
  }
}

long sigma(const long ps[], const char ks[], int k)
{
  long z = 1;
  for (long t = 1, i = 0; i < k; z *= t, t = 1, i++)
    for (long p = 1, j = 0; j < ks[i]; j++) t += p *= ps[i];
  return z;
}

int main(int argc, char *argv[])
{
  static item a[n2]; static char c[n2]; double H = 1;
  for (int i = 2; i * i < n2; i++)
    if (!c[i]) for (int j = i * i; j < n2; j += i) c[j] = 1;
  for (long i0 = -n2, i = 2; i <= n; i++) {
    if (i >= i0 + n2) factor(i0 = i, a, c);
    long s1 = sigma(a[i-i0].ps, a[i-i0].ks, a[i-i0].n);
    double s2 = (H += 1.0 / i, H + exp(H) * log(H));
    if (s1 >= s2) printf("ERROR:%ld\n", i);
  }
  printf("Finish:%ld\n", n);
}

