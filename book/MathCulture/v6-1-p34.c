// http://www.global-sci.org/v1/mc/issues/6/no1/freepdf/34s.pdf
#include <stdio.h>
#include <math.h>

double a(int n)
{
  double v = 0, t = 1;
  for (n++; t != 0; n++) t /= n, v += t, t = -t;
  return v;
}

int main(void)
{
  double v = 1 - 1 / M_E;
  for (int i = 1; i <= 1000; v = 1 - i++ * v)
    if (i < 5 || (i > 15 && i < 23) || i > 998)
      printf("a(%3d): %.12lf %16.12lf\n", i-1, a(i-1), v);
}

