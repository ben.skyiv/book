using System;
using System.Drawing;
using System.Windows.Forms;

namespace Skyiv.CellularAutomaton.MajorityClassification
{
  sealed class MainForm : Form
  {
    static readonly int sizeCellular = 4;
    static readonly int nCellular = 201;
    static readonly int lines = nCellular;
    static readonly Pen pen = new Pen(Color.Black, sizeCellular);
    static readonly string strRuler =
      "0000010100000110000101011000011100000111000001000001010101" +
      "0101110110010001110111000001010000000101111101111111111011" +
      "011101111111";
    static readonly bool[] ruler = new bool[strRuler.Length];

    Graphics gc;

    MainForm()
    {
      SuspendLayout();

      var btnRefresh = new Button();
      btnRefresh.Text = "Refresh";
      btnRefresh.Location = new Point(nCellular * sizeCellular - 80, lines * sizeCellular + 9);
      btnRefresh.Click += new EventHandler(BtnRefresh_Click);
      Controls.Add(btnRefresh);

      Text = "Majority Classification";
      BackColor = Color.White;
      ClientSize = new Size(nCellular * sizeCellular + 1, lines * sizeCellular + 1 + 32);
      for (var i = 0; i < ruler.Length; i++) ruler[i] = strRuler[i] == '1';

      ResumeLayout(false);
    }

    void BtnRefresh_Click(object sender, EventArgs e)
    {
      Invalidate();
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      gc = e.Graphics;
      DrawGrid(true);
      var cellulars = GetInitCellulars();
      DrawCellulars(cellulars, 0);
      DisplayMessage(cellulars);
      for (var i = 1; i < lines; i++)
      {
        StepIt(cellulars);
        DrawCellulars(cellulars, i);
      }
      base.OnPaint(e);
    }

    void StepIt(bool[] cellulars)
    {
      var buf = new bool[cellulars.Length];
      for (var i = 0; i < nCellular; i++)
        buf[i] = ruler[GetValue(cellulars, i)];
      Array.Copy(buf, cellulars, cellulars.Length);
    }

    int GetValue(bool[] cellulars, int idx)
    {
      var n = 0;
      idx = (idx + 3) % nCellular;
      for (var i = 0; i < 7; i++)
        if (cellulars[(idx - i + nCellular) % nCellular])
          n += 1 << i;
      return n;
    }

    void DrawCellulars(bool[] cellulars, int line)
    {
      for (var i = 0; i < cellulars.Length; i++)
        if (cellulars [i])
          Set(i, line);
    }

    void DisplayMessage(bool[] cellulars)
    {
      var blacks = 0;
      foreach (var cellular in cellulars)
        if (cellular)
          blacks++;
      Out("Black:{0} White:{1} CLR:{2} OS:{3}",
        blacks, cellulars.Length - blacks, Environment.Version, Environment.OSVersion);
    }

    void Out(string fmt, params object[] args)
    {
      gc.DrawString(string.Format(fmt, args), new Font("DejaVu Sans", 10),
        Brushes.Blue, new Point(5, lines * sizeCellular + 9));
    }

    bool[] GetInitCellulars()
    {
      var rand = new Random();
      var cellulars = new bool[nCellular];
      for (var i = 0; i < cellulars.Length; i++)
        cellulars [i] = rand.Next() % 2 == 0;
      return cellulars;
    }

    void DrawGrid(bool onlyBorder)
    {
      var pen = new Pen(Color.Red, 1);
      var len = nCellular * sizeCellular;
      for (var i = onlyBorder ? lines : 0; i <= lines; i++)
      {
        var k = i * sizeCellular;
        gc.DrawLine(pen, 0, k, len, k);
      }
      len = lines * sizeCellular;
      for (var i = onlyBorder ? nCellular : 0; i <= nCellular; i++)
      {
        var k = i * sizeCellular;
        gc.DrawLine(pen, k, 0, k, len);
      }
    }

    void Set(int x, int y)
    {
      var y2 = y * sizeCellular + sizeCellular / 2;
      gc.DrawLine(pen, x * sizeCellular, y2, (x + 1) * sizeCellular, y2);
    }

    static void Main()
    {
      Application.Run(new MainForm());
    }
  }
}
