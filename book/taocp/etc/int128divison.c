#include <stdio.h>

long count(int high)
{
  long n = 0;
  for (int d = 3; d < high; d += 2) {
    if (d % 5 == 0) continue;
    __int128 d4 = (long)d * d; d4 *= d4;
    for (int c = d - 1; c > 0; c--) {
      __int128 c4 = (long)c *c;
      c4 *= d4 - c4 * c4;
      if (c4 % 625 == 0) n++;
    }
  }
  return n;
}

int main(void)
{
  int high = (int)5e5;
  printf("%d: %ld\n", high, count(high));
}
