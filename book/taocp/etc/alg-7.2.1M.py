j = 1                        # M0
m = [2, 4, 3, 2]             # M0
a = [0, 0, 0, 0]             # M1
while j != 0:                # M5
    print(a[1:])             # M2
    j = len(m) - 1           # M3
    while a[j] == m[j] - 1:  # M4
        a[j], j = 0, j - 1   # M4
    a[j] += 1                # M5
