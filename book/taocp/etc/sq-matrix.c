#include <stdio.h>

int main(void)
{
  for (int a = 0; a < 10; a++) {
    printf("%d:", a);
    for (int b = 0; b < 10; b++)
      if (a >= b) printf(" %5d", a*a*a*a + b*b*a*a);
    printf("\n");
  }
  printf(">>");
  for (int a = 0; a < 10; a++) printf(" %5d", a);
  printf("\n");
}
