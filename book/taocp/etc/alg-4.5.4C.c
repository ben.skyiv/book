#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])
{
  long n = (argc > 1) ? atol(argv[1]) : (5 * 5 * 41); // 13 * 29
  if (n % 2 != 1) n++;
  long s =  (long)sqrt(n), a = (s << 1) + 1, b = 1, r = s * s - n;
  c2: if (r == 0) goto c6;
  c3: r += a; a += 2;
  c4: r -= b; b += 2;
  c5: if (r > 0) goto c4; else goto c2;
  c6: printf("%ld: %ld (%ld)\n", n, (a - b) / 2, s);
}
