// gcc -O3 vol2-ex-3.c -lm
#include <stdio.h>
#include <math.h>
#define min(x,y) ((x) < (y) ? (x) : (y))
const double epsilon = 1e-10;
typedef __int128  i16;

double fourth_root(i16 x) { return exp(0.25*log((double)x)); }
int is_power4(i16 w4, i16 w) { return w * w * w * w == w4; }

int possible_power4(i16 x)
{
  double y = fourth_root(x); y -= (long)y;
  return y < epsilon || y > 1 - epsilon;
}

int main(void)
{ // solve: z^4 = w^4+x^4+y^4, 0 <= w <= x <= y < z
  // 422481: 414560, 217519, 95800
  printf("epsilon: %lg\n", epsilon); fflush(stdout);
  for (i16 z = 1; ; z++) {
    i16 z4 = z * z; z4 *= z4;
    for (i16 y = z - 1; y > 0; y--) {
      i16 y4 = y * y; y4 *= y4;
      i16 zy = z4 - y4;
      i16 x0 = 2 + (i16)fourth_root(zy);
      for (i16 x = min(y, x0); x >= 0; x--) {
        i16 x4 = x * x; x4 *= x4;
        i16 w4 = zy - x4;
        if (w4 > x4) break;
        if (w4 < 0) continue;
        if (!possible_power4(w4)) continue;
        i16 w = fourth_root(w4) + 0.5;
        printf("%ld: %ld, %ld, %ld %s\n", (long)z, (long)y,
          (long)x, (long)w, is_power4(w4, w) ? "OK" : "*");
        fflush(stdout);
      }
    }
  }
}
