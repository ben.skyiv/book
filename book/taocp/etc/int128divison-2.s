# https://blog.csdn.net/qq_49005782/article/details/116356620
opcode source target
disp(base,index,scale): disp+base+index*scal
b:byte, w:word, l:long, q:quad
adcq: add 128-bit integer
sbbq: sub 128-bit integer

movabsq	$ 5283147502710415582, %r15 # imm = 0x495182A9930BE0DE
movabsq	$-3276141747490816367, %rbx # imm = 0xD288CE703AFB7E91
movabsq	$ 5401206664782156713, %r12 # imm = 0x4AF4F0D844D013A9
movabsq	$   14757395258967641, %r13 # imm = 0x00346DC5D6388659
movabsq	$-7644330744145238189, %rbp # imm = 0x95E9E1B089A02753
movabsq	$   29514790517935282, %r10 # imm = 0x0068DB8BAC710CB2

-24(%rsp) # high : int
%rdx      # d    : int
%r14      # c    : int
%rax      # d4   : __int128
%rsi      # c4   : __int128
