#include <stdio.h>
#include <math.h>
const double epsilon = 1e-10;

int main(void)
{ // solve: d^4 = a^4+b^4+c^4, 0 <= a <= b <= c < d
  printf("epsilon: %lg\n", epsilon); fflush(stdout);
  for (int d = 1; ; d += 2) {
    if (d % 5 == 0) continue;
    __int128 d4 = (long)d * d; d4 *= d4;
    for (int c = d - 1; c > 0; c--) {
      if (c % 5 == 0) continue;
      __int128 dc, c4 = (long)c * c; c4 *= c4;
      if ((dc = d4 - c4) % 625 != 0) continue;
      for (int u, v, w, b = 5; ; b += 5) {
        __int128 a4, b4 = (long)b * b; b4 *= b4;
        if ((a4 = dc - b4) < b4) break;
        if ((u = a4 %  9) != 0 && u != 1 && u != 4 && u != 7) continue;
        if ((v = a4 % 13) != 0 && v != 1 && v != 3 && v != 9) continue;
        if ((w = a4 % 29) != 0 && w != 1 && w != 7 && w != 16 &&
            w != 20 && (w < 23 || w > 25)) continue;
        double z = sqrt(sqrt((double)a4));
        double y = z - (int)z;
        if (y > epsilon && y < 1 - epsilon) continue;
        int a = (int)(z + 0.5);
        if (a == d) continue;
        printf("%d: %d, %d, %d (%lg)\n", d, c, b, a,
          (y < 0.5) ? y : (y - 1)); fflush(stdout);
        if ((__int128)a * a * a * a == a4) return 0;
      }
    }
  }
}
