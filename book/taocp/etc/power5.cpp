#include <stdio.h>
#include <time.h>
#include <queue>
#include <cmath>
using namespace std;
typedef __int128 i16;

extern char* i2str(i16 x);
inline i16 p5(int x) { i16 z = (long)x * x; return z * z * x; }

const int hi = 150, h1 = 109, h2 = hi;
const i16 hi5 = p5(hi);

void print_time()
{
  static char buf[32];
  static time_t t;
  time(&t);
  strftime(buf, sizeof(buf), "%F %T", localtime(&t));
  printf("%s| ", buf);
}

class i16comparer
{ public: bool operator()(const i16 &a, const i16 &b){ return a>b; }};

priority_queue<i16, vector<i16>, i16comparer> q1, q2;

void output(i16 x)
{
  print_time(); printf("%s: %lu; %lu\n", i2str(x), q1.size(), q2.size());
}

void load_q1(int c, int b)
{
  i16 abc, bc = p5(c) + p5(b);
  if (bc >= hi5) return;
  for (int a = 1; a <= b; a++) {
    if ((abc = bc + p5(a)) >= hi5) break;
if(c==110&&b==84&&a==27)fprintf(stderr,"[%s]\n",i2str(abc));
    q1.push(abc);
  }
}

void load_q2(int e)
{
  i16 e5 = p5(e);
  for (int d = e - 1; d > 0; d--) {
if(e==144&&d==133)fprintf(stderr,"<%s>\n",i2str(e5 - p5(d)));
    q2.push(e5 - p5(d));
  }
}

void initialize()
{
  print_time(); printf("high: %d (%d, %d)\n", hi, h1, h2);
  for (int c = 1; c < h1; c++)
    for (int b = 1; b <= c; b++) load_q1(c, b);
  for (int e = 2; e < h2; e++) load_q2(e);
  print_time(); printf("size: %lu; %lu\n", q1.size(), q2.size());
}

bool ab=0;
void update_q1(int &c0, int &b0, i16 &abc0, i16 &x1)
{
bool aa=0;if(ab||(c0==110&&(b0>=82&&b0<=86)))
{aa=1;fprintf(stderr,"(%s,%d,%d)",i2str(x1),c0,b0);}
  load_q1(c0, b0); x1 = q1.top();
  if (b0 < c0) b0++; else b0 = 1, c0++;
  abc0 = p5(c0) + p5(b0) + 1;
if(aa)fprintf(stderr,"(%s)\n",i2str(abc0));
}

void update_q2(int &e0, i16 &ed0, i16 &x2)
{
  load_q2(e0); x2 = q2.top(); ++e0; ed0 = p5(e0) - p5(e0 - 1);
}

static int an=0;
void match()
{
  int c0 = h1, b0 = 1, e0 = h2;
  i16 abc0 = p5(c0) + 2, ed0 = p5(e0)-p5(e0-1);
fprintf(stderr,"<%s>(abc0)\n",i2str(abc0));
  for (; ; ) {
    i16 x1 = q1.top();ab=0;
if(x1>=20301000000&&++an<10){ab=1;
fprintf(stderr,"<%s>*",i2str(x1));
fprintf(stderr,"(%s:%d,%d)*\n",i2str(abc0),c0,b0);}
    if (c0 < hi && x1 > abc0) update_q1(c0, b0, abc0, x1);
    i16 x2 = q2.top(); if (e0 < hi && x2 > ed0) update_q2(e0, ed0, x2);
    if (x1 == x2) { output(x1); q1.pop(); q2.pop(); continue; }
    if (x1 < x2) { if (q1.size() > 0) q1.pop(); else break; }
    else         { if (q2.size() > 0) q2.pop(); else break; }
  }
  print_time(); printf("Finish, size: %lu; %lu\n", q1.size(), q2.size());
}

int main()
{ // solve: e^5 = a^5+b^5+c^5+d^5
  initialize();
  match();
}
