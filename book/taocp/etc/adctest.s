# https://blog.csdn.net/notbaron/article/details/106437310
# $ as -o adctest.o adctest.s
# $ ld -o adctest adctest.o -lc -I /lib64/ld-linux-x86-64.so.2
.section .data
d1:
   .quad 1,5; //16字节整数
d2:
   .quad 3,2; //16字节整数
out:
   .asciz "The result is %qd\n"
.section .text
.globl _start
_start:
   movq d1+0, %rbx; //保存d1的前8个字节到ebx
   movq d1+8, %rax; //保存d1的后8个字节到ebx
   movq d2+0, %rdx; //保存d2的前8个字节到ebx
   movq d2+8, %rcx; //保存d2的后8个字节到ebx
   addq %rbx, %rdx; //前8个字节相加
   adcq %rcx, %rax; //后8个字节相加
   adcq %rdx, %rax; //全部相加
   movq $out, %rdi
   movq %rax ,%rsi
   call printf
   movq $60,  %rax
   movq $0,   %rdi
   syscall
