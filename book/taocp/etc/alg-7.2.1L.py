n = 4                                   # L0
a, f = ['0'] * n, list(range(n+1))      # L1
while True:                             # L4
    print(''.join(a[::-1]))             # L2
    j, f[0] = f[0], 0                   # L3
    if j == n: break                    # L3
    f[j], f[j+1] = f[j+1], j+1          # L3
    a[j] = '1' if a[j] == '0' else '0'  # L4
