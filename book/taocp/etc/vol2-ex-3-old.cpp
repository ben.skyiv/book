#include <iostream>
#include <queue>
#include <cmath>
#include <cassert>
using namespace std;
typedef __int128 i16;

inline i16 U(i16 x) { return x >> 32; }
inline int V(i16 x) { return (int)(x & 0xFFFFFFFF); }
inline i16 T(i16 u, int v) { return (u << 32) | v; }
inline i16 p4(int x) { i16 z = (long)x * x; return z * z; }
inline int i4root(i16 x) { return (int)(sqrt(sqrt((double)x)) + 0.5); }

const int hi = 5e5, h0 = hi / 5;
const i16 hi4 = p4(hi);

class i16comparer
{ public: bool operator()(const i16 &a, const i16 &b){ return U(a)>U(b); }};

priority_queue<i16, vector<i16>, i16comparer> q1, q2;

void load_q1(int b)
{
  i16 b4 = p4(b);
  for (int a = 5; a <= b; a += 5) {
    i16 ab, a4 = p4(a);
    if ((ab = a4 + b4) >= hi4) break;
    q1.push(T(ab, b));
  }
}

void load_q2(int d)
{
  if (d % 5 == 0) return;
  i16 d4 = p4(d);
  for (int c = d - 1; c > 0; c--) {
    i16 dc, c4 = p4(c);
    if ((dc = d4 - c4) % 625 == 0) q2.push(T(dc, d));
  }
}

i16 update_q1(int &b0, i16 &ab, i16 &ab0)
{
  load_q1(b0);
  i16 x1 = T(ab = ab0 = p4(b0), b0);
  b0 += 5;
  return x1;
}

i16 update_q2(int &dh, i16 &dc, i16 &dc0)
{
  load_q2(dh);
  i16 x2 = T(dc = dc0 = p4(dh) - p4(dh - 1), dh);
  if ((dh += 2) % 5 == 0) dh += 2;
  return x2;
}

void output(i16 x1, i16 x2)
{
  int b = V(x1), d = V(x2);
  cout << "size: "  << q1.size() << "; " << q2.size() << endl
       << d << ": " << i4root(p4(d) - U(x2)) << "; "
       << b << "; " << i4root(U(x1) - p4(b)) << endl;
}

void initialize()
{
  cout << "high: " << hi << " (" << h0 << ")" << endl;
  for (int b = 5; b < h0; b += 5) load_q1(b);
  for (int d = 3; d < h0; d += 2) load_q2(d);
  cout << "size: " << q1.size() << "; " << q2.size() << endl;
}

void match()
{
  int b0 = h0, dh = h0 + 1; assert(h0 % 10 == 0);
  for (i16 ab0 = p4(b0), dc0 = p4(dh) - p4(dh - 1); ; ) {
    i16 x1 = q1.top(), ab = U(x1);
    if (b0 < hi && ab > ab0) x1 = update_q1(b0, ab, ab0);
    i16 x2 = q2.top(), dc = U(x2);
    if (dh < hi && dc > dc0) x2 = update_q2(dh, dc, dc0);
    if (ab == dc) { output(x1, x2); break; }
    if (ab < dc) { if (q1.size() > 0) q1.pop(); else break; }
    else         { if (q2.size() > 0) q2.pop(); else break; }
  }
}

int main()
{ // solve: d^4 = a^4+b^4+c^4, 0 <= a <= b <= c < d
  initialize();
  match();
}
