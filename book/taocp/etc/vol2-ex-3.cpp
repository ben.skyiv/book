#include <iostream>
#include <queue>
#include <cmath>
#include <tuple>
#include <cassert>
using namespace std;
typedef __int128 i16;
inline i16 p4(int x) { i16 z = (long)x * x; return z * z; }
const double epsilon = 1e-10;
const int hi = 5e5, h0 = hi / 5;
const i16 hi4 = p4(hi);
class i16comparer
{ public: bool operator()(const i16 &a, const i16 &b){ return a > b; }};
priority_queue<i16, vector<i16>, i16comparer> q1, q2;

void load_q1(int b)
{
  i16 ab, b4 = p4(b);
  for (int a = 5; a <= b; a += 5) {
    if ((ab = p4(a) + b4) >= hi4) break;
    q1.push(ab);
  }
}

void load_q2(int d)
{
  if (d % 5 == 0) return;
  i16 dc, d4 = p4(d);
  for (int c = d - 1; c > 0; c--)
    if ((dc = d4 - p4(c)) % 625 == 0) q2.push(dc);
}

void initialize()
{
  cout << "high: " << hi << " (" << h0 << ")" << endl;
  for (int b = 5; b < h0; b += 5) load_q1(b);
  for (int d = 3; d < h0; d += 2) load_q2(d);
  cout << "size: " << q1.size() << "; " << q2.size() << endl;
}

void update_q1(int &b0, i16 &x1, i16 &ab0)
{  load_q1(b0); x1 = p4(b0) + 625; ab0 = p4(b0 += 5) + 625; }

void update_q2(int &dh, i16 &x2, i16 &dc0)
{
  load_q2(dh); x2 = p4(dh) - p4(dh - 1);
  if ((dh += 2) % 5 == 0) dh += 2;
  dc0 = p4(dh) - p4(dh - 1);
}

i16 match()
{
  i16 z = 0; int b0 = h0, dh = h0 + 1; assert(h0 % 10 == 0);
  for (i16 ab0 = p4(b0) + 625, dc0 = p4(dh) - p4(dh - 1); ; ) {
    i16 x1 = q1.top(); if (b0 < hi && x1 > ab0) update_q1(b0, x1, ab0);
    i16 x2 = q2.top(); if (dh < hi && x2 > dc0) update_q2(dh, x2, dc0);
    if (x1 == x2) { z = x1; break; }
    if (x1 < x2) { if (q1.size() > 0) q1.pop(); else break; }
    else         { if (q2.size() > 0) q2.pop(); else break; }
  }
  cout << "size: " << q1.size() << "; " << q2.size() << endl;
  return z;
}

auto seek(i16 x0, bool add)
{
  for (int x = 1; ; x++) {
    i16 x4 = p4(x);
    double z = sqrt(sqrt((double)(add ? x0 + x4 : x0 - x4)));
    double y = z - (int)z;
    if (y < epsilon || y > 1 - epsilon) return make_tuple(x,(int)(z+0.5));
  }
}

int main()
{ // solve: d^4 = a^4+b^4+c^4, 0 <= a <= b <= c < d
  initialize(); i16 x = match(); if (x == 0) return 0;
  auto [a,b] = seek(x, 0); auto [c,d] = seek(x, 1);
  cout << d << ": " << c << ", " << b << ", "<< a << endl;
}
