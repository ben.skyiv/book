	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_7:                   # in Loop: Header=BB0_4 Depth=1
	movq	-8(%rsp), %rdx     # d
	addq	$2, %rdx           # d += 2
	movq	-16(%rsp), %r14    # ?
	addq	$2, %r14           # ?
	cmpq	-24(%rsp), %rdx    # d < high ?
	jae	.LBB0_2              # 是否退出外循环
.LBB0_4:                   # 外循环顶部
	imull	$-858993459, %edx, %eax  # imm = 0xCCCCCCCD
	cmpl	$858993460, %eax         # imm = 0x33333334
	movq	%rdx, -8(%rsp)     # d
	movq	%r14, -16(%rsp)    # ?
	jb	.LBB0_7              # if (d % 5 == 0) continue
	movq	%rdx, %rax         # d4 = d
	imulq	%rdx, %rax         # d4 *= d
	mulq	%rax               # d4 *= d4
	movq	%rax, %r8
	movq	%rdx, %r11
	.p2align	4, 0x90
.LBB0_6:                # 内循环顶部
	leal	-2(%r14), %esi  #
	imulq	%rsi, %rsi      # c4 *= c
	movq	%rsi, %rax      #
	mulq	%rsi            # c4 *= c4
	movq	%r8,  %rdi      #
	subq	%rax, %rdi      # c4 = d4 - c4
	movq	%r11, %r9       #
	sbbq	%rdx, %r9       #
	movq	%rdi, %rax
	mulq	%rsi
	imulq	%rsi, %r9
	addq	%rdx, %r9
	movq	%rax, %rsi
	imulq	%r15, %rsi
	mulq	%rbx
	addq	%rsi, %rdx
	imulq	%rbx, %r9
	addq	%rdx, %r9
	addq	%r12, %rax
	adcq	%r13, %r9
	cmpq	%rbp, %rax
	sbbq	%r10, %r9
	adcq	$0,  %rcx
	addq	$-1, %r14       # c--
	cmpq	$2,  %r14       # c ?> 2
	jg	.LBB0_6           # 内循环结束
	jmp	.LBB0_7           # 外循环结束
