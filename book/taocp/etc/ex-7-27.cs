// wget https://www-cs-faculty.stanford.edu/~knuth/sgb-words.txt
using System;
using System.IO;
using System.Collections.Generic;

static class Ex27
{
  static int Count(string s)
  {
    var a = new HashSet<char>();
    foreach (var c in s) a.Add(c);
    return a.Count;
  }

  static void Main()
  {
    var a = new int[6];
    foreach (var s in File.ReadLines("sgb-words.txt")) {
      var n = Count(s);
      a[n]++;
      if (n == 2) Console.Write(s + " ");
    }
    Console.WriteLine();
    var z = 0;
    for (var i = 1; i < 6; z += a[i++])
      Console.WriteLine("{0}: {1,4}", i, a[i]);
    Console.WriteLine("T: {0,4}", z);
  }
}

