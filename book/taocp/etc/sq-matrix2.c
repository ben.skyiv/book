#include <stdio.h>

int main(void)
{
  for (int d = 1; d < 10; d++) {
    printf("%d:", d);
    for (int c = 0; c < d; c++)
      printf(" %4d", d*d*d*d - c*c*c*c);
    printf("\n");
  }
  printf(">>");
  for (int c = 0; c < 9; c++) printf(" %4d", c);
  printf("\n");
}
