https://www-cs-faculty.stanford.edu/~knuth/sgb.html
$ lftp ftp.cs.stanford.edu
> cd pub/sgb
> get boilerplate.w
> get gb_flip.w
> bye
$ cweave boilerplate.w
$ cweave gb_flip.w
$ xetex gb_flip.tex
$ xdg-open gb_flip.pdf
$ ctangle gb_flip.w
$ cc test_flip.c gb_flip.c
./a.out
OK, the gb_flip routines seem to work!
