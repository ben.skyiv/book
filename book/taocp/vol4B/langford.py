from sys import argv
n = int(argv[1])  # see page 70 of TAOCP vol4B.en
for c in ["{:02d}".format(i + 1) for i in range(n)]: print(c, end=' ')
for c in ["s{:02d}".format(i + 1) for i in range(2*n)]: print(c, end=' ')
print()
for i in range(1, n+1):
    for j in range(1, 2*n):
        k = i + j + 1
        if k > 2*n: continue
        # if i == 1 and j >= n: continue  # see ans7.2.2.1--15
        if j > n // 2 and i == n - (1 if n % 2 == 0 else 0): continue
        print("{:02d} s{:02d} s{:02d}".format(i, j, k))
