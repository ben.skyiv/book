https://www-cs-faculty.stanford.edu/~knuth/programs.html
$ wget https://www-cs-faculty.stanford.edu/~knuth/programs/dlx1.w
$ cweave dlx1.w
$ xetex dlx1.tex
$ ctangle dlx1.w
$ make dlx1
$ mv dlx1 ~/bin

$ dlx1 m1 < test1.txt
(6 options, 5+2 items, 22 entries successfully read)
1:
 A D (2 of 2)
 E F C (1 of 1)
 B G (1 of 1)
Altogether 1 solution, 418+468 mems, 30 updates, 620 bytes, 6 nodes, ccost 7%.

$ python langford.py 3 | dlx1 m1
(8 options, 9+0 items, 32 entries successfully read)
1:
 03 s01 s05 (1 of 1)
 02 s03 s06 (1 of 1)
 01 s02 s04 (1 of 1)
Altogether 1 solution, 729+394 mems, 25 updates, 844 bytes, 4 nodes, ccost 10%.

$ time python langford.py 16 | dlx1 m100000000
(352 options, 48+0 items, 1408 entries successfully read)
 after 10000000453 mems: 2703674 sols, 18 2e 3c 8a 39 56 55 44 23 13 23 0.01096
...
 after 1120000000012 mems: 325693983 sols, 88 dd 8c 3a 68 17 56 55 14 33 12 22 11 11 0.99621
Altogether 326721800 solutions, 47210+1123272951633 mems, 70526170851 updates, 24160 bytes, 5087679409 nodes, ccost 8%.
real	32m1.525s
