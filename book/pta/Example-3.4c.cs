using System;

static class Example34c
{
  static int total = 20, repeat = (int)1e6;
  static Random rand = new Random();

  static int GetPTimes()
  {
    int z = 0, x = 0;
    for (int d, t = 1; t <= total; t++) {
      x += d = (rand.Next(2) == 0) ? 1 : -1;
      if (x > 0 || x == 0 && d < 0) z++;
    }
    return z;
  }

  static void Main()
  {
    int z = 0, k = 16;
    for (var i = 0; i < repeat; i++) {
      var x = GetPTimes();
      if (x >= k || x  <= total - k) z++;
    }
    Console.WriteLine("{0}", (double)z / repeat);
  }
}
