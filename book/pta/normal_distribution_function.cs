using System;
using System.IO;

static class Test
{
    static readonly int n = 330;
    static double [] a = new double[n];
    static double [] b = new double[n];

    static void FillA(string fileName)
    {
        var i = 0;
        foreach (var s in File.ReadLines(fileName))
            a[i++] = double.Parse(s.Split(':')[1]);
    }

    static void FillB(string fileName)
    {
        var i = 0;
        foreach (var s in File.ReadLines(fileName)) {
            var ss = s.Substring(0, 73).Split('&');
            for (var j = 0; j <= 9; j++)
                b[i++] = double.Parse(ss[j + 1]);
        }
    }

    static void Main()
    {
        FillA("normal_distribution_function.txt");
        FillB("normal_distribution_function.tex");
        for (var i = 0; i < n; i++) {
            if (a[i] == b[i]) continue;
            Console.WriteLine("{0:F2}: {1:F4} {2:F4} {3:F0}",
                i / 100.0, a[i], b[i], Math.Abs(a[i] - b[i]) * 10000);
        }
    }
}
