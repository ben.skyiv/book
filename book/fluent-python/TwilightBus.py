class TwilightBus:
    """让乘客销声匿迹的巴士"""

    def __init__(self, passengers=None):
        if passengers is None:
            self.passengers = []  # 1
        else:
            self.passengers = passengers  # 2

    def pick(self, name):
        self.passengers.append(name)

    def drop(self, name):
        self.passengers.remove(name)  # 3

