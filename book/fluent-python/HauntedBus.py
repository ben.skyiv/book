class HauntedBus:
    """备受幽灵乘客折磨的巴士"""

    def __init__(self, passengers=[]):  # 1
        self.passengers = passengers  # 2

    def pick(self, name):
        self.passengers.append(name)  # 3

    def drop(self, name):
        self.passengers.remove(name)

