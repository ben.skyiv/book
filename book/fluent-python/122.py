import unicodedata
import string

def shave_marks(txt):
    """去掉全部变音符号"""
    norm_txt = unicodedata.normalize('NFD', txt)  # 1
    shaved = ''.join(c for c in norm_txt
                     if not unicodedata.combining(c))  # 2
    return unicodedata.normalize('NFC', shaved)  # 3

def shave_marks_latin(txt):
    """把拉丁基字符中的所有变音符号删除"""
    norm_txt = unicodedata.normalize('NFD', txt)  # 1
    latin_base = False
    keepers = []
    for c in norm_txt:
        if unicodedata.combining(c) and latin_base:   # 2
            continue  # 忽略拉丁基字符上的变音符号
        keepers.append(c)                             # 3
        # 如果不是组合字符，那就是新的基字符
        if not unicodedata.combining(c):              # 4
            latin_base = c in string.ascii_letters
    shaved = ''.join(keepers)
    return unicodedata.normalize('NFC', shaved)   # 5

single_map = str.maketrans("""‚ƒ„†ˆ‹‘’“”•–—˜›""",  # 1
                           """'f"*^<''""---~>""")

multi_map = str.maketrans({  # 2
    '€': '<euro>',
    '…': '...',
    'Œ': 'OE',
    '™': '(TM)',
    'œ': 'oe',
    '‰': '<per mille>',
    '‡': '**',
})

multi_map.update(single_map)  # 3


def dewinize(txt):
    """把Win1252符号替换成ASCII字符或序列"""
    return txt.translate(multi_map)  # 4


def asciize(txt):
    no_marks = shave_marks_latin(dewinize(txt))     # 5
    no_marks = no_marks.replace('ß', 'ss')          # 6
    return unicodedata.normalize('NFKC', no_marks)  # 7

