class DemoException(Exception):
    """为这次演示定义的异常类型。"""

def demo_exc_handling():
    print('-> coroutine started')
    while True:
        try:
            x = yield
        except DemoException:  # 1
            print('*** DemoException handled. Continuing...')
        else:  # 2
            print('-> coroutine received: {!r}'.format(x))
    raise RuntimeError('This line should never run.')  # 3

