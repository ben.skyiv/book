import tkinter as tk
import tkinter.ttk as ttk

def clicked():
  txt.delete(0, "end")
  txt.insert(0, lsb.get(lsb.curselection()))

w = tk.Tk()
w.title("测试")
sw = w.winfo_screenwidth()
sh = w.winfo_screenheight()
w.geometry("600x400+2000+100")
lbl = tk.Label(w, text="{}x{}".format(sw, sh), font=("宋体", 50))
txt = tk.Entry(w, width=50)
txt.focus()
cmb = ttk.Combobox(w)
cmb['values'] = (1,2,3,4,"数据")
cmb.current(1)
lsb = tk.Listbox(w, height=6)
for item in ["韦小宝", "任盈盈", "田伯光", "上官明华", "萧峰"]: lsb.insert("end", item)
lsb.selection_set(3)
btn = tk.Button(w, text="开始", bg="orange", fg="red", command=clicked)
lbl.grid(column=0, row=0, sticky="w")
txt.grid(column=0, row=1, sticky="w")
lsb.grid(column=0, row=2, sticky="w")
cmb.grid(column=0, row=3, sticky="w")
btn.grid(column=0, row=4, sticky="w")
w.mainloop()
