using System;

sealed class E463
{
  static readonly int n = 100;
  static int[] f = new int[n + 1];

  static int F(int n)
  {
    if (n == 1) return f[1] = 1;
    if (n == 3) return f[3] = 3;
    if (n % 2 == 0) return f[n] = f[n / 2];
    int k = n/4, a = f[2*k+1], b = f[k];
    return f[n] = (n%4 == 1) ? (2*a-b) : (3*a-2*b);
  }

  static void Main()
  {
    long sum = 0;
    for (var i = 1; i <= n; i++)
    {
      sum += F(i);
//if (i % 2 == 1) Console.Write("{0}:{1} ", i, f[i]);
    }
    Console.WriteLine(sum);
  }
}

