import Math.NumberTheory.Primes.Factorisation
main = print $ length $ filter (<=10) [ div (τ x) 2 | x <- [2 .. div (10^6) 4] ]

