eumu(n)={s=0;fordiv(n,d,s+=eulerphi(d)*moebius(n/d));return(s)}
D(n)=-1+sum(e=1,logint(n,2),(moebius(e)*sqrtnint(n,e)\
-2*eumu(e))*(sqrtnint(n,e)-1)+2*eulerphi(e))

