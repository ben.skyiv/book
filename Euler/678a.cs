using System;
using System.Numerics;
using System.Collections.Generic;

static class E678A
{
  static readonly long n = (long)1e18;

  static long L(double a) { return (long)(a + 0.5); }

  static List<(int, int)> PFS(long n)
  { // 不计素因子 2
    while (n % 2 == 0) n /= 2;
    var list = new List<(int, int)>();
    for (int p = 3; n > 1; p += 2) {
      if (n % p != 0) continue;
      var e = 0;
      for (; n % p == 0; e++) n /= p;
      list.Add((p, e));
    }
    return list;
  }

  static long R2(int f, List<(int, int)> list)
  {
    long z = 4;
    var odd = false;
    foreach (var pe in list) {
      var (p, e) = pe;
      if (p % 4 == 1) z *= f * e + 1;
      else if (f * e % 2 == 1) odd = true;
    }
    return odd ? 0 : z;
  }

  static void Main()
  {
    double nn = Math.Log(n), n2 = Math.Log(2);
    long z = 0;
    for (int f = 3; f <= (int)L(nn / n2); f++) {
      for (long c = 2; ; ++c) {
        if (BigInteger.Pow(c, f) > n) break;
        var list = PFS(c);
        var r2 = R2(f, list) / 8;
        if (r2 == 0) continue;
        z += r2;
        // Console.Write("{0,2}: {1,3}: {2,4}: ", f, c, r2);
        // foreach (var pe in list) Console.Write(pe);
        // Console.WriteLine();
      }
      Console.WriteLine("{0,2}: {1}", f, z);
    }
    Console.WriteLine("F2({0:N0}) = {1}", n, z);
  }
}

