#include <iostream>
#include <cassert>

const long m = 1e8; // < 6'469'693'230 for cz = 10
const int n = 10'001, cz = 10;

typedef struct { int ps[cz]; char ks[cz], n; } item;
template<class T> T gcd(T x, T y) { return x ? gcd(y%x, x) : y; }

void factor(long i0, item a[], const char c[])
{
  static long t[n];
  for (int i = 0; i < n; i++) t[i] = 1, a[i].n = 0;
  for (int p = 2; (long)p * p < i0 + n; p++)
    if (!c[p])
      for (long q = p; q < i0 + n; q *= p)
        for (int i = q-1-(i0-1)%q; i < n; t[i] *= p, i += q)
          if (q != p) ++a[i].ks[a[i].n - 1];
          else a[i].ps[a[i].n] = p, a[i].ks[a[i].n++] = 1;
  for (int i = 0; i < n; i++) {
    if (i + i0 == t[i]) continue;
    a[i].ps[a[i].n] = (i + i0) / t[i];
    a[i].ks[a[i].n++] = 1;
  }
}

void split(const item &n, int &x, long &y)
{ // n == x^2 * y, y is square-free
  y = x = 1;
  for (int k = 0; k < n.n; k++) {
    if (n.ks[k] & 1) y *= n.ps[k];
    for (int i = 0; i < n.ks[k] / 2; i++) x *= n.ps[k];
  }
}

long s1(long z, int x, int v)
{
  int d = gcd(x, v); x = x / d; v = v / d;
  for (long m, n = z; n % x == 0; n = m)
    z += m = n / x * v;
  return z;
}

long s_value(int x, long y)
{
  if (x == 1) return 0;
  long z = 0, n = y*x*x;
  for (int v = x-1; v > 0; v--)
    z = std::max(z, s1(n, x, v));
  return z;
}

int main()
{
  assert((long)n*n > m+n);
  static item a[n];
  static char c[n];
  for (int i = 2; i * i < n; i++)
    if (!c[i])
      for (int j = i * i; j < n; j += i) c[j] = 1;
  long s0 = 0, s = 0, z = 0;
  for (long i0 = -n, i = 4; i <= m; s0 = s, i++) {
    if (i >= i0 + n) factor(i0 = i, a, c);
    int x; long y; split(a[i-i0], x, y);
    s = std::max(s, s_value(x, y));
    z += (i&1) ? (-s) : s;
    if (s != s0) std::cerr << i << " ";
  }
  std::cout << "T(" << m << ") = " << z << std::endl;
}

