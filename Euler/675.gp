\\ http://oeis.org/A048691
\\ Multiplicative with S(p^e)=2e+1
S(n)=sumdiv(n,d,1<<omega(d))
F(n)=sum(i=2,n,S(i!))

