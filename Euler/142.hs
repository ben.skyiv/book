import Math.NumberTheory.Powers.Squares ( isSquare' )

main = let n = 20 in print $ sum $ (\(u,v,w) -> let x = div (u+v) 2 in
  [x, div (u-v) 2, w-x]) $ head [(div c 4, a2*b2, div (a2*(b2+1)^2) 4)
  | a <- [3,5..n], b <- [a+2,a+4..n], let a2 = a^2; b2 = b^2,
  let c = (a2+b2)*(a2*b2+1), isSquare' c]

