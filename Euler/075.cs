using System;
using System.Diagnostics;

namespace euler {
    class Problem75 {

        public static void Main(string[] args) {
            new Problem75().Bruteforce();
        }


        public void Bruteforce() {
            Stopwatch clock = Stopwatch.StartNew();

            int limit = 1500000;
            int[] triangles = new int[limit+1];
                                    
            int result =0;
            int mlimit = (int)Math.Sqrt(limit / 2);

            for (long m = 2; m < mlimit; m++) {
                for (long n = 1; n < m; n++) {
                    if (((n + m) % 2) == 1 && gcd(n, m) == 1) {
                        long a = m * m + n * n;
                        long b = m * m - n * n;
                        long c = 2 * m * n;                        
                        long p = a + b + c;                                                
                        while(p <= limit){
                            triangles[p]++;
                            if (triangles[p] == 1) result++;
                            if (triangles[p] == 2) result--;
                            p += a+b+c;
                        }
                    }
                }
            }
            
            clock.Stop();
            Console.WriteLine("There are {0} triangles which can be bent one way", result);
            Console.WriteLine("Solution took {0} ms", clock.ElapsedMilliseconds);
        }
       
        private long gcd(long a, long b) {
            long y, x;

            if (a > b) {
                x = a;
                y = b;
            } else {
                x = b;
                y = a;
            }

            while (x % y != 0) {
                long temp = x;
                x = y;
                y = temp % x;
            }

            return y;
        }
    }        
}
