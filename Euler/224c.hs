-- Problem 224: (2,2,3) Problem 223: (1,1,1) (1,2,2)
f (a,b,c) = (2*c+b+2*a, 2*c+2*b+a, 3*c+2*b+2*a)
g (a,b,c) = (2*c+b-2*a, 2*c+2*b-a, 3*c+2*b-2*a)
h (a,b,c) = (2*c-2*b+a, 2*c-b+2*a, 3*c-2*b+2*a)

