#include <iostream>

int gcd(int x, int y) { return x ? gcd(y%x, x) : y; }

long s1(long z, int x, int v)
{
  int d = gcd(x, v); x = x / d; v = v / d;
  for (long m, n = z; n % x == 0; n = m)
    z += m = n / x * v;
  return z;
}

long s_value(int x, long y)
{
  if (x == 1) return 0;
  long n = y*x*x, z0 = 0, z;
  int v9 = -1;
  for (int v = x-1; v > 0; z0 = z, v--)
    if ((z = s1(n, x, v)) < z0) z = z0;
    else v9 = v;
  std::cout << "(" << x << "," << v9 << ")" << std::endl;
  return z;
}

int main()
{ // S(10^17) = 864'827'525'163'591'010; // (100,99)
  int x = 1e8;
  long y = 10, n = y*x*x;
  std::cout << "S(10^17): " << s_value(x, y)  << std::endl;
  std::cout << "(100,99): " << s1(n, 100, 99) << std::endl;
  std::cout << "(100,90): " << s1(n, 100, 90) << std::endl;
}

