#include <stdio.h>

int main(void)
{ // m7 * 7 == 1 (mod m), n must be multiple of 10
  const int m = 1117117717, m7 = 478764736, n = (int)1e9;
  long a = 0, d = 1, z = 15;
  for (int i = 2; i < n; i++, d = d * 7 % m)
    z = (z + 6 * (a = (a + 3 * d) * 7 % m) + 105 * d) % m;
  const int ks[] = {2,3,5,5,0,4,3,1,1,6};
  int k = ks[0]; long w = 2 * a + d;
  for (int i = 1; i <= n - 2; i++) {
    a = (a * m7 + (k - 3) * (d =  d * m7 % m)) % m;
    k = (i == n - 2) ? 2 : ks[i % 10];
    w = (w + k * a + k * (k - 1) / 2 * d) % m;
  }
  z -= w; if (z < 0) z += m;
  printf("H(%d) = %ld\n", n, z);
}

