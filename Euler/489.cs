using System;
using big = System.Numerics.BigInteger;

sealed class E489
{
  big Gcd(big a, big b) { return (b == 0) ? a : Gcd(b, a % b); }
  
  long G(int a, int b)
  {
    long a3 = a*a*a, z = a * (a3*a3 + 27L*b*b), i = 0;
    big max = 0;
    for (long n = 0; n <= z; n++)
    {
      big n1 = n, n2 = n + a;
      big g = Gcd(n1*n1*n1 + b, n2*n2*n2 + b);
      if (g == z) return n;
      if (g <= max) continue;
      max = g; i = n;
    }
    return i;
  }

  long H(int m, int n)
  {
    long v = 0;
    for (int a = 1; a <= m; a++)
    {
Console.Error.Write(a + ":");
      for (int b = 1; b <= n; b++)
      {
        v += G(a, b);
Console.Error.Write(b + ".");
      }
Console.Error.WriteLine();
    }
    return v;
  }

  static void Main()
  {
    Console.WriteLine(new E489().H(5, 5));
  }
}

