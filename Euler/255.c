#include <stdio.h>

long solve(long s, long a, long b, long c)
{
  s += b - a + 1;
  long n = (c + (a + c - 1) / c) / 2 - 1;
  for (long m = c*((2*n+1)-c); a <= b; a = m + 1) {
    if ((m += (c << 1)) > b) m = b;
    if (++n != c) s = solve(s, a, m, n);
  }
  return s;
}

int main()
{
  long a = 10000000000000, b = 10*a-1, c = 7000000;
  printf("%.10lf\n", solve(0, a, b, c) / (b-a+1.0));
  return 0;
}

