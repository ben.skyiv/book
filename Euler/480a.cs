using System;
using System.Linq;
using Skyiv.Utils;

sealed class E480A
{
  static void Main()
  {
    Console.WriteLine((new int[]{6,1,1,6,4,1,1,5,1,1,5,1,3,4,4,2,1,1}.
      PermutationFrom0To(15).Sum() - 1).ToString("N0"));
  }
}

