using System;
using System.Collections.Generic;

static class E638
{
  sealed class Pt
  {
    public static Pt Zero = new Pt(0, 0);
    public int X, Y;
    public Pt(int x, int y) { X = x; Y = y; }
    public Pt Clone() { return new Pt(X, Y); }
    public override string ToString() { return string.Format("{0},{1}|", X, Y); }
  }

  static void F(int x, int y, List<Pt> a)
  {
    if (x < 0 || y < 0) return;
    a.Add(new Pt(x, y));
    if (x == 0 && y == 0) return;
    F(x - 1, y, a);
    F(x, y - 1, a);
  }
  
  static int GetArea(Pt[] b)
  {
    var z = 0;
    for (var i = 1; i < b.Length; i++)
      if (b[i].Y == b[i - 1].Y) z += b[i].Y;
    return z;
  }

  static int[] GetAreas(int n, List<Pt> a)
  {
    var c = new Pt[a.Count];
    for (var i = 0; i < a.Count; i++) c[i] = a[i].Clone();
    var b = new Pt[n + n + 1];
    var area = new int[n * n + 1];
    for (var i = 0; i < a.Count; i++) {
      if (a[i].X == 0 && a[i].Y == 0) {
        a[i].X = -1; b[0] = Pt.Zero;
        for (int  k = 1, j = i - 1; j >= 0; j--) {
          if (a[j].X < 0) continue;
          b[k++] = c[j];
          a[j].X = -1;
        }
        area[GetArea(b)]++;
      }
    }
    return area;
  }
  
  static void Out(int n)
  {
    var a = new List<Pt>();
    F(n, n, a);
    foreach (var i in GetAreas(n, a)) Console.Write(i + " ");
    Console.WriteLine();
  }

  static void Main(string[] args)
  {
    int n = (args.Length == 0) ? 3 : int.Parse(args[0]);
    for (var i = 1; i <= n; i++) Out(i);
  }
}

