main = let p = 10^6; m = p in print $ (flip div 2) $ sum $
  takeWhile (>0) [(1+2*m-z)*z | b <- [1..], let z = b * (div p b - 1)]

