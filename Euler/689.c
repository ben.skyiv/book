#include <stdio.h>
#include <math.h>

const int n = 44; // 55
const double a = 0.5, b = M_PI * M_PI / 6 - a;
static double z = 0;

void f(int i, double z0, double z1, double z2)
{
  if (++i > n) return; z0 /= 2;
  double u = 1.0 / i / i, v = z1 + u;
  if (v > a) z += z0; else f(i, z0, v, z2);
  if ((v = z2 + u) < b)    f(i, z0, z1, v);
}

int main(void)
{
  printf("(%d) ", n); fflush(stdout);
  f(0, 1.0, 0.0, 0.0);
  printf("%.10lf\n", z);
}
