// opt: see 036_overview.doc

#include <stdio.h>
#include <string.h>

const char *mask[] = { "000","001","010","011","100","101","110","111" };

const char *binary(int n)
{
  static char buf1[23], buf2[65];
  sprintf(buf1, "%o", n);
  int i;
  for (i = 0; buf1[i] != '\0'; i++)
  {
    int k = i * 3;
    const char *v = mask[buf1[i] - '0'];
    buf2[k] = v[0]; buf2[k+1] = v[1]; buf2[k+2] = v[2];
  }
  buf2[i * 3] = '\0';
  for (i = 0; buf2[i] == '0'; ) i++;
  return buf2 + i;
}

int isPalindromeString(const char *s)
{
  for (int i = 0, j = strlen(s) - 1; i < j; i++, j--)
    if (s[i] != s[j])
      return 0;
  return 1;
}

int isPalindrome(int n)
{
  static char buf[65];
  sprintf(buf, "%d", n);
  if (!isPalindromeString(buf)) return 0;
  return isPalindromeString(binary(n));
}

int main(void)
{
  int sum = 0;
  for (int i = 1; i < 1000000; i++)
  {
    if (!isPalindrome(i)) continue;
    printf("%d ", i);
    sum += i;
  }
  printf("[%d]\n", sum);
  return 0;
}
