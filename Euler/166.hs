main = print $ sum [product [let zs = 0:xs; u = maximum zs; v = minimum zs
  in if u-v > 9 then 0 else v-u+10 | xs <- [[c, b-d, a-b-d],
  [b-a, c+d-a, b+d-a], [-b-c, a-b-c-d, -c-d], [a, d, c+d]]] |
  a <- [-9..9], b <- [a-9..a+9], c <- [-9..9], d <- [a-c-9..a-c+9]]

