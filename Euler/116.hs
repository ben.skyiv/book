binomial n m = let f n m = product [n..m] in div (f (m+1) n) $ f 1 (n-m)
f n m = sum $ map (\x -> binomial (n + x - x * m) x) [1..div n m]
main = let n = 50 in print $ sum $ map (f n) [2..4]

