import Math.NumberTheory.Moduli ( chineseRemainder,powerMod )
n = 10^12; m = 5^5; f 0 = 1
f n = g (mod n m) * f (div n 5) * if odd $ div n m then -1 else 1
g n = let z = div n 5 in mod (div (product [z+1 .. n]) (5^z)) m
main = print $ chineseRemainder [(0, 2^5), (f n * powerMod 2 (- sum
  [div n (5^k) | k <- [1 .. floor $ logBase 5 $ fromIntegral n]]) m, m)]

