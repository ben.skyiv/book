using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class E652
{
  static readonly int  m = (int)1e9;
  static readonly long n = (long)1e10;
  static readonly double ln = Math.Log(n);
  static readonly int n2 = (int)Math.Sqrt(n);
  static readonly int n3 = (int)Math.Exp(ln/3) + 1;
  static readonly int n4 = (int)Math.Sqrt(n2);
  static HashSet<(int a, int b)> set = new HashSet<(int a, int b)>();

  static int Gcd(int a, int b) { return b == 0 ? a : Gcd(b, a % b); }
  static int D2(int b) { return Math.Max(0, (int)(ln/Math.Log(b))-1); }

  static (long, long) D1()
  { // Count (a^e, a^f) for: 1 < a, 0 < e < f, a^f <= n
    long b, z1, z = 0, w = 0; int m3 = (int)Math.Log(n, 3);
    for (int e = 1; e < m3; e++)
      for (int f = e + 1; ; f++) {
        if (Gcd(e, f) != 1) continue;
        if ((z1 = (long)Math.Exp(ln/f)) < 3) break;
        if ((b = 1L << f) <= n2) {
          if (b > n4) w += D2((int)b);
          else set.Add((1 << e, (int)b));
          for (int i = 3; (b = ((long)i).Pow(f)) <= n2; i++)
            if (b > n4) w += D2((int)b);
            else set.Add(((int)((long)i).Pow(e), (int)b));
        }
        z += z1 - 2;
      }
    Console.WriteLine("D1: {0} {1} ({2})", z, w, set.Count);
    return (z, w);
  }

  static (long, long) D2()
  { // Count (a^e, b^e) for: 0 < e, 1 < a < b, b != a^i, b^e <= n
    long z = (n2 - n3) * (n2 - n3 + 1) / 2, w = 0;
    for (int a = 2; a < n3; a++) {
      for (int d, b = a + 1; (d = D2(b)) > 0/* && b <= n2*/; b++) {
        if (set.Contains((a, b))) continue;
        for (long a2 = (long)a*a, b2 = (long)b*b; b2 <= n2; a2 *= a, b2 *= b)
          if (b2 > n4) w += D2((int)b2);
          else set.Add(((int)a2, (int)b2));
        z += d;
      }
      if (a % 1000 == 0) Console.Write(".");
    }
    Console.WriteLine("D2: {0} {1} ({2})", z, w, set.Count);
    return (z, w);
  }

  static void Main()
  {
    Console.WriteLine("{0:N0} {1:N0} {2:N0} {3:N0}", n, n2, n3, n4);
    var (z1, z3) = D1(); var (z2, z4) = D2();
    var z = m + 3 - (n * 3 + (z1 + z2 - z3 - z4) * 2) % m;
    Console.WriteLine("D(10^{0}) = {1:N0} ({2} {3}-{4}-{5}) ({6})",
      Math.Log(n,10), z, z1, z2, z3, z4, set.Count);
  }
}

