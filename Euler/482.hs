import Data.Maybe ( fromJust,isNothing )
import Math.NumberTheory.Powers.Squares ( exactSquareRoot )

f p a b c = let (q,r) = divMod (b*c*(b+c-a)) p in
  if r /= 0 then Nothing else exactSquareRoot q
-- S(10^4) = 884,667  18m2.461s
main = let n = 10^3 in print $ let n2 = div n 2 in
  sum $ map sum $
  [[p, fromJust ia, fromJust ib, fromJust ic] |
    a <- [1..n2], b <- [a..n2], c <- [b..n2], a+b > c,
    let p = a+b+c, p <= n,
    let ia = f p a b c, not $ isNothing ia,
    let ib = f p b a c, not $ isNothing ib,
    let ic = f p c a b, not $ isNothing ic]

