a(f) = local(l, s, t, q); l = #f; s = 0; forvec(v = vector(l, i, [0, 1]), q = sum(i = 1, l, v[i]); t = (-1)^(l - q)*Mod(2,10^9)^prod(i = 1, l, f[i] + v[i]); s += t); s;
