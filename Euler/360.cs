// http://en.wikipedia.org/wiki/Pythagorean_quadruple
using System;
using System.Collections.Generic;

sealed class E360
{
  static void Main()
  {
    var set = new HashSet<Tuple<long,long,long>>();
    long sum = 0, r = (long)Math.Pow(5, 10), max = (long)Math.Sqrt(r);
    for (long m = 0; m <= max; m++)
      for (long m2 = m*m, n = 0; n <= max; n++)
        for (long mn = m2 + n*n, q2, q, p2, p = 0;
          (q2 = r - mn - (p2 = p*p)) >= 0 && q2 <= mn - p2; p++)
        {
          if (p2 < q2 || (q = (long)Math.Sqrt(q2)) * q != q2) continue;
          long a = mn - p2 - q2, b = (m*q + n*p) << 1;
          long c = Math.Abs((n*q - m*p) << 1);
          if (a > b) { long t = a; a = b; b = t; }
          if (b > c) { long t = b; b = c; c = t; }
          if (a > b) { long t = a; a = b; b = t; }
          if (!set.Add(Tuple.Create(a, b, c))) continue;
          sum += (a + b + c) * ((a < b && b < c) ? 6 : (a < c) ? 3 : 1) *
            (1 << ((a > 0) ? 1 : 0) + ((b > 0) ? 1 : 0) + ((c > 0) ? 1 : 0));
        }
    Console.WriteLine(sum << 10); // S(10^10) == 1024 * S(5^10)
  }
}

