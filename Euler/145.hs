import Data.Digits ( digitsRev,unDigits )

main = let n = 10^9 in print $ length $ filter (\n -> let x = digitsRev 10 n
  in ((/=0) $ head x) && (all odd $ digitsRev 10 $ n + unDigits 10 x)) [1..n]

