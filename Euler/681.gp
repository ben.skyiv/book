SP(nmax) = {
  parsum(n = 1, nmax,
    my(res = 0, k1, k2, k3, d4);
    fordiv (k1 = n^2, d1,
      if (d1^4 > k1, break);
      fordiv (k2 = k1 \ d1, d2,
        if (d2^3 > k2, break);
        if (d2 < d1, next);
        fordiv (k3 = k2 \ d2, d3,
          if (d3^2 > k3, break);
          if (d3 < d2, next);
          d4 = k3 \ d3;
          if (d1 + d2 + d3 <= d4 || bitand(d1 + d2 + d3 + d4, 1), next);
          res += d1 + d2 + d3 + d4)));
    res) }
print(SP(10^6))
quit()
