using System;
using System.Numerics;

sealed class E097
{
  static void Main()
  {
    var mod = BigInteger.Pow(10, 10);
    Console.WriteLine(BigInteger.ModPow(2, 7830457, mod) * 28433 % mod + 1);
  }
}
