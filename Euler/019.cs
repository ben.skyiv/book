using System;

sealed class E019
{
  static void Main()
  {
    var sum = 0;
    var date1 = new DateTime(1901, 1, 1);
    var date2 = new DateTime(2001, 1, 1);
    for (var date = date1; date < date2; date = date.AddMonths(1))
      if (date.DayOfWeek == DayOfWeek.Sunday) sum++;
    Console.WriteLine(sum);
  }
}
