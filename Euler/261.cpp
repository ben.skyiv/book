#include <cstdio>
typedef long int64;
typedef unsigned long uint64;
const uint64 M=10000000000;
uint64 sum=0;
bool fx(uint64 m, uint64 a, uint64 b, uint64 c)
{
   uint64 k=(4*m+2)*a-2*m*m-b, nextm=a+c-1;
   if (k>M) return a>m;
   sum+=k;
   c+=k+a-m;
   if (a>m) fx(nextm,c-1,a,k+1);
   return fx(m,k,a,c);
}
int main(int ac, char** av)
{
  uint64 m=0;
   while (++m, fx(m,m,0,1)) ;
   printf("%lu ", sum);
   int64 a=1, b=0, c=0, k;
   while (m=18*a-b, b=c-16, c=a=m, k=2*m*(m+1), k<=M) sum-=k;
   printf("%lu\n", sum);
   return 0;
}
