import Math.NumberTheory.Primes.Testing
main = print $ length [ n | n <- [2..50000000], millerRabinV 2 (2*n*n-1) ]

