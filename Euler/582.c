#include <stdio.h>
#include <math.h>

long eval(long x, long y, long n)
{
  double s = sqrt(3);
  if (x % 2 == 1 && y % 2 == 0)
  { long z = x; x = x * 2 + y * 3; y = z + y * 2; }
  return 1 + (long)((n - log10((y*3 + x*s)/s/4))
    / log10((y % 2 == 0) ? (2 + s) : (7 + s*4)));
}

long count(long d, long n)
{
  long y1 = d * 4, z = eval(y1 + d * 3, y1, n);
  for (long y = d + 1; y < y1; y++) {
    long x2 = y*y*3 + d*d, x = (long)sqrt(x2);
    if (x * x == x2) z += eval(x, y, n);
  }
  return z;
}

int main(void)
{
  for (long z = 0, n = 10; n <= 1e9; n *= 10, z = 0) {
    for (int d = 1; d <= 100; d++) z += count(d, n);
    printf("T(10^10^%d) = %ld\n", (int)log10(n), z);
  }
}

