f n m a b c sum
  | k > n     = (a > m, sum)
  | a <= m    = f n m k a d $ k+sum
  | otherwise = f n m k a d $ snd $ f n (a+c-1) (d-1) a (k+1) $ k+sum
  where k = (4*m+2)*a - 2*m*m - b; d = c + k + a - m

g n m sum
  | fst v     = g n (m+1) (snd v)
  | otherwise = v
  where v = f n m m 0 1 sum

h n a b c k sum = let m = 18*a - b; k = 2*m*(m+1)
  in if k > n then sum else h n m (c-16) m k (sum-k)

main = let n = 10^10 in print $ h n 1 0 0 0 $ snd $ g n 1 0

