using System;
using Skyiv.Utils;

static class E694
{
  static readonly long n = (long)1e18;
  static readonly int[] primes =
    Primes.GetPrimes(0, 1 + (int)Math.Exp(Math.Log(n) / 3));
  static long z = n;
  
  static void Compute(int k, long n)
  {
    if (k < 0) return;
    Compute(k - 1, n);
    int p = primes[k];
    for (n /= (long)p * p * p; n > 0; z += n, n /= p)
      Compute(k - 1, n);
  }

  static void Main()
  {
    Compute(primes.Length - 1, n);
    Console.WriteLine("S({0:N0}) = {1:N0}", n, z);
  }
}
