\\ $ time gp -q -p 20000000000000000 632a.gp
c(n,e)=z=vector(e);forsquarefree(i=1,floor(sqrt(n)),\
k=moebius(i)*(n\(i[1]*i[1]));\
for(j=1,#z,z[j]+=k*binomial(#i[2]~,j)));z
f(n,e)=z=c(n,e);s=sum(i=1,#z,abs(z[i]));\
lift(Mod(prod(i=1,#z,abs(z[i]),n-s),1000000007))
print(f(10^16,8));quit()

