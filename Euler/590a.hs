import Data.List ( (\\),group )
import Math.Combinat.Numbers ( binomial )
import Math.NumberTheory.Primes ( primes )
import Math.NumberTheory.Powers ( powerMod )
m = 9; m1 = 10^m; m2 = 4*5^(m-1)
p2 n = powerMod 2 (if n < m then n else mod (n-m) m2 + m) m1
f1 :: Integer -> [Integer]; f1 n = map (\i -> floor $ logBase
  (fromIntegral i) (fromIntegral n)) $ takeWhile (<=n) primes
f2 = map (\xs -> (head xs, length xs)) . group
f3 = map (\(a,b) -> [(replicate i a, binomial b i) | i <- [0..b]])
f4 xs = zip
  (foldl (\a b -> [i++j         | i <- a, j <- b]) [[]] $ map (map fst) xs)
  (foldl (\a b -> [mod (i*j) m1 | i <- a, j <- b]) [1]  $ map (map snd) xs)
hl n = let zs = f1 n; xss = f4 $ f3 $ f2 zs
  in foldl (\z (xs,i) -> mod (z +
  (if even (length zs - length xs) then 1 else -1) * i * p2
  (product (map succ xs) * product (zs \\ xs))) m1) 0 xss
main = print $ hl 5000

