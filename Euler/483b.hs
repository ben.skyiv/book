import Data.Ratio ( (%) )
f n = product [2..n]
combin n r = if r > n-r then combin n (n-r) else div (product [n-r+1..n]) $ f r
r z = let m = length z in
  foldl (\s x -> 1 % fromIntegral (x+1) * s) (f (sum z) % product z)
  [length $ takeWhile (\j -> z!!i == z!!j) [i+1..m-1] | i <- [0..m-1]]
t 2 n k = [[i,j] | i <- [k .. div n 2], j <- [i..n-i]]
t l n k = concat [map (i:) $ t (l-1) (n-i) i | i <- [k .. div n l]]
g n = let m = f n in sum $ concat [[fromRational $ 
  (sum [combin i (k-1) * f (k-1) | i <- [k-1..n-1]]) * k * k % m | k <- [2..n]],
  map (\z -> fromRational $ (foldl1 lcm z)^2 * combin n (sum z) % m * r z) $
  concat $ takeWhile (not . null) [t l n 2 | l <- [2..]], [fromRational $ 1%m]]
main = let n = 20 in print $ (n, g n)

