import Data.List ( delete,isInfixOf,sort )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Testing ( isPrime )

main = let n = 200; m = 10^12 in print $ (!!(n-1)) $ sort $
  filter primeProof $ filter (isInfixOf "200" . show) $ [p'*q' |
  (q,q') <- takeWhile ((< div m 4) . snd) $ map (\q -> (q,q^3)) $ primes,
  p' <- takeWhile (< div m q') $ map (^2) $ delete q primes] where
  primeProof n = all (\x -> let m = 10^x in all (not . isPrime .
    ((n - mod n (m*10) + mod n m)+) . (m*)) [0..9])
    [0 .. floor $ logBase 10 $ fromIntegral n]

