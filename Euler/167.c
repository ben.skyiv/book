#include <stdio.h>

int main(void)
{
  const long long K = 100000000000 - 3;
  static int v[4*(10+1)], w[10000000];
  long long sum = 0;
  for (int n = 2; n <= 10; n++) {
    int p = 4*n + 4, m = 2*n + 1, u = m, i = 0, k = 3;
    for (; u - m < p; u += 2, i = (i+1) % p, k++)
      w[k-3] = u - 2*n + 1, v[i] = u + p;
    for (int j = 0, z = u -= 2; u - z < p; i = (i+1) % p, k++) {
      if ((z = u, u += 2) == v[j]) u = v[(j+1) % p], j = (j+2) % p;
      w[k-3] = u - m; v[i] = u + p;
    }
    k -= 4; sum += m - 2*(K%k <= m) + K/k*(u-m) + w[K%k];
  }
  printf("%lld\n", sum);
  return 0;
}

