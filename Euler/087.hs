import Data.List
import Data.Numbers.Primes
ps = take 40 primes
main = print $ length $ nub [ x | a <- ps, b <- ps, c <- ps, let x = a^2+b^3+c^4, x <= 500 ]

