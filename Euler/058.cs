using System;

sealed class E058
{
  bool[] composite;

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }

  double Compute(int n)
  {
    int level = (n-1)/2, primes = 0;
    long v = 1;
    for (int i = 1; i <= level; i++)
      for (int k = 2 * i, j = 0; j < 4; j++)
        if (!composite[v += k]) primes++;
    return primes / (4.0 * level + 1) * 100;
  }
  
  int Compute()
  {
    int n = 26591;
    composite = Sieve(n * n);
    for (var i = 26001; i <= n; i += 2)
      if (Compute(i) < 10) return i;
    return 0;
  }

  static void Main()
  {
    Console.WriteLine(new E058().Compute());
  }
}
