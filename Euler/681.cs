using System;
using System.Collections.Generic;

static class E681
{
  static readonly int n = 100;
  
  static IEnumerable<(int,int,int)> Triplets()
  {
    for (int i9 = (int)Math.Sqrt(n/6), i = 1; i <= i9; i++)
      for (int j9 = Math.Max((n-3)/6/i,
        (int)Math.Sqrt(n/3+i*i-1)), j = i + 1; j <= j9; j++) {
        int p = j*j-i*i, q = 2*i*j, r = j*j+i*i;
        if (p * q > n) continue;
        yield return (p, q, r);
        yield return (q, p, r);
      }
  }
  
  static HashSet<(int,int,int,int)> FromTriplets()
  {
    var set = new HashSet<(int,int,int,int)>();
    foreach (var t in Triplets()) {
      var (p, q, r) = t;
      for (int k9 = n/p - q, k = 1; k <= k9; k++) {
        var a = new int[] {k, r, r, k + q + q};
        Array.Sort(a);
        set.Add((a[0],a[1],a[2],a[3]));
      }
    }
    return set;
  }

  static void Main()
  {
    var set = FromTriplets();
    long z = 0;
    for (int a = 1; a <= n; a++)
      for (int b = a; b <= n; b++)
        for (int c = b; c <= n; c++)
          for (int d9 = Math.Min(n, a+b+c-1), d = c; d <= d9; d++) {
            double s = (a + b + c + d) / 2.0;
            double area = Math.Sqrt((s-a)*(s-b)*(s-c)*(s-d));
            long area2 = (long)(area + 0.5);
            if (area2 > n) continue;
            if (area2 != area) continue;
            if (a == b && c == d) continue;
            var t = (a,b,c,d);
            if (set.Contains(t)) { set.Remove(t); continue; }
            z++;
            Console.WriteLine("{0} {1,2} {2,2} {3,2}: {4}",a,b,c,d,area2);
          }
    Console.WriteLine("{0} {1}", z, set.Count);
  }
}
