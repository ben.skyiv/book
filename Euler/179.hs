import Math.NumberTheory.Primes.Factorisation ( τ )

main = let n = 10^7 in print $ let xs = [τ i | i <- [2..n]]
  in length $ filter id $ zipWith (==) xs $ tail xs

