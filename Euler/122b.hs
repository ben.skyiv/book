import Data.Array ( (!),(//),listArray )
main = let n = 200; s = [23,43,59,77,83,107,149,163,165,179] in let
  c = foldl (\a i -> if (a ! i /= 0) then a else a // [(j,i) | j <-
    [i*i, i*i+i .. n], a ! j == 0]) (listArray (1,n) $ repeat 0)
    [2 .. floor $ sqrt $ fromIntegral n] in
  print $ sum $ foldl (\a i -> min (1 + head a) (let j = c ! i in
  if j == 0 then i else a !! (i - 1 - j) + a !! (i - 1 - div i j))
  - (if elem i s then 1 else 0) : a) [0] [2..n]

