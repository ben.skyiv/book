#include <stdio.h>

int main(void)
{
  const int n = 1000, m = n * (n + 1) / 2;
  static int a[m], b[m];
  long t = 0;
  for (int i = 0, j = 0, k = 0; k < m; k++) {
    a[k] = b[k] = (int)((t=(615949*t+797807)%(1<<20))-(1<<19));
    if (k == 0) continue;
    if (++j > i) j = 0, i++;
    else b[k] += b[k-1];
  }
  int z = b[0];
  for (int i = 0; i < n; i++)
    for (int v, j = 0; j <= i; j++) {
      if (z > (v = a[i * (i + 1) / 2 + j])) z = v;
      for (int k = 1; k + i < n; k++) {
        v += b[(i + k) * (i + k + 1) / 2 + j + k];
        if (j > 0) v -= b[(i + k) * (i + k + 1) / 2 + j - 1];
        if (z > v) z = v;
      }
    }
 printf("%d\n", z);
 return 0;
}

