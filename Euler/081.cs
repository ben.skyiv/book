using System;
using System.IO;

sealed class E081
{
  static readonly int size = 80;

  int Compute()
  {
    var grid = Read();
    for (var i = size - 2; i >= 0; i--)
    {
      grid[size-1, i] += grid[size-1, i+1];
      grid[i, size-1] += grid[i+1, size-1];
    }
    for (var i = size - 2; i >= 0; i--)
      for (var j = size - 2; j >= 0; j--)
        grid[i, j] += Math.Min(grid[i+1, j], grid[i, j+1]);
    return grid[0, 0];
  }

  int[,] Read()
  {
    var i = 0;
    var grid = new int[size, size];
    foreach (var line in File.ReadLines("081.txt"))
    {
      var fields = line.Split(',');
      for (var j = 0; j < fields.Length; j++)
        grid[i, j] = int.Parse(fields[j]);
      i++;
    }
    return grid;
  }

  static void Main()
  {
    Console.WriteLine(new E081().Compute());
  }
}

