import Math.NumberTheory.Powers ( powerMod )
main = let n = 10^8+7; k = 10^4+7; m = 10^8
  in print $ let z = powerMod 2 n m - 1 in mod
  (powerMod 2 (n-k) m * (n + (k-1) * z) * z * z) m

