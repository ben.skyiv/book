import Skyiv.RomanNumerals ( fromRoman,toRoman )
g s = foldl (\z c -> let t = s ++ [c] in
  if t == toRoman (fromRoman t) then t : z else z) [] "IVXLCDM"
f (s1,p1,_) = foldl (\(s2,p2,z)(s,p) -> let a1 = 0.02; a2 = 0.14
  in let s3 = g s; a3 = p / (a2 * fromIntegral (length s3) + a1)
  in (s3 ++ s2, (replicate (length s3) (a2 * a3)) ++ p2, z +
  fromIntegral (fromRoman s) * a1 * a3)) ([],[],0) $ zip s1 p1
main = print $ sum $ takeWhile (\x -> x == 0 || x > 1e-9)
  $ map (\(_,_,z) -> z) $ iterate f ([""], [1], 0)

