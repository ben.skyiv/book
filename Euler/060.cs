using System;
using System.Collections.Generic;

sealed class E060
{
  bool[] composite;
  int[] primes;

  int[] GetPrimes(int max)
  {
    var primes = new List<int>();
    Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) primes.Add(i);
    return primes.ToArray();
  }

  void Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
  }
  
  bool IsPrime(int p1, int p2)
  {
    return !composite[int.Parse(p1.ToString() + p2)];
  }
  
  int Compute()
  {
    primes = GetPrimes(10000000);
    int[] a = { 3, 7, 109 };
    int i = 0;
    while (primes[i] <= a[a.Length - 1]) i++;
    for (; ; i++)
    {
      var p2 = primes[i];
      var ok = true;
      foreach (var p1 in a)
        if (!IsPrime(p1, p2) || !IsPrime(p2, p1))
        { ok = false; break; }
      if (ok) return p2;
    }
  }
  
  static void Main()
  { // TODO -- no finish
    Console.WriteLine(new E060().Compute());
  }
}
