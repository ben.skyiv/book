#include <stdio.h>
#include <math.h>

int main()
{
  const long n = 10000000000, m = n / 4;
  const int d = m / 4 + 1, h = sqrt(n) / 4;
  static char a[d];
  for (int i = 0, i2 = 0; i < h; i++, i2 = i*i)
    for (int j = 0, j2 = 0; i2+j2+j < d; j++, j2= j*j)
      a[i2+j2+j]++;
  long z = 0;
  for (int i=0, j=2, k=2; i < d; k = i==j?k+2:k, j = i==j?j+k:j, i++) {
    if (a[i] <= 2) continue;
    int b = i==j, u = (a[i]-1)*(a[i]-2)/2, v = u * a[i]/3, w = b?1:0;
    for(long r = 4L*i + 1; r <= m; r <<= 1, w = b?1-w:0) z += v - w*u;
  }
  printf("%ld\n", z);
}

