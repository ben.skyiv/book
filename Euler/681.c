#include <stdio.h>
#include <math.h>

int min(int a, int b) { return (a < b) ? a : b; }

int main(void)
{
  int n = (int)1e2;
  long z = 0;
  //for (int q, p = 1; p * p <= n; p++) // M(p,p,q,q) = pq
  //  z += (q = n / p, (3L * p + q) * (q - p + 1));
  for (int a = 1; a <= n; a++)
    for (int b = a; b <= n; b++)
      for (int c = b; c <= n; c++)
        for (int d9 = min(n, a+b+c-1), d = c; d <= d9; d++) {
          double s = (a + b + c + d) / 2.0;
          double area = sqrt((s-a)*(s-b)*(s-c)*(s-d));
          long area2 = (long)(area + 0.5);
          if (area2 > n) continue;
          if (area2 != area) continue;
if (!(a==b&&c==d))printf("%d %2d %2d %2d: %3ld\n",a,b,c,d,area2);
          z += a + b + c + d;
        }
  printf("SP(%d) = %ld\n", n, z);
}
