import Data.List(sort)
import Math.NumberTheory.Primes(factorSieve,sieveFactor)
f (x:xs) m | xs == [] = if x < m then 0 else 1
           | True = sum [f xs (m-i) | i <- [0..min m x]]
main = let n = 10^8; s = factorSieve n in print ((sum $
  map (\n -> let xs = sort $ map snd $ sieveFactor s n
  in f xs $ div (sum xs) 2) [2..n]) + 1)

