-- main = print $ sum [ x | x <- [1..999], mod x 3 == 0 || mod x 5 == 0 ]

import System.Environment

main = do
  s <- getArgs
  print $ let n = read $ head s in (f n 3) + (f n 5) - (f n 15)

f x y = let n = div x y in y * div (n * (n + 1)) 2

