import Data.Set ( empty,insert,toList )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Testing ( isPrime )
import Math.NumberTheory.Powers.Squares ( integerSquareRoot )

m = 10^8; ps = takeWhile (<=m) primes
h n i = let (q,r) = divMod n i in if r == 0 then (fst $ h q i, i) else (n,0)
g n s = if isPrime n then if n <= m then insert n s else s else let
  a = min m $ integerSquareRoot n; z = fst t; w = snd t
  t = foldl (\(s,n) i -> let (k,j) = h n i in (if j == 0 then s
    else insert i s, k)) (s,n) $ takeWhile (<=a) ps
  in if w == 1 || w > m then z else insert w z
f n = sum $ toList $ foldl (\s i -> g i s) empty $
  let n1 = n+1; n2 = n*n; n3 = n2*n; n4 = n2*n2; n5 = n2-n+1
  in [n1, n5, n5+n4-n3, n1*(n3*n4+1)-n3*(n1+n2)]
main = let n = 10^2 in print $ sum [f i | i <- [1..n]]

