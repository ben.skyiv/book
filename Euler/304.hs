import Data.Matrix ( (!),elementwiseUnsafe,fromList,identity,nrows )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )

main = let p = 10^14; n = 10^5; m = 1234567891011 in
  print $ mod (sum $ map (fib m) $ take n $ sieveFrom p) m
fib m n = mod ((powerMod  (fromList 2 2 [1,1,1,0]) (n-2)
  (fromList 2 2 [m,m..]) * fromList 2 1 [1,1]) ! (1,1)) m
powerMod a n m = (\(_,_,x) -> x) $ until (\(n,_,_) -> n <= 0)
  (\(n,a,v) -> (div n 2, elementwiseUnsafe mod (a*a) m,
  if odd n then elementwiseUnsafe mod (v*a) m else v))
  (n, a, identity $ nrows a)

