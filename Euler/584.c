#include <stdio.h>

const int y = 50, p = 4, d = 7;
  
int in2(int a, int b)
{
  if (a > b) { int t = a; a = b; b = t; }
  return b - a < d || y + a - b < d;
}
  
int in4(int a, int b, int c, int d)
{
  return in2(a, b) && in2(a, c) && in2(b, c)
      && in2(a, d) && in2(b, d) && in2(c, d);
}

int in(const int a[], int n)
{
  for (int i = n - 3; i >= 0; i--)
    for (int j = n - 2; j > i; j--)
      for (int k = n - 1; k > j; k--)
        if (in4(a[n], a[k], a[j], a[i])) return 1;
  return 0;
}

int main(void)
{
#define SIZE 7
  static int a[SIZE]; static long n[SIZE];
  for (a[1] = 0; a[1] < y; a[1]++) { fprintf(stderr,"%d ",a[1]);
  for (a[2] = 0; a[2] < y; a[2]++)
  for (a[3] = 0; a[3] < y; a[3]++) if (in(a, 3)) n[3]++; else
  for (a[4] = 0; a[4] < y; a[4]++) if (in(a, 4)) n[4]++; else
  for (a[5] = 0; a[5] < y; a[5]++) if (in(a, 5)) n[5]++; else
  for (a[6] = 0; a[6] < y; a[6]++)               n[6]++; }
  double z = 0, w = (double)y*y*y;
  for (int i = 4; i <= SIZE; i++, w *= y) z += i*n[i-1]/w;
  printf("\nN(%d,%d,%d) = %.8lf\n", y, p, d, z);
}

