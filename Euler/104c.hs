import Data.List
main = print $ head [ i | (i,f) <- zip [1..] fibs,
  let t = sort $ show $ mod f (10^9); h = sort $ take 9 $ show f,
  t == "123456789", t == h ]
  where fibs = 1 : 1 : zipWith (+) fibs (tail fibs)

