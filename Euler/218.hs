import Data.List ( sort )

main = let z = 10^16 in print $ length $
  -- filter (\x -> mod x 6 /= 0 || mod x 28 /= 0) $
  map (\[n,m] -> (m*m-n*n)*m*n) $ map sort [[m,n] |
  p <- [2 .. iSqrt $ iSqrt z], q <- [1..p-1], gcd p q == 1, odd (p+q),
  let m = 2*p*q; n = p*p-q*q, gcd m n == 1, odd (m+n)]
  where iSqrt = floor . sqrt . fromIntegral

