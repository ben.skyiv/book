using System;

sealed class E014
{
  int[] lens;

  int GetLength(long x)
  {
    var length = 1;
    for (var x0 = x; x != 1; length++)
    {
      if (x < x0) return lens[x] + length - 1;
      x = (x % 2 == 0) ? (x / 2) : (3 * x + 1);
    }
    return length;
  }

  int Run(int x)
  {
    lens = new int[x + 1];
    var max = int.MinValue;
    var idx = 0;
    for (var i = 1; i <= x; i++)
    {
      lens[i] = GetLength(i);
      if (max >= lens[i]) continue;
      max = lens[i];
      idx = i;
    }
    return idx;
  }

  static void Main(string[] args)
  {
    Console.WriteLine(new E014().Run(int.Parse(args[0])));
  }
}
