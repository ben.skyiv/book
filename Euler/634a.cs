using System;

static class E634
{
  static void Main()
  {
    long z = 0, n = (long)9e18;
    for (var b = 2; ; b++) {
      var a2 = n / b / b / b;
      if (a2 < 4) break;
      z += (long)Math.Sqrt(a2) - 1;
    }
    Console.WriteLine("F({0:N0}) < {1:N0}", n, z);
  }
}

