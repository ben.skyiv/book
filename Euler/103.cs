using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

sealed class E103
{
  int[] mask = { 1, 2, 4, 8, 0x10, 0x20, 0x40, 0x80, 0x100, 0x200 };
  
  string ToString<T>(HashSet<T> set)
	{
	  if (set == null) return "null";
    var sb = new StringBuilder("{");
    foreach(var v in set) sb.Append(v + ",");
    sb.Length--;
    sb.Append("}");
    return sb.ToString();
  }

  IEnumerable<HashSet<T>> AllSubset<T>(HashSet<T> set)
  { // return all subset except empty-set
    var n = set.Count;
    var array = set.ToArray();
    var list = new List<HashSet<T>>();
    for (var i = 1; i < (1 << n); i++)
    {
      var subset = new HashSet<T>();
      for (var k = 0; k < n; k++)
        if ((i & mask[k]) != 0)
          subset.Add(array[k]);
      list.Add(subset);
    }
    return list;
  }
  
  bool IsSumSet2(HashSet<int> set1, HashSet<int> set2)
  {
    var sum1 = set1.Sum();
    var sum2 = set2.Sum();
    if (sum1 == sum2) return false;
    if (set1.Count > set2.Count && sum1 <= sum2) return false;
    if (set1.Count < set2.Count && sum1 >= sum2) return false;
    return true;
  }
  
  bool IsSumSet(HashSet<int> set, HashSet<int> subset)
  {
    var set2 = new HashSet<int>(set);
    set2.ExceptWith(subset);
    foreach (var subset2 in AllSubset(set2))
      if (!IsSumSet2(subset, subset2)) return false;
    return true;
  }

  bool IsSumSet(HashSet<int> set)
  {
    foreach (var subset in AllSubset(set))
      if (!IsSumSet(set, subset))
        return false;
    return true;
  }

  void Compute()
  {
    for (var i = 31; i < 45; i++)
      for (var j = i + 1; j < 46; j++)
        for (var k = j + 1; k < 47; k++)
          for (var l = k + 1; l < 48; l++)
            for (var m = l + 1; m < 49; m++)
              for (var n = m + 1; n < 50; n++)
    {
      var set = new HashSet<int>(new int[]{20,i,j,k,l,m,n});
      if (!IsSumSet(set)) continue;
      Console.WriteLine("{0} {1}", ToString(set), set.Sum());
    }
  }

  static void Main()
  {
    new E103().Compute();
  }
}
