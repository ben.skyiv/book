#include <stdio.h>

const int n = 3 + (int)1e7;
static int a[n];

long max_sum() 
{
  long sum = 0, max = 0;
  for (int i = 0; i < n; i++)
    if ((sum += a[i]) > max) max = sum;  
    else if (sum < 0) sum = 0;
  return max;
}

int main(void)
{
  const int m1 = (int)1e7, m2 = m1 + (int)2e5, n1 = n - 1;
  int t0 = 0, t1 = 0, t2 = 1, t3; long z = 0;
  printf("S(%d,%d) - S(%d,%d) = ", n, m2, n, m1); fflush(stdout);
  for (int i = 1; i <= m2; i++) {
    a[t0] += t1 + t1 - n1;
    if (i > m1) z += max_sum();
    t3 = t0+t1+t2; if (t3 >= n) t3 -= n; if (t3 >= n) t3 -= n;
    t0 = t1, t1 = t2, t2 = t3;
    t3 = t0+t1+t2; if (t3 >= n) t3 -= n; if (t3 >= n) t3 -= n;
    t0 = t1, t1 = t2, t2 = t3;
  }
  printf("%ld\n", z);
}

