main = print $ f $ map (^2) [1..100] where
  f xs = let n = length xs in
    sum $ map fst $ filter snd  $ (g n) !! (div n 2) where
    g n = if n == 0 then [[(0,True)]] else h xs $ g (n-1)
    h xs ys = let n = length ys; m = xs !! (n-1) in [(0,True)]
      : [combine (ys !! (i-1)) (ys !! i) m | i <- [1..n-1]]
      ++ [[(sum $ filter (<=m) xs, True)]]
    combine xs [] n = map (\(a,b) -> (n+a,b)) xs
    combine [] ys _ = ys
    combine ((x,a):xs) ((y,b):ys) n
      | x + n == y = (y,False) : combine xs ys n
      | x + n <  y = (x+n,a) : combine xs ((y,b):ys) n
      | otherwise  = (y,b) : combine ((x,a):xs) ys n

