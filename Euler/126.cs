using System;

sealed class E126
{
  static void Main()
  {
    int v = 20000, c = 1000;
    var count = new int[v + 1];
    for (int z = 1; Cubes(z, z, z, 1) <= v; ++z)
      for (int y = z; Cubes(z, y, z, 1) <= v; ++y)
        for (int x = y; Cubes(z, y, x, 1) <= v; ++x)
          for (int n = 1; Cubes(z, y, x, n) <= v; ++n)
            count[Cubes(z, y, x, n)]++;
    Console.WriteLine(Array.IndexOf(count, c));
  }

  static int Cubes(int x, int y, int z, int n)
  {
    return 2*(x*y+y*z+x*z) + 4*(x+y+z+n-2)*(n-1);
  }
}

