#include <stdio.h>
#include <math.h>

double f(double x, double *p)
{
  static double a[7], b[7], h[7];
  double s = sin(x + M_PI / 4);
  for (int i = 0; i < 6; i++) {
    if (i > 0) s *= (10.0 - i) / (11.0 - i);
    double t = (i>0) ? sin(asin(s)-M_PI/4) : sin(x);
    b[i] = (i > 0) ? (10 / sqrt(1 - s*s)) : 6; // 1 - 29
    a[i] = b[i] * sqrt(1 - t * t);
    h[i] = b[i] * t;
  }  
  h[6] = fabs(h[0]+h[1]+h[2]+h[3]+h[4]+h[5]);
  a[6] = h[6] * tan(0.75 * M_PI - asin(s * (10.0 / 5)));
  b[6] = sqrt(a[6] * a[6] + h[6] * h[6]);
  *p = (b[0]+b[6])/10+b[1]/9+b[2]/8+b[3]/7+b[4]/6+b[5]/5;
  return 100 - (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]);
}

int main()
{
  double z;
  for (double a = 0.1, b = M_PI / 4 - 0.1; a < b; ) {
    double x = (a + b) / 2, y = f(x, &z);
    if (fabs(y) < 1e-10) break;
    if (y < 0) a = x;
    else b = x;
  }
  printf("%.10f\n", z);
}

