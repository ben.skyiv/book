#include <stdio.h>

const int N = 257, M = N;//16;
static int a0[N], b0[N], *a = a0, *b = b0;

void shuffle(int n)
{
  for (int i9 = n >> 1, i = 0; i < i9; i++) {
    b[(i << 1)    ] = a[i];
    b[(i << 1) + 1] = a[i + i9];
  }
  int *t = a; a = b; b = t;
}

int s(int n)
{
  for (int i = 0; i < n; i++) a[i] = i;
  int z = 1;
  for (; z <= M; z++) {
    shuffle(n);
    if (a[1] == 1) break;
  }
  return z; // z == M + 1 for override
}

int main(void)
{
  //long z = 0;
  //for (int i = 2; i < N; i += 2)
  //  if (s(i) == M) { z += i; printf("%d,", i); fflush(stdout); }
  //printf(": %ld\n", z);
  for (int i = 2; i < N; i += 2) printf("%d,", s(i));
  printf("\n");
}

