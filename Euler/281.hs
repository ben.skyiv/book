import Math.NumberTheory.Primes.Factorisation ( φ )

main = let z = 10^15 in print $ sum $ takeWhile (>0) [sum
  $ takeWhile (<=z) [div (sum [φ d * (\x -> div (product
  [2..m*x]) $ product [2..x] ^ m) (div n d) | d <- [1..n],
  mod n d == 0]) $ m*n | n <- [1..]] | m <- [2..]]

