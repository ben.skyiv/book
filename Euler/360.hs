import Data.Maybe ( fromJust,isJust )
import Math.NumberTheory.Powers.Squares ( exactSquareRoot,integerSquareRoot' )

main = let n = 10^10; m = 2^10 in print $ let
  s = [[x, y, fromJust z] | x <- [0,m..n], let y2 = n*n - x*x,
    y <- [0, m .. integerSquareRoot' y2], let z = exactSquareRoot $ y2 - y*y, isJust z]
  in sum $ map (\(a,b) -> 2^b * sum a) $ zip s $ map (sum . map signum) s

