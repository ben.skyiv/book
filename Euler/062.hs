import Data.List
e1_3 x = floor $ exp (1/3 * log x)
f = sort . show . (^3)
main = let n = 7; a = 1 + (e1_3 $ 10^n); b = e1_3 $ 10^(n+1)
  in print $ head [ (x,y,z) | x <- [a..b], y <- [x+1..b], z <- [y+1..b],
  let x3 = f x, x3 == f y && x3 == f z ]

