import Data.List
import Data.Ord
import qualified Data.IntMap as M

isqrt a = filter ((== a) . (^2)) $ [approx]
    where approx = floor . sqrt . fromIntegral $ a

main = print . maximumBy (comparing $ length . snd) . M.assocs 
       . foldl' (\m e -> M.insertWith (++) (sum e) [e] m) M.empty
             $ [[a,b,c]
                    | a <- [1..333],
                      b <- [a..500],
                      c <- isqrt $ a^2 + b^2
               ]

