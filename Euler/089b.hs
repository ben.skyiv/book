import Data.Text ( pack,unpack,replace )
main = do
  s <- readFile "089.txt"
  print $ length s - (length $ unpack
    $ replace (pack "IIII")  (pack "IV")
    $ replace (pack "XXXX")  (pack "XL")
    $ replace (pack "CCCC")  (pack "CD")
    $ replace (pack "VIIII") (pack "IX")
    $ replace (pack "LXXXX") (pack "XC")
    $ replace (pack "DCCCC") (pack "CM")
    $ pack s)

