main = print $ length $ filter (\[a,b] -> all (\(c,d) -> let g c d = elem c a && elem d b in g c d || g d c) $ zip [1,1,4,6,6,5,6,6] $ 8:0:0:[0..]) $ let f 0 _ = [[]]; f _ [] = []; f n (x:xs) = map (x:) (f (n-1) xs) ++ f n xs in f 2 $ f 6 $ 6:[0..8]

