pow 1 x = x
pow n x = (\[z,a,b] [_,c,d] -> [z,a*c+z*b*d,a*d+b*c]) x $ pow (n-1) x
main = let p = 10^8 in print $ sum [ div p $ sum a | a <- map
  (\xs@[x,y,z] -> if all even xs then [div x 2,div y 2,div z 2] else [x,y,z])
  [ [y*y-x*x, 2*x*y, y*y+x*x] | a <- [3,5..40], let [_,k,n] = pow a [2,1,1],
  let m = lcm (n+k) (n-1); x = div m (n+k); y = div m (n-1) ], sum a < p ]

