using System;

sealed class E232
{
  static readonly int n = 100, m = 1 + (int)Math.Ceiling(Math.Log(n, 2));
  double[,] score = new double[n, n];
  int[,] t = new int[n, n];
  
  double F(int x, int y)
  {
    if (x >= n) return 0;
    if (y >= n) return 1;
    double z0 = score[x, y];
    if (z0 != 0) return z0;
    for (var i = 1; i <= m; i++)
      for (var j = 1; j <= m; j++)
      {
        double u = Math.Pow(0.5, i), v = Math.Pow(0.5, j);
        double z = (v * F(x, (1<<j-1)+y) + u * F(x+1, (1<<i-1)+y)
          + (1-u) * F(x+1, y)) / (1+v);
        if (z0 < z) { z0 = z; t[x, y] = j; }
      }
    score[x, y] = z0;
    return z0;
  }
  
  void Solve()
  {
    F(0, 0);
    for (var i = n-1; i >= 0; i--)
    {
      for (var j = 0; j < n; j++) Console.Write(t[i, j]);
      Console.WriteLine();
    }
  }

  static void Main()
  {
    new E232().Solve();
  }
}

