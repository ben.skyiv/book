public class E156
{
  static final int m = 11;
	static long[] p10 = new long[m+2];
	static long answer = 0;

	// g(n, d) == f(n - 1, d)
	// find all n so that g(n, d) == a n + b, and n has max totalDigits digits
	static void solve(int d, int totalDigits, int a, long b, long sol) {
		if(totalDigits == 0) {
			if(b == 0) answer += sol - 1;
			return;
		}
		long max = p10[totalDigits+1];
		long above = max / 9, below = max / 90;
		int j = totalDigits - 1;

		if( b < -below && 10 * a * max + 10 * b < j * max - 10 * below) return; //a n + b < k / 10 * n - below
		if( b >  above && 10 * a * max + 10 * b > j * max + 10 * above) return; //a n + b > k / 10 * n + above

		/* for f = d */                 solve(d, j, a - 1, b + a * d * p10[j+1] - d * j * p10[j]           , 10 * sol + d);
		for(int f = 0    ; f <  d; ++f) solve(d, j, a    , b + a * f * p10[j+1] - f * j * p10[j]           , 10 * sol + f);
		for(int f = d + 1; f <= 9; ++f) solve(d, j, a    , b + a * f * p10[j+1] - f * j * p10[j] - p10[j+1], 10 * sol + f);
	}

	public static void main(String[] args) {
		long p = 1; for(int i = 1; i <= m+1; ++i, p *= 10) p10[i] = p; // p[i+1] = 10^i, p[0] = 0;
		for(int d = 1; d <= 9; ++d) solve(d, m, 1, -1, 0); // find n-1 of max m digits so that f(n-1, d) == g(n, d) == n-1
		System.out.println(answer);
	}
}
/*
I used a solution that is really fast, but it's not like any other fast solution posted (i think).

first i switched f(n, d) to g(n, d) = f(n - 1, d). This makes the algorithm easier. so i need to find g(n, d) == n - 1.

The function g (or f) is not in my program, but i started with one for g:

let f be the first digit of n, and r the rest of the digits, so n = fr. Let j be the length of r.

g(fr) = 
| f < d  =  g(r, d) + f * j * 10 ^ (j - 1)
| f = d  =  g(r, d) + f * j * 10 ^ (j - 1) + r
| f > d  =  g(r, d) + f * j * 10 ^ (j - 1) + 10 ^ j

Then i made a function solve:
solve(int d, int totalDigits, int a, long b, long sol)
which does: find all n so that g(n, d) == a n + b, and n has max totalDigits digits

It tries all 10 first digits, then recurse for the leftover digits. Because the first digit, f, is chosen, the function for g can be partially calculated, and g(r, d) can be calculated.

But this would try all numbers n. But I can cut large branches by using an upper and lower limit for g(n, d) which
I found by plotting some values of g. (This is the code with above and below)

The number of recursive calls is 37769, so quite fast.
*/

