import Control.Monad ( forM_,when )
import Data.Array.ST ( newArray,readArray,runSTUArray,writeArray )
import Data.Array.Unboxed ( (!),assocs,UArray )

sieve :: Int -> UArray Int Bool
sieve n = runSTUArray $ do
  a <- newArray (1, n) True; writeArray a 1 False
  forM_ [2 .. floor . sqrt $ fromIntegral n] $ \i -> do
    p <- readArray a i
    when p (forM_ [i*i, i*i+i .. n] (\j -> writeArray a j False))
  return a

main = let n = 10^8; ps = sieve (n+1) in print $ sum $ filter (\n -> all
  (ps!) $ map (uncurry (+)) $ filter ((==0) . (mod n) . fst) $ takeWhile
  (uncurry (<=)) [(d, div n d) | d <- [2..]]) [p-1 | (p,True) <- assocs ps]

