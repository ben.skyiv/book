import Math.NumberTheory.Primes ( primes )
merge (a:as) (b:bs) | a < b = a : merge as (b:bs)
                    | a > b = b : merge (a:as) bs
powers = 2 : merge (tail primes) (map (^2) powers)
main = let n = 500500; m = 500500507 in print $
  foldl1 (\a b -> mod (a * b) m) $ take n powers

