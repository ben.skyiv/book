import Math.NumberTheory.ArithmeticFunctions ( totient )
main = let n = 10^18 :: Int in print $ (\(k,a,b) -> div (2*(n-b)) k
  + a) $ last $ takeWhile (\(_,_,b) -> b <= n) $ iterate (\(k,a,b)
  -> let c = totient(k) in (k+1, a+c, b + div (c*k) 2)) (2,1,0)

