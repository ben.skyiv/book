using System;
using System.Threading;
using System.Threading.Tasks;

static class E584
{
  static void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }

  static bool In(int y, int a, int b)
  {
    if (a > b) Swap(ref a, ref b);
    return b - a < 7 || y + a - b < 7;
  }

  static bool In(int y, int a, int b, int c)
  {
    return In(y, a, b) && In(y, a, c) && In(y, b, c);
  }

  static bool In(int y, int[] a, int n)
  {
    for (var i = n - 2; i >= 0; i--)
      for (var j = n - 1; j > i; j--)
        if (In(y, a[n], a[j], a[i])) return true;
    return false;
  }

  static void Compute(int y)
  {
    var a = new int[12]; var n = new long[a.Length];
    for (a[ 1] = 0; a[ 1] < y; a[ 1]++)
    for (a[ 2] = 0; a[ 2] < y; a[ 2]++) if (In(y, a,  2)) n[ 2]++; else
    for (a[ 3] = 0; a[ 3] < y; a[ 3]++) if (In(y, a,  3)) n[ 3]++; else
    for (a[ 4] = 0; a[ 4] < y; a[ 4]++) if (In(y, a,  4)) n[ 4]++; else
    for (a[ 5] = 0; a[ 5] < y; a[ 5]++) if (In(y, a,  5)) n[ 5]++; else
    for (a[ 6] = 0; a[ 6] < y; a[ 6]++) if (In(y, a,  6)) n[ 6]++; else
    for (a[ 7] = 0; a[ 7] < y; a[ 7]++) if (In(y, a,  7)) n[ 7]++; else
    for (a[ 8] = 0; a[ 8] < y; a[ 8]++) if (In(y, a,  8)) n[ 8]++; else
    for (a[ 9] = 0; a[ 9] < y; a[ 9]++) if (In(y, a,  9)) n[ 9]++; else
    for (a[10] = 0; a[10] < y; a[10]++) if (In(y, a, 10)) n[10]++; else
    for (a[11] = 0; a[11] < y; a[11]++)                   n[11]++;
    double z = 0, w = y * y;
    for (var i = 2; i < n.Length; i++, w *= y) z += (i+1)*n[i]/w;
    var sw = Console.Out; Monitor.Enter(sw); try {
      sw.Write("{0,2}: {1:F8}:", y, z);
      for (var i = 2; i < n.Length; i++) sw.Write(" " + n[i]);
      sw.WriteLine();
    } finally { Monitor.Exit(sw); }
  }

  static void Main()
  {
    Parallel.For(14, 28, (i,_) => Compute(i));
  }
}

