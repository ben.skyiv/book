import Math.NumberTheory.Primes.Factorisation ( factorise )

main = let n = 4 in print $ fst $ head $ dropWhile (not . snd) [(i, all
  ((==n) . (length . factorise)) [i .. i-1 + fromIntegral n]) | i <- [2..]]

