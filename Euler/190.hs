main = print $ sum [floor $ (2 / (fromIntegral m + 1)) **
  ((fromIntegral m * (fromIntegral m + 1)) / 2) *
  fromIntegral (product [i^i | i <- [1..m]]) | m <- [2..15]]

