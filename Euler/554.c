#include <stdio.h>

const int m = 100000007;
static int c[m];

long inv(long a)
{
  long z = 1;
  for (long c, q, x, y = 0, b = m; b != 0; z = y, y = x)
    c = a - (q = a/b) * b, a = b, b = c, x = z - q*y;
  return (z < 0) ? (z + m) : z;
}

long b(long n, long k)
{
  if (n < k) return 0;
  if (n >= m) return b(n / m, k / m) * b(n % m, k % m) % m;
  return inv((long)c[k] * c[n-k]) * c[n] % m;
}

long f(long n)
{
  long z = n % m;
  return (8 * b(n+n, n) - 7 - z - z - 3 * z * z) % m;
}

int main()
{
  c[0] = 1;
  for (long i = 1; i < m; i++) c[i] = i * c[i-1] % m;
  long v, a = 0, b = 1, z = 0;
  for (int i = 2; i <= 90; ++i, a = b, b = v)
    z = (z + f(v = a + b)) % m;
  printf("%ld\n", (z < 0) ? (z + m) : z);
}

