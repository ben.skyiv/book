import Data.List ( sort,sortBy,tails )

f [] _ _ = 0; f [p] r s = area [p,r,s]; f (p:q:xs) r s
  | (snd p - snd r) * (fst q - fst r) <= (fst p - fst r) * (snd q - snd r) = f (p:xs) r s
  | True = max (f (q:xs) p s + area [p,r,s]) $ f (q:xs) r s
g :: [Int] -> [(Int,Int)]; g (x:y:ps) = (y,x) : g ps
cot p q = fromIntegral (snd q - snd p) / fromIntegral (fst q - fst p)
area xs = sum $ zipWith (\a b -> fst a * snd b - snd a * fst b) xs $ tail xs ++ [head xs]
main = print $ (fromIntegral . (\xs -> maximum $ map (\(p:ps) -> f (sortBy (\a b ->
  compare (cot p b) $ cot p a) ps) p p) $ init . tails $ sort xs)) (take 500 $ g $
  tail $ map (\s -> mod s 2000 - 1000) $ iterate (\s -> mod (s*s) 50515093) 290797) / 2

