#include <stdio.h>

int main(void)
{
  long s = 0, m = 16, a = m, b = m - 1, c = m - 2, d = m - 3;
  for (int n = 3; n <= 16; n++) {
    a *= m; b *= (m - 1); c *= (m - 2); d *= (m - 3);
    s += (m - 1) * a + (3 * m - 7) * c - (3 * m - 5) * b - (m - 3) * d;
  }
  printf("%lX\n", s);
  return 0;
}

