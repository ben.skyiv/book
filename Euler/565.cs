using System;
using Skyiv.Utils;

sealed class E565 {
  static readonly int d = 2017;
  
  long Pow(long a, int n) {
    long z = a; // n > 0
    while (--n > 0) z *= a;
    return z;
  }
  
  long Count(long p, int a, long n) {
    long q1 = Pow(p, a), q2 = q1 * p;
    long n1 = n / q1, n2 = n / q2;
    return q1*n1*(n1+1)/2 - q2*n2*(n2+1)/2;
  }

  long Compute(long n) {
    var ps = Primes.GetPrimes(0, (int)n);
//Console.WriteLine("π({0:N0}) = {1:N0}", n, ps.Length);
    long v = 0;
    foreach (var p in ps) {
      for (long i = 1, q = p, z = 1 + q; q <= n; i++, q *= p, z += q) {
        if (z % d == 0) v += Count(p, (int)i, n);
//Console.WriteLine("{0,11:N0} {1} {2,11:N0}", p, i, z);
      }
    }
    return v;
  }

  static void Main() {
    Console.WriteLine(new E565().Compute(1000000000).ToString("N0"));
  }
}

