using System;
using System.Drawing;
using System.Collections.Generic;

static class E634
{
  static void Main()
  {
    long n = (long)3e6;
    var dict = new Dictionary<long, List<Point>>();
    for (var b = 2; ; b++) {
      var a2 = n / b / b / b;
      if (a2 < 4) break;
      var a9 = (long)Math.Sqrt(a2);
      for (var a = a9; a > 1; a--) {
        var key = a*a*b*b*b;
        List<Point> value;
        if (!dict.TryGetValue(key, out value))
          dict.Add(key, value = new List<Point>());
        value.Add(new Point((int)a, b));
      }
    }
    foreach (var kvp in dict) {
      if (kvp.Value.Count < 4) continue;
      foreach (var v in kvp.Value)
        Console.Write("({0},{1})", v.X, v.Y);
      Console.WriteLine();
    }
    Console.WriteLine("F({0}) = {1}", n, dict.Count);
  }
}

