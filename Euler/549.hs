import Math.NumberTheory.Primes ( factorSieve,sieveFactor )
f (p,a) b = let z = p*b in if (sum . takeWhile (>0) . tail
  . iterate (`div` p)) z >= a then z else f (p,a) $ b+1
s z n = maximum $ map (\(q,a) -> let p = fromIntegral q
  in if a <= p then p*a else f (p,a) p) $ sieveFactor z n
main = let n = 10^8 in print $ sum $ map (s $ factorSieve n) [2..n]

