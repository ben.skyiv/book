import Math.NumberTheory.Primes

main = do
  a <- readFile "578.txt"
  mapM_ print $ let xs = map read $ words a in zip xs $ map primeCount xs

