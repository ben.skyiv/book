{- https://projecteuler.net/thread=15&page=2#4725
   Dynammic programming solution in Haskell
   using a generic memoization function
-}
import Data.Array

tabulate bounds f = array bounds [(i,f i) | i <- range bounds]
dp bounds f = let memo = tabulate bounds (f (memo!)) in (memo!)

paths n = dp ((0,0),(n,n)) f (n,n) where
 f rec (x,y) | x == 0 || y == 0 = 1
             | otherwise = rec (x-1,y) + rec (x,y-1)

main = print $ paths 20

-- GHCI> let fac n = product [1..n] in div (fac 40) (fac 20^2)

{- https://projecteuler.net/thread=15&page=2#4724
Here's a Haskell program that uses dynamic programming and
self-referential data structures to make the naive algorithm more
efficient (though not as efficient as using combinatorics, of course):

module Main where
import System.Environment
import Data.List
import Data.Array

-- paths r d = paths (r-1) d + paths r (d-1)
paths r d = path' r d
    where pathA = array ((0,0),(r,d)) 
                . map (\(r,d) -> ((r,d), path' (r-1) d + path' r (d-1)))
                $ indices
          path' 0 _ = 1
          path' _ 0 = 1
          path' r d = pathA ! (r,d)
          indices = [ (a, b) | a <- [0..r], b <- [0..d] ]

main = do [n,m] <- fmap (map read) getArgs
          print $ paths n m
-}

