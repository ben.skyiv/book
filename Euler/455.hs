import Math.NumberTheory.Powers ( powerMod )

main = let m = 10^6 in print $ sum $ map f [(x,x) | x <- [2..m], mod x 10 /= 0]
  where f (n,x) = let y = powerMod n x $ 10^9 in if x == y then x else f (n,y)

