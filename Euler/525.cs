using System;

static class E525
{
  static double Circum(int a, int b)
  {
    double r = (double)(a-b)/(a+b), h = r*r;
    double z = 1.0, c = 1, hn = 1;
    for (int i = 1; i <= 10000; i += 2)
      z += (c*=(double)i*i/(i+1)/(i+1))*(hn*=h)/i/i;
    return Math.PI * (a+b) * z;
  }

  static double C(int a, int b)
  {
    int n = 1000000;
    double x0 = a, y0 = 0, tm = Math.PI / 2;
    double z  = 0, Y0 = a;
double circum = 0;
    for (int i = 1; i <= n; i++) {
      double t = tm * i / n;
      double x = a * Math.Cos(t);
      double y = b * Math.Sin(t);
      double Y = Math.Sqrt(x*x + y*y);
      double dY = Y - Y0;
      double dy = y - y0, dx = x - x0;
      double dX = Math.Sqrt(dx*dx + dy*dy);
      double dz = Math.Sqrt(dX*dX + dY*dY);
circum += dX;
//Console.WriteLine("{0,2}: {1:F4} ({2:F4}:{3:F4},{4:F4})",
//i,Z,dX,Y0,Y);
      z += dz; x0 = x; y0 = y; Y0 = Y;
    }
Console.WriteLine("{0} C({1}, {2})", Circum(a, b), a, b);
Console.WriteLine("{0} {1}", 4 * circum, 4 * z);
    return 4 * z;
  }

  static void Main()
  {
    Console.WriteLine("{0:F8}", C(2, 4));
  }
}

