from math import exp,log,pi,ceil,floor
def f(k,n): return(exp(k/n)-1)
n = 10000
F = [f(i,n) for i in range(0,ceil(log(pi+1)*n)+1)]
SubsetSum = [F[i]+F[j] for i in range(1,ceil(log(pi+1)*n)+1) for j in range(i,ceil(log(pi-F[i]+1)*n)+1)]
SortedSubsetSum = sorted(SubsetSum)
(lower,upper) = (0,len(SortedSubsetSum)-1)
best = SortedSubsetSum[upper] + SortedSubsetSum[lower]
bestind = (upper,lower)
while upper >=lower:
    test = SortedSubsetSum[upper] + SortedSubsetSum[lower]
    if abs(test-pi) < best:
        best = abs(test-pi)
        bestind = (upper*1,lower*1)
    if test > pi: upper -= 1
    else: lower += 1
vals = (SortedSubsetSum[bestind[1]],SortedSubsetSum[bestind[0]])
temp = [i**2+j**2 for i in range(1,ceil(log(pi+1)*n)+1) for j in range(i,ceil(log(pi-F[i]+1)*n)+1) if F[i]+F[j] in vals]
print(sum(temp))

