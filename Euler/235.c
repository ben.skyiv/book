#include <stdio.h>
#include <math.h>

long double f(long double x)
{
  long double z = powl(x, 5000);
  return (900*(1-z) + 15000*z - 3*(1-z)/(1-x)) / (1-x) + 6e11;
}

int main(void)
{
  long double eps = 1e-12, a = 1 + 1e-13, b = 2, x;
  while (b - a >= eps)
    if (f(x = (a + b) / 2) > 0) a = x;
    else b = x;
  printf("%.12Lf\n", x);
  return 0;
}

