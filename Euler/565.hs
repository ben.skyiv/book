import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Testing ( isPrime )

count p a n = let q1 = p^a; q2 = q1*p; n1 = div n q1; n2 = div n q2
  in div (q1*n1*(n1+1) - q2*n2*(n2+1)) 2
g n d p = map (\(i,_,_) -> (p,i)) $ filter (\(_,_,z) -> mod z d == 0)
  $ tail $ takeWhile (\(_,q,_) -> q <= n) $ iterate
  (\(i,q,z) -> let r = q*p in (i+1,r,z+r)) (1,p,1+p)
f n d = let ps = takeWhile (<= (floor $ sqrt $ fromIntegral n)) primes
  in concat [g n d p | p <- ps]
h n d = let d2 = d+d in map (\p -> (p,1)) $ filter isPrime 
  $ takeWhile (<=n) $ map pred [d2,d2+d2..]
s n x = sum $ map (\(p,a) -> count p a n) x
f1 n h2 = let h3 = map fst h2; h1 = head h3 in takeWhile (\x -> x*h1 <= n) h3
f2 n h3 = [k | i <- init h3, j <- filter (>i) h3, let k = i*j, k <= n]
main = let n = 10^11; d = 2017 in print $ let h2 = h n d; h3 = f1 n h2 in
  (s n $ f n d) + s n h2 - sum [count i 1 n | i <- f2 n (h3 ++ [2311^2])]

-- 2992480851924313898 2m9.536s

