// mcs 517.cs ../lib/Primes.cs ../lib/ModPow.cs
using System;
using Skyiv.Utils;

sealed class E517
{
  static readonly int m = 1000000007;

  int Combin(long c, long a, long b)
  {
    var v = (c * a) % m;
    if (b != 1) v = (v * b.ModInv(m)) % m;
    return (int)v;
  }
  
  int G(int n)
  {
    double a = Math.Sqrt(n--);
    bool tag = false;
    int n0 = (int)a, sum = 1;
    for (int c = 1, k = n0, c1 = k, c2 = 1; ; c2++, c1++, n--)
    {
      var tag0 = tag;
      if (tag) { tag = false; c1--; }
      var z = n - k * a;
      if (z < 0) { k--; z += a; tag = true; }
      if (c1 == c2) { sum = (sum + n - n0 + 1) % m; break; }
      if (c1 == c2 + 1) c = c1;
      else if (tag0) c = Combin(c, c1 + 1 - c2, c2);
      else c = Combin(c, c1, c2);
      sum = (sum + c) % m;
    }
    return sum;
  }

  int Compute(int low, int high)
  {
    var sum = 0;
    foreach (var p in Primes.GetPrimes(low, high))
    {
      sum = (sum + G(p)) % m;
      Console.Error.Write("{0:N0} ", p);
    }
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E517().Compute(10000000, 10010000));
  }
}

