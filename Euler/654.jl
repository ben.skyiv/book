# T(10, 100) = 862820094 (mod 10^9+7)
n = 10; m = 100; c = 10^9+7
a = [ x <= n-y ? 1 : 0 for x in 1:(n-1), y in 1:(n-1) ]
@time println((a^(m+1))[1,1] % c)

