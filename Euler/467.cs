using System;
using System.Numerics;

sealed class E467
{
  int u, v;
  byte[,] f;
  byte[] a, b;
  BigInteger z;

  void GetPnAndCn(int max)
  {
    a = new byte[max];
    b = new byte[max];
    // http://en.wikipedia.org/wiki/Prime_number_theorem
    // #Approximations_for_the_nth_prime_number
    var ln = Math.Log(max);
    var composite = Sieve((int)((ln + Math.Log(ln)) * max));
    for (int i = 2, p = 0, c = 0; p < max; i++)
      if (!composite[i]) a[p++] = DigitalRoot(i);
      else if (c < max) b[c++] = DigitalRoot(i);
    Array.Reverse(a);
    Array.Reverse(b);
  }
  
  byte DigitalRoot(int n)
  { // http://en.wikipedia.org/wiki/Digital_root
    return (byte)(n - (n - 1) / 9 * 9);
  }

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }

  // http://en.wikipedia.org/wiki/Longest_common_subsequence_problem
  void LongestCommonSubsequence(int N)
  {
    int[,] m = new int[N + 1, N + 1];
    f = new byte[N + 1, N + 1];
    for (int i = 0; i <= N; i++) m[i, 0] = 0;
    for (int j = 0; j <= N; j++) m[0, j] = 0;
    for (int i = 1; i <= N; i++)
      for (int j = 1; j <= N; j++)
        if (a[i - 1] == b[j - 1])
        {
          m[i, j] = m[i - 1, j - 1] + 1;
          f[i, j] = 7; // left_up
        }
        else if (m[i - 1, j] == m[i, j - 1])
        {
          m [i, j] = m[i - 1, j];
          f[i, j] = (a[i - 1] > b[j - 1]) ? (byte)8 : (byte)4;
        }
        else if (m[i - 1, j] > m[i, j - 1])
        {
          m [i, j] = m[i - 1, j];
          f[i, j] = 4; // left
        }
        else
        {
          m[i, j] = m[i, j - 1];
          f[i, j] = 8; // up
        }
    SubSequence(N, N);
    Make(0, 0);
  }

  void SubSequence(int i, int j)
  {
    if (i == 0 || j == 0) return;
    if (f[i, j] == 7)
    {
      Make(i, j);
      z = z * 10 + a[u]; u--; v--;
      SubSequence(i - 1, j - 1);
    }
    else if (f[i, j] == 8) SubSequence(i, j - 1);
    else SubSequence(i - 1, j);
  }
  
  void Make(int i, int j)
  {
    while (u > i - 1 && v > j - 1)
      if (a[u] < b[v]) { z = z * 10 + a[u]; u--; }
      else             { z = z * 10 + b[v]; v--; }
    for (; u > i - 1; u--) z = z * 10 + a[u];
    for (; v > j - 1; v--) z = z * 10 + b[v];
  }
  
  int Compute(int max)
  {
    GetPnAndCn(max);
    u = max - 1; v = max - 1;
    LongestCommonSubsequence(max);
    return (int)(z % 1000000007);
  }
  
  static void Main(string[] args)
  {
    Console.WriteLine(new E467().Compute(int.Parse(args[0])));
  }
}
