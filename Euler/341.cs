using System;
using System.Collections.Generic;

sealed class E341
{
  Dictionary<int, int> a = new Dictionary<int, int>();
  
  int G(int n)
  {
    if (n == 1) return 1;
    int g0 = n-1, g1, g2, g3, g4;
    if (!a.TryGetValue(g0, out g1)) a.Add(g0, g1 = G(g0));
    if (!a.TryGetValue(g1, out g2)) a.Add(g1, g2 = G(g1));
    g3 = n - g2;
    if (!a.TryGetValue(g3, out g4)) a.Add(g3, g4 = G(g3));
    return 1 + g4;
  }

  static void Main()
  {
    var e = new E341();
    Console.WriteLine("{0} {1}", e.G(100000), e.a.Count);
  }
}

