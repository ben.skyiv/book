#include <stdio.h>
#include <math.h>

typedef unsigned long u64;

u64 powu(u64 a, u64 b)
{
  u64 v = 1;
  for (; b; b >>= 1, a *= a) if (b & 1) v *= a;
  return v;
}

u64 f(u64 n, int p)
{
  u64 z = 1, a = 1, b = (1UL << p) - 1;
  for (u64 i = b; i > 1; i -= 2) a *= i;
  for (int i = (int)(log(n) / log(2)); i >= 0; i--) {
    u64 m = n >> i; z *= powu(a, m >> p);
    for (u64 k = m & b, j = 1; j <= k; j += 2) z *= m + j - k;
  }
  return z;
}

int main(void)
{
  u64 n = 1; for (int i = 20; i > 1; i--) n *= i;
  printf("%lX\n", (f(n, 23) << 2) & ((1UL << 48) - 1));
}

