#include <iostream>
#include <cmath>

int main(void)
{
  const long L = 3'141'592'653'589'793;
  static long F[(int)sqrt(L)+1];
  for (long t = sqrt(L), g = (t%2)?t:(t-1); g >= 1; g -= 2) {
    for (long m = 1; m <= sqrt(t=L/(g*g)); m++)
      F[g] += m/2-ceill((std::max(1.0, m-sqrt(t-m*m))+1)/2)+1;
    for (long q = 3; L/(q*q*g*g) >= 1; q += 2) F[g] -= F[q*g];
  }
  std::cout << F[1] << std::endl;
}

