import Data.List
import Data.Char
main = do
  a <- readFile "008.txt"
  print . maximum . map (product . take 13) . tails $ map digitToInt $ filter isDigit a
-- 0m0.006s
