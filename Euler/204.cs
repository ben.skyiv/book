using System;

sealed class E204
{
  static readonly int[] primes =
  {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97};

  static bool IsHamming(int n)
  {
    for (int i = 0, prime = primes[i]; ;)
      if (n == 1) return true;
      else if (n % prime == 0) n /= prime;
      else if (++i >= primes.Length) return false;
      else prime = primes[i];
  }

  static void Main()
  {
    int count = 0;
    for (var i = 1; i <= 1000000000; i++)
      if (IsHamming(i)) count++;
    Console.WriteLine(count);
  }
}
