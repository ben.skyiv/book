#include <stdio.h>
#include <math.h>

static int getIabc(double p, double a, double b, double c)
{
  double v = sqrt(b*c*(b+c-a)/p);
  return (v == (int)v) ? (int)v : -1;
}

int main(void)
{
  int count = 0, n = 10000;
  long sum = 0;
  for (int a = 1; a <= n / 2; a++)
    for (int b = a; b <= n / 2; b++) {
      long ab = (long)a*b;
      long c1 = 2L*ab*(a+b);
      int ic1 = (int)sqrt(a/(a+2.0*b)*ab);
      int ic2 = (int)sqrt((a+b-n/2.0)/(a+b+n/2.0)*ab);
      for (int ic = ic1; ic >= ic2; ic--) {
        long c2 = (long)ic*ic + ab;
        if (c1 % c2 != 0) continue;
        int c = (int)(c1/c2) - a - b;
        if (a + b <= c) break;
        int ia, ib, p = a + b + c;
        if (p > n) break;
        if ((ia = getIabc(p, a, b, c)) < 0) continue;
        if ((ib = getIabc(p, b, c, a)) < 0) continue;
        sum += p + ia + ib + ic;
        count++;
      }
    }
  printf("sum:%ld count:%d\n", sum, count);
}
// S(10^3) =   3,619  0m00.102s   4
// S(10^4) = 884,667  1m12.264s 103

