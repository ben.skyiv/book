import Math.NumberTheory.Primes ( primes )
main = let n = 10^9; z = div n 2; m = 10^9 in mapM_
  print $ map (flip mod m) $ takeWhile (<=z) primes

