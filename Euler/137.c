#include <stdio.h>

int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }

int main(void)
{
  long z = 0;
  for (long n = 0, a = 1, b = 2; n < 15; b++)
    for (; a*(a+b) < b*b; a++)
      if(gcd(a,b) == 1 && a*b%(b*b-a*b-a*a) == 0)
        z = a*b/(b*b-a*b-a*a), n++;
  printf("%ld\n", z);
  return 0;
}

