from math import factorial

def C(n, m, k, tab = {}):
  if n == 0: return int(m == 0)
  if n > m or n > k: return 0
  if not tab.has_key((n, m, k)):
    tab[n, m, k] = n * C(n-1, m-1, k) + 2*n * C(n, m-1, k) + n * C(n+1, m-1, k)
  return tab[n, m, k]

def P253(n = 40):
 return n - sum(C(1, n, i) for i in range(n))/float(factorial(n))

print P253()

