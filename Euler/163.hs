-- http://www.math.uni-bielefeld.de/~sillke/SEQUENCES/grid-triangles
main = let n = 36; b = n*n; c = b*n in print $ sum [ div (2*c + 5*b + 2*n) 8,
  2 * (div c 2 - div n 6), 6 * sum [ div (n*(n+1)*(n+2)) 6, div (2*c + 5*b + 2*n) 8,
  div (2*c + 3*b - 3*n) 18, div (2*c + 3*b - 3*n) 10 ], 3 * (div (22*c + 45*b - 4*n) 48) ]

