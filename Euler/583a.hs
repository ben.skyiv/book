main = print [(g,w) | a <- [2..10], b <- [1..a-1], gcd a b == 1, odd (a+b),
  let g = 2*a*b; w = a*a-b*b ]

