using System;

sealed class E214
{
  static void Main()
  {
    int n = 40000000, m = 25;
    int[] a = new int[n], c = new int[n];
    for (var i = 1; i < n; i++) a[i] = i;
    c[1] = 1;
    long sum = 0;
    for (var p = 2; p < n; p++)
    {
      if (a[p] != p) { c[p] = c[a[p]] + 1; continue; }
      for (var i = p + p; i < n; i += p) a[i] = a[i] / p * (p - 1);
      if ((c[p] = c[p - 1] + 1) == m) sum += p;
    }
    Console.WriteLine(sum);
  }
}

