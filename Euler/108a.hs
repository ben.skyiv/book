import Math.NumberTheory.Primes.Factorisation ( τ )

main = let n = 10^3 in print $ head [i | i <- [1..], div (τ (i*i) + 1) 2 >= n]

