import Math.NumberTheory.Primes.Sieve ( primes )

main = let z = 10^10 in print . fst . head . dropWhile
  (\(n,p) -> 2*n*p < z) $ filter (odd . fst) $ zip [1..] primes

