n = 20 000; k = 20 000; q = 1 004 535 809
p = Pol(vector(n, i, Mod(prime(n-i+2) - prime(n-i+1), q)))
p = Mod(p*x+1, x^(n+1))
p = p^k; print(lift(polcoeff(lift(p), n))); quit()

