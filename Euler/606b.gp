a=Map()
g(n)=if(mapisdefined(a,n,&z),z,z=0;forprime(p=2,n,z+=p^3);mapput(a,n,z);z)
s(n)=r=n^(1/3);z=0;forprime(q=3,r/2,z+=q^3*g(min(q-1,r/q)));z

