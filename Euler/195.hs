main = let n = 1053779; z = 12*n*n in print $ foldl
  (\a v -> foldl (\a u -> a + floor (fromIntegral n * sqrt 12 /
  (if mod (u+v) 3 == 0 then 1 else 3) / fromIntegral ((u-v)*v))) a
  $ filter ((==1).(gcd v)) $ takeWhile ((<=z).(*v).(*v).(^2).((-)v))
  [2*v+1..]) 0 $ takeWhile ((<z).(^4)) [1..]

