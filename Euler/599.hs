import Math.Combinat.Numbers ( binomial )
g k n = binomial (n+k-1) n
f n = let c = n + div (n * (n-1) * (n+1)) 3 in
  3 * g (c-n) 8 + sum [g n i * g (c-n) (8-i) | i <- [1..8]]
main = print $ f 10

