using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class E652
{
  static readonly long n = (long)1e6;
  static readonly int n2 = (int)Math.Sqrt(n);
  static int c1 = 0, c2 = 0;
  static HashSet<(int a, int b)> set = new HashSet<(int a, int b)>();

  static int Gcd(int a, int b) { return b == 0 ? a : Gcd(b, a % b); }

  static long D1()
  { // Count Log(a^e, a^f) for: 1 < a, 0 < e < f, a^f <= n
    long z = 0; int m3 = (int)Math.Log(n, 3); var ln = Math.Log(n);
    for (int e = 1; e < m3; e++)
      for (int f = e + 1; ; f++) {
        if (Gcd(e, f) != 1) continue;
        var z1 = (long)Math.Exp(ln/f);
        if (z1 < 3) break;
        long b = 1L << f;
        if (b <= n2) {
          set.Add((1 << e, (int)b)); c1++;
          for (int i = 3; (b = ((long)i).Pow(f)) <= n2; i++)
          { set.Add(((int)((long)i).Pow(e), (int)b)); c1++; }
        }
        z += z1 - 2;
      }
    return z;
  }

  static long D2()
  { // Count Log(a^e, b^e) for: 0 < e, 1 < a < b, b != a^i, b^e <= n
    long z = 0; var ln = Math.Log(n);
    for (int a = 2; a < n2; a++)
      for (int b = a + 1; ; b++) {
        if (set.Contains((a, b))) continue;
        var e = (int)(ln / Math.Log(b));
        if (e < 2) break;
        for (long i = 2, a2 = a*a, b2 = b*b; b2 <= n2; a2 *= a, b2 *= b, i++)
        { set.Add(((int)a2, (int)b2)); c2++; }
        z += e - 1;
      }
    return z;
  }

  static void Main()
  {
    long z = n - 2; // Count Log(a, a) for: 1 < a <= n
    long z1 = D1(), z2 = D2();
    z += (z1 + z2) << 1; z = (n-1)*(n-1) - z;
    Console.WriteLine("D(10^{0}) = {1:N0} ({2} {3}) ({4}:{5}+{6})",
      Math.Log(n, 10), z, z1, z2, set.Count, c1, c2);
  }
}

