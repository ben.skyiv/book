#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static long R;
double sq(double x) { return x*x; }
int hole(int x, int y, int u1, int v1, int u2, int v2)
{ return x >= u1 && y >= v1 && x < u2 && y < v2; }

double s(double z, int w, int h, int u1, int v1, int u2, int v2)
{
  double v = 0; long cnt = 0;
  for (double x1 = 0; x1 < w; x1 += z)
    for (double y1 = 0; y1 < h; y1 += z) {
      if (hole(x1, y1, u1, v1, u2, v2)) continue;
      for (double x2 = 0; x2 < w; x2 += z)
        for (double y2 = 0; y2 < h; y2 += z) {
          if (hole(x2, y2, u1, v1, u2, v2)) continue;
          cnt++, v += sqrt(sq(x2 - x1) + sq(y2 - y1));
        }
    }
  double a = v / cnt;
  printf("(%d,%d - %d,%d) %ld %.5lf\n", u1,v1,u2,v2,cnt/R,a);
  fflush(stdout); return a;
}

int main(int argc, char* argv[])
{
  int wide = (argc > 1) ? atoi(argv[1]) : 3;
  int high = (argc > 2) ? atoi(argv[2]) : 3;
  int e    = (argc > 3) ? atoi(argv[3]) : 5;
  long n = 1L << e; R = 1L << (e << 2);
  double step = 1.0 / n, v = 0;
  printf("%dx%d %.15lf\n", wide, high, step); fflush(stdout);
  if (wide < 3 || high < 3) {
    wide = high = 5;
    printf("%dx%d %.15lf\n", wide, high, step); fflush(stdout);
    v += s(step, wide, high, 3, 1, 4, 2);
    v += s(step, wide, high, 1, 1, 2, 2);
    v += s(step, wide, high, 2, 1, 3, 2);
    v += s(step, wide, high, 2, 2, 3, 3);
    v += s(step, wide, high, 1, 1, 2, 4);
    v += s(step, wide, high, 1, 1, 4, 2);
    v += s(step, wide, high, 1, 2, 4, 3);
  } else
    for (int x1 = 1; x1 < wide - 1; x1++)
      for (int y1 = 1; y1 < high - 1; y1++)
        for (int x2 = x1 + 1; x2 < wide; x2++)
          for (int y2 = y1 + 1; y2 < high; y2++)
            v += s(step, wide, high, x1, y1, x2, y2);
  printf("%.5lf\n", v); fflush(stdout);
}

