import Data.List
import Data.Ratio
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Factorisation ( factorise )
gpd x = if x == 1 then 1 else (fst . last . factorise) x -- greatest prime divisor
suminvsq = sum.(map ((1%).(^2)))
primpm p = map (p*) $ filter ((<=p).gpd) [1..]
rsets e x p ns = [ms| ms<-map (ns++) (subsequences ((takeWhile (<=e) (primpm p))\\ns)),
                      (gpd.denominator) (x-suminvsq ms)<p]
ans e = foldr (\p ns -> concatMap (rsets e (1%2) p) ns) [[]] (takeWhile (<=e) primes)
main = print $ length (ans 80)

