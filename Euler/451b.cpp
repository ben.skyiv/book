#include <iostream>
#include <numeric>
#include <cstring>

int main()
{
  const int m = 20'000'000;
  static short size[m + 1];
  static int I[m + 1], *divisors[m + 1];
  for (int i = 2; i <= m; i++)
    for (int j = i; j <= m; j += i)
      size[j]++;
  for (int i = 2; i <= m; I[i++] = 1)
    divisors[i] = new int[size[i]];
  memset(size, 0, sizeof(size));
  for (int i = 2; i <= m; i++)
    for (int j = i; j <= m; j += i)
      divisors[j][size[j]++] = i;
  long n;
  for (int x = 2; x < m; x++)
    for (int i = 0; i < size[x - 1]; i++) {
      long p = divisors[x - 1][i];
      for (int j = 0; j < size[x + 1]; j++)
        if ((n = p * divisors[x + 1][j]) > m) break;
        else if (x < n - 1) I[n] = x;
    }
  std::cout << std::accumulate(I+3,I+m+1,0L) << std::endl;
}

