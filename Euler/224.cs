using System;
using System.Collections.Generic;

sealed class E224
{
  static void Push(Stack<int[]> s, int p, int a, int b, int c, bool i)
  { if (i && a + b + c <= p) s.Push(new int[]{a, b, c}); }

  static void Main()
  {
    int count, p = 75000000;
    var s = new Stack<int[]>();
    Push(s, p, 2, 2, 3, true);
    for (count = 0; s.Count > 0; count++)
    {
      var t = s.Pop();
      int a = t[0], b = t[1], c = t[2];
      Push(s, p, 2*c+b-2*a, 2*c+2*b-a, 3*c+2*b-2*a, a < b);
      Push(s, p, 2*c+b+2*a, 2*c+2*b+a, 3*c+2*b+2*a, true);
      Push(s, p, 2*c-2*b+a, 2*c-b+2*a, 3*c-2*b+2*a, true);
    }
    Console.WriteLine(count);
  }
}

