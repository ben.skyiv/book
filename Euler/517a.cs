// dmcs 517a.cs ../lib/Primes.cs ../lib/ModPow.cs
using System;
using Skyiv.Utils;

sealed class E517
{
  static readonly int m = 1000000007, min = 10000000, max = 10010000;
  static readonly int [] a1 = new int [max+1], a2 = new int [max+1];
  
  static E517()
  {
    a1[0] = a2[0] = 1;
    for (int i = 1; i <= max; i++)
      a2[i] = (a1[i] = (int)(a1[i-1] * (long)i  % m)).ModInv(m);
  }

  int C(int n, int k)
  {
    return (int)(a1[n] * (long)a2[k] % m * a2[n-k] % m);
  }
  
  int G(int n)
  {
    double a = Math.Sqrt(n);
    int z = 1;
    for (int x = 1; x <= (int)a; x++) z = (z + C(x + (int)(n-x*a), x)) % m;
    return z;
  }

  int Compute(int low, int high)
  {
    var z = 0;
    foreach (var p in Primes.GetPrimes(low, high)) z = (z + G(p)) % m;
    return z;
  }

  static void Main()
  {
    Console.WriteLine(new E517().Compute(min, max));
  }
}

