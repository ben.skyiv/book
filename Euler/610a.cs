using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class E610a
{
  static readonly HashSet<string> romans = new HashSet<string>();
  static bool Valid(string s) { return romans.Contains(s.TrimStart('M')); }

  static E610a()
  {
    for (int i = 0; i < 1000; i++) romans.Add(i.ToRoman());
  }

  static void Add(Dictionary<string, double> dict, string k, double v)
  {
    double v0;
    if (!dict.TryGetValue(k, out v0)) dict.Add(k, v);
    else dict[k] = v0 + v;
  }

  static void Main()
  {
    var cs = "IVXLCDM";
    var dict1 = new Dictionary<string, double>();
    var dict2 = new Dictionary<string, double>();
    dict1.Add("", 1);
    double a1 = 0.02, a2 = 0.14, a3 = a1 * a2, v0, v1 = a1, v2 = 0;
    for (int i = 1; i < 20; i++, a3 *= a2) { // i up to 2000
      foreach (var kvp in dict1)             // a3 < 1e-308
        foreach (var c in cs) {
          var s = kvp.Key + c;
          if (Valid(s)) Add(dict2, s, kvp.Value);
          else Add(dict2, kvp.Key, kvp.Value);
        }
      foreach (var kvp in dict2) {
        v1 += v0 = kvp.Value * a3;
        v2 += v0 * kvp.Key.FromRoman();
      }
      Console.WriteLine("{0,4}: {1,6} {2:F9} {3,13:F9}",i,dict2.Count,v1,v2);
      dict1.Clear(); var dict0 = dict1; dict1 = dict2; dict2 = dict0;
    }
  }
}

