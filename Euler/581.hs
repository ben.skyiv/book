import Math.NumberTheory.Primes ( factorSieve,sieveFactor )
--  10^9: 22,495,633,669 10m45.851s
n = 10^6; m = 47; sieve = factorSieve n
b n = (<=m) $ last $ map fst $ sieveFactor sieve n
f u v = sum $ map fst $ filter snd $ zip [u,u+2..] [b i && b (i+i+v) | i <- [2..n]]
main = print $ 3 + f 3 (-1) + f 4 1

