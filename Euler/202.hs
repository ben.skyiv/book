import Data.List ( partition )
import Math.NumberTheory.Primes.Factorisation ( factorise )

f n = if mod n 3 == 0 then 0 else div (g n ps + if null xs then (if mod n 3 == 1
  then -1 else 1) * 2^(length ys + if odd n then 0 else 1) else 0) 3 where
  ps = map fst $ factorise n
  (xs, ys) = partition ((==1) . (mod 3)) $ dropWhile (<5) ps
  g m ps = if null ps then m else g (m - div m (head ps)) $ tail ps

main = let n = 12017639147 in print $ if gcd n 6 /= 1 then 0 else f $ div (n+3) 2

