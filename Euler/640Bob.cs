using System;

static class E640Bob
{
  static readonly int cards = 12;
  static readonly int i9 = cards * cards / 4;
  static readonly short b0 = (short)((1 << cards) - 1);

  static void Main()
  {
    short b = b0;
    int max = 0; long n = 0, z = 0;
    var rand = new Random();
    for (var v = 0; n < (long)1e8; ) {
      if (b == 0) { if (max < v) max = v; n++; z += v; v = 0; b = b0; }
      v++;
      switch (rand.Next(i9)) {
        case 0: // 11
          if      ((b & 1) != 0) b &= ~1;
          else if ((b & 2) != 0) b &= ~2;
          else                   b |=  2;
          break;
        case 1: // 22
          if      ((b & 2) != 0) b &= ~2;
          else if ((b & 8) != 0) b &= ~8;
          else                   b |=  8;
          break;
        case 2: // 33
          if      ((b &    4) != 0) b &= ~4;
          else if ((b & 0x20) != 0) b &= ~0x20;
          else                      b |=  0x20;
          break;
        case 3: // 44
          if      ((b & 0x80) != 0) b &= ~0x80;
          else if ((b &    8) != 0) b &= ~8;
          else                      b |=  8;
          break;
        case 4: // 55
          if      ((b & 0x200) != 0) b &= ~0x200;
          else if ((b &  0x10) != 0) b &=  ~0x10;
          else                       b |=   0x10;
          break;
        case 5: // 66
          if      ((b & 0x800) != 0) b &= ~0x800;
          else if ((b &  0x20) != 0) b &=  ~0x20;
          else                       b |=   0x20;
          break;
        case 6: case 7: // 12
          if      ((b & 1) != 0) b &= ~1;
          else if ((b & 2) != 0) b &= ~2;
          else if ((b & 4) != 0) b &= ~4;
          else                   b |=  4;
          break;
        case 8: case 9: // 13
          if      ((b & 1) != 0) b &= ~1;
          else if ((b & 4) != 0) b &= ~4;
          else if ((b & 8) != 0) b &= ~8;
          else                   b |=  8;
          break;
        case 10: case 11: // 14
          if      ((b &    1) != 0) b &=    ~1;
          else if ((b &    8) != 0) b &=    ~8;
          else if ((b & 0x10) != 0) b &= ~0x10;
          else                      b |=  0x10;
          break;
        case 12: case 13: // 15
          if      ((b &    1) != 0) b &=    ~1;
          else if ((b & 0x10) != 0) b &= ~0x10;
          else if ((b & 0x20) != 0) b &= ~0x20;
          else                      b |=  0x20;
          break;
        case 14: case 15: // 16
          if      ((b & 0x40) != 0) b &= ~0x40;
          else if ((b &    1) != 0) b &=    ~1;
          else if ((b & 0x20) != 0) b &= ~0x20;
          else                      b |=  0x20;
          break;
        case 16: case 17: // 23
          if      ((b &    2) != 0) b &=    ~2;
          else if ((b &    4) != 0) b &=    ~4;
          else if ((b & 0x10) != 0) b &= ~0x10;
          else                      b |=  0x10;
          break;
        case 18: case 19: // 24
          if      ((b &    2) != 0) b &=    ~2;
          else if ((b &    8) != 0) b &=    ~8;
          else if ((b & 0x20) != 0) b &= ~0x20;
          else                      b |=  0x20;
          break;
        case 20: case 21: // 25
          if      ((b & 0x40) != 0) b &= ~0x40;
          else if ((b &    2) != 0) b &=    ~2;
          else if ((b & 0x10) != 0) b &= ~0x10;
          else                      b |=  0x10;
          break;
        case 22: case 23: // 26
          if      ((b & 0x80) != 0) b &= ~0x80;
          else if ((b &    2) != 0) b &=    ~2;
          else if ((b & 0x20) != 0) b &= ~0x20;
          else                      b |=  0x20;
          break;
        case 24: case 25: // 34
          if      ((b & 0x40) != 0) b &= ~0x40;
          else if ((b &    4) != 0) b &=    ~4;
          else if ((b &    8) != 0) b &=    ~8;
          else                      b |=     8;
          break;
        case 26: case 27: // 35
          if      ((b & 0x80) != 0) b &= ~0x80;
          else if ((b &    4) != 0) b &=    ~4;
          else if ((b & 0x10) != 0) b &= ~0x10;
          else                      b |=  0x10;
          break;
        case 28: case 29: // 36
          if      ((b & 0x100) != 0) b &= ~0x100;
          else if ((b &     4) != 0) b &=     ~4;
          else if ((b &  0x20) != 0) b &=  ~0x20;
          else                       b |=   0x20;
          break;
        case 30: case 31: // 45
          if      ((b & 0x100) != 0) b &= ~0x100;
          else if ((b &     8) != 0) b &=     ~8;
          else if ((b &  0x10) != 0) b &=  ~0x10;
          else                       b |=   0x10;
          break;
        case 32: case 33: // 46
          if      ((b & 0x200) != 0) b &= ~0x200;
          else if ((b &     8) != 0) b &=     ~8;
          else if ((b &  0x20) != 0) b &=  ~0x20;
          else                       b |=   0x20;
          break;
        default: // 56
          if      ((b & 0x400) != 0) b &= ~0x400;
          else if ((b &  0x10) != 0) b &=  ~0x10;
          else if ((b &  0x20) != 0) b &=  ~0x20;
          else                       b |=   0x20;
          break;
      }
    }
    Console.WriteLine("1e{0} {1} {2}",
      Math.Log10(n), max, (double)z / n);
  }
}
// 1e8 786 50.2973519 2m27s

