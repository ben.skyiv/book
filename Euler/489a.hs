main = let m = 18; n = 1900 in print $ sum [f a b 0 (a*(a^6+27*b*b)) []
  | a <- [1..m], b <- [1..n]] where
  f a b n z xs = if n > z then (0-) . snd . maximum $ xs else
    let x = gcd (n^3+b) ((n+a)^3+b) in if x == z then n
    else f a b (n+1) z $ (x,-n):xs

