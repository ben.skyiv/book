using System;

sealed class E425
{
  int[] bs;
  
  int Find(int x)
  {
    return (x == bs[x]) ? x : (bs[x] = Find(bs[x]));
  }
  
  bool[] GetComposite(int n)
  {  
    bs = new int[n+1];
    var composite = new bool[n+1];
    composite[1] = true;
    for (var i = 2; i <= n; bs[i] = i, i++)
      if (!composite[i])
        for (var j = i + i; j <= n; j += i)
          composite[j] = true;
    return composite;
  }

  long Compute(int n)
  {
    var cs = GetComposite(n);
    long sum = 0;
    for (int q, z = 1, p = 2; p <= n; p++)
    {
      if (cs[p]) continue;
      if (z * 10 <= p) z *= 10;
      if ((q = p%z) * 10 >= z && !cs[q]) bs[Find(q)] = Find(p);
      for (int i = 1; i <= z; i *= 10)
        for (int j = (q=p)/i%10 - ((i == z) ? 1 : 0); j > 0; j--)
          if (!cs[q -= i])
            bs[Find(q)] = Find(p);
      if (Find(2) != Find(p)) sum += p;
    }
    return sum;
  }
  
  static void Main()
  {
    Console.WriteLine(new E425().Compute(10000000));
  }
}
