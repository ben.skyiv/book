using System;
using Skyiv.Utils;

static class E637
{
  static int F(int[] a, int n, int b)
  {
    if (n < b) return 0;
    if (a[n] != 0) return a[n];
    var s = ((long)n).ToStringBase(b);
    int z = n, m = s.Length - 1;
    for (var k = (1 << m) - 1; k > 0 && z > 1; k--) {
      int t = 0, c = 0;
      for (int i = 0, j = k; i <= m; i++, j >>= 1) {
        c = c * b + s[i] - '0';
        if ((j & 1) != 0) { t += c; c = 0; }
      }
      z = Math.Min(F(a, t += c, b), z);
    }
    return a[n] = z + 1;
  }
  
  static int[] GetFs(int n, int b)
  {
    var a = new int[n + 1];
    for (var i = 1; i <= n; i++) { a[i] = 0; F(a, i, b); }
    return a;
  }

  static void Main()
  {
    var n = (int)1e7; Console.WriteLine("n = {0:N0}", n);
    var a1 = GetFs(n, 10); Out(a1);
    var a2 = GetFs(n,  3); Out(a2);
    long z = 0;
    for (var i = 1; i <= n; i++) if (a1[i] == a2[i]) z += i;
    Console.WriteLine("{0:N0}", z);
  }

  static void Out(int[] a)
  { a[0] = 100;
    var d = new System.Collections.Generic.Dictionary<int,int>();
    foreach (var i in a) { int v; d.TryGetValue(i, out v); d[i] = v+1; }
    foreach (var k in d) Console.Write("{0}:{1:N0} ", k.Key, k.Value);
    Console.WriteLine();
  }
}

