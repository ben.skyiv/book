using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class E578
{
  static int n2;
  static int[] primes;
  static HashSet<long> pis = new HashSet<long>();
  
  static int Pi(long n)
  {
    if (n > n2) pis.Add(n);
    return 1;
  }

  static long C(long n, int i, int e)
  {
    long p = primes[i], z = 0;
    if (p > n) return 1;
    if (p * p > n) return Pi(n) - i + 1; // Pi(p-1) == i
    for (int j = 0; j <= e && n >= 1; ++j, n /= p)
      z += C(n, i + 1, (j != 0) ? j : e);
    return z;
  }

  static void Main()
  {
    long n = 10000000000000; // n < 2^64
    n2 = 100000000; // > 100 + (int)Math.Sqrt(n);
    primes = Primes.GetPrimes(0, n2);
    Console.Error.WriteLine("π({0:N0}) = {1:N0}", n2, primes.Length);
    var z = C(n, 0, 64);
    foreach (var pi in pis) Console.WriteLine(pi);
    Console.Error.WriteLine("N({0:N0}) = {1:N0}, Count:{2:N0}",
      n, z, pis.Count);
  }
}

