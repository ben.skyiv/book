#include <stdio.h>
#include <math.h>

int main(void)
{
  const int n = 47, m = (int)pow(3, 15), v = 10*n;
  static int s[n+1][v], c[n+1][v];
  int z = 0;
  for (int k = 1; k <= n; k++)
    for (int i = 0; i <= k; i++, z = (z + s[k][v/2]) % m)
      for (int j = 0; j < v; j++) {
        if (i == 0) { c[i][j] = (j == v/2) ? 1 : 0; continue; }
        int w = (k%2==0)?(i>k/2)?1:-1:(i<=k/2)?-1:(i>k/2+1)?1:0;
        c[i][j] = s[i][j] = 0;
        for (int d = 0; d <= 9; d++) {
          if (d == 0 && i == 1) continue;
          int t = c[i-1][j-w*d];
          c[i][j] = (c[i][j] + t) % m;
          s[i][j] = (s[i][j] + t*d + s[i-1][j-w*d]*10) % m;
        }
      }
  printf("%d\n", z);
  return 0;
}

