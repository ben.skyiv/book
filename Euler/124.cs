using System;
using System.Collections.Generic;

sealed class E124
{
  struct Pair : IComparable<Pair>
  {
    public int R, N;
    public Pair(int r, int n) { R = r; N = n; }
    public int CompareTo(Pair other)
    {
      if (R > other.R) return 1;
      if (R < other.R) return -1;
      return (N > other.N) ? 1 : -1;
    }
  }

  int Compute(int n, int k)
  {
    var rads = new Pair[n + 1];
    for (var i = 0; i <= n; i++) rads[i] = new Pair(1, i);
    for (var i = 2; i <= n; i++)
    {
      if (rads[i].R != 1) continue;
      rads[i].R = i;
      for (var j = i + i; j <= n; j += i) rads[j].R *= i;
    }
    Array.Sort(rads);
    return rads[k].N;
  }

  static void Main()
  {
    Console.WriteLine(new E124().Compute(100000, 10000));
  }
}

