import Data.List ( sort )
import Math.NumberTheory.Primes.Factorisation ( factorise )

main = let n = 10^5; k = 10^4 in print $ snd $ (!!(k-1)) $ sort
  $ zip [product $ map fst $ factorise i | i <- [1..n]] [1..]

