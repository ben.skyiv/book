#include <stdio.h>

long h(int n)
{
  long z = 0;
  for (int i = 1, j = n-2; j > 0; ++i, j -= 3)
    z += j * (j + 1L) / 2 * i;
  return z;
}

long f(int n)
{
  long z = 0;
  for (int i = 3; i <= n; i++)
    z += h(i);
  return z;
}

int main(void)
{
  printf("%ld\n", f(12345));
  return 0;
}

