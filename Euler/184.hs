import Data.Map.Strict as Map ( foldl,insertWith,singleton )

main = let r = 105 in print $ let
  f t x y z w = if z >= r*r then t else f (insertWith (+)
    (floor $ atan2 y (fromIntegral x) * fromIntegral (r*r) * 4 / pi)
    1 (fst t), (2 + snd t)) x (y+1) (z+w+2) (w+2)
  (m,z) = Prelude.foldl (\t x -> f t x 1 (x*x+1) 1)
    ((singleton 0 (r-1)), r+r-2) [x | x <- [1..r-1]]
  in (*4) $ fst $ Map.foldl (\(n,s) x -> (n+x*s*(z-s-x),s+x)) (0,0) m

