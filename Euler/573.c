#include <stdio.h>

int main()
{
  int n = (int)1e6; double z = 1.0, t = 1.0;
  for (int i = 2; i <= n; i++, z += t) t *= (n-i+1.0)/n;
  printf("%lf\n", z);
}

