import Data.List ( sort )
import Math.NumberTheory.Primes.Testing ( isPrime )
import Math.NumberTheory.Primes.Factorisation ( factorise )

h [] = [([],[])]
h ((p,k):xs) = [((p,n):ns,(p,k-n):ms) | n <- [0..k], (ns,ms) <- h xs]
g q [] = [(0,[])]
g q ((p,k):xs)
  | p > q = [(0,(p,k):xs)]
  | p < q = [(m,(p,k):ns) | (m,ns) <- g q xs]
  | True  = [(m,(p,k-m):xs) | m <- [0..k]]
f n xs = if all ((==0) . snd) xs then if n == 1 then [1,2] else [1] else
  [r*q^(k+1) | (ns,ms) <- h xs, let q = foldl (\r (p,k) -> r*p^k) 1 ns+1,
  q > n, isPrime q, (k,ns) <- g q ms, r <- f q ns]
main = print $ (!!(150000-1)) $ sort $ f 1 . factorise $ product [2..13]

