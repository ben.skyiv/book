// g++ -O3 -fopenmp 611b.cpp
#include <cmath>
#include <vector>
#include <numeric>
#include <iostream>

int work(int n, int m)
{
  long a = n*(long)m + 1, b = a + n - 1; std::vector<char> s(n);
  for (long i2, i = 1; (i2 = i*i) < b; i++)
    for (long k, j = std::max(i+1, (long)sqrt(std::max(0L, a-i2)));
      (k = i2 + j*j) <= b; j++) if (k >= a) s[k - a] = !s[k - a];
  return std::accumulate(s.begin(), s.end(), 0);
}

int main()
{
  int n = 1e5, i9 = 1e5; long z = 0;
  #pragma omp parallel for reduction(+:z) schedule(dynamic)  
  for (int i = 0; i < i9; i++) z += work(n, i);
  std::cout << z << std::endl;
}

