using System;
using System.Linq;
using System.Collections.Generic;

sealed class E249
{
  static readonly long m = 10000000000000000;

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }

  int[] GetPrimes(int max)
  {
    var primes = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) primes.Add(i);
    return primes.ToArray();
  }
  
  long Solve(int n)
  {
    var ps = GetPrimes(n);
    var z = ps.Sum();
    var a = new long[2, z + 1];
    a[0, ps[0]] = 1;
    for (var i = 1; i < ps.Length; i++)
      for (int k, j = 2; j <= z; a[i&1, j++] %= m)
      {
        a[i&1, j] = a[(i-1)&1, j];
        if ((k = j-ps[i]) >= 0) a[i&1, j] += a[(i-1)&1, k];
        if (ps[i] == j) a[i&1, j]++;
      }
    long v = 0;
    int t = (ps.Length-1)&1;
    foreach (var p in GetPrimes(z)) v = (v + a[t, p]) % m;
    return v;
  }

  static void Main()
  {
    Console.WriteLine(new E249().Solve(5000));
  }
}

