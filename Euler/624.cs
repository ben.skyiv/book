using System;

static class E624
{
  static void Main()
  { // Fibonacci sequence
    long n = 3; // P(3) = 9/31 = 0.29032258...
    double a = 0, b = 0, c = 0.25;
    for (long i = 2, t, u = 1, v = 1; i < 90;
      c /= 2, t = u, u = v, v = t + v, i++) {
      double z = c * u;
      b += z;
      if (i % n == 0) a += z;
      Console.WriteLine("{0,-18} {1,-18} {2}", a, b, a / b);
    }
  }
}

