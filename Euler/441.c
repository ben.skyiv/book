#include <stdio.h>
#include <stdlib.h>

int gcd(int a, int b)
{
  for (int t; b != 0; a = b, b = t) t = a % b;
  return a;
}

double compute(int n)
{
  double sum = 0;
  for (int m = 2; m <= n; m++) {
    for (int p = 1; p < (m + 1) / 2; p++) {
      int q = m - p;
      if (gcd(p, q) != 1) continue;
      long z = (long)p * q;
      sum += 1.0 / z;
    }
  }
  return sum;
}

int main(int argc, char *argv[])
{
  int n = (argc > 1) ? atoi(argv[1]) : 100;
  printf("s(%d) = %lf\n", n, compute(n) + (n - 1) / 2.0);
  return 0;
}
