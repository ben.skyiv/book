using System;
using Skyiv.Utils;

static class E646
{
  static readonly int m = 7 + (int)1e9, low = 20, high = 60, n = 70;
  static long cnt = 0, z = 0;
  static int[] pp, aa;
  static double[] zz;

  static void T(int k, double z1, int z2, long z3)
  {
    var w1 = z1; var w2 = z2; var w3 = z3;
    for (int i = 0; i <= aa[k]; i++, z1 = w1, z2 = w2, z3 = w3) {
      if (k == aa.Length - 1) Console.Write("{0} ", i);
      z1 += i * zz[k];
      z2 += i;
      z3 = (z3 * pp[k].ModPow(i, m)) % m;
      if (z1 > high) return;
      if (k > 0) T(k - 1, z1, z2, z3);
      else if (z1 >= low) { cnt++; z = (z + (((z2 & 1) == 0) ? z3 : -z3)) % m; }
    }
  }

  static void Main()
  {
    pp = Primes.GetPrimes(0, n);
    Array.Reverse(pp);
    aa = new int[pp.Length];
    zz = new double[pp.Length];
    for (var i = 0; i < pp.Length; i++) {
      zz[i] = Math.Log10(pp[i]);
      for (var j = n / pp[i]; j > 0; j /= pp[i]) aa[i] += j;
    }
    Console.WriteLine("T({0}, {1}, {2}): ({3})", n, low, high, aa[aa.Length - 1]);
    T(aa.Length - 1, 0, 0, 1);
    if (z < 0) z += m;
    Console.WriteLine("\n{0:N0} ({1:N0})", z, cnt);
  }
}

