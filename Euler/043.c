#include <stdio.h>

static char a[11];

int valid(int start)
{
  static int digits[10];
  for (int i = 0; i < 10; i++) digits[i] = 0;
  for (int i = start; i < 10; i++) {
    int d = a[i] - '0';
    if (digits[d]) return 0;
    digits[d] = 1;
  }
  return 1;
}

int set(int start, int n)
{
  static char buf[5];
  sprintf(buf, "%03d", n);
  if (start < 7) {
    if (a[start + 2] != buf[2]) return 0;
    if (a[start + 1] != buf[1]) return 0;
    a[start] = buf[0];
  }
  else for (int i = 0; i < 3; i++) a[start + i] = buf[i];
  return valid(start);
}

int main(void)
{
  for (int i = 0; i < 10; i++) a[i] = '-';
  for (int i17 = 1; i17 <= 58; i17++)
    if (set(7, 17 * i17))
      for (int i13 = 1; i13 <= 76; i13++)
        if (set(6, 13 * i13))
          for (int i11 = 1; i11 <= 90; i11++)
            if (set(5, 11 * i11))
              for (int i7 = 1; i7 <= 142; i7++)
                if (set(4, 7 * i7))
                  for (int i5 = 1; i5 <= 199; i5++)
                    if (set(3, 5 * i5))
                      for (int i3 = 1; i3 <= 333; i3++)
                        if (set(2, 3 * i3))
                          for (int i2 = 1; i2 <= 499; i2++)
                            if (set(1, 2 * i2))
                              printf("%s\n", a);
  return 0;
}
