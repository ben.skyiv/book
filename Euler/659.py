n = 10 ** 7
a = [4*k*k + 1 for k in range(n+1)]
b = [0         for k in range(n+1)]

def f(i, k):
  for j in range(k, n+1, i): 
    b[j] = max(b[j], i)
    while a[j] % i == 0: a[j] //= i

for k in range(1, n+1): 
  i = a[k]
  if i > k+k: f(i, k); f(i, i-k)

print(sum(b))

