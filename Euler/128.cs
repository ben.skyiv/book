using System;

sealed class E128
{
  static void Main()
  {
    Console.WriteLine(Compute(10));
    Console.WriteLine(Compute(2000));
  }

  static long Compute(int limit)
  {
    long n = 1;
    for (int count = 1; count < limit; n++) {
      if (IsPrime(6*n-1) && IsPrime(6*n+1) && IsPrime(12*n+5))
        if (++count >= limit) return 3*n*n - 3*n + 2;
      if (IsPrime(6*n-1) && IsPrime(6*n+5) && IsPrime(12*n-7) && n!=1) count++;
    }
    return 3*n*n - 3*n + 1;
  }

  static bool IsPrime(long n)
  {
    if (n < 2) return false;
    if (n < 4) return true;
    if (n % 2 == 0) return false;
    if (n < 9) return true;
    if (n % 3 == 0) return false;
    if (n < 25) return true;
    int s = (int)Math.Sqrt(n);
    for (int i = 5; i <= s; i += 6)
      if (n % i == 0 || n % (i + 2) == 0)
        return false;
    return true;
   }
}

