import Data.Set ( toList )
import Data.List ( subsequences )
import Math.NumberTheory.ArithmeticFunctions ( divisors )
main = print [length $ filter (==n) $ map (foldl1 lcm) $ tail
  $ subsequences $ toList $ divisors n | n <- [1..12] :: [Int]]

