#include <stdio.h>

const long n =    (long)1e10, m = (long)1e9;
const int n2 = 2 + (int)1e5; // n2 > sqrt(n + n2)

void factor(long i0, long a[], const char c[])
{ // a[]: largest prime factor
  static long t[n2];
  for (int i = 0; i < n2; i++) t[i] = 1;
  for (int p = 2; (long)p * p < i0 + n2; p++)
    if (!c[p])
      for (long q = p; q < i0 + n2; q *= p)
        for (long i = q-1-(i0-1)%q; i < n2; t[i] *= p, i += q)
          if (q == p) a[i] = p;
  for (int i = 0; i < n2; i++)
    if (i + i0 != t[i]) a[i] = (i + i0) / t[i];
}

int main(int argc, char *argv[])
{
  static long a[n2]; static char c[n2];
  for (int i = 2; i * i < n2; i++)
    if (!c[i]) for (int j = i * i; j < n2; j += i) c[j] = 1;
  long z0 = 2, z9 = n, z = 0;
  printf("[%ld, %ld] ", z0, z9); fflush(stdout);
  for (long i0 = -n2, i = z0; i <= z9; i++) {
    if (i >= i0 + n2) factor(i0 = i, a, c);
    z = (z + a[i - i0]) % m;
  }
  printf("%ld\n", z);
}

