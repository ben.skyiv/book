#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int compute_n(long p)
{
  long n = ((long)sqrt(1 + 12 * p) + 1) / 6;
  if (n * (3 * n - 1) == p) return n;
  n++;
  if (n * (3 * n - 1) == p) return n;
  return -1;
}

int main(int argc, char *argv[])
{
  int max = 2345;
  for (int i = 1; i < max; i++) {
    long pi = i * (3 * (long)i - 1);
    for (int j = i + 1; j < max; j++) {
      long pj = j * (3 * (long)j - 1);
      long pk = pj - pi;
      int k = compute_n(pk);
      if (k < 0) continue;
      long pl = pi + pj;
      int l = compute_n(pl);
      if (l < 0) continue;
      printf("%ld: %d %d %d %d\n", pk/2, i, j, k, l);
      return 0;
    }
  }
  return 0;
}
