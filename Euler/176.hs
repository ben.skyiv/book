import Math.NumberTheory.Primes.Sieve ( primes )
--  2*47547+1 == 5*7*11*13*19
main = let xs = [5,7,11,13,19] in print $ product [ a^b | (a,b) <- zip
  primes $ div (last xs + 1) 2 : reverse (map (\x -> div x 2) $ init xs) ]

