import Data.Time.Calendar ( fromGregorian )
import Data.Time.Calendar.WeekDate ( toWeekDate )
 
main = print $ length [() | y <- [1901..2000], m <- [1..12],
  let (_,_,d) = toWeekDate $ fromGregorian y m 1, d == 7]

