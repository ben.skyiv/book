# See https://arxiv.org/pdf/1111.1150.pdf
from numba import jit
from math import sqrt
import numpy as np

@jit('i8(i8)', nopython=True)
def gen(n):
  ans = 0
  MOD = 10**9 #2 * 10**9

  def gcd(a):
    t = 0
    for x in a:
      while x != 0:
        x, t = t % x, x
    return abs(t)

  def nLattice(a, b, c, t):
    d_1, d_2, d_3 = gcd(a), gcd(b), gcd(c)
    return (t + 1) * (t * t - t + 1 + d_1 + d_2 + d_3)

  def cmp(a, b):
    if a[0] != b[0]: return a[0] < b[0]
    if a[1] != b[1]: return a[1] < b[1]
    if a[2] != b[2]: return a[2] < b[2]
    return False

  def add(M, t):
    nonlocal ans

    if cmp(-M[0], M[0]) and cmp(M[0], M[1]) and cmp(M[1], M[2]):
      g = gcd(M.ravel())
      M = M // g
      t = t // g
      y = np.sum(np.abs(M), axis=0)
      max_scale = n // np.max(y)

      if max_scale > 0:
        for scale in range(1, max_scale + 1):
          ans += np.prod(n + 1 - y * scale) % MOD * (nLattice(M[0] * scale, M[1] * scale, M[2] * scale, t * scale) % MOD) % MOD

  def SqrRoot(x):
    return int(sqrt(x + 0.9))

  def addTuple(case, a, b, c, d):
    if gcd([a, b, c, d]) != 1: return

    M0, M1, M2 = a**2 + b**2 - c**2 - d**2, 2 * (b * c + a * d), 2 * (b * d - c * a)
    M3, M4, M5 = 2 * (b * c - a * d), a**2 - b**2 + c**2 - d**2, 2 * (c * d + b * a)
    M6, M7, M8 = 2 * (b * d + c * a), 2 * (c * d - b * a), a**2 - b**2 - c**2 + d**2

    if not (0 <= M0 <= M3 <= M6 or 0 >= M0 >= M3 >= M6):
      return

    M = np.array([
      M0, M1, M2,
      M3, M4, M5,
      M6, M7, M8,
    ]).reshape((3, 3))
    t = a * a + b * b + c * c + d * d
    if t > 4 * n: return
    if case == 0 and 0 <= M[0, 0] <= M[1, 0] <= M[2, 0]:
      add(M, t)
    if case == 1 and 0 >= M[0, 0] >= M[1, 0] >= M[2, 0]:
      add(-M, t)

  for case in range(2):
    for a in range(-n, n + 1):
      if a * a > 4 * n: continue
      print(a)
      for b in range(-n, n + 1):
        if a * a + b * b > 4 * n: continue
        for c in range(-n, n + 1):
          if a * a + b * b + c * c > 4 * n: continue

          L, R = -SqrRoot(5 * n - a**2 - b**2 - c**2), SqrRoot(5 * n - a**2 - b**2 - c**2)
          L, R = max(L, -n), min(R, n)
          if (a + b > 0 and case == 0) or (a + b < 0 and case == 1):
            L = max(L, (b - a) * c // (a + b) - 1)
          elif (a + b > 0 and case == 1) or (a + b < 0 and case == 0):
            R = min(R, (b - a) * c // (a + b) + 1)

          t1, t2 = 2 * a**2 + b**2 - c**2 - 2 * b * c, a**2 + b**2 - c**2
          if (case == 0 and t2 < 0) or (case == 1 and t1 < 0):
            continue
          t1, t2 = max(t1, 0), max(t2, 0)
          if case == 0:
            t = SqrRoot(t2) + 1
            L, R = max(L, -t), min(R, +t)
            Lj, Rj = a - SqrRoot(t1) + 1, a + SqrRoot(t1) - 1
          else:
            t = SqrRoot(t1) + 1
            L, R = max(L, a - t) , min(R, a + t)
            Lj, Rj = -SqrRoot(t2) + 1, +SqrRoot(t2) - 1

          if Rj < L or R < Lj or True:
            for d in range(L, R + 1):
              addTuple(case, a, b, c, d)
          else:
            bp = MOD
            for d in range(L, R + 1):
              if Lj <= d <= Rj:
                break
              bp = d
              addTuple(case, a, b, c, d)
            for d in range(R, L - 1, -1):
              if Lj <= d <= Rj or d == bp:
                break
              addTuple(case, a, b, c, d)

  return ans // 2 % MOD

print(gen(5000))

