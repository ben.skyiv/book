import Data.Ratio
import Data.List
import Data.Map hiding (map)

universe = "thereisasyetinsufficientdataforameaningfulanswer"
boundary = 15

prob480 = let there be light = Just it
          in if universe /= []
             then there was light
             else Nothing

was = undefined
light = it
it = w $ p "legionary" + p "calorimeters" - p "annihilate" + p "orchestrated" - p "fluttering"

-- entropy: distribution of char information. Map of char->count
universeEntropy = countMap universe 
countMap xs = fromListWith (+) $ zip xs (repeat 1)

fact = scanl (*) (1%1) [1%1..] -- n!
inverseFact = map (1/) fact -- 1/n!
-- generating function: (Σ_{i=0..multivac} x^i/i!)^hyperspace
generating (multivac, hyperspace) = (genericTake (succ multivac) inverseFact)^fromIntegral hyperspace 
-- count bound entropy = number of different string for entropy whose len <= bound
count bound = sum. take (succ bound). map numerator. zipWith (*) fact. foldl1 (*). map generating

decrease entropy k = update dec k entropy
    where dec x = if x>1 then Just (x-1) else Nothing

p = pos boundary universeEntropy
pos _ _ [] = 0
pos bound entropy (c:cs) = 1 + pos (pred bound) (decrease entropy c) cs +
                           (sum $ map (count (pred bound). freq. decrease entropy) ks)
    where ks = takeWhile (<c) $ keys entropy
          freq = toList . countMap . elems

-- Inefficiency is not a concern since it only takes a timeless interval for AC to do it.
w i = word i universeEntropy []
word i entropy s | p s == i = s  
word i entropy s = word i decreasedEntropy (s++[next])
    where next = last . takeWhile notEnough $ keys entropy
          decreasedEntropy = decrease entropy next
          notEnough charData = p (s++[charData]) <= i

-- [mis]use list to represent polynomial: f(x) = Σls!!i * x^i
instance Num a => Num [a] where 
   (f:fs) + (g:gs) = f+g : fs+gs
   fs + [] = fs
   [] + gs = gs
   (f:fs) * (g:gs) = f*g : [f]*gs + fs*(g:gs)
   _ * _ = []
   abs           = undefined
   signum        = map signum
   fromInteger n = [fromInteger n]
   negate        = map negate

main = print prob480

