def min_factor_sum(N):
  def f(n):
    return n * (n + 1) // 2 - 1

  v = int(N ** 0.5)
  s_cnt = [i - 1 for i in range(v + 1)]
  s_sum = [f(i) for i in range(v + 1)]
  l_cnt = [N // i - 1 if i else 0 for i in range(v + 1)]
  l_sum = [f(N // i) if i else 0 for i in range(v + 1)]
  used = [0] * (v + 1)
  
  ret = 0
  for p in range(2, v + 1):
    if s_cnt[p] == s_cnt[p - 1]:
      continue
    p_cnt = s_cnt[p - 1]
    p_sum = s_sum[p - 1]
    q = p * p
    ret += p * (l_cnt[p] - p_cnt)
    l_cnt[1] -= l_cnt[p] - p_cnt
    l_sum[1] -= (l_sum[p] - p_sum) *  p
    interval = (p & 1) + 1
    end = min(v, N // q)
    for i in range(p + interval, end + 1, interval):
      if used[i]:
        continue
      d = i * p
      if d <= v:
        l_cnt[i] -= l_cnt[d] - p_cnt
        l_sum[i] -= (l_sum[d] - p_sum) * p
      else:
        t = N // d
        l_cnt[i] -= s_cnt[t] - p_cnt
        l_sum[i] -= (s_sum[t] - p_sum) * p
    if q <= v:
      for i in range(q, end, p * interval):
        used[i] = True
    for i in range(v, q - 1, -1):
      t = i // p
      s_cnt[i] -= s_cnt[t] - p_cnt
      s_sum[i] -= (s_sum[t] - p_sum) * p
  return l_sum[1] + ret

print(min_factor_sum(10 ** 12))

