using System;

sealed class E540
{
  static int C(int n, int z, int i, int k, int[,] a)
  {
    int sum = 0;
    for (int j = k-1; j >= 0; j--)
    {
      int u = z * a[i,j], v = n/(2*u);
      sum += ((i % 2 == 0) ? (n/u - v) : v) - C(n, u, i, j, a);
    }
    return sum;
  }

  static int Count(int n, int i, int k, int[,] a)
  {
    return ((i % 2 == 0) ? (n - n/2) : (n/2)) - C(n, 1, i, k, a);
  }

  static void Main()
  {
    const int SIZE = 8;
    const long n = 3141592653589793;
    int m = (int)Math.Sqrt(n/2.0);
    var a = new int[m+1,SIZE];
    var b = new int[m+1];
    for (int i = 3; i <= m; i += 2)
      if (b[i] == 0)
        for (int j = i; j <= m; j += i)
          a[j, b[j]++] = i;
    long z = 0;
    for (int j, i = 1; i < (j = (int)Math.Sqrt(n-(long)i*i)); i++)
      z += Count(j, i, b[i], a) - Count(i, i, b[i], a);
    Console.WriteLine(z);
  }
}

