using System;
using System.Threading;
using System.Threading.Tasks;

sealed class E441
{
  static readonly int max = 10000000, mod = 10000;

  int Gcd(int a, int b)
  {
    for (int t; b != 0; a = b, b = t) t = a % b;
    return a;
  }
  
  double R(int n)
  {
    double z = 0;
    for (int p = 1; p < (n + 1) / 2; p++) {
      int q = n - p;
      if (Gcd(p, q) != 1) continue;
      z += 1.0 / ((long)p * q);
    }
    return z;
  }
  
  double Smod(int m)
  {
    var dt1 = DateTime.Now;
    double z = 0; // compute ragne: [2, max]: i % mod == m
    for (var i = (2+m) % mod; i <= max; i += mod) z += R(i);
    var dt2 = DateTime.Now;
    Console.WriteLine("{0:dd HH:mm:ss} {1,9:N3} {2,4} {3,2}",
      dt2, (dt2-dt1).TotalSeconds, m, Thread.CurrentThread.ManagedThreadId);
    return z;
  }

  double S()
  {
    var mutex = new object(); double z = 0;
    Parallel.For(0, mod, () => 0.0, (i,_,x) => x+Smod(i),
      x => { lock (mutex) z += x; });
    return z + (max - 1) / 2.0;
  }

  static void Main()
  {
    Console.WriteLine("{0:dd HH:mm:ss} {1:N0} {2:N0}", DateTime.Now, max, mod);
    var z = new E441().S();
    Console.WriteLine("{0:dd HH:mm:ss} {1}", DateTime.Now, z);
  }
}

