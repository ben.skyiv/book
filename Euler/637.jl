ds(n, base) = n < base ? n : n % base + ds(div(n, base), base)

# Tests if some integer with a digits sum of < base
# can be constructed from n in one step
function test(n, k, base)::Bool
  bb = base
  while bb < n
    !test(div(n, bb), n % bb + k, base) || return true
    bb *= base
  end
  ds(n + k, base) < base
end

function f(n, base=10)
  n >= base || return 0
  ds(n, base) >= base || return 1
  !test(n, 0, base) || return 2
  3
end

@show @time sum(n for n in 1:10^7 if f(n, 3) == f(n))

