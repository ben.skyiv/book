import Data.Ratio ( (%) )
import Math.NumberTheory.Primes.Factorisation ( φ )

main = let r = 15499%94744; n = 2*3*5*7*11 in print $ until (\n -> φ n % (n-1) < r) (+n) n

