#include <stdio.h>

static long a[20];

void f(long n, int b)
{
  for (int i = 0; n > 0; n /= b) a[i++] += n % b;
}

int digitsSum(long n, int b)
{
  int z = 0;
  for (; n > 0; n /= b) z += n % b;
  return z;
}

int main(void)
{
  int b1 = 32, b2 = 4;
  long z = 0, w = 0, n = (long)1e12;
  for (long i = 1; i <= n; i++) {
    if (digitsSum(i, b1) != digitsSum(i, b2)) continue;
    z += i, w++;
f(i,b1);
  }
  printf("M(%ld, %d, %d) = %ld (%ld)\n", n, b1, b2, z, w);
  for (int i = 0; i < 20; i++) printf("%ld ", a[i]); puts("");
}

