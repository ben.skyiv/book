#include <stdio.h>

int main(void)
{
  const int z = 11111111, m = 1000000;
  static int a[z + m]; a[1] = 1;
	for (int i= 1, j = 1; j < z; i++)
    for(int k = a[a[j] = i]; k > 0; j++, k--)
      a[j] = i;
  long sum = 0, u = 0, v = 0, n, i = 1;
  for (n = 1; n < m; v += i*a[i], u += a[i++])
    if(n*n*n - v <= i*a[i])
      sum += u + (n*n*n - v + i - 1) / i, n++;
  printf("%ld\n", sum);
  return 0;
}

