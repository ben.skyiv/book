import Data.Maybe ( fromJust,isJust )
import Data.Ratio ( (%),denominator,numerator )

main = print $ head [m | let a = [5248,1312,2624,5760,3936]; b = [640,1888,3776,3776,5664],
  let c = [b%a | (a,b) <- zip a b]; x = sum a; y = sum b, v <- [5..y],
  u <- [x, x-1 .. ceiling $ x*v%y], let m = u*y % (x*v), let
  f _ _ _ [] = Nothing
  f a b z (x:xs) = let y = m * z * fromInteger x in
    if y > fromInteger (min b (v-a)) then Nothing else
    if denominator y == 1 then Just (x, numerator y) else f a b z xs,
  let c0 = f  0 (b!!0) (c!!0) [1 .. min (a!!0) u], isJust c0, let (a0,b0) = fromJust c0,
  let c1 = f b0 (b!!1) (c!!1) [1 .. min (a!!1) $ u-a0], isJust c1,
  let (a1,b1) = fromJust c1; c2 = f (b0+b1) (b!!2) (c!!2) [1 .. min (a!!2) $ u-a0-a1],
  isJust c2, let (a2,b2) = fromJust c2,
  isJust $ f (b0+b1+b2) (b!!3) (c!!3) [1 .. min (a!!3) $ u-a0-a1-a2]]

