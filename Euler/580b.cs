using System;
using System.Collections.Generic;

static class E580A
{
  static HashSet<long> set = new HashSet<long>();

  static long C(long n)
  {
    set.Clear();
    for (long j, i = 5; (j = i*i) <= n; i += 4)
      for (long k = j << 2, m = j; m <= n; m += k)
        set.Add(m);
    return set.Count;
  }

  static void Main()
  {
    long i0 = (long)1, gap = 10000;
    for (long v0 = -1, cnt = 0, i = i0; i < i0 + gap; i++, cnt++) {
      var v1 = C(i);
      if (v1 == v0) continue;
      if (v1 != v0 + 1) throw new Exception("v1");
      Console.Write(cnt + ",");
      v0 = v1; cnt = 0;
    }
    Console.WriteLine();
  }
}

