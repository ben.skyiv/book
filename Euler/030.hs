import Data.Char ( digitToInt )
main = print
  [n | n <- [2..10^6], n == (sum . map ((^5) . digitToInt) . show) n]

