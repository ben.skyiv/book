import Math.NumberTheory.Powers.Squares ( isSquare )

main = let n = 10^4 in print $ sum [s | s <- [4..n], a <- [1..s-3],
  let e=a*a; f=s+a; t = div f $ gcd e f, d <- [t,t+t..s-a-2],
  let g = s-a-d, isSquare $ g*g - 4 * (div (d*e) f)]

