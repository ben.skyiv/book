using System; // mcs 585.cs && python 585.py | mono 585.exe

static class E585
{
  static void Main()
  {
    for (string s; (s = Console.ReadLine()) != null;) {
      var ss = s.Split(':');
      var s2 = ss[2].Split();
      if (s2.Length > 5) continue;
      var s1 = ss[1].Split();
      if (s1.Length <= 3) continue;
      var s0 = ss[0].Split();
      var b = int.Parse(s0[0]);
      if (int.Parse(s0[1]) % b == 0) continue;
      if (int.Parse(s0[2]) % b == 0) continue;
      Console.WriteLine("{0,-11}: {1,-45}: {2}", ss[0], ss[1], ss[2]);
    }
  }
}

