#include <cstdio>
#include <cmath>
#include <iostream>
#include <functional>

int main() {
  const long N = 1e13; constexpr int v = sqrt(N);
  static long larges[v + 1]; static int smalls[v + 1], primes[v + 1];
  for (int i = 1; i <= v; ++i) smalls[i] = i - 1, larges[i] = N / i - 1;
  for (int p = 2; p <= v; ++p) if (smalls[p] > smalls[p - 1]) {
    int pcnt = smalls[p - 1]; long q = long(p) * p;
    for (int i = 1; i <= std::min(long(v), N / q); ++i) {
      long d = long(i) * p;
      larges[i] -= ((d <= v) ? larges[d] : smalls[N / d]) - pcnt;
    }
    for (int i = v; i >= q; --i) smalls[i] -= smalls[i / p] - pcnt;
  }
  int prime_count = 0;
  for (int i = 2; i <= v; ++i)
    if (smalls[i] > smalls[i - 1]) primes[prime_count++] = i;
  primes[prime_count++] = v + 1;
  std::function< long(long, int, int) > rec = [&] (long m, int pi, int lim) {
    long ret = ((m > v) ? larges[N / m] : smalls[m]) - pi;
    for (int pj = pi; pj < prime_count; ++pj) {
      int p = primes[pj], e = 1;
      long nm = m / p;
      if (nm < p) break;
      for (; nm >= p; nm /= p, ret += 1, e += 1) {
        ret += rec(nm, pj + 1, e);
        if (e == lim) break;
      }
    }
    return ret;
  };
  printf("%ld\n", rec(N, 0, 64) + 1);
  return 0;
}

