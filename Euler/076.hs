-- http://en.wikipedia.org/wiki/Partition_(number_theory)
import Data.Array ( (!),array )

main = let n = 100 in print $ pred $ let a = array ((0,0),(n,n)) $ ((0,0),1) : [((i,j), sum [a!(i-k, min (i-k) k) | k <-[1..j]]) | i <- [1..n], j <- [1..i]] in a!(n,n)

