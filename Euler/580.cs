using System;
using System.Linq;
using System.Collections.Generic;

static class E580
{
  static long n = (long)1e9, n2;
  static long[] a; // use for after square

  static long Lcm(long a, long b) { return a / Gcd(a, b) * b; }
  static long F(long d) { return (d > n2) ? 0 : (1 + (n/d/d - 1) / 4); }
  static long H(long n) { return 1 + (n-1)/4 - C(a.Length, 1, false); }

  static long Gcd(long a, long b)
  {
    for (long t; b != 0; a = b, b = t) t = a % b;
    return a;
  }

  static long C(int size, long m, bool odd)
  {
    if (m > n2) return 0;
    long z = 0;
    for (int i = size - 1; i >= 0; i--) {
      long d = Lcm(m, a[i]);
      if (d > n2) continue;
      if (odd) z -= F(d); else z += F(d);
      z += C(i, d, !odd);
    }
    return z;
  }

  static bool Multiple(List<long> list, long n)
  {
    foreach (var i in list)if (n % i == 0) return true;
    return false;
  }

  static void Init()
  {
    var list = new List<long>();
    for (long i = 5; i*i < n; i += 4)
      if (!Multiple(list, i)) list.Add(i);
    a = list.ToArray();
    Console.WriteLine("Count: {0:N0}", a.Length);
  }

  static void Main(string[] args)
  {
    if (args.Length > 0) n = long.Parse(args[0]);
    n2 = (long)Math.Sqrt(n);
    Console.WriteLine("n = {0:N0}", n);
    Init();
    Console.WriteLine("H(n) = {0:N0}", H(n));
  }
}

