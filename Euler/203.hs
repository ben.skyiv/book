import Data.Set as Set ( foldl,filter,fromList )
import Math.NumberTheory.Primes.Factorisation ( factorise )

main = let n = 51 in print $ Set.foldl (+) 0 $ Set.filter squarefree
  $ fromList [combin i j | i <- [2..n-1], j <- [0 .. div i 2]] where
  combin n r = div (product [n-r+1..n]) $ product [1..r]
  squarefree n = all (<2) $ map snd $ factorise n

