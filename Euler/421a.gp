f(n, m) = { z = 0; forprime(p = 2, m, a = polrootsmod(x^15+1, p);
  z += p * sum(i = 1, #a, (n+p-lift(a[i]))\p)); z}
print(f(10^11, 10^8)); quit() \\ runtime: 3m58.812s

