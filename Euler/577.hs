main = let n = 12345 in print $ div (sum [sum $ zipWith
  (\i j -> i*j*(j+1)) [1..] [i-2,i-5..1] | i <- [3..n]]) 2

