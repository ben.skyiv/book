main = print $ 724 + 12 * div (10^18 - 10024) 104
{-
import qualified Data.Set as Set

type Board = Set.Set Posn
data Direction = N | S | W | E deriving Show
data Posn = Posn Integer Integer deriving (Eq,Ord,Show)
data State = State Board Direction Posn deriving Show

initState = State Set.empty N (Posn 0 0)

nextStep (State b d posn@(Posn x y)) =
    if Set.member posn b
    then let newDir = counterClockwise d
         in State (Set.delete posn b) newDir (move newDir posn)
    else let newDir = clockwise d
         in State (Set.insert posn b) newDir (move newDir posn)

run state 0 = state
run state steps = run (nextStep state) (steps-1)

counterClockwise N = W
counterClockwise S = E
counterClockwise W = S
counterClockwise E = N

clockwise N = E
clockwise S = W
clockwise W = N
clockwise E = S

move N (Posn x y) = Posn x (y+1)
move S (Posn x y) = Posn x (y-1)
move W (Posn x y) = Posn (x-1) y
move E (Posn x y) = Posn (x+1) y

numBlacks :: State -> Integer
numBlacks (State b _ _) = fromIntegral $ Set.size b

bound = 1000000000000000000
baseSteps = 10752
periodSteps = 104
stepsToGo = bound - baseSteps
numPeriods = quot stepsToGo periodSteps
remainderSteps = mod stepsToGo periodSteps
s1 = run initState baseSteps
s2 = run s1 periodSteps
s3 = run s1 remainderSteps
b1 = numBlacks s1
b2 = numBlacks s2
b3 = numBlacks s3
solution = b1 + numPeriods*(b2-b1) + b3-b1
main = print solution
-}
