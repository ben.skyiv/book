main = let n = 124; m = 3*10^3; z = 10^5 in print $ (!!(n-1)) $ foldl
  (\xs t -> filter (\x -> mod t x /= 0) xs) [3,5..m] $ take z $
  map (\(a,b,c) -> a) $ iterate (\(a,b,c) -> (b,c,a+b+c)) (1,1,1)

