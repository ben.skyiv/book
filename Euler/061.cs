using System;
using System.Collections.Generic;

sealed class E061
{
  Dictionary<int, List<int>>[] dicts;
  bool[] mark = new bool[6];
  int[] set = new int[6];

  List<int> GetPolygonalNumbers(Func<int, int> f)
  {
    var list = new List<int>();
    for (var i = 0; ; i++)
    {
      var p = f(i);
      if (p < 1000) continue;
      if (p > 9999) break;
      list.Add(p);
    }
    return list;
  }
  
  Dictionary<int, List<int>> GetDictionary(List<int> list)
  {
    var dict = new Dictionary<int, List<int>>();
    foreach (var n in list)
    {
      var key = n / 100;
      List<int> value;
      if (!dict.TryGetValue(key, out value))
        dict.Add(key, value = new List<int>());
      value.Add(n);
    }
    return dict;
  }
  
  int Compute(int n, int idx)
  {
    if (idx > 5) return 1;
    int key = n % 100;
    set[idx] = n;
    if (idx == 5 && key == set[0] / 100) return 3;
    for (var i = 1; i < 6; i++)
    {
      if (mark[i]) continue;
      List<int> value;
      if (!dicts[i].TryGetValue(key, out value)) continue;
      mark[i] = true;
      foreach (var j in value)
        if (Compute(j, idx + 1) > 0) return 2;
      mark[i] = false;
    }
    return 0;
  }
  
  int Compute()
  {
    var p = new List<int>[6];
    p[0] = GetPolygonalNumbers(n => n*(n+1)/2);
    p[1] = GetPolygonalNumbers(n => n*n);
    p[2] = GetPolygonalNumbers(n => n*(3*n-1)/2);
    p[3] = GetPolygonalNumbers(n => n*(2*n-1));
    p[4] = GetPolygonalNumbers(n => n*(5*n-3)/2);
    p[5] = GetPolygonalNumbers(n => n*(3*n-2));
    dicts = new Dictionary<int, List<int>>[6];
    for (var i = 0; i < p.Length; i++) dicts[i] = GetDictionary(p[i]);
    foreach (var n in p[0])
      if (Compute(n, 0) > 0)
        break;
    var sum = 0;
    for (var i = 0; i < 6; sum += set[i++]) Console.Write("{0} ", set[i]);
    Console.WriteLine();
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E061().Compute());
  }
}
