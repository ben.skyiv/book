#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  int n = atoi(argv[1]), level = (n-1)/2, v = 1;
  long sum = 1;
  for (int i = 1; i <= level; i++)
    for (int k = 2 * i, j = 0; j < 4; j++)
      sum += v += k;
  printf("%d %d %ld\n", n, v, sum);
  return 0;
}
