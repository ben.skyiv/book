import Data.Ix ( range )
import Data.Map ( findWithDefault,fromDistinctAscList,fromListWith,toList )
 
data Step a = Step { minDigit::a, maxDigit::a, lastDigit::a } deriving (Eq,Ord)
 
main = let n = 40 in print $ sum . map (sum . map snd . filter ((\(Step a b _)
  -> a == 0 && b == 9) . fst) . toList) . take n $ iterate (\m -> fromListWith
  (+) $ [Step a b n | (a,b) <- range ((0,0), (9,9)), n <- range (a,b)] >>= 
  (\m s@(Step a b n) -> map (\x -> (x, findWithDefault 0 s m))
  [Step (min a (n-1)) b (n-1), Step a (max b (n+1)) (n+1)]) m)
  $ fromDistinctAscList [(Step i i i, 1) | i <- [1..9]]

