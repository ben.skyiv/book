def counts_and_sums(n, start):
    if n == 1:
        c = [1 - start] + [1] * 9
        s = range(10)
    else:
        c = [0] * (n * 9 + 1)
        s = [0] * (n * 9 + 1)
        k = 10 ** (n - 1)
        for j, (c_prev, s_prev) in enumerate(zip(*counts_and_sums(n - 1, 0))):
            for i in range(start, 10):
                c[i + j] += c_prev
                s[i + j] += s_prev + i * c_prev * k
    return c, s

def t(n):
    if n == 1:
        return 45
    odd = n % 2
    n //= 2
    k = 10 ** (n + odd)
    c = m = 0
    for cl, sl, ch, sh in zip(*counts_and_sums(n, 0) + counts_and_sums(n, 1)):
        c += sl * ch + sh * cl * k
        m += cl * ch
    return c * 10 ** odd + t(n * 2 - 1 + odd) + odd * 45 * m * k // 10

print t(47) % 3 ** 15

