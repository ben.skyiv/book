using System;
using System.Collections.Generic;

static class E580A
{
  static void Main(string[] args)
  {
    long n = (args.Length > 0) ? long.Parse(args[0]) : 10000000;
    var set = new HashSet<long>();
    for (long j, i = 5; (j = i*i) <= n; i += 4)
      for (long k = j << 2, m = j; m <= n; m += k)
        set.Add(m);
    Console.WriteLine("H({0:N0}) = {1:N0}", n, 1+(n-1)/4-set.Count);
  }
}

