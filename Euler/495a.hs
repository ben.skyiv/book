import Data.MemoTrie ( memo3 )

f :: Int -> Int -> Int -> Int
f n l 1 = 1
f n l k = sum $ map (\i -> g (div n i) (i+1) (k-1)) $
  takeWhile (\i -> div n i >= i+1) [i | i <- [l..], mod n i == 0]
g = memo3 f
w n k = g n 1 k
main = print $ w (product [2..14]) 10
-- W(14!,10) = 4,199,437    0m58.273s

