// http://en.wikipedia.org/wiki/Euler%27s_totient_function

using System;
using System.Collections.Generic;

sealed class E243
{
  int[] primes;
  int[] sieve;  // 0 for prime, else prime factor
  int[] totient;

  int Gcd(int a, int b)
  {
    for (int t; b != 0; a = b, b = t) t = a % b;
    return a;
  }

  SortedList<int, int> Merge(IEnumerable<int> list)
  {
    var v = new SortedList<int, int>();
    foreach (var p in list)
    {
      int n;
      if (!v.TryGetValue(p, out n)) v.Add(p, 1);
      else v[p] = n + 1;
    }
    return v;
  }
  
  SortedList<int, int> GetFactors(int x)
  {
    var list = new List<int>();
    var limit = (int)Math.Sqrt(x) + 1;
    for (int i = 0, p = primes[i]; p <= limit; p = primes[i])
      if (x % p == 0) { list.Add(p); x /= p; }
      else i++;
    if (x != 1) list.Add(x);
    return Merge(list);
  }

  int[] GetPrimes()
  {
    var primes = new List<int>();
    for (var i = 2; i < sieve.Length; i++)
      if (sieve[i] == 0) primes.Add(i);
    primes.Add(int.MaxValue);
    return primes.ToArray();
  }

  void Initialize(int max)
  {
    int limit = (int)Math.Sqrt(max);
    sieve = new int[max + 1];
    totient = new int[max + 1];
    for (int i = 2; i <= limit; i++)
      if (sieve[i] == 0)
        for (int j = i * i; j <= max; j += i)
          sieve[j] = i;
    primes = GetPrimes();
  }
  
  int Totient2(int n)
  {
    var factors = GetFactors(n);
    int v = 1;
    foreach (var kvp in factors)
      v *= (kvp.Key - 1) * Pow(kvp.Key, kvp.Value - 1);
    return v;
  }
  
  int Pow(int x, int y)
  {
    int z = 1;
    while (y-- > 0) z *= x;
    return z;
  }
  
  int Totient(int n)
  {
    if (n == 1) return 1;
    if (sieve[n] == 0) return n - 1;
    int a = sieve[n], b = n / a;
    if (Gcd(a, b) == 1) return totient[a] * totient[b];
    return Totient2(n);
  }
  
  long Compute(long n, long d, int max)
  {
    Initialize(max);
    for (var i = 2; i <= max; i++)
      if ((totient[i] = Totient(i)) * d < n * (i - 1)) return i;
    return 0;
  }

  static void Main(string[] args)
  { // dmcs 243.cs && ./243.exe 15499 94744 500000000
    var n = int.Parse(args[0]);
    var d = int.Parse(args[1]);
    var max = int.Parse(args[2]);
    Console.WriteLine(new E243().Compute(n, d, max));
  }
}

