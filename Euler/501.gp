f(n) = {
s=primepi(n^(1/7));
forprime(p=2,n^(1/3),k=n\p^3;s+=primepi(k)-(p<=k););
forprime(p=2,n^(1/3),forprime(q=p+1,sqrt(n\p),s+=primepi(n\(p*q))-primepi(q)));
s
};
print(f(10^12)); quit()

