// gcc -O3 -march=native -fopenmp 598.c
#include <stdio.h>

static int a[] = {97,48,/*24,16,9,7,*/5,5,4,3,3,2,2,2,2,1,1,1,1,1,1,1,1,1};

long f(int n, long x, long y)
{
  int b = a[n] + 2;
  if (n == 0) return b * y % (x + y) == 0;
  long z = 0;
  #pragma omp parallel for reduction(+:z)
  for (int i = b - 1; i > 0; i--) z += f(n-1, x*i, y*(b-i));
  return z;
}

int main(void)
{ // 91,609,369
  printf("%ld\n", f(sizeof(a)/sizeof(a[0])-1, 2L, 1L));
}

