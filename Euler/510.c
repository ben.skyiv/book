#include <stdio.h>
#include <math.h>

int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }

long s(int a, int b, int c, int n)
{
  long a2 = (long)a * a, b2 = (long)b * b, c2 = (long)c * c;
  long r = n / b2, z1 = a2 + b2 + c2, z2 = r * z1;
  return r * (z1 + z2) / 2;
}

int main(void)
{
  long z = 0; int n = 1000000000;
  for (int n2 = (int)sqrt(n), a = 2; a <= n2; a++)
    for (int b = a; b <= n2; b++)
    {
      long ab = (long)a * b;
      int d = a + b;
      if (ab % d != 0) continue;
      int c = (int)(ab / d);
      if (gcd(gcd(a, b), c) != 1) continue;
      z += s(a, b, c, n);
//fprintf(stderr, "(%d,%d,%d)", a, b, c);
    }
  printf("%ld\n", z);
}

