using System;

sealed class E504
{
  static int[,] zs;

  int GetZ(int a, int b)
  {
    var z = 0;
    for (var x = 1; x <= a - 1; x++)
    {
      var y = b - x * (double)b / a;
      var c = (int)y;
      if (c == y) c--;
      z += c;
    }
    return z;
  }
  
  int GetN(int a, int b, int c, int d)
  {
    return zs[a,b] + zs[a,d] + zs[c,b] + zs[c,d] + a + b + c + d - 3;
  }
  
  bool IsSquare(int n)
  {
    var z = Math.Sqrt(n);
    return z == (int)z;
  }

  int Compute(int m)
  {
    zs = new int[m+1, m+1];
    for (var a = 1; a <= m; a++)
      for (var b = 1; b <= m; b++)
        zs[a, b] = GetZ(a, b);
    var z = 0;
    for (var a = 1; a <= m; a++)
      for (var b = 1; b <= m; b++)
        for (var c = 1; c <= m; c++)
          for (var d = 1; d <=m; d++)
            if (IsSquare(GetN(a, b, c, d)))
              z++;
    return z;
  }

  static void Main()
  {
    Console.WriteLine(new E504().Compute(100));
  }
}

