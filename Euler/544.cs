using System;
using System.Drawing;

sealed class E544
{
  static readonly int mod = 1000000007;
  
  static void Main(string[] args)
  {
    Console.WriteLine("{0:N0}", new E544().S(
      (args.Length > 0) ? int.Parse(args[0]) : 4,
      (args.Length > 1) ? int.Parse(args[1]) : 4,
      (args.Length > 2) ? int.Parse(args[2]) : 1112131415));
  }

  int S(int row, int col, int nColor)
  {
    Console.Write("S({0},{1},{2}): ", row, col, nColor);
    var colorCnt = new Grid(row, col).ColorCnt;
    foreach (var i in colorCnt) Console.Write(" {0:N0}", i);
    Console.WriteLine();
    return S(colorCnt, nColor);
  }

  int S(long[] colorCnt, int nColor)
  {
    long z = 0;
    for (var i = Math.Min(nColor, colorCnt.Length - 1); i >= 2; i--)
      z = (z + colorCnt[i] % mod * P2(nColor, i)) % mod;
    return (int)z;
  }

  long P2(int n, int k)
  {
    long z = 1;
    for (int m = n + 1, v = k + 1, i = m - k,
      j = i + (v - i % v) % v; i <= m; i++)
      z = (z * ((i == j) ? (i / v) : i)) % mod;
    return z;
  }
}

sealed class Grid
{
  readonly int size;
  readonly Point[] check;
  readonly int[] grid;
  readonly long[] colorCnt;
  public long[] ColorCnt { get { return colorCnt; } }

  public Grid(int row, int col)
  {
    colorCnt = new long[(size = row * col) + 1];
    grid = new int[size + 1]; grid[1] = 1; grid[2] = 2;
    check = GetCheck(col);
    Update(2, 3);
  }

  void Update(int nColor, int k)
  {
    int maxColor = nColor + 1, x = grid[check[k].X], y = grid[check[k].Y];
    if (k == size)
    { // assume row > 1 && col > 1, else: (x == y || x == 0 || y == 0)
      colorCnt[nColor] += nColor - ((x == y) ? 1 : 2);
      colorCnt[maxColor]++;
      return;
    }
    for (int color = 1; color <= maxColor; color++)
    {
      if (color == x || color == y) continue;
      if (nColor < (grid[k] = color)) nColor = color;
      Update(nColor, k+1);
    }
  }

  Point[] GetCheck(int col)
  {
    var a = new Point[size + 1];
    for (var k = 1; k < a.Length; k++)
    {
      a[k].X = ((k - 1) % col == 0) ? 0 : (k - 1);
      a[k].Y = Math.Max(0, k - col);
    }
    return a;
  }
}

/*
  int S(int[] colorCnt, int nColor)
  {
    long z = 0;
    for (var i = 2; i <= nColor; i++)
      z = (z + F(colorCnt, i)) % mod;
    return (int)z;
  }

  long F(int[] colorCnt, int nColor)
  {
    long z = 0;
    for (var i = Math.Min(nColor, colorCnt.Length - 1); i >= 2; i--)
      z = (z + colorCnt[i] * P(nColor, i)) % mod;
    return z;
  }

  long P(int n, int k)
  {
    long z = 1;
    for (var i = 0; i < k; i++)
      z = z * (n - i) % mod;
    return z;
  }
*/

