using System;
using System.Collections.Generic;

sealed class E012
{
  int[] primes;

  E012(int max)
  {
    var list = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) list.Add(i);
    list.Add(int.MaxValue);
    primes = list.ToArray();
  }

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  SortedList<long, int> Merge(IEnumerable<long> list)
  {
    var v = new SortedList<long, int>();
    foreach (var p in list)
    {
      int n;
      if (!v.TryGetValue(p, out n)) v.Add(p, 1);
      else v[p] = n + 1;
    }
    return v;
  }
  
  SortedList<long, int> GetFactors(long x)
  {
    var list = new List<long>();
    var limit = (int)Math.Sqrt(x) + 1;
    for (int i = 0, p = primes[i]; p <= limit; p = primes[i])
      if (x % p == 0) { list.Add(p); x /= p; }
      else i++;
    if (x != 1) list.Add(x);
    return Merge(list);
  }
  
  int Divisors(int n)
  {
    var v = 1;
    foreach (var factor in GetFactors(n))
      v *= factor.Value + 1;
    return v;
  }
  
  int Compute()
  {
    for (var i = 1; i < 123456; i++)
    {
      var n = i * (i + 1) / 2;
      if (Divisors(n) >= 500) return n;
    }
    return 0;
  }

  static void Main()
  {
    Console.WriteLine(new E012(123456).Compute());
  }
}

