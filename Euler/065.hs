import Data.List
import Data.Ratio
digitsRev b = unfoldr (\x -> if x == 0 then Nothing else let (q,r) = divMod x b in Just(r,q))
main = let n = 100 in print $ sum $ digitsRev 10 $ numerator $ (+) 2 $
  foldr (\x a -> (\r -> denominator r % numerator r) $ x+a) 0 $ take (n-1) $
  1:2:concat [ [1,1,x] | x <- [4,6..] ]

