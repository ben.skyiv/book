import Data.Set ( elems )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Factorisation ( divisors )

ts (xs:xss) = if head (head xss) > head xs then [xs] else xs : ts xss
ds (xs:xss) = if head (head xss) > head xs then xss  else ds xss
rs (xs:xss) n = drop (if n == 0 then 1 else 0) xs : rs xss (n-1); rs _ _ = []
ms lss zss = f lss [] (-1) (-1) zss where
  f (ls:lss) rss v i zss = if v > -1 && head ls >= v then
    f lss (ls:rss) v (i+1) zss else f lss (ls:rss) (head ls) 0 zss
  f _ rss v i zss = if v < head (head zss) then v : ms (rs rss i) zss
    else head (head zss) : ms (tail (head zss) : rss) (tail zss)
f m n = g (reverse $ takeWhile (<m) primes) 1 where
  g (p:[]) c = [p^i | i <- [0..], c*i+c >= n]
  g (p:ps) c = let z = [[x*p^i | x <- g ps $ c*i+c] | i <- [0..]] in ms (ts z) (ds z)
t s = length $ filter (\(a,b) -> let (q,r) = divMod (b-2) (a+1) in q+q+r+4 < a)
  $ takeWhile (\(a,b) -> a < b) [(a, div s a) | a <- elems $ divisors s]
main = let n = 200; m = 30 in print $ head [s | s <- f m (n+n), t s == n]

