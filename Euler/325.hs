f n m = if n == 1 then (1,1) else let
  u = floor $ fromIntegral n / ((1+5**0.5)/2); (a,b) = f u m
  v = mod (u * div (n*n+n) 2 + div ((n*n+n)*(2*n+1)) 6 - a) m
  in (mod (v + div (n*u*(u+1)) 2 - div (n^3-n) 6 - b) m, v)

main = let n=10^16; m=7^10 in print $ mod (div((4*n-1)*(n*n+n))6 - (fst $ f n m)) m

