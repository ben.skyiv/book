import Math.NumberTheory.Primes.Factorisation
main = print $ sum [ div (τ x) 2 | x <- [2 .. div (10^6) 4] ]

