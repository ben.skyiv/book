#include <stdio.h>

int l(int x, int y)
{
  int z = 1;
  for (; y > 1; x++, z++) y = (int)((long)y * y % x);
  return z;
}

int g(int x)
{
  int z = 0;
  for (int w, y = x / 2; y > 1; y--)
    if (z < (w = l(x, y))) z = w;
printf("%d ",z);
    return z;
}

int main(void)
{
  int z = 0, n = 100;
  for (int w, x = n; x > 3; x--)
    if (z < (w = g(x))) z = w;// else break;
  printf("f(%d) = %d\n", n, z);
}
