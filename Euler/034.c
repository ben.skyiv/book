#include <stdio.h>

static int factorials[] = { 1,1,2,6,24,120,720,5040,40320,362880 };

int dfsum(int n)
{
  int sum = 0;
  for (; n > 0; n /= 10) sum += factorials[n % 10];
  return sum;
}

int main(void)
{
  int sum = 0;
  for (int i = 10; i < 12345678; i++)
  {
    if (i != dfsum(i)) continue;
    printf("%d %d\n", i, sum += i);
  }
  return 0;
}

