import Data.List ( zipWith4 )

z = let x = tail z; y = tail x in 0:0:1:1:zipWith4 (\a b c d -> a+b+c+d) z x y (tail y)
main = let n = 50 in print $ z !! (n+2)

