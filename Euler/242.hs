f x | x < 2 = x | even x = 3 * f (div x 2) | True = let z = div (x-1) 2 in 2 * f z + f (z+1)
main = let n = 10^12 in print $ f $ succ $ div (n-1) 4

