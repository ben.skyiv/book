import numpy

def f(k, m): # counts palindromes divisible by m with k, k-2, k-4,... digits
  A = [10**i % m for i in range(k)]
  B = [(A[i] + A[-(i+1)]) % m for i in range(k//2)]
  if k % 2 == 1: B.append(A[k//2])
  V = numpy.array([1] + [0]*(m-1))
  for x in B:
    W = numpy.copy(V)
    for d in range(1, 10):
      i = x*d % m
      if i == 0: V += W
      else: V[:i] += W[-i:]; V[i:] += W[:-i]
  return V[0] - 1

def main(k, m): return f(k, m) + f(k-1, m)

print(main(32, 10000019))

