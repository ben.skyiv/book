#include <stdio.h>
#include <math.h>

int main()
{
  const int n = 1000000, m = (int)(log(n) / log(2));
  static int a[n];
  a[0] = 9;
  for(int i = 0; i <= n - 10; i += 9)
    for (int j = 1; j <= 9; j++)
      a[i+j] = j;
  for(int x = 2, i = 1; i <= m; i++, x *= 2)
    for(int z = (i == m) ? n : (x+x), j = 2; j*j < z; j++)
      for(int c = j-(j-1)/9*9, k = j*(1+(x-1)/j); k < z; k += j)
        if(a[k] < c + a[k/j]) a[k] = c + a[k/j];
  int sum = 0;
  for(int i = 2; i < n; i++) sum += a[i];
  printf("%d\n", sum);
  return 0;
}

