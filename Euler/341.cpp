#include<cstdio>  
#include<algorithm>  
using namespace std;  

const long M = 2000000; //180000000000;
static long g[M];  

long f(long n) { return lower_bound(g + 1, g + M, n) - g; }

int main()  
{  
  g[1] = 1; g[2] = 3;  
  for (long i = 3; i < M; i++) g[i] = g[i-1] + lower_bound(g+1, g+i, i) - g;
  long sum = 0;
  for (long i = 1; i < 1000; i++) sum += f(i*i*i);
  printf("%ld\n", sum);
  return 0;  
}

