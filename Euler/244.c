#include <stdio.h>
#include <memory.h>

const int WIDE = 4, SIZE = WIDE*WIDE, MAX = 40, SIZE2 = 1048576, target = 0x5A5A;
static unsigned char cache[SIZE2];
static char moves[MAX];

void go(const char *_a, unsigned char dist, char dir, int from, int to)
{
  char a[SIZE];
  memcpy(a, _a, SIZE);
  if( dir ) moves[dist++] = dir;
  a[from] = a[to];
  a[to] = '.';
  int id = 0;
  for(int i=0;i<SIZE;i++) id |= (a[i]=='1')?(1<<i):(0);
  id |= to<<SIZE;
  if( cache[id]<dist ) return;
  cache[id] = dist;
  if( id==target ) {printf("%d moves: %.*s\n",dist,dist,moves); return; };
  if( dist>=MAX ) return;
  if( dir!='U' && to-WIDE>=0      ) go(a,dist,'D',to,to-WIDE);
  if( dir!='L' && to%WIDE!=0      ) go(a,dist,'R',to,to-1   );
  if( dir!='R' && to%WIDE!=WIDE-1 ) go(a,dist,'L',to,to+1   );
  if( dir!='D' && to+WIDE<SIZE    ) go(a,dist,'U',to,to+WIDE);
}

int main()
{
  memset(cache, 0xFFFFFFFF, SIZE2);
  go(".011001100110011", 0, 0, 0, 0);
}

