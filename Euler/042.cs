using System;

sealed class E042
{
  bool[] triangles;
  
  E042()
  {
    var n = 100;
    triangles = new bool[n*(n+1)/2 + 1];
    for (; n > 0; n--) triangles[n*(n+1)/2] = true;
  }

  int Value(string word)
  {
    var sum = 0;
    foreach (var c in word) sum += c - 'A' + 1;
    return sum;
  }
  
  int Compute()
  {
    var count = 0;
    foreach (var word in Console.ReadLine().Split(','))
      if (triangles[Value(word.Trim('"'))]) count++;
    return count;
  }
  
  static void Main()
  {
    Console.WriteLine(new E042().Compute());
  }
}
