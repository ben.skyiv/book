import Data.List
pandigital m n = (=="123456789") . sort $ show m ++ show n ++ show (m*n)
main = print . sum . nub $ [ x*y | x <- [2..98], y <- [123..9786], pandigital x y ]

