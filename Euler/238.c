#include <stdio.h>

static int s0 = 14025256, m = 20300713, n = 1, a[90000000], b[20000000];
int g(int n) { int i = 0; while (n > 0) n /= 10, i++; return i; }
int d(int n, int m) { while (m > 0) n /= 10, m--; return n % 10; }
void f(int m) {	for (int i = g(m)-1; i >= 0; n++) b[n] = d(m,i--)+b[n-1]; }

int main(void)
{
  for (int x = s0; f(x),1; ) if ((x = (long)x*x%m) == s0) break;
  int u = b[n-1];
  for (int i = 1; i <= 100; i++)
    for (int k, j = 1; j < n; j++) {
      if ((k = b[j]-b[i-1]) < 0) k += u;
      if (a[k] == 0) a[k] = i;
    }
  long sum = 0, w, z = 2000000000000000;
  for (int i = 1; i < u; sum += a[i++]*w) w = (z-i)/u + 1;
  for (w = u; w <= z; w += u) sum += a[u];
  printf("%ld\n", sum);
  return 0;
}

