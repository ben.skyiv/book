#include <stdio.h>
#include <math.h>

int is_triangular(long n)
{ // n must >= 0
  double d = sqrt(1 + (n << 3));
  long a = (long)d;
  return a == d && ((a - 1) & 1) == 0;
}

int main()
{
  long n = 17526000000000;
  long z = 0, i9 = ((long)sqrt(1 + ((n / 3) << 3)) - 1) >> 1;
  #pragma omp parallel for reduction(+:z) schedule(dynamic)
  for (long i = 0; i <= i9; i++)
    for (long c, a = (i*(i+1)) >> 1, b9 = (n - a) >> 1,
      j = i, b = a; b <= b9; b += ++j)
      if ((c = n - a - b) >= b && is_triangular(c))
        z += (a == c) ? 1 : (a == b || b == c) ? 3 : 6;
  printf("G(%ld) = %ld\n", n, z);
}
