#include <stdio.h>

typedef struct { long ps[12]; char ks[12], n; } item;

const long n = (long)1e11, m = 998244353;
const int n2 = 1 + 316228; // n2 > sqrt(n + n2)

long power(long a, int b)
{
  long v = 1;
  for (; b; b >>= 1, a *= a) if (b & 1) v *= a;
  return v;
}

void factor(long i0, item a[], const char c[])
{
  static long t[n2];
  for (int i = 0; i < n2; i++) t[i] = 1, a[i].n = 0;
  for (int p = 2; (long)p * p < i0 + n2; p++)
    if (!c[p])
      for (long q = p; q < i0 + n2; q *= p)
        for (long i = q-1-(i0-1)%q; i < n2; t[i] *= p, i += q) {
          if (q != p) ++a[i].ks[a[i].n - 1];
          else a[i].ps[a[i].n] = p, a[i].ks[a[i].n++] = 1;
        }
  for (int i = 0; i < n2; i++) {
    if (i + i0 == t[i]) continue;
    a[i].ps[a[i].n] = (i + i0) / t[i];
    a[i].ks[a[i].n++] = 1;
  }
}

long phi(item a)
{
  long z = 1;
  for (int i = 0; i < a.n; i++) z *= (a.ps[i]-1) * power(a.ps[i], a.ks[i]-1);
  return z;
}

int main(void)
{ // g(100) = 18065
  static item a[n2]; static char c[n2];
  for (int i = 2; i * i < n2; i++)
    if (!c[i]) for (int j = i * i; j < n2; j += i) c[j] = 1;
  long z = 0;
  for (long i0 = -n2, i = 1; i <= n; i++) {
    if (i >= i0 + n2) factor(i0 = i, a, c);
    long u = n / i % m;
    z = (z + phi(a[i - i0]) % m * u % m * u) % m;
  }
  long m2 = (m + 1) / 2;
  printf("%ld\n", (z + (n+1) % m * (n%m) % m * m2 % m) * m2 % m);
}

