using System;
using System.Collections.Generic;

static class E598
{
  static int[] exps = {97,48,24,16,9,7,5,5,4,3,3,2,2,2,2,1,1,1,1,1,1,1,1,1};
  static Dictionary<Tuple<long, long>, int> dict
   = new Dictionary<Tuple<long, long>, int>();
  static long z = 0;
  static long Gcd(long a, long b) { return (b == 0) ? a : Gcd(b, a % b); }

  static int GetSplitPosition()
  {
    int i = 0, j = exps.Length - 1;
    for (long p = exps[0] + 1, q = exps[j] + 1; i < j; )
      if (p > q) q *= exps[--j] + 1;
      else p *= exps[++i] + 1;
    return i;
  }

  static void Build(int n0, int n, long x, long y)
  {
    int b = exps[n] + 2;
    if (n == n0) {
      for (int i = b - 1; i > 0; i--) {
        long u = x * i, v = y * (b - i);
        var d = Gcd(u, v); var key = Tuple.Create(u / d, v / d);
        int w; dict.TryGetValue(key, out w); dict[key] = w + 1;
      }
    } else for (int i = b - 1; i > 0; i--) Build(n0, n-1, x*i, y*(b-i));
  }
  
  static void Make(int n0, int n, long x, long y)
  {
    int b = exps[n] + 2;
    if (n == n0) {
      for (int i = b - 1; i > 0; i--) {
        long u = x * i, v = y * (b - i);
        var d = Gcd(u, v); var key = Tuple.Create(v / d, u / d);
        int w; if (dict.TryGetValue(key, out w)) z += w;
      }
    }
    else for (int i = b - 1; i > 0; i--) Make(n0, n-1, x*i, y*(b-i));
  }

  static void Main()
  {
    var k = GetSplitPosition(); Console.WriteLine("Split in {0}", k);
    Build(0, k, 1, 1); Console.WriteLine("Count: {0:N0}", dict.Count);
    Make(k + 1, exps.Length - 1, 2, 1); Console.WriteLine("{0:N0}", z);
  }
}

