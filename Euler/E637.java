public class E637 {
  public static void main(String[] args) {
    int n = 10000000, b1 = 10, b2 = 3;
    int[] m1 = new int[n+1];
    int[] m2 = new int[n+1];
    long g = 0;
    for (int i = 1; i <= n; i++)
      if (f(i, b1, m1) == f(i, b2, m2)) g += i;
    System.out.format("g(%d,%d,%d)=%d%n", n, b1, b2, g);
  }

  static int f(int n, int b, int[] m) {
    if (n < b) return 0;
    if (m[n] != 0) return m[n];
    String s = Integer.toString(n, b);
    int len = s.length(), min = 1<<30;
    for (int i = (1<<(len-1))-1; i > 0; i--) {
      int j = i, tsum = 0, sum = 0;
      for (int k = 0; k < len; k++, j >>= 1) {
        sum *= b; sum += s.charAt(k) - '0';
        if ((j & 1) != 0) { tsum += sum; sum = 0; }
      }
      min = Math.min(min, f(tsum += sum, b, m));
      if (min <= 1) break;
    }
    return m[n]  = 1 + min;
  }
}

