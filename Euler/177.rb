require 'set'

EPS = 1.0e-9
$cot = (0..180).map{|a| 1 / Math.tan(a * Math::PI / 180)}
set = Set.new
for a in 1..45
  for b in a..(178 - a)
    for c in a..(179 - a - b)
      for d in a..(179 - b - c)
        t1 = $cot[c + d] + $cot[b] - $cot[a + b] - $cot[c]
        t2 = $cot[a + b] * $cot[c + d] - $cot[b] * $cot[c]
        t = Math.atan(t1 / t2) * 180 / Math::PI
        if (t - t.round).abs < EPS
          t = t.round
          e = 180 - b - c - d
          f = (b - t + 180) % 180
          g = (c + t + 180) % 180
          h = 180 - a - b - c
          next if e < a or f < a or g < a or h < a
          set.add [[a, b, c, d], [c, d, e, f], [e, f, g, h], [g, h, a, b],
                   [d, c, b, a], [f, e, d, c], [h, g, f, e], [b, a, h, g]].sort.first
        end
      end
    end
  end
end
puts set.size

