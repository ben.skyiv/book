#include <stdio.h>
#include <math.h>

int main(int argc, char**argv)
{
  const int r = 50, n = 21;
  static int a[n];
  for(int i = 0; i < n; i++)
    a[i%2 != 0 ? (i-1)/2 : n-1-i/2] = r - i;
  double len = a[0] + a[n-1];
  for (int i = 0; i < n - 1; i++)
    len += 2*sqrt(r*(a[i]+a[i+1]-r));
  printf("%d\n", (int)(len * 1000 + 0.5));
  return 0;
}

