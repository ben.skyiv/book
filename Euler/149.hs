import Data.Array ( (!),listArray )

n = 2000
fib = listArray (0,n^2-1) [(flip (-) 500000) . (flip mod) (10^6) $
  if k < 56 then 100003 - 200003*k + 300007*k^3
  else fib!(k-25) + fib!(k-56) + 10^6 | k <- [1..n^2]]
row a i = map (a!) [i*n .. (i+1)*n-1]
col a i = map (a!) [i,n+i .. n*(n-1)+i]
d1  a i = map (a!) $ if i < 0 then [k + n*(k+i) | k <- [-i..n-1]]
  else [i*n + k*(n+1) | k <- [0..n-i-1]]
d2  a i = map (a!) $ if i < 0 then [n*(k-i) + n-k-1 | k <- [0..n+i-1]]
  else [n*k - i + n-k-1 | k <- [0..n-i-1]]
f = snd . foldl (\(s,m) x -> let z = max (s+x) 0 in (z, max m z)) (0,0)
main = print $ foldl1 max $ concat [
  map (f . row fib) [0..n-1], map (f . d1 fib) [2-n..n-2],
  map (f . col fib) [0..n-1], map (f . d2 fib) [2-n..n-2]]

