#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static int a[(int)6e6]; // len > π(√(1e16)), primes from 3

int sieve(int max)
{
  char *c = malloc(max);
  int high = (int)sqrt(max);
  for (int i = 2; i <= high; i++)
    if (c[i] == 0)
      for (int j = i * i; j <= max; j += i)
        c[j] = 1;
  int j = 0;
  for (int i = 3; i < max; i += 2) if (c[i] == 0) a[j++] = i;
  free(c);
  printf("Count: %d\n", j); fflush(stdout);
  return j;
}

long gcd(long a, long b)
{ for (long t; b != 0; a = b, b = t) t = a % b; return a; }
long lcm(long a, long b) { return a / gcd(a, b) * b; }
long f(long n, long n2, long d)
{ return (d > n2) ? 0 : (1 + ((n/d/d - 1) >> 2)); }

long c(long n, long n2, int size, long m, int odd)
{
  if (m > n2) return 0;
  long z = 0;
  for (int i = size - 1; i >= 0; i--) {
    long d = lcm(m, a[i]);
    if (d > n2) continue;
    if (odd) z -= f(n, n2, d);
    else     z += f(n, n2, d);
    z += c(n, n2, i, d, !odd);
  }
  return z;
}

long h(long n, long n2, int size)
{ return 1 + (n-1) / 4 - c(n, n2, size, 1, 0); }

int main(void)
{ // 10^16: 2,327,213,148,095,366 // 10^7: 2,327,192
  long n = (long)1e7; int n2 = (int)sqrt(n);
  printf("n = %ld\n", n); fflush(stdout);
  int size = sieve(n2), m = size;
  long z1 = 0, z = 0;
  for (int p = 1, i = 0; i < size; p = a[i++]) {
    if (p != 1 && p % 4 == 1) continue;
    n = n / p / p;
    if (n < 2) break;
    n2 = (int)sqrt(n);
    m = 0; while (m < size && a[m] <= n2) m++;
    z1 = h(n, n2, m);
    z += z1;
printf("%2d %8ld %4d:%4d %3d %7ld\n",
p,n,n2,a[m-1],m,z1);fflush(stdout);
  } // error: z value
  printf("%ld\n", z); fflush(stdout);
}

