sealed class E154
{
  int[] n5s, n2s;

  E154(int n)
  {
    n5s = new int[n + 1];
    n2s = new int[n + 1];
    for (var i = 0; i <= n; i++)
    {
      n5s[i] = GetNs(5, i);
      n2s[i] = GetNs(2, i);
    }
  }

  int GetNs(int b, int n)
  {
    var nz = 0;
    for (var z = b; z <= n; z *= b) nz += n / z;
    return nz;
  }

  int Compute(int lgm)
  {
    int sum = 0, n = n5s.Length-1, v5 = n5s[n]-lgm, v2 = n2s[n]-lgm;
    for (var i = 0; i <= n; i++)
      for (int n5 = n5s[i], n2 = n2s[i], j = 0; j <= n - i; j++)
        if (v5 >= n5+n5s[j]+n5s[n-i-j] && v2 >= n2+n2s[j]+n2s[n-i-j])
          sum++;
    return sum;
  }

  static void Main(string[] args)
  {
    System.Console.WriteLine(new E154
      (int.Parse(args[1])).Compute(int.Parse(args[0])));
  }
}

