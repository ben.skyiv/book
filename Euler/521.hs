import Data.Array ( (!),listArray )
import Data.MemoTrie ( memo2 )
import Math.NumberTheory.Primes.Sieve ( primes )

n = 10^2; m = 10^9; n1 = length a1; prs = listArray (1, n1) a1
a1 = takeWhile ((>=) $ floor $ sqrt $ fromIntegral n) primes
d = memo2 d0; s = memo2 s0
d0 n 0 = n; d0 n i = d n (i-1) - d (div n $ prs!i) (i-1)
s0 n 0 = div (n*(n+1)) 2; s0 n i = s n (i-1) - (prs!i) * s (div n $ prs!i) (i-1)
main = print $ flip mod m $ pred $ (+) (s n n1)
  $ sum [(prs!i) * d (div n $ prs!i) (i-1) | i <- [1 .. n1]]

