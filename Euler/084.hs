import qualified Data.Foldable as F
import           Data.Function (on)
import           Data.List (sortBy, foldl1', foldl')
import qualified Data.Map as M
import           Data.Ord (comparing)
import           GHC.Exts (Down(..))
import           System.Random

-- What are the spaces that are most often occupied at the end of a turn.
-- The board has 40 spaces, using 2 4-die. Some spaces have side effects.

-- space x double count -> probability
type Pmf = M.Map (Int,Int) Double
space = fst
prob = snd
probs = M.elems . combineDice
combineDiceAssocs = M.assocs . combineDice
-- squish the state of being on a space with double count to just being on a space
combineDice = M.fromAscListWith (+) . map (\ ((s,_),p) -> (s,p)) . M.assocs

-- roll 1 turn at a time until the distribution converges
euler84 = combine . topThree . untilConverges roll $ start
combine = foldl1' (\x y -> 100*x+y)
topThree = take 3 . map space . sortBy (comparing (Down . prob)) . combineDiceAssocs
untilConverges f s = let s' = f s
                     in if (dot `on` probs ) s s' > 0.999999 then s' else untilConverges f s'
dot u v = sum (zipWith (*) u v) / (norm u * norm v)
  where
    norm = sqrt . sum . map (^2)
    
-- | (space x doublecount) x prob, starting on go no doubles certainly
start :: Pmf
start = M.singleton (0,0) 1

-- roll the die and apply effects to all spaces
roll :: Pmf -> Pmf
roll pmf = sideEffect $
           M.fromListWith (+)          -- sum probabilities for those with same space,double count
           [jailTest $ move s r dc d p |
            ((s,dc),p) <- M.assocs pmf,
            (r,d) <- roll2die]
jailTest x@((s,d),p) = if s==g2j then ((jail,0),p) else x
-- roll and account for consecutive triples
move s r dc d p = (case (if d then dc+1 else 0) of 3 -> (jail,0)
                                                   dc' -> ((s+r) `rem` 40, dc') ,
                   rollProb p)
roll2die = [(i+j, i==j) | i<-[1..sides], j<-[1..sides]]
sides = 4

-- process side effects of landing on a space and rolling triple doubles
sideEffect :: Pmf -> Pmf
sideEffect pmf = chest . chance $ pmf where
  chance pmf = let (yes,no) = M.partitionWithKey (\ (s,_) _ -> any (==s) chs) pmf
                   allMoves ((s,d),p) = map (\f -> (f s d, cardProb p)) chfs
               in M.unionsWith (+) [no, M.map (\x -> stayChance x) yes, M.fromListWith (+) (concatMap allMoves (M.assocs yes))]
  chest pmf = let (yes,no) = M.partitionWithKey (\ (s,_) _ -> any (==s) ccs) pmf
                  goAndJail ((s,d),p) = map (\x -> (x, cardProb p)) [(advGo s, d), (jail,0)]
              in M.unionsWith (+) [no, M.map (\x -> stayChest x) yes, M.fromListWith (+) (concatMap goAndJail (M.assocs yes))]

rollProb = if sides==4 then scaleFloat (-4) else scaleFloat (-2) . (/9) -- /36
cardProb = scaleFloat (-4)
stayChance p = scaleFloat (-4) (6*p) -- 6/16
stayChest p = scaleFloat (-4) (14*p) -- 14/16

chfs = (\s _ -> (goJail s,0)) :
       map (\f -> \s d -> (f s, d)) [advGo, const 11, const 24, const 39, const 5, nextR, nextR, nextU, subtract 3]

advGo = const 0
goJail = const jail
jail = 10
g2j = 30
ccs = [2,17,33]
chs = [7,22,36]
-- shift last to zero, flatten, squish to [0..], expand as arith progression
-- rr's are 0..3 10i+5
nextR i = (i+5) `rem` 40 `quot` 10 * 10 + 5
-- u's are 0,1 16i+12, little hacky but works
nextU i = (i+12) `rem` 40 `quot` 24 * 16 + 12

main = print euler84

