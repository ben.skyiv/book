using System;
using System.Collections.Generic;

sealed class E512
{
  long ModPow(long v, int a, long b, int m)
  { // return: (v * a^b) % m
    for (long c = a; b != 0; b >>= 1, c = c * c % m)
      if ((b & 1) != 0)
        v = v * c % m;
    return v;
  }
  
  List<int>[] GetPrimeFactors(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var pfs = new List<int>[max + 1];
    for (int i = 2; i <= limit; i++)
      if (pfs[i] == null)
        for (int j = i + i; j <= max; j += i)
        {
          if (pfs[j] == null) pfs[j] = new List<int>();
          pfs[j].Add(i);
        }
    return pfs;
  }
  
  List<Tuple<int, int>> Factorise(int n, List<int> primes)
  {
    var list = new List<Tuple<int, int>>();
    if (primes != null)
    {
      foreach (var p in primes)
      {
        var k = 0;
        for (; n > 0 && n % p == 0; k++) n /= p;
        list.Add(Tuple.Create(p, k));
      }
      if (n > 1) list.Add(Tuple.Create(n, 1));
    }
    else list.Add(Tuple.Create(n, 1));
    return list;
  }
  
  int Totient(List<Tuple<int, int>> factors, long e, int m)
  {
    long v = 1;
    foreach (var i in factors)
      v = ModPow(v, i.Item1, e * i.Item2 - 1, m) * (i.Item1 - 1) % m;
    return (int)v;
  }
  
  int F(int n, List<Tuple<int, int>> factors)
  {
    int m = n + 1, v = 0;
    for (var i = 1; i <= n; i++) v = (Totient(factors, i, m) + v) % m;
    return v;
  }
  
  long Compute(int n)
  {
    Console.Error.WriteLine("{0:dd HH:MM:ss} {1,10:N0}", DateTime.Now, n);
    var v = 1L;
    var pfs = GetPrimeFactors(n);
    for (var i = 2; i < pfs.Length; i++)
    {
      v += F(i, Factorise(i, pfs[i]));
      if (i % 1000 != 0) continue;
      Console.Error.WriteLine("{0:dd HH:MM:ss} {1,10:N0} ", DateTime.Now, i);
    }
    return v;
  }

  static void Main()
  {
    Console.WriteLine(new E512().Compute(100));
  }
}

