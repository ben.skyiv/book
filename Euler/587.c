#include <stdio.h>
#include <math.h>

double f(double x)
{
  return x - 0.5*((x-1)*sqrt(x*(2-x))+asin(x-1));
}

double area(int n)
{
  double x = (n+1-sqrt(2*n)) * n / (n*n+1);
  return f(1) - f(x) + 0.5 * x * x / n;
}

int main(void)
{
  int n = 1;
  while (area(n) / (1 - M_PI/4) >= 0.001) n++;
  printf("%d\n", n);
}
