using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class E618
{
  static Stack<int> stack = new Stack<int>();
  static int[] ps;
  static long z, m = (long)1e9;

  static void S(int n, int i0)
  {
    for (var i = i0; i < ps.Length; i++) {
      stack.Push(ps[i]);
      var n1 = n - ps[i];
      if (n1 == 0) {
        long z0 = 1;
        foreach (var k in stack) z0 = (z0 * k) % m;
        z = (z + z0) % m;
      }
      if (n1 <= 0) { stack.Pop(); break; }
      S(n1, i); stack.Pop();
    }
  }

  static long S(int n)
  {
    stack.Clear();
    ps = Primes.GetPrimes(0, n);
    z = 0; S(n, 0);
    return z;
  }

  static void Main()
  { // http://oeis.org/A002098
    for (var i = 1; i <= 21; i++) Console.Write(S(i) + ",");
    Console.WriteLine();
  }
}

