#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static char a[2000001];

void sieve(int max)
{
  int high = (int)sqrt(max);
  for (int i = 2; i <= high; i++)
    if (a[i] == 0)
      for (int j = i * i; j <= max; j += i)
        a[j] = 1;
}

long primesSum(int max)
{
  sieve(max);
  long sum = 0;
  for (int i = 2; i <= max; i++)
    if (a[i] == 0) sum += i;
  return sum;
}

int main(int argc, char *argv[])
{
  printf("%ld\n", primesSum((argc > 1) ? atoi(argv[1]) : 10));
  return 0;
}

