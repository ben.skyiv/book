using System;

sealed class E521
{
  int[] GetPrimeFactors(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var pfs = new int[max + 1];
    for (int i = 2; i <= limit; i++)
      if (pfs[i] == 0)
        for (int j = i * i; j <= max; j += i)
          if (pfs[j] == 0) pfs[j] = i;
    return pfs;
  }
  
  long Compute(int n)
  {
    long v = 0;
    var pfs = GetPrimeFactors(n);
    for (var i = 2; i <= n; i++) v += (pfs[i] == 0) ? i : pfs[i];
    return v;
  }

  static void Main()
  {
    Console.WriteLine(new E521().Compute(1000000));
  }
}
// 10^2:          1,257
// 10^3:         79,189
// 10^4:      5,786,451
// 10^5:    455,298,741
// 10^6: 37,568,404,989

