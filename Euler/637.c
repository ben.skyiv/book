#include <stdio.h>

int ds(int n, int b) { return (n < b) ? n : (n % b + ds(n / b, b)); }

int test(int n, int k, int b)
{
  for (int d = b; d < n; d *= b) if (test(n / d, n % d + k, b)) return 1;
  return ds(n + k, b) < b;
}

int f(int n, int b)
{
  if (n < b) return 0;
  if (ds(n, b) < b) return 1;
  return test(n, 0, b) ? 2 : 3;
}

int main(void)
{
  long z = 0;
  for (int i = 1; i < (int)1e7; i++) if (f(i, 10) == f(i, 3)) z += i;
  printf("%ld\n", z);
}

