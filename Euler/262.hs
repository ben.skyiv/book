import Text.Printf ( printf )

e = 1e-6; s = 1e-3; m = 1600; z = h 0 $ max' 0 e
h x y = (5e3-5e-3*(x*x+y*y+x*y)+12.5*(x+y))*exp(-abs(1e-6*(x*x+y*y)-15e-4*(x+y)+0.7))
bs f a b | b-a < e = b | f c = bs f a c | True = bs f c b where c = (a+b)/2
dist x1 y1 x2 y2 = sqrt $ (x2-x1)^2 + (y2-y1)^2
max' x e = bs (\y -> h x y >= h x (y+e)) 0 m
xMax = bs (\x -> h x (max' x e) <= z) 0 m; xMid = max' 0 e
yU x = bs (\y -> h x (y+e) >= z) 0 (yM+e); yM = max' xMax e
yL x = m - bs (\y -> h x (m-y-e) >= z) 0 (m-yM-e)
f g h z x y = bs (g . (\t -> (h (t+s) - h (t-s)) / (s+s) - (y - h t) / (x-t))) x z
path f x = sum $ map (\z -> dist z (f z) (z+s) $ f $ z+s) [x, x+s .. xMax-s]
main = let a = 200; b = 1400 in printf "%.3f\n" ((\x1 y1 x2 y2 -> let
  x3 = f (>0) yU xMid x1 y1; x4 = f (<0) yL xMax x2 y2 in dist x1 y1 x3 (yU x3) +
  path yU x3 + path yL x4 + dist x4 (yL x4) x2 y2) a a b b :: Double)

