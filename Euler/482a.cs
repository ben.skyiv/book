using System;
using System.Linq;
using System.Collections.Generic;

sealed class E482
{
  int Gcd(int a, int b) { return (b == 0) ? a : Gcd(b, a % b); }
  long Lcm(int a, int b) { return a / Gcd(a, b) * (long)b; }
  long Lcm(int a, int b, int c) { return Lcm((int)Lcm(a, b), c); }
  
  int[][] Step1(int p)
  {
    var set = new HashSet<int[]>();
    for (int n9 = p/8, n = 1; n < n9; n++)
      for (int n2 = n*n, m9 = (int)Math.Sqrt(n9-n2), m = n + 1; m < m9; m++)
      {
        if ((m - n) % 2 == 0 || Gcd(m, n) > 1) continue;
        int m2 = m*m;
        var a = new int[3];
        a[0] = 2*m*n; a[1] = m2 - n2; a[2] = m2 + n2;
        set.Add(a);
      }
    return set.ToArray();
  }
  
  long[] Compute(int v, int[] ti, int[] tj, int[] tk)
  {
    int i = (v&4)>>2, j = (v&2)>>1, k = v&1;
    var r = Lcm(ti[i], tj[j], tk[k]);
    var a = new long[2];
    a[0] = (r/ti[i]*ti[1-i] + r/tj[j]*tj[1-j] + r/tk[k]*tk[1-k]) * 2;
    a[1] = r/ti[i]*ti[2] + r/tj[j]*tj[2] + r/tk[k]*tk[2] + a[0];
//Console.WriteLine("({0}:[{1},{2},{3}],[{4},{5},{6}],[{7},{8},{9}])(r:{10},p:{11},{12})",
//v,ti[0],ti[1],ti[2],tj[0],tj[1],tj[2],tk[0],tk[1],tk[2],r,a[0],a[1]);
    return a;
  }
  
  long Compute(int p)
  {
    long sum = 0;
    var array = Step1(p);
    double pi2 = Math.PI / 2, eps = 1e-9;
    var a = new double[3,2];
    for (int len = array.Length, i = 0; i < len; i++)
    {
      var ti = array[i];
      a[0,1] = pi2 - (a[0,0] = Math.Atan2(ti[0], ti[1]));
      for (var j = i; j < len; j++)
      {
        var tj = array[j];
        a[1,1] = pi2 - (a[1,0] = Math.Atan2(tj[0], tj[1]));
        for (var k = j; k < len; k++)
        {
          var tk = array[k];
          a[2,1] = pi2 - (a[2,0] = Math.Atan2(tk[0], tk[1]));
          for (var v = 0; v < 8; v++)
            if (Math.Abs(a[0,(v&4)>>2]+a[1,(v&2)>>1]+a[2,v&1]-pi2) < eps)
            {
              var z = Compute(v, ti, tj, tk);
              if (z[0] <= 0) throw new Exception("z[0] <= 0");
              if (z[0] > p) continue;
              for (long zi = 0; z[0]*zi <= p; zi++) sum += z[1]*zi;
            }
        }
      }
    }
    return sum;
  }
  
  static void Main()
  {
    Console.WriteLine(new E482().Compute(10000));
  }
}

