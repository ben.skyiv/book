#include <iostream>
#include <bitset>

int main(void)
{
  const int n = 2000000000;
  static std::bitset<n+1> bs[4];
  long z = 0, e, f[] = {0,1,1,4};
  for (int a = 1, c = 1; (c = a*a) <= n; a++)
    for (int b = 1, d = 1; (d = b*b) <= n && (e = c+d) <= n; b++)
      for (int i = 0; i < 4 && (e += f[i]*d) <= n; i++)
        bs[i].set(e);
  for (int i = 0; i <= n; i++)
    if (bs[0][i] && bs[1][i] && bs[2][i] && bs[3][i])
      z++;
  std::cout << z << std::endl;
  return 0;
}

