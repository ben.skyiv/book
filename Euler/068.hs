import Data.List ( group,permutations )

main = putStrLn $ maximum $ filter ((==16) . length) $ map (\xs -> let
  zs = drop 5 xs in concat $ take 5 $ zipWith3 (\a b c -> concatMap show
  [a,b,c]) xs zs $ tail $ cycle zs) $ filter (\xs -> let (ys,zs) = splitAt 5 xs
  in elem 10 ys && head ys == minimum ys && (null . tail . group . take 5 $
  zipWith3 (\a b c -> a+b+c) (tail $ cycle zs) zs xs)) $ permutations [1..10]

