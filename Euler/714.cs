using System;
using System.Numerics;
using System.Collections.Generic;

static class E714
{
  static readonly int max = 50000, maxLen = 18;
  static long[] numbers = new long[max + 1];

  static IEnumerable<int> GetDivisors(long n)
  {
    for (int m = (int)Math.Min(max, n), i = 1; i <= m; i++)
      if (n % i == 0) yield return i;
  }

  static bool IsDuodigits(BigInteger n)
  { // assert: n >= 10
    int a = (int)(n % 10), b = a;
    for (n /= 10; n != 0; ) {
      BigInteger r;
      n = BigInteger.DivRem(n, 10, out r);
      var i = (int)r;
      if (i == a) continue;
      if (b == a) b = i;
      else if (b != i) return false;
    }
    return true;
  }

  static BigInteger D(int n)
  {
    Console.WriteLine("{0}:", n);
    BigInteger z = n;
    while (!IsDuodigits(z)) z += n;
    Console.WriteLine("{0:N0}", z);
    return z;
  }

  static void Main()
  {
    Console.WriteLine("D({0}) (10^{1})", max, maxLen);
    var cnt = 0;
    foreach (var n in Duodigits.Get(maxLen)) {
      cnt++;
      foreach (var i in GetDivisors(n))
        if (numbers[i] == 0) numbers[i] = n;
    }
    Console.WriteLine("Duodigits:{0:N0}", cnt);
    BigInteger z = 0;
    for (var i = 1; i <= max; i++) z += (numbers[i] == 0) ? D(i) : numbers[i];
    Console.WriteLine("{0:N0}", z);
  }

  sealed class Duodigits
  {
    static readonly int M = 9;
    long v;
    int a, b, d;

    Duodigits(long v, int a, int b, int d)
    {
      this.v = v;
      this.a = Math.Min(a, b);
      this.b = Math.Max(a, b);
      this.d = d;
    }

    IEnumerable<Duodigits> AppendSuffix()
    {
      if (a != b) {
        yield return new Duodigits(10 * v + a, a, b, d+1);
        yield return new Duodigits(10 * v + b, a, b, d+1);
      }
      else for (var i = 0; i <= M; i++)
        yield return new Duodigits(10 * v + i, a, i, d+1);
    }

    IEnumerable<Duodigits> GetList(int len)
    {
      if (d >= len) yield return this;
      else foreach (var n in AppendSuffix())
        foreach (var m in n.GetList(len))
          yield return m;
    }

    public static IEnumerable<long> Get(int maxLen)
    {
      for (var i = 1; i < 100; i++) yield return i;
      for (var len = 3; len <= maxLen; len++)
        for (var a = 1; a <= M; a++)
          for (var b = 0; b <= M; b++)
            foreach (var n in new Duodigits(10*a+b, a, b, 2).GetList(len))
              yield return n.v;
    }
  }
}
