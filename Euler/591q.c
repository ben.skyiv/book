#include <stdio.h>
#include <quadmath.h>

static __float128 s, c;

__float128 f(long a)
{
  __float128 b = a / s;
  return fminq(fabsq(lroundq(b - c) * s - a + M_PIq),
               fabsq(lroundq(b + c) * s - a - M_PIq));
}

int main(void)
{
  int n = 99; s = sqrtq(n); c = M_PIq / s;
  __float128 x, x0 = 1; long i0, i9 = (long)1e8;
  for (long i = 1; i <= i9; i++)
    if ((x = f(i)) < x0)
      printf("%2d: %12ld: %e\n", n, i0 = i, (double)(x0 = x));
  printf("F(%d, %ld) = %ld\n", n, i9, i0);
}

