#include <iostream>
#include <map>
#include <set>

long long g(int N, int x);
std::map<int, int> factorise(int N);

// Let f(k) be the number of integer-sided triangles whose area is exactly
// k times their perimeter

long long f(int k){
  // By the triangle inequality, for all triangles, there are real numbers 
  // x, y and z such that the sides of the triangle are x+y, y+z, z+x.
  // (In fact, these are given by x=(b+c-a)/2, y=(c+a-b)/2 and z=(a+b-c)/2)
  // The perimeter is 2(x+y+z), and the area is sqrt(xyz(x+y+z)).
  // Thus area = k * perimeter, iff sqrt(xyz(x+y+z))=2k(x+y+z).
  // Squaring gives xyz(x+y+z)=4k^2(x+y+z)^2, or xyz=4k^2(x+y+z)
  // Note that if a+b+c is odd then x, y, z are all half-integers, which would
  // make the left-hand-side be the eighth of an odd integer, and the right hand
  // side an integer. Thus, a+b+c must be even, and so x,y,z are all integers.

  // Furthermore, the reverse argument works, so any solution to 
  // xyz = 4n^2(x+y+z) gives such a triangle.

  // Let us write N for 4k^2
  int N = 4*k*k;

  // xyz = N(x+y+z) iff
  // (xy-N)(xz-N) = x(xyz-Ny-Nz)+N^2 = Nx^2+N^2

  // Assuming x <= y <= z, we can stop once
  // (x^2-N)(x^2-N) > Nx^2+N^2
  // x^4-2Nx^2+N^2 > Nx^2+N^2
  // x^4 > 3Nx^2
  // x^2 > 3N
  
  // Thus x < sqrt(12)k, so x fits into an int.

  int x = 1;
  long long ans = 0;
  while (x * x < 3 * N){
    ans += g(N, x);
    x += 1;
  }
  return ans;
}

long long record_number_of_factors = 0;

long long g(int N, int x){
  // We want to find twice the sum of (x+y+z) for all triples where
  // (xy-N)(xz-N) = Nx^2+N^2 where x <= y <= z, and x and N are given.
  
  // At this point it will be useful to have a factorization of the right hand
  // side. Note the largest value for N is 4*10^6, and the largest value for x^2
  // is less than 12*10^6, giving RHS = 48*10^12+16*10^12 = 64*10^12.

  // However, we already know that the right hand side is N * (x^2 + N), which
  // are at most 4 * 10^6 and 16*10^6. Thus we only need to be able to factorize
  // numbers up to 16,000,000. We will precalculate the factorizations of these
  // numbers.
  std::map<int, int> fac1 = factorise(N);
  std::map<int, int> fac2 = factorise(x*x+N);

  // Multiply fac2 into fac1
  for (std::map<int,int>::iterator fac = fac2.begin(); fac != fac2.end(); ++fac)
    fac1[fac->first] += fac->second;

  // I checked to see how many factors the number could have with:

  // long long number_of_factors = 1;
  // for (std::map<int, int>::iterator fac = fac1.begin(); fac != fac1.end(); ++fac)
  //   number_of_factors *= (1 + fac->second);
  // if (number_of_factors > record_number_of_factors){
  //   record_number_of_factors = number_of_factors;
  //   std::cout << "N = " << N << ", x = " << x; 
  //   std::cout << ", number of factors of N(x^2+N) is " ;
  //   std::cout << number_of_factors << std::endl;
  // }

  // The largest number of factors if 9720. 

  long long factors[10000];
  int number_of_factors = 1;
  factors[0] = 1;
  for (std::map<int, int>::iterator fac = fac1.begin(); fac != fac1.end(); ++fac){
    for (int i = 0; i < (number_of_factors * fac->second); ++i)
      factors[i + number_of_factors] = factors[i] * fac->first;
    number_of_factors *= (1 + fac->second);
  }
  
  // Now N(x^2+N) = factors[i] * factors[(number_of_factors-1)-i] for all 
  // 0 <= i <= number_of_factors - 1

  int lwr_bound = x * x - N;

  long long* factor = factors;
  long long* cofactor = factors + (number_of_factors - 1);
  long long* end_of_factors = factors + number_of_factors;
  long long ans = 0;
  while (factor != end_of_factors){
    if (*factor >= lwr_bound)
      if (*factor <= *cofactor)
	if ((*factor + N) % x == 0){
	  if ((*cofactor + N) % x == 0){
	    int dummy = 14;
	    long long y = (*factor + N) / x;
	    long long z = (*cofactor + N) / x;
	    ans += 2 * (x + y + z);
	  }
	}
    ++factor;
    --cofactor;
  }
  return ans;
}

int* prime_factors;
int* cofactors;
const int max_N = 16000000;

void calculate_factorizations(){
  prime_factors = new int[max_N+1];
  cofactors = new int[max_N+1];
  for (int t = 2; t <= max_N; ++t){
    prime_factors[t] = t;
    cofactors[t] = 1;
  }
  for (int t = 2; t * t <= max_N; ++t)
    if (cofactors[t] == 1){
      for (int j = 1; j * t <= max_N; ++j){
	prime_factors[j*t] = t;
	cofactors[j*t] = j;
	
      } 
    }  
}

std::map<int, int> factorise(int N){
  std::map<int,int> ans;
  while (N != 1){
    ans[prime_factors[N]]++;
    N = cofactors[N];
  }
  return ans;
}


int main(){
  calculate_factorizations();
  long long ans = 0;
  for (int n = 1; n <= 1000; ++n){
    ans += f(n);
  }
  std::cout << ans << std::endl;
} // 0m13.506s

