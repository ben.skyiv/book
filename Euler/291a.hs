import Math.NumberTheory.Primes.Testing ( millerRabinV )

main = print $ length $ filter (millerRabinV 2) $ takeWhile (<(5*10^15)) [(i+i)*(i+1)+1 | i <- [1..]]

