f n s = g n 1 s where
  g m k [] = n
  g m k (x:xs) | mod h 3 == 0 = g (div h 3) (k*3) xs
               | otherwise    = f (n+k) s
    where h | x == 'D' = m | x == 'U' = 4*m+2 | x == 'd' = 2*m-1

main = print $ f (10^15) "UDDDUdddDDUDDddDdDddDDUDDdUUDd"

