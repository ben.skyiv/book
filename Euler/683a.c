#include <stdio.h>
#include <stdlib.h>

const int n = 500;
static double a[n / 2][n / 2];
static double s[n / 2], v1[n / 2], v2[n / 2];

int dist(int a, int b, int n)
{
  int z = abs(a - b);
  return (z > n / 2) ? (n - z) : z;
}

void make1(int n)
{
  for (int i = 0; i < n / 2; i++) s[i] = 0;
  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      s[dist(i, j, n) - 1]++;
  for (int i = 0; i < n / 2; i++) s[i] /= n * n;
}

void make2(int n)
{
  for (int k = 0; k < n / 2; k++) {
    v1[k] = 0; for (int i = 0; i < n / 2; i++) a[k][i] = 0;
    int d;
#define Q(i,j) d = dist(i,j,n); if (d == 0) v1[k]++; else a[k][d-1]++
    Q(  0, k+1); Q(  0, k); Q(  0, k+2);
    Q(  1, k+1); Q(  1, k); Q(  1, k+2);
    Q(n-1, k+1); Q(n-1, k); Q(n-1, k+2);
    v1[k] /= 9; for (int i = 0; i < n / 2; i++) a[k][i] /= 9;
  }
}

double f(int n)
{
  make1(n); make2(n);
  double zz = 1, z1 = 1.0 / n, z2 = 0;
  double *t = v1, *u = v2; int i, m = n / 2;
  for (i = 1; zz > 1e-8; i++) {
    double z0 = 0;
    for (int j = 0; j < m; j++) z0 += s[j] * t[j];
    z1 += z0; z2 += zz = z0 * i * i;
    for (int j = 0; j < m; j++) {
      u[j] = 0;
      for (int k = 0; k < m; k++) u[j] += a[j][k] * t[k];
    }
    double *w = t; t = u, u = w;
  }
  printf("%3d: %20.10lf (%.18lf) (%d)\n", n, z2, z1, i);
  fflush(stdout); return z2;
}

int main(void)
{
  double z = 0; printf("n = %d\n", n); fflush(stdout);
  for (int i = 2; i <= n; i++) z += f(i);
  printf("G(%d) = %.12lg\n", n, z);
}
