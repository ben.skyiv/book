def f(n): return [int(d, 2) for d in bin(n)[2:].split('00') if d != '']
for n in range(1,19): print(f(10**n))

