import Math.Combinat.Numbers ( binomial )

main = print $ length [() | x <- [1..100], y <- [1..x], binomial x y > 10^6]

