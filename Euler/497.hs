main = print $ head $ (!!(10000)) $ iterate (\[z,a,b,c,k,v,n] -> let
  d = a*3; e = b*6; f = c*9; u = k*10; w = v*2 + if even n then 1 else 0
  in map (flip mod $ 10^9) [z + (e-d)*(2*u-d-e) + 4*w*(f-d)*(u-1) +
  if odd n then (f-d)*(d+f-2) else (f-d)*(d+f-2*u), d, e, f, u, w, n+1])
  [0,1,1,1,1,0,1]

