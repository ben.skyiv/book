using System;

static class E648
{
  static readonly Random rand = new Random();
  
  static int V(double p)
  {
    int z = 0;
    for (int sq = 1, q = 1, s = 0; ; ) {
      var b = rand.NextDouble() < p;
      s += b ? 1 : 2;
      if (s == sq) break;
      if (b || s - 1 != sq) continue;
      z++; sq += q += 2;
    }
    return z;
  }

  static void Main()
  {
    int n = (int)1e9;
    for (var p = 0.0; p <= 1.000001; p += 0.01) {
      int max = 0; int z = 0;
      for (var i = 0; i < n; i++) {
        var z0 = V(p);
        if (max < z0) max = z0;
        z += z0;
      }
      Console.WriteLine("{0:F2} {1:F9} ({2:D2})", p, (double)z / n, max);
    }
  }
}

