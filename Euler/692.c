#include <stdio.h>

int main(void)
{
  int n = 80;
  long u = 1, v = 1, a = 1, b = 1;
  for (int i = 3; i <= n; i++) {
    long t = u; u = v, v += t;
    long c = u + a + b; a = b, b = c;
  }
  printf("F(%d) = G(%ld) = %ld\n", n, v, b);
}
