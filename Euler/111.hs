import Data.List (delete)
import Control.Monad (replicateM)
import Math.NumberTheory.Primes.Testing (isPrime)

f n x (y:ys) = concat [map ((replicate i x ++) . (y:)) $ f (n-i) x ys | i <- [0..n]]
f n x []     = [replicate n x]

main = let n = 10 in print $ sum $ concatMap (\d -> head $ dropWhile null
  [filter isPrime $ map read $ filter ((/='0') . head) $ concatMap (f (n-i) d)
  $ replicateM i $ delete d "0123456789" | i <- [1..9]]) "0123456789"

