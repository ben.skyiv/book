import Data.Array ( (!),listArray,range )
import Data.Ratio ( (%) )
n = 20
f1 n = product [2..n]
f2 n r = if r > n-r then f2 n (n-r) else div (product [n-r+1..n]) $ a1!r
a1 = listArray (0,n) $ map f1 [0..n]
a2 = let b = ((0,0),(n,n)) in listArray b $ map (uncurry f2) $ range b
r z = let m = length z in
  foldl (\s x -> 1 % fromIntegral (x+1) * s) (a1 ! (sum z) % product z)
  [length $ takeWhile (\j -> z!!i == z!!j) [i+1..m-1] | i <- [0..m-1]]
t 2 n k = [[i,j] | i <- [k .. div n 2], j <- [i..n-i]]
t l n k = concat [map (i:) $ t (l-1) (n-i) i | i <- [k .. div n l]]
g n = let m = a1 ! n in sum $ concat [[fromRational $ 
  (sum [a2 ! (i, k-1) * a1 ! (k-1) | i <- [k-1..n-1]]) * k * k % m | k <- [2..n]],
  map (\z -> fromRational $ (foldl1 lcm z)^2 * a2 ! (n, sum z) % m * r z) $
  concat $ takeWhile (not . null) [t l n 2 | l <- [2..]], [fromRational $ 1%m]]
main = print $ (n, g n)

