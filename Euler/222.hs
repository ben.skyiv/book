import Data.Array ( (!),listArray )

main = let
  r = 50; n = 21
  a = listArray (0,n-1) $ let z = r-n+1 in [r-1,r-3..z] ++ [z,z+2..]
  in print $ round $ 1000 * sum [fromIntegral $ a!0 + a!(n-1), (2*)
  $ sum [sqrt $ fromIntegral $ r*(a!i+a!(i+1)-r) | i <- [0..n-2]]]

