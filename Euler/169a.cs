using System;
using System.Collections.Generic;

sealed class E169
{
  sealed class Eq : EqualityComparer<HashSet<int>>
  {
    public override bool Equals(HashSet<int> a, HashSet<int> b)
    {
      return a.SetEquals(b);
    }
    
    public override int GetHashCode(HashSet<int> a)
    {
      var n = 0;
      foreach (var i in a) n ^= i;
      return n;
    }
  }

  HashSet<HashSet<int>> mask = new HashSet<HashSet<int>>(new Eq());

  int Compute(HashSet<int> set, int k)
  {
    if (--k < 1) return 0;
    if (set.Contains(k) || set.Contains(-k)) return 0;
    var set2 = new HashSet<int>(set);
    set2.Remove(k + 1);
    set2.Add(k);
    set2.Add(-k);
    if (mask.Contains(set2)) return 0;
    mask.Add(set2);
    return 1 + Compute(set2);
  }

  int Compute(HashSet<int> set)
  {
    var count = 0;
    foreach (var i in set) count += Compute(set, i);
    return count;
  }
  
  int Compute(int n)
  {
    var set = new HashSet<int>();
    for (var i = 1; n > 0; n /= 2, i++)
      if (n % 2 != 0) set.Add(i);
    return 1 + Compute(set);
  }

  static void Main(string[] args)
  {
    var n = (args.Length > 0) ? int.Parse(args[0]) : 10;
    Console.WriteLine("f({0}) = {1}", n, new E169().Compute(n));
    /*var e169 = new E169();
    var max = 64;
    for (var i = 1; i <= max; i++) Console.Write("{0,3}", i);
    Console.WriteLine();
    for (var i = 1; i <= max; i++) Console.Write("{0,3}", e169.Compute(i));
    Console.WriteLine();*/
  }
}
