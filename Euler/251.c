#include <stdio.h>
#include <math.h>

int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }

void extGcd(int a, int b, int* px, int* py)
{
  if (b == 0) *px = 1, *py = 0; 
  else extGcd(b, a % b, py, px), *py -= a / b * *px;
}

int main(void)
{
  long max = 110000000, count = 0;
  for (long n = 1, n2 = sqrt(max / 5); n <= n2; n++)
    for (long c, d = 1, d2 = sqrt(max - 5 * n * n - n); d < d2; d += 2) {
      if (gcd(d, n) != 1) continue;
      int p, q; extGcd(d, 8 * n, &q, &p);
      p = 8 * n - 3 * q * q % (8 * n);
      c = max + 1 - (3 * n + d) * (d * d * p + 3) / (8 * n) - n * n * p;
      if (c >= 0) count += c / (8 * n * n * n + (3 * n + d) * d * d) + 1;
    }
  printf("%ld\n", count);
  return 0;
}

