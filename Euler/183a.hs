import Data.Set as Set ( (\\),fromList,null )
import Math.NumberTheory.Primes.Factorisation ( factorise )

main = let m = 5; n = 10^4 in print $ sum [if (\a b -> Set.null $
  fromList (map fst (factorise (div b $ gcd a b))) \\ fromList [2,5])
  z (round $ fromIntegral z / exp 1) then -z else z | z <- [m..n]]

