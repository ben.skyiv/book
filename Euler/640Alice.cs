using System;

static class E640Alice
{
  static readonly int cards = 4;
  static readonly int i9 = cards * cards / 4;
  static readonly short b0 = (short)((1 << cards) - 1);
  
  static void Main()
  {
    short b = b0;
    int max = 0; long n = 0, z = 0;
    var rand = new Random();
    for (var v = 0; n < (long)1e7; ) {
      if (b == 0) { if (max < v) max = v; n++; z += v; v = 0; b = b0; }
      v++;
      switch (rand.Next(i9)) {
        case 1: case 2:
          if      ((b & 4) != 0) b &= ~4;
          else if ((b & 1) != 0) b &= ~1;
          else if ((b & 2) != 0) b &= ~2;
          else                   b |=  2;
          break;
        case 0: 
          if      ((b & 1) != 0) b &= ~1;
          else if ((b & 2) != 0) b &= ~2;
          else                   b |=  2;
          break;
        default: // case 3:
          if      ((b & 8) != 0) b &= ~8;
          else if ((b & 2) != 0) b &= ~2;
          else                   b |=  2;
          break;
      }
    }
    Console.WriteLine("1e{0} {1} {2}",
      Math.Log10(n), max, (double)z / n);
  }
}
// 1e9  72 5.673673948
// 1e10 82 5.6736408566
//         5.673651

