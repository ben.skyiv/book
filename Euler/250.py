n = 250250; z = 250; m = 10**16
a = z*[0]; b = [1] + (z-1)*[0]
for i in range(1, n+1): a[pow(i,i,z)] += 1
for i in range(z):
  for _ in range(a[i]):
    b = [(b[k] + b[(k-i)%z]) % m for k in range(z)]
print(b[0] - 1)

