#include <stdio.h>
#include <math.h>

const int n = 500000000;
static int a[n / 2 + 1];

int totient(int n)
{
  int v = n - 1, p = a[n / 2], q;
  if (p != 0) {
    v = (p - 1) * a[(q = n / p) / 2];
    if (q % p == 0) v = v / (p - 1) * p;
  }
  return a[n / 2] = v;
}

int main(void)
{
  for (int i = 1; i <= (int)sqrt(n) / 2; i++)
    if (a[i] == 0)
      for (int k = 2*i+1, j = 2*i*(i+1); j <= n / 2; j += k)
        a[j] = k;
  long v = 1;
  for (int i = 3; i <= n; i += 2) v += totient(i);
  printf("%ld\n", v); // 11.128s
  return 0;
}

