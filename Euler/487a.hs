import Math.NumberTheory.Powers ( powerMod )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )

f k n p = if mod k (p-1) == 0 then n - div n p else let m = mod n p
  in if p-1-m >= m then sum [powerMod i k p | i <- [1..m]]
  else p + let z = f k (p-1-m) p in if odd k then z else -z
s k n p = mod ((n+1) * f k n p - f (k+1) n p) p

main = let k = 10^4; n = 10^12; m = 2*10^9; b = m + 2000
  in print $ sum $ map (s k n) $ takeWhile (<b) $ sieveFrom m

