using System;

sealed class E512
{
  int[] totients, pfs;

  void PrimeFactors(int max)
  {
    int limit = (int)Math.Sqrt(max);
    pfs = new int[max + 1];
    for (int i = 2; i <= limit; i++)
      if (pfs[i] == 0)
        for (int j = i * i; j <= max; j += i)
          pfs[j] = i;
  }
  
  int Totient(int n)
  {
    int v = n - 1, p = pfs[n], q;
    if (p != 0)
    {
      v = (p - 1) * totients[q = n / p];
      if (q % p == 0) v = v / (p - 1) * p;
    }
    return totients[n] = v;
  }
  
  long Compute(int n)
  {
    Console.Error.WriteLine("{0:dd HH:MM:ss} {1,11:N0}", DateTime.Now, n);
    totients = new int[n + 1];
    PrimeFactors(n);
    long v = 1;
    for (var i = 3; i <= n; i += 2)
    {
      v += Totient(i);
      if ((i + 1) % 10000000 != 0) continue;
      Console.Error.WriteLine("{0:dd HH:MM:ss} {1,11:N0} ", DateTime.Now, i+1);
    }
    return v;
  }

  static void Main()
  {
    Console.WriteLine(new E512().Compute(500000000));
  }
}

