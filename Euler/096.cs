using System;
using System.Collections.Generic;

sealed class E096
{
  int Compute()
  {
    var sum = 0;
    var ss = new string[9];
    for (var i = 0; i < 50; i++)
    {
      Console.In.ReadLine();
      for (var j = 0; j < 9; j++) ss[j] = Console.In.ReadLine();
      sum += new Sudoku(ss).Out();
    }
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E096().Compute());
  }
}

sealed class Sudoku
{
  byte[,] input, output;
  int steps = 0;

  public Sudoku(string[] ss)
  {
    input = new byte[9, 9];
    for (int y = 0; y < 9; )
    {
      var s = ss[y];
      int x = 0;
      for (int i = 0; i < s.Length; i++)
        if (Char.IsDigit(s, i) && x < 9) input[x++, y] = (byte)(s[i] - '0');
      if (x != 0) y++;
    }
  }

  public int Out()
  {
    Compute(input);
    return output[0,0]*100 + output[1,0]*10 + output[2,0];
  }

  bool Compute(byte[,] input)
  {
    List<byte[,]> list = StepIt(input);
    if (list == null) return true;
    foreach (byte[,] temp in list) if (Compute(temp)) return true;
    return false;
  }

  // return null for finish
  List<byte[,]> StepIt(byte[,] input)
  {
    if (steps++ > 100000) throw new Exception("太复杂了");
    output = input;
    int theX = -1, theY = -1;
    byte[] theDigits = null;
    for (int y = 0; y < input.GetLength(1); y++)
    {
      for (int x = 0; x < input.GetLength(0); x++)
      {
        if (input[x, y] != 0) continue;
        byte[] digits = GetDigits(input, x, y);
        if (digits.Length == 0) return new List<byte[,]>();
        if (theDigits != null && theDigits.Length <= digits.Length) continue;
        theX = x;
        theY = y;
        theDigits = digits;
      }
    }
    if (theDigits == null) return null;
    List<byte[,]> result = new List<byte[,]>();
    foreach (byte digit in theDigits)
    {
      byte[,] temp = (byte[,])input.Clone();
      temp[theX, theY] = digit;
      result.Add(temp);
    }
    return result;
  }

  byte[] GetDigits(byte[,] input, int x, int y)
  {
    bool[] mask = new bool[10];
    for (int i = 0; i < 9; i++)
    {
      mask[input[x, i]] = true;
      mask[input[i, y]] = true;
    }
    for (int i = x / 3 * 3; i < x / 3 * 3 + 3; i++)
      for (int j = y / 3 * 3; j < y / 3 * 3 + 3; j++)
        mask[input[i, j]] = true;
    List<byte> list = new List<byte>();
    for (int i = 1; i < mask.Length; i++) if (!mask[i]) list.Add((byte)i);
    return list.ToArray();
  }
}

