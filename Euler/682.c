#include <stdio.h>

void sort(int *a, int *b, int *c)
{
  int t = 0;
  if (*a > *b) {
    if (*b > *c) t = *a, *a = *c, *c = t;
    else if (*a > *c) t = *a, *a = *b, *b = *c, *c = t;
    else t = *a, *a = *b, *b = t;
  }
  else {
    if (*a > *c) t = *a, *a = *c, *c = *b, *b = t;
    else if (*b > *c) t = *b, *b = *c, *c = t;
  }
}

long f(int n, int a, int b, int c, int d)
{
  int p = a+5*b+6*d, q = a+5*c+d, r = (n-2*p-3*q)/5;
  //if (r < 0) return 0;
//printf("%2d %2d %2d: ",a,b,c);
//printf("%2d %2d %2d: %2d %d: ",p,q,r,(p+q+r)/2,2*p+3*q+5*r);
  sort(&p, &q, &r);
  long z0 = (p + 1L) * (q + 1L);
  int s = (p + q - r) / 2;
  if (s > 0) z0 -= s * (s + 1L);
//printf("%2ld\n",z0);
  return z0;
}

int main(void)
{
  int m = 7 + (int)1e9, n = (int)1e7;
  long z = 0; int nn = n / 5;
  for (int a = 0; a <= 8; a += 2) {              // 10a <=> 2(b+c):  5 -> 2,3
    fprintf(stderr, "%d ", a);
    for (int b9 = (nn-a)/2, b = 0; b <= b9; b += 2)     // 5,5   -> 2,2,2,2,2
      for (int c9 = (nn-a-b-b)/3, c = 0; c <= c9; c++) {// 5,5,5 -> 3,3,3,3,3
        long z0 = f(n, a, b, c, 0);
        z = (z + z0) % m;
      }
  }
  for (int a = 0; a <= 2; a += 2) {
    fprintf(stderr, "%d ", a);
    for (int b9 = (nn-3-a)/2, b = 0; b <= b9; b += 2) {
      long z0 = f(n, a, b, 0, 1);
      z = (z + z0) % m;
    }
  }
  printf("f(%d) = %ld\n", n, z);
}
