// gcc -O4 -march=native -fopenmp 581.c && time ./a.out
#include <stdio.h>

#define n  20000002
#define n2 10000001
const long m = (long)2e9; // n > sqrt(m+n), n == 2*n2

void factor(long i0, int len, char a[], const char c[])
{ // a[i]: is (i0+i) NOT p-smooth?
  static long t[n], p0 = 47;
  for (int i = 0; i < len; i++) t[i] = 1;
  for (long p = 2; p * p < i0 + len; p++)
    if (!c[p])
      for (long q = p; q < i0 + len; q *= p)
        for (int i = q-1-(i0-1)%q; i < len; t[i] *= p, i += q)
          if (q == p) a[i] = p0 < p;
  for (int i = 0; i < len; i++)
    if (i + i0 != t[i]) a[i] = p0 < (i + i0) / t[i];
}

long compute(const char a[], const char b[], long i0, int j)
{
  long z = 0; int j2 = j + j;
  if (a[j2]) return 0;
  if (!b[j])   z += i0 + j2 - 1;
  if (!b[j+1]) z += i0 + j2;
  return z;
}

int main(void)
{
  printf("%ld %d %d\n", m, n, n2);
  static char a[n], b[n], c[n];
  for (int i = 2; i * i < n; i++)
    if (!c[i]) for (int j = i * i; j < n; j += i) c[j] = 1;
  long z = 1;
  for (long i0 = 3, i = 1; i <= m; i += n2, i0 = i + i + 1) {
    factor(i, n2, b, c); factor(i0, n, a, c);
    // #pragma omp parallel for reduction(+:z)
    for (int j = 0; j < n2; j++) z += compute(a, b, i0, j);
  }
  printf("%ld\n", z);
}
// 1e7:  1,253,782,345  3s
// 1e8:  6,227,351,613 30s
// 1e9: 22,495,633,669

