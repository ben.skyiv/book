using System;
using System.Collections.Generic;
using System.Text;

sealed class E558
{
  sealed class Rbase
  {
    long value;
    SortedSet<long> bits;

    public int Count { get { return bits.Count; } }
    public long Value { get { return value; } }
    public void Increase() { value++; To101(0); Normalize(); }
    
    public Rbase()
    {
      value = 1;
      bits = new SortedSet<long>();
      bits.Add(0);
    }
    
    void Verify()
    {
      long n0 = long.MinValue / 2;
      foreach (var n in bits) {
        if (n - n0 < 3) throw new Exception(string.Format(
          "Verify: {0}: ({1},{2})", value, n, n0));
        n0 = n;
      }
    }

    void Normalize()
    {
      bool b;
      do {
        From111And101();
        b = To141();
      } while (b);
      Verify();
    }

    void From111And101()
    {
      bool b;
      do {
        while (From111()) ;
        b = From101();
      } while (b);
    }

    void To101(long n)
    {
      if (!bits.Contains(n)) { bits.Add(n); return; }
      if (bits.Contains(n-1)) throw new Exception("Increase");
      bits.Add(n-1);
      if (bits.Contains(n-3)) To101(n-3);
      else bits.Add(n-3);
    }
    
    bool From101()
    {
      foreach (var n in bits) {
        if (bits.Contains(n+1) || bits.Contains(n-1)
          || !bits.Contains(n-2)) continue;
        bits.Add(n+1); bits.Remove(n); bits.Remove(n-2);
        return true;
      }
      return false;
    }

    bool From111()
    {
      foreach (var n in bits) {
        if (bits.Contains(n+2) || !bits.Contains(n-1)
          || !bits.Contains(n-2)) continue;
        bits.Add(n+2); bits.Remove(n);
        bits.Remove(n-1); bits.Remove(n-2);
        return true;
      }
      return false;
    }
    
    bool To141()
    {
      var ex = false;
      foreach (var n in bits) {
        if (!bits.Contains(n+1)) continue;
        ex = Ex141(bits.Contains(n+2) ? (n+1) : n);
        break;
      }
      return ex;
    }
    
    bool Ex141(long n)
    {
      if (bits.Contains(n+2)) throw new Exception("Ex141:" + n);
      bits.Add(n+2);
      if (bits.Contains(n-3)) To101(n-3);
      bits.Add(n-3); bits.Remove(n); bits.Remove(n+1);
      return true;
    }

    public override string ToString()
    {
      var sb = new StringBuilder();
      sb.Append(value + ":");
      var dot = false;
      foreach (var bit in bits.Reverse()) {
        if (bit < 0 && !dot) { dot = true; sb.Append("."); }
        sb.Append(" " + Math.Abs(bit));
      }
      return sb.ToString();
    }
  }
  
  bool IsSquare(long n)
  {
    long r = (long)Math.Sqrt(n);
    return r*r == n;
  }
  
  long Compute(long n)
  {
    long z = 0;
    var v = new Rbase();
    for (long n2 = n*n, i = 1; i <= n2; i++) {
      if (i != v.Value) throw new Exception("v.Value");
      if (IsSquare(i)) {
        Console.WriteLine("{0,2} {1}", v.Count, v);
        z += v.Count;
      }
      v.Increase();
    }
    return z;
  }

  static void Main()
  {
    try { Console.WriteLine(new E558().Compute(100)); }
    catch (Exception ex) { Console.WriteLine("Error: " + ex.Message); }
  }
}

