using System;

sealed class E112
{
  bool IsBouncy(int n)
  {
    for (int d, d0 = -1, flag = 0; n > 0; d0 = d, n /= 10)
    { // flag: 0:d0==d, 1:d0>d, -1:d0<d
      d = n % 10;
      if (d0 < 0) continue;
      if (d0 > d)
  	    if (flag < 0) return true;
        else flag = 1;
      if (d0 < d)
	      if (flag > 0) return true;
        else flag = -1;
    }
    return false;
  }

  int Compute()
  {
    for (int i = 1, bouncies = 0; ; i++)
    {
      if (IsBouncy(i)) bouncies++;
      if (bouncies * 100 >= i * 99) return i;
    }
  }
  
  static void Main()
  {
    Console.WriteLine(new E112().Compute());
  }
}

