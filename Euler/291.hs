import Math.NumberTheory.Primes.Testing ( isPrime )

main = print $ length $ filter isPrime $ takeWhile (<(5*10^15)) [(i+i)*(i+1)+1 | i <- [1..]]

