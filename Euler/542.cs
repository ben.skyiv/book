using System;
using System.IO;
using System.Linq;

sealed class E542
{
  static void Main()
  {
    var a = File.ReadAllLines("542.txt")[0].Split().
      Select(x => int.Parse(x)).ToArray();
    for (int i = 0; i < a.Length - 1; i++)
      if ((a[i+1] - a[i]) % 2 != 0)
        Console.Write("{0} ", a[i]);
  }
}

