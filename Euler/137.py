def fib(n,a=1,b=1):
    while n>1:
        n-=1
        a,b=a+b,a
    return b
 
def golden_nugget(m):
    return fib(2*m)*fib(2*m+1)
 
print golden_nugget(15)

