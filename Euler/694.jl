using Primes, Base.Iterators

const prs = Int128.(primes(10^6+100))

function S(n, i)
	res = n
	for j in countfrom(i)
		(q = (p = prs[j])^3) ≤ n || break
		while q ≤ n
			res += S(n ÷ q, j+1)
			q *= p
		end
	end
	res
end

S(n) = S(n, 1)

@show @time S(10^18)
