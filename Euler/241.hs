import Math.NumberTheory.Primes.Factorisation ( factorise )

main = print $ sum $ foldl (\ns x -> f ns (10^18) 1 2 x) [] [3,5..11] where
  f ns n m u v = let (p,i) = head $ factorise u in
    if mod m p == 0 then ns else foldl (\ns (a,m) -> let
    x = u*(p^(a+1)-1); y = v*(p^a*(p-1)); g = gcd x y; z = div x g; w = div y g
    in if z == w then m : ns else if z > 1 && w > 1 then f ns n m z w else ns)
    ns $ takeWhile ((<=n) . snd) [(a,m*p^a) | a <- [i..]]

