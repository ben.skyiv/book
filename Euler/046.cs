using System;

sealed class E046
{
  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    composite[0] = composite[1] = true;
    return composite;
  }
  
  int Compute(int max)
  {
    var composite = Sieve(max);
    for (var i = 9; i <= max; i += 2)
    {
      if (!composite[i]) continue;
      for (var k = 1; ; k++)
      {
        var k2 = ((k * k) << 1);
        if (k2 >= i) return i;
        if (!composite[i - k2]) break;
      }
    }
    return 0;
  }

  static void Main()
  {
    Console.WriteLine(new E046().Compute(10000));
  }
}

