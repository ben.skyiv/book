import Data.MemoTrie ( memo2 )
import Math.NumberTheory.Primes.Sieve ( primes )

f x = g x x; g = memo2 h; h 0 _ = 1; h 1 _ = 0
h n m = sum [g (n-x) $ min x (n-x) | x <- takeWhile (<=m) primes]
main = let n = 5000 in print $ until ((>n) . f) succ 2

