using System;
using System.Collections.Generic;

sealed class E035
{
  bool[] composite;

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    composite[0] = composite[1] = true;
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  bool IsTruncatablePrime(int x)
  {
    for (var m = 10; ; m *= 10)
    {
      var n = x % m;
      if (composite[n]) return false;
      if (n == x) break;
    }
    for (var n = x / 10; n > 0; n /= 10)
      if (composite[n]) return false;
    return true;
  }

  int Compute(int max)
  {
    var sum = 0;
    composite = Sieve(max);
    for (var i = 11; i <= max; i += 2)
    {
      if (!IsTruncatablePrime(i)) continue;
      Console.Write(i + " ");
      sum += i;
    }
    return sum;
  }

  static void Main()
  {
    Console.WriteLine("[{0}]", new E035().Compute(999999));
  }
}
