#include <stdio.h>

typedef struct { int n, ps[10]; } item;

const int max = 3800, n = 1000*max;

void init(char cs[], int ps[], int idx[])
{
  for (int i = 2; i * i < n; i++)
    if (!cs[i]) for (int j = i * i; j < n; j += i) cs[j] = 1;
  for (int j = 0, i = 2; i < n; i++) {
    if (cs[i]) continue;
    ps[j] = i; idx[i] = j++;
  }
}

void factor(item as[], const char c[], const int idx[])
{
  static int t[n];
  for (int i = 0; i < n; i++) t[i] = 1, as[i].n = 0;
  for (int p = 2; p * p < n; p++)
    if (!c[p])
      for (long q = p; q < n; q *= p)
        for (int i = q; i < n; t[i] *= p, i += q)
          if (q == p) as[i].ps[as[i].n++] = p;
  for (int i = 0; i < n; i++)
    if (i != t[i]) as[i].ps[as[i].n++] = i / t[i];
  for (int i = 0; i < n; i++)
    for (int j = 0; j < as[i].n; j++)
      as[i].ps[j] = idx[as[i].ps[j]];
}

int get_q_idx(item a, item b)
{ // -1 for gcd(a, b) != 1
  int i, j, d = 0, q = 0;
  for (i = 0, j = 0; i < a.n && j < b.n; ) {
    int u = a.ps[i], v = b.ps[j], min;
    if (u == v) return -1;
    if (u < v) i++, min = u; else j++, min = v;
    if (d) continue;
    if (min != q) { d = 1; break; }
    q++; //printf("[%d %d %d %d %d]",i,j,u,v,q);
  }
  while (!d && i < a.n && a.ps[i++] == q) q++;
  while (!d && j < b.n && b.ps[j++] == q) q++;
  return q;
}

int main()
{
  static item as[n]; static char cs[n];
  static int ps[n], idx[n], vs[max];//, v2[max];
  init(cs, ps, idx); factor(as, cs, idx);
//printf("(%d)",get_q_idx(as[14],as[5]));
  for (int a = n - 1; a > 0; a--)
    for (int b0 = a-max+1, b = (b0>1) ? b0 : 1; b <= a; b++) {
      int p, i = get_q_idx(as[a], as[b]);
      if (i < 0) continue;
      long q = ps[i], q2 = q * q;
      if ((p = a + b) < max && p < q2) vs[p] = a;
      if ((p = a - b) < max && p < q2 && p > 1) vs[p] = a;
    }
//for(int i=2;i<max;i++)printf("%d:%d%s%d ",i,vs[i],v2[i]>=0?"+":"",v2[i]);
  long z = 0;
  for (int i = 2; i < max; i++) {
    if (cs[i]) continue;
    if (vs[i] == 0) printf("%d ", i);
    z += vs[i];
  }
  printf("(%ld)\n", z);
}

