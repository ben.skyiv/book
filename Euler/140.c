#include <stdio.h>

int main(void)
{
  long z = 0;
  for (long n=1,a=1,b=2; n <= 15; n++,a=a+b,b=a+b) z+=(a*b+3*a*a)/(b*b-a*b-a*a);
  for (long n=1,a=2,b=5; n <= 15; n++,a=a+b,b=a+b) z+=(a*b+3*a*a)/(b*b-a*b-a*a);
  printf("%ld\n",z);
  return 0;
}

