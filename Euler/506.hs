import Data.List ( zipWith4 )
import Math.NumberTheory.Moduli ( powerMod )

main = let n = 10^14; m = 123454321; q = 10^6; v = powerMod (q-1) (-1::Int) m
  in print $ flip mod m $ sum $ zipWith4
  (\h t a p -> (q * v * (powerMod q h m - 1) - h) * a * p * v + (h+1) * t)
  [let (h,r) = divMod n 15 in h - if i <= r then 0 else 1 | i <- [1..15]]
  [1,2,3,4,32,123,43,2123,432,1234,32123,43212,34321,23432,123432]
  [123432,234321,343212,432123,321234,123432,432123,212343,432123
  ,123432,321234,432123,343212,234321,123432]
  $ map (10^) [1,1,1,1,2,3,2,4,3,4,5,5,5,5,6]

