#include <bits/stdc++.h>
using namespace std;

const int N = 10000000;
vector<pair<int,int>> vec[N+1];

int gcd(int a, int b) { return b == 0 ? a : gcd(b,a%b); }

bool get(int a, int b, int& c)
{
  if (vec[a].empty()) return false;
  size_t lo = 0, hi = vec[a].size() - 1;
  while(lo <= hi) {
    size_t mid = (lo + hi) / 2;
    if      (vec[a][mid].first < b) lo = mid + 1;
    else if (vec[a][mid].first > b) hi = mid - 1;
    else { c = vec[a][mid].second; return true; }
  }
  return false;
}

int main()
{
  for (int m = 2; ; m++) {
    bool any = false;
    for (int n = 1; n < m; n++) {
      int a = m*m - n*n, b = 2*m*n, c = m*m + n*n;
      if (2 * (a + b) > N) break;
      any = true;
      if (n % 2 == 1 && m % 2 == 1) continue;
      if (gcd(n,m) != 1) continue;
      for (int d = 1; 2 * d * (a + b) <= N; d++) {
        vec[d*a].push_back(make_pair(d*b, d*c));
        vec[d*b].push_back(make_pair(d*a, d*c));
      }
    }
    if (!any) break;
  }
  long sm = 0;
  for(int a = 1; a <= N; a++) {
    sort(vec[a].begin(), vec[a].end());
    if (a % 2 != 0) continue;
    for (size_t i = 0; i < vec[a].size(); i++) {
      int b = vec[a][i].first;
      for (size_t j = 0; j < vec[a/2].size(); j++) {
        int d2, d1 = vec[a/2][j].second, h = vec[a/2][j].first;
        if (h >= b || a + 2*b + 2*d1 > N) break;
        if (get(a/2, b+h, d2)) sm += a + 2*b + 2*d1;
      }
    }
  }
  cout<<sm<<endl;return 0;
}

