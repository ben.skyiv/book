#include <stdio.h>
#include <math.h>

int g(int s, int p, int q)
{
  double z, w;
  for (double a = 0.001, b = 0.999 * M_PI; b - a > 1e-10; ) {
    double r = (s + q) / sin(z = (a + b) / 2);
    if (sin(z - (w = asin((s + p) / r))) * r <= p + q - 2 * M_PI) a = z;
    else b = z;
  }
  return (int)((z * (s + p) + w * (s + q)) / M_PI);
}

int main()
{
  int n = 500; long z = 0;
  for (int s = 5; s < n; s++)
    for (int p = 5; p <= n - s; p++)
      for (int q = p + 1; q <= n - s - p; q++)
        z += g(s, p, q);
  printf("%ld\n", z);
}

