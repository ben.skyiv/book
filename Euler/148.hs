import Data.List ( tails )
import Data.Digits ( digitsRev )

main = let n = 10^9; p = 7; f x = div (x*x+x) 2; in print $ sum
  $ map (\(e,x:xs) -> f p ^ e * f x * product (map (+1) xs))
  $ zip [0..] $ init $ tails $ digitsRev p n

