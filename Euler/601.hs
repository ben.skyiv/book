p s n = let m = n-2; r = foldl1 lcm [1..s] in div m r - div m (lcm r $ s+1)
main = print $ sum [p i (4^i) | i <- [1..31]]

