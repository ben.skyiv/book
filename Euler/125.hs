import Data.List ( tails )
import Data.Set ( foldl,fromList )

main = let n = 10^8 in print $ Data.Set.foldl (+) 0 $ fromList
  $ filter (\x -> let z = show x in z == reverse z) $ concatMap
  (takeWhile (<n) . drop 1 . scanl1 (+)) $ tails $ takeWhile
  (< div n 2) $ map (^2) [1..]

