#include <stdio.h>
#include <stdlib.h>

int g(long n)
{
  int z = 0;
  for (int i; n > 1; )
    if ((i = n % 7) == 0) n /= 7;
    else z += i = 7 - i, n /= 7, n++;
  return z;
}

int main()
{
  for (long z = 0, i7 = 1, k = 0, i = 1; ; i++) {
    z += g(i);
    if (i != i7) continue;
    i7 *= 7; printf("S(7^%02ld) = %ld\n", k++, z);
  }
}

