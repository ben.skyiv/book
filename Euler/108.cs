// http://en.wikipedia.org/wiki/Euler%27s_totient_function

using System;
using System.Collections.Generic;

sealed class E108
{
  int[] primes;
  int[] sieve;  // 0 for prime, else prime factor
  int[] totient;

  int Gcd(int a, int b)
  {
    for (int t; b != 0; a = b, b = t) t = a % b;
    return a;
  }

  SortedList<int, int> Merge(IEnumerable<int> list)
  {
    var v = new SortedList<int, int>();
    foreach (var p in list)
    {
      int n;
      if (!v.TryGetValue(p, out n)) v.Add(p, 1);
      else v[p] = n + 1;
    }
    return v;
  }
  
  SortedList<int, int> GetFactors(int x)
  {
    var list = new List<int>();
    var limit = (int)Math.Sqrt(x) + 1;
    for (int i = 0, p = primes[i]; p <= limit; p = primes[i])
      if (x % p == 0) { list.Add(p); x /= p; }
      else i++;
    if (x != 1) list.Add(x);
    return Merge(list);
  }

  int[] GetPrimes()
  {
    var primes = new List<int>();
    for (var i = 2; i < sieve.Length; i++)
      if (sieve[i] == 0) primes.Add(i);
    primes.Add(int.MaxValue);
    return primes.ToArray();
  }

  void Initialize(int max)
  {
    int limit = (int)Math.Sqrt(max);
    sieve = new int[max + 1];
    totient = new int[max + 1];
    for (int i = 2; i <= limit; i++)
      if (sieve[i] == 0)
        for (int j = i * i; j <= max; j += i)
          sieve[j] = i;
    primes = GetPrimes();
  }
  
  int Totient2(int n)
  {
    var factors = GetFactors(n);
    int v = 1;
    foreach (var kvp in factors)
      v *= (kvp.Key - 1) * Pow(kvp.Key, kvp.Value - 1);
    return v;
  }
  
  int Pow(int x, int y)
  {
    int z = 1;
    while (y-- > 0) z *= x;
    return z;
  }
  
  int Totient(int n)
  {
    if (n == 1) return 1;
    if (sieve[n] == 0) return n - 1;
    int a = sieve[n], b = n / a;
    if (Gcd(a, b) == 1) return totient[a] * totient[b];
    return Totient2(n);
  }
  
  long Compute()
  {
    var max = 30;
    Initialize(max);
    for (var i = 2; i <= max; i++)
    {
      totient[i] = Totient(i);
      Console.Write("{0}:{1} ", i, i - totient[i] + 1);
    }
    return 0;
  }

  static void Main()
  {
    Console.WriteLine(new E108().Compute());
  }
}

