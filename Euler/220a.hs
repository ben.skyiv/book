dragon 0 = ((0,0),(0,1))
dragon n
  | even n    = ((x+y, y-x), (x+y, v-u))
  | otherwise = ((x+y, v-u), (u+v, v-u))
  where ((x,y),(u,v)) = dragon $ div n 2
main = print . fst . dragon $ 10^12

