import Text.Printf ( printf )

f 0 _ a b c = a * b * c
f n z a b c = sum [f (n-1) 0 a b c * (13+a+b+c),
  if a == 0 then   f (n-1) 0 1 b c     else 0,
  if b == 0 then   f (n-1) 0 a 1 c     else 0,
  if c == 0 then   f (n-1) z a b (1-z) else 0]

main = let n = 16 in printf "%X\n" (f n 1 0 0 0 :: Int)

