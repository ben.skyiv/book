#include <stdio.h>

int valid(int z, const int a[])
{
  static int b[10];
  if (z < 1000 || z > 9999) return 0;
  for (int i = 0; i < 10; i++) b[i] = a[i];
  for (; z > 0; z /= 10) {
    int r = z % 10;
    if (b[r]) return 0;
    b[r] = 1;
  }
  return 1;
}

int main(void)
{
  static int a[10] = { 1 };
  for (int i = 1; i <= 9; i++) {
    a[i] = 1;
    for (int j = 1; j <= 9; j++) {
      if (a[j]) continue;
      a[j] = 1;
      for (int k = 1; k <= 9; k++) {
        if (a[k]) continue;
        a[k] = 1;
        for (int l = 1; l <= 9; l++) {
          if (a[l]) continue;
          a[l] = 1;
          for (int m = 1; m <= 9; m++) {
            if (a[m]) continue;
            a[m] = 1;
            int x = i * 10 + j, y = k * 100 + l * 10 + m;
            //int x = i, y = j * 1000 + k * 100 + l * 10 + m;
            int z = x * y;
            if (valid(z, a))
              printf("%d * %d = %d\n", x, y, z);
            a[m] = 0;
          }
          a[l] = 0;
        }
        a[k] = 0;
      }
      a[j] = 0;
    }
    a[i] = 0;
  }
  return 0;
}
