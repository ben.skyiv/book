import Data.Array ( (!),listArray )

main = let
  n = 12*10^3; f n = let (q,r) = divMod n 6 in q*(3*q+r-2) + if r == 5 then 1 else 0
  a = listArray (5,div n 3) [f i - sum [a!div i m | m <- [2..div i 5]] | i <- [5..]]
  in print $ f n - f (div n 2) - sum [a ! div n m | m <- [3,5..div n 5]]

