sealed class E224
{
  int Compute(int p, int a, int b, int c, int v)
  {
    if (a + b + c > p) return v; v++;
    v = Compute(p, 2*c+b+2*a, 2*c+2*b+a, 3*c+2*b+2*a, v);
    v = Compute(p, 2*c-2*b+a, 2*c-b+2*a, 3*c-2*b+2*a, v); return (a == b) ?
    v : Compute(p, 2*c+b-2*a, 2*c+2*b-a, 3*c+2*b-2*a, v);
  }

  static void Main()
  {
    System.Console.WriteLine(new E224().Compute(75000000, 2, 2, 3, 0));
  }
}

