import Data.List ( sort )
import Data.Array ( (!),assocs,listArray )
import Math.NumberTheory.Primes.Factorisation ( factorise )

main = let
  n = 120000-1; rad = product . map fst . factorise
  rads = listArray (1,n) [rad i | i <- [1..]]
  invs = sort $ map (\(a,b) -> (b,a)) $ assocs rads
  in print $ sum [c | (rc,c) <- invs, 2 * rc < c,
  (ra, a) <- takeWhile (\x -> fst x * 2 * rc < c) invs,
  a < div c 2, gcd ra rc == 1, ra * rads!(c-a) < div c rc]

