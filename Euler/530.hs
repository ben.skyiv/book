import System.Environment

main = do
  s <- getArgs
  print $ let n = read $ head s in
    sum [gcd d $ div n d | d <- [1..n], mod n d == 0]
{-
f(1) = 1
f(p^{2k-1}) = 2 * (p^k - 1) / (p-1)
f(p^{2k}) = p^k + f(p^{2k-1})
f(a*b) = f(a)*f(b), if gcd(a, b) == 1 -}

