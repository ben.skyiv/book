using System;
using System.Drawing;
using System.Collections.Generic;

static class E662
{
  static readonly int m = 7 + (int)1e9, n = 10000;
  static readonly int[] fib = GetFibonacci();
  static readonly Point[] hyp = GetHypotenuse();
  static int[,] f = new int[n+1, n+1];

  static int Gcd(int a, int b) { return b == 0 ? a : Gcd(b, a % b); }
  
  static int[] GetFibonacci()
  {
    var a = new List<int>();
    for (int r = 0, s = 1, t = 1; t <= 2*n; r = s, s = t) a.Add(t = r + s);
    return a.ToArray();
  }

  static Point[] GetHypotenuse()
  {
    int max = (int)Math.Sqrt(n);
    var a = new List<Point>();
    for (var i = 1; i < max; i += 2)
      for (var j = 2; j < max; j += 2) {
        int m = Math.Max(i, j), n = Math.Min(i, j);
        if (Gcd(m, n) != 1) continue;
        for (var k = 1; k < E662.n; k++) {
          int x = k * (m*m - n*n), y = k*2*m*n, z = k * (m*m + n*n);
          if (x > E662.n || y > E662.n) continue;
          if (Array.BinarySearch(fib, z) < 0) continue;
          a.Add(new Point(x, y));
        }
      }
    return a.ToArray();
  }

  static int F(int x, int y)
  {
    if (x == 0 && y == 0) return 1;
    if (f[x, y] != 0) return f[x, y];
    int v = 0, a, b;
    foreach (var i in fib) { if ((a = x-i) < 0) break; v = (v+F(a,y)) % m; }
    foreach (var i in fib) { if ((b = y-i) < 0) break; v = (v+F(x,b)) % m; }
    foreach (var i in hyp)
    { if ((a = x-i.X) < 0 || (b = y-i.Y) < 0) continue; v = (v+F(a,b)) % m; }
    foreach (var i in hyp)
    { if ((a = x-i.Y) < 0 || (b = y-i.X) < 0) continue; v = (v+F(a,b)) % m; }
    return f[x, y] = v;
  }

  static void Main()
  {
foreach (var i in fib) Console.Write(i+" ");Console.WriteLine("({0})", fib.Length);
foreach (var i in hyp) Console.Write("({0},{1})",i.X,i.Y);
Console.WriteLine("({0})", hyp.Length);
    Console.WriteLine("F({0},{0}) = {1}", n, F(n, n));
  }
}

