import Data.Array ( (!),listArray )
import Math.Combinat.Numbers ( binomial )
import Math.NumberTheory.Primes.Sieve ( primes )

n = 10^11; v = 242; m = round $ logBase 3 (v+1); a = listArray (1,z) ps
z = length $ takeWhile ((>=) $ div n $ product $ take (m-1) ps) ps
ps = (7:) $ (9:) $ tail $ filter ((==1) . (flip mod 3)) primes
g (t,j) | j < m = 0 | odd $ m-j = -x | True = x
  where x = let v = div n t in  t * binomial j m * div (v*(v+1)) 2
f [] x = x; f ((t,i,j):xs) x = f xs $ f [(t*a!k, k+1, j+1) |
  k <- takeWhile (\x -> t*(a!x)^(max 1 $ m-j) <= n) [i..z]] x + g (t,j)
main = print $ f [(1,1,0)] 0

