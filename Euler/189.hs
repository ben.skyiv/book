import System.Environment ( getArgs )
import Control.Monad ( foldM )
import Data.Maybe ( fromJust )
import Data.Map ( fromListWith,toList )
import Data.List ( (\\),elemIndex,nub )
import Math.NumberTheory.Powers.Squares ( integerSquareRoot )

main = do
  s <- getArgs
  let n = if null s then 64 else read $ head s
  let m = integerSquareRoot n
  let f1 xs = map (succ . fromJust . flip elemIndex (nub xs)) xs
  let f2 xs = f1 [m | (n, m) <- zip [0..] xs, even n]
  let f3 xs z = map (:xs) $ [1..3] \\ [head xs, z]
  let f4 xs = 0 : foldr ((++) . (:[0])) [] xs
  let f5 xs = [(f2 zs, k) | (ys, k) <- xs, zs <- foldM f3 [0] $ f4 ys]
  let f6 = f5 . toList . fromListWith (+)
  print $ sum $ map snd $ (!!m) $ iterate f6 [([],1)]

