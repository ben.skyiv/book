f(n, m) = { z = 0; forprime(p = 2, m, d = gcd(15, p-1); sqrtn(Mod
  (1, p), d, &q); z += p * sum(i = 1, d, (n+lift(q^i))\p)); z }
print(f(10^11, 10^8)); quit() \\ runtime: 23.831s

