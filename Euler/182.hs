main = let p = 1009; q = 3643 in print $ let a = p-1; b = q-1 in sum [e |
  e <- [2..a*b-1], gcd e (a*b) == 1, gcd (e-1) a == 2, gcd (e-1) b == 2]

