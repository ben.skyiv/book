using System;
using System.Numerics;
using System.Collections.Generic;
using Skyiv.Utils;

static class E615
{
　static readonly int m = 123454321, n = (int)1e6, z = (int)2e5;
　static readonly int[] primes = Primes.GetPrimes(0, z);
　static readonly double max = primes[primes.Length - 1] / 2.0;
　static readonly List<double> list = new List<double>();

　static void Make(double u, int i0)
　{
　　double v = u; for (; v <= max; v *= 2) list.Add(v);
　　for (var i = i0; i < primes.Length &&
　　　(v = u * primes[i] / 2) <= max; i++) Make(v, i);
　}

　static Tuple<long, int> Tran(double u)
　{ // return: (a, b): u == a / 2^b
　　for (var i = 0; ; i++, u *= 2)
　　　if (Math.Abs(Math.Round(u) - u) < 1e-3)
　　　　return Tuple.Create((long)Math.Round(u), i);
　}

　static void Main()
　{
　　Make(1, 1); list.Sort(); var t = Tran(list[n - 1]);
　　Console.WriteLine(BigInteger.ModPow(2, n-t.Item2, m) * t.Item1 % m);
　}
}

