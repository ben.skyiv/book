using System;

sealed class E144
{
  static void Main()
  {
    int z = 0;
    for (double xA = 0, yA = 10.1, x0 = 1.4, y0 = -9.6;
      x0 > 0.01 || x0 < -0.01 || y0 < 0; z++) {
      double sA = (y0-yA)/(x0-xA), s0 = -4*x0/y0, tA = (sA-s0)/(1+sA*s0);
      double sB = (s0-tA)/(1+tA*s0), iB = y0 - sB * x0;
      double a = 4+sB*sB, b = 2*sB*iB, c = iB*iB-100, d = Math.Sqrt(b*b-4*a*c);
      double a1 = (-b-d)/(2*a), a2 = (-b+d)/(2*a); xA = x0; yA = y0;
      x0 = (Math.Abs(a1-x0) > Math.Abs(a2-x0)) ? a1 : a2; y0 = sB*x0 + iB;
    }
    Console.WriteLine(z);
  }
}

