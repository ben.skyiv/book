import Data.List ( sort )
import Data.Set ( fromList,size )

main = let cot = [1 / tan (x*pi/180) | x <- [0..180]] in print $ size
  $ fromList [head $ sort [[a,b,c,d],[c,d,e,f],[e,f,g,h],
  [g,h,a,b],[d,c,b,a],[f,e,d,c],[h,g,f,e],[b,a,h,g]] |
  a <- [1..45], b <- [a..178-a], c <- [a..179-a-b], d <- [a..179-b-c],
  let t1 = cot !! (c+d) + cot !! b - cot !! (a+b) - cot !! c,
  let t2 = cot !! (a+b) * cot !! (c+d) - cot !! b * cot !! c,
  let tt = atan (t1 / t2) * 180 / pi; t = round tt,
  abs (tt - fromIntegral t) < 10**(-9),
  let f = mod (b - t + 180) 180; e = 180 - b - c - d,
  let g = mod (c + t + 180) 180; h = 180 - a - b - c,
  e >= a && f >= a && g >= a && h >= a]

