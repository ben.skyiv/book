const M = 10^9+7
⦿(a, b) = a * b % M
⊕(a, b) = (a + b) % M

w(a, n) = a ≠ 1 ? (powermod(a, n+1, M) - 1) ⦿ invmod(a-1, M) : n ⊕ 1

function I(α, n)
	binx = ones(Int, α)
	bin(a) = binx[a+1]
	for k in 1:α-1
		binx[k+1] = bin(k-1) ⦿ (α - k + 1) ⦿ invmod(k, M)
	end
	reduce(⊕, bin(a) * (-1)^(α-a+1) * w(a, n) for a in 0:α-1)
end

@show @time I(10^7, 10^12)

