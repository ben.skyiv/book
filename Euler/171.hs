import Data.List ( genericLength, genericReplicate, group )

sumNums xs = div (11111111111111111111 * (product $ take (length xs - 1) [19,18..])
  * sum xs) $ product (map (product . enumFromTo 1 . genericLength) $ group xs)

digitGroups = concatMap (f [] 9 20) $ takeWhile (< 9*9*20) $ map (^2) [1..] where
  f xs _ _ 0 = [xs]
  f xs _ 0 _ = []
  f xs 0 _ _ = []
  f xs d l s = [x | n <- [0..l], let z = d^2 * n, z <= s, s <= z + (d-1)^2 * (l-n),
    x <- f (genericReplicate n d ++ xs) (d-1) (l-n) (s-z)]

main = print $ mod (sum $ map sumNums digitGroups) (10^9)

