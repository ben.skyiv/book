main = print $ length $ filter (\(p,q) -> length (show p) > length
  (show q)) $ take 1000 $ iterate (\(p,q) -> (p+q+q, p+q)) (1,1)

