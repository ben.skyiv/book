using System.Collections.Generic;

sealed partial class E548
{
  sealed class Gozinta
  {
    HashSet<int> chain = new HashSet<int>();
    
    Gozinta(int mask, int[] a)
    {
      for (var i = 0; mask > 0; i++, mask >>= 1)
        if ((mask & 1) != 0) chain.Add(a[i]);
    }
    
    public static void Add(HashSet<Gozinta> g, params int[] a)
    {
      for (var n = (1 << a.Length) - 1; n >= 0; n--)
        g.Add(new Gozinta(n, a));
    }
    
    public override bool Equals(object other)
    {
      var b = other as Gozinta;
      if (b == null) return false;
      return chain.SetEquals(b.chain);
    }
    
    public override int GetHashCode()
    {
      var v = 0;
      foreach (var i in chain) v ^= i;
      return v;
    }  
  }

  static void Make1(HashSet<Gozinta> c, int[] a, int[] b, int bi, int n)
  {
    var t = new int[n+1];
    for (int j, i = 0; i < n; i++) {
      for (j = 0; j <= i; j++) t[j] = a[j];
      for (     ; j <= n; j++) t[j] = b[j-1+bi];
      Gozinta.Add(c, t);
    }
  }

  static void Make2(HashSet<Gozinta> c, int[] a1, int[] a2, int[] a3, int n)
  {
    int m = 3;
    var a = new int[m, n];
    for (var i = 0; i < n; i++)
      { a[0,i] = a1[i]; a[1,i] = a2[i+1]; a[2,i] = a3[i+1]; }
    var t = new int[m+n-1];
    M2(c, t, a, 0, 0, 0);
  }

  static void M2(HashSet<Gozinta> c, int[] t, int[,] a, int y, int x, int i)
  {
    t[i] = a[y, x];
    if (i == t.Length - 1) Gozinta.Add(c, t);
    if (y+1 < a.GetLength(0)) M2(c, t, a, y+1, x, i+1);
    if (x+1 < a.GetLength(1)) M2(c, t, a, y, x+1, i+1);
  }
}

