using System;
using System.Collections.Generic;

static class E583
{
  static int Gcd(int a, int b) { return b == 0 ? a : Gcd(b, a%b); }

  static void Add(Dictionary<int,HashSet<int>> d, int k, int v)
  {
    HashSet<int> s;
    if (!d.TryGetValue(k, out s)) d.Add(k, s = new HashSet<int>());
    s.Add(v);
  }

  static Dictionary<int,HashSet<int>> GetTs(int m)
  {
    var dict = new Dictionary<int,HashSet<int>>();
    for (int a = 2; ; a++) {
      var done = true;
      for (int x, y, b = 1; b < a; b++) {
        if ((x = a*b << 1) + (y = a*a - b*b) >= m) break;
        if ((1 & (a + b)) == 0 || Gcd(a, b) != 1) continue;
        for (int u = x, v = y; u + v < m; u += x, v += y)
        { Add(dict, u, v); Add(dict, v, u); }
        done = false;
      }
      if (done) break;
    }
    return dict;
  }

  static void Main()
  {
    int n = (int)1e7; long z = 0; var ts = GetTs(n >> 1);
    foreach (var kvp in ts) {
      var w = kvp.Key; HashSet<int> vs, us = kvp.Value;
      if (!ts.TryGetValue(w + w, out vs)) continue;
      foreach (var h in us) {
        int p, f = w + (int)Math.Sqrt((long)h*h + (long)w*w);
        foreach (var g in vs)
          if (g > h && (p = (f+g)<<1) <= n && us.Contains(g+h))
            z += p;
      }
    }
    Console.WriteLine(z);
  }
}

