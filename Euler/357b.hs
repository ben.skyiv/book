import Control.Monad ( forM_,when )
import Data.Array.ST ( newArray,readArray,runSTUArray,writeArray )
import Data.Array.Unboxed ( (!),assocs,UArray )

sieve :: Int -> UArray Int Bool
sieve n = runSTUArray $ do
  let m = div (n-1) 2
  sieve <- newArray (1, m) True
  forM_ [1 .. div (floor . sqrt $ fromIntegral n) 2] $ \i -> do
    p <- readArray sieve i
    when p $ do
      forM_ [2*i*(i+1), 2*i*(i+2)+1 .. m] $ \j -> do
        writeArray sieve j False
  return sieve

main = let n = 10^8; ps = sieve (n+1) in print $ succ $ sum $ filter
  (\n -> all (\n -> odd n && ps ! (div n 2)) $ map (uncurry (+)) $
  filter ((==0) . (mod n) . fst) $ takeWhile (uncurry (<=))
  [(i, div n i) | i <- [2..]]) [i+i | (i,True) <- assocs ps]

