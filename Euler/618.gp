f(n)={s=vector(fibonacci(n)+1,i,Mod(i==1,10^9));
forprime(p=2,#s-1,for(i=p+1,#s,s[i]+=p*s[i-p]));
lift(sum(i=2,n,s[fibonacci(i)+1]))}
print(f(24));quit() \\ runtime: 44.5s

