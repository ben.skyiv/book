using System;
using System.Collections.Generic;

static class E583
{
  static int Gcd(int a, int b) { return b == 0 ? a : Gcd(b, a%b); }

  static void Add(HashSet<int>[] a, int k, int v)
  {
    if (a[k] == null) a[k] = new HashSet<int>();
    a[k].Add(v);
  }

  static HashSet<int>[] GetTs(int m)
  {
    var ts = new HashSet<int>[m];
    for (int a = 2; ; a++) {
      var done = true;
      for (int x, y, b = 1; b < a; b++) {
        if ((x = a*b << 1) + (y = a*a - b*b) >= m) break;
        if ((1 & (a + b)) == 0 || Gcd(a, b) != 1) continue;
        for (int u = x, v = y; u + v < m; u += x, v += y)
        { Add(ts, u, v); Add(ts, v, u); }
        done = false;
      }
      if (done) break;
    }
    return ts;
  }

  static void Main()
  {
    int n = (int)1e7; long z = 0; var ts = GetTs(n >> 1);
    for (var w = 0; w < ts.Length; w++)
      if (w+w < ts.Length && ts[w] != null && ts[w+w] != null)
        foreach (var h in ts[w]) {
          int p, f = w + (int)Math.Sqrt((long)h*h + (long)w*w);
          foreach (var g in ts[w + w])
            if (g > h && (p = (f+g)<<1) <= n && ts[w].Contains(g+h))
              z += p;
      }
    Console.WriteLine(z);
  }
}

