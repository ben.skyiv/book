main = print $ until (\n -> let
  h = 1 / (n + sqrt(2*n) + 1); θ = 2 * asin(sqrt(h/2))
  in (h - θ + sin θ) / 2 / (1 - pi/4) < 0.001) succ 1

