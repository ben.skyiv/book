import Data.List ( foldl' )
 
main = let n = 10^6 in print $ foldl' f (1,1) [2..n-1] where
g m n = if n==1 then m else g (m+1) $ if even n then div n 2 else 3*n+1
f u v = (\x y -> if snd x < snd y then y else x) u (v, g 1 v)

