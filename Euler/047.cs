using System;
using System.Collections.Generic;

sealed class E047
{
  int[] primes;

  void Initialize(int max)
  {
    var list = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) list.Add(i);
    primes = list.ToArray();
  }

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  int DistinctPrimeFactors(int x)
  {
    int count = 0, limit = (int)Math.Sqrt(x) + 1;
    for (int p0 = 0, i = 0, p = primes[i]; p <= limit; p = primes[i])
      if (x % p == 0) { if(p0 != p) count++; x /= (p0 = p); }
      else i++;
    if (x != 1) count++;
    return count;
  }
  
  bool Ok(int x) { return DistinctPrimeFactors(x) == 4; }
  
  int Compute(int max)
  {
    Initialize(max);
    for (var i = 2; i <= max; i++)
      if (Ok(i))
        if (Ok(i + 1))
          if (Ok(i + 2))
            if (Ok(i + 3)) return i;
            else i += 3;
          else i += 2;
        else i++;
    return 0;
  }

  static void Main()
  {
    Console.WriteLine(new E047().Compute(1234567));
  }
}

