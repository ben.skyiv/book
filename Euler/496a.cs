using System;

sealed class E496
{
  long Compute(int n)
  {
    long sum = 0;
    for (var q = 1; q < n; q++)
      for (int d = n-q, b = 1; b <= d; b++)
      {
        long x = q + (b<<1), z = x * q;
        if (z % b == 0 && x > z / b) sum += q + b;
      }
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E496().Compute(100000));
  }
}

