using System;

class E582
{ // from Jrdevil
  static long Count(long d, long n)
  {
    long y1 = d * 4, z = Eval(y1 + d * 3, y1, n);
    for (long y = d + 1; y < y1; y++) {
      long x2 = y*y*3 + d*d, x = (long)Math.Sqrt(x2);
      if (x * x == x2) z += Eval(x, y, n);
    }
    return z;
  }

  static long Eval(long x, long y, long n)
  {
    double s = Math.Sqrt(3);
    if (x % 2 == 1 && y % 2 == 0)
    { long z = x; x = x * 2 + y * 3; y = z + y * 2; }
    return 1 + (long)((n - Math.Log10((y*3 + x*s)/s/4))
      / Math.Log10((y % 2 == 0) ? (2 + s) : (7 + s*4)));
  }

  static void Main()
  {
    for (long z = 0, n = 10; n <= 1e9; n *= 10, z = 0) {
      for (var d = 1; d <= 100; d++) z += Count(d, n);
      Console.WriteLine("T(10^10^{0}) = {1}", Math.Log10(n), z);
    }
  }
}

