#include <stdio.h>
#include <time.h>

const int m = 10000019;

long rev(long n)
{
  long z = 0; for (; n > 0; n /= 10) z = z * 10 + n % 10;
  return z;
}

int g(long n, long b)
{
  return (rev(n) + n * (__int128)b) % m == 0;
}

long f(int e)
{
  clock_t t = clock();
  printf("%d: ", e << 1); fflush(stdout);
  long a = 1; for (int i = 1; i < e; i++) a *= 10;
  long z = 0, b = a * 10;
  for (long i = a; i < b; i++) if (g(i, b)) z++;
  t = clock() - t;
  printf("%10ld (%.0f seconds)\n", z, (float)t / CLOCKS_PER_SEC);
  return z;
}

int main(void)
{ // 16:8, 18:101, 20:711, 22:
  long z = 0; for (int e = 7; e <= 16; e++) z += f(e);
  printf("(a:%ld)\n", z);
}

