#include <iostream>
#include <cmath>

int f(int n, int m, int odd, int k, int a[])
{
  int z = 0;
  for (int i = k - 1; i >= 0; i--) {
    int u = m * a[i], v = n / (u<<1);
    z += (odd ? v : (n/u - v)) - f(n, u, odd, i, a);
  }
  return z;
}

int count(int n, int i, int k, int a[])
{
  return ((i&1) ? (n/2) : (n-n/2)) - f(n,1,i&1,k,a);
}

int main(void)
{
  const long n = 3'141'592'653'589'793;
  const int SIZE = 8, m = (int)sqrt(n/2.0);
  static int a[m+1][SIZE], b[m+1];
  for (int i = 3; i <= m; i += 2)
    if (b[i] == 0)
      for (int j = i; j <= m; j += i)
        a[j][b[j]++] = i;
  long z = 0;
  for (int j, i = 1; i < (j = (int)sqrt(n-(long)i*i)); i++)
    z += count(j, i, b[i], a[i]) - count(i, i, b[i], a[i]);
  std::cout << z << std::endl;
}

