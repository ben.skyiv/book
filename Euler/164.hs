import Data.Maybe ( fromJust )
import Data.Map as Map ( fromList,lookup )

main = print $ sum $ [f (n, z, 0) | z <- [1..9]] where
  f (d, a, b) = if d == 1 then 1 else sum [fromJust $ Map.lookup
    (d-1,z,a) $ fromList [(x, f x) | d <- [1..n-1], a <- [0..9],
    b <- [0..9], let x = (d,a,b)] | z <- [0..9], z + a + b <= 9]
  n = 20

