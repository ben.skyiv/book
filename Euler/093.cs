// TODO: Result Error.
using System;
using System.Collections.Generic;

sealed class E093
{
  static readonly double eps = 0.000001;
  static readonly string[] permutations = {
	"1234","2134","3214","2314","3124","1324","4321","3421",
	"3241","4231","2431","2341","4123","1423","1243","4213",
	"2413","2143","4132","1432","1342","4312","3412","3142"
  };
  
  double Compute(double x, double y, int op)
  {
    switch (op)
	{
	  case 0: return x + y;
	  case 1: return x - y;
	  case 2: return x * y;
	  case 3: return x / y;
	  default: throw new Exception("op");
	}
  }

  HashSet<int> Compute(int[] abcd)
  {
    var a = new int[4];
	var set = new HashSet<int>();
    foreach (var perm in permutations)
	{
	  for (var i = 0; i < 4; i++) a[i] = abcd[perm[i] - '1'];
	  for (var i = 0; i < 4; i++)
	  {
	    var b = Compute(a[0], a[1], i);
	    for (var j = 0; j < 4; j++)
		{
		  var c = Compute(b, a[2], j);
		  for (var k = 0; k < 4; k++)
		  {
		    var d = Compute(c, a[3], k);
			var n = (int)Math.Round(d);
			if (n > 0 && Math.Abs(d - n) < eps) set.Add(n);
		  }
		}
      }
	}
    return set;
  }
  
  int GetN(HashSet<int> set)
  {
    var n = 1;
    while (set.Contains(n)) n++;
	return n - 1;
  }

  void Compute()
  {
    var max = 0;
	var abcd = "";
    var a = new int[4];
	for (a[0] = 1; a[0] < 7; a[0]++)
	  for (a[1] = a[0] + 1; a[1] < 8; a[1]++)
	    for (a[2] = a[1] + 1; a[2] < 9; a[2]++)
		  for (a[3] = a[2] + 1; a[3] < 10; a[3]++)
		  {
            var n = GetN(Compute(a));
			if (max >= n) continue;
			max = n;
			abcd = a[0].ToString() + a[1] + a[2] + a[3];
	      }
    Console.WriteLine("{0}:{1}", max, abcd);
  }

  static void Main()
  {
    new E093().Compute();
  }
}