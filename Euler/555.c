#include <stdio.h>

int main(void)
{
  long p = 1000000, m = p, z = 0;
  for (long a, b = 1; (a = p/b-1) > 0; b++)
    z += (1 + 2*m - a*b) * a * b;
  printf("%ld\n", z / 2);
}

