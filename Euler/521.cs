// dmcs 521.cs lib/Primes.cs -r:System.Drawing.dll && mono 521.exe
using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using Skyiv.Utils;

sealed class E521
{
  long n = 1000000;//000000;
  int m = 1000000000;
  int[] primes;
  long M(long a, long b) { return (a > n / b) ? 0 : a * b; }
  
  Point S(int p, int high, int sign, long a, int low)
  {
    var t = Point.Empty; long z;
    for (var i = low; i < high && (z = M(a, primes[i])) != 0; i++)
    {
      var s = S(p, high, -sign, z, i+1);
      var q = n / z;
      t.X = (int)((s.X + t.X + sign * q%m * p) % m);
      t.Y = (int)((s.Y + t.Y + sign * q%m * ((q+1)%m) / 2 % m * (z%m)) % m);
    }
    return t;
  }

  Point S(int k)
  {
    var dt1 = DateTime.Now;
    var p = primes[k];
    var s = S(p, k, -1, p, 0);
    var q = n / p;
    var v = new Point((int)((q*p+s.X)%m), (int)((q%m*((q+1)%m)/2%m*p+s.Y)%m));
    if (k % 100 == 0)
    {
      var dt2 = DateTime.Now;
      Console.WriteLine("{0:dd HH:MM:ss} {1,7:F3} {2,7:N0} {3,6:N0} {4,2}",
        dt2, (dt2 - dt1).TotalSeconds, p, k, Thread.CurrentThread.ManagedThreadId);
    }
    return v;
  }

  void Compute()
  {
    var dt1 = DateTime.Now;
    Console.Write("{0:dd HH:MM:ss} {1:N0} ", dt1, n);
    int v = 0; var mutex = new object();
    primes = Primes.GetPrimes(2, (int)Math.Sqrt(n));
    Console.WriteLine("primes:{0:N0}", primes.Length);
    Parallel.For(0, primes.Length, () => 0,
      (i,_,x) => { var s = S(i); return (s.X - s.Y + x) % m; },
      x => { lock(mutex) v = (v + x) % m; });
    v = (int)((v - 1 + n % m * ((n+1) % m) / 2) % m);
    if (v < 0) v += m;
    var dt2 = DateTime.Now;
    Console.WriteLine("{0:dd HH:MM:ss} {1} {2:N0}", dt2, dt2 - dt1, v);
  }

  static void Main()
  {
    new E521().Compute();
  }
}

