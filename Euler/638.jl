using Nemo
a = 15; b = 10; k = 3; c = a + b; m = 10^9+7
R,q = PowerSeriesRing(ResidueRing(ZZ,m),a*b+1,"q")
G1 = prod([1 - q^i for i = a+1 : c])
G2 = prod([1 - q^i for i =   1 : b])
G = G1 * inv(G2)
@time println(sum([coeff(G,i)*powermod(k,i,m) for i = 0 : a*b]))

