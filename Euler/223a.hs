import Data.Set ( elems,fromList )
import Math.NumberTheory.Primes.Factorisation ( divisors )

main = let p = 25*10^6 in print $ (+) (fromIntegral $ div (p-1) 2) $
  length $ concat $ map (\(a,xs) -> filter (\(b,c) -> a+b+c <= p) $ filter
  ((>=a) . fst) $ map (\(b,c) -> (div b 2, div c 2)) $ filter (even . fst)
  $ map (\(n,m) -> (m-n,m+n)) $ take (div (length xs) 2) $ zip xs $
  reverse xs) [(a, elems $ let f = elems . divisors in fromList
  [x*y | x <- f $ a-1, y <- f $ a+1]) | a <-[2 .. div p 3]]

