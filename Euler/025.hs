fibs = 0 : 1 : zipWith (+) fibs (tail fibs)
main = let n = 1000 in print $ length $ takeWhile (<10^(n-1)) fibs

