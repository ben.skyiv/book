#include <stdio.h>

int main(void)
{
  long n = (long)1e16;
  long m = n + 1;
  int a[128], j = 0;
  for (int i = 0; m > 0; m /= 2, i++) if (m % 2 == 1) a[j++] = i;
  int k = a[j - 1];
  long z = (k-3) * (1L << k) + k + 3;
  for (int i = j - 2; i >= 0; i--) z += (k-1) * (1L << a[i]) + 1;
  printf("S(%ld) = %ld\n", n, z);
}
