using System;
using System.Linq;

sealed class E244
{
  static readonly int w = 4, size = w*w, max = 40, target = 0x5A5A;
  static long[] sums = new long[max + 1];
  static int[] cache = Enumerable.Repeat(int.MaxValue, 1048576).ToArray();
  static int[] moves = new int[max];
  static int min = 0;

  static long Checksum(int[] moves, int length)
  {
    long sum = 0;
    for (var i = 0; i < length; i++) sum = (sum*243 + moves[i]) % 100000007;
    return sum;
  }

  static void Solve(int[] src, int dist, int dir, int from, int to)
  {
    if (dir != 0) moves[dist++] = dir;
    var a = src.ToArray();
    a[from] = a[to];
    a[to] = '.';
    var id = to << size;
    for (var i = 0; i < size; i++) if (a[i] == '1') id |= 1 << i;
    if (cache[id] < dist) return;
    cache[id] = dist;
    if (id == target) sums[min = dist] += Checksum(moves, dist);
    if (id == target || dist >= max) return;
    if (dir != 'U' && to - w >= 0  ) Solve(a, dist, 'D', to, to-w);
    if (dir != 'L' && to % w != 0  ) Solve(a, dist, 'R', to, to-1);
    if (dir != 'R' && to % w != w-1) Solve(a, dist, 'L', to, to+1);
    if (dir != 'D' && to + w < size) Solve(a, dist, 'U', to, to+w);
  }

  static void Main()
  {
    Solve(".011001100110011".Select(x => (int)x).ToArray(), 0, 0, 0, 0);
    Console.WriteLine(sums[min]);
  }
}

