#include <stdio.h>

int sum5(int n)
{
  int sum = 0;
  for (; n > 0; n /= 10) {
    int r = n % 10, r2 = r * r;
    sum += r2 * r2 * r;
  }
  return sum;
}

int main(void)
{
  int sum = 0;
  for (int i = 2; i < 999999; i++)
  {
    if (sum5(i) != i) continue;
    sum += i;
    printf("%d ", i);
  }
  printf("[%d]\n", sum);
  return 0;
}
