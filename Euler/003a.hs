factors = flip fact 2
fact 1 _ = []
fact n k = let p = head [ x | x <- [k..], mod n x == 0 ] in p : fact (div n p) p
main = print $ {-last $-} factors 600851475143

