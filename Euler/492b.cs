﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

sealed class E492
{
  bool[] Sieve(int min, int max)
  {
    int limit = (int)Math.Sqrt(max);
    bool[] b = new bool[limit+1], c = new bool[max-min+1];
    for (int p = 2; p <= limit; p++)
    {
      if (b[p]) continue;
      for (int q = p + p; q <= limit; q += p) b[q] = true;
      for (int q = (int)Math.Ceiling((double)min/p)*p; q <= max; q += p) c[q-min] = true;
    }
    return c;
  }

  int[] GetPrimes(int min, int max)
  {
    var primes = new List<int>();
    var composite = Sieve(min, max);
    for (var i = 0; i < composite.Length; i++)
      if (!composite[i]) primes.Add(min+i);
    return primes.ToArray();
  }

  Tuple<int, List<int>>[] GetPrimeLists(int[] primes, int k)
  {
    var n = primes.Length / k;
    var t = new Tuple<int, List<int>>[n];
    for (var i = 0; i < n; i++) t[i] = Tuple.Create(i, new List<int>());
    for (var i = 0; i < primes.Length; i++)
      t[i % n].Item2.Add(primes[i]);
    return t;
  }

  Tuple<int,int> Compute(HashSet<int> set, long n, long p)
  {
    set.Clear();
    long a = 1;
    int i;
    for (i = 0; !set.Contains((int)a); i++)
    {
      if (i < 50) set.Add((int)a);
      a = ((6 * a + 10) * a + 3) % p;
    }
    int size = i;
    long b = a;
    a = 1;
    for (i = 0; b != a; i++) a = ((6 * a + 10) * a + 3) % p;
    int i0 = i, i1 = (int)((n - i) % (size - i));
    for (i = i1; i > 0; i--) a = ((6 * a + 10) * a + 3) % p;
    return Tuple.Create((int)a, i0);
  }

  long Compute(long n, Tuple<int, List<int>> take)
  {
    var dt1 = DateTime.Now;
    var set = new HashSet<int>();
    var max = 0;
    long sum = 0;
    foreach (long p in take.Item2)
    {
      var t = Compute(set, n, p);
      sum += t.Item1;
      if (max < t.Item2) max = t.Item2;
    }
    var dt2 = DateTime.Now;
    Console.WriteLine("{0:HH:mm:ss} {1,9:N3} {2,2} [{3:N0}...]({4}) {5,5} {6,2}",
      dt2, (dt2-dt1).TotalSeconds, max, (take.Item2.Count > 0) ? take.Item2[0] : 0, 
      take.Item2.Count, take.Item1, Thread.CurrentThread.ManagedThreadId);
    return sum;
  }

  long Compute(int a, int b, int k, long n)
  {
    n--;
    var primes = GetPrimes(a, b);
    var takes = GetPrimeLists(primes, k);
    Console.WriteLine("{0:HH:mm:ss} {1:N0} primes, {2:N0} takes",
      DateTime.Now, primes.Length, takes.Length);
    var list = new List<long>(takes.Length);
    lock (list) Parallel.ForEach(takes, take => { list.Add(Compute(n, take)); });
    var sum = list.Sum();
    Console.WriteLine("{0:HH:mm:ss} {1:N0} items, {2:N0}",
      DateTime.Now, list.Count, sum);
    return sum;
  }

  static void Main(string[] args)
  {
    int x = 1000000000, y = int.Parse(args[0]), k = int.Parse(args[1]);
    long n = 1000000000000000;
    var dt1 = DateTime.Now;
    Console.WriteLine("{0:HH:mm:ss} {1:N0} {2:N0}", dt1, y, k);
    var z = new E492().Compute(x, x + y, k, n);
    var dt2 = DateTime.Now;
    Console.WriteLine("{0:HH:mm:ss} {1} {2}", dt2, dt2-dt1, z);
  }
}
