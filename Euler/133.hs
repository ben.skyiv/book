import Math.NumberTheory.Powers ( powerMod )
import Math.NumberTheory.Primes.Sieve ( primes )

main = print $ sum [p | p <- takeWhile (<10^5) primes, 1 /= powerMod 10 (10^16::Int) (9*p)]

