import Data.Set(empty,insert)
import Data.List(foldl',sort)
import Math.NumberTheory.Primes(factorSieve,sieveFactor)
main = let n = 10^8; s = factorSieve n in print $
  length $ foldl' (\a x -> insert x a) empty $
  map (\n -> let xs = sort $ map snd $ sieveFactor s n
  in xs) [2..n]

