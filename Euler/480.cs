using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Skyiv.Utils;

sealed class E480
{
  static readonly string ls;
  static readonly int[] ns;
  static readonly Dictionary<char, int> cs;
  static readonly int max = 15;
  
  static E480()
  {
    var phrase = "thereisasyetinsufficientdataforameaningfulanswer";
    var query = phrase.ToArray().OrderBy(x => x).
      GroupBy(x => x, (x,xs) => new {Key = x, Count = xs.Count()});
    ls = new string(query.Select(x => x.Key).ToArray());
    ns = query.Select(x => x.Count).ToArray();
    cs = new Dictionary<char, int>();
    for (var i = 0; i < ls.Length; i++) cs.Add(ls[i], i);
  }

  static int[] DupAndDel(int[] ns, int letter)
  {
    ns = ns.Dup();
    --ns[letter];
    if (ns[letter] < 0) return null;
    return ns;
  }
  
  static int[] W2A(string word)
  {
    var a = new int[word.Length];
    for (var i = 0; i < a.Length; i++) a[i] = cs[word[i]];
    return a;
  }
  
  static int[] P2A(long position)
  {
    var list = new List<int>();
    var ns2 = ns;
    for (int i, length = max; position > 0; length--)
    {
      long pos = 0;
      for (i = ns2.Length - 1; i >= 0; i--)
        if (ns2[i] > 0 && (pos = P(i, length, ns2)) <= position)
          break;
      ns2 = DupAndDel(ns2, i);
      position -= pos;
      list.Add(i);
    }
    return list.ToArray();
  }

  static long P(int letter, int length, int[] ns)
  { // return: number of word before letter
    long sum = 1;
    for (var i = 0; i < letter; i++)
    {
      var ns2 = DupAndDel(ns, i);
      if (ns2 != null) sum += ns2.PermutationFrom0To(length - 1).Sum();
    }
    return sum;
  }
  
  static long P(int[] word, int length, int[] ns)
  {
    if (word.Length == 0) return 0;
    return P(word[0], length, ns) + P(word.Tail(), length-1, DupAndDel(ns, word[0]));
  }
  
  static long P(string word)
  {
    return P(W2A(word), max, ns);
  }
  
  static string W(long position)
  {
    var sb = new StringBuilder();
    foreach (var i in P2A(position)) sb.Append(ls[i]);
    return sb.ToString();
  }

  static void Main()
  {
    Console.WriteLine(W(P("legionary") + P("calorimeters")
      - P("annihilate") + P("orchestrated") - P("fluttering")));
  }
}

