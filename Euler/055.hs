main = print $ length [x | x <- [1..10^4], f 50 x]
rev = read . reverse . show; f 0 _ = True
f n x | z == rev z = False | True = f (n-1) z where z = x + rev x

