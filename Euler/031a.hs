main = print $ ways [200,100,50,20,10,5,2,1] 200
ways _ 0 = 1
ways [_] _ = 1
ways (x:xs) n = sum $ map (ways xs . (n-)) [0,x..n]

