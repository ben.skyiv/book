import Math.NumberTheory.Primes.Testing ( isPrime )

main = print $ head $ dropWhile (\n -> isPrime n || any
  isPrime (takeWhile (>0) [n - 2*i*i | i <- [1..]])) [3,5..]

