import Data.Bits ( (.|.),(.&.),bit,shiftL )
import Data.Array as Array ( (!),array,range )
import Data.Map as Map ( (!),assocs,singleton,unionWith )

f r c = if r < c then f c r else ans Array.! (r,0,0) where
  full = bit c - 1 :: Int
  triominoes = [[1,1,1],[7,0,0] ,[3,1,0],[3,2,0],[1,3,0],[2,3,0]]
  fillOne = foldl go (toMap [0,0,0] 1) [0..c-1]
  toMap [r0,r1,r2] cnt = singleton r0 $ singleton (r1,r2) cnt
  go cache i = foldl (unionWith (unionWith (+))) cache $
    [toMap ts cnt | (r0,m) <- assocs cache, ((r1,r2),cnt) <- assocs m,
    tri <- map (map (`shiftL` i)) triominoes, canPut tri [r0,r1,r2],
    let ts = zipWith (.|.) tri [r0,r1,r2], all (<=full) ts]
  canPut t1 t2 = and $ zipWith ok t1 t2
  ok r1 r2 = r1 .&. r2 == 0
  bnd = ((0,0,0), (r,full,full))
  ans = array bnd [(i,fillAll i) | i <- range bnd]
  fillAll (0,0,0)   = 1
  fillAll (0,_,_)   = 0
  fillAll (r,r0,r1) = sum [cnt * ans Array.! (r-1, nr1 .|. r1, nr2) |
    ((nr1,nr2),cnt) <- assocs $ fillOne Map.! (full-r0), ok nr1 r1]

main = print $ f 9 12

