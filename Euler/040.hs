import Data.Char ( digitToInt )

main = print $ product $ map (digitToInt . ((concatMap show [0..])!!) . (10^)) [0..6]

