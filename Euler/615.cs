using System;
using System.IO;
using System.Collections.Generic;
using Skyiv.Utils;

sealed class E615Worker
{
  static readonly string fmt = "{0:yyyy-MM-dd HH:mm:ss}: {1,2} {2}";
  readonly int n;
  readonly int[] primes;
  readonly SortedSet<double> ns;
  int total;  // only for Make
  long limit; // only for Make

  public E615Worker(int n)
  {
    ns = new SortedSet<double>();
    primes = Primes.GetPrimes(0, this.n = n);
  }

  void Make(int items)
  {
    total = items;
    limit = (1L << (total - 1)) * n;
    Make(items, 1, 1);
  }

  void Make(int curr, long prod, int idx)
  {
    if (curr == 1) {
      for (var i = idx; i < primes.Length; i++) {
        var z = prod * primes[i];
        if (z > limit) return;
        double z1 = z;
        for (var k = 1; k < total; k++)
          if ((z1 /= 2.0) < n)
            if (!ns.Add(z1)) Console.Write("[{0}]", z1);
      }
    }
    else
      for (var i = idx; i < primes.Length; i++) {
        var z = prod * primes[i];
        if (z > limit) return;
        Make(curr - 1, z, i);
      }
  }

  public void Run()
  {
    for (int count = ns.Count, i = 2; ; i++, count = ns.Count) {
      Make(i);
      Console.WriteLine(fmt, DateTime.Now, i, ns.Count - count);
      if (count + 2 >= ns.Count) break;
    }
    Console.WriteLine(fmt, DateTime.Now, ns.Count, "Step1");
    ns.Add(double.MaxValue);
    var it = ns.GetEnumerator();
    it.MoveNext(); double j = it.Current;
    using (var sw = new StreamWriter("/home/ben/tmp/615.txt"))
      for (int i = 2; i <= n; )
        if      (i == j) throw new Exception("[==]");
        else if (i <  j) sw.WriteLine(i++);
        else { sw.WriteLine(j); it.MoveNext(); j = it.Current; }
    Console.WriteLine(fmt, DateTime.Now, "Finish", "");
  }
}

sealed class E615
{
  static void Main()
  { // answer: 2^(n-1) * (n-th number)
    new E615Worker(200000).Run();
  }
}

