import Data.Digits ( digitsRev )

bouncy n = let xs = digitsRev 10 n; ts = zip xs $ tail xs
  in any (\(a,b) -> a > b) ts && any (\(a,b) -> a < b) ts
main = print $ snd $ until (\(b,a) -> b*100 >= a*99)
  (\(b,a) -> (if bouncy (a+1) then b+1 else b, a+1)) (0,1)

