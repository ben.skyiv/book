// mcs 660b15.cs 660.cs && ./660b15.exe

using System;

static class E660b15
{
  static E660 e = new E660(15);

  static int P555()
  { // (1xxxx, 2xxxx, xxxxx) (1xxxx, Exxxx, xxxxx)
    var z = 0;
    for (var i1 = 1; i1 <= e.B / 2; i1++)
      for (var i2 = i1+1; i2 <= e.B - i1; i2++)
        foreach (var s in E660.Permutation(e.GetRest(i1, i2), 8))
          z += e.F(e.B*(e.B*(e.B*(e.B*i1+s[1])+s[2])+s[3])+s[4],
                   e.B*(e.B*(e.B*(e.B*i2+s[5])+s[6])+s[7])+s[8]);
    return z;
  }

  static int P456()
  { // (xxxx, Exxxx, 10xxxx)
    var z = 0;
    foreach (var s in E660.Permutation(e.GetRest(0, 1, e.B-1), 8))
      z += e.F(      e.B*(e.B*(e.B*s[1] +s[2])+s[3])+s[4],
        e.B*(e.B*(e.B*(e.B*(e.B-1)+s[5])+s[6])+s[7])+s[8]);
    return z;
  }

  static void Main() { Console.WriteLine(P555()+P456()); }
}

