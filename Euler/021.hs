-- http://en.wikipedia.org/wiki/Amicable_numbers
-- main = print $ sum [ 220, 284, 1184, 1210, 2620, 2924, 5020, 5564, 6232, 6368 ]

import Math.NumberTheory.Primes.Factorisation ( divisorSum )

main = let n = 10^4 in print $ sum [a+b | a <- [1..n], let b = divisorSum a - a, a > b && b > 0 && divisorSum b - b == a]

