using System;
using System.Collections.Generic;

sealed class E022
{
  static void Main()
  {
    var list = new List<string>();
    foreach (var name in Console.ReadLine().Split(','))
      list.Add(name.Trim('"'));
    list.Sort();
    long idx = 0, sum = 0;
    foreach (var name in list)
    {
      var score = 0;
      foreach (var c in name) score += c - 'A' + 1;
      sum += ++idx * score;
    }
    Console.WriteLine(sum);
  }
}
