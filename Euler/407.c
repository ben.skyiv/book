#include <stdio.h>

int inv(int a, int m)
{
  int x0 = 1;
  for (int c, q, x, x1 = 0, b = m; b; x0 = x1, x1 = x)
    c = a - (q = a/b) * b, a = b, b = c, x = x0 - q*x1;
  return (x0 < 0) ? (x0+m) : x0;
}

int main(void)
{
  const int N = 10000000 + 1;
  static int minp[N];
  long z = 0;
  for (int i = 2; i < N; i++) {
    if (!minp[i])
      for (int j = i; j < N; j += i)
        if (!minp[j]) minp[j] = i;
    int pw[10], n = 0, maxa = 0;
    for (int q = 1, x = i; x > 1; pw[n++] = q, q = 1)
      for (int p = minp[x]; x % p == 0; x /= p)
        q *= p;
    for (int mask = 0; mask < (1<<n); mask++) {
      int a, m1 = 1, m2 = 1;
      for (int k = 0; k < n; k++)
        if (mask & 1<<k) m1 *= pw[k];
        else m2 *= pw[k];
      if (maxa < (a = inv(m1, m2) * m1)) maxa = a;
    }
    z += maxa;
  }
  printf("%ld\n", z);
  return 0;
}

