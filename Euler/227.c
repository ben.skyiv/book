#include <stdio.h>
#include <stdlib.h>

int main()
{
  const int n = 100, m = 70000;
  static double a[n], b[] = {1.0/36,2.0/9,0.5,2.0/9,1.0/36}, d = 1;
  for (int k = 0; k < m; k++)
    for (int i = 1; i <= n / 2; a[i++] = d, d = 1)
      for (int z, j = 0; j < 5; j++)
        d += b[j] * a[z = abs(i + j - 2), (z > n/2) ? (n - z) : z];
  printf("%.6f\n", a[n/2]);
}

