using System;

sealed class E539
{
  int[] Remove(int[] a, bool left2right)
  {
    var n = a.Length;
    var b = new int[n/2];
    var z = (!left2right && n % 2 == 0) ? 0 : 1;
    for (var i = 0; i < b.Length; i++) b[i] = a[2*i+z];
    return b;
  }
  
  int P(int n)
  {
    var a = new int[n];
    for (var i = 0; i < n; i++) a[i] = i+1;
    for (var b = true; a.Length > 1; b = !b)
      a = Remove(a, b);
Console.Write("{0} ", a[0]/2);
    return a[0];
  }

  void S(int a, int b)
  {
    for (var i = a; i <= b; i += 2) P(i);
  }

  static void Main()
  {
    new E539().S(1002, 2000);
  }
}

