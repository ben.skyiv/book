#include <stdio.h>

static double x[31][25][25];
static int a[] = {1,2,4,8,16}, b[31][25];

void inverse(int s, int i)
{
  double w, z = x[s][i][i] = -(b[s][i] = 1) / x[s][i][i];
  for (int j = 0; j < 25; j++) {
    if (j == i || (w = x[s][i][j] * z) == 0) continue;
    for (int k = 0; k < 25; k++) if (k != i) x[s][k][j] += w * x[s][k][i];
    x[s][i][j] = w;
  }
  for (int k = 0; k < 25; k++) if (k != i) x[s][k][i] *= z;
}

void copy(int r, int s)
{
  for (int i = 0; i < 25; b[s][i] = b[r][i], i++)
    for (int j = 0; j < 25; j++) x[s][i][j] = x[r][i][j];
}

void tran(double *pz, int s, int d, int r, double y)
{
  for (int k = 0; k < 25; k++) if (b[d][k]) *pz += y * x[d][r][k];
  if (s == 31) return;
  for (int k = 0; k < 5; k++) if (!b[d][k+20]) tran(pz,d+a[k],s,k,y*x[d][r][k+20]);
}

void initialize(void)
{
  double f[] = {2,3,3,3,2,3,4,4,4,3,3,4,4,4,3,3,4,4,4,3,2,3,3,3,2};
  for (int i = 0; i < 25; i++) {
    double z = 1 / f[i];
    if (i-5 >= 0) x[0][i][i-5] = z;
    if (i+5 < 25) x[0][i][i+5] = z;
    if (i%5 >  0) x[0][i][i-1] = z;
    if (i%5 <  4) x[0][i][i+1] = z;
    x[0][i][i] = -1; b[0][i] = 0;
  }
  for (int i = 0; i < 20; i++) inverse(0, i);
  for (int j = 1, s = 20, k = 1; k < 31; k++, j = 1, s = 20) {
    for (int i = k; (i & 1) == 0; i >>= 1, j <<= 1) s++;
    copy(k-j, k); inverse(k, s);
  }
}

int main(void)
{
  initialize();
  double z = 0;
  for (int i = 0; i < 20; i++) z += x[0][12][i];
  for (int i = 0; i < 5; i++) tran(&z, a[i], 0, i, x[0][12][20+i]);
  printf("%lf\n", z) ;
}

