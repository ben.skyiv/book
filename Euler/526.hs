import Math.NumberTheory.Primes.Testing ( isPrime )
import Math.NumberTheory.Primes.Factorisation ( factorise )

-- (49 204 481 123 927 705, 9 999 967 315 458 641):  311m,   10^5
-- (49 235 690 753 201 211, 9 999 914 618 550 671): 1237m, 4*10^5
-- (49 235 690 753 201 211, 9 999 914 618 550 671), 1236m, 4*10^5, 9 999 737 409 657 551
-- (49 580 620 131 241 271, 9 999 452 799 100 481), 4116m,14*10^5, 9 999 116 036 873 651
n = 9999737409657551; m = 14*10^5; z = 11 + 30 * div n 30
main = print $ (maximum xs, snd $ last xs) where
  xs = map (\x -> (sum $ map (fst .last . factorise) [x..x+8], x))
    $ take m $ filter (\x -> all isPrime [x,x+2,x+6,x+8]) [z,z-30..1]

