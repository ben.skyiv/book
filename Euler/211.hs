import Math.NumberTheory.Powers.Squares
import Math.NumberTheory.Primes.Factorisation
main = print $ sum [ n | n <- [1..64000-1], isSquare' $ σ 2 n ]

