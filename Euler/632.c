#include <stdio.h>

const int c9 = 7+(int)1e8; // c9 is prime number
const long n = (long)1e16; // $ ulimit -s 819200
const int k9 = 8; // 2*3*5*7*11*13*17*19*23 = 223,092,870
static long p2s[5761460];  // π(1e8) = 5,761,455
static long z = 0;
static int k0 = 0;

void c1(int k, int isOdd, int i, long x)
{
  if (k > k0) return;
  if (x > n / p2s[i]) return;
  long m = x * p2s[i];
  z += (n / m) * (isOdd ? 1 : -1);
  c1(k  ,  isOdd, i+1, x);
  c1(k+1, !isOdd, i+1, m);
}

long c2(int k) { z=0; k0=k; c1(1,1,0,1); return z; }

int main(void)
{
  static char cs[c9+1];
  for (int i = 2; i * i <= c9; i++)
    if (!cs[i]) for (int j = i * i; j <= c9; j += i) cs[j] = 1;
  for (int i = 2, j = 0; i <= c9; i++) if (!cs[i]) p2s[j++] = (long)i*i;
  for (int i = 1; i <= k9; i++)
    printf("a%d=%ld\n", i, c2(i));
}
