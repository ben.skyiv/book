#include <stdio.h>

int main(void)
{
  long a[] = { 1,1,1,2 }, z = 5, n = 1000000000;
  for (long m = 6, k = 2, i = 0; k < n; i++) {
    if (i >= 4) a[i&3] = a[(i-1)&3] + a[(i-4)&3];
    if (a[i&3] > n - k) z += m * (n - k), k = n;
    else z += m++ * a[i&3], k += a[i&3];
  }
  printf("%ld\n", z);
  return 0;
}

