-- main = print $ maximum [ n | x <- [100..999], y <- [x..999], let n = x*y, (\x -> x == reverse x) $ show n ]
main = print $ head [ n | x <- [9,8..1], y <- [9,8..0], z <- [9,8..0], n <- [100001*x + 10010*y + 1100*z], not $ null [ m | m <- [100..999], mod n m == 0 && div n m < 1000 ] ]

