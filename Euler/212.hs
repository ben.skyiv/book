main = let n = 50000 in print $ volume $ take n $ cubs fib where
  fib = [mod (100003-200003*x+300007*x^3) $ 10^6 | x <- [1..55]]
    ++ zipWith (\x y -> mod (x+y) $ 10^6) fib (drop 31 fib)
  cubs (x:y:z:u:v:w:s) = let a = map (`mod`10000) [x,y,z] in
    (a ++ (zipWith (+) a $ map ((+1).(`mod`399)) [u,v,w])) : cubs s
  volume [] = 0
  volume ([x0,y0,z0,x1,y1,z1]:cs0) = (x1-x0)*(y1-y0)*(z1-z0) +
    sum (map volume [ds1, ds2, ds3, ds4, ds5, ds6]) where
      (cs1, ds1) = split 0 x0 cs0; (ds4, cs4) = split 0 x1 cs3
      (cs2, ds2) = split 1 y0 cs1; (ds5, cs5) = split 1 y1 cs4
      (cs3, ds3) = split 2 z0 cs2; (ds6, _  ) = split 2 z1 cs5
      split i v s = f [] [] s where
        f a b [] = (a, b)
        f a b (c:cs)
          | v <= c!!i     = f (c:a) b cs
          | v >= c!!(i+3) = f a (c:b) cs
          | otherwise = f ((take i c ++ v : drop (i+1) c) : a)
            ((take (i+3) c ++ v : drop (i+4) c) : b) cs

