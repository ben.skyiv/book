import Data.List ( tails,sort,subsequences,sortBy,groupBy)
import Data.Ratio ( (%),denominator )
import Data.Ord ( comparing )
import Data.Function ( on )

invSq n = 1 % (n * n)
seek x y = f x $ zip3 y (map invSq y) (map (sum . map invSq) $ init $ tails y) where
  f 0 _ = [[]]
  f x ((n,r,s):z) | r>x = f x z | s<x = [] | otherwise = map (n:) (f (x-r) z) ++ f x z
  f _ _ = []
main = print $ length [u ++ v | (x,s) <- foldl (\a p -> (map (\z -> (fst $ head z,
  map snd z)) . groupBy (on (==) fst) . sortBy (comparing fst)) $ [(y, u ++ v) |
  (x,s) <- a, (y,v) <- [(y,t) | t <- subsequences [n*p | n <- [1 .. div 80 p], all
  ((/=0) . mod n) $ 11 : takeWhile (>= p) [13,7,5]], let y = x + (sum . map invSq) t,
  mod (denominator y) p /= 0], u <- s]) [(0,[[]])] [13,7,5], u <- seek (1%2 - x)
  [n | a <- [0..4], b <- [0..2], let n = 2^a * 3^b, n <= 80 ], v <- s]

