import Skyiv.RomanNumerals ( fromRoman,toRoman )
main = do
  s <- readFile "089.txt"; let ss = lines s
  print $ (length $ concat ss) - (length $ concatMap (toRoman . fromRoman) ss)

