from sympy import *
n = 20; x,y,z,w = symbols('x y z w')
e1 = -sqrt(x)+sqrt(y)+sqrt(z)+sqrt(w)
for a in range(1,n-3):
  for b in range(a+1,n-2):
    for c in range(b+1,n-1):
      for d in range(c+1,n):
        i1 = e1.subs({x:a,y:b,z:c,w:d})
        i2 = expand(i1**2)
        print("%d %d %d %d:%s:%s" % (a, b, c, d, i1, i2))

