n, low, high, k, mod = 70, 10**20, 10**60, 13, 10**9+7

# Factorize n!
factorization = {}
for i in range(2, n+1):
  i2 = i
  for j in range(2, i):
    while i2 % j == 0:
      i2 = i2 // j
      factorization[j] += 1
  if i2 == i: factorization[i] = 1

ls = [1]  # The divisors with only factors < k
hs = [1]  # The divisors with only factors >= k
for p in factorization:
  e = factorization[p]            # Exponent of this p
  pp = 1                          # Current (-p)^i
  if p >= k: l, ds = hs, hs[:]    # Select the right list to modify
  else: l, ds = ls, ls[:]
  for i in range(1, e+1):
    pp *= -p
    for d in ds:
      if abs(d*pp) <= high: l.append(d*pp)

hs.append(high+1)
list.sort(ls, key=abs) # length 3'204'364 for k=13
list.sort(hs, key=abs) # length 1'105'920 for k=13

lowsum,  il = 0, 0 # sum over index [0, i) of ls such that a*x <  low
highsum, ih = 0, 0 # sum over index [0, i) of ls such that a*x <= high
ans = 0
for a in reversed(ls):
  while abs(a*hs[il]) <  low:  lowsum,  il = lowsum +hs[il], il+1
  while abs(a*hs[ih]) <= high: highsum, ih = highsum+hs[ih], ih+1
  ans += a*(highsum-lowsum)

print(ans%mod)

