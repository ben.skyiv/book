using System;
using Skyiv.Utils;

static class E578
{
  static int n2;
  static int[] primes;
  
  static int Pi(long n)
  {
    var i = Array.BinarySearch(primes, (int)n);
    return (i >= 0) ? (i + 1) : ~i;
  }

  static long C(long n, int i, int e)
  {
    long p = primes[i], z = 0;
    if (p > n) return 1;
    if (p * p > n) return Pi(n) - i + 1; // Pi(p-1) == i
    for (int j = 0; j <= e && n >= 1; ++j, n /= p)
      z += C(n, i + 1, (j != 0) ? j : e);
    return z;
  }

  static void Main()
  {
    long n = 1000000; // n < 2^64
    n2 = 100 + (int)n;
    primes = Primes.GetPrimes(0, n2);
    Console.WriteLine("π({0:N0}) = {1:N0}", n2, primes.Length);
    Console.WriteLine("C({0:N0}) = {1:N0}", n, C(n, 0, 64));
  }
}

