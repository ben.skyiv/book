using System;
using System.Collections.Generic;

sealed class E492
{
  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }

  int[] GetPrimes(int min, int max)
  {
    var primes = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i] && i >= min) primes.Add(i);
    return primes.ToArray();
  }
  
  int Compute(HashSet<int> set, long n, long p)
  {
    set.Clear();
    long a = 1;
    int i;
    for (i = 0; !set.Contains((int)a); i++)
    {
      if (i < 100) set.Add((int)a);
      a = ((6*a + 10) * a + 3) % p;
    }
    int size = i;
    long b = a;
    a = 1;
    for (i = 0; b != a; i++) a = ((6*a + 10) * a + 3) % p;
    int i0 = i, i1 = (int)((n - i) % (size - i));
    for (i = i1; i > 0; i--) a = ((6*a + 10) * a + 3) % p;
    Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss} {1:N0} {2,11:N0} {3,3}"
      + " {4,11:N0} {5,11:N0}", DateTime.Now, p, size, i0, i1, a);
    return (int)a;
  }

  long Compute(int a, int b, long n)
  {
    n--;
    long sum = 0;
    var set = new HashSet<int>();
    foreach (var p in GetPrimes(a, b)) sum += Compute(set, n, p);
    return sum;
  }

  static void Main(string[] args)
  {
    int step = 1000 * int.Parse(args[0]), k = int.Parse(args[1]);
    int start = 1000000000, a = start + k * step, b = a + step;
    long n = 1000000000000000;
    Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss} {1:D2} {2:N0} {3:N0}  {4:N0}",
      DateTime.Now, k, a, b, n);
    var z = new E492().Compute(a, b, n);
    Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss} {1,13}", DateTime.Now, z);
  }
}

