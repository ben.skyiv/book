#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static double a;

long G(double x)
{
  if (x < a ) return 1;
  return G(x - 1) + G(x - a);
}

int main(int argc, char * argv[])
{
  int n = (argc > 1) ? atoi(argv[1]) : 90;
  a = sqrt(n);
  printf("G(%d) = %ld\n", n, G(n));
  return 0;
}

