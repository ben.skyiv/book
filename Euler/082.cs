using System;
using System.IO;
using System.Linq;

sealed class E082
{
  static readonly int size = 80;

  int Compute()
  {
    var g = Read();
    var z = new int[size];
    for (var i = 0; i < size; i++) z[i] = g[i, size-1];
    for (int j, i = size - 2; i >= 0; i--)
    {
      z[0] += g[0, i];
      for (j = 1; j < size; j++) z[j] = Math.Min(z[j-1]+g[j,i], z[j]+g[j,i]);
      for (j = size - 2; j >= 0; j--) z[j] = Math.Min(z[j], z[j+1]+g[j,i]);
    }
    return z.Min();
  }

  int[,] Read()
  {
    var i = 0;
    var grid = new int[size, size];
    foreach (var line in File.ReadLines("081.txt"))
    {
      var fields = line.Split(',');
      for (var j = 0; j < fields.Length; j++)
        grid[i, j] = int.Parse(fields[j]);
      i++;
    }
    return grid;
  }

  static void Main()
  {
    Console.WriteLine(new E082().Compute());
  }
}

