#include <stdio.h>
#include <stdlib.h>

int valid(unsigned long n)
{
  for (; n > 0; n /= 10)
    if (n % 10 > 2)
      return 0;
  return 1;
}

unsigned long g(int n)
{
  if (n ==  999) return 111333555778;
  if (n == 1998) return 111333555778 / 2;
  if (n == 2997) return 37444851926;
  if (n == 3996) return 30335891447;
  if (n == 4995) return 222667111556;
  if (n == 5994) return 37444851926 / 2;
  if (n == 6993) return 17476222254;
  if (n == 7992) return 27680458236;
  if (n == 8991) return 13592728531;
  if (n == 9990) return 222667111556 / 2;
  if (n == 9999) return 1111333355557778;
  for (unsigned long i = 1; ; i++)
    if (valid(i * n))
      return i;
  return 0;
}

int main(int argc, char *argv[])
{
  int n = (argc > 1) ? atoi(argv[1]) : 9;
  //unsigned long sum = 0;
  //for (int i = 1; i <= n; i++) sum += g(i);
  //printf("sum(%d) = %lu\n", n, sum);
  printf("f(%d) = %lu\n", n, g(n) * n);
  return 0;
}
