#include <stdio.h>

int main(void)
{
  const int u = 100000000, k = 100000;
  static int d[u+1];
  for (int i = 1; i <= u; i++)
    for (int j = i; j <= u; j += i)
      d[j]++;
  long sum = 0;
  for (int max = 0, i = 1; i <= u - k + 1; i++) {
    if (max == d[i-1]) {
      max = 0;
      for (int j = i; j < i + k; j++)
        if (max < d[j]) max = d[j];
    }
    else if (max < d[i+k-1]) max = d[i+k-1];
    sum += max;
  }
  printf("%ld\n", sum);
  return 0;
}

