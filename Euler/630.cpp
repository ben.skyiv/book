#include <bits/stdc++.h>

using namespace std;

struct Point
{
  int x, y;
  Point(){}
  Point(int _x, int _y):x(_x), y(_y) {}
  Point operator +(const Point&a) const {return Point(x+a.x, y+a.y);}
  Point operator -(const Point&a) const {return Point(x-a.x, y-a.y);}
  long dot(const Point&a) const {return (long)x*a.x + (long)y*a.y;}
  long crs(const Point&a) const {return (long)x*a.y - (long)y*a.x;}
  Point rot90() const {return Point(-y,x);} 
  bool operator < (const Point&a) const
  { return x < a.x || x == a.x && y < a.y; }
};

struct Line
{
  Point x, y;
  Line() {}
  Line(Point _x, Point _y):x(_x), y(_y) {}
  Point vec() const {return y-x;}
};

bool cmp(Line x, Line y)
{
	Point vx = x.vec(), vy = y.vec();
	long c = vx.crs(vy);
	if (c != 0) return c < 0;
	return x.x.dot(vx.rot90()) < y.x.dot(vx.rot90());
}

bool eq(Line x, Line y)
{
	Point vx = x.vec(), vy = y.vec();
	if (vx.crs(vy) != 0) return false;
	return x.x.dot(vx.rot90()) == y.x.dot(vx.rot90());
}

int main()
{
  const int n = 2500;
  static Point pts[n];
  static Line lines[n*n];
	for (int z = 290797, i = 0; i < n; i++) {
		z = (long)z * z % 50515093; int x = z % 2000 - 1000;
		z = (long)z * z % 50515093; int y = z % 2000 - 1000;
		pts[i] = Point(x, y);
  }
	sort(pts, pts+n);
  int m = 0;
	for (int i = 0; i < n; i ++)
		for (int j = i+1; j < n; j ++)
			lines[m++] = Line(pts[i], pts[j]);
	sort(lines, lines+m, cmp);
	m = unique(lines, lines+m, eq) - lines;
	long v = (long)m*(m-1);
	for (int i = 0, j = 0; i < m; i = j) {
		while (j < m && lines[i].vec().crs(lines[j].vec()) == 0) j++;
		v -= (long)(j-i)*(j-i-1);
	}
	cout << v << endl;
	return 0;
}

