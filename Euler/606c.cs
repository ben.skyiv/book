using System;
using Skyiv.Utils;

static class E606
{
  static readonly int m = (int)1e9;

  static int[] GetArray()
  {
    var n = (int)1e6;
    var ps = Primes.GetPrimes(0, n);
    var a1 = new int[ps.Length];
    a1[0] = ps[0] * ps[0] * ps[0];
    for (var i = 1; i < ps.Length; i++)
      a1[i] = (int)((a1[i-1] + (long)ps[i]*ps[i]*ps[i]) % m);
    var a = new int[n + 1];
    for (var i = 0; i < ps.Length; i++) a[ps[i]] = a1[i];
    for (var i = 2; i <= n; i++) if (a[i] == 0) a[i] = a[i-1];
    return a;
  }

  static void Main()
  {
    long n = (long)1e9, z = 0;
    for (var a = GetArray(); ; ) {
      var s = Console.ReadLine();
      if (s == null) break;
      long q = int.Parse(s);
      int p = (int)Math.Min(q - 1, n / q);
      z = (z + q*q%m*q%m*a[p]) % m;
    }
    Console.WriteLine(z);
  }
}

