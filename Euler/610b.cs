using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class E610
{
  static readonly HashSet<string> rs = new HashSet<string>();
  static E610() { for (var i = 999; i >= 0; i--) rs.Add(i.ToRoman()); }
  static double P(char c) { return (c == '#') ? 0.02 : 0.14; }
  static void Main() { Console.WriteLine(Math.Round(140/.86 + E(""), 8)); }

  static double E(string s)
  {
    var v = ""; double p = 0, z = 0;
    foreach (var c in "#IVXLCDM")
      if (c == '#' || rs.Contains(s + c)) { p += P(c); v += c; }
    foreach (var c in v) z += P(c)/p * (c == '#' ? s.FromRoman() : E(s+c));
    return z;
  }
}

