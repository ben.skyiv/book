using System;
using System.Linq;
using System.Collections.Generic;

static class E583
{
  static int Gcd(int a, int b)
  { for (int t; b != 0; a = b, b = t) t = a % b; return a; }

  static Dictionary<int,int[]> GetTs(int max)
  {
    var set = new SortedSet<Tuple<int,int,int>>();
    for (var a = 2; a <= (int)Math.Sqrt(max); a++)
      for (var b = 1; b < a; b++) {
        if ((1 & (a + b)) == 0 || Gcd(a, b) != 1) continue;
        long u = (long)a * a, v = (long)b * b, z = u + v;
        if (z > max) continue;
        int x = (a*b) << 1, y = (int)(u - v);
        set.Add(Tuple.Create(x, y, (int)z));
        set.Add(Tuple.Create(y, x, (int)z));
      }
    var dict = new Dictionary<int,HashSet<int>>();
    foreach (var t in set)
      for (var i = 1; ; i++) {
        if (t.Item3 * i > max) break;
        int x = t.Item1 * i; HashSet<int> v;
        if (!dict.TryGetValue(x, out v))
          dict.Add(x, v = new HashSet<int>());
        v.Add(t.Item2 * i);
      }
    var ts = new Dictionary<int,int[]>();
    foreach (var kvp in dict) {
      var a = kvp.Value.ToArray();
      Array.Sort(a);
      ts.Add(kvp.Key, a);
    }
    return ts;
  }

  static void Main()
  {
    int n = (int)1e7; long z = 0;
    var ts = GetTs(n / 2);
    foreach (var kvp in ts) {
      if (kvp.Value.Length < 2) continue;
      var w = kvp.Key;
      int[] vs, us = kvp.Value;
      if (!ts.TryGetValue(w + w, out vs)) continue;
      foreach (var g in us) {
        int f = (int)Math.Sqrt((long)g*g+(long)w*w);
        int u, h, i = Array.BinarySearch(vs, g);
        for (var j = (i >= 0) ? i : ~i; j < vs.Length; j++)
          if ((u = (w + (h = vs[j]) + f) << 1) <= n &&
            Array.BinarySearch(us, h + g) >= 0) z += u;
      }
    }
    Console.WriteLine(z);
  }
} // 28.5s + 30.6s + 0.6s = 59.7s

