// mcs 660b11.cs 660.cs && ./660b11.exe

using System;

static class E660b11
{
  static E660 e = new E660(11);

  static int P344()
  { // (xxx, 1xxx, 2xxx) (xxx, 9xxx, Axxx)
    var z = 0;
    for (var i1 = 1; i1 <= e.B - 2; i1++)
      foreach (var s in E660.Permutation(e.GetRest(i1, i1+1), 6))
        z += e.F( e.B*(e.B*s[1] +s[2])+s[3],
          e.B*(e.B*(e.B*i1+s[4])+s[5])+s[6]);
    return z;
  }

  static void Main() { Console.WriteLine(P344()); }
}

