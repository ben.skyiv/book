import Math.NumberTheory.Powers ( powerMod )

main = let m = 97 in print $ map (\x -> powerMod x (-1::Int) m) [1..m-1]

