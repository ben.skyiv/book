import Data.Ratio ( (%),numerator )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )
import Math.Combinat.Numbers ( bernoulli,binomial )

f k n = numerator $ (sum $ map (\i -> bernoulli i * (binomial (k+1) i % 1)
  * ((n+1)^(k+1-i) % 1)) [0..k]) * (1 % (k+1))
s k n = (n+1) * f k n - f (k+1) n

main = let k = 10^4; n = 10^12; m = 2*10^9; b = m + 2000; a = s k n
  in print $ sum $ map (mod a) $ takeWhile (<b) $ sieveFrom m

