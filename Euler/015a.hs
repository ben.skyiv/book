import qualified Data.Map

primeCache = Data.Map.empty

storeCache n m cache = Data.Map.insert n m cache

readCache n cache = cache Data.Map.! n

checkCache n cache = Data.Map.member n cache

gridCache addr@(x,y) (maxx,maxy) cache
    | and [ x < maxx, y < maxy ] = 
        if checkCache addr cache then
            (readCache addr cache, cache)
        else
            let (s1,c1) = gridCache (x+1,y) (maxx,maxy) cache
                (s2,c2) = gridCache (x,y+1) (maxx,maxy) c1
            in
              cachePaths $! ((s1+s2),c2)
    | otherwise = (1, cache)
    where cachePaths (paths,cache') = (paths, storeCache addr paths cache')

main = print $ fst $ gridCache (0,0) (20,20) primeCache

