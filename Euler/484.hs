import Math.NumberTheory.Primes.Sieve ( primes )

main = let n = 5*10^15 in print $ pred $ f n $ map (\p ->
  let gs = scanl (*) 1 $ cycle $ replicate (p-2) p ++ [p*p,1] in
  (p, zipWith (-) (tail gs) gs)) $ map fromIntegral primes where
  f n ((p,ds):xs) = let q = p*p in if q > n then n else
    (+) (f n xs) $! sum $ zipWith (\n d -> d * f n xs)
    (takeWhile (>0) $ iterate (`div` p) $ div n q) ds

