-- http://oeis.org/A018806  f(n) ~ 6n²ln(n)/π²
f n = sum [gcd i j | j <- [1..n], i <- [1..n]]
main = let n = 20 in print [f i | i <- [1..n]]
-- g(n) = (f(n) + n(n+1)/2) / 2

