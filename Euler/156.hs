import Data.Array ( (!),listArray )

main = print $ sum $ map (f 0 11 0 1 (-1)) [1..9] where
  p = listArray (0,13) $ 0 : map (10^) [0..12] 
  f v t z a b d
    | t == 0 = if b == 0 then v + z - 1 else v
    | b > x && w > k*m + 10*x || b < -y && w < k*m - 10*y = v
    | otherwise = let c = a - 1; g x = b + a*x*p!t - k*x*p!k in
      sum [            f v k (10*z+d) c (g d)       d,
      sum $ map (\x -> f v k (10*z+x) a (g x)       d) [0..d-1],
      sum $ map (\x -> f v k (10*z+x) a (g x - p!t) d) [d+1..9]]
    where k = t-1; m = p!(t+1); x = div m 9; y = div m 90; w = 10*(a*m+b)

