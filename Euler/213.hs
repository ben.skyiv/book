import Data.Array ( accumArray,assocs,elems )
import Data.List ( genericLength )
import Text.Printf ( printf )

size = 30; turns = 50; r = [0 .. size -1]
ix x y = if x >= 0 && y >= 0 && x < size && y < size then x + y*size else -1
arr = accumArray (+) 0 (0, size^2 - 1)
q x y = arr [(ix x y, 1.0)]
oneTurn a = arr $ concatMap oneSquare $ assocs a
oneSquare (i, 0) = []
oneSquare (i, v) = let
  (y, x) = divMod i size; z = v / genericLength s
  s = filter ((>=0) . fst) [(ix (x+1) y, z),
    (ix (x-1) y, z), (ix x (y+1), z), (ix x (y-1), z)] in s
main = printf "%.6f\n" (sum $ foldr (zipWith (*)) [1,1..] $ [map (1-) $
  elems $ (iterate oneTurn $ q x y) !! turns | x <- r, y <- r] :: Double)
-- from sannysanoff

