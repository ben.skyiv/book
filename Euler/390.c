// $ gcc -std=c99 390.c -lm -lgmp && time ./a.out
// S(10^7)  =       334,160,265      4.041s
// S(10^8)  =     6,808,392,896     45.868s
// S(10^9)  =   138,485,497,305   8m37.583s
// S(10^10) = 2,919,133,642,971  96m34.098s

#include <stdio.h>
#include <math.h>
#include <gmp.h>

int main(void)
{
  long n = 10000000000;
  mpz_t     n2, p2, q2, a2, a, sum;
  mpz_inits(n2, p2, q2, a2, a, sum, 0);
  mpz_set_si(n2, n);
  mpz_mul_si(n2, n2, n);
  for (long p9 = (long)sqrt(n / 2), p = 1; p < p9; p++) {
    mpz_set_si(p2, p);
    mpz_mul_si(p2, p2, p);
    for (long q = p; ; q++) {
      mpz_set_si(q2, q);
      mpz_mul_si(q2, q2, q);
      mpz_set_si(a2, 2 * p);
      mpz_mul_si(a2, a2, q);
      mpz_mul(a2, a2, a2);
      mpz_add(a2, a2, p2);
      mpz_add(a2, a2, q2);
      if (mpz_cmp(a2, n2) > 0) break;
      if (!mpz_perfect_square_p(a2)) continue;
      mpz_sqrt(a, a2);
      mpz_add(sum, sum, a);
    }
  }
  gmp_printf("S(%ld) = %Zd\n", n, sum);
  mpz_clears(n2, p2, q2, a2, a, sum, 0);
  return 0;
}

