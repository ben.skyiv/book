#include <stdio.h>
#include <stdlib.h>

typedef struct { long ps[11]; char n; } item;

// compute f(2n)
const long n = (long)5e6, m = 7 + (long)1e9;
const int n2 = 2237; // n2 > sqrt(n + n2)
const int CPUs = 6;

void factor(long i0, item a[], const char c[])
{
  static long t[n2];
  for (int i = 0; i < n2; i++) t[i] = 1, a[i].n = 0;
  for (int p = 2; (long)p * p < i0 + n2; p++)
    if (!c[p])
      for (long q = p; q < i0 + n2; q *= p)
        for (long i = q-1-(i0-1)%q; i < n2; t[i] *= p, i += q) {
          if (q == p) a[i].ps[a[i].n++] = p;
        }
  for (int i = 0; i < n2; i++)
    if (i + i0 != t[i]) a[i].ps[a[i].n++] = (i + i0) / t[i];
}

long gcd(long a, long b) { return b == 0 ? a : gcd(b, a % b); }
long lcm(long a, long b) { return a / gcd(a, b) * b; }

long c2a(long n, long a[], int size, long m, int odd)
{
  long z = 0;
  for (int i = size - 1; i >= 0; i--) {
    long d =  lcm(m, a[i]);
    z += (odd ? (-1) : 1) * (n / d) + c2a(n, a, i, d, !odd);
  }
  return z;
}

long c2(long n, long a[], int size) { return c2a(n, a, size, 1, 0); }

long f1(long i, item a)
{
  int d = a.n > 0 && a.ps[0] == 2;
  return i - c2(i, a.ps + d, a.n - d);
}

int main(int argc, char * argv[])
{
  static item a[n2]; static char c[n2];
  for (int i = 2; i * i < n2; i++)
    if (!c[i]) for (int j = i * i; j < n2; j += i) c[j] = 1;
  int k = atoi(argv[1]) - 100;
  long per = 1 + n / CPUs;
  long z0 = 1 + per * (k - 1);
  long z9 = (k == CPUs) ? n : (per * k);
  long z = 0;
  printf("%02d/%02d:[%ld,%ld] ", k, CPUs, z0, z9); fflush(stdout);
  for (long i0 = -n2, i = z0; i <= z9; i++) {
    if (i >= i0 + n2) factor(i0 = i, a, c);
    z = (z + f1(n, a[i - i0]) - f1(i, a[i - i0])) % m;
  }
  printf("%ld\n", z);
}

