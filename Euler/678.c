#include <stdio.h>
#include <math.h>

static long n = (long)1e18;

long pow64(long a, long b)
{
  long v = 1;
  for (long c = a; b != 0; b >>= 1, c *= c)
    if ((b & 1) != 0) v = v * c;
  return v;
}

long L(double a) { return (long)(a + 0.5); }

int valid(long a, long c, int e, int f, long cf)
{
  long ae = pow64(a, e), be = cf - ae;
  if (be <= ae) return 0;
  double bb = exp(log(be) / e);
  long b = L(bb);
  if (fabs(bb - b) > 1e-6) return 0;
  if (pow64(b, e) != be) return 0;
  printf("%9ld %9ld %9ld %2d %2d\n", a, b, c, e, f);
  return 1;
}

int main(void)
{
  double nn = log(n), n2 = log(2);
  long z = 0;
  for (int f = 3; f <= (int)L(nn / n2); f++)
    for (long c = 2; c <= L(exp(nn / f)); c++) {
      double fnc2 = f * log(c) - n2;
      long cf = pow64(c, f);
      if (cf > n) continue; // TODO: cf > 2^63
      for (int e = 3; e <= (int)L(fnc2 / n2); e++)
        if (e != f)
          for (long a = 1; a <= L(exp(fnc2 / e)); a++)
            if (valid(a, c, e, f, cf)) z++;
    }
  printf("F1(%ld) = %ld\n", n, z);
}

// 1e14: 159     5.702s
// 1e15: 239    20.723s
// 1e16: 346  1m14.858s
// 1e17: 501  4m37.013s
// 1e18: 712 16m51.468s

