roman = {'I':1, 'V':5, 'X':10, 'L':50, 'C':100, 'D':500, 'M':1000}
decI = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX']
decX = ['', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC']
decC = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM']

def to_roman(n):
  n, nI = divmod(n, 10)
  n, nX = divmod(n, 10)
  n, nC = divmod(n, 10)
  return 'M' * n + decC[nC] + decX[nX] + decI[nI]

def from_roman(str):
  dlist = [roman[c] for c in str] + [0]; n, i = 0, 0
  while i < len(dlist) - 1:
    if dlist[i] >= dlist[i+1]: n += dlist[i]; i += 1
    else: n += dlist[i+1]-dlist[i]; i += 2
  return n

def is_minimal(str): return str == to_roman(from_roman(str))

P = 0.14; PS = 0.02; tot = 0
letters = list(roman.keys()); slist = ['']; plist = [1]

for _ in range(30):
  slist2 = []; plist2 = []
  for i in range(len(slist)):
    slist3 = [slist[i] + d for d in letters]
    slist4 = [s for s in slist3 if is_minimal(s)]
    ptot = PS + len(slist4)*P
    plist4 = [plist[i]*P/ptot]*len(slist4)
    tot += from_roman(slist[i])*plist[i]*PS/ptot
    slist2 += slist4; plist2 += plist4
  slist = slist2; plist = plist2
  print(len(slist), tot)

