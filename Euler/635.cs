using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

static class E635
{
  static readonly int n = 5;
  static readonly int[] a = new int[n];

  static string ToString(int[] a)
  {
    var sb = new StringBuilder();
    foreach (var i in a) sb.Append(i + " ");
    return sb.ToString();
  }
  
  static IEnumerable<string> F1()
  {
    for (a[0] =      1; a[0] <= n+1; a[0]++)
    for (a[1] = a[0]+1; a[1] <= n+2; a[1]++)
    for (a[2] = a[1]+1; a[2] <= n+3; a[2]++)
    for (a[3] = a[2]+1; a[3] <= n+4; a[3]++)
    for (a[4] = a[3]+1; a[4] <= n+5; a[4]++)
    if (a.Sum() % n == 0) yield return ToString(a);
  }
  
  static IEnumerable<string> F2()
  {
    for (var i = (1L << n) - 1; i >= 0; i--) {
      var k = 1L;
      for (var j = 0; j < n; j++, k <<= 1) {
        a[j] = j + 1;
        if ((i & k) == 0) a[j] += n;
      }
      Array.Sort(a);
      yield return ToString(a);
    }
  }
  
  static void Main()
  {
    var s = new HashSet<string>(F2());
    foreach (var i in F1())
      if (!s.Contains(i))
        Console.WriteLine(i);
  }
}

