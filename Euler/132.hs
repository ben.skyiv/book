import Math.NumberTheory.Powers ( powerMod )
import Math.NumberTheory.Primes.Sieve ( primes )

main = print $ sum $ take 40 [x | x <- primes, 1 == powerMod 10 (10^9::Int) (9*x)]

