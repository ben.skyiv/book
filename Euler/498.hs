import Math.Combinat.Numbers ( binomial )

main = let n = 10^13; d = 10^4; m = d + 1000; c = 999999937 in print $ flip mod c $ sum
  [flip mod c $ (-1)^(k+1) * binomial n (d+k) * binomial (d+k) k | k <- [0..m-d-1]]

