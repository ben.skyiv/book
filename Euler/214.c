#include <stdio.h>

int main(void)
{
  const int n = 40000000, m = 25;
  static int a[n], c[n] = { 0, 1 };
  for (int i = 1; i < n; i++) a[i] = i;
  long sum = 0;
  for (int p = 2; p < n; p++) {
    if (a[p] != p) { c[p] = c[a[p]] + 1; continue; }
    for (int i = p+p; i < n; i += p) a[i] = a[i]/p*(p-1);
    if ((c[p] = c[p - 1] + 1) == m) sum += p;
  }
  printf("%ld\n", sum);
}

