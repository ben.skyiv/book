#include <stdio.h>

int main(void)
{
  long v = 3;
  for (int i = 4; i <= 30; i += 2) {
    int n = (1<<i)-i, d = 0, x = 0, y = n-1;
    int m = 2*(n-1), r = 0, c = 1, w = 0, z; 
    while (x < y)
      if (2 * x >= m - r) {
        r -= y; r -= --y; w += z = r >= 0;
        if (r < 0) { r += x; r += ++x; }
        v += 2 * ((c&1) ? (c-2*w) : (2*w-c));
        d += c + (c&1) - w; w = z; c = 1;
      }
      else r += x, r += ++x, ++c;
    if (x == y) d += 1-w, v += (c>1) ? (4*w-1) : 1;
    v += 2L * d * (n - d);
  }
  printf("%ld\n", v);
  return 0;
}

