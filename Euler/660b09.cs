// mcs 660b09.cs 660.cs && ./660b09.exe

using System;

static class E660b09
{
  static E660 e = new E660(9);

  static int P333()
  { // (1xx, 2xx, xxx) (1xx, 7xx, xxx) (3xx, 5xx, xxx)
    var z = 0;
    for (var i1 = 1; i1 <= e.B / 2; i1++)
      for (var i2 = i1+1; i2 <= e.B - i1; i2++)
        foreach (var s in E660.Permutation(e.GetRest(i1, i2), 4))
          z += e.F((i1 * e.B + s[1]) * e.B + s[2],
                   (i2 * e.B + s[3]) * e.B + s[4]);
    return z;
  }

  static int P234()
  { // (xx, 8xx, 1xxx)
    var z = 0;
    foreach (var s in E660.Permutation(e.GetRest(1, 8), 4))
      z += e.F(s[1] * e.B + s[2], (8 * e.B + s[3]) * e.B + s[4]);
    return z;
  }

  static int P144()
  { // (x, x8xx, x0xx)
    var z = 0;
    foreach (var s in E660.Permutation(e.GetRest(0, 8), 4))
      z += e.F(s[1], ((s[2] * e.B + 8) * e.B + s[3]) * e.B + s[4]);
    return z;
  }

  static void Main() { Console.WriteLine(P333() + P234() + P144()); }
}

