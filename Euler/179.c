#include <stdio.h>
#include <math.h>

int main(void)
{
  const int n = 10000000;
  static int a[n + 1], p[n];
  int m = (int)sqrt(n);
  for (int i = 2; i <= m; i++) p[i] = 1;
  for (int i = 2; i <= m; i++) {
    if (p[i] == 0) continue;
    for (int j = i*i; j <= m; j += i) p[j] = 0;
    for (int j = i*i; j <= n; j += i) a[j] = i;
  }
  a[1] = 1;
  for (int i = 2; i <= n; i++)
    if (a[i] == 0) a[i] = 2;
    else {
      int b = 2, p = a[i], j = i / p;
      for (; j % p == 0; b++) j /= p;
      a[i] = a[j] * b;
    }
  int v = 0;
  for (int i = 2; i < n; i++) if (a[i] == a[i+1]) v++;
  printf("%d\n", v);
  return 0;
}

