using System;
using System.Collections.Generic;

sealed class E302
{
  long limit, count;
  int[] exps, primes;

  int Gcd(int a, int b)
  {
    for (int t; b != 0; a = b, b = t) t = a % b;
    return a;
  }

  int[] GetPrimes(int max)
  {
    var primes = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) primes.Add(i);
    return primes.ToArray();
  }

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  bool IsAchilles(IEnumerable<int> exps)
  {
    var gcd = 0;
    foreach (var exp in exps)
    {
      if (exp < 2) return false;
      if (exp == 0) continue;
      if (gcd == 1) return true;
      gcd = (gcd == 0) ? exp : Gcd(gcd, exp);
    }
    return false;
  }
  
  long GetNumber()
  {
    long n = 1;
    for (var i = 0; i < exps.Length; i++)
      if (exps[i] > 0)
        n *= (long)Math.Pow(primes[i], exps[i]);
    return n;
  }
  
  SortedList<int, int> Merge(IEnumerable<int> list)
  {
    var v = new SortedList<int, int>();
    foreach (var p in list)
    {
      int n;
      if (!v.TryGetValue(p, out n)) v.Add(p, 1);
      else v[p] = n + 1;
    }
    return v;
  }
  
  IEnumerable<int> GetFactors(int x)
  {
    var list = new List<int>();
    var limit = (int)Math.Sqrt(x) + 1;
    for (int i = 0, prime = primes[i]; prime <= limit; prime = primes[i])
      if (x % prime == 0) { list.Add(prime); x /= prime; }
      else i++;
    if (x != 1) list.Add(x);
    return list;
  }
  
  int GetIndex(int prime)
  { // 需要使用更快速的算法，例如建立 primes 的 Dictionary
    for (var i = 0; i < primes.Length; i++)
      if (prime == primes[i]) return i;
    return -1;
  }
  
  IEnumerable<int> GetTotientExps()
  {
    var list = new int[exps.Length];
    for (var i = 0; i < exps.Length; i++)
    {
      if (exps[i] == 0) continue;
      list[i] += exps[i] - 1;
      foreach (var kvp in Merge(GetFactors(primes[i] - 1)))
        list[GetIndex(kvp.Key)] += kvp.Value;
    }
    return list;
  }
  
  void Compute2(int idx, long n)
  {
    if (idx < 0) return;
    var p = primes[idx];
    var expLimit = (int)Math.Log(n/*limit*/ / 4, p);
    for (var exp = 2; exp <= expLimit; exp++)
    {
      exps[idx] = exp;
      if (IsAchilles(exps) && GetNumber() <= limit
        && IsAchilles(GetTotientExps())
        ) count++;
      Compute2(idx - 1, n / (long)Math.Pow(p, exp));
    } 
  }
  
  long Compute(long max)
  {
    limit = max;
    var primeLimit = (int)Math.Sqrt(limit / 8) + 1;
    primes = GetPrimes(primeLimit);
    exps = new int[primes.Length];
    count = 0;
    Compute2(exps.Length - 1, max);
    return count;
  }

  static void Main()
  {
    Console.WriteLine(new E302().Compute(10000));
  }
}
