#include <iostream>
#include <cmath>
#include <ctime>
#include <unordered_set>

int main()
{
  std::unordered_set<long> s;
  int m = 97; long z = 0, n = (long)9e18;
  float R = CLOCKS_PER_SEC * 60.0;
  std::cout << n << " " << clock() / R << std::endl;
  for (int i = 0; i < m; i++) {
    s.clear();
    for (int b = 2; ; b++) {
      long b3 = (long)b * b * b, a2 = n / b3;
      if (a2 < 4) break;
      for (int a = (int)sqrt(a2); a > 1; a--) {
        long c = b3 * a * a;
        if (c % m == i) s.insert(c);
      }
    }
    z += s.size();
    std::cout << s.size() << ": " << i
      << " " << clock() / R << std::endl;
  }
  std::cout << z << " " << clock() / R << std::endl;
}

