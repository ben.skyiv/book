using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class E610
{
  static Dictionary<int, List<string>> GetRomans()
  {
    var dict = new Dictionary<int, List<string>>();
    for (int i = 0; i < 1000; i++) {
      var r = i.ToRoman(); var n = r.Length; List<string> s;
      if (!dict.TryGetValue(n, out s)) dict.Add(n, s = new List<string>());
      s.Add(r);
    }
    return dict;
  }
  
  static double Sum(List<string> s)
  {
    double z = 0;
    foreach (var r in s) z += r.FromRoman();
    return z;
  }

  static void Main()
  {
    double a1 = 0.02, a2 = 0.14 * 7, a3 = a2, v1 = a1, v2 = 0;
    var romans = GetRomans(); var s1 = romans[0];
    double s0 = 0; int n = 0, i = 1;
    for (; i < 13; i++, a3 *= a2) {
      var s2 = romans[i];
      foreach (var r in s1) s2.Add("M" + r);
      n  = s2.Count;
      var v0 = a1 * a3 / n;
      v1 += v0 * n;
      v2 += v0 * (s0 = Sum(s2));
      Console.WriteLine("{0,4}: {1,4} {2:F9} {3,15:F9}",i,n,v1,v2);
      s1 = s2;
    }
    for (; i < 20/*1800*/; i++, a3 *= a2) {
      var v0 = a1 * a3 / n;
      v1 += v0 * n;
      v2 += v0 * (s0 += 1000 * n);
      Console.WriteLine("{0,4}: {1,4} {2:F9} {3,15:F9}",i,n,v1,v2);
    }
  }
}

