import Data.Matrix

main = let n = 10^12; m = 10^8; d = 4 in print $ mod ((powerMod
  (fromList 1 d [2,2,-2,1] <-> submatrix 1 (d-1) 1 d (identity d))
  (n-d) (fromList d d [m,m..]) * fromList d 1 [8,4,1,1]) ! (1,1)) m
powerMod a n m = (\(_,_,x) -> x) $ until (\(n,_,_) -> n <= 0)
  (\(n,a,v) -> (div n 2, elementwiseUnsafe mod (a*a) m,
  if odd n then elementwiseUnsafe mod (v*a) m else v))
  (n, a, identity $ nrows a)

