import Data.MemoTrie ( memo )
import Math.Combinat ( stirling2nd )
main = print (mod (div ((10^e-1)*(10^e-2) - sum [(h d1 * h d2 + d1
　* h (d1+1) * h d2 + d2 * h d1 * h (d2+1)) * product [10-d1-d2..9]
　| d1 <- [1..8], d2 <- [1..9-d1]]) 2) 1000267129)
　where e = 18; h = memo (\d -> sum [stirling2nd i d | i <- [1..e]])

