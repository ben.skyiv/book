\p 3000
f(n) = { my(g, g0, g1, g2, s);
g = 0*x; g0 = x; g1 = 1 - x; s = 1;
for (i = 3, (n+2)^2,
  g2 = 0*x;
  if (!issquare(i-1), g2 += g1*x);
  if (!issquare(i-2), g2 += g0*(1-x));
  if (issquare(i), g += s * g2; s++);
  g0 = g1; g1 = g2); g }
g = f(50); i = 0.0;
while (i <= 1.0, printf("%.3f %.8f\n", i, subst(g,x,i)); i += 0.01)
quit()

