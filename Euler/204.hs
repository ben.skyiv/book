import Math.NumberTheory.Primes.Sieve ( primes )

main = let n = 10^9; m = 100 in  print $ f n 1 1 $ takeWhile (<=m) primes where
  f n m _ [] = m
  f n m p (x:xs) = if p > n then m-1 else f n (m+1) (p*x) (x:xs) + f n 0 p xs

