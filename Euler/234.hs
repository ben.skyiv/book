import Math.NumberTheory.Primes.Sieve ( primes )

n = 999966663333
ps = take ((+1) $ length $ takeWhile (<=(floor $ sqrt $ fromIntegral n)) primes) primes
f a b c d = let z = (*a) $ ceiling (fromIntegral c / fromIntegral a) in
  sum $ filter (\x -> x <= n && mod x b /= 0) [z,z+a..d]
main = print $ sum $ map (\(a,b,c,d) -> f a b c d + f b a c d) $
  map (\(a,b) -> (a,b,a*a+1,b*b-1)) $ zip ps $ tail ps

