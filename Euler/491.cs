// dmcs 491.cs lib/MultisetPermutationCount-slow.cs lib/Utils.cs
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Skyiv.Utils;

sealed class E491
{
  static readonly int[] all = {0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9};

  List<int[]> Read()
  {
    var list = new List<int[]>();
    foreach (var s in File.ReadLines("491.txt"))
      for (var i = 0; i < s.Length / 22; i++)
      {
        var a = new int[10];
        for (var j = 0; j < 10; j++)
        {
          a[j] = s[i*22 + j*2 + 1] - '0';
        }
        list.Add(a);
      }
Console.WriteLine("[{0}]", list.Count);
    var list2 = Remove(list);
Console.WriteLine("[{0}]", list2.Count);
    return list2;
  }
  
  List<int[]> Remove(List<int[]> list)
  {
    var list2 = new List<int[]>();
    for (var i = 0; i < list.Count; i++)
    {
      var a = list[i];
      var b = Not(a);
      if (!InList(list2, b)) list2.Add(a);
    }
    return list2;
  }
  
  bool InList(List<int[]> list, int[] x)
  {
    foreach (var a in list) if (Eq(a, x)) return true;
    return false;
  }
  
  string ToString(int[] a)
  {
    var sb = new StringBuilder();
    for (var i = 0; i < a.Length; i++) sb.Append(a[i].ToString());
    return sb.ToString();
  }
  
  bool Eq(int[] a, int[] b)
  {
    if (a.Length != b.Length) return false;
    for (var i = 0; i < a.Length; i++) if (a[i] != b[i]) return false;
    return true;
  }
  
  int[] Group(int[] a)
  {
    var list = new List<int>();
    var q = from i in a group i by i;
    foreach (var g in q) list.Add(g.Count());
    return list.ToArray();
  }
  
  int[] Not(int[] a)
  {
    var b = new List<int>(all);
    foreach (var i in a) b.Remove(i);
    return b.ToArray();
  }
  
  long Compute(int[] a1)
  {
    var a2 = Not(a1);
    var ns1 = Group(a1);
    var ns2 = Group(a2);
    var n1 = new MultisetPermutationCount(ns1).All();
    var n2 = new MultisetPermutationCount(ns2).All();
    long n1z = 0, n2z = 0;
    if (a1[0] == 0) {ns1[0]--; n1z = new MultisetPermutationCount(ns1).All();}
    if (a2[0] == 0) {ns2[0]--; n2z = new MultisetPermutationCount(ns2).All();}
    var v = (n1-n1z)*n2;
    if (!Eq(a1, a2)) v += (n2-n2z)*n1;
//Console.WriteLine("[{0}:{1,8}][{2}:{3,8}] {4,7:N0}/{5,7:N0} {6,7:N0}/{7,7:N0} {8,17:N0}",
//ToString(a1),ToString(ns1),ToString(a2),ToString(ns2),n1,n1z,n2,n2z,v);
    return v;
  }

  long Compute()
  {
    long v = 0;
    foreach (var a in Read()) v += Compute(a);
    return v;
  }

  static void Main()
  {
    Console.WriteLine(new E491().Compute());
  }
}

