using Nemo # https://oeis.org/A008443
n = 10^6 # 17526 * 10^9
R,x = PowerSeriesRing(ZZ, n+1, "x")
z = div(1 + floor(Int64, sqrt(8*n + 1)), 2)
G = sum([x^(div(k*(k-1),2)) for k = 1 : z])^3
println(coeff(G,n))

