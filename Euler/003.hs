factors 1 = []
factors n = let p = head [ x | x <- [2..], mod n x == 0 ] in p : factors (div n p)
main = print $ {-last $-} factors 600851475143

