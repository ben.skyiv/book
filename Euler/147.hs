main = let m = 47; n = 43 in print $ sum $ map f [(a,b) | a <- [1..m], b <- [1..n]]
  where f (a,b) = if a < b then f (b,a)
        else (div (a*(a+1)*b*(b+1)) 4) + (div (b*((a+a-b)*(b*b*4-1)-3)) 6)

