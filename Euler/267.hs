import Text.Printf ( printf )
import Math.Combinat.Numbers ( binomial )

main = let n = 1000; m = 431 in printf "%.12f\n" (((-)1) $ fromIntegral
  (sum $ map (binomial n) [0..m]) / fromIntegral 2^n :: Double)

