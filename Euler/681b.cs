using System;
using Skyiv.Utils;

static class E681
{
  static long sp = 0;

  static void RunC(int a, int b, long c, int[] w,
    int i, int low, int high, (int,int)[] pfs, int n)
  {
    if (i < 0) return;
    var (p, e) = pfs[i];
    for (var j = 0; j <= e; j++, c *= p, --w[i]) {
      if (i == 0 && c >= low && c <= high) {
        long d = n*(long)n/a/b/c, r = a + b + c + d;
        if (c <= d && r % 2 == 0 && r - d > d) sp += r;
      }
      RunC(a, b, c, (int[])w.Clone(), i-1, low, high, pfs, n);
    }
  }

  static void RunB(int a, long b, int[] w,
    int i, int low, int high, (int,int)[] pfs, int n)
  {
    if (i < 0) return;
    var (p, e) = pfs[i];
    for (var j = 0; j <= e; j++, b *= p, --w[i]) {
      if (i == 0 && b >= low && b <= high) {
        var pfs2 = new (int,int)[pfs.Length];
        for (var k = 0; k < w.Length; k++)
        { var (p2, _) = pfs[k]; pfs2[k] = (p2, w[k]); }
        RunC(a, (int)b, 1, (int[])w.Clone(), w.Length - 1,
          (int)b, (int)(n/Math.Sqrt((double)a*b)+0.5), pfs2, n);
      }
      RunB(a, b, (int[])w.Clone(), i-1, low, high, pfs, n);
    }
  }

  static void RunA(long a, int[] w,
    int i, int low, int high, (int,int)[] pfs, int n)
  {
    if (i < 0) return;
    var (p, e) = pfs[i];
    for (var j = 0; j <= e; j++, a *= p, --w[i]) {
      if (i == 0 && a >= low && a <= high) {
        var pfs2 = new (int,int)[pfs.Length];
        for (var k = 0; k < w.Length; k++)
        { var (p2, _) = pfs[k]; pfs2[k] = (p2, w[k]); }
        RunB((int)a, 1, (int[])w.Clone(), w.Length-1, Math.Max(2,(int)a),
          (int)(Math.Pow((double)n*n/a,1.0/3)+0.5), pfs2, n);
      }
      RunA(a, (int[])w.Clone(), i-1, low, high, pfs, n);
    }
  }

  static void Main()
  {
    long z = 4; int n = (int)1e6;
    Console.Write("SP({0:N0}) = ", n);
    foreach (var (i, pfs) in FactoriseList.Get(n)) {
      var w = new int[pfs.Length];
      for (var j = 0; j < pfs.Length; j++)
      { var (p,e) = pfs[j]; pfs[j] = (p, w[j] = e << 1); }
      sp = (i + 1) << 1;
      RunA(1, w, pfs.Length - 1, 1, (int)(Math.Sqrt(i)+0.5), pfs, i);
      z += sp;
    }
    Console.WriteLine(z.ToString("N0"));
  }
}
