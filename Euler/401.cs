using System;
using System.Numerics;

sealed class E401
{
  static long F(long n, long m)
  {
    return (long)(new BigInteger(n)*(n+1)*(2*n+1)/6%m);
  }

  static void Main()
  {
    long i = 1, j, z = 0, m = 1000000000, n = 1000000000000000;
    for (long j0 = n; (j = n/i) >= i; i++, j0 = j)
      z = (z + F(j, m) + (j0 - j) * F(i - 1, m)) % m;
    if (j != i - 2) z += F(j, m);
    Console.WriteLine(z % m);
  }
}

