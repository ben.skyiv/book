import Data.Char ( digitToInt )
import Data.MemoTrie ( memo3 )
f2 :: Int -> Int -> Int -> (Int, Int)
f2 = memo3 f2'; ds = sum . map digitToInt . show
f2' r i j = until ((>=10^(i+2)) . snd) (\(k,r) -> f1 k r i j) (0,r)
f1 k v i j = if i < 0 then (k + 1, v + j + ds v) else let
  d = 10^(i+1); (c,r) = divMod v d; (k2,r2) = f2 r (i-1) (j + ds c)
  in (k + k2, c*d + r2)
f n = snd $ fst $ until ((<0) . snd) (\((k,v),r) -> (last $
  takeWhile ((<=n) . fst) $ iterate (\(k,v) -> f1 k v r 0)
  (k,v), r-1)) ((1,1), floor $ logBase 10 $ fromIntegral n)
main = print $ f $ 10^15

