f = foldl (\a x -> a+a+x) 0
g n zs xs | z == 0 && length zs == 2^n = [reverse b] | True = concatMap
  (\x -> if elem (x+z) zs then [] else g n ((x+z):zs) $ x:xs) [0,1]
  where (a,b) = splitAt (n-1) xs; z = f $ reverse $ 0:a
main = let n = 5 in print $ sum $ map f $ g n [0] $ take n [0,0..]

