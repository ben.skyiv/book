import Math.NumberTheory.Powers ( isSquare )
import Data.Ratio ( (%),denominator,numerator )

main = let n = 10^5; d = 10^12 in print $ sum $ map (\s -> let
  (a,b) = closest d s in denominator $ if abs (a*a-s%1) < abs (b*b-s%1)
  then a else b) $ filter (not. isSquare) [2..n] where
  closest d s = (q, (k*qn+pn)%(k*qd+pd)) where
    (qn,qd) = (numerator q, denominator q)
    (pn,pd) = (numerator p, denominator p)
    k = div (d-pd) qd; a = floor $ sqrt $ fromIntegral s
    (q:p:_) = reverse $ takeWhile ((<=d) . denominator) $
      convergents $ (\(x:xs) -> x : cycle xs) $ period 0 1
    convergents xs = zipWith (%) (drop 2 $ a) (drop 2 $ b) where
      a = 0 : 1 : zipWith3 (\x y z -> x*y+z) xs (tail a) a
      b = 1 : 0 : zipWith3 (\x y z -> x*y+z) xs (tail b) b
    period m d = let m' = d*a'-m; d' = div (s-m'*m') d; a' = div (a+m) d
      in a' : if a' == a+a then [] else period m' d'

