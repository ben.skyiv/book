import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Factorisation ( factorise )

main = print $ mod (sum $ take (10^6-9) xs) $ 10^18 where
  xs = 9876543150 : zipWith f [11..] xs
  f i n = maximum $ n : [(p*) $ foldl (\a x -> p*a + x) 0 $ (\p x ->
    g x $ reverse $ takeWhile (<x) $ scanl1 (+) $ iterate (p*) 1) p $
    1234567890 * (sum . tail . takeWhile (>0) . iterate (flip div p))
    i | p <- map fst $ factorise i]
  g x (b:bs) = let (q,r) = divMod x b in q : g r bs; g _ _ = []

