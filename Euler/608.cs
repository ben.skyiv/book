using System;
using System.IO;

static class E608
{
  static void Main()
  {
    long z = 0;
    for (var i = 1; i <= 15; i++) {
      long v = 0;
      foreach (var s in File.ReadLines(i.ToString("D2") + ".txt"))
        v = long.Parse(s.Split()[0]);
      Console.Write(v + " ");
      z += v;
    }
    Console.WriteLine();
    Console.WriteLine("[{0}]", z % 1000000007);
  }
}

