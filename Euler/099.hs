main = do
  a <- readFile "099.txt"
  print $ snd $ maximum $ flip zip [1..] $ map (\(x,y) -> 
    (read $ tail y) * log (read x)) $ map (break (==',')) $ words a

