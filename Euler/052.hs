import Data.List ( sort )
import Data.Function ( on )

main = print $ head [x | x <- [1..], all ((on (==) $ sort . show) x . (*x)) [2..6]]

