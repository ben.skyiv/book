f xs@[z,m,n,a,b]
  | n > m     = xs
  | otherwise = f [z + div (a*b+3*a*a) (b*b-a*b-a*a), m, n+1, a+b, a+b+b]
main = print $ head $ f [head $ f [0, 15, 1, 1, 2], 15, 1, 2, 5]

