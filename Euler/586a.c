#include <stdio.h>
#include <math.h>

int count(long n)
{
  int z = 0; long m = n << 2;
  for (int b = 1; ; b++) {
    double d1 = sqrt(5L*b*b + m);
    int d2 = (int)d1, b2 = b + b, a2;
    if ((a2 = d2 - b2 - b) <= b2) break;
    if (d1 == d2 && (a2 & 1) == 0) z++;
  }
  return z;
}

int main(void)
{
  long n = (long)1e15, z = 0; int r = 40;
  long i11 = 14641, n1 = i11*19*29*31*41;
  for (long i = n1; i <= n; i += n1)
    if (count(i) == r) { printf("%ld ", i); fflush(stdout); z++; }
  //printf("f(%ld, %d) = %ld\n", n, r, z);
}
