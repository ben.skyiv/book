sealed class Matrix
{
  public static int M { get; set; }
  int n; int[,] v;

  Matrix(int n) { this.n = n; v = new int[n,n]; }
  public int ElementAt(int i, int j) { return v[i,j]; }

  public static Matrix GetA(int n)
  {
    var m = new Matrix(n);
    for (var i = 0; i < n; i++)
      for (var j = 0; j < n - i; j++)
        m.v[i, j] = 1;
    return m;
  }

  public static Matrix operator * (Matrix a, Matrix b)
  {
    var n = a.n;
    if (n != b.n) throw new System.Exception("a.n must = b.n");
    var m = new Matrix(n);
    for (var i = 0; i < n; i++)
      for (var j = 0; j < n; j++)
        for (var k = 0; k < n; k++)
          m.v[i,j] = (int)((m.v[i,j] + (long)a.v[i,k] * b.v[k,j]) % M);
    return m;
  }

  public Matrix Clone()
  {
    var m = new Matrix(n);
    for (var i = 0; i < n; i++)
      for (var j = 0; j < n; j++)
        m.v[i,j] = v[i,j];
    return m;
  }

  public Matrix Pow(long n)
  {
    if (n <= 0) throw new System.Exception("n must > 0");
    var z = Clone();
    if (n-- == 1) return z;
    for (var c = Clone(); n != 0; n >>= 1, c *= c)
      if ((n & 1) != 0) z *= c;
    return z;
  }
}

static class E654
{
  static void Main()
  {
    int n = 5000;
    long m = (long)1e12;
    Matrix.M = 7 + (int)1e9;
    System.Console.WriteLine("T({0},{1}) mod {2} = {3}",
      n, m, Matrix.M, Matrix.GetA(n-1).Pow(m+1).ElementAt(0,0));
  }
}
