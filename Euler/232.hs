import Text.Printf ( printf )
import Data.MemoTrie ( memo2 )

n = 100; m = succ $ ceiling $ logBase 2 $ fromIntegral n
f :: Int -> Int -> Double; g = memo2 f
f x y = if x >= n then 0 else if y >= n then 1 else
  maximum [(v * g x (y+2^(j-1)) + u * g (x+1) (y+2^(i-1))
  + (1-u) * g (x+1) y) / (1+v) | i <- [1..m], j <- [1..m],
  let u = 0.5 ** fromIntegral i; v = 0.5 ** fromIntegral j]
main = printf "%.8f\n" $ g 0 0

