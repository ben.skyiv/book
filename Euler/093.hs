import Data.List
main = print $ snd $ maximum [ (length $ takeWhile (\x -> fst x == snd x)
  $ zip [1..] $ dropWhile (<= 0) $ h xs, xs) | a <- [0..9], b <- [a+1..9],
  c <- [b+1..9], d <-[c+1..9], let xs = [a,b,c,d] ]
h [x] = [x]
h xs = nub $ sort [ calc a op b | (x,y) <- part xs, not $ null x, not $ null y,
  a <- h x, b <- h y, op <- "+-/*", op /= '/' || (b /= 0 && mod a b == 0) ]
part [] = [([],[])]
part (x:xs) = uncurry (++) $ unzip [ ((x:a,b),(a,x:b)) | (a,b) <- part xs ]
calc a '+' b = a + b
calc a '-' b = a - b
calc a '*' b = a * b
calc a '/' b = div a b
