import Math.NumberTheory.Primes ( primes )
import Math.NumberTheory.Powers ( powerMod )

m = 10007; n = 100
s1 = zipWith (\i p -> powerMod p i m) [1 :: Int ..] $ take n primes
s2 = [(s1 !! i) + (s1 !! (div (i+1) 10000)) | i <- [0 .. n-1]]
main = print s2

