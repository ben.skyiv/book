f (0,0,0,1) = 0
f (0,0,1,0) = f (0,0,0,1) + 1
f (0,1,0,0) = f (0,0,1,1) + 1
f (1,0,0,0) = f (0,1,1,1) + 1
f (a,b,c,d) = (pa + pb + pc + pd) / (a + b + c + d)  where
  pa | a > 0 = a * f (a-1,b+1,c+1,d+1) | otherwise = 0
  pb | b > 0 = b * f (a,b-1,c+1,d+1)   | otherwise = 0
  pc | c > 0 = c * f (a,b,c-1,d+1)     | otherwise = 0
  pd | d > 0 = d * f (a,b,c,d-1)       | otherwise = 0
main = print $ f (1,1,1,1)

