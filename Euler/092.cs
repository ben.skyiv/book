using System;
using System.Collections.Generic;

sealed class E092
{
  static HashSet<int> setx = new HashSet<int>();
  static HashSet<int> set1 = new HashSet<int>();
  static HashSet<int> set89 = new HashSet<int>();
  
  static int SquareDigit(int n)
  {
    int v = 0;
    for (int r; n > 0; n /= 10) v += (r = n % 10) * r;
    return v;
  }
  
  static bool Is89(int n)
  {
    setx.Clear();
    for (; ; n = SquareDigit(n))
    {
      if (n <= 567) setx.Add(n);
      if (set1.Contains(n)) { set1.UnionWith(setx); return false; }
      if (set89.Contains(n)) { set89.UnionWith(setx); return true; }
    }
  }

  static void Main()
  {
    set1.Add(1);
    set89.Add(89);
    int count = 0;
    for (var i = 1; i <= 10000000; i++) if (Is89(i)) count++;
    Console.WriteLine(count);
  }
}

