import Data.Set ( fromList )
import Math.NumberTheory.Primes.Testing ( isPrime )
main = let n = 10^12 in print $ flip (-) 16 $ sum $ fromList
  $ concat [takeWhile (<=n) [a^b | b <- [2..], not (isPrime a
  && isPrime b)] | a <- [2 .. ceiling $ sqrt $ fromIntegral n]]

