import Data.List ( sort )
import Control.Monad ( guard )
import Data.Set ( (\\),Set,empty,fromList,insert,member,toList )

data Env a = Env { guesses::[(Int,[a])], restrictions::[Set a],
  assignmentsLeft::Int } deriving (Eq,Ord)

combine 0 _      = [[]]
combine n []     = []
combine n (x:xs) = map (x:) (combine (n-1) xs) ++ combine n xs

solve (Env _  _ 0) = [[]]
solve (Env [] r _) = mapM (toList . (fromList "0123456789" \\)) r
solve e' = do
  is <- m; newE <- f is; rest <- solve newE
  return $ interleaveAscIndices is (l is) rest where
    f = (\p x -> guard (p x) >> return x) check . update e
    m = combine n $ positions g (restrictions e)
    e = e' { guesses = sort . guesses $ e' }
    l = fst . flip splitAscIndices g
    (n, g) = head . guesses $ e
    check e = all (\(n, s) -> length (positions s (restrictions e)) >= n)
      (guesses e) && let ns = (0:) . map fst . guesses $ e in
      minimum ns >= 0 && maximum ns <= assignmentsLeft e
    positions s = map fst . filter (not . snd) . zip [0..] . zipWith member s
    interleaveAscIndices = indices 0 where
      indices _ [] [] ys = ys
      indices n (i:is) (x:xs) ys
        | i == n = x : indices (n + 1) is xs ys
        | True   = head ys : indices (n + 1) (i:is) (x:xs) (tail ys)

splitAscIndices = indices 0 where
  indices _ [] xs = ([], xs)
  indices n (i:is) (x:xs)
    | i == n = let (b, e) = indices (n + 1) is     xs in (x:b, e)
    | True   = let (b, e) = indices (n + 1) (i:is) xs in (b, x:e)

update (Env ((_,a):gs) r l) is = Env gs' restriction (l - length is) where
  (assignment, newRestriction) = splitAscIndices is a
  (_         , oldRestriction) = splitAscIndices is r
  restriction = zipWith insert newRestriction oldRestriction
  gs' = map (\(n', a') -> let (dropped, newA) = splitAscIndices is a' in
    (n' - length (filter id $ zipWith (==) assignment dropped), newA)) gs

main = putStrLn . head . solve $ Env guess (replicate n empty) n where
  n = length . snd . head $ guess; guess = [      (2,"5616185650518293"),
    (1,"3847439647293047"),(3,"5855462940810587"),(3,"9742855507068353"),
    (3,"4296849643607543"),(1,"3174248439465858"),(2,"4513559094146117"),
    (3,"7890971548908067"),(1,"8157356344118483"),(2,"2615250744386899"),
    (3,"8690095851526254"),(1,"6375711915077050"),(1,"6913859173121360"),
    (2,"6442889055042768"),(0,"2321386104303845"),(2,"2326509471271448"),
    (2,"5251583379644322"),(3,"1748270476758276"),(1,"4895722652190306"),
    (3,"3041631117224635"),(3,"1841236454324589"),(2,"2659862637316867")]

