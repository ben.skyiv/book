using System;

static class E640A2
{
  static readonly int cards = 4, maxDeep = 22;
  static readonly int i9 = cards * cards / 4;
  static readonly long[] a = new long[maxDeep];

  static void Run(short b0, int deep)
  {
    if (deep >= maxDeep) return;
    for (var i = 0; i < i9; i++) {
      var b = b0;
      switch (i) {
        case 1: case 2:
          if      ((b & 4) != 0) b &= ~4;
          else if ((b & 1) != 0) b &= ~1;
          else if ((b & 2) != 0) b &= ~2;
          else                   b |=  2;
          break;
        case 0: 
          if      ((b & 1) != 0) b &= ~1;
          else if ((b & 2) != 0) b &= ~2;
          else                   b |=  2;
          break;
        default: // case 3:
          if      ((b & 8) != 0) b &= ~8;
          else if ((b & 2) != 0) b &= ~2;
          else                   b |=  2;
          break;
      }
      if (b == 0) a[deep]++;
      else Run(b, deep + 1);
    }
  }

  static void Main()
  {
    Run((short)((1 << cards) - 1), 0);
    long n = 1; double z = 0;
    for (var i = 1; i <= maxDeep; i++) {
      z += (double)i * a[i-1] / (n *= 4);
      if (i < 4 || i % 2 != 0) continue;
      Console.WriteLine("{0,2}: {1,16} {2,14} {3}",
        i, n, a[i-1], z);
    }
  }
}
