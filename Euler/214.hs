import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Factorisation ( φ )

f x = if x == 1 then 1 else (+1) $ f $ φ x
main = let n = 4*10^7; m = 25 in print $ sum $
  filter ((==m) . f) $ takeWhile (<n) primes

