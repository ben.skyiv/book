using System;
using System.Collections.Generic;

sealed class E002
{
  static void Main(string[] args)
  {
    var max = int.Parse(args[0]);
    var sum = 0;
    for (int c, a = 1, b = 2; b < max; c = a + b, a = b, b = c)
      if (b % 2 == 0) sum += b;
    Console.WriteLine(sum);
  }
}

