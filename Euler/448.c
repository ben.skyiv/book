#include <stdio.h>

int gcd(int a, int b)
{
  for (int t; b != 0; a = b, b = t) t = a % b;
  return a;
}

unsigned long a(int n)
{
  unsigned long v = 2;     // v = (v + ...) % m
  for (int i = 2; i < n; i++) v += i / gcd(n, i);
  return v;
}

int main(void)
{ // S(99,999,999,019) % 999,999,017.
  int n = 99999, m = 999999017;
  unsigned long s = 0;
  for (int k = 2; k <= n; k++) s = (a(k) + s) % m;
  printf("%ld\n", s + 1); // 525,519,998 7m45.460s
  return 0;
}

