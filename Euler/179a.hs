import Data.Array ( (!),accumArray,assocs,elems,listArray )
import Math.NumberTheory.Primes.Sieve ( primes )

main = let
  n = 10^7; m = floor $ sqrt $ fromIntegral n
  a = accumArray (curry snd) 0 (2,n) [(i,p) | p <- takeWhile (<=m) primes, i <- [p*p,p*p+p..n]]
  b = listArray (1,n) $ (1:) $ map (\(i,x) -> if x == 0 then 2 else f i x) $ assocs a
  f i p = let (j, c, _) = g (div i p, 2, p) in b!j * c
  g (i, n, p) = if mod i p /= 0 then (i, n, p) else g (div i p, n+1, p)
  in print $ fst $ foldl (\(a,i) j -> if i == j then (a+1,j) else (a,j)) (0,0) $ elems b

