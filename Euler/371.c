#include <stdio.h>

int main()
{
  double u = 2, v = 2.004;
  for (int i = 498; i >= 0; i--) {
    u = (1000 + (998-2*i)*u    ) / (999-i);
    v = (1000 + (998-2*i)*v + u) / (999-i);
  }
  printf("%0.8lf\n", v);
  return 0;
}

