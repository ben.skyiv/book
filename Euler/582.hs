import Data.Maybe ( fromJust,isJust )
import Math.NumberTheory.Powers ( exactSquareRoot )
-- 10^8: 1245, 22m15s
main = let n = 10^3 in print $
  length [c | a <- [1..n], d <- [1..100], 
  let c2 = 3*a*(a+d)+d*d, let c1 = exactSquareRoot c2,
  isJust c1, let c = fromJust c1, c <= n]

