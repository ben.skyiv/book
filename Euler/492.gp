f(p, n) = {
  local(t=(Mod(11+x,Mod(1,p)*(x^2-117))/2)^lift(Mod(2,p^2-1)^(n-1)));
  lift((lift(t+1/t)-5)/6)
};
X=10^9; Y=10^7; N=10^15; r=0; forprime(p=X,X+Y,r+=f(p,N)); print(r); quit()

