import Data.Array ( (//),(!),accum,elems,listArray )

n' = 5; m' = n' + 3; m = 5*m'; b = ((0,0),(m',10*m'))
c = listArray b [0,0..]; s = listArray b [0,0..]
f3 n i
  | even n && i >  div n 2     =  1
  | even n && i <= div n 2     = -1
  | odd  n && i <= div n 2     = -1
  | odd  n && i >  div n 2 + 1 =  1
  | otherwise                  =  0
f2 c s n i j = let s' = s // [((i,j+m),0)]; c' = c // [((i,j+m),0)]; w = f3 n i
  in foldl (\(c,s) d -> if d == 0 && i == 1 then (c,s) else let
  ct = c ! (i-1, j-w*d+m)
  c' = accum (+) c [((i,j+m),ct)]
  s' = accum (+) s [((i,j+m),10*s!(i-1,j-w*d+m)+ct*d)]
  in (c',s')) (c',s') [0..9]
f1 (z,c,s) (n,i) = foldl (\(z,c,s) j -> let
  cs = f2 c s n i j
  c' = if i == 0 then c // [((i,j+m), if j == 0 then 1 else 0)] else fst cs
  s' = if i == 0 then s else snd cs
  in (z + s'!(n,m), c', s')) (z,c,s) [10-m..m-10]
main = print $ (\(z,_,_) -> mod z $ 3^15) $ foldl f1 (0,c,s)
  [(n,i) | n <- [1..n'], i <- [0..n]]

