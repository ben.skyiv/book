// http://en.wikipedia.org/wiki/Letter_frequency
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

sealed class E059
{
  List<int> cipher;

  Dictionary<int, int> GetFrequency()
  {
    cipher = new List<int>();
    var dict = new Dictionary<int, int>();
    var ss = Console.In.ReadToEnd().Trim().Split(',');
    foreach (var s in ss)
    {
      var n = int.Parse(s);
      int count;
      dict.TryGetValue(n, out count);
      dict[n] = count + 1;
      cipher.Add(n);
    }
    return dict;
  }
  
  int[] GetKey()
  {
    var dict = new SortedDictionary<int, int>();
    foreach (var kvp in GetFrequency())
      dict.Add(-(kvp.Value * 1000 + kvp.Key) , kvp.Key);
    var list = dict.Values.ToArray();
    var key = new int[3];
    key[0] = 't' ^ list[14];
    key[1] = ' ' ^ list[0];
    key[2] = 'e' ^ list[3];
    return key;
  }

  int Compute()
  {
    var key = GetKey();
    int sum = 0, i = 0;
    foreach (var c in cipher)
    {
      var n = c ^ key[i++ % 3];
      // Console.Write("{0}", (char)n);
      sum += n;
    }
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E059().Compute());
  }
}

