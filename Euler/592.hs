import Text.Printf ( printf )

m = 16^12
f 0 = 1
f n = mod (g (mod n m) * f (div n 2) * if odd $ div n m then -1 else 1) m
-- g n = let z = div n 2 in mod (div (product [z+1 .. n]) (2^z)) m
g n = mod (product [1,3..n]) m
h = (flip mod 4) . sum . takeWhile (>0) . tail . iterate (`div` 2)
main = let n = 20 in printf "%X\n" ((f n * 2 ^ h n) :: Integer)

