import Data.List
digitsRev b = unfoldr (\x -> if x == 0 then Nothing else let (q,r) = divMod x b in Just(r,q))
main = print $ let xs = map fst $ filter (\x -> 0 < snd x) $ zip [1..] $ digitsRev 2 $ 10^25
  in fst $ foldl (\(a,z) x -> let b=a*x+z in (b,b-a)) (1,0) $ zipWith (-) xs $ 0:init xs

