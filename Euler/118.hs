import Data.List ( group, permutations, (\\) )
import Math.NumberTheory.Primes.Testing ( isPrime )

comb 0 _      = [[]]
comb _ []     = []
comb n (x:xs) = map (x:) (comb (n-1) xs) ++ comb n xs

f 0 _      = [[]]
f _ []     = []
f n (x:xs) = if x > n then [] else map (x:) (f (n-x) (x:xs)) ++ f n xs

g = product . map (product . enumFromTo 1 . length) . group

p = length . filter (isPrime . read) . permutations

q [_] ys    = p ys
q (x:xs) ys = sum [p zs * q xs (ys \\ zs) | zs <- comb x ys, p zs /= 0]

main = print . sum . map (\a -> div (q a "123456789") $ g a) $ f 9 [1..8]

