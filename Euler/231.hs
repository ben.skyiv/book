import Math.NumberTheory.Primes.Sieve ( primes )

main = let n = 2*10^7; m = 15*10^6 in print $ sum [p * (f n p
  - f m p - f (n-m) p) | p <- takeWhile (<=n) primes] where
  f n p = sum $ takeWhile (>0) $ iterate (flip div p) n

