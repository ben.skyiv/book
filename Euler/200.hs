import Data.List ( isInfixOf,sort )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Testing ( isPrime )

primeProof xs = not . any (isPrime . read) $ concatMap (\n ->
  let (a,b) = splitAt n xs; zs = [x : tail b | x <- ['0'..'9']]
  in map (a++) zs) [0 .. length xs - 1]

main = print $ let n = 200; qn = 800; pn = 20000 in (!!(n-1)) $
  filter (\x -> let z = show x in isInfixOf "200" z && primeProof z) $
  sort [q*q*q*p*p | q <- take qn primes, p <- take pn primes, q /= p]

