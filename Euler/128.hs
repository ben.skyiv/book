import Math.NumberTheory.Primes.Testing ( isPrime )

main = let
  z = 2000
  f b c n
    | c >= z = 3*n*n - 3*n + if b == 1 then 2 else 1
    | g 0 && g 2 && g (6*n+6) && b /= 1 = f 1 (c+1) n
    | g 0 && g 6 && g (6*n-6) && n /= 1 = f 2 (c+1) (n+1)
    | otherwise                         = f 3 c (n+1)
    where g = isPrime . (+)(6*n-1)
  in print $ f 0 1 1

