#include <stdio.h>

int modPow(int a, int b, int m)
{
  long v = 1;
  for (; b; b >>= 1, a = (long)a * a % m) if (b & 1) v = v * a % m;
  return (int)v;
}

int valid(int a[], int n, int p)
{
  for (int i = p - 1; i > 1; ) {
    if (modPow(n, (p-1)/a[i], p) == 1) return 0;
    for (int m = a[i]; i % m == 0; ) i /= m;
  }
  return 1;
}

int main(void)
{
  const int n = 100000000; static int a[n]; static long x[n]; long z = 0;
  for (int i = 2; i < n; i++) if (!a[i]) for (int j = i; j < n; j += i) a[j] = i;
  for (int i = 3; i < n; i++) x[i] = (long)i * (i - 1) - 1;
  for (int p, i = 3; i < n; i++) {
    if (x[i] == 1 || x[i] > n) continue;
    if (valid(a, i, p = (int)x[i]) || valid(a, p+1-i, p)) z += p;
    for             (int j = i;     j < n; j += p) while (x[j] % p == 0) x[j] /= p;
    if (p != 5) for (int j = p+1-i; j < n; j += p) while (x[j] % p == 0) x[j] /= p;
  }
  printf("%ld\n", z);
}

