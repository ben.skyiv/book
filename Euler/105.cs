using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

sealed class E103
{
  int[] mask = { 1,2,4,8,0x10,0x20,0x40,0x80,0x100,0x200,0x400,0x800,0x1000 };
  
  IEnumerable<HashSet<T>> AllSubset<T>(HashSet<T> set)
  { // return all subset except empty-set
    var n = set.Count;
    var array = set.ToArray();
    var list = new List<HashSet<T>>();
    for (var i = 1; i < (1 << n); i++)
    {
      var subset = new HashSet<T>();
      for (var k = 0; k < n; k++)
        if ((i & mask[k]) != 0)
          subset.Add(array[k]);
      list.Add(subset);
    }
    return list;
  }
  
  bool IsSumSet2(HashSet<int> set1, HashSet<int> set2)
  {
    var sum1 = set1.Sum();
    var sum2 = set2.Sum();
    if (sum1 == sum2) return false;
    if (set1.Count > set2.Count && sum1 <= sum2) return false;
    if (set1.Count < set2.Count && sum1 >= sum2) return false;
    return true;
  }
  
  bool IsSumSet(HashSet<int> set, HashSet<int> subset)
  {
    var set2 = new HashSet<int>(set);
    set2.ExceptWith(subset);
    foreach (var subset2 in AllSubset(set2))
      if (!IsSumSet2(subset, subset2)) return false;
    return true;
  }

  bool IsSumSet(HashSet<int> set)
  {
    foreach (var subset in AllSubset(set))
      if (!IsSumSet(set, subset))
        return false;
    return true;
  }
  
  long Compute()
  {
    long sum = 0;
    foreach (var line in File.ReadLines("105.txt"))
    {
      var set = new HashSet<int>(line.Split(',').Select(x => int.Parse(x)));
      if (IsSumSet(set)) sum += set.Sum();
    }
    return sum;
  } 
  
  static void Main()
  {
    Console.WriteLine(new E103().Compute());
  }
}
