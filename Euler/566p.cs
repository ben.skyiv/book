using System;
using System.Collections.Generic;
using Skyiv.Numerics;

static class E566
{
  static readonly int C = 360;

  sealed class F
  {
    Quadratic p = 0; Quadratic[] xyz; bool icing = true;
    List<Quadratic> s = new List<Quadratic>(), t = new List<Quadratic>();

    public F(int a, int b, int c)
    {
      xyz = new Quadratic[]{ Quadratic.From(a,C,0,1),
        Quadratic.From(b,C,0,1), Quadratic.From(c,0,C,c) };
      s.Add(0); s.Add(C);
    }

    void Step(Quadratic v)
    {
      if (p + v > C) Left();
      var c = s.Count; var q = p + v; int i, j = 0;
      while (j < c && q > s[j]) j++;
      for (i = j; i > 0 && p < s[i]; ) i--;
      bool e = (j - i) % 2 == 0, b = p == s[i], d = q == s[j]; t.Clear();
      for (var k = i+1; k < j; k++) t.Add(q-s[k]+p);
      for (var k = i+1; k < j; k++) s[j-k+i+(e?0:b?-1:d?1:0)] = t[k-i-1];
      if (e) return;
      if ( b      )   s[j - 1] = q;
      if (!b &&  d) { s[i + 1] = p;    if (j == c-1) s.Add(C); }
      if ( b &&  d) { s.RemoveAt(j-1); if (j != c-1) s.RemoveAt(j-1);  }
      if (!b && !d) { s.Insert(j, q);                s.Insert(i+1, p); }
      if ( b && i == 0) { s.Insert(0, 0); icing = !icing; }
    }

    void Left()
    {
      var c = s.Count; var i = c - 2;
      while (i > 0 && p < s[i]) i--;
      if (i % 2 == 1) icing = !icing; t.Clear(); t.Add(0);
      for (var k = i+1; k < c - ((c%2==0) ? 1 : 0); k++) t.Add(s[k]-p);
      for (var k = 1; k <= i - ((p==s[i]) ? 1 : 0); k++) t.Add(s[k]-p+C);
      t.Add(C); s.Clear(); var u = s; s = t; t = u; p = 0;
    }

    public int Compute()
    {
      Quadratic v;
      for (var k = 0; k < (int)1e9; p += v) {
        Step(v = xyz[k++ % 3]);
        if (s.Count <= 2) return icing ? k : -k;
      }
      throw new Exception("Over");
    }
  }

  static void Main()
  {
    //Console.WriteLine(new F(9,10,11).Compute());
    long z = 0; int n = 17; // G(17): 11s
    for (int a = 9; a < n - 1; a++)
      for (int b = a + 1; b < n; b++)
        for (int c = b + 1; c <= n; c++) {
          var w = new F(a, b, c).Compute();
          Console.WriteLine("F({0,2} {1,2} {2,2}) = {3,6}", a, b, c, w);
          z += (w >= 0) ? w : (-w - w);
        }
    Console.WriteLine("G({0}) = {1}", n, z);
  }
}

