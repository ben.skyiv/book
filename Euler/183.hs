main = let m = 5; n = 10^4 in print $ sum [if terminating z (round $
  fromIntegral z / exp 1) then -z else z | z <- [m..n]] where
  terminating a b = foldl reduce (div b $ gcd a b) [2,5] == 1
  reduce n x = if mod n x == 0 then reduce (div n x) x else n

