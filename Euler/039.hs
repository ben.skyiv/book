import Data.List
factors 1 = []
factors n = let p = head [ x | x <- [2..], mod n x == 0 ] in p : factors (div n p)
triangles p = [ (m,n,p) | m <- [2..p], mod p (2*m) == 0, let n = div p (2*m) - m, n > 0, m > n ]
to'right'sides p (x,y,z) = let n = div p z; a = x^2-y^2; b = 2*x*y in sort [n*a, n*b]
f p = nub $ map (to'right'sides p) $ concat $ map triangles $
  (nub . (filter (>=12)) . (map product) . subsequences . factors) p
main = print $ fst $ head $ filter (\(_,x) -> length x > 6) $ map (\x -> (x, f x)) [12,14..1000]

