#include <stdio.h>
#include <math.h>
#include <gmp.h>

int digits_sum(const char s[])
{
  if (s[1] != '.') return -1;
  int sum = s[0] - '0';
  for (int i = 2; i <= 100; i++) sum += s[i] - '0';
  return sum;
}

int main(void)
{
  const int digits = 105;
  char buf[digits + 10];
  mpf_set_default_prec(digits * log(10) / log(2));
  printf("precision: %lu bits\n", mpf_get_default_prec());
  mpf_t x;
  mpf_init(x);
  int sum = 0;
  for (int i = 1; i < 100; i++) {
    mpf_sqrt_ui(x, i);
    gmp_sprintf(buf, "%.*Ff", digits, x);
    int ds = digits_sum(buf);
    if (ds > 100) sum += ds;
    printf("%2d %3d %.40s...\n", i, ds, buf);
  }
  printf(":%d\n", sum);
  mpf_clear(x);
  return 0;
}
