using System;

public class ProjectEuler153
{
  static long Gcd(long p, long q)
  {
    while (q > 0) { long r = p % q; p = q; q = r; }
    return p;
  }

  static void Main()
  {
    long n = 100000000, s = 0;
    for (long i = 1; i <= n; i++) s += (n / i) * i;
    for (long a = 1; a <= (long)Math.Sqrt(n); a++)
      for (long b = 1; b <= a; b++)
        if (Gcd(a, b) == 1)
        {
          long m = 0, l = n / (a * a + b * b);
          for (long i = 1; i <= l; i++) m += (l / i) * i;
          s += m * (a == b ? 2 * a : 2 * (a + b));
        }
    Console.WriteLine(s);
  }
}