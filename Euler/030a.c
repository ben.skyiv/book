#include <stdio.h>

int sum(int n)
{
  int z = 0;
  for (; n > 0; n /= 10) {
    int i = n % 10;
    z += i*i*i*i*i*i*i*i*i;
  }
  return z;
}

int main()
{
  printf("Start\n"); fflush(stdout); int z = 0;
  for (int i = 2; i <= (int)1e9; i++) {
    if (sum(i) != i) continue;
    z += i;
    printf("%d\n", i); fflush(stdout);
  }
  printf("[%d]\n",z); fflush(stdout);
  return 0;
}

