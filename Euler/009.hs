main = print $ head [ a*b*c | a <- [1..], b <- [1..a], let c = 1000-a-b, a*a+b*b == c*c ]
-- main = print $ head [ a*b*c | m <- [1..], n <- [1..m-1], let a = m*m-n*n; b = 2*m*n; c = m*m+n*n, a+b+c == 1000 ]
-- 0m0.026s 0m0.003s

