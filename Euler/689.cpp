#include <stdio.h>
#include <random>
using namespace std;

bool g(double x)
{
  double z = 0;
  for (int i = 1; i <= 1000 && x != 0; i++) {
    if ((x *= 2) >= 1) x--, z += 1.0 / i / i;
    if (z > 0.5) return true;
    if (x >= 1) exit(1);
  }
  return false;
}

int main()
{
  random_device rd;
  mt19937 gen(rd());
  uniform_real_distribution<> dis(0,1);
  int z = 0, n = (int)1e9;
  for (int i = 0; i < n; i++)
    if (g(dis(gen))) z++;
  printf("%.10lf\n", (double)z / n);
}
