-- https://projecteuler.net/thread=17&page=2#2779

import Data.Char

wordify n = case ((map digitToInt) . show) n of
  [_] -> a !! n
  [1, _] -> a !! n
  [y, 0] -> b !! y
  [y, x] -> (b !! y) ++ "-" ++ (a !! x)
  [z, 0, 0] -> (a !! z) ++ " hundred"
  [z, y, x] -> (a !! z) ++ " hundred and " ++ (wordify (y * 10 + x))
  [1, 0, 0, 0] -> "one thousand"
  where
    a = ["","one","two","three","four","five","six","seven","eight","nine","ten","eleven",
      "twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"]
    b = ["","","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"]

main = print . length . filter (\x -> x>='a' && x<='z') . concat . map wordify $ [1..1000]

