using System;
using System.Linq;

sealed class E258
{
  static readonly long z = 1000000000000000000;
  static readonly int m = 20092010, n = 2000;
  
  static void Product(int[] c, int[] a, int[] b)
  {
    var v = new long[n + n - 1];
    for (int i = 0; i < n; i++)
      for (int j = 0; j <= i; j++)
        v[i] += (long)a[j] * b[i - j];
    for (int i = n; i < 2 * n - 1; i++)
      for (int j = i - n + 1; j < n; j++)
        v[i] += (long)a[j] * b[i - j];
    for (int i = 1; i < n - 1; i++)
      c[i] = (int)((v[i] + v[i+n-1] + v[i+n]) % m);
    c[0]   = (int)((v[0] + v[n]) % m);
    c[n-1] = (int)((v[n-1] + v[n+n-2]) % m);
  }

  static void Main()
  { 
    int[] v = new int[n], a = new int[n];
    v[0] = a[1] = 1;
    for (long k = z; k > 0; Product(a, a, a), k /= 2)
      if (k % 2 != 0) Product(v, v, a);
    Console.WriteLine(v.Sum(x => (long)x) % m);
  }
}

