-- prime factor of b has form 3x+1
main = let a = 2*3*5*11*17*23*29*41; b = 7*13*19*31*37*43; n = a*b
  in print $ pred $ sum [x | x <- [1, a+1 .. n-1], mod (x^3) n == 1]

