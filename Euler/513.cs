using System;

sealed class E513
{
  static void Main()
  {
    int n = 10000, v = 0;
    for (var c = 2; c <= n; c += 2)
    {
      var c2 = (long)c * c;
      for (var b = 1; b <= c; b++)
      {
        var d = (((long)b * b) << 1) - c2;
        for (var a = c - b + 1; a <= b; a++)
        {
          var x = Math.Sqrt(d + (((long)a * a) << 1));
          if ((int)x == x) v++;
        }
      }
    }
    Console.WriteLine("F({0:N0}) = {1:N0}", n, v);
  }
}

