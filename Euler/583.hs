import Data.Maybe ( fromJust,isJust )
import Math.NumberTheory.Powers.Squares ( exactSquareRoot,isSquare' )
-- S(10^4) = 884,680, Find S(10^7)
main = let n = 10^3 in mapM_ print [(w,h,g) | w <- [2,4..n], h <- [1..n],
  g <- [1..h-1], isSquare' (w*w+h*h), let w0 = div w 2, let w2 = w0*w0,
  let f0 = exactSquareRoot(g*g+w2), isJust f0, let f = fromJust f0,
  let gh = g+h, isSquare' (w2+gh*gh)] --, let p = w + 2*(f+h), p <= n]

