#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
  const int n = (int)(sqrt((5000000000000000 << 1) - 1) / 2 - 0.5);
  long p, z = 0, *a = (long *)calloc(n, sizeof(long));
  for (int i = 0; i < n; i++) a[i] = ((long)i << 1) * (i + 1) + 1;
  for (int i = 1; i < n; i++) {
    if ((p = a[i]) == 1) continue;
    if (p == ((long)i << 1) * (i + 1) + 1) z++;
    for (long j = i;     j < n; j += p) while (a[j] % p == 0) a[j] /= p;
    for (long j = p-i-1; j < n; j += p) while (a[j] % p == 0) a[j] /= p;
  }
  free(a);
  printf("%ld\n", z);
  return 0;
}

