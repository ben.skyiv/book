#include <stdio.h>

int ways(int n, int k, const int a[])
{
  if (k <= 0) return 1;
  int count = 0;
  for (; n >= 0; n -= a[k]) count += ways(n, k - 1, a);
  return count;
}

int main(void)
{
  int a[] = { 1, 2, 5, 10, 20, 50, 100, 200 }; // ans: 73682
  printf("%d\n", ways(200, sizeof(a) / sizeof(a[0]) - 1, a));
  return 0;
}
