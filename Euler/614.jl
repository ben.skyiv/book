using Nemo
n = 10^4; m = 10^9+7
R,x = PowerSeriesRing(ResidueRing(ZZ,m),n+1,"x")
G = prod([1 + x^k for k = 1 : n if k % 4 != 2])
@time println(sum([coeff(G,i) for i = 1 : n]))
