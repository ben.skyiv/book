#include <stdio.h>

int fn(int n)
{
  int m = 1, count = 0;
  for (long k = (long)(n+1)*n; m <= n; m++, k += n)
    if (k % m == 0) count++;
  return count;
}

int main(void)
{
  for (int i = 2; ; i++)
  {
    int z = fn(i);
    if (z < 1000) continue;
    printf("%d:%d ", i, z);
    break;
  }
  return 0;
}
