using System;
using System.Collections.Generic;
using Skyiv.Utils;

static class E650
{
  static readonly int m = 7 + (int)1e9;
  static List<Tuple<int,int>>[] pfss;

  static List<int>[] Sieve(int max)
  { // pfs[i]:the prime factorisation of i for p <= sqrt(max)
    int limit = (int)Math.Sqrt(max);
    var pfs = new List<int>[max + 1];
    for (int i = 2; i <= limit; i++)
      if (pfs[i] == null)
        for (int j = i + i; j <= max; j += i) {
          if (pfs[j] == null) pfs[j] = new List<int>();
          pfs[j].Add(i);
        }
    return pfs;
  }

  static List<Tuple<int,int>> GetPrimeFactors(List<int> lst, int n)
  { // return: the prime factorisation of n
    var lst2 = new List<Tuple<int,int>>();
    if (lst != null)
      for (int j = 0, k = 0; j < lst.Count; )
        if(n % lst[j] == 0) { n /= lst[j]; k++; }
        else { lst2.Add(Tuple.Create(lst[j++], k)); k = 0; }
    if (n != 1) lst2.Add(Tuple.Create(n, 1));
    return lst2;
  }
  
  static void Add(Tuple<int,int> t, int k, Dictionary<int,int> dict)
  {
    int a;
    dict.TryGetValue(t.Item1, out a);
    dict[t.Item1] = k * t.Item2 + a;
  }

  static long D(int n)
  {
    var dict = new Dictionary<int,int>();
    for (int k = n - 1, i = 0; k > 0; i++, k -= 2) {
      foreach (var t in pfss[n - i]) Add(t,  k, dict);
      foreach (var t in pfss[1 + i]) Add(t, -k, dict);
    }
    long z = 1;
    foreach(var kvp in dict) {
      z = z * (kvp.Key.ModPow(kvp.Value  + 1, m) - 1) % m;
      z = z * (kvp.Key - 1).ModInv(m) % m;
    }
    return z;
  }

  static void Main()
  {
    int n = 20000;
    var lists = Sieve(n);
    pfss = new List<Tuple<int,int>>[n + 1];
    for (var i = 1; i <= n; i++) pfss[i] = GetPrimeFactors(lists[i], i);
    long z = 0;
    for (var i = 1; i <= n; i++) z = (D(i) + z) % m;
    Console.WriteLine(z);
  }
}

