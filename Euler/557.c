#include <stdio.h>
#include <math.h>

long gcd(long a, long b) { return a ? gcd(b % a, a) : b; }

int main(void)
{ // b = (sum - diff) / 2, c = (sum + diff) / 2
  long z = 0, n = 10000; // s == a+b+c+d, b*c*(s+a) == a*a*d
  for (long s = 4; s <= n; s++)
    for (long a = 1; a < s-2; a++)
      for (long t = (s+a) / gcd(a*a, s+a), d = t; ; d += t) {
        long diff, delta, prod = a*a*d / (s+a), sum = s-a-d;
        if (sum < 2 || (delta = sum*sum - 4*prod) < 0) break;
        if (diff = sqrt(delta), diff * diff == delta) z += s;
      }
  printf("%ld\n", z);
}

