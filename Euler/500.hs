import Data.Array ( (!),listArray )
import Math.NumberTheory.Primes.Sieve ( primes )

main = let
  n = 500500; m = 500500507; k = length b - 1
  a = listArray (0,k) b; ps = listArray (0,10^6) primes
  b = snd $ until ((<=0) . fst . fst) (\((n,i),a) -> let
    j = succ $ floor $ logBase 2 $ logBase (fromIntegral
      (ps ! i)) $ fromIntegral $ ps ! (n+i-1)
    in ((n-j, i+1), j:a)) ((n,0),[]); mask = [1,2,4,8,16,32]
  in print $ foldl (\z i -> fst $ until ((>= mask !! (a ! (k-i)))
    . snd) (\(z,j) -> (mod (z * (ps ! i)) m, j+1)) (z,1)) 1 [0..k]

