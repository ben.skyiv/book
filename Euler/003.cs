using System;
using System.Collections.Generic;

sealed class E003
{
  int[] GetPrimes(int max)
  {
    var primes = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) primes.Add(i);
    primes.Add(int.MaxValue);
    return primes.ToArray();
  }

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }

  IEnumerable<long> Factors(long x)
  {
    var list = new List<long>();
    var limit = (int)Math.Sqrt(x) + 1;
    var primes = GetPrimes(limit);
    for (int i = 0, prime = primes[i]; prime <= limit; prime = primes[i])
      if (x % prime == 0) { list.Add(prime); x /= prime; }
      else i++;
    if (x != 1) list.Add(x);
    return list;
  }

  static void Main(string[] args)
  {
    foreach (var n in new E003().Factors(long.Parse(args[0])))
      Console.Write(" " + n);
    Console.WriteLine();
  }
}

