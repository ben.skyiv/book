main = let p = 75*10^6 in print $ f p 2 2 3 0 where
  f p a b c v = if a+b+c > p then v else let
    z =           f p (2*c-2*b+a) (2*c-b+2*a) (3*c-2*b+2*a) $
                  f p (2*c+b+2*a) (2*c+2*b+a) (3*c+2*b+2*a) (v+1) in
    if a < b then f p (2*c+b-2*a) (2*c+2*b-a) (3*c+2*b-2*a) z else z

