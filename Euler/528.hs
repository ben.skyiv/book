import Data.List ( subsequences )
import Math.Combinat.Numbers ( binomial )
s n k b = sum $ map (\x -> (-1)^(length x) * binomial
  (n + k - sum [b^m+1 | m <- x]) k) $ subsequences [1..k]
main = print $ mod (sum [s (10^k) k k | k <- [10..15]]) $ 10^9+7

