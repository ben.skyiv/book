import Text.Printf ( printf )

main = let eps = 1e-12 :: Double in printf "%.12f\n" $ fst $ until
  (\(a,b) -> b-a < eps/2) (\(a,b) -> let x = (a+b)/2 in if f x < 0
  then (x,b) else (a,x)) (1, 2) where
  f r = 4700*r**5001-4701*r**5000-2e11*r*r+(4e11+300)*r-2e11-299

-- plot [x=1.0021:1.0025] 4700*x**5001-4701*x**5000-2e11*x**2+(4e11+300)*x-2e11-299
-- (1+1e-12, 0)
-- (1.1    , 4.311204598665878e209)
-- (1.0021, -563406.8)
-- (1.0025, 1589979.4)
-- (1.0023221086326854,-0.00094604492)
-- (1.0023221086331402, 0.00128173828)

