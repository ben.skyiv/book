using System;

sealed class E400
{
  static void Main ()
  {
    long m = 1000000000000000000;
    int n = 10000, z = 1 << (int)Math.Log(n, 2), l = 0, r = 0;
    long[] a = new long[z], b = new long[z], c = new long[z];
    for (var i = 0; i < n; i++)
    {
      c[0] = 1;
      for (var j = 0; j < z-1; j++) c[j+1] = (a[r^j]+b[l^j]) % m;
      var s = b; b = a; a = c; c = s;
      var t = (l^r) + 1; r = l; l = t;
    }
    Console.WriteLine(a[1]);
  }
}

