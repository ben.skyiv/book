small p (x:xs) = if p x then [] else x : small p xs
primes = 2:3:5:[ n | n <- [7,9..], all (\m -> 0 /= mod n m) $
  small (>ceiling(sqrt $ fromIntegral n)) primes ]
main = let n = 10001 in print $ primes !! (n - 1)
-- 0m0.161s

