#include <stdio.h>

long g(long n)
{
  long z = n;
  for (long i = 2; ; i++) {
    long j = n / i, k = i - 1;
    z += (n / k - j) * k;
    if (i > j) break;
    if (i < j) z += j;
  }
  return z;
}

int main(void)
{
  printf("%ld\n", g((long)1e12));
}

