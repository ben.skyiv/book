using System;
using System.Numerics;

static class E672
{
  static void Main()
  {
    BigInteger z = 1; int n = (int)1e2;
    for (int i = 1; i < n; i++) z *= 7;
    BigInteger w = z;
    z = (7 * z - 1) / 11;
    z = w - z; w /= 7;
    for (int k, i = n - 2; i >= 0; i--, z -= k * w, w /= 7) {
      k = (int)(z / w);
      Console.Write(k + " ");
      if ((n - i - 1) % 10 == 0) Console.WriteLine();
    }
    Console.WriteLine();
  }
}

