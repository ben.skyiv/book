// http://www.mathblog.dk/project-euler-143-investigating-the-torricelli-point-of-a-triangle/
using System;
using System.Collections.Generic;

sealed class E143
{
  static void Main()
  {
    Console.WriteLine(new E143().Compute(120000));
  }

  int Compute(int n)
  {
    var z = 0;
    var bs = GetB(n, GetS(n));
    for (var i = 0; i < n; i++) if (bs[i]) z += i;
    return z;
  }

  List<Tuple<int, int>> GetS(int n)
  {
    var s = new List<Tuple<int, int>>();
    for (int m = (int)Math.Sqrt(n), u = 1; u < m; u++)
      for (var v = 1; v < u; v++) {
        if (Gcd(u, v) != 1 || (u - v) % 3 == 0) continue;
        int a = 2 * u * v + v * v, b = u * u - v * v;
        if (a + b > n) break;
        for (var k = 1; k * (a + b) < n; k++) {
          s.Add(new Tuple<int,int>(k*a, k*b));
          s.Add(new Tuple<int,int>(k*b, k*a));
        }
      }
    s.Sort();
    return s;
  }
  
  bool[] GetB(int n, List<Tuple<int, int>> s)
  {
    var idx = new int[n];
    for (var i = 0; i < n; i++) idx[i] = -1;
    for (var i = 0; i < s.Count; i++)
      if(idx[s[i].Item1] == -1)
        idx[s[i].Item1] = i;
    var bs = new bool[n];
    for (var i = 0; i < s.Count; i++) {
      int a = s[i].Item1, b = s[i].Item2;
      List<int> p = GetV(a, idx, s), q = GetV(b, idx, s);
      for (var j = 0; j < p.Count; j++)
        if (q.IndexOf(p[j]) != -1 && a + b + p[j] < n)
          bs[a + b + p[j]] = true;
    }
    return bs;
  }

  List<int> GetV(int k, int[] idx, List<Tuple<int, int>> s)
  {
    var v = new List<int>();
    for (var i = idx[k]; i < s.Count && s[i].Item1 == k; i++)
      v.Add(s[i].Item2);
    return v;
  }

  int Gcd(int a, int b) { return b == 0 ? a : Gcd(b, a % b); }
}

