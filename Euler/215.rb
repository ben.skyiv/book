Wid=32
Hgt=10

def dovalid(cnf,n=0)
  if n==Hgt
    #test for cracks
    (1..Hgt-1).each{|i| return if cnf[i]==0 and cnf[i-1]==0}
    yield(cnf)
  else
    dovalid(cnf,n+1){|lcnf| yield(lcnf)}
    if cnf[n]==2
      cnf[n]=0
      dovalid(cnf,n+1){|lcnf| yield(lcnf)}
      cnf[n]=2
    end
  end
end

sol=Hash.new{0}
sol[Array.new(Hgt,0)]=1
(Wid-1).times{
  solnew=Hash.new{0}
  sol.each{|cnf,num|
    cnf.collect!{|e| e==2 ? 0 : e+1}
    dovalid(cnf){|vcnf|
      lvcnf=vcnf.clone
      solnew[lvcnf]+=num
    }
  }
  sol=solnew
}
sum=0
sol.each{|comb,num| sum+=num if !comb.include?(0) }
p sum

