#include <stdio.h>

int eq_s2(int n, long i)
{
  int d, v = 1;
  for (long j = 1; j != i && v <= n; v++, j = (j << 1) + d)
    if ((d = (j > i)) != 0) j -= i;
  return v == n;
}

int main(void)
{
  int n = 60; long i, m = 1L << n / 2 - 2, z = 0;
  for (long j = 1L << (n-1), k = 1; (i = j / k) > m; j++, k += 2)
    if (j % k == 0 && eq_s2(n, i)) z += i;
  for (; i > 0; i--) if (eq_s2(n, i)) z += i;
  printf("%ld\n", z << 1);
}
