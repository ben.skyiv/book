import Control.Monad ( foldM )
import Data.Array ( (!),listArray )
import Data.List ( sort )
import Math.NumberTheory.Primes.Testing ( isPrime )

n = 10^12; m = 2^32; n1 = length a1
a1 = drop 3 $ sort $ filter isPrime $ map succ
  $ foldM (\k p -> takeWhile (<n) $ iterate (*p) k) 1 [2,3,5]
a2 = listArray (1, n1) a1; a p = takeWhile (<n) $ map (p^) [0..]
s v k = flip mod m $ sum $ map (\(i,z) -> z + s z (succ i))
  $ takeWhile ((<=n) . snd) [(i, v*(a2!i)) | i <- [k .. n1]]
main = print $ flip mod m $ sum
  [z + s z 1 | i <- a 2, j <- a 3, k <- a 5, let z = i*j*k, z <= n]

