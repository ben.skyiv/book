using System;
using System.Collections.Generic;
using Skyiv;

static class E602
{
  static readonly NrRandom rand = new NrRandom();

  static IEnumerable<double> GetProbabilities()
  {
    for (var i = 2; ; i += i)
      for (var j = 1; j < i; j += 2)
        yield return (double)j / i;
  }
  
  static double[] GetProbabilities(int n)
  {
    var ps = new double[n];
    int i = 0;
    foreach (var p in GetProbabilities()) {
      if (i >= n) break;
      ps[i++] = p;
    }
    return ps;
  }
  
  static long E1(int n, double p)
  {
    var a = new int[n];
    while (rand.NextDouble() < p)
      for (var i = 0; i < n; i++)
        if (rand.NextDouble() >= p)
          a[i]++;
    long z = 1;
    foreach (var i in a) z *= i;
    return z;
  }

  static double E(int n, double p)
  {
    int times = 1000000; long z = 0;
    for (var i = 0; i < times; i++) z += E1(n, p);
    return (double)z / times;
  }

  static void Main()
  {
    int n = 3;
    var ps = GetProbabilities(10);
    foreach (var p in ps) {
      double e1 = p * (p * (p + 4) + 1), e2 = E(n, p);
      Console.WriteLine("e({0}, {1}) = {2} ({3})", n, p, e1, e2);
    }
  }
}

