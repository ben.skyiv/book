import Math.NumberTheory.Powers.Squares ( isSquare' )

main = let n = 10^6 in print $ pred $ snd $ until ((>=n) . fst)
   (\(c,a) -> (c + sum [if s > a+1 then a+1 - div (s+1) 2 else
   div s 2 | s <- [2..a+a], isSquare' $ a*a+s*s], a+1)) (0,1)

