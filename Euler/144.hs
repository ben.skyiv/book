f t@(z,xA,yA,x0,y0)
  | abs x0 <= 0.01 && y0 > 0 = t
  | otherwise = let
    sA = (y0-yA)/(x0-xA); s0 = -4*x0/y0; tA = (sA-s0)/(1+sA*s0)
    sB = (s0-tA)/(1+tA*s0); iB = y0-sB*x0
    a = 4+sB*sB; b = 2*sB*iB; c = iB*iB-100; d = sqrt $ b*b-4*a*c
    a1 = (-b-d)/(2*a); a2 = (-b+d)/(2*a)
    x1 = if abs (a1-x0) > abs (a2-x0) then a1 else a2
    in f (z+1, x0, y0, x1, sB*x1+iB)
main = print $ (\(z,_,_,_,_) -> z) $ f (0, 0.0, 10.1, 1.4, -9.6)

