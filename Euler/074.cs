using System;
using System.Collections.Generic;

sealed class E074
{
  static readonly int[] factorials = { 1,1,2,6,24,120,720,5040,40320,362880 };

  int DigitFactorialSum(int n)
  {
    var sum = 0;
    for (; n > 0; n /= 10) sum += factorials[n % 10];
    return sum;
  }
  
  int Compute(int limit)
  {
    var chains = new Dictionary<int, int>();
    var dict = new Dictionary<int, int>();
    for (var i = 1; i <= limit; i++)
    {
      dict.Clear();
      for (int k = 1, len, b, a = i; ; a = b, k++)
      {
        dict.Add(a, k);
        b = DigitFactorialSum(a);
        if (chains.TryGetValue(b, out len))
        {
          foreach (var kvp in dict) chains[kvp.Key] = k + 1 - kvp.Value + len;
          break;
        }
        if (dict.TryGetValue(b, out len))
        {
          foreach (var kvp in dict)
            chains[kvp.Key] = (kvp.Value >= len) ? (k+1-len) : (k+1-kvp.Value);
          break;
        }
      }
    }
    var count = 0;
    foreach (var kvp in chains)
    {
      if (kvp.Key > limit) continue;
      if (kvp.Value == 60) count++;
    }
    return count;
  }

  static void Main()
  {
    Console.WriteLine(new E074().Compute(1000000));
  }
}
