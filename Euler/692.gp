\\ http://oeis.org/A139764
\\ h(n) = n if n is a Fibonacci number, else h(n-(largest Fibonacci number < n)).
n = fibonacci(80) \\ 23416728348467685
h(n)=my(f); forstep(k=log(n*sqrt(5))\log(1.61803)+2, 2, -1, f=fibonacci(k); if(f<=n, n-=f; if(!n, return(f)); k--))
g(n)=sum(i=1,n,h(i))
f(n)=g(fibonacci(n))
