// csc 673a.cs ../lib/Permutation.cs ../lib/Utils.cs && mono 673a.exe
using System;
using Skyiv.Utils;

static class E673a
{
  static bool Equal((int,int)[] a, (int,int)[] b)
  {
    Array.Sort(b);
    for (var i = 0; i < a.Length; i++)
      if (a[i] != b[i]) return false;
    return true;
  }
  
  static (int,int)[] Make((int,int)[] b, int[] a)
  {
    var z = new (int,int)[b.Length];
    for (var i = 0; i < b.Length; i++) {
      var (p,q) = b[i];
      z[i] = (a[p-1],a[q-1]);
    }
    return z;
  }

  static void Main()
  {
    (int,int)[] b0 = { (1,3), (4,6) };
    (int,int)[] d0 = { (2,3), (5,6) };
    foreach (var a in Permutation.Generate(6)) {
      var b1 = Make(b0, a);
      var d1 = Make(d0, a);
      if (!Equal(b0, b1) || !Equal(d0, d1)) continue;
      Console.WriteLine("{0} {1} {2}",
        a.ToArrayString(), b1.ToArrayString(), d1.ToArrayString());
    }
  }
}
