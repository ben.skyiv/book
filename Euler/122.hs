import Data.List ( nub )
import Data.Array ( (!),(//),bounds,elems,listArray )
main = let n = 200 in print . sum . elems $ f 2 [2,1]
  $ listArray (1,n) $ 0:1: repeat (maxBound::Int) where
  f d s a = foldl g a $ nub $ filter (> head s) [i+j | i <- s, j <- s] where
    g a e = if e > (snd $ bounds a) then a else case compare (a ! e) d of
      GT -> f (d+1) (e:s) $ a // [(e,d)]; EQ -> f (d+1) (e:s) a; LT -> a

