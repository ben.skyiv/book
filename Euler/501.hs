import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Counting ( primeCount )

main = let n = 10^12 in print $ sum [sum [sum $ primeCount (div n (p^3))
  : [(primeCount $ div n $ p*q) - (primeCount q) | q <- takeWhile ((>)
  $ floor $ sqrt $ fromIntegral $ div n p) $ dropWhile (<=p) primes] |
  p <- takeWhile ((>) $ floor $ fromIntegral n ** (1/3)) primes],
  negate $ primeCount $ floor $ fromIntegral n ** (1/4),
  primeCount $ floor $ fromIntegral n ** (1/7)]

