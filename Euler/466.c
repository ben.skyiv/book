#include <stdio.h>

int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }
long count(long n, int ij, int k) { return n * k / ij; }

int main(void)
{
  int m = 7; long n = 21;
  long z = n;
  for (int i = 2; i <= m; i++) {
    long n9 = n - n / i;
    for (int j = 2; j < i; j++) {
      int ij = i/gcd(i,j)*j;
      long n1 = count(n, ij, j-1);
      long n2 = count(n, ij, j);
      n9 -= n2 - n1;
    }
printf("%ld ", n9);
    z += n9;
  }
  printf("%ld\n", z);
}

