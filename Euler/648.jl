using Nemo

function isSquare(n); i = isqrt(n); i * i == n; end

function F(n, m = 10^9)
  R, x = PowerSeriesRing(ResidueRing(ZZ,m),n+1,"x")
  G0, G1, G, s = x, 1-x, 0*x, 1
  for i in 3 : (n + 2) * (n + 2)
    G2 = 0*x
    if !isSquare(i-1); G2 += G1 * x; end
    if !isSquare(i-2); G2 += G0 * (1-x); end
    if isSquare(i); G += s * G2; s += 1; end
    G0 = G1; G1 = G2
  end
  sum([coeff(G, i) for i = 0 : n])
end

@time println(F(1000))

