using System;
using System.Collections.Generic;

sealed class E482
{
  int Gcd(int a, int b) { return (b == 0) ? a : Gcd(b, a % b); }
  void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }
  
  HashSet<Tuple<int,int,int>> Step1(int p)
  {
    var set = new HashSet<Tuple<int,int,int>>();
    for (int n9 = (int)(p/(6*Math.Sqrt(3))), n = 1; n < n9; n++)
      for (int n2 = n*n, m9 = (int)Math.Sqrt(n9-n2), m = n + 1; m < m9; m++)
      {
        if ((m - n) % 2 == 0 || Gcd(m, n) > 1) continue;
        int m2 = m*m, a = 2*m*n, b = m2 - n2, c = m2 + n2;
        if (a > b) Swap(ref a, ref b);
        for (var k = 1; k*a <= n9; k++) set.Add(Tuple.Create(k*a, k*b, k*c));
      }
    return set;
  }
  
  Dictionary<int, List<Tuple<int,int>>> Step2(int p)
  {
    var dict = new Dictionary<int, List<Tuple<int,int>>>();
    foreach (var i in Step1(p))
    {
      List<Tuple<int,int>> list;
      if (!dict.TryGetValue(i.Item1, out list))
        dict.Add(i.Item1, list = new List<Tuple<int,int>>());
      list.Add(Tuple.Create(i.Item2, i.Item3));
      if (!dict.TryGetValue(i.Item2, out list))
        dict.Add(i.Item2, list = new List<Tuple<int,int>>());
      list.Add(Tuple.Create(i.Item1, i.Item3));
    }
    foreach (var kvp in dict) kvp.Value.Sort();
    return dict;
  }

  long Compute(int p)
  {
    long sum = 0;
    foreach (var kvp in Step2(p))
    {
      var d = kvp.Value;
      long r = kvp.Key, r2 = r * r;
      for (int n = d.Count, i = 0; i < n; i++)
        for (int u = d[i].Item1, x = d[i].Item2, j = i; j < n; j++)
        {
          long v = d[j].Item1, y = d[j].Item2;
          long xyr = x * y * r, uvr = u * v - r2;
          if (uvr <= 0 || xyr % uvr != 0) continue;
          long z = xyr / uvr;
          if (z < y) continue;
          long w = (long)Math.Sqrt(z * z - r2), p2 = 2 * (u + v + w);
          if (p2 <= p) sum += p2 + x + y + z;
        }
    }
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E482().Compute(10000000));
  }
}

/* sum:884,667 count:103
$ dmcs 482.cs && time mono 482.exe
1400824879147

real	1m32.586s
user	1m30.911s
sys	0m1.423s
*/

