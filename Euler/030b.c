#include <stdio.h>

static int a[10];

int sum(int n)
{
  int z = 0;
  for (; n > 0; n /= 10) z += a[n % 10];
  return z;
}

int main()
{
  printf("Start\n"); fflush(stdout);
  for (int i = 0; i <= 9; i++)
    a[i] = i*i*i*i*i*i*i*i*i;
  int z = 0;
  for (int i = 2; i <= (int)1e9; i++) {
    if (sum(i) != i) continue;
    z += i;
    printf("%d\n", i); fflush(stdout);
  }
  printf("[%d]\n", z); fflush(stdout);
  return 0;
}

