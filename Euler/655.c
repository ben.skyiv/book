#include <stdio.h>

void out_int128(__int128 n) // n > 0
{ if (n == 0) return; out_int128(n/10); putchar(n%10+'0'); }

int is_palindrome(__int128 n)
{
  static char a[64]; int i = -1;
  for (; n > 0; n /= 10) a[++i] = n % 10;
  for (int j = 0; j < i; j++, i--) if (a[i] != a[j]) return 0;
  return 1;
}

int main(void)
{
  int m = 10000019;
  for (__int128 i = m; ; i += m)
    if (is_palindrome(i))
    { out_int128(i); printf(" %ld\n", (long)(i/m)); fflush(stdout); }
}

