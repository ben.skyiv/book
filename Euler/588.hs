f 0 = 1; f n = uncurry g $ divMod n 8
g q 1 = 5 * f q
g q 3 = 2 * f (4*q+1) - 3 * f q
g q 5 = 3 * f (2*q+1) + 2 * f q
g q 7 = 2 * f (4*q+1) + 3 * f (2*q+1) - 6 * f q
g q r = f $ 4*q + div r 2
main = print $ sum [f $ 10^i | i <- [1..18]]

