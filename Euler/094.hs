import Math.NumberTheory.Powers.Squares ( isSquare )

main = let n = 10^9 in print $ sum $ concatMap (\x -> [
  if isSquare $ x*x - div ((x-1)^2) 4 then 3*x-1 else 0,
  if isSquare $ x*x - div ((x+1)^2) 4 then 3*x+1 else 0])
  [3, 5 .. div n 3 - 1]

