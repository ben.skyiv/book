using System;
public class Euler217 {
  public static void Main() {
    int N = 47, MOD = (int)Math.Pow(3, 15), MAX = N + 3, m = 5 * MAX, z = 0;
    int[,] s = new int[MAX, 10 * MAX], c = new int[MAX, 10 * MAX];
    for (int n = 1; n <= N; n++)
      for (int i = 0; i <= n; i++) {
        for (int j = 10 - m; j < m - 10; j++)
          if (i == 0) c[i, j+m] = (j == 0) ? 1 : 0;
          else {
            s[i, j + m] = 0; c[i, j + m] = 0;
            int w = (n%2==0)?(i>n/2)?1:-1:(i<=n/2)?-1:(i>n/2+1)?1:0;
            for (int d = 0; d <= 9; d++) {
              if (d == 0 && i == 1) continue;
              int ct = c[i - 1, j - w * d + m];
              c[i, j + m] = (c[i, j + m] + ct) % MOD;
              s[i,j+m] = (s[i,j+m] + 10*s[i-1,j-w*d+m] + ct*d) % MOD;
            }
          }
        z = (z + s[n, m]) % MOD;
      }
      Console.WriteLine(z);
  }
}

