#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static char *a; // 0m0.003s

void sieve(int max)
{
  a = malloc(max);
  int high = (int)sqrt(max);
  for (int i = 2; i <= high; i++)
    if (a[i] == 0)
      for (int j = i * i; j <= max; j += i)
        a[j] = 1;
}

int nthprime(int n)
{
  sieve((int)((log(n) + log(log(n))) * n));
  for (int k = 1, i = 3; ; i += 2) {
    if (k == n) return i - 2;
    if (a[i] == 0) k++;
  }
}

int main(int argc, char *argv[])
{
  printf("%d\n", nthprime((argc > 1) ? atoi(argv[1]) : 10001));
  free(a);
  return 0;
}
