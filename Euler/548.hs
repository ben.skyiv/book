import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Factorisation ( factorise )

f1 n = takeWhile (<=n) $ map (\p -> p * 2^(2*p-2)) $ tail primes
f2 n = takeWhile (<=n) $ map snd $
  filter (\(x, _) -> length x == 2 && all ((== 1) . snd) x)
  [(factorise z, z*2^k) | m <- [1..], let k = 2*m; z = 2*m*m+6*m+3]
main = let n = 10^16 in print $ f1 n ++ f2 n

