using Combinatorics

const FA = filter(n -> issubset(digits(n), 1:3) || n == 0, big(0):100)
v(k) = Tuple(Int(i == k) for i in 1:3)

# Ways of appending k digits to sequence of u1 of 1s, u2 of 2s, and u3 of 3s to make it a 123-number
function g(k, u::Tuple)
    F1, F2, F3 = (filter(x -> 0 ≤ x ≤ k, FA .- w) for w in u)
    reduce(+, multinomial(f1, f2, k - f1 - f2) for f1 in F1, f2 in F2 if k - f1 - f2 in F3; init=0)
end

function F(n)
    k, res, u = 0, zero(n), (0, 0, 0)
    while (x = g(k += 1, u)) ≤ n  n -= x end

    for l in k-1:-1:0
        i = 0
        while n > (x = g(l, u .+ v(i += 1)))  n -= x end
        u = u .+ v(i)
        res = 10res + i
    end
    res
end

@show F(big(111111111111222333)) % big(123123123)
