using System;
using System.Collections.Generic;

static class E681
{
  static void Main()
  {
    int n = 100;
    long z = 0;
    var areas = new List<(int,int,int,int)>[n+1];
    for (var i = 1; i <= n; i++) areas[i] = new List<(int,int,int,int)>();
    for (int a = 1; a <= n; a++)
      for (int b = a; b <= n; b++)
        for (int c = b; c <= n; c++)
          for (int d9 = Math.Min(n, a+b+c-1), d = c; d <= d9; d++) {
            double s = (a + b + c + d) / 2.0;
            double area = Math.Sqrt((s-a)*(s-b)*(s-c)*(s-d));
            int area2 = (int)(area + 0.5);
            if (area2 > n) continue;
            if (area2 != area) continue;
            areas[area2].Add((a,b,c,d));
            z += a + b + c + d;
          }
    Console.WriteLine("SP({0}) = {1}", n, z);
    for (var i = 1; i <= n; i++) {
      Console.Write("{0,3}: {1,2}: ", i, areas[i].Count);
      int z0 = 0;
      foreach (var t in areas[i]) {
        var (a,b,c,d) = t;
        z0 += a + b + c + d;
        int s0 = (a + b + c + d) / 2;
        var e = new int[] {s0-a,s0-b,s0-c,s0-d};
        Array.Sort(e);
        Console.Write("{0,2} {1,2} {2,2} {3,2}|", e[0], e[1], e[2], e[3]);
      }
      Console.WriteLine("({0})", z0);
    }
  }
}
