using System;
using System.Numerics;

sealed class E015
{
  int size;
  BigInteger[,] a;
  
  E015(int size)
  {
    this.size = size;
    a = new BigInteger [size+1, size+1];
    for (var i = 0; i <= size; i++) a[0, i] = a[i, 0] = 1;
    for (var y = 1; y <= size; y++)
      for (var x = 1; x <= size; x++)
        a[x, y] = a[x-1, y] + a[x, y-1];
  }

  BigInteger Compute()
  {
    return a[size, size];
  }

  static void Main(string[] args)
  {
    Console.WriteLine(new E015(int.Parse(args[0])).Compute());
  }
}
