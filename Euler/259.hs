import Data.Array ( (!),listArray,range )
import Data.Ratio ( denominator,numerator )
import Data.Set as Set ( foldl,fromList,toList )

a = let b = ((1,1),(9,9)) in listArray b $ map (toList . fromList . uncurry f) $ range b

f m n = ((fromIntegral $ foldl1 (\x y -> 10*x + y) [m..n]):)
  $ concat $ [[u+v,u-v,u*v] ++ if v == 0 then [] else [u/v]
  | z <- [m..n-1], u <- a ! (m,z), v <- a ! (z+1,n)]

main = print $ Set.foldl (+) 0 $ fromList $ map numerator
  $ filter (\x -> x > 0 && denominator x == 1) $ f 1 9

