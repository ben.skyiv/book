import Data.List ( (\\),nub,transpose )

f xs [] = xs; f xs cs = if null zs then xs else f (zs:xs)
  $ filter (not . null) $ map (\\zs) cs where
  zs = g $ map nub $ transpose cs
  g [x] = x; g (x:y:ys) = (x \\ y) \\ g (y:ys)

main = do 
  s <- readFile "079.txt"
  putStrLn $ head $ sequence $ reverse $ f [] $ words $ s

