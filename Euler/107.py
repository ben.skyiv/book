edges = set()
for v1,line in enumerate(open('107.txt')):
  for v2,weight in enumerate(line.split(',')):
    if weight.strip() != '-':
      i,j = v1 > v2 and (v1,v2) or (v2,v1)
      edges.add((int(weight), i, j))
gs = dict((i,i) for i in range(40))
total = 0 # Kruskal's algorithm
for w,v1,v2 in sorted(edges):
  if (gs[v1] != gs[v2]):
    for v in [v for (v,g) in gs.items() if g == gs[v2]]:
      gs[v] = gs[v1]
    total += w
    if(len(set(gs.values())) == 1): break
print(sum(w for (w,_,_) in edges) - total)

