import Math.Combinat.Numbers ( binomial,catalan )

main = let n = 12 in print $ sum $ map (\i -> div (binomial n i *
  binomial (n-i) i) 2 - binomial n (i+i) * catalan i) [2 .. div n 2]

