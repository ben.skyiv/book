// http://en.wikipedia.org/wiki/Farey_sequence
// http://en.wikipedia.org/wiki/Euler%27s_totient_function
// http://en.wikipedia.org/wiki/M%C3%B6bius_function

using System;

sealed class E072
{
  int[] sieve;    // 0 for prime, else min prime factor
  sbyte[] mobius; // value in 0, -1, 1

  int Gcd(int a, int b)
  {
    for (int t; b != 0; a = b, b = t) t = a % b;
    return a;
  }

  void Initialize(int max)
  {
    int limit = (int)Math.Sqrt(max);
    sieve = new int[max + 1];
    mobius = new sbyte[max + 1];
    for (int i = 2; i <= limit; i++)
      if (sieve[i] == 0)
        for (int j = i * i; j <= max; j += i)
          sieve[j] = i;
  }
  
  sbyte Mobius(int n)
  {
    if (n == 1) return 1;
    if (sieve[n] == 0) return -1;
    int a = sieve[n], b = n / sieve[n];
    return  (sbyte)((Gcd(a, b) != 1) ? 0 : mobius[a] * mobius[b]);
  }
  
  long Compute(int n)
  {
    Initialize(n);
    long k, sum = 0;
    for (var i = 1; i <= n; i++)
      sum += (mobius[i] = Mobius(i)) * (k = n / i) * k;
    return (sum - 1) / 2;
  }

  static void Main()
  {
    Console.WriteLine(new E072().Compute(1000000));
  }
}

