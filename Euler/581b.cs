using System;
using System.Numerics;
using System.Collections.Generic;

static class E581
{
  static readonly int[] primes = {2,3,5,7,11,13,17};//,19,23,29,31,37,41,43,47};
  static readonly int m = primes.Length;

  static int MinIndex(BigInteger[] xs)
  {
    int idx = 0; BigInteger min = xs[0];
    for (int i = 1; i < m; i++)
      if (min > xs[i]) min = xs[idx = i];
    return idx;
  }

  static IEnumerable<BigInteger> Smooth()
  {
    yield return 1;
    var xs = new BigInteger[m];
    var qs = new Queue<BigInteger>[m];
    for (var i = 0; i < m; i++)
      (qs[i] = new Queue<BigInteger>()).Enqueue(xs[i] = primes[i]);
    for (int idx; ; ) {
      var x = xs[idx = MinIndex(xs)];
      yield return x;
      qs[idx].Dequeue();
      for (var i = idx; i < m; i++) qs[i].Enqueue(x * primes[i]);
      xs[idx] = qs[idx].Peek();
    }
  }

  static void Out(BigInteger sum, long x, bool mark = false)
  {
    Console.WriteLine("{0:HH:mm}: {1} {2} {3}",
      DateTime.Now, x, sum, mark ? "*" : "");
  }

  static void Main()
  {
    long R = (long)1e10, x0 = 0, x = 1;
    var ys = Smooth().GetEnumerator();
    for (BigInteger sum = 0, z = 1; ; ) {
      if (x0 != x && x % R == 0) Out(sum, x0 = x, true);
      if (z > ys.Current) ys.MoveNext();
      else if (z < ys.Current) z += ++x;
      else { Out(sum += x, x); z += ++x; ys.MoveNext(); }
    }
  }
}

