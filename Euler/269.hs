import Data.Ratio ( (%) )
import Data.Map ( toList,fromListWith )

g [c,b,a] [f,e,d] = d+(c+b+a)*e+(a*b+b*c+c*a)*f
ok _ [d] = elem d [0..9]; ok [b,a] [e,d] = all (flip elem [0..9]) [e, d+(b+a)*e]
ok x@[c,b,a] y@[f,e,d] = all (flip elem [0..9]) [f, e+(c+b+a)*f, g x y]
next [a] [d] = [ceiling $ -d%a .. floor $ (9-d)%a]
next [b,a] [e,d] = let z = -(d+(b+a)*e)%(b*a) in [ceiling z .. floor $ z+9%(b*a)]
next x@[c,b,a] y@[f,e,d] = let z = -(g x y)%(c*b*a) in [ceiling z .. floor $ z+9%(c*b*a)]
count n [] = 10^n; count n (0:s) = count (n-1) s
count n s = pred $ sum $ map snd $ filter (ok s . fst) $ (!! (n - length s)) $
  iterate (toList . fromListWith (+) . concatMap (\(bs,c) -> [(b : init bs, c)
  | b <- next s bs])) [(take (length s) [0,0..], 1)]
roots m n | n == 0 = [[]] | True = concat [map (z:) $ roots (z+1) $ n-1 | z <- [m..9]]
main = let n = 16 in print $ - sum (map (count n . (0:)) $ roots 1 3)
  - sum [(-1)^i * (sum $ map (count n) $ roots 0 i) | i <- [1..3]]

