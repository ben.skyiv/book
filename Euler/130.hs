import Data.List ( genericLength )
import Math.NumberTheory.Primes.Testing ( isPrime )

main = let n = 25 in print $ sum $ take n $ map fst $ filter
  (\(x,a) -> a /= 0 && mod (x-1) a == 0) $ map (\z -> (z,
  if gcd z 10 /= 1 then 0 else (1+) $ genericLength $
  takeWhile (/=0) $ iterate (\x -> mod (x*10+1) z) 1)) $
  filter (not . isPrime) [5..]

