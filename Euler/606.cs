using System;
using System.Numerics;
using System.Collections.Generic;
using Skyiv.Utils;

static class E606
{
  static void Main()
  { // Compute S(10^n) mod m, n:6,12,18,24,30,36.
    int m = (int)1e9, n = 36;
    var r = (int)Math.Pow(10, n / 6);
    var s = (long)r * r;
    var a = new long[r + r - 1];
    for (var i = 0; i < r; i++) a[i] = s / (i+1);
    for (var i = r; i < r + r - 1; i++) a[i] = r + r - i - 1;
    var b = new Dictionary<long, int>();
    foreach (var v in a) {
      var w = (BigInteger.One + v) * v / 2;
      b.Add(v, (int)((w * w - 1) % m));
    }
    var ps = Primes.GetPrimes(0, r);
    foreach (var p in ps) {
      long p2 = (long)p * p, bp = b[p - 1];
      foreach (var v in a) {
        if (v < p2) break;
        b[v] = (int)((b[v] - p2 * p % m * (b[v/p]-bp)) % m);
      }
    }
    long q, z = 0;
    foreach (var p in ps) {
      if (p > (q = s / p)) break;
      z = (z + (long)(b[q] - b[p]) * p % m * p % m * p) % m;
    }
    Console.WriteLine(z);
  }
}

