import java.util.PriorityQueue;

public class E247 {
	public static void main(String[] args) {
		PriorityQueue<Square> queue = new PriorityQueue<Square>();
		queue.add(new Square(1.0,.0,(Math.sqrt(5)-1)/2,0,0));
		for (int n = 1; n < 1000000; n++) {
			Square r = queue.poll();
			if (r.below == 3 && r.left == 3) System.out.print(n + " ");
			double h1 = r.h + r.size;
			double s1 = (Math.sqrt((r.x+h1)*(r.x+h1)-4*(r.x*h1-1))-r.x-h1)/2;
			double x2 = r.x + r.size;
			double s2 = (Math.sqrt((x2+r.h)*(x2+r.h)-4*(x2*r.h-1))-x2-r.h)/2;
			queue.add(new Square(r.x, h1, s1, r.left, r.below+1));
			queue.add(new Square(x2, r.h, s2, r.left+1, r.below));
		}
	}

	public static class Square implements Comparable<Square> {
		public double x, h, size;
		public int left, below;
		
		public Square(double x, double h, double size, int left, int below) {
			this.x = x;
			this.h = h;
			this.size = size;
			this.left = left;
			this.below = below;
		}

		public int compareTo(Square o) {
			return (this.size < o.size) ? 1 : -1;
		}
	}
}

