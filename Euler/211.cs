using System;

sealed class E211
{
  static void Main(string[] args)
  {
    int sum = 1, limit = (args.Length > 0) ? int.Parse(args[0]) : 64000000;
    var σ2 = new long[limit];
    for (var n = 2; n < limit; n++)
    {
      for (var i = n; i < limit; i += n) σ2[i] += (long)n * n;
      var d = Math.Sqrt(++σ2[n]);
      if (d == (long)d) sum += n;
    }
    Console.WriteLine(sum);
  }
}
