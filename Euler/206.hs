main = print $ search $ 10 * div (ceiling $ sqrt 1929394959697989990) 10

search n
  | f (n^2) = n
  | otherwise = search $ n - 10

f n = all (\(x,y) -> x==y) $ filter (\(x,_) -> x/='_') $ zip "1_2_3_4_5_6_7_8_9" $ show n

