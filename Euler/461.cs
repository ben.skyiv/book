using System;

sealed class E461
{
  static readonly double target = Math.PI;

  static int GetLength(double[] f, int m, double eps)
  {
    int len = 0;
    for (short i = 0; i <= m; i++)
      for (short j = i; f[i] + f[j] <= target + eps; j++)
        len++;
    return len;
  }

  static void Main()
  {
    var n = 10000;
    var eps = 1.0 / ((long)n*n);
    var m = (int)(1 + n * Math.Log(target + eps + 1));
    var f = new double[m + 1];
    for (var i = 0; i <= m; i++) f[i] = Math.Exp(i/(double)n) - 1;
    var len = GetLength(f, m, eps);
    var values = new double[len];
    var keys = new Tuple<short, short>[len];
    var k = 0;
    for (short i = 0; i <= m; i++)
      for (short j = i; ; j++, k++)
      {
        var v = f[i] + f[j];
        if (v > target + eps) break;
        values[k] = v;
        keys[k] = Tuple.Create(i, j);
      }
    Array.Sort(values, keys);
    var best = 1.0;
    int i0 = -1, j0 = -1;
    for (int i = 0, j = len - 1; i <= j; )
    {
      double v = values[i] + values[j] - target, u = Math.Abs(v);
      if (best > u) { best = u; i0 = i; j0 = j; }
      if      (v > 0) j--;
      else if (v < 0) i++;
      else break;
    }
    var z = keys[i0].Item1*keys[i0].Item1 + keys[i0].Item2*keys[i0].Item2
          + keys[j0].Item1*keys[j0].Item1 + keys[j0].Item2*keys[j0].Item2;
    Console.WriteLine("{0} ({1},{2}) {3}", z, keys[i0], keys[j0], best);
  }
}

