import Data.Char ( ord )
import Data.List ( sort )

main = do
  s <- readFile "022.txt"
  let f w n = (n*) . sum . map (\x -> ord x - ord 'A' + 1) $ w
  print . sum $ zipWith f (sort $ read $ "[" ++ s ++ "]") [1..]

