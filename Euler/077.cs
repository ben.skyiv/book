using System;
using System.Collections.Generic;

sealed class E077
{
  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  int Compute()
  {
    var n = 100;
    var composite = Sieve(n);
    var a = new Dictionary<int, int>[n + 1];
    for (var i = 2; i <= n; i++)
    {
      a[i] = new Dictionary<int, int>();
      for (int v = composite[i] ? 0 : 1, p = i; p > 1; p--)
        if (!composite[p])
          a[i].Add(p, v += (p > i / 2) ? 0 : a[i - p][p]);
      if (a[i][2] > 5000) return i;
    }
    return 0;
  }

  static void Main()
  {
    Console.WriteLine(new E077().Compute());
  }
}
