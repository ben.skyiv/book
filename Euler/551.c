#include <stdio.h>

int digitsSum(long n)
{
  int z = 0;
  for (; n > 0; n /= 10) z += n % 10;
  return z;
}

int main()
{
  long n = (long)1e6, z = 1;
  for (long i = 2; i <= n; i++) z += digitsSum(z);
  printf("%ld\n", z);
}

