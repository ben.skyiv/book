#include <stdio.h>

long compute(void)
{
  long h, p, t;
  for (int i = 144; ; i++) {
    h = i * (((long)i << 1) - 1);
    for (int j = i; ; j++) {
      p = j * (3 * (long)j - 1) / 2;
      if (p > h) break;
      if (p < h) continue;
      for (int k = j; ; k++) {
        t = k * ((long)k + 1) / 2;
        if (t == p) return t;
        if (t > p) break;
      }
    }
  }
  return 0;
}

int main(void)
{
  printf("%ld\n", compute());
  return 0;
}
