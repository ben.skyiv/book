using System;

static class E581
{
  static readonly long p0 = 47, m = (long)1e12;
  static readonly int n = 2 + (int)Math.Sqrt(m + m), n2 = n / 2;
  static readonly long R = n2 * 1000L;    // only for debug
  static readonly long[] t = new long[n]; // only for Factor()

  static void Factor(long i0, int len, bool[] a, bool[] c)
  { // a[i]: is (i0+i) NOT p-smooth?
    for (int i = 0; i < len; i++) t[i] = 1;
    for (long p = 2; p * p < i0 + len; p++)
      if (!c[p])
        for (long q = p; q < i0 + len; q *= p)
          for (long i = q-1-(i0-1)%q; i < len; t[i] *= p, i += q)
            if (q == p) a[i] = p0 < p;
    for (int i = 0; i < len; i++)
      if (i + i0 != t[i]) a[i] = p0 < (i + i0) / t[i];
  }

  static void Main()
  {
    Console.WriteLine("{0:N0} {1:N0} {2:N0} {3:N0}", m, n, n2, R);
    bool[] a = new bool[n], b = new bool[n2 + 1], c = new bool[n];
    for (int i = 2; i * i < n; i++)
      if (!c[i]) for (int j = i * i; j < n; j += i) c[j] = true;
    long z = 1;
    for (long i0 = 3, i = 1; i <= m; i += n2, i0 = i + i + 1) {
      if (i % R == 1) Console.WriteLine(
        "{0:HH:mm}: {1,19:N0} {2:N0}", DateTime.Now, i, z);
      Factor(i, n2 + 1, b, c); Factor(i0, n, a, c);
      for (int j2 = 0, j = 0; j < n2; j++, j2 += 2) {
        if (a[j2]) continue;
        if (!b[j])   z += i0 + j2 - 1;
        if (!b[j+1]) z += i0 + j2;
      }
    }
    Console.WriteLine("{0:N0}", z);
  }
}
//  1e7:     1,253,782,345    1s
//  1e8:     6,227,351,613   11s
//  1e9:    22,495,633,669    2m
// 1e10:    73,845,318,116   22m
// 1e11:   412,398,142,771  552m
// 1e12: 2,227,616,372,734 3153m

