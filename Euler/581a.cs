using System;
using System.Linq;
using System.Numerics;
using System.Collections.Generic;

static class E581
{
  static readonly int[] primes = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47};
  static readonly int m = primes.Length;
  static readonly long R = 100;
  static BigInteger sum = 0;
  static long x0 = 0;

  static int MinIndex(BigInteger[] xs)
  {
    int idx = 0; BigInteger min = xs[0];
    for (int i = 1; i < m; i++)
      if (min > xs[i]) min = xs[idx = i];
    return idx;
  }

  static IEnumerable<long> Sequence()
  {
    for (long i = 1; ; i++) yield return i;
  }

  static IEnumerable<BigInteger> Smooth()
  {
    yield return 1;
    var xs = new BigInteger[m];
    var qs = new Queue<BigInteger>[m];
    for (var i = 0; i < m; i++)
      (qs[i] = new Queue<BigInteger>()).Enqueue(primes[i]);
    for (;;) {
      for (var i = 0; i < m; i++) xs[i] = qs[i].Peek();
      var idx = MinIndex(xs);
      var x = xs[idx];
      yield return x;
      qs[idx].Dequeue();
      for (var i = idx; i < m; i++) qs[i].Enqueue(x * primes[i]);
    }
  }

  static void Out(long x, bool mark = false)
  {
    Console.WriteLine("{0,2} {1} {2}", x, sum, mark ? "*" : "");
  }

  static void F(IEnumerable<long> xs, IEnumerable<BigInteger> ys)
  {
    long x = xs.First();
    BigInteger y = ys.First();
    BigInteger z = ((BigInteger.One + x) * x) / 2;
    if (x0 != x && x % R == 0) Out(x0 = x, true);
    if      (z < y) F(xs.Skip(1), ys);
    else if (z > y) F(xs, ys.Skip(1));
    else { sum += x; Out(x); F(xs.Skip(1), ys.Skip(1)); }
  }

  static void Main()
  {
    F(Sequence(), Smooth());
  }
}

