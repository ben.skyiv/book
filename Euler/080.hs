import Data.Char
import Data.Digits
main = print $ sum [ (sum . take 100 . digits 10) x | n <- [1..100],
  let z = n*10^202; x = (\x -> f x 1 x) z, x^2 /= z ] where 
  f x l h
    | l + 1 >= h = l
    | (^2) m > x = f x l m
    | otherwise  = f x m h
    where m = div (l + h) 2

