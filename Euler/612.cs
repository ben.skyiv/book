using System;
using System.Numerics;

static class E612
{
  static BigInteger P(int n, int k)
  {
    var z = BigInteger.One;
    for (var i = n - k + 1; i <= n; i++) z *= i;
    return z;
  }

  static void Main()
  {
    int m = 1000267129, e = 18;
    var a1 = new BigInteger[e + 1, 10]; a1[0, 0] = 1;
    var a2 = new BigInteger[e + 1, 10];
    for (var n = 1; n <= e; n++)
      for (var d = 1; d < 10; d++) {
        a1[n, d] = d*a1[n-1, d] + a1[n-1, d-1];
        a2[n, d-1] = (d-1)*a1[n, d];
      }
    var z = BigInteger.Zero;
    for (var n1 = 1; n1 <= e; n1++)
      for (var n2 = 1; n2 <= e; n2++)
        for (var d1 = 1; d1 <= 8; d1++)
          for (var d2 = 1; d2 <= 9 - d1; d2++)
            z += (a1[n1, d1] * a1[n2, d2]
                + a1[n1, d1] * a2[n2, d2]
                + a2[n1, d1] * a1[n2, d2])
                * P(9, d1 + d2);
    var v = BigInteger.Pow(10, e);
    Console.WriteLine("{0}", ((v-1)*(v-2) - z) / 2 % m);
  }
}

