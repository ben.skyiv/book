def mul13mod((a,b),(c,d),n): return (a*c+13*b*d) %n, (a*d+b*c) %n  
def powermod13((i,j), b, n):
    r,s = 1,0
    while b > 0:
        if b & 1 == 1: r,s=mul13mod((r,s),(i,j),n)
        i,j=mul13mod((i,j),(i,j),n)
        b//=2
    return r,s
    
X,Y,N=10^9,10^7,10^15
z=0
for m in primes(X,X+Y):
    h=(m+1)/2
    if m%13 in (1,3,4,9,10,12): pp=m-1    
    else: pp=2*(m+1)
    i,j=(3*h%m,h),(3*h%m,m-h)    
    pm=power_mod(2,N,pp)
    p=(powermod13(i,pm,m)[0]+powermod13(j,pm,m)[0]-5)%m
    while(p%6):p+=m
    z+=p/6

print z      
