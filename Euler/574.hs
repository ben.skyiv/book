import Data.Maybe ( fromJust,isJust )
import Math.NumberTheory.Primes ( primes )
import Math.NumberTheory.Powers.Squares ( integerSquareRoot,exactSquareRoot )
f p = head [(x,y,g) | let r = product $ takeWhile (<= integerSquareRoot p) primes,
  y <- [1..], let d = p*p + 4*r*y, let t = exactSquareRoot d, isJust t,
  let s = fromJust t, odd s,  let x = div (p + s) 2; g = gcd x (x - p) ]
main = print $ f 3701
{-
889156635470181925 = V(3733)
567623363587463949 = V(3769)
741775417109072814 = V(3779)
899919866101921535 = V(3797)
457419847966042781 = S(3700)
-}
