// mcs 660b14.cs 660.cs && ./660b14.exe

using System;

static class E660b14
{
  static E660 e = new E660(14);

  static int P455()
  { // (xxxx, 1xxxx, 2xxxx) (xxxx, Cxxxx, Dxxxx)
    var z = 0;
    for (var i1 = 1; i1 <= e.B - 2; i1++)
      foreach (var s in E660.Permutation(e.GetRest(i1, i1+1), 8))
        z += e.F( e.B*(e.B*(e.B*s[1] +s[2])+s[3])+s[4],
          e.B*(e.B*(e.B*(e.B*i1+s[5])+s[6])+s[7])+s[8]);
    return z;
  }

  static void Main() { Console.WriteLine(P455()); }
}

