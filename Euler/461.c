#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct { double v; int i; int j; } val_t;

int cmp(const void* a, const void* b)
{
  double d = ((val_t*)a)->v - ((val_t*)b)->v;
  return (d > 0) ? 1 : (d < 0) ? -1 : 0;
}

int get_length(const double f[], double target, int m)
{
  int len = 0;
  for (int i = 0; i <= m; i++)
    for (int j = i; f[i] + f[j] <= target; j++)
      len++;
  return len;
}

val_t* get_vals(const double f[], double target, int m, int len)
{
  val_t* vals = (val_t*)calloc(len, sizeof(val_t));
  int k = 0;
  for (int i = 0; i <= m; i++)
    for (int j = i; ; j++, k++) {
      double v = f[i] + f[j];
      if (v > target) break;
      vals[k].v = v; vals[k].i = i; vals[k].j = j;
    }
  return vals;
}

int main(void)
{
  double target = acos(-1.0);
  int n = 10000;
  int m = (int)(1 + n * log(target + 1));
  double* f = (double*)calloc(m + 1, sizeof(double));
  for (int i = 0; i <= m; i++) f[i] = exp(i/(double)n) - 1;
  int len = get_length(f, target, m);
  val_t* vals = get_vals(f, target, m, len);
printf("len:%d, m:%d, sizeof(val_t):%lu\n", len, m, sizeof(val_t));
  qsort(vals, len, sizeof(val_t), cmp);
printf("%.17lf(%d,%d)\n", vals[len-1].v,vals[len-1].i,vals[len-1].j);
  double best = 1.0;
  int i0 = -1, j0 = -1;
  for (int i = 0, j = len - 1; i <= j; ) {
    double v = vals[i].v + vals[j].v - target;
    double u = fabs(v);
    if (best > u) best = u, i0 = i, j0 = j;
    if      (v > 0) j--;
    else if (v < 0) i++;
    else break;
  }
  int z = vals[i0].i*vals[i0].i + vals[i0].j*vals[i0].j
        + vals[j0].i*vals[j0].i + vals[j0].j*vals[j0].j;
  printf("%d (%d, %d, %d, %d) %lg\n",
    z, vals[i0].i, vals[i0].j, vals[j0].i, vals[j0].j, best);
  free(vals);
  free(f);
  return 0;
}

