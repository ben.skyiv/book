import Data.Set ( toList )
import Math.NumberTheory.Primes.Factorisation ( divisors, factorise )
main = print . sum . map (sum . map (\n -> product [ a+1 | (_,a) <- factorise n ]) .
  (\n -> [ div (n*(a+b)) (a*b) | let ds = toList $ divisors n, a <- ds, b <- dropWhile
  (<a) ds, gcd a b == 1, a*b <= n ])) . takeWhile (<=10^9) . iterate (10*) $ 10
-- Call (a,b,p) a primitive tuple of equation 1/a+1/b=p/10^n
-- a and b are divisors of 10^n, gcd a b == 1, a <= b and a*b <= 10^n
-- I noticed that the number of variants with a primitive tuple
-- is equal to the number of divisors of p.
-- So I produced all possible primitive tuples per 10^n and
-- summed all the number of divisors of every p

