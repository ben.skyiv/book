m = 10**36; n = int(round(m**(1/3))); r = int(n**0.5); Primes = []
V = [n // i for i in range(1,r+1)]; V += list(range(V[-1]-1,0,-1))
S = {i: i * i * (i + 1) * (i + 1) // 4 - 1 for i in V}
for p in range(2, r + 1):
  if S[p] > S[p - 1]:
    Primes += [p]
    sp = S[p - 1]
    p2 = p * p
    for v in V:
      if v < p2: break
      S[v] -= p * p * p * (S[v // p] - sp)
Ans = 0
for p in Primes:
  j = n // p
  if p > j: break
  Ans += p * p * p * (S[j] - S[p])
print(Ans)

