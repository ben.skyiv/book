using System;
using System.Collections.Generic;

sealed class E087
{
  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }

  int Compute(int n)
  {
    var set = new HashSet<int>();
    var composite = Sieve((int)Math.Sqrt(n));
    for (int c9 = (int)Math.Pow(n,0.25), c = 2; c <= c9; c++)
      if (!composite[c])
        for (int c2=c*c,c4=c2*c2,b9=(int)Math.Pow(n-c4,1.0/3),b=2; b <= b9; b++)
          if (!composite[b])
            for (int b3=b*b*b,a9=(int)Math.Sqrt(n-c4-b3),a=2; a <= a9; a++)
              if (!composite[a])
                set.Add(c4 + b3 + a * a);
    return set.Count;
  }

  static void Main()
  {
    Console.WriteLine(new E087().Compute(50000000));
  }
}
