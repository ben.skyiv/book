import Math.NumberTheory.Primes.Testing ( isPrime )

g 0 = 0; g n = let (q,r) = divMod n 10 in r + g q
f (z,xs) _ = foldl (\(z,rs) i -> foldl (\(z,rs) n ->
  let a = n*10 + i; (q,r) = divMod a (i + g n) in
  if r == 0 then (if isPrime q then foldl (\z i ->
  let b = a*10+i in if isPrime b then b+z else z) z [1,3..9]
  else z, a:rs) else (z,rs)) (z,rs) xs) (z,[]) [0..9]
main = let n = 14 in print $ fst $ foldl f (0,[1..9]) [3..n]

