import Math.NumberTheory.Primes.Testing ( isPrime )

main = print $ succ $ (\(z,_,_,_) -> z) $ until (\(_,n,p,_) -> p /= 0
  && div (100*p) n < 10) (\(z,n,p,i) -> (z+2, n+4, foldl (\p i ->
  if isPrime i then p+1 else p) p [i+z,i+z+z..i+4*z], i+4*z)) (2,5,0,1)

