using System;
using System.Numerics;
using System.Collections.Generic;

sealed class E066
{
  int[] GetPrimes(int max)
  {
    var primes = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) primes.Add(i);
    return primes.ToArray();
  }

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }

  bool IsSquare(BigInteger z)
  {
    for (BigInteger y = (BigInteger)Math.Sqrt((double)z) - 5; ; y++)
    {
      BigInteger y2 = y * y;
      if (z == y2) return true;
      if (z < y2) return false;
    }
  }

  BigInteger GetY2(int d)
  {
    for (BigInteger y2, k = 1; ; k++)
    {
      if (IsSquare(y2 = k * (d * k - 2))) return y2;
      if (IsSquare(y2 = k * (d * k + 2))) return y2;
    }
  }

  BigInteger Compute(int limit)
  {
    BigInteger xmax2 = 0;
    foreach (var d in GetPrimes(limit))
    {
Console.Write(d + " ");
      var x2 = d * GetY2(d) + 1;
      if (xmax2 < x2) xmax2 = x2;
    }
    return xmax2;
  }

  static void Main()
  {
    Console.WriteLine("x^2=" + new E066().Compute(60));
  }
}
