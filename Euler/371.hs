import Data.Array ( (!), listArray )

a = listArray ((0,0,0),(501,1,1)) [f d u v | d <- [0..501], u <- [0,1], v <- [0,1]]

f d u v = let g p q = if p == 0 then 0 else fromIntegral p / 1000 * q in
  (g (1-u) (a!(d+1,1,v)) + g (1000+2*(u+v-d-1)) (a!(d+1,u,v)) +
  g  (1-v) (a!(d+1,u,1)) + 1) / (1 - fromIntegral (d-v) / 1000)

main = print $ a ! (0,0,0)

