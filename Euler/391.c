#include <stdio.h>

int bitCount(unsigned x)
{
  x -= ((x >> 1) & 0x55555555);
  x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
  x = (x + (x >> 4)) & 0x0F0F0F0F; x += x >> 8;
  return (x + (x >> 16)) & 0x3F;
}

int main(void)
{
  const int m = 20000000, N = 1000;
  static int a[m];
  long sum = 0;
  for (int i = 1; i < m; ++i) a[i] = a[i-1] + bitCount(i);
  for (int i, j, n = 1; n <= N; ++n) {
    for (i = j = m-1; a[i] > n; j = i) while (a[i]+n >= a[j]) --i;
    sum += a[i]*a[i]*a[i];
  }
  printf("%ld\n", sum);
  return 0;
}

