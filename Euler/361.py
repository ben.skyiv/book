import sys

def goodNumbers(n):
    assert(n >= 0)

    if n <= 1:
        return 2 ** n

    smallerPowerOfTwo = 2
    while 2 * smallerPowerOfTwo <= n:
        smallerPowerOfTwo *= 2
    assert(smallerPowerOfTwo <= n < 2 * smallerPowerOfTwo)

    return min(2 * n - smallerPowerOfTwo // 2, n + smallerPowerOfTwo)

# sum(goodNumbers(i) for i in range(n))
def goodNumbersCumulative(n):
    assert(n >= 0)

    if n <= 1:
        return n

    smallerPowerOfTwo = 2
    while 2 * smallerPowerOfTwo <= n:
        smallerPowerOfTwo *= 2
    assert(smallerPowerOfTwo <= n < 2 * smallerPowerOfTwo)

    if n < smallerPowerOfTwo * 3 // 2:
        return ((19 * (smallerPowerOfTwo // 2) ** 2 - 9 * smallerPowerOfTwo // 2 + 8) // 6
            + (n - smallerPowerOfTwo) * (n + smallerPowerOfTwo // 2 - 1))
    else:
        return ((43 * (smallerPowerOfTwo // 2) ** 2 - 15 * smallerPowerOfTwo // 2 + 8) // 6
            + (n - 3 * smallerPowerOfTwo // 2) * (n + 7 * smallerPowerOfTwo // 2 - 1) // 2)


# returns n such that goodNumbersCumulative(n) <= g < goodNumbersCumulative(n + 1)
def inverseGoodNumbersCumulative(g):
    assert(g >= 0)
    if g == 0:
        return 0

    lower = 1
    while goodNumbersCumulative(2 * lower) <= g:
        lower *= 2
    upper = 2 * lower
    assert(goodNumbersCumulative(lower) <= g < goodNumbersCumulative(upper))
    while upper - lower > 1:
        middle = lower + (upper - lower) // 2
        if goodNumbersCumulative(middle) <= g:
            lower = middle
        else:
            upper = middle
        assert(goodNumbersCumulative(lower) <= g < goodNumbersCumulative(upper))
    assert(upper == lower + 1)

    return lower

# Bits does not include the leading 1
# Starts with the least significant
# Returns (prefix, invert)
def generateGoodNumberBitsInfo(bits, index):
    g_bits = goodNumbers(bits)
    assert(bits >= 0)
    assert(0 <= index < g_bits)

    if bits <= 2:
        table = [ [ 1 ],
                  [ 0, 1 ],
                  [ 5, 2, 0 ] ]
        return (table[bits][index], False)
    assert(bits >= 3)

    lowerBits = bits // 2
    upperBits = (bits + 1) // 2

    if index < goodNumbers(lowerBits):
        prefix, invert = generateGoodNumberBitsInfo(lowerBits, index)
        invertFirst = True
        skipFirst = bits % 2 == 0
    else:
        prefix, invert = generateGoodNumberBitsInfo(upperBits, g_bits - 1 - index)
        invertFirst = False
        skipFirst = bits % 2 == 1

    if invertFirst:
        invert = not invert

    prefix *= 2
    if skipFirst:
        prefix += 1

    return (prefix, invert)


def bitParity(n):
    result = False
    while n:
        result = not result
        n &= n - 1
    return result

def leastSignificantOne(n):
    assert(n > 0)
    return ((n ^ (n - 1)) + 1) >> 1

def mostSignificantOne(n):
    assert(n > 0)
    while True:
        lsb = leastSignificantOne(n)
        if lsb == n:
            break
        n -= lsb
    return n

def valueOfBlock(start, finish, modulus):
    length = finish - start

    currentLength = 1
    currentPowerOfTwo = 2
    result = 0
    while currentLength < length:
        result = (result
            + currentPowerOfTwo * (currentPowerOfTwo - 1 - result)) % modulus
        currentLength <<= 1
        currentPowerOfTwo = currentPowerOfTwo * currentPowerOfTwo % modulus
    assert(currentLength == length)

    if bitParity(start):
        result = (currentPowerOfTwo - 1 - result) % modulus
    return result

def valueOfSubsequence(start, finish, modulus):
    assert(start <= finish)

    if start == finish:
        return 0

    differentBits = start ^ (finish - 1)
    if differentBits:
        differentSuffix = (mostSignificantOne(differentBits) << 1) - 1
    else:
        differentSuffix = 0

    if (start & differentSuffix) == 0 and \
            ((finish - 1) & differentSuffix) == differentSuffix:
        return valueOfBlock(start, finish, modulus)
    else:
        commonPrefix = start & ~differentSuffix
        assert( commonPrefix == ((finish - 1) & ~differentSuffix) )

        middle = commonPrefix + ((differentSuffix + 1) >> 1)
        assert(start < middle < finish)

        lowBlock = valueOfSubsequence(start, middle, modulus)
        highBlock = valueOfSubsequence(middle, finish, modulus)
        return (lowBlock + pow(2, middle - start, modulus) * highBlock) % modulus


def elementOfA(n, modulus):
    assert(n >= 0)

    if n <= 6:
        return n

    m = inverseGoodNumbersCumulative(n - 1)
    assert(m >= 3)
    # A_n has m + 1 bits, i.e. 2**m <= A_n < 2**(m + 1)

    G_m = goodNumbersCumulative(m)
    assert(G_m < n <= G_m + goodNumbers(m))

    i =  n - G_m - 1
    assert(0 <= i < goodNumbers(m))

    prefix, invert = generateGoodNumberBitsInfo(m, i)

    result = valueOfSubsequence(prefix, prefix + m + 1, modulus)
    if invert:
        result = (pow(2, m + 1, modulus) - 1 - result) % modulus
    return result

if len(sys.argv) == 2:
    M = int(sys.argv[1])
else:
    M = 18

digitsInResult = 9
answerModulus = 10 ** digitsInResult

answer = 0
for k in range(1, M + 1):
    term = elementOfA(10 ** k, answerModulus)
    answer += term

answer %= answerModulus
print(answer)

