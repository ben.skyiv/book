s = 0
L = 1000000
for n in xrange(2, L+1):
    x = [0, n]
    while x[0]!=x[1] and x[1]:
        x = [x[1], pow(n, x[0], 10**9)]
    s += x[1]

print s

