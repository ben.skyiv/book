main = let n = 1e6 in print $ sum $ scanl (\a i -> a*(n-i)/n) 1 [1..n-1]

