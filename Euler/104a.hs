import Data.List
main = print $ let fibs = 1:1:zipWith (+) fibs (tail fibs)
  in fst . head . filter (("123456789"==) . sort . take 9 . show . snd)
  $ filter (("123456789"==) . sort . show . flip mod (10^9) . snd)
  $ zip [1..] $ fibs

