#include <stdio.h>

int main(void)
{
  long n = (long)1e16, z = 0;
  int i = 0, j = 0;
  for (long m = n + 1; m > 0; m /= 2, i++)
    if (m % 2 == 1) j++, z += 1L << i;
  z -= 1L << --i; z *= i - 1;
  z += (i-3) * (1L << i) + i + j + 2;
  printf("S(%ld) = %ld\n", n, z);
}
