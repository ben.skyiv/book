import Math.NumberTheory.Powers.Squares ( isSquare' )

main = let n = 10^4 in print $ sum . concat $
  map (\(s,a) -> let e=a*a; f=s+a; t = div f $ gcd e f in
  map fst $ filter (isSquare' . snd) $ takeWhile ((>=0) . snd)
  [(s, g*g - 4 * (div (d*e) f)) | d <- [t,t+t..s-a-2], let g = s-a-d])
  [(s, a) | s <- [4..n], a <- [1..s-3]]

