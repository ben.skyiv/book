import Data.Ratio ( (%) )
import Data.Set ( empty,insert,size )

main = print $ size $ foldl (\s n -> foldl (\s i -> insert (i%n) s) s [0..n-1]) empty [1864..1909]

