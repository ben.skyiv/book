import Data.Maybe ( fromJust )
import Data.Array ( (!),listArray )
import Data.Numbers.Primes ( primes )
import Math.NumberTheory.Moduli ( invertMod )

m = 10^9 + 7; low = 10^7; high = low + 10^4
a1 = listArray (0,high) $  1 : [mod (i * (a1!(i-1))) m | i <- [1..high]]
a2 = listArray (0,high) [fromJust $ invertMod (a1!i) m | i <- [0..high]]
c n k = mod ((a1!n) * (a2!k) * (a2!(n-k))) m
g n = let a = sqrt $ fromInteger n in succ $ sum [c (x + floor
  (fromInteger n - a * fromInteger x)) x | x <- [1 .. floor a]]
main = print $ mod (sum $ map g $ filter (>low) $ takeWhile (<high) primes) m

