import Math.NumberTheory.Primes ( primes )
f n p = (p, sum $ takeWhile (>0) $ tail $ iterate (`div` p) n)
main = let n = 200 in print $ foldl (\a (_,i) -> a * div
  ((i+1)*(i+2)) 2) 1 $ map (f n) $ takeWhile (<=n) primes

