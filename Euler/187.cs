using System;

sealed class E187
{
  const  int n = 100000000;
  static byte[] a = new byte[n];

  static void Main()
  {
    int s = 0;
    for (int i = 2; i < n; i++)
      if (a[i] != 1)
      {
        if (a[i] == 2) s++;
        for (int j = i + i; j < n; j += i) a[j] = (byte)(a[i] == 2 ? 1 : 2);
      }
    Console.WriteLine(s);
  }
}
