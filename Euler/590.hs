import Data.List ( (\\),subsequences )
import Math.NumberTheory.Powers ( powerMod )
import Math.NumberTheory.Primes ( primes )
m = 9; m1 = 10^m; m2 = 4*5^(m-1); f1 :: Int -> [Int]
f1 n = map (\i -> floor $ logBase (fromIntegral i)
  (fromIntegral n)) $ takeWhile (<= fromIntegral n) primes
p2 n = powerMod 2 (if n < m then n else mod (n-m) m2 + m) m1
hl n = let zs = f1 n; xss = subsequences zs; m = 10^9
  in foldl (\z xs -> mod (z +
  (if even (length zs - length xs) then 1 else -1) * p2
  (product (map succ xs) * product (zs \\ xs))) m1) 0 xss
main = print $ hl 50
