import Data.Map.Strict as Map ( empty,lookup,insert )
import Data.Maybe ( fromJust,isNothing )
import Data.Ratio ( (%) )
f n = product [2..n]
combin n r = if r > n-r then combin n (n-r) else div (product [n-r+1..n]) $ f r
r z d = let v = Map.lookup z d in if not $ isNothing v then (fromJust v, d)
  else let a = head z; b = z !! 1 in if m == 2 then
  let v = combin (a+b) b * f (a-1) * f (b-1) % if a == b then 2 else 1
  in (v, insert z v d) else let
  (p,q) = r (tail z) d
  v = foldl (\s (i,x) -> if a == x then s/(i%(i-1)) else s)
    (combin (sum z) a * f (a-1) % 1 * p) $ zip [2..] $ tail z
  in (v, q)
  where m = length z
t 2 n k = [[i,j] | i <- [k .. div n 2], j <- [i..n-i]]
t l n k = concat [map (i:) $ t (l-1) (n-i) i | i <- [k .. div n l]]
g n = let m = f n in sum $ concat [[fromRational $ 1%m], [fromRational $ 
  (sum [combin i (k-1) * f (k-1) | i <- [k-1..n-1]]) * k * k % m | k <- [2..n]],
  [fst $ foldl (\(v,d) z -> let (p,q) = r z d in (v + (fromRational $
  (foldl1 lcm z)^2 * combin n (sum z) % m * p), q)) (0,empty) $
  concat $ takeWhile (not . null) [t l n 2 | l <- [2..]]]]
main = let n = 60 in print $ g n

