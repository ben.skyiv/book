import Data.List ( nub )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Testing ( isPrime )
ns 1 = n' 2 1
ns n = concat . map (n' $ primes !! (n - 1)) $ ns $ n - 1
n' b f = (takeWhile (<10^9)) . (map ((f*) . (b^))) $ [1..]
pft n = (head . filter isPrime . map (n+)) [2..] - n
main = print $ sum . nub . map pft $ (concat . (map ns)) [1..9]

