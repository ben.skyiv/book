using System;
using System.Text;

sealed class E040
{
  static void Main()
  {
    var sb = new StringBuilder(".");
    for (var i = 1; i < 200000; i++) sb.Append(i);
    Console.WriteLine("{0:N0}: {1} {2} {3} {4} {5} {6} {7}",
      sb.Length, sb[1], sb[10], sb[100], sb[1000],
      sb[10000], sb[100000], sb[1000000]);
  }
}
