function g(n, s)
    n ≤ s && return n == s
    for i in 1:ndigits(n)-1
        g(n % 10^i, s - n ÷ 10^i) && return true
    end
    false
end

T(N) = sum(n^2 for n in 2:isqrt(N) if mod(n, 9) ≤ 1 && g(n^2, n))

@show @time T(10^12)
