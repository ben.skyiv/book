// mcs 660b10.cs 660.cs && ./660b10.exe

using System;

static class E660b10
{
  static E660 e = new E660(10);

  static int P334()
  { // (2xx, 3xx, 1xxx) (2xx, 9xx, 1xxx) (3xx, 4xx, 1xxx)
    var z = 0;
    for (var i1 = 2; i1 < e.B-1; i1++)
      for (var i2 = i1+1; i2 <= e.B-1; i2++)
        foreach (var s in E660.Permutation(e.GetRest(1, i1, i2), 4))
          z += e.F(e.B*(e.B*i1+s[1])+s[2],
                   e.B*(e.B*i2+s[3])+s[4]);
    return z;
  }

  static int P244()
  { // (xx, 19xx, 2xxx) (xx, 29xx, 3xxx)
    var z = 0;
    for (var i1 = 2; i1 <= 7; i1++)
      foreach (var s in E660.Permutation(e.GetRest(i1, i1+1, 9), 4))
        z += e.F(s[1]*e.B + s[2], ((i1*e.B+9)*e.B+s[3])*e.B + s[4]);
    return z;
  }

  static void Main() { Console.WriteLine(P334() + P244()); }
}

