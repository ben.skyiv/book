#include <stdio.h>

const long n = (long)1e12;

long f(long k)
{
  long z = 0; k -= 2;
  for (long a, b, i = 1; ; i++) {
    if ((a = (k << 1) * i + 1, a *= a) > n) break;
    if ((b = ((i*k+1)*i*(k-2)*(k-2)) >> 1) > n) break;
    z += a + b;
  }
  return z;
}

int main(void)
{
  long z = 0;
  for (long w, k = 3; (w = f(k)) > 0; k += 2) z += w;
  printf("%ld\n", z);
}
