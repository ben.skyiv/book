#include <stdio.h>

const int max = 100000000;
static char composite[max + 2];

void sieve()
{
  for (int i = 2; i * i < max + 2; i++)
    if (!composite[i])
      for (int j = i * i; j < max + 2; j += i)
        composite[j] = 1;
}

int valid(int n)
{
  for (int d = 2; d * d <= n; d++)
    if (n % d == 0 && composite[d + n / d])
      return 0;
  return 1;
}

int main(void)
{
  sieve();
  long sum = 0;
  for (int n = 1; n <= max; n++)
    if (!composite[n + 1] && valid(n))
      sum += (long)n;
  printf("%ld\n", sum);
  return 0;
}

