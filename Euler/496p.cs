using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

sealed class E496
{
  static readonly int max = 1000000000, mod = 8000;
  int[] primes;

  int[] Sieve(int max)
  { // primes[n]: 0 for n is prime, else one of the prime factors of n
    int limit = (int)Math.Sqrt(max);
    var primes = new int[max + 1];
    for (int i = 2; i <= limit; i++)
      if (primes[i] == 0)
        for (int j = i * i; j <= max; j += i)
          primes[j] = i;
    return primes;
  }

  List<Tuple<int,int>> GetPrimeFactors(int n)
  { // return: the prime factorisation of n
    var lst = new List<Tuple<int,int>>();
    for (int p, k; n > 1 && (p = primes[n]) != 0; lst.Add(Tuple.Create(p, k)))
      for (k = 0; n % p == 0; k++) n /= p;
    if (n > 1) lst.Add(Tuple.Create(n, 1));
    return lst;
  }

  List<List<T>> GetSubsets<T>(List<T> lst)
  {
    return (from m in Enumerable.Range(0, 1 << lst.Count)
      select (from i in Enumerable.Range(0, lst.Count)
      where (m & (1<<i)) != 0 select lst[i]).ToList()).ToList();
  }

  List<List<T>> Transform<T>(List<List<T>> lst)
  { // example: {{2,4,8},{3,9}} => {{2,3},{2,9},{4,3},{4,9},{8,3},{8,9}}
    var lst1 = new List<List<T>>();
    lst1.Add(new List<T>());
    foreach (var lst2 in lst)
    {
      var lst3 = new List<List<T>>();
      foreach (var t in lst2)
        foreach (var lst4 in lst1)
        {
          var lst5 = new List<T>(lst4);
          lst5.Add(t);
          lst3.Add(lst5);
        }
      lst1 = lst3;
    }
    return lst1;
  }

  static List<List<int>> Expand(List<Tuple<int,int>> pfs, int n)
  { // example: {(7,2),(23,1)}, 600 => {{7,49,343},{23,529}}
    var lst = new List<List<int>>();
    foreach (var i in pfs)
    {
      var lst2 = new List<int>();
      long z = 1;
      for (var j = 2*i.Item2; j > 0 && (z *= i.Item1) < n; j--) lst2.Add((int)z);
      lst.Add(lst2);
    }
    return lst;
  }

  HashSet<int> GetDivisors(List<Tuple<int,int>> pfs, int n)
  { // return divisors of n^2 in range (1,n)
    var set = new HashSet<int>();
    foreach (var set1 in Transform(Expand(pfs, n)))
      foreach (var subset in GetSubsets(set1))
      {
        long z = 1;
        foreach (var i in subset) z *= i;
        if (z > 1 && z < n) set.Add((int)z);
      }
    return set;
  }

  long Compute2(int a)
  {
    if (primes[a] == 0) return 0;
    long sum = 0, a2 = (long)a*a;
    foreach (var b in GetDivisors(GetPrimeFactors(a), a))
      if (a2 / b < a + b + b)
        sum += a;
    return sum;
  }
  
  long Compute(int k)
  {
    var dt1 = DateTime.Now;
    long sum = 0; // compute range: [0, max]: a % mod == k
    for (var a = k; a <= max; a += mod) sum += Compute2(a);
    var dt2 = DateTime.Now;
    Console.WriteLine("{0:dd HH:mm:ss} {1,12:N3} {2,5:N0} {3,2}",
      dt2, (dt2-dt1).TotalSeconds, k, Thread.CurrentThread.ManagedThreadId);
    return sum;
  }

  void Compute()
  {
    DateTime dt1 = DateTime.Now, dt2;
    Console.WriteLine("{0:dd HH:mm:ss} {1:N0} {2:N0}", dt1, max, mod);
    primes = Sieve(max);
    Console.WriteLine("{0:dd HH:mm:ss} Parallel compute begin", DateTime.Now);
    long z = 0;
    Parallel.For(0, mod, () => 0L, (i,_,x) => x+Compute(i),
      x => Interlocked.Add(ref z, x));
    Console.WriteLine("{0:dd HH:mm:ss} {1}", DateTime.Now, z);
    Console.WriteLine("{0:dd HH:mm:ss} {1}", dt2 = DateTime.Now, dt2-dt1);
  }

  static void Main()
  {
    new E496().Compute();
  }
}

