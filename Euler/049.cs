using System;
using System.Collections.Generic;

sealed class E049
{
  IEnumerable<int> GetPrimes(int max)
  {
    var list = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i] && i > 1000) list.Add(i);
    return list;
  }

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  int Sort(int x)
  {
    var s = x.ToString();
    var a = s.ToCharArray();
    Array.Sort(a);
    return int.Parse(new string(a));
  }
  
  bool Has3(List<int> list)
  {
    if (list.Count < 3) return false;
    for (var i = 0; i < list.Count - 2; i++)
      for (var j = i + 1; j < list.Count - 1; j++)
      {
        var d1 = list[j] - list[i];
        for (var k = j + 1; k < list.Count; k++)
          if (list[k] - list[j] == d1) return true;
      }
    return false;
  }
  
  void Compute()
  {
    var primes = GetPrimes(9999);
    var dict = new SortedDictionary<int, List<int>>();
    foreach (var prime in primes)
    {
      List<int> list;
      var key = Sort(prime);
      if (!dict.TryGetValue(key, out list))
         dict.Add(key, list = new List<int>());
      list.Add(prime);
    }
    foreach (var kvp in dict)
    {
      if (!Has3(kvp.Value)) continue;
      foreach (var n in kvp.Value) Console.Write(n + " ");
      Console.WriteLine();
    }
  }

  static void Main()
  {
    new E049().Compute();
  }
}

