#include <stdio.h>

extern int exactSquareRoot(long x);

int main(void)
{
  int p = 250000;
  long count = (p - 1) / 2;
  for (int a = 2; a <= p/3; a++)
    for (int b = a; b <= (p-a)/2; b++) {
      int c = exactSquareRoot((long)a*a + (long)b*b - 1);
      if (c >= 0 && a + b + c <= p) count++;
    }
  printf("%ld\n", count);
  return 0;
}
//         p     count C          Haskell
//    25,000    37,347 0m0.450s   0m1.397s
//   250,000   454,422 0m35.474s  2m34.726s
// 2,500,000 5,352,755 59m7.711s  

