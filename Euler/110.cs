using System;
using System.Linq;

static class E110
{
  static void Main()
  {
    var n = 4000000;
    var p = new[] { 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47 };
    var a = Enumerable.Repeat(1, p.Length).ToArray();
    var b = Enumerable.Range(0, p.Length);
    while (a.Aggregate(1, (z,x) => z*x) <= 2*n)
      a[b.Max(x => Tuple.Create(Math.Pow(2.0/a[x],1.5)/p[x],x)).Item2] += 2;
    Console.WriteLine(a.Zip(p, (z,x) => (int)Math.Pow(x,z/2)).Aggregate(1L, (z,x) => z*x));
  }
}

