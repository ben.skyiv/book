using System;
using System.Collections.Generic;

sealed class E420
{
  static int Gcd(int a, int b) { return (b == 0) ? a : Gcd(b, a % b); }

  static void Main()
  {
    var n = 10000000;
    var dict = new Dictionary<int, List<Tuple<int, int>>>();
    for (var i = 1; i*i < n; i++)
      for (var j = 1; j < i; j++)
      {
        var key = i*i - j*j;
        List<Tuple<int, int>> v;
        if (!dict.TryGetValue(key, out v))
          dict.Add(key, v = new List<Tuple<int, int>>());
        v.Add(Tuple.Create(i, j));
      }
    var z = 0;
    foreach (var kvp in dict)
    {
      var v = kvp.Value;
      for (var i = 0; i < v.Count; i++)
        for (var j = i; j < v.Count; j++)
        {
          int a = v[i].Item1, d = v[j].Item1;
          int r0 = a + d, r1 = v[i].Item2 + v[j].Item2, g = Gcd(r0,r1);
          if (kvp.Key % ((r0 /= g) * r0 - (r1 /= g) * r1) != 0) continue;
          var bc = kvp.Key / (r0 * r0 - r1 * r1) * r1 * r1;
          if ((bc << 1) + a * a + d * d >= n) continue;
          bc /= r1 * r1;
          for (int k2, k = 1; (k2 = k * k) <= bc; k++)
            if (bc % k == 0)
              z += ((i == j) ? 1 : 2) * ((k2 == bc) ? 1 : 2);
        }
    }
    Console.WriteLine(z);
  }
}

