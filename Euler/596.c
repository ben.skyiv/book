#include <stdio.h>

long g(long n, int m)
{
  long z = n;
  for (long i = 2; ; i++) {
    long j = n / i, k = i - 1, v = (n / k - j) % m;
    z += (v * (v + 1 + j % m * 2) / 2) % m * k;
    if (i > j) break;
    z = (z + i * j) % m;
  }
  return z;
}

long f(long n, int m)
{
  long z = (1 + 8 * g(n*n, m) - 32 * g(n*n/4, m)) % m;
  return (z < 0) ? (z + m) : z;
}

int main(void)
{
  printf("%ld\n", f(100000000, 1000000007));
}
