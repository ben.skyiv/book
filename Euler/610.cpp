#include <unordered_set>
#include <unordered_map>
#include <cstdio>
using namespace std;

extern string to_roman(int n);
extern int from_roman(string s);
static unordered_set<string> romans;

string trimStart(string s, char c)
{ int i = 0; while (s[i] == c) i++; return s.erase(0, i); }

bool valid(string s) { return romans.count(trimStart(s, 'M')) != 0; }

void add(unordered_map<string, long double> &m, string k, long double v)
{
  auto it = m.find(k);
  if (it != m.end()) it->second += v;
  else m.insert(pair<string, long double>(k, v));
}

int main()
{
  for (int i = 0; i < 1000; i++) romans.insert(to_roman(i));
  string cs("IVXLCDM");
  unordered_map<string,long double> map1, map2;
  map1.insert(pair<string, long double>("", 1));
  long double a1 = 0.02, a2 = 0.14, a3 = a1 * a2, v0, v1 = a1, v2 = 0;
  for (int i = 1; i < 200; i++, a3 *= a2) {
    for (const auto& kvp : map1)
      for (const auto& c : cs) {
        auto s = kvp.first + c;
        if (valid(s)) add(map2, s, kvp.second);
        else  add(map2, kvp.first, kvp.second);
      }
    for (const auto& kvp : map2)
      v1 += v0 = kvp.second * a3, v2 += v0 * from_roman(kvp.first);
    printf("%4d: %6d %11.9llf %13.9llf %lle\n", i, map2.size(), v1, v2, a3);
    fflush(stdout); map1.clear(); auto t = map1; map1 = map2; map2 = t;
  }
}

