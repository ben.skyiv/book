using System;
using System.Text;
using System.Collections.Generic;
using Skyiv.Utils;

static class E641a
{
  static readonly int exp = 8;//36; // 1e36
  static readonly Prime2Helper ph = new Prime2Helper((int)(Math.Pow(10, exp/4.0)));
  static readonly int[] primes = ph.GetPrimes();

  static int Get6k()
  { // p^(6k)
    var z = 0;
    foreach (var p in primes) {
      var k = (int)(exp / Math.Log10(p) / 6);
      if (k == 0) break;
      z += k;
    }
    return z;
  }

  static List<int> GetPrimeFactors(int n)
  { // n is odd number
    var list = new List<int>();
    for (int i9 = (int)Math.Sqrt(n), i = 3; i <= i9; ) {
      if (n % i != 0) { i += 2; continue; }
      list.Add(i); n /= i;
    }
    if (n != 1) list.Add(n);
    return list;
  }

  static bool Valid(List<int> list)
  {
    if (list.Count < 2) return false;
    double z = 0;
    for (var i = list.Count - 1; i >= 0; i--)
      z += list[i] * Math.Log10(primes[i]);
    return z <= exp;
  }
  
  static List<List<int>> GetExpList()
  {
    var exps = new List<List<int>>();
    for (var i = 1; i < 1000000; i++) {
      var list = GetPrimeFactors(6 * i + 1);
      list.Reverse();
      for (var j = list.Count - 1; j >= 0; j--) list[j]--;
      if (!Valid(list)) continue;
      exps.Add(list);
    }
//    Out(exps);
    return exps;
  }
  
  static int GetIndex(int q)
  {
    var k = Array.BinarySearch(primes, q);
    return (k >= 0) ? k : (~k - 1);
  }

  static int Get2(List<int> exps)
  {
    var z = 0;
    for (var i = 0;  ; i++) {
      var p = primes[i];
      var q = (int)Math.Pow(10, (exp - exps[0] * Math.Log10(p)) / exps[1]);
      if (exps[0] == exps[1]) { if (q <= p) break; z -= i; }
      else { if (q < 2) break; if (q < p) z++; }
      z += GetIndex(q);
    }
Console.Write("({0} {1}):{2} ",exps[0],exps[1],z);
    return z;
  }

  static int Get3(List<int> exps)
  { // exps: (6 4 4)
    if (!(exps[1] == exps[2] && exps[0] != exps[1])) throw new Exception("Get3()");
    var z = 0;
    for (var i = 0; i < primes.Length - 1; i++) {
      var p = primes[i];
      for (var j = i + 1; ; j++) {
        var q = primes[j];
        var x = exp - exps[1] * Math.Log10(p) - exps[2] * Math.Log10(q);
        var r = (int)Math.Pow(10, x / exps[0]);
        if (r < 2) break;
        z += GetIndex(r); if (r < p) z++; if (r >= q) z--;
      }
    }
Console.Write("({0} {1} {2}):{3} ",exps[0],exps[1],exps[2],z);
    return z;
  }

  static int GetMulit(List<int> exps, int i0)
  {
    if (exps.Count == 2) return Get2(exps);
    if (exps.Count == 3) return Get3(exps);
    throw new Exception("Invalid exps.Count: [" + exps.Count + "]");
  }

  static int GetMulti()
  { // p^4*q^4, p^6*q^6, p^10*q^4, ..., p^6*q^4*r^4, ...
    var z = 0;
    foreach (var exps in GetExpList()) z += GetMulit(exps, 0);
    return z;
  }

/*  static void Out(List<List<int>> exps)
  {
    foreach (var list in exps) {
      var sb = new StringBuilder("(");
      foreach (var p in list) sb.Append(p + " ");
      sb.Length--; Console.Write(sb + ") ");
    }
    Console.WriteLine(exps.Count);
  }
*/
  static void Main()
  { // f(1e8) == 69
    try { Console.WriteLine(1 + Get6k() + GetMulti()); }
    catch (Exception ex) { Console.WriteLine("Error: " + ex); }
  }
}

