binomial n m = let f n m = product [n..m] in div (f (m+1) n) $ f 1 (n-m)
f m n = sum [binomial (k+x) (k-x) | x <- [0..div (n+1) (m+1)], let k = n + 1 - x*m]
main = let m = 3; n = 50 in print $ f m n

