using System;
using System.Linq;
using System.Collections.Generic;

static class E687
{
  static readonly Random rand = new Random();
  static readonly byte[] a = Enumerable.Range(0,52).Select(i=>(byte)(i/4)).ToArray();
  static readonly int[] s = new int[a.Length / 4];
  static readonly HashSet<int> ps = new HashSet<int>(new int[]{11,10,8,6,2,0});
  static void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }

  static void RandomPermutation()
  {
    for (var n = a.Length; n > 1; n--)
      Swap(ref a[rand.Next(n)], ref a[n - 1]);
  }
  
  static int Perfect()
  {
    Array.Clear(s, 0, s.Length);
    for (var i = a.Length - 1; i > 0; i--)
      if (a[i] == a[i-1]) s[a[i]] = 1;
    return s.Sum();
  }

  static void Main()
  {
    long z1 = 0, z2 = 0; int n = (int)1e8;
    for (var i = 0; i < n; i++) {
      RandomPermutation();
      int z = Perfect();
      if (ps.Contains(z)) z1++;
      z2 += z;
    }
    Console.WriteLine("{0:F10} {1}", z1/(double)n, s.Length - z2/(double)n);
  } // 0.3285311900 10.17416858
}
