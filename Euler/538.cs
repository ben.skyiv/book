using System;
using System.Collections.Generic;

sealed class E538
{
  sealed class Us
  {
    List<long> u = new List<long>();
    
    public Us() { for (var i = 0; i < 5; i++) u.Add(0); }
    
    public bool Add(long n)
    {
      var index = u.BinarySearch(n);
      if (index == 0 || ~index == 0) return false;
      u.Insert((index > 0) ? index : (~index), n);
      u.RemoveAt(0);
      return true;
    }
    
    public long F()
    {
      return  u[0]+u[1]+u[2]+u[3]+u[4] - ((
        Area2(u[0],u[1],u[2],u[3]) <
        Area2(u[1],u[2],u[3],u[4])) ? u[0] : u[4]);
    }

    double Area2(long a, long b, long c, long d)
    { // return: area^2
      double s = (a + b + c + d) / 2.0;
      return (s-a) * (s-b) * (s-c) * (s-d);
    }
  }

  byte[] bs;

  long[] Pow3s(int n)
  {
    var s3 = new long[n + 1];
    s3[0] = 1;
    for (var i = 1; i <= n; i++) s3[i] = 3 * s3[i-1];
    return s3;
  }

  int MakeBs(int n)
  {
    int n3 = 3 * n, n2 = n << 1;
    bs = new byte[n3 + 2];
    int z = 0;
    for (var i = 0; ; i++)
    {
      var j = i << 1;
      if (j > n3) break;
      bs[j] = bs[i];
      bs[j+1] = (byte)(bs[i] + 1);
      if (j <= n2 && z < bs[j+1]) z = bs[j+1];
    }
    return z;
  }

  long Compute(int n)
  {
    var s3 = Pow3s(MakeBs(n));
    var u = new Us();
    long z = 0, v = 0;
    for (var i = 1; i <= n; i++)
    {
      var a = u.Add((1L<<bs[3*i]) + s3[bs[i<<1]] + bs[i+1]);
      if (i < 4) continue;
      if (a) v = u.F();
      z += v;
    }
    return z;
  }

  static void Main()
  {
    Console.WriteLine(new E538().Compute(3000000));
  }
}

