import Control.Monad ( guard )
import Data.Maybe ( fromJust )
import Data.List ( groupBy,nub,sort,sortBy )
import Math.NumberTheory.Powers.Squares ( isSquare )

main = do
  s <- readFile "098.txt"
  print $ (\(n,_,_) -> n) $ last $ sort $ (filter ((>1) . length) $ groupBy
    (\a b -> sort a == sort b) $ sortBy (\a b -> compare (sort a) $ sort b)
    $ words $ map (\x -> if elem x "\"," then ' ' else x) s) >>= (\xs ->
    [[a,b] | a <- xs, b <- xs, a /= b]) >>= (\[a,b] -> let n = length a in
    (takeWhile (<10^n) $ dropWhile (<10^(n-1)) $ map (^2) [1..]) >>= f a b)
f a b n = let s = map (\x -> fromJust $ lookup x $ zip b $ show n) a; m = read s
  in do guard $ isSquare m; guard $ head s /= '0'; guard $ nub s == s; [(m,a,b)]

