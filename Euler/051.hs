import qualified Data.Map as M ( empty,foldr,insertWith )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )

main = let n = 8; m = 6 in putStrLn $ head $ head $ M.foldr
  (\i a -> if length i == n then i:a else a) [] $ foldr
  (\i a -> foldr (\b a -> foldr (\b a -> M.insertWith (++)
  b [i] a) a (tail $ f b '_' i)) a ['0'..'9']) M.empty $
  map show $ takeWhile (<10^m) $ sieveFrom $ 10^(m-1) where
  f a b (x:xs) = let zs = f a b xs in map (x:) zs ++
    if x == a then map (b:) zs else []; f _ _ _ = [[]]

