// gcc --std=c11 -O4 -march=native -fopenmp 492.c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static int ex13(long p)
{
  int r = p % 13;
  return r == 1 || r == 3 || r == 4 || r == 9 || r == 10 || r == 12;
}

static long compute(long p)
{
  const long N = 1000000000000000;
  long k = 1, r = p + p + (ex13(p) ? (-2) : 2);
  for (long n = N-1, m = 2; n != 0; n >>= 1, m = m*m%r) if (n&1) k = k*m%r;
  long a = (p + 11) >> 1, b = (p + 3) >> 1, x = 1, y = 0;
  for (long x0, y0, a0, b0; k != 0;
    k >>= 1, a0 = (a*a + 13*b%p*b) % p, b = b0 = 2*a*b % p, a = a0)
    if (k&1) x0 = (x*a + 13*y%p*b) % p, y = y0 = (y*a + x*b) % p, x = x0;
  return (x + x + p - 5) % p * (1 + ((p % 6 == 1) ? 5 : 1) * p) / 6 % p;
}

int main()
{
  const int X = 1000000000, Y = 10000000, P = sqrt(X + Y);
  char *b = calloc(P + 1, sizeof(char)), *c = calloc(Y + 1, sizeof(char));
  int count = 0, *primes = calloc(Y + 1, sizeof(int));
  for (int r, p = 2; p <= P; p++) {
    if (b[p] != 0) continue;
    for (int q = p + p; q <= P; q += p) b[q] = 1;
    if ((r = X / p * p) < X) r += p;
    for (int q = r; q <= X+Y; q += p) c[q-X] = 1;
  }
  for (int i = 0; i <= Y; i++) if (c[i] == 0) primes[count++] = i + X;
  long result = 0;
  #pragma omp parallel for reduction(+:result)
  for (int i = 0; i < count; i++) result += compute(primes[i]);
  free(b); free(c); free(primes);
  printf("%ld\n", result);
  return 0;
}

