f x y m = sum $ map fst $ take m $ iterate (\(x,y) -> (3*x+2*y+3,4*x+3*y+5)) (x,y)
main = let n = 40; m = 1 + div n 2 in print $ f 0 (-1) m + f 0 0 m

