#include <stdio.h>

const int MOD = 1e9 + 7, n = 1e7, N = n * 1.1;
long p[N / 4], q[N], a[N];

int main()
{
  p[0] = 1;
  for (int k = 1; k * (3 * k - 1) / 2 <= n / 4; ++k)
    p[k * (3 * k - 1) / 2] = p[k * (3 * k + 1) / 2] = k & 1 ? MOD - 1 : 1;
  for (int i = 1; i <= n / 4; ++i) {
    if (i % 1000000 == 0) { printf("%d.", i/1000000);  fflush(stdout); }
    long v = 0;
    for (int k = 1; k*k <= i; ++k) v += k & 1 ? p[i-k*k] : MOD - p[i-k*k];
    p[i] = (p[i] + v * 2) % MOD;
  }
  for (int i = 1; i <= n / 4; ++i) p[i] = (p[i] + p[i - 1]) % MOD;
  q[0] = 1;  long ans = p[n / 4] - 1;
  for (int t = 1; t * t <= n; ++t) {
    if (t % 1000 == 0) { printf("%d:", t/1000); fflush(stdout); }
    for (int i = (t - 1) * (t - 1); i < t * t; ++i) a[i] = q[i], q[i] = 0;
    for (int i = t * t; i <= n; ++i) {
      a[i] = q[i];
      q[i] = ((i >= 2 * t ? q[i - 2 * t] : 0) + a[i - 2 * t + 1]) % MOD;
      ans += q[i] * p[(n - i) / 4] % MOD;
    }
  }
  printf("%ld\n", ans % MOD);
}

