#include <stdio.h>

const long m = 1000000000, inv3 = 666666667;

long f(long n)
{
  return n %= m, n*(n+1)/2 % m * (2*n+1) % m * inv3 % m;
}

int main(void)
{
  long i = 1, j, z = 0, n = 1000000000000000;
  for (long j0 = n; (j = n/i) >= i; i++, j0 = j)
    z = (z + f(j) + (j0 - j) * f(i - 1)) % m;
  if (j != i - 2) z += f(j);
  printf("%ld\n", z % m);
}

