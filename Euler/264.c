#include <stdio.h>
#include <math.h>

double g(long a, long b) { return sqrt(a*a + b*b); }

double f(int m, long a, long b, long c, long d) 
{
  double x, z = 0;
  long r2 = a*a+b*b, u = 5-a-c, v = -b-d;
  if (c*c + d*d == r2 && u*u + v*v == r2 &&
    (a > c || (a == c && b > d)) && (c > u || (c == u && d > v)))
    if ((x = g(c-a, d-b) + g(u-a, v-b) + g(u-c, v-d)) <= m)
      z += (b > 0 && b != -d) ? (2*x) : x;
  return z;
}

int main(void)
{
  long m = 100000;
  double t,          z = f(m, 5, 0, 4, 3) + f(m, 5, 0, 4, -3)
    + f(m, 5, 0, 0, 5) + f(m, 5, 0, 3, 4) + f(m, 5, 0, 3, -4);
  for (long a = 6; a <= m/3; a++)
    for (long d, c, b = (a+1) & 1; b <= m/3; b += 2) {
      if ((b | (d = a - 5)) == 0) continue;
      if (b - 3 > sqrt(3) * d) break;
      c = lround(b * (t = sqrt((10.0*d+25)/(d*d+b*b)+0.75)) - 0.5 * d);
      z += f(m, a, b, c, lround(b ? (-0.5*b - t*d) : sqrt(a*a - c*c)));
    }
  printf("%.4lf\n", z);
  return 0;
}

