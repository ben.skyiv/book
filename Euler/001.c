#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  int sum = 0, limit = (argc > 1) ? atoi(argv[1]) : 1000;
  for (int i = 3; i < limit; i += 3) sum += i;
  for (int i = 5; i < limit; i += 5) if (i % 3 != 0) sum += i;
  printf("%d\n", sum);
  return 0;
}

