using System;
using System.Drawing;
using System.Collections.Generic;

static class E619
{
  static readonly int m = 1000000007, a = 111, b = 135; //1000000,1234567

  static List<Point>[] Factor()
  {
    var d = b - a + 1;
    if (d <= Math.Sqrt(b + d)) throw new Exception("d too small");
    var c = new bool[d];
    for (int i = 2; i * i < d; i++)
      if (!c[i]) for (int j = i * i; j < d; j += i) c[j] = true;
    var s = new List<Point>[d]; var t = new int[d];
    for (int i = 0; i < d; i++) { t[i] = 1; s[i] = new List<Point>(); }
    for (int p = 2; (long)p * p < a + d; p++)
      if (!c[p])
        for (long q = p; q < a + d; q *= p)
          for (long i = q-1-(a-1)%q; i < d; t[i] *= p, i += q) {
            if (q == p) s[i].Add(new Point(p, 1));
            else s[i][s[i].Count-1] =
              new Point(s[i][s[i].Count-1].X, s[i][s[i].Count-1].Y+1);
          }
    for (int i = 0; i < d; i++)
      if (i + a != t[i]) s[i].Add(new Point((i + a) / t[i], 1));
    return s;
  }

  static Dictionary<HashSet<int>,int> GetDict(List<Point>[] s)
  {
    var dict = new Dictionary<HashSet<int>,int>(HashSet<int>.CreateSetComparer());
    foreach (var i in s) {
      int v; var k = new HashSet<int>();
      foreach (var z in i) if ((z.Y & 1) != 0) k.Add(z.X);
      if (!dict.TryGetValue(k, out v)) dict.Add(k, 1);
      else dict[k] = v + 1;
    }
    return dict;
  }

  static long C(int n, int k)
  { // n >= k, k >= 0
    if (k > n - k) k = n - k;
    System.Numerics.BigInteger z = 1;
    for (var i = n - k + 1; i <= n; i++) z *= i;
    for (var i = 1; i <= k; i++) z /= i;
    return (long)(z % m);
  }

  static IEnumerable<HashSet<HashSet<int>>> GetR(List<HashSet<int>> t)
  { // TODO: too slow
    if (t.Count > 24) yield break;
    for (var n = (1 << t.Count) - 1; n > 0; n--) {
      var a = new HashSet<int>();
      for (int i = 0, z = n; z > 0; z >>= 1, i++)
        if ((z & 1) != 0) a.SymmetricExceptWith(t[i]);
      if (a.Count > 0) continue;
      var s = new HashSet<HashSet<int>>();
      for (int i = 0, z = n; z > 0; z >>= 1, i++)
        if ((z & 1) != 0) s.Add(t[i]);
foreach(var _1 in s){foreach(var _2 in _1)Console.Write(_2+" ");
Console.Write("|");}Console.WriteLine();
      yield return s;
    }
  }

  static long Compute(Dictionary<HashSet<int>,int> dict)
  {
    long z = 1;
    var list = new List<HashSet<int>>();
    foreach (var kvp in dict) {
      if (kvp.Key.Count == 0) continue;
      list.Add(kvp.Key);
      for (var i = 2; i <= kvp.Value; i += 2)
        z = (z + C(kvp.Value, i)) % m;
    }
    foreach (var s in GetR(list)) {
      long w = 1;
      foreach (var k in s) w = w * dict[k] % m;
      z = (z + w) % m;
    }
    return (z * (long)System.Numerics.BigInteger
      .ModPow(2, dict[new HashSet<int>()], m) - 1) % m;
  }

  static void Main()
  {
    var dict = GetDict(Factor());
var max=0;foreach(var kvp in dict)if(max<kvp.Value)max=kvp.Value;
Console.WriteLine("({0},{1}) {2} {3}", a, b, dict.Count, max);
    long z = Compute(dict);
    Console.WriteLine(z);
  }
}

