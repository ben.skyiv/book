import Data.Digits ( digitsRev )
import Math.NumberTheory.GCD ( extendedGCD )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )

f (p,q) = let a = 10^(length $ digitsRev 10 p); (_,u,_) = extendedGCD a q in p + a * mod (u*(q-p)) q

main = let n = 10^6 in print $ let ps = sieveFrom 5 in
  sum $ map f $ takeWhile ((<=n) . fst) $ zip ps $ drop 1 ps

-- 0m0.119s

