import math  # 810,834,388
s = 0; y = 0; c = 31250000
while True:
  y2 = y*y; x2 = math.sqrt(c*(8*c+3.2*y2)) + 4.8*c - y2
  if x2 < 0: break
  x3 = 1.8*(c-y2)
  z = 2*(int(math.sqrt(x2))-int(math.sqrt(x3))) if x3 >= 0 else 2*int(math.sqrt(x2)) + 1
  s += 2*z if y != 0 else z; y += 1
print(s)

