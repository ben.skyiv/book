import Data.Digits ( digits,unDigits )

f n k = pred $ unDigits (k+1) $ digits k n
main = let n = 7 in print $ takeWhile (/=0) $ scanl (\k n -> f k n) n [2..]

