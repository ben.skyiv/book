import Data.Char ( digitToInt,intToDigit )
import Data.List ( find,genericReplicate,mapAccumR )

fact n = product [2..n]
p x = snd $ mapAccumR ((\f x y -> let (a,b) = f x y in (b,a)) divMod) x $ map fact [1..9]

divid x d = let
  f x d = if d == 0 then if x == 0 then [[]] else [] else
     concat [map (k:) $ f (x-k) (d-1) | k <- [0 .. min 9 x]]
  in concat [map (k:) $ f (x-k) (d-1) | k <- [1..9]]

main = let n = 150 in print $ sum $ map (sum . zipWith (*) [1..9])
  $ map (\x -> if x < 45 then let
  f m = maybe (f $ m+1) p $ find ((==x) . sum . map digitToInt . show) $ let
    h m k = if k == 9 then [m * fact 9] else concat [map (z * fact k +) $
	h (m-z) (k+1) | z <- reverse [0 .. min m k]] in h m 1 in f 1 else if x < 63 then let
  f x (y:ys) = if sum x < last y then x else if sum x > sum y || sum x == sum y && x < y
    then f y ys else f x ys in (\(x:xs) -> f x xs) $ map (p . read . map intToDigit) $
  concatMap (divid x) [div (x+8) 9 ..] else p $ foldl (\a x -> a*10 + toInteger x) 0 $
  mod x 9 : genericReplicate (div x 9) 9) [1..n]

