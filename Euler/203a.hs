import Data.List ( nub,tails )
import Math.NumberTheory.Primes.Factorisation ( factorise )
fpow n = foldr (.) id . replicate n
clump n = fpow n init  . map (take n) . tails
pascal = iterate (map sum . clump 2 . (:) 0 . flip (++) [0]) [1]
squarefree n = all (<2) $ map snd $ factorise n
main = print . sum . filter squarefree . nub . concat . flip take pascal $ 51

