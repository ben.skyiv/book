using System;
using System.Collections.Generic;

sealed partial class E548
{
  static void Main(string[] args)
  {
    var n = (args.Length > 0) ? int.Parse(args[0]) : 4; // g(a^n*b*c)
    int[] a1  = new int[n], a2 = new int[n];
    var bc = new int[2][];
    for (var i = 0; i < bc.Length; i++) bc[i] = new int[n+1];
    a1[0] = 2; a2[0] = (bc[0][0] = 3) * (bc[1][0] = 5);
    for (var i = 1; ; i++) {
      bc[0][i] = bc[0][i-1] * 2;
      bc[1][i] = bc[1][i-1] * 2;
      if (i >= n) break;
      a1[i] = a1[i-1] * 2;
      a2[i] = a2[i-1] * 2;
    }
    var c = new HashSet<Gozinta>();
    for (var i = 0; i < bc.Length; i++) {
      Gozinta.Add(c, bc[i]);
      Make1(c, bc[i], a2, 0, n);
      Make1(c, a1, bc[i], 1, n);
      Make2(c, a1, bc[i], a2, n-1);
    }
    Console.WriteLine("g(a^{0}*b*c) = {1}", n, c.Count);
  }
}

