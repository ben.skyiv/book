// clang -O3 683.c && ./a.out | gnomon -i
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define L(x) if (--x < 0) x = n-1
#define R(x) if (++x >= n) x = 0

int f1(int n)
{
  int i = rand() % n, j = rand() % n;
  for (int k = 0; k < (int)1e9; k++) {
    if (i == j) return k;
    switch (rand() % 9) {
      case 1:       L(j); break;
      case 2:       R(j); break;
      case 3: L(i);       break;
      case 4: L(i); L(j); break;
      case 5: L(i); R(j); break;
      case 6: R(i);       break;
      case 7: R(i); L(j); break;
      case 8: R(i); R(j); break;
    }
  }
  fprintf(stderr, "*"); exit(1);
}

double f(int k, int n, int *min, int *max)
{
  *min = 1 << 30; *max = 0;
  double z = 0;
  for (int i = k; i > 0; i--) {
    int w = f1(n);
    z += (double)w * w;
    if (w < *min) *min = w;
    if (w > *max) *max = w;
  }
  return z / k;
}

int main(void)
{
  srand(time(NULL));
  const int k = (int)1e7, n = 50;
  printf("%2d: (%d)\n", n, k); fflush(stdout);
  double z = 0;
  for (int min, max, i = 2; i <= n; i++) {
    double w = f(k, i, &min, &max); z += w;
    printf("%2d: %13lf %14lf %d %4d\n", i, w, z, min, max);
    fflush(stdout);
  }
}
