import Data.List

main = print $ let n = 100 in length $ nub [ a^b | a <- [2..n], b <- [2..n] ]

