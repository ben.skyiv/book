import Data.Maybe ( fromJust )
import Data.List ( findIndex,group,nub,sort )

hand x = (rank, kind =<< [4,3,2,1]) where
  rank
    | flush && straight   = 9
    | hasKinds 4          = 8
    | all hasKinds [2,3]  = 7
    | flush               = 6
    | straight            = 5
    | hasKinds 3          = 4
    | length (kind 2) > 1 = 3
    | hasKinds 2          = 2
    | True                = 1
  hasKinds = not . null . kind
  ranks    = reverse $ sort $ map fst x
  flush    = length (nub (map snd x)) == 1
  kind n   = map head $ filter ((==n) . length) $ group ranks
  straight = length (kind 1) == 5 && head ranks - last ranks == 4

main = do
  f <- readFile "054.txt"
  print $ length $ filter (\(a,b) -> hand a > hand b) $ map (splitAt
    5 . map (\[r,s] -> let f s x = fromJust $ findIndex (==x) s
    in ((f"23456789TJQKA") r, (f "SHDC") s)) . words) $ lines f

