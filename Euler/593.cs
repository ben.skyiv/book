using System;
using Skyiv.Utils;

static class E593
{
  static int[] GetS2(int n, int m)
  { // nthPrime < n ln(n ln n) for n > 5
    var primes = Primes.GetPrimes(0, (int)(n*Math.Log(n*Math.Log(n))));
    int[] s1 = new int[n], s2 = new int[n];
    for (var i = 0; i < n; i++) {
      s1[i] = primes[i].ModPow(i+1, m);
      s2[i] = s1[i] + s1[(i+1) / 10000];
    }
    return s2;
  }

  static void Main()
  { // k must be even
    int m = 10007, n = (int)1e7, k = (int)1e5;
    Console.WriteLine("n = {0}, k = {1}", n, k);
    var s2 = GetS2(n, m);
    Console.WriteLine("s2: {0}", s2.Length);
    int[] a = new int[m+m-1], b = new int[m+m-1];
    var t = new int[k]; Array.Copy(s2, t, k); Array.Sort(t);
    int k2 = k / 2, c = t[k2 - 1], d = t[k2];
    for (var i = 0; i < k; i++) ((i < k2) ? a : b)[t[i]]++;
    long z = c + d;
    for (int p, q, i = k; i < n; i++, z += c + d)
      if ((p = s2[i - k]) <= c) {
        a[p]--;
        if ((q = s2[i]) <= c) { a[q]++; while (a[c] == 0) c--; }
        else if (q <= d) a[c = q]++;
        else { a[c = d]++; b[d]--; b[q]++; while (b[d] == 0) d++; }
      } else { // p >= d
        b[p]--;
        if ((q = s2[i]) >= d) { b[q]++; while (b[d] == 0) d++; }
        else if (q >= c) b[d = q]++;
        else { b[d = c]++; a[c]--; a[q]++; while (a[c] == 0) c--; }
      }
    Console.WriteLine("F(n, k) = {0:F1}", z / 2.0);
  }
}

