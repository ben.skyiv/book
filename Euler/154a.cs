using System;
using System.Numerics;
 
sealed class E154
{
  static void Main(string[] args)
  {
    int sum = 0, m = 1000, n = int.Parse(args[0]);
    for (var i = 0; i <= n; i++)
    {
      for (var j = 0; j <= n - i; j++)
      {
        var c = C(n, i, j);
        if (c % m == 0) sum++;
//Console.Write("{0,2} ", c);
      }
//Console.WriteLine();
    }
    Console.WriteLine("(m:{0:N0} n:{1:N0}): {2:N0}", m, n, sum);
  }

  static BigInteger C(int n, int i, int j)
  {
    BigInteger nn = 1, ii = 1, jj = 1;
    for (var i0 = 1; i0 <= i; i0++, n--) { ii *= i0; nn *= n; }
    for (var j0 = 1; j0 <= j; j0++, n--) { jj *= j0; nn *= n; }
    return nn / ii / jj;
  }
}

