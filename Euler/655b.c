#include <stdio.h>
#include <time.h>

const int m = 10000019;

long rev(long n)
{
  long z = 0; for (; n > 0; n /= 10) z = z * 10 + n % 10;
  return z;
}

int g2(int i, long n, long b)
{
  return (rev(n) + (i + n * 10) * (__int128)b) % m == 0;
}

int g1(long n, long b)
{
  int z = 0; for (int i = 0; i < 10; i++) if (g2(i, n, b)) z++;
  return z;
}

long f(int e)
{
  clock_t t = clock();
  printf("%d: ", (e << 1) + 1); fflush(stdout);
  long a = 1; for (int i = 1; i < e; i++) a *= 10;
  long z = 0, b = a * 10;
  for (long i = a; i < b; i++) z += g1(i, b);
  t = clock() - t;
  printf("%10ld (%.0f seconds)\n", z, (float)t / CLOCKS_PER_SEC);
  return z;
}

int main(void)
{ // 17:91, 19:924, 21:8956, 23:
  long z = 0; for (int e = 7; e <= 15; e++) z += f(e);
  printf("(b:%ld)\n", z);
}

