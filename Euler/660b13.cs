// mcs 660b13.cs 660.cs && ./660b13.exe

using System;

static class E660b13
{
  static E660 e = new E660(13);

  static int P445()
  { // (2xxx, 3xxx, 1xxxx) (2xxx, Cxxx, 1xxxx)
    var z = 0;
    for (var i1 = 2; i1 < e.B-1; i1++)
      for (var i2 = i1+1; i2 <= e.B-1; i2++)
        foreach (var s in E660.Permutation(e.GetRest(1, i1, i2), 6))
          z += e.F(e.B*(e.B*(e.B*i1+s[1])+s[2])+s[3],
                   e.B*(e.B*(e.B*i2+s[4])+s[5])+s[6]);
    return z;
  }

  static void Main() { Console.WriteLine(P445()); }
}

