using System;
using System.Collections.Generic;

sealed class E051
{
  bool[] composite;

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  bool Compute(int v)
  {
    var count = 0;
    for (var i = 1; i <= 9; i++)
    {
      if (composite[v + i * 101010]) count++;
      if (count > 1) return false;
    }
    return true;
  }
  
  int Compute()
  {
    composite = Sieve(1000000);
    for (var i = 0; i <= 9; i++)
      for (int i2 = i * 10000, j = 0; j <= 9; j++)
        for (int j2 = i2 + j * 100, k = 1; k <= 9; k += 2)
        {
          var v = j2 + k;
          if (Compute(v)) return v;
        }
    return 0;
  }

  static void Main(string[] args)
  {
    Console.WriteLine(new E051().Compute());
  }
}

