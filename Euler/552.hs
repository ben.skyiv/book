import Data.Maybe ( fromJust )
import System.Environment ( getArgs )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Moduli ( chineseRemainder )

solve lim = sum . map snd $ filter check ps where
  ps = zip [1..] $ takeWhile (<lim) primes
  check (i,p) = any ((==0) . flip mod p) $ take i $
    let next x y = (fromJust $ chineseRemainder [x,y], snd x * snd y)
    in map fst . scanl1 next $ zip [1..] primes
main = getArgs >>= print . solve . read . head

