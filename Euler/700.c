#include <stdio.h>

int main(void)
{
  long m = 4503599627370517, a = 1504170715041707, z = 0;
  for (long max = m, c = 0, i = 1; ; i++) {
    if ((c += a) > m) c -= m;
    if (c >= max) continue;
    z += max = c;
    printf("%16ld %ld\n", c, i);
    if (c == 1) break;
  }
  printf("%ld\n", z);
}
/* 1e13 21m37s
1076492  10260071388
 667327  10543898409
 258162  10827725430
 107159  21939277882
  63315  54990108217
  19471  88040938552
  14569 297173645993
   9667 506306353434
   4765 715439060875
1517926517696910
*/
