#include <stdio.h>

int m(int k, int n) { int m = 0; for (; n % k == 0; n /= k) m++; return m; }

int main(void)
{
  int lgm = 12, n = 200000, v2 = 0, v5  = 0, s2[n+1], s5[n+1];
  s2[0] = 0; s2[1] = 0;
  s5[0] = 0; s5[1] = 0; s5[2] = 0; s5[3] = 0; s5[4] = 0;
  for (int i = 2; i < n; i += 2) s2[i] = s2[i+1] = v2 += m(2, i);
  for (int i = 5; i < n; i += 5)
    s5[i] = s5[i+1] = s5[i+2] = s5[i+3] = s5[i+4] = v5 += m(5, i);
  s2[n] = v2 += 6; s5[n] = v5 += 5; v2 -= lgm - 1; v5 -= lgm - 1;
  long sum = 0; int i9 = 65625, j1 = 4*5*5*5*5*5+2, j2 = 2*5*5*5*5*5*5+2;
  for (int i = 2; i < i9; i++)
    for (int n5=s5[i], n2=s2[i], j9=(n-i)/2+1, j=(i<j1)?j2:i; j < j9; j++)
      if (n5 + s5[j] + s5[n-i-j] < v5 && n2 + s2[j] + s2[n-i-j] < v2)
        sum += (j == n-i-j || i == j) ? 3 : 6;
  printf("%ld\n", sum);
  return 0;
}
