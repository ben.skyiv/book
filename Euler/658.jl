const M = 10^9+7
⦿(a, b) = mod(a * b, M)
⊕(a, b) = mod(a + b, M)

w(a, n) = a ≠ 1 ? (powermod(a, n+1, M) - 1) ⦿ invmod(a-1, M) : n ⊕ 1

function S(k, n)
	binx = ones(Int, k+1)
	bin(a) = binx[a+1]
	for a in 1:k-1
		binx[a+1] = bin(a-1) ⦿ (k + 2 - a) ⦿ invmod(a, M)
	end

	betax = zeros(Int, k)
	beta(a) = betax[a+1]
	for a in 1:k-1
		betax[a+1] = (bin(a) - (-1)^a - beta(a-1)) ⦿ invmod(2, M)
	end

	reduce(⊕, beta(a) * (-1)^(a+1) * w(a, n) for a in 0:k-1)
end

@show @time S(10^7, 10^12)
