import Math.NumberTheory.Moduli ( powerMod )
import Math.NumberTheory.Primes.Factorisation ( φ )

t 0 = 1; t n = 2 ^ t (n-1)
h n m | n < 4 = mod (t n) m | True = powerMod 2
  (let (p,q) = g m; z = φ q in until (>p) (+z) $ h (n-1) z) m
g m = let f (c,m) | odd m = (c,m) | True = f (c+1, div m 2) in f (0,m)
f 0 _ = 1; f 1 _ = 3; f 2 _ = 7
f 3 m = powerMod 2 (6::Int) m - 3; f 4 m = h 7 m - 3
f _ m = h (length $ takeWhile (>1) $ iterate (φ . snd . g) m) m - 3
main = let m = 14^8 in print $ mod (sum [f n m | n <- [0..6]]) m

