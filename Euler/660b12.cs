// mcs 660b12.cs 660.cs && ./660b12.exe

using System;

static class E660b12
{
  static E660 e = new E660(12);

  static int P444()
  { // (1xxx, 2xxx, xxxx) (1xxx, Axxx, xxxx) (5xxx, 6xxx, xxxx)
    var z = 0;
    for (var i1 = 1; i1 <= e.B / 2; i1++)
      for (var i2 = i1+1; i2 <= e.B - i1; i2++)
        foreach (var s in E660.Permutation(e.GetRest(i1, i2), 6))
          z += e.F(e.B*(e.B*(e.B*i1+s[1])+s[2])+s[3],
                   e.B*(e.B*(e.B*i2+s[4])+s[5])+s[6]);
    return z;
  }

  static int P345()
  { // (xxx, Bxxx, 10xxx)
    var z = 0;
    foreach (var s in E660.Permutation(e.GetRest(0, 1, e.B-1), 6))
      z += e.F(      e.B*(e.B*s[1] +s[2])+s[3],
        e.B*(e.B*(e.B*(e.B-1)+s[4])+s[5])+s[6]);
    return z;
  }

  static void Main() { Console.WriteLine(P444() + P345()); }
}

