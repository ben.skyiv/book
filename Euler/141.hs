import Data.List ( nub )
import Math.NumberTheory.Powers.Squares ( isSquare )

f n a b = let h a b c = a^3*b*c*c + b*b*c in [h a b c |
  c <- [b,b+b]] ++ takeWhile (<n) [h a b (x*x) | x <- [1..]]

main = let n = 10^12; m = 120 in print $ sum $ nub
  [c | (a,b) <- takeWhile (\(a,b) -> a^3*b + b*b < n)
  [(a,b) | a <- [2..m], b <- [1..a-1]], gcd a b == 1,
  c <- f n a b, isSquare c]

