using System;
using System.Drawing;
using System.Collections.Generic;
using Line = System.Tuple<int,int,int>;

static class E630
{
  static int Gcd(int a, int b) { return b == 0 ? a : Gcd(b, a%b); }

  static IEnumerable<Point> GetPoints(int n)
  {
    for (long s = 290797; n > 0; n--) {
      s = s * s % 50515093; var x = s % 2000 - 1000;
      s = s * s % 50515093; var y = s % 2000 - 1000;
      yield return new Point((int)x, (int)y);
    }
  }

  static Line GetLine(Point pt1, Point pt2)
  {
    if (pt1 == pt2) throw new Exception("pt1 == pt2");
    int a = pt2.Y - pt1.Y, b = pt1.X - pt2.X;
    int c = pt2.X * pt1.Y - pt1.X * pt2.Y;
    if (a < 0 || (a == 0 && b < 0)) { a = -a; b = -b; c = -c; }
    var g = Math.Abs(Gcd(Gcd(a, b), c));
    if (g != 1) { a /= g; b /= g; c /= g; }
    return Tuple.Create(a, b, c);
  }

  static void Main()
  {
    var n = 2500;
    var pts = new List<Point>();
    foreach (var pt in GetPoints(n)) pts.Add(pt);
    var lines = new HashSet<Line>();
    var ks = new Dictionary<Point, int>();
    long z = 0;
    for (var i = 1; i < n; i++)
      for (var j = 0; j < i; j++) {
        var u = GetLine(pts[i], pts[j]);
        if (lines.Contains(u)) continue;
        int d, g = Gcd(u.Item1, u.Item2);
        var k = new Point(u.Item1 / g, u.Item2 / g);
        ks.TryGetValue(k, out d);
        ks[k] = d + 1;
        z += lines.Count - d;
        lines.Add(u);
      }
    Console.WriteLine("{0}: {1} {2}", n, lines.Count, z * 2);
  }
}

