import Data.Array ( (!),array )
import Data.MemoTrie ( memo2 )

data Tree = Tree [(Int, Tree)]

main = print $ sum $ map (optionsAbove (1::Int)) [0 .. length xs - 1] where
  width = 32; height = 10
  tree n
    | n < 2     = Tree []
    | n < 4     = Tree [(n, Tree [])]
    | otherwise = Tree [(i,t) | i <- [2,3], let t@(Tree x) = tree $ n-i, not $ null x]
  expand (Tree []) = [[]]
  expand (Tree xs) = concat $ map (\(i, xs) -> map (i:) $ expand xs) xs
  holes xs []         = xs
  holes (x:xs) (y:ys) = holes ((x+y):x:xs) ys
  xs = zip [0..] $ map (tail . reverse . tail . holes [0]) $ expand $ tree width
  optionsAbove' n m
    | n == height = 1
    | otherwise = sum $ map (optionsAbove $ n+1) $ (!m) $ array (0, length xs - 1)
      [(fst a, [fst b | b <- xs, not $ any (flip elem $ snd a) (snd b)]) | a <- xs]
  optionsAbove = memo2 optionsAbove'

