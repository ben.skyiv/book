using System;
using Skyiv.Utils;

sealed class E247
{
  struct Square : IComparable<Square>
  {
    public double X, Y, Size;
    public int Left, Below;
    public int CompareTo(Square other) { return (Size < other.Size) ? 1 : -1; }
    public Square(double x, double y, double size, int left, int below) : this()
    { X = x; Y = y; Size = size; Left = left; Below = below; }
  }

  static void Main()
  {
    var queue = new PriorityQueue<Square>();
    queue.Enqueue(new Square(1, 0, (Math.Sqrt(5) - 1) / 2, 0, 0));
    var max = 0;
    for (var n = 1; n < 1000000; n++)
    {
      var s = queue.Dequeue();
      if (s.Left == 3 && s.Below == 3) max = n;
      double x2 = s.X + s.Size, y2 = s.Y + s.Size, a = s.X + y2, b = x2 + s.Y;
      double s1 = (Math.Sqrt(a*a - 4*(s.X*y2-1)) - a) / 2;
      double s2 = (Math.Sqrt(b*b - 4*(x2*s.Y-1)) - b) / 2;
      queue.Enqueue(new Square(s.X, y2, s1, s.Left, s.Below + 1));
      queue.Enqueue(new Square(x2, s.Y, s2, s.Left + 1, s.Below));
    }
    Console.WriteLine(max);
  }
}

