import Data.Set ( elems )
import Data.Maybe ( fromJust )
import Data.List ( findIndex )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Testing ( isPrime )
import Math.NumberTheory.Primes.Factorisation ( divisors )

search x [p] = [p*q | d <- takeWhile (<= div x p + p - 1) $ dropWhile
  (<2*p+1) $ elems $ divisors $ p*p-p+1, let q = d - p + 1, isPrime q]
search x ps = [m*q | let m = product ps; n = product $ map pred ps; p = last ps,
  k <- filter even [div (m*p-1) ((m-n)*p+n) + 1 .. minimum [div (m*(x-1)) (m*n+(m-n)*x),
  head ps - 1, div m (m-n)]], let (q,r) = divMod (n*k+1) (m-(m-n)*k), r == 0, isPrime q]

f a b z = if z == 1 then [[]] else concat [map (p:) $ f p (div b p) $ z-1 | p <- takeWhile
  (<= (floor $ (** (1 / fromInteger z)) $ fromInteger b)) $ dropWhile (<=a) primes]

main = let n = 2*10^11 in print $ sum $ map (sum . concatMap (search n) . f 2 n)
  [2 .. toInteger $ (+1) $ fromJust $ findIndex (>n) $ scanl (*) 1 $ tail primes]

