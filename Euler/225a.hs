main = let n = 124 in print $ (!!(n-1)) $ filter (\x -> ((/=0) . last)
  $ head $ dropWhile (\xs -> last xs /= 0 && sum xs /= 3)
  $ iterate (\[a,b,c] -> [b, c, mod (a+b+c) x]) [1,1,3]) [1,3..]

