using System;
using Skyiv.Utils;

static class E618
{
  static void Main()
  {
    int n = 24, m = (int)1e9, z = 0; var fib = new int[n+1];
    for (int a=1, b=1, c=b, i=1; i <= n; i++, b += a, a=c, c=b) fib[i] = a;
    int[] ps = Primes.GetPrimes(0, fib[n]), s = new int[fib[n]+1]; s[0] = 1;
    foreach (var p in ps)
      for (var i = p; i <= fib[n]; i++)
        s[i] = (int)((s[i] + (long)p * s[i-p]) % m);
    for (var i = 2; i <= n; i++) z = (z + s[fib[i]]) % m;
    Console.WriteLine(z); // 1.168s
  }
}

