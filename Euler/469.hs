import Text.Printf ( printf )

main = let n = 21 in printf "%.14f\n" ((n - 1 - a (n-3)) / n :: Double)
  where a n = if n < 2 then n else ((n-1) * a (n-1) + 2 * a (n-2) + 1) / n

