#include <stdio.h>

int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }

int main(void)
{
  long a, p, n = 25000000000000, z = 0;
  for (int u = 1; 3 * (a = (long)u * u) <= n; u++)
  {
if (u % 1000 == 0) fprintf(stderr, "%d ", u / 1000);
    for (int v = u; ; v++)
    {
      if (gcd(u, v) != 1) continue;
      long b = (long)v * u;
      long c = (long)v * v;
      if (a + b <= c) break;
      if ((p = a + b + c) > n) break;
      z += n / p;
    }
  }
  printf("%ld\n", z);
  return 0;
}

