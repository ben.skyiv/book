f 0 _ _ _ _ = []
f c 1 r k p | r > c = [] | r < c = [(r,1,p)] | True = [(r,k,p*k)]
f c n r k p | r > c = z ++ f c (n-1) (r-c) (k+1) (p*k) | True = z
  where z = f (c-1) n r 1 p
main = let n = 20; dice = 12; top = 10; target = 70 in print $ sum $ concat
  [let v = n-top in scanl (\a b -> div (a*(k+v-b)*(c-1)) $ b+1) (div (product
  [2..n]) $ m * product [k+1..k+v]) [0..v-1] | (c,k,m) <- f dice top target 1 1]

