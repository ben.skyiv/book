main = do
  triangle <- readFile "018.txt"
  print $ (f 0 0 . (map read) . words) triangle

f level offset x
  | idx >= length x = 0
  | otherwise = x!!idx + max (f (level+1) offset x) (f (level+1) (offset+1) x)
  where idx = offset + div (level*(level+1)) 2

