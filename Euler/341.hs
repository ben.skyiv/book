import Data.MemoTrie ( memo )

f :: Int -> Int
f 1 = 1
f n = 1 + g (n - g (g $ n-1))
g = memo f

main = let n = 10^3 in print $ sum [g $ n^3 | i <- [1..n]]

