import Data.Map ( (!),fromList )

main = do
  s <- readFile "089.txt"
  let ss = lines s; z = [1000,900,500,400,100,90,50,40,10,9,5,4,1]
  print $ (-) (length $ concat ss) $ length $ concatMap ((\x -> let
    f 0 = []; f n = let m = head $ dropWhile (>n) z in m : f (n-m) in concat $ map
    ((fromList $ zip z ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"])!)
    (f x)) . (\x -> let
    f [] = []; f [x] = [x]; f (a:b:c) | a < b = (b-a) : f c | True = a : f (b:c)
    in sum $ f $ map ((fromList $ zip "MDCLXVI" [1000,500,100,50,10,5,1])!) x)) ss

