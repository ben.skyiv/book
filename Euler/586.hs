xs = [(u,v) | u <- [1..57], v <- [u+2 .. 28], u^2-5*v^2 == -836]
ys = [(a,b) | (u,v) <- xs, let a = div (v+u) 2; b = div (v-u) 2, b > 0 ]

