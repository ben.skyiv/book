#include <stdio.h>
#include <math.h>

long gety(long x2, int d)
{
  long x21 = x2 - 1;
  for (long y = (long)sqrt((double)x21 / d); ; y++) {
    long dy2 = d * y * y;
    if (x21 == dy2) return y;
    if (x21 < dy2) break;
  }
  return 0;
}

int main(void)
{
  int limit = 61;
  long xmax = 0, MAX = 12345678;
  for (int d = /*2*/60; d <= limit; d++) {
    double d2 = sqrt(d);
    long x = (long)d2;
    if (d2 == x) continue;
    fprintf(stderr, "%d ", d);
    for (x++; x < MAX; x++) if (gety(x * x, d) > 0) break;
    if (xmax < x) xmax = x;
  }
  printf("[%ld]\n", xmax);
  return 0;
}
