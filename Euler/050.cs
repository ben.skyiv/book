using System;
using System.Collections.Generic;

sealed class E050
{
  bool[] composite;
  int[] primes;

  int[] GetPrimes(int max)
  {
    var primes = new List<int>();
    var composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
      if (!composite[i]) primes.Add(i);
    return primes.ToArray();
  }

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  int Compute(int max)
  {
    primes = GetPrimes(max);
    var prime = 0;
    for (int len = -1, i = 0; i < primes.Length; i++)
    {
      long sum = 0;
      for (var j = i; j < primes.Length; j++)
      {
        sum += primes[j];
        if (sum >= composite.Length || composite[sum]) continue;
        if (j - i > len) { len = j - i; prime = (int)sum; };
      }
    }
    return prime;
  }

  static void Main(string[] args)
  {
    Console.WriteLine(new E050().Compute(1000000));
  }
}

