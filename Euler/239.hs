import Text.Printf ( printf )
import Math.Combinat.Numbers ( binomial )

f x y = product [2..x+y] + sum [(-1)^i * product [2..x+y-i] * binomial x i | i <- [1..x]]
main = printf "%.12f" (fromIntegral (binomial 25 3 * f 22 75) / product [2..100] :: Double)

