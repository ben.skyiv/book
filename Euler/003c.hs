factors n = if even n then 2 : factors (div n 2) else fact n 3
fact 1 _ = []
fact n k = if null xs then [n] else let p = head xs in p : fact (div n p) p
  where xs = [ x | x <- [k,(k+2)..ceiling $ sqrt $ fromIntegral n], mod n x == 0 ]
main = print $ last $ factors 600851475143

