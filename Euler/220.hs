import Data.Map ( (!),insert,singleton )

n = 10^12
f t@(a,b,c,i,p,x,y)
  | p >= n*10 = t
  | otherwise = let x' = x + y; y' = y - x; p' = p + p in
    f (insert i x' a, insert i y' b, insert i p' c, i+1, p', x', y')
g t@(x,y,d,i,n)
  | n <= 0 = t
  | n <= c!(i-1) = g (x,y,d,i-1,n)
  | otherwise = let (z,w) = h d i in g (x+z, y+w, mod (d+3) 4, i-1, (c!i)-n)
h 0 i = (a!i,b!i); h 1 i = (b!i,-a!i); h 2 i = (-a!i,-b!i); h 3 i = (-b!i,a!i)
(a,b,c,i,_,_,_) = f (singleton 1 0, singleton 1 1, singleton 1 1, 2, 1, 0, 1)
main = print $ (\(x,y,_,_,_) -> (x,y)) $ g (0,0,0,i,n)

