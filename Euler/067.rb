s = []
File.readlines('067.txt').each do |line|
  s << line.strip.split(" ").map {|x| x.to_i}
end
s.each_with_index do |row, i|
  row.each_with_index do |col, j|
    if i != 0 && j == 0
      s[i][j] += s[i-1][j]
    elsif i != 0 && j == row.count - 1
      s[i][j] += s[i-1][j-1]
    elsif i != 0
      s[i][j] += [s[i-1][j-1], s[i-1][j]].max
    end
  end
end
p s.last.max

