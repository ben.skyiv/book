f t@(z,n,a,b) = if n >= 15 then t else f $ g (z,n,a,b+1)
g t@(z,n,a,b) = if a*(a+b) >= b*b then t else
  let (q,r) = divMod (a*b) (b*b-a*b-a*a) in
  if gcd a b == 1 && r == 0 then f (q,n+1,a+1,b)
  else f (z,n,a+1,b)
main = print $ (\(z,_,_,_) -> z) $ f (0,0,1,1)

