using System;
using Skyiv.Utils;

static class E675
{
  static readonly int m = 87 + (int)1e9, n = (int)1e6;
  static int[] ps = Primes.GetPrimes(0, n);

  static int S2(int n)
  {
    long z = 1;
    foreach (var p in ps) {
      if (p > n) break;
      int e = 0;
      for (var i = n; i > 0; ) e += i /= p;
      z = (z * (1 + (e << 1))) % m;
    }
    return (int)z;
  }

  static void Main()
  {
    int z = 0;
    for (var i = 2; i <= n; i++) z = (z + S2(i)) % m;
    Console.WriteLine("F({0}) = {1}", n, z);
  }
}
