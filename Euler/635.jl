using Primes

function s(n, m)
  z, s2, s3 = 0, 1, 1
  for i in 1:n
    v = powermod(i, -1, m)
    s2 = 2s2 * (2i-1) % m * v % m
    s3 = 3s3 * (3i-2) % m * (3i-1) % m * powermod(2i*(2i-1), -1, m) % m
    !isprime(i) || (z += (5*(-1)^(i+1) * (i-1) + s2 + s3) * v % m)
  end
  mod(z, m)
end

@time @show s(10^8, 10^9+9)

