#include <iostream>
#include <cstring>
#include <bitset>

int q(int n)
{
  static char a0[(int)1e6], b0[(int)1e6], *a = a0, *b = b0;
  int len = 5; memset(a, 1, len);
  for (n--; n > 0; n--, len += 4) {
    memset(b, 0, len + 4);
    for (int i = 0; i < len; i++)
      for (int j = 0; j < 5; j++)
        b[i + j] ^= a[i];
    char *t = a; a = b; b = t;
  }
  for (int i = 0; i < len; i++) n += a[i];
  return n;
}

int main()
{
  long long n = 10, w = 1, z = 0; std::bitset<63> b = n;
  for (int i = 0; i < 18; i++, z += w, w = 1, b = n *= 10)
    for (int k = 0, j = b.size() - 1; j >= 0; j--)
      if ((k = (k << 1) + b[j]) != 0 && ((j > 1
        && b[j-1] == 0 && b[j-2] == 0) || j == 0))
        w *= q(k), k = 0;
  std::cout << z << std::endl;
}
