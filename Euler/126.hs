import Data.List ( group,sort )

main = let c = 1000; v = 20*n in print $ head $ head $
  filter ((==c) . length) $ group $ sort $ map f [(x,y,z,n) |
  x <- takeWhile (\x -> f (x,x,x,1) <= v) [1..],
  y <- takeWhile (\y -> f (x,y,x,1) <= v) [x..],
  z <- takeWhile (\z -> f (x,y,z,1) <= v) [y..],
  n <- takeWhile (\n -> f (x,y,z,n) <= v) [1..]] where
  f (x,y,z,n) = 2*(x*y+y*z+x*z) + 4*(x+y+z+n-2)*(n-1)

