using System;
using System.Collections.Generic;

sealed class E373
{
  int Gcd(int a, int b) { return (b == 0) ? a : Gcd(b, a % b); }
  
  Dictionary<int,List<int>> GetDict(int n)
  {
    var dict = new Dictionary<int,List<int>>();
    for (int m = (int)Math.Sqrt(n - 1), i = 2; i <= m; i++)
      for (int c, j = 1; j < i && (c = i * i + j * j) <= n; j++)
      {
        if ((i + j) % 2 == 0 || Gcd(i, j) > 1) continue;
        int a = 2 * i * j, b = i * i - j * j, p = a, q = b, r = c;
        do
        {
          List<int> list;
          if (!dict.TryGetValue(r, out list))
            dict.Add(r, list = new List<int>());
          list.Add(p); list.Add(q);
          p += a; q += b; r += c;
        } while (r <= n);
      }
    foreach (var kvp in dict) kvp.Value.Sort();
    return dict;
  }

  long Compute(int n)
  {
    long sum = 0;
    foreach (var kvp in GetDict(n))
    {
      long r = kvp.Key, r2 = r * r;
      var d = kvp.Value;
      for (int m = d.Count, i = 0; i < m; i++)
      {
        long u = d[i];
        for (int j = i; j < m; j++)
        {
          long v = d[j];
          long z = (long)Math.Sqrt(r2-v*v)*u + (long)Math.Sqrt(r2-u*u)*v;
          if (z % r == 0 && r * v <= z) sum += r;
        }
      }
    }
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E373().Compute(10000000));
  }
}

