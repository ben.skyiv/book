#include <stdio.h>

int main(void)
{
  const int n = 100000000;
  static int a[n / 2 + 1];
  long v = 0;
  for (int i = 0; i <= n / 2; i++) a[i] = i + 1;
  for (int i = 1; i <= n / 2; i++) {
    v += a[i] * (n / i - 1);
    for (int j = 2; i*j <= n / 2; j++) a[i*j] -= a[i];
  }
  printf("%ld\n", (v - n + 1) * 6);
  return 0;
}

