using System;
using System.Numerics;
using System.Collections.Generic;

sealed class E273
{
  bool IsPrime(int n)
  {
    for (var i = 3; i < n; i += 2) if (n % i == 0) return false;
    return true;
  }

  int[] Get4k1Primes(int max)
  {
    var primes = new List<int>();
    for (var i = 5; i <= max; i += 4)
      if (IsPrime(i)) primes.Add(i);
    return primes.ToArray();
  }

  long Compute(int[] p, Complex[] a, int n, Complex c)
  {
    return n == p.Length ? (long)Math.Min(Math.Abs(c.Real), Math.Abs(c.Imaginary))
      : Compute(p, a, n+1, c) + Compute(p, a, n+1, c * a[p[n]])
      + Compute(p, a, n+1, c * Complex.Conjugate(a[p[n]]));
  }

  long Compute(int n)
  {
    var a = new Complex[n];
    for (var i = 1; i*i < n; i += 2)
      for (var j = 2; i*i + j*j < n; j += 2)
        a[i*i + j*j] = new Complex(i, j);
    var p = Get4k1Primes(n);
    long sum = 0;
    for(var i = 0; i < p.Length; i++) sum += Compute(p, a, i+1, a[p[i]]);
    return sum;
  }
  
  static void Main()
  {
    Console.WriteLine(new E273().Compute(150));
  }
}

