f1 r = floor $ until (\n -> let x = n / (n + sqrt(2*n) + 1)
  in (f 1 - f x + x*x/n/2) / (1 - pi/4) < r) succ 1
  where f x = x - ((x-1) * sqrt(x*(2-x)) + asin(x-1)) / 2
f2 x = floor $ until (\n -> let
  h = 1 / (n + sqrt(2*n) + 1); θ = 2 * asin(sqrt(h/2))
  in (h - θ + sin θ) / 2 / (1 - pi/4) < x) succ 1
f3 x = floor $ until (\n -> let
  h = 1 / (n + sqrt(2*n) + 1); θ = 2 * asin(sqrt(h/2))
  in h / 2 / (1 - pi/4) < x) succ 1
main = mapM_ print $ map (\x -> (x, f1 x, f2 x, f3 x)) [0.1**i | i <- [1..10]]
