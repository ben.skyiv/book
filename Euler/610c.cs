using System.Collections.Generic;
using Skyiv.Utils;

static class E610
{
  static bool Valid(string s) { return s == s.FromRoman().ToRoman(); }

  static void Main()
  {
    List<double> p1 = new List<double>(), p2 = new List<double>();
    List<string> s1 = new List<string>(), s2 = new List<string>();
    s1.Add(""); p1.Add(1);            var s3 = new List<string>();
    var cs = "IVXLCDM"; var e = 8; var z = 0.0;
    for (double a1 = 0.02, a2 = 0.14, z0 = 1.0; ; z0 = z) {
      for (var i = 0; i < s1.Count; i++) {
        s3.Clear(); string t;
        foreach (var c in cs) if (Valid(t = s1[i] + c)) s3.Add(t);
        var a3 = p1[i] / (a1 + a2 * s3.Count); s2.AddRange(s3);
        foreach (var _ in s3) p2.Add(a2 * a3);
        z +=     s1[i].FromRoman() * a1 * a3;
      }
      if (System.Math.Round(z0, e) == System.Math.Round(z, e)) break;
      var s0 = s1; s1 = s2; s2 = s0; s2.Clear();
      var p0 = p1; p1 = p2; p2 = p0; p2.Clear();
    }
    System.Console.WriteLine(System.Math.Round(z, e));
  }
}
