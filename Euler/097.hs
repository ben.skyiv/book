import Math.NumberTheory.Moduli ( powerMod )

main = let m = 10^10 in print $ mod (28433 * powerMod 2 (7830457::Int) m + 1) m

