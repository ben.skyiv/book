import Data.Array ( (!),listArray )

main = let n = 100 in print $ length $ filter ((<n) . (sum . map
  ((listArray (0,62) $ 0:25:50:[1..20] ++ [2,4..40] ++ [3,6..])!)))
  [[a,b,c] | a <- 2:[23..42], b <- [0..62], c <- [b..62]]

