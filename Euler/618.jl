using Primes; using Nemo
n = 10; m = 10^9; fib = [1 1]
for i = 3 : n fib = [fib fib[i-1] + fib[i-2]] end
R,x = PowerSeriesRing(ResidueRing(ZZ,m),fib[n]+1,"x")
P = inv(prod([1 - p*x^p for p = primes(fib[n])]))
println(sum([coeff(P,fib[i]) for i = 2 : n]))

