#include <stdio.h>

const int SIZE = 15;
static int a[SIZE*(SIZE+1)/2];

int maximum(int a, int b) { return (a > b) ? a : b; }

void read()
{
  for (int i = 0; i < sizeof(a)/sizeof(a[0]); i++) scanf("%d", a + i);
}
int compute(int level, int offset)
{
  if (level >= SIZE) return 0;
  int sum1 = compute(level+1, offset);
  int sum2 = compute(level+1, offset+1);
  return a[level*(level+1)/2+offset] + maximum(sum1, sum2);
}

int main(void)
{
  read();
  printf("%d\n", compute(0, 0));
  return 0;
}
