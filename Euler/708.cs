using System;
using Skyiv.Utils;

static class E708
{
  static readonly long n = (long)1e8;
  static readonly PrimeHelper ph = new PrimeHelper(n);
  static readonly int[] ps = ph.GetPrimes(true);
  static long z = 1;

  static void S(long v, int q, int q0, int d)
  {
    for (int p, i = 0; (p = ps[i]) <= q; i++) {
      long u = n / v / p; if (u < q0) return;
      z += (ph.π(u) - ph.π(q0 - 1)) << d;
      S(v * p, p, q0, d + 1);
    }
  }

  static void Main()
  {
    Console.WriteLine("S({0:N0})", n);
    z += ph.π(n) << 1;
    for (int p, m = (int)Math.Sqrt(n), i = 0; (p = ps[i]) <= m; i++) {
      long u = n / p; if (u < p) break;
      z += (ph.π(u) - ph.π(p - 1)) << 2;
      S(p, p, p, 3);
    }
    Console.WriteLine("{0:N0}", z);
  }
}
