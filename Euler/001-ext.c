#include <stdio.h>
#include <time.h>

long gcd(long a, long b) { return b == 0 ? a : gcd(b, a % b); }
long lcm(long a, long b) { return a / gcd(a, b) * b; }

long c1(long n, int a[], int size)
{
  long z = 0;
  for (long i = 1; i <= n; i++) {
    int b = 0;
    for (int j = 0; j < size; j++)
      if (i % a[j] == 0) { b = 1; break; }
    if (b) z += i;
  }
  return z;
}

long f(long n, long d) { return n /= d, (d * n * (n + 1)) >> 1; }

long c2a(long n, int a[], int size, long m, int odd)
{
  long z = 0;
  for (int i = size - 1; i >= 0; i--) {
    long d =  lcm(m, a[i]);
    // printf("%cf(%ld) ", odd ? '-' : '+', d);
    z += (odd ? (-1) : 1) * f(n, d) + c2a(n, a, i, d, !odd);
  }
  return z;
}

long c2(long n, int a[], int size) { return c2a(n, a, size, 1, 0); }

void test(const char* msg, long (*c)(long, int[], int),
  long n, int a[], int size)
{
  clock_t t = clock();
  long z = c(n, a, size);
  t = clock() - t;
  printf("%s: %ld, %9lf seconds\n", msg, z, (double)t/CLOCKS_PER_SEC);
}

int main(void)
{
  static int a[] = {2, 9, 15, 35, 39};
  long n = 999999999;
  int size = sizeof(a) / sizeof(a[0]);
  test("c1", c1, n, a, size);
  test("c2", c2, n, a, size);
  return 0;
}

