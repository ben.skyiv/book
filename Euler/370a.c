#include <stdio.h>
#include <math.h>

#define max(a,b) ((a)>(b)?(a):(b))

const double eps = 1e-9;
const int sqP = 5000000, N = 1000;
const long P = (long)sqP * sqP;
const int M = max(P/N/N/3,sqP)+5, NN = 6*N*N; // (3+sqrt(5))*N*N
const int NX = max(sqP/2, N)+5; // sqP/sqrt(3+sqrt(5))
static char a[M], mu[M];
static int num[NN];
static long sx[NX];

int main()
{
  double phi = (1 + sqrt(5)) / 2, d35 = 3 + sqrt(5);
  for (int i = 1; i < M; i++) mu[i] = 1;
  for (int i = 2; i < M; i++)
    if(!a[i])
      for (int j = i; j < M; j += i)
        a[j] = 1, mu[j] = (j/i%i) ? -mu[j] : 0;
  for (int n = 1; n <= N; n++)
    for (int x = n; x < n*phi - eps; x++)
      num[n*n + n*x+x*x]++;
  long Ps, ans = 0;
  for (int n = 1; n < NN; n++) {
    if (num[n] == 0) continue;
    long p = P/n, sqp = sqrt(p) + eps, cur = 0;
    for (int i = 1; i <= sqp; i++) cur += mu[i] * p / ((long)i*i);
    ans += num[n] * cur;
  }
  for (int n = N+1; n < NX; n++) sx[n] = sx[n-1] + (int)((phi-1)*n) + 1;
  for (int maxs = P/3/(N+1)/(N+1), s = 1; s <= maxs; s++) {
    if (mu[s] == 0) continue;
    int nx = max(sqrt((Ps=P/s)/d35)+eps, N) + 1;
    for (long xx; xx = nx*phi,nx*(nx+xx)+xx*xx <= Ps; ) nx++;
    ans += sx[nx-1];
    for (int maxn = sqrt(Ps/3.)+eps, n = nx; n <= maxn; n++)
      ans += (int)((sqrt(4*Ps-3*(long)n*n)-n)/2+eps) - n + 1;
  }
  printf("%ld\n", ans);
  return 0;
}

