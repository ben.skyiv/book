-1  + √2 + √3  + √6  : 2√3  + 4√2  + 12
-√2 + √3 + 2   + √6  : 2√2  + 2√6  + 15
-1  + √2 + √5  + √10 : 2√5  + 8√2  + 18
-√2 + √3 + √6  + 3   : 2√3  + 4√6  + 20
-1  + √2 + √6  + √12 : 2√6  + 10√2 + 21
-√3 + 2  + √6  + √8  : 2√2  + 4√3  + 21
-√2 + 2  + √5  + √10 : 2√10 + 6√2  + 21 *
-1  + √2 + √7  + √14 : 2√7  + 12√2 + 24
-1  + √3 + √5  + √15 : 4√5  + 8√3  + 24
-√2 + 2  + √6  + √12 : 4√3  + 8√2  + 24
-√3 + √5 + √6  + √10 : 4√2  + 2√15 + 24
-√2 + 2  + √7  + √14 : 2√14 + 10√2 + 27
-2  + √5 + √8  + √10 : 2√2  + 4√5  + 27
-1  + √3 + √6  + √18 : 4√6  + 10√3 + 28
-√2 + √5 + √6  + √15 : 6√3  + 4√10 + 28
-√2 + √3 + √10 + √15 : 2√5  + 8√6  + 30
-√3 + √6 + √7  + √14 : 2√21 + 8√2  + 30
-2  + √6 + √8  + √12 : 4√2  + 4√6  + 30

F  (30) = 213
F2a(30) =  76
F2b(30) = 119
F4 (30) =  18

