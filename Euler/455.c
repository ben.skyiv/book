#include <stdio.h>

long modPow(long a, long b, long m)
{
  long v = 1;
  for (; b; b >>= 1, a = a * a % m) if (b & 1) v = v * a % m;
  return v;
}

int main(void)
{
  long z = 0;
  for (long x, y, n = 2; n <= 1000000; n++, n += !(n % 10), z += x)
    for (x = n; (y = modPow(n, x, 1000000000)) != x; x = y) ;
  printf("%ld\n", z);
  return 0;
}
