import Math.NumberTheory.Primes.Sieve ( sieveFrom )
import Math.NumberTheory.Moduli ( powerMod )

main = let n = 10^7 in print $ sum [flip mod p $ powerMod 10
  (-1::Int) p | p <- (3:) $ takeWhile (<n) $ sieveFrom 7]

