using System;

static class E551
{
  static int DigitsSum(long n)
  {
    int z = 0;
    for (; n > 0; n /= 10) z += (int)(n % 10);
    return z;
  }
  
  static Tuple<long, long> F2(long r, long i, long j)
  {
    var t = Tuple.Create(0L, r);
    var r0 = (long)Math.Pow(10, i + 2);
    do { t = F1(t.Item1, t.Item2, i, j);
    } while (t.Item2 < r0);
    return t;
  }
  
  static Tuple<long, long> F1(long k, long v, long i, long j)
  {
    Tuple<long, long> t;
    if (i < 0) t = Tuple.Create(k+1, v + j + DigitsSum(v));
    else {
      long d = (long)Math.Pow(10, i + 1), c = v / d;
      var t2 = F2(v % d, i - 1, j + DigitsSum(c));
      t = Tuple.Create(k + t2.Item1, c*d + t2.Item2);
    }
//Console.WriteLine("  F1({0},{1},{2},{3})={4}",k,v,i,j,t);
    return t;
  }

  static long F(long n)
  {
    var t = Tuple.Create(1L, 1L);
    for (var i = (long)Math.Log10(n); i >= 0; i--) {
      for (Tuple<long, long> t2; ; t = t2) {
        t2 = F1(t.Item1, t.Item2, i, 0);
Console.WriteLine("F1({0}, {1}) = {2}", t, i, t2);
        if (t2.Item1 > n) break;
      }
    }
    return t.Item2;
  }

  static void Main()
  {
    Console.WriteLine(F(1234));
  }
}

