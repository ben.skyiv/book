using System;
using System.Collections.Generic;

static class E700
{
  static readonly long a = 1504170715041707, m = 4503599627370517;
  static readonly long v = 3451657199285664; // InvMod(a, m)

  static IEnumerable<long> Next()
  {
    for (long max = m, c = 0; ; ) {
      if ((c += a) > m) c -= m;
      if (c < max) yield return max = c;
    }
  }
  
  static IEnumerable<long> Prev()
  {
    for (long max = m, c = 0, i = 1; ; i++) {
      if ((c += v) > m) c -= m;
      if (c < max) { max = c; yield return i; }
    }
  }

  static void Main()
  {
    var prev = Prev().GetEnumerator();
    var next = Next().GetEnumerator();
    long z = 0;
    for (; ;) {
      prev.MoveNext(); long z0 = prev.Current;
      next.MoveNext(); long z1 = next.Current;
      Console.WriteLine("{0} {1}", z0, z1);
      if (z0 > z1) break;
      z += z0 + ((z0 == z1) ? 0 : z1);
    }
    Console.WriteLine(z);
  }
}
