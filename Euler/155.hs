import Data.Ratio ((%))
import Data.Map as M ((!),fromList,insert)
import Data.Set as S (elems,empty,fromList,singleton,size,union)

main = let
  n = 18
  s = M.fromList $ zip [1..n+1] $ singleton (60%1) : repeat empty
  f (t,s) i = let
    x = foldl (\(u,s) j -> let
      v = union u $ S.fromList $ concat [[z,(x*y)/z] |
        x <- elems $ s!j, y <- elems $ s!(i-j), let z = x+y]
      in (v, insert i v s)) (empty,s) [1 .. div i 2] 
    in (union t $ fst x, snd x)
  in print $ size $ fst $ foldl f (s!1,s) [2..n]

