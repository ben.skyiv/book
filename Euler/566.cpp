#include <set>
#include <tuple>
#include <NTL/ZZ.h>

const int CNT = 3, MAX = 4'000'000;

int find(const std::set<double> s, double x, double eps)
{
  int z = 0;
  for (auto y : s) { if (std::abs(x - y) <= eps) return z; ++z; }
  return -1;
}

void extend(std::set<double> s[], const double r[], int i, double eps)
{
  for (double x = 1; ; i = (i + 1) % CNT) {
    auto it = s[i].lower_bound(x - eps);
    if (it != s[i].end() && std::abs(x - *it) <= eps) return;
    s[i].insert(x); x = (x < r[i] - eps) ? (1 - x) : (x - r[i]);
  }
}

const int *perms(double a, double b, double c)
{
  static int t[CNT + 1]; static double eps = 1e-9;
  double r[] = {a, b, c}; std::set<double> s[CNT];
  for (int i = 0; i < CNT; ++i) extend(s, r, i, eps);
  for (int i = 0; i < CNT; ++i) t[i] = find(s[i], r[i], eps) << 1;
  t[CNT] = (s[0].size() - 1) << 1; return t;
}

std::tuple<int,int> match(int k, const bool s[], const bool r[])
{
  static int next[MAX]; next[0] = -1;
  for (int i = 1, j = -1; i < k; next[i++] = ++j)
    while (j != -2 && r[i] != r[j + 1])
      j = (j == -1) ? -2 : next[j];
  for (int i = 0, j = -1, m; i < k << 1; ++i) {
    while (s[i] != r[j + 1]) j = next[j];
    if (++j != k - 1) continue;
    if (k % (m = k - next[k - 1] - 1) != 0) m = k;
    return { m, (i + 1) % m };
  }
  return { 0, 0 };
}

long solve(const int start[], const int each[], int n)
{
  static bool s[MAX << 1], r[MAX], visited[MAX];
  for (int i = 0; i < n; ++i) visited[i] = false;
  long z = 0, w = 1, g, t;
  for (int k = 0, i = 0; i < n; ++i) {
    if (visited[i]) continue;
    for (int j = i; !visited[j]; j = each[j], ++k)
      visited[j] = true, s[k] = start[j] & 1, r[k] = j & 1;
    for (int i = 0; i < k; ++i) s[i + k] = s[i];
    int m, v; std::tie(m, v) = match(k, s, r); k = 0;
    if (m == 0 || (g = NTL::GCD(w, m), z % g != v % g)) return 1e18;
    m /= g, t = (v - z) * NTL::InvMod(w / g, m) / g % m;
    if (t < 0) t += m; z += t * w, w *= m;
  }
  return z;
}

long count(const int t[])
{
  static int s[CNT + 1][MAX];
  for (int i = 0; i < CNT; ++i)
    for (int j = 0; j < t[CNT]; ++j)
      s[i][j] = (j >= t[i]) ? (j - t[i]) : (t[CNT] - 1 - j);
  for (int i = 0; i < t[CNT]; ++i) s[3][i] = s[1][s[0][i]];
  for (int i = 0; i < t[CNT]; ++i) s[1][i] = s[2][s[3][i]];
  return std::min(solve(s[0], s[1], t[CNT]) * CNT + 1,
         std::min(solve(s[3], s[1], t[CNT]) * CNT + 2,
                  solve(s[1], s[1], t[CNT]) * CNT + 3));
}

int main()
{
  int n = 53; long z = 0;
  for (int a = 9; a < n - 1; ++a)
    for (int b = a + 1; b < n; ++b)
      for (int c = b + 1; c <= n; ++c)
        z += count(perms(1.0 / a, 1.0 / b, 1 / sqrt(c)));
  printf("%ld\n", z);
}
