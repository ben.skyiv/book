#include <stdio.h>

int main(void)
{
  long m = 1000000000, z = 0, a = 1, b = 1, c = 1, k = 1, v = 0;
  for (int n = 1; n <= 10000; n++) {
    a = a*3 % m; b = b*6 % m; c = c*9 % m; k = k*10 % m;
    v = (v*2+(n-1&1)) % m; z = (z + (b-a)*(2*k-a-b) + 4*v*(c-a)*(k-1)
      + (((n&1) == 0)	? (c-a)*(a+c-2*k) : (c-a)*(a+c-2))) % m;
  }
  printf("%ld\n", z);
  return 0;
}


