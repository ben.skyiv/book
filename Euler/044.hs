import Data.Set ( fromList,member )

main = print $ head [a-b | let s = [div (3*n*n-n) 2 | n <- [1..10^4]], let
  f = flip member $ fromList s, a <- s, b <- takeWhile (<a) s, f $ a+b, f $ a-b]

