#include <iostream>

typedef struct { int ps[10]; char ks[10], n; } item;

int inv(int a, int m)
{
  int x = 1, y = 0, z = 0, w = 1, b = m;
  for (a = (a < b) ? a : (a % b); a != 0; ) {
    int c = b / a; b -= c * a; z -= c * x; w -= c * y;
    std::swap(a, b); std::swap(x, z); std::swap(y, w);
  }
  return (z < 0) ? (z + m) : z;
}

int I(int ps[], char ks[], int k, int n, long m, long v)
{
  if (k == 0) return v;
  int u, r1, r2, p = ps[k - 1], w = 1;
  for (int i = 0; i < ks[k - 1]; i++) w *= p;
  if (p == 2) {
    if (ks[k - 1] == 1) return (v % 2) ? v : (v + m);
    if (ks[k - 1] == 2) {
      for (r1 = v; r1 % 4 != 1; ) r1 += m;
      for (r2 = v; r2 % 4 != 3; ) r2 += m;
      if (r1 == 4 * m - 1) return r2;
      if (r2 == 4 * m - 1) return r1;
      return (r1 > r2) ? r1 : r2;
    }
    r1 = ((u = inv(w, m)) * w * (v - 1L) + 1) % (w*m);
    r2 = (u * w * (v + 1L) - 1 + w*m) % (w*m);
    if (r1 < r2 && r2 != n - 1) r1 = r2;
    r2 = (u*w * (v + w*m - (w/2 + 1L)) + w/2 + 1) % (w*m);
    if (r1 < r2) r1 = r2;
    r2 = (u*w * (v + w*m - (w/2 - 1L)) + w/2 - 1) % (w*m);
    return (r1 > r2) ? r1 : r2;
  }
  r1 = I(ps,ks,k-1,n,w*m,((u=inv(w,m))*w*(v-1L)+1)%(w*m));
  r2 = I(ps,ks,k-1,n,w*m,(u*w*(v+1L)-1+w*m)%(w*m));
  return (r2 == n - 1) ? r1 : (r1 > r2) ? r1 : r2;
}

const int m = 20'000'000, n = 4'500; // n > sqrt(m + n)

void factor(int i0, item a[], const char c[])
{
  static int t[n];
  for (int i = 0; i < n; i++) t[i] = 1, a[i].n = 0;
  for (int p = 2; p * p < i0 + n; p++)
    if (!c[p])
      for (long q = p; q < i0 + n; q *= p)
        for (int i = q-1-(i0-1)%q; i < n; t[i] *= p, i += q)
          if (q != p) ++a[i].ks[a[i].n - 1];
          else a[i].ps[a[i].n] = p, a[i].ks[a[i].n++] = 1;
  for (int i = 0; i < n; i++) {
    if (i + i0 == t[i]) continue;
    a[i].ps[a[i].n] = (i + i0) / t[i];
    a[i].ks[a[i].n++] = 1;
  }
}

int main()
{
  static item a[n];
  static char c[n];
  for (int i = 2; i * i < n; i++)
    if (!c[i])
      for (int j = i * i; j < n; j += i) c[j] = 1;
  long z = 0;
  for (int i0 = -n, i = 3; i <= m; i++) {
    if (i >= i0 + n) factor(i0 = i, a, c);
    z += I(a[i-i0].ps, a[i-i0].ks, a[i-i0].n, i, 1, 1);
  }
  std::cout << z << std::endl;
}

