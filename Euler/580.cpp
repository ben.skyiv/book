#include <iostream>
#include <cmath>

int main()
{
  const long n = (long)1e16; const int m = (int)sqrt(n);
  static int a[m+1]; static bool b[m+1]; long z = (n+3)/4;
  for (int i = 3; i <= m; i += 2) a[i] = -1;
  for (int u, i = 3; i <= m; z += (n/i/i+3)/4*u, i += 2)
    if (u = ((i & 3) == 1 || b[i]) ? a[i] : 0)
      for (int k = i << 1, j = i + k; j <= m; j += k)
        a[j] -= u, b[j] = true;
  std::cout << z << std::endl;
}
