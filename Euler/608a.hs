import Data.Set ( toList )
import Math.NumberTheory.ArithmeticFunctions ( divisors,sigma )
main = let m = 6 :: Int; n = 10^2 in print $ sum $ map (sum .
  map (sigma 0)) [map (*i) (toList $ divisors m) | i <- [1..n]]

