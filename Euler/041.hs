import Data.Char ( intToDigit )
import Data.List ( permutations,sort )
import Math.NumberTheory.Primes.Testing ( isPrime )

main = print $ last $ filter isPrime $ concatMap (map read
  . sort . permutations . enumFromTo '1' . intToDigit) [4,7]

