-- http://files.cnblogs.com/skyivben/002_overview.pdf

main = print $ sum $ takeWhile (<4000000) $
  map fst $ iterate (\(a,b)->(b,a+4*b)) (2,8)
{-
main = print $ sum $ takeWhile (<4000000) $ filter even $
  map fst $ iterate (\(a,b)->(b,a+b)) (1,2)
-}
{-
fibs = 1 : 2 : zipWith (+) fibs (tail fibs)
main = print . sum . takeWhile (<4000000) . filter even $ fibs
-}

