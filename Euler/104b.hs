import Data.List
main = print.(1+).length.fst.break f $ map fst $ iterate (\(a,b)->(b,a+b)) (1,1)
f n = f1 n && f2 n
f1 n = (=="123456789") . sort $ show $ mod n (10^9)
f2 n = (=="123456789") . sort $ take 9 $ show n

