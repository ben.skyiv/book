#include <stdio.h>
#include <math.h>

int main()
{
  const int n = (int)1e8, r = 6;
  static char c[n + 1]; int z = 0;
  for (int a = (int)sqrt(n); a > 1; a--)
    for (int t, v, b = a - 1; b > 0; b--)
      if ((t = a + b, v = t*t + a*b) <= n)
        c[v]++;
  for(int i = 0; i <= n; i++)
    if (c[i] == r) z++;
  printf("f(%d, %d) = %d\n", n, r, z);
}

