import qualified Control.Monad.ST.Strict as StrictST
import qualified Control.Monad.ST.Lazy as LazyST
import Data.Array.ST ( STUArray,newArray,newListArray,readArray,writeArray )
import Control.Monad ( liftM2,when )

data Network s =
   Network
      (STUArray s Telephone Telephone)
      (STUArray s Telephone Size)
 
type Telephone = Int
type Size = Int

initialize :: StrictST.ST s (Network s)
initialize =
   let bnds = (0,999999)
   in  liftM2 Network
          (newListArray bnds [0..])
          (newArray bnds 1)
 
union :: Network s -> Telephone -> Telephone -> StrictST.ST s ()
union net@(Network repr size) m n = do
   mroot <- find net m
   nroot <- find net n
   when (mroot/=nroot) $ do
      msize <- readArray size mroot
      nsize <- readArray size nroot
      let totalSize = msize+nsize
      if msize < nsize
        then do
           writeArray repr mroot nroot
           writeArray size nroot totalSize
        else do
           writeArray repr nroot mroot
           writeArray size mroot totalSize
 
find :: Network s -> Telephone -> StrictST.ST s Telephone
find (Network repr _size) =
   let go n = do
          m <- readArray repr n
          if n==m
            then return m
            else go m
   in  go
 
callersStart :: [Telephone]
callersStart =
   map (\k -> fromInteger $ mod (100003 - 200003*k + 300007*k^(3::Int)) 1000000) [1..]
 
callers :: [Telephone]
callers =
   take 55 callersStart ++
   zipWith (\s24 s55 -> mod (s24+s55) 1000000) (drop 31 callers) callers
 
pairs :: [a] -> [(a,a)]
pairs (x0:x1:xs) = (x0,x1) : pairs xs
pairs _ = error "list of callers must be infinite"

answer :: Int
answer =
   (1+) . length . takeWhile (<990000) $
   LazyST.runST (do
      net@(Network _repr size) <- LazyST.strictToLazyST initialize
      mapM
         (\(from,to) ->
            LazyST.strictToLazyST $
               union net from to >>
               find net 524287 >>=
               readArray size) $
         filter (uncurry (/=)) $
         pairs $ callers)
 
main :: IO ()
main = print answer

