import Math.NumberTheory.Powers.Squares ( isSquare',integerSquareRoot )

main = let p = 25*10^5 in print $ div (p-1) 2 + length [ () |
  a <- [2 .. div p 3], b <- [a .. div (p-a) 2], let c2 = a*a + b*b - 1,
  isSquare' c2, a + b + integerSquareRoot c2 <= p  ]

