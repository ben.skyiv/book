import Data.Set ( elems )
import Math.NumberTheory.Primes.Factorisation ( τ,divisors )

t s = length $ filter (\(a,b) -> let (q,r) = divMod (b-2) (a+1) in q+q+r+4 < a)
  $ takeWhile (\(a,b) -> a < b) [(a, div s a) | a <- elems $ divisors s]
main = let n = 200 in print $ head [s | s <- [2,4..], τ s > n+n, t s == fromIntegral n]

