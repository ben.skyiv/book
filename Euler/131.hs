import Math.NumberTheory.Primes.Testing ( isPrime )

main = print $ length $ takeWhile (<10^6) [p | n <- [1..], let p = 3*n*(n+1)+1, isPrime p]

