using System;

class E225
{
  static void Main()
  {
    int k = 5;
    for (int n = 0; n < 124; k += 2)
    {
      int d = 5;
      for (int a=1, b=1, c=3; d != 0 && a+b+c != 3; d = (a+b+c)%k, a = b, b = c, c = d);
      if (d != 0) n++;
    }
    Console.WriteLine(k - 2);
  }
}

