#include <stdio.h>

int main(void)
{
  int n = 4500;
  for (int i = 0, a = 0; a*a <= n; a++)
    for (int b = 0; a*a + b*b <= n; b++)
      if (a*a + b*b == n)
        printf("%2d: %3d %3d\n", ++i, a, b);
}

