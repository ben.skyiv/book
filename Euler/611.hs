import Data.List ( group,sort ) -- OEIS: A025302
f n = length $ filter odd $ map length $ group $ sort
  [i+j | let s = takeWhile (<n) [i*i | i <- [1..]],
  i <- s, j <- dropWhile (<=i) s, i+j <= n]
main = print $ f $ 10^6

