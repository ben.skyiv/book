main = let n = 50; s = [0..n] in print $ length [() | x0 <- s, y0 <- s,
  x1 <- s, y1 <- s, x0*y1-x1*y0 /= 0, x0*(x1-x0)+y0*(y1-y0) == 0] + n*n

