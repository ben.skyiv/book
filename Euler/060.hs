import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Testing ( isPrime )

main = print $ sum $ head [[p1,p2,p3,p4,p5] |
  let ps1 = takeWhile (<10000) primes; g p q = read $ shows p $ show q,
  let f p = filter (\q -> all isPrime [g p q, g q p]) . dropWhile (<=p),
  p1 <- ps1, let ps2 = f p1 ps1, p2 <- ps2, let ps3 = f p2 ps2,
  p3 <- ps3, let ps4 = f p3 ps3, p4 <- ps4, let ps5 = f p4 ps4, p5 <- ps5]

