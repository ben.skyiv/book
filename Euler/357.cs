// dmcs 357.cs lib/Primes.cs
using System;
using Skyiv.Utils;

sealed class E357
{
  bool[] composite;
  
  bool Valid(int p)
  {
    if (composite[p]) return false;
    var n = p - 1;
    for (var i = 2; ; i++)
    {
      var j = n / i;
      if (i > j) break;
      if (n % i != 0) continue;
      if (composite[i + j]) return false;
    }
    return true;
  }

  long Compute(int max)
  {
    composite = Primes.GetCompositeArray(max);
    long sum = 1;
    for (var p = 3; p < composite.Length; p += 2)
      if (Valid(p))
        sum += p - 1L;
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E357().Compute(100000000));
  }
}

