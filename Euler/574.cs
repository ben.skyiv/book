using System;
using System.Linq;
using System.Collections.Generic;

static class E574
{ // TODO: V(37) == 22 but 25
  static readonly int n = 200;
  static long q2;
  static readonly int[] v = new int[n+1];
  static readonly int[] primes  = {2,3,5,7,11,13,17,19,23};
  static readonly int[] maxExps = {9,6,4,4, 3, 3, 3, 3};
  static readonly int[] masks   = {1,2,4,8,16,32,64,128};
  static readonly int[] pPows = new int[maxExps.Length];

  static void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }
  static void Sort(ref long a, ref long b) {  if (a < b) Swap(ref a, ref b); }

  static void S3(long c, long a)
  {
    if (c < 2 || c > n || c >= q2) return;
    var j = (int)c;
    if (v[j] == 0 || a < v[j]) v[j] = (int)a;
  }

  static void S2(int[] s, int iCount)
  {
//Console.Write(iCount + ":");
//foreach(var k in s) Console.Write(" " + k);
//Console.WriteLine();
    q2 = primes[iCount]; q2 *= q2;
    for (var mask = (1 << (iCount - 1)) - 1; mask >= 0; mask--) {
      long a = 1, b = 1;
      for (var i = 0; i < iCount; i++) {
      long c = s[i];
 	    if ((masks[i] & mask) == 0) a *= c;
	      else b *= c;
	    }
  		Sort(ref a, ref b);
	  	S3(a + b, a);
		  S3(a - b, a);
		}
  }
  
  static int Pow(int a, int b)
  {
    var z = a;
    while (--b > 0) z *= a;
    return z;
  }

  static List<int[]> Exps0(int idx)
  {
    var list = new List<int[]>();
    Exps(list, idx); 
    return list;
  }

  static void Exps(List<int[]> list, int idx)
  {
    if (idx < 0) { list.Add((int[])pPows.Clone()); return; }
    for (var i = 0; i < maxExps[idx]; i++) {
      pPows[idx] = Pow(primes[idx], i+1);
      Exps(list, idx - 1);
    }
  }
  
  static void S1(int iCount)
  {
    foreach (var s in Exps0(iCount - 1)) S2(s, iCount);
  }

  static long S()
  {
    v[2] = 1;
    for (var i = 1; i < primes.Length; i++) S1(i);
for(var i=0;i<v.Length;i++)if (v[i]!=0)Console.Write("{0}:{1} ",i,v[i]);
Console.WriteLine();
    return v.Select(x => (long)x).Sum();
  }

static void Out(int[]s){foreach(var i in s)
Console.Write("{0,2} ",i);Console.WriteLine();}

  static void Main()
  {
    Console.WriteLine("S({0}) = {1:N0}", n, S());
  }
}

