import Data.Array ( (!),listArray )
import Math.NumberTheory.Primes.Sieve ( primes )

n = 2^50
p = let a = takeWhile (<=n) (map (^2) primes) ++ [n+1] in listArray (0, length a) a
f isOdd i x z
  | m < n     = ((f isOdd (i+1) x) . (f (not isOdd) (i+1) m)) v
  | otherwise = v
  where m = p!i * x; v = z + if isOdd then div n m else - div n m
main = print $ (-) n $ f True 0 1 0

