main = print $ ways 200 [ 200,100,50,20,10,5,2,1 ]
ways _ [_] = 1
ways n a@(x:xs)
  | n < 0 = 0
  | otherwise = ways n xs + ways (n-x) a

