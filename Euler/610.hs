import Data.Ratio ( (%) )
import Data.Set ( empty,insert,member )
import Skyiv.RomanNumerals ( fromRoman,toRoman )
rs = foldl (\z x -> insert (toRoman x) z) empty [0..999]
pr c = if c == '#' then 2 % 100 else 14 % 100
f s = let
  (p,v) = foldl (\(p,v) c -> if c == '#' || member (s ++ [c])
    rs then (p + pr c, c : v) else (p,v)) (0, "") "#IVXLCDM"
  in foldl (\z c -> z + pr c / p * if c == '#' then
  fromIntegral (fromRoman s) else f (s ++ [c])) 0 v
main = print $ 14000 % 86 + f ""

