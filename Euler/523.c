#include <stdio.h>

int main()
{
  double e = 0;
  for (int t = 2, i = 2; i <= 30; i++, t *= 2)
    e += (t - 1.0) / i;
  printf("%.2lf\n", e);
  return 0;
}

