using System;
using System.IO;
using System.Collections.Generic;
using Skyiv.Utils;

static class E578
{
  static int n2;
  static int[] primes;
  static Dictionary<long,long> pis;
  
  static Dictionary<long,long> LoadPis()
  {
    pis = new Dictionary<long,long>();
    foreach (var s in File.ReadLines("/home/ben/tmp/578b.txt")) {
      var ss = s.Substring(1, s.Length - 2).Split(',');
      pis.Add(long.Parse(ss[0]), long.Parse(ss[1]));
    }
    return pis;
  }

  static long Pi(long n)
  {
    if (n > n2) return pis[n];
    var i = Array.BinarySearch(primes, (int)n);
    return (i >= 0) ? (i + 1) : ~i;
  }

  static long C(long n, int i, int e)
  {
    long p = primes[i], z = 0;
    if (p > n) return 1;
    if (p * p > n) return Pi(n) - i + 1; // Pi(p-1) == i
    for (int j = 0; j <= e && n >= 1; ++j, n /= p)
      z += C(n, i + 1, (j != 0) ? j : e);
    return z;
  }

  static void Main()
  {
    long n = 10000000000000;
    n2 = 100 + (int)Math.Sqrt(n); //100000000;
    primes = Primes.GetPrimes(0, n2);
    Console.WriteLine("π({0:N0}) = {1:N0}", n2, primes.Length);
    Console.WriteLine("Count: {0:N0}", LoadPis().Count);
    Console.WriteLine("C({0:N0}) = {1:N0}", n, C(n, 0, 64));
  }
}

