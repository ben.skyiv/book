main = let n = 520 in print $ sum $ drop n $ take (n+2) $ iterate
  (\x -> fromInteger(floor $ 2**(30.403243784-x**2))/10^9) (-1)
