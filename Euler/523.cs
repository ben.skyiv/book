using System;
using System.Collections.Generic;

sealed class E523
{
  static void Main(string[] args)
  {
    var n = (args.Length > 0) ? int.Parse(args[0]) : 4;
    var e = new E523();
    for (var i = 1; i <= n; i++) e.F(i);
  }

  void F(int n)
  {
    var s = new Stack<Elem>[2];
    for (var k = 0; k < s.Length; k++) s[k] = new Stack<Elem>();
    new Elem(n).Fork(s[0]);
    long z = s[0].Count, c = z + 1;
//Console.Write("{0} ", z);
    var i = 2;
    for (; ; i++)
    {
      while (s[i%2].Count > 0) s[i%2].Pop().Fork(s[(i+1)%2]);
      var cnt = s[(i+1)%2].Count;
      if (cnt == 0) break;
      c += cnt;
      z += cnt * (long)i;
//Console.Write("{0} ", cnt);
    }
//Console.WriteLine(":" + z);
    Console.WriteLine("[{0,2} {1,4} {2,11:N0} {3,15:N0} {4} ]",
      n, (n == 1) ? n : i, c, z, (double)z/c);
  }
}

sealed class Elem
{
  byte[] a;

  public Elem(int n)
  {
    a = new byte[n];
    for (var i = 0; i < n; i++) a[i] = (byte)(i + 1);
  }
  
  Elem(byte[] b, int i)
  {
    a = new byte[b.Length];
    for (var j = 0; j < i; j++) a[j] = b[j+1];
    a[i] = b[0];
    for (var j = b.Length - 1; j > i; j--) a[j] = b[j];
  }
  
  public void Fork(Stack<Elem> stack)
  {
    var n = a.Length;
    var m = a[0];
    if (m == n) return;
    var u = a[1];
    for (var i = 1; i < n; i++)
    {
      var v = a[i];
      if (v < u) break;
      if (m < v) stack.Push(new Elem(a, i));
      u = v;
    }
  }
  
  /*public override string ToString()
  {
    var sb = new StringBuilder();
    foreach (var i in a) sb.Append(i);
    return sb.ToString();
  }*/
}

