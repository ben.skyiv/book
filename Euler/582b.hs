import Data.Maybe ( fromJust,isJust )
import Math.NumberTheory.Powers ( exactSquareRoot )
main = let n = 10^6 in mapM_ print $ map (\d ->
  (d, [c | a <- [1..n], let c2 = 3*a*(a+d)+d*d,
  let c1 = exactSquareRoot c2, isJust c1,
  let c = fromJust c1, c <= n])) [1..100]

