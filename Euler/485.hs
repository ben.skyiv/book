import Data.Array ( (!),accumArray )
-- u = 10^8; k = 10^5;
main = let u = 10^3; k = 10; d = accumArray (+) 0 (0,u) [(j,1) | i <- [1..u], j <- [i,i+i..u]]
  in print $ sum $ scanl (\m i -> if m == d!(i-1) then foldl (\m j -> max m $ d!j) 0 [i..i+k-1]
  else max m $ d!(i+k-1)) 0 [1..u-k+1]

