main = let c = 31250000 in print $ (\(s,_,_) -> s) $ last
  $ takeWhile (\(_,_,x) -> x >= 0) $ iterate (\(s,y,_) -> let
  y2 = y*y; x = sqrt (c*(8*c+3.2*y2)) + 4.8*c - y2; z = 1.8*(c-y2)
  v = 2 * floor (sqrt x) in (s + (if y /= 0 then 2 else 1) *
  if z >= 0 then v - 2 * floor (sqrt z) else v+1, y+1, x)) (0,0,0)

