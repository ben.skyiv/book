import Data.List ( (\\),group,nub,sort )

main = print $ sum $ map count $ rmdup $ nub $ filter (\x -> elem (sum x)
  [23,34,45]) $ combine 10 d09 where
  d09 = sort $ [0..9] ++ [0..9]
  combine 0 _      = [[]]
  combine n []     = []
  combine n (x:xs) = map (x:) (combine (n-1) xs) ++ combine n xs
  rmdup = foldl (\a i -> if elem (d09 \\ i) a then a else i : a) []
  count a1 = let
    mc xs = foldl (\a x -> div a (product [2..x])) (product [2 .. sum xs]) xs
    tr xs = map length $ group xs
    fz a ns = if head a /= 0 then 0 else mc $ (head ns - 1) : tail ns
    ns1 = tr a1; ns2 = tr a2; n1 = mc ns1; n2 = mc ns2
    n1z = fz a1 ns1; n2z = fz a2 ns2; a2 = d09 \\ a1
    in  (n1-n1z) * n2 + if a1 == a2 then 0 else (n2-n2z) * n1

