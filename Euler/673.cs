using System;
using System.IO;
using System.Collections.Generic;

static class E673
{
  static readonly int n = 500;

  static void F(string fileName)
  {
    var b = new bool[n + 1];
    var set = new HashSet<(int X, int Y)>();
    foreach (var s in File.ReadLines(fileName))
    {
      var ss = s.Split(',');
      int x = int.Parse(ss[0]), y = int.Parse(ss[1]);
      b[x] = b[y] = true;
      set.Add((x, y));
    }
    for (var i = 1; i <= n; i++)
      if (!b[i]) Console.Write(i + " ");
    Console.WriteLine(":{0}", set.Count);
  }

  static void Main()
  {
    F("/home/ben/tmp/beds.txt");
    F("/home/ben/tmp/desks.txt");
  }
}

