import math
def Sai(n):
 primes=[]
 h  = [True] * int(n**(1.0/3)+1)
 h[:2] = [False, False]
 for i in range(2, int(n ** (1.0/3*0.5)) + 1):
    if h[i]:
        h[i*i::i] = [False] * len(h[i*i::i])
 for i, e in enumerate(h):
    if e:
        primes.append(i)
 sums=n
 for p in primes:
  for i in range(0,int(math.log(n)/math.log(p))+1):
   sums+=n//(p**(i+3)) #p**3的倍数
 for p1 in primes:
  if (p1*p1)**3 >=n: break
  for p2 in primes:
   if (p1*p2)**3 >=n: break
   if p2>p1:
    for i in range(0,int(math.log(n)/math.log(p1))+1):
     for j in range(0,int(math.log(n/p1**i)/math.log(p2))+1):
      sums+=n//(p1**(i+3)*p2**(j+3)) #(p1**3的倍数*p2**3的倍数
 return sums

print(Sai(10**3))
