import Data.List (unfoldr, sort)

valid :: Int -> Bool
valid = all (<=2) . unfoldr (\x -> if x == 0 then Nothing else let (q,r) = divMod x 10 in Just(r,q))

g :: Int -> ([Int], Int) -> [([Int], Int)]
g n (xs, r) = [ (y:xs, div (n * y + r) 10) | y <- ys ] where
  ys | null xs   = filter (\x -> (n * x + r) `mod` 10 <= 2) [1..9]
     | otherwise = filter (\x -> (n * x + r) `mod` 10 <= 2) [0..9]

step :: Int -> [([Int], Int)] -> Int
step n xxs
  | null yys  = step n $ concatMap (g n) xxs
  | otherwise = foldl (\a b -> a * 10 + b) 0 . fst . head . sort $ yys
  where yys = filter (valid . snd) xxs

main = print . sum $ map (\x -> step x $ g x ([], 0)) [1..10000]

