import Data.List
main = let a = 2*10^6; t = 4*a in print $ snd $ head $ sort $ unfoldr
  (\(m,n) -> if m > n then Nothing else let w=m*n*(m+1)*(n+1);u=abs(w-t);v=m*n
  in if w > t then Just ((u,v), (m,n-1)) else Just ((u,v), (m+1,n))) (1,2000)
-- main = print . snd . head $ sort [ (abs $ m*n*(m+1)*(n+1)-8*10^6, m*n) | m<-[1..2000], n<-[1..m] ]
