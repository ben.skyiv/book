import Math.Combinat.Numbers ( binomial )
import Math.NumberTheory.Primes.Sieve ( primes )

f n (c:cs) (p:ps) q x | p*q > n = x | True = f n cs ps (p*q)
  $ f n (c:cs) ps q $ x + c * div n (p*q); f _ _ _ _ x = x
main = let n = 10^16; m = 100; k = 4; ps = takeWhile (<m) primes in print $
  f n [(-1)^(i+k+1) * binomial i (k-1) | i <- [0 .. length ps - 1]] ps 1 0

