#include <stdio.h>

int main(void)
{
  const int n = 10; // n must be multiple of 10
  long a = 0, d = 1, z = 15;
  for (int i = 2; i < n; i++, d *= 7)
    z += 6 * (a = (a + 3 * d) * 7) + 105 * d;
  printf(": %10ld: S(7^%d)\n", z, n-1);
  const int ks[] = {2,3,5,5,0,4,3,1,1,6};
  int k = ks[0]; z -= k * a + k * (k - 1) / 2 * d;
  for (int i = 1; i <= n - 2; i++) {
    a = a / 7 + (k - 3) * (d /= 7);
    k = (i == n - 2) ? 2 : ks[i % 10];
    z -= k * a + k * (k - 1) / 2 * d;
  }
  printf(": %10ld: H(%d)\n", z, n);
}

