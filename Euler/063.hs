-- main = print $ sum $ map (\x -> last $ takeWhile (\n -> x^n >= 10^(n-1)) [1..]) [1..9]
main = print $ sum [floor $ 1 / (1 - logBase 10 x) | x <- [1..9]]

