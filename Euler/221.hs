import Data.List ( sort )
import Data.Set ( elems )
import Math.NumberTheory.Primes.Factorisation ( divisors )

main = let n = 150000 in print $ (!! fromIntegral (n-1)) $ sort $ concat
  [map (\(a,b) -> r*(r+a)*(r+b)) $ zip (take (div (length s) 2) s)
  $ reverse s | r <- [1..n], let s = elems $ divisors $ r^2 + 1]

