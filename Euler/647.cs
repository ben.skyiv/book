using System;

static class E647
{
  static readonly long n = (long)1e5;
  static readonly int i9 = 100;

  static bool IsGonal(int k, int a, int b)
  {
    if (k < 3 || (k & 1) == 0) return false;
    for (var i = 1; i <= i9; i++) {
      var n = a * ((i * ((long)i * (k - 2) + 4 - k)) >> 1) + b;
      var d = Math.Sqrt((4L - k) * (4L - k) + (n << 3) * (k - 2));
      if (d != (int)d) return false;
      if (((int)d + k - 4) % ((k << 1) - 4) != 0) return false;
    }
Console.Write("{0},", b);
    return true;
  }

  static long F(int k)
  {
    long z = 0;
    for (var a = 1; a <= n; a++)
      for (var b = 1; b <= n; b++)
        if (IsGonal(k, a, b)) z += a + b;
    return z;
  }

  static void Main()
  {
    F(19);
    //for (var i = 3; i <= 17; i += 2) z += F(i);
    //Console.WriteLine(z);
  }
}

