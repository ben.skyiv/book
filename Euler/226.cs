using System;

sealed class E226
{
  static readonly double eps = 1e-8;
  
  static void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }

  struct X : IComparable<X>
  {
    public long C { get; private set; }
    public int K { get; private set; }
    public readonly double V;
    public X Div2Pow(int k) { return new X(C, K + k); }
    public static X Avg(X a, X b) { return Add(a, b).Div2Pow(1); }
    
    public X(long c, int k) : this()
    {
      C = c; K = k; V = (double)C / (1L<<K);
      if (C > 0) for (; C  % 2 == 0; K--) C /= 2;
    }
    
    public int CompareTo(X other)
    {
      if (K > other.K) return C.CompareTo(other.C*(1L<<(K-other.K)));
      if (K < other.K) return (C*(1L<<(other.K-K))).CompareTo(other.C);
      return C.CompareTo(other.C);
    }
    
    public static X Add(X a, X b)
    {
      if (a.K < b.K) Swap(ref a, ref b);
      return new X(a.C + b.C * (1L << (a.K - b.K)), a.K);
    }
  }
  
  double Blancmange(X x)
  {
    double y = 0;
    long d = 1;
    for (var i = 0; i < x.K; i++, d <<= 1)
    {
      double u = x.V * d, v = u - Math.Floor(u);
      if (v > 0.5) v = 1 - v;
      y += v / d;
    }
    return y;
  }
  
  X Cross()
  {
    X a = new X(0, 1), b = new X(1, 2);
    while (a.CompareTo(b) < 0)
    {
      var x = X.Avg(a, b);
      var y =  0.5 - Math.Sqrt(x.V / 2 - x.V * x.V) - Blancmange(x);
      if (Math.Abs(y) < eps) return x;
      if (y > 0) a = x;
      else b = x;
    }
    throw new Exception();
  }
  
  double Trapzd(double s, X b, int n)
  {
    if (n == 1) return 0.5 * b.V * Blancmange(b);
    int it = 1 << (n - 2);
    X del = b.Div2Pow(n - 2), x = del.Div2Pow(1);
    double sum = 0;
    for (var j = 1; j <= it; j++, x = X.Add(x, del)) sum += Blancmange(x);
    return (s + b.V * sum / it) / 2;
  }
  
  double Qtrap(X b)
  {
    double s = 0, s0 = 0;
    for (var j = 1; j < 20; j++, s0 = s)
      if (Math.Abs((s = Trapzd(s,b,j)) - s0) < eps * Math.Abs(s0)) return s;
    throw new Exception();
  }
  
  double Circle(double x)
  {
    return 0.25 - Math.PI / 64 -
      (x - (x - 0.25) * Math.Sqrt(x/2 - x*x) - Math.Asin(4*x - 1) / 16) / 2;
  }
  
  double Compute()
  {
    var b = Cross();
    return 0.25 - Qtrap(b) - Circle(b.V);
  }

  static void Main()
  {
    Console.WriteLine("{0:F8}", new E226().Compute());
  }
}

