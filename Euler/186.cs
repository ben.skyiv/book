using System;
using System.Collections.Generic;

sealed class E186
{
  static readonly int m = 1000000, target = 990000, pm = 524287;

  static IEnumerable<int> S()
  {
    const int n = 56;
    var a = new int[n];
    for (long k = 1; k < n; k++)
      yield return a[k] = (int)((100003-k*(200003-300007*k*k))%m);
    for (int k = n, i = 0; ; i = ++k % n)
      yield return a[i] = (a[(i + 1) % n] + a[(i + 32) % n]) % m;
  }
  
  static int GetCount(int max)
  {
    var b = new bool[m];
    b[pm] = true;
    var count = 1;
    for (var n = 0; ; n++)
    {
      var enumerator = S().GetEnumerator();
      var k = 0;
      for (var i = 0; i < max; )
      {
        enumerator.MoveNext();
        var caller = enumerator.Current;
        enumerator.MoveNext();
        var called = enumerator.Current;
        if (caller == called) continue;
        if (b[caller] || b[called])
        {
          if      (!b[caller]) { b[caller] = true; k++; }
          else if (!b[called]) { b[called] = true; k++; }
        }
        i++;
      }
      if (k == 0) return count;
      count += k;
    }
  }
  
  static int BinarySearch(int target, int low, int high)
  {
    while (low < high)
    {
      var mid = (low + high) / 2;
      var count = GetCount(mid);
      if (count > target) high = mid - 1;
      else if (count < target) low = mid + 1;
      else return mid;
    }
    throw new Exception("Not found");
  }

  static void Main()
  {
    int max = 1 + BinarySearch(target - 1, 0, 8000000);
    for (; ; max++) if (GetCount(max) >= target) break;
    Console.WriteLine(max);
  }
}

