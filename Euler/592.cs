using System;

static class E592
{
  static readonly ulong m1 = (1UL << 48) - 1;
  static ulong[] a = new ulong[64];

  static int h(ulong n)
  {
    ulong z = 0;
    for (ulong i = n >> 1; i > 0; i >>= 1) z = (z + i) & 3;
    return (int)z;
  }

  static ulong f(ulong n)
  {
    ulong z = 1, v = 1, k = 3; int i;
    for (i = 0; n > 0; i++, n >>= 1) a[i] = n & m1;
    Array.Sort(a, 0, i);
    for (int j = 0; j < i; j++) {
      ulong k9 = a[j];
      Console.WriteLine("> {0}", k9);
      for (; k <= k9; k += 2) v *= k;
      z *= v;
    }
    return z;
  }

  static void Main()
  { // 6: EA2FB47EECF7
    ulong n = 20, z = 1;
    for (; n > 1; n--) z *= n;
    Console.WriteLine("{0:X}", (f(z) << h(z)) & m1);
  }
}

