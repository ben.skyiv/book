import Data.Maybe ( fromMaybe )
import Data.Map.Strict ( empty, insert, lookup )

get key a = fromMaybe 0 $ Data.Map.Strict.lookup key a

main = let n = 10^6{-10^7-} in print $ fst $ foldl (\z x -> let
  a = snd z; q = get x a + div (x*x+6) 12 - div x 4 * div (x+2) 4 in
  (fst z + q, foldl (\z x -> insert x (get x z - q) z) (insert x q a)
  [x+x,x+x+x..n])) (0,empty) [3..n]

