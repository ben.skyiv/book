import Data.List ( permutations )

main = let n = 10 in print $ sum $ map (\m -> (^3) $ length $ filter 
  (\xs -> all (\(a,b) -> abs (a-b) < 4) $ zip xs $ tail xs)
  $ map (\xs -> (1:xs) ++ [m]) $ permutations [2..m-1]) [1..n]

{- [1,1,1,2,6,14,28,56,118,254,541,1140,2401,5074,10738] 15m57.619s -}

