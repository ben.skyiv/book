public class E311 {
  public static void main(String[] args) {
    int m = 25000, n = m*m + 1;
    long z = 0, nl = 4L * m * m;
    byte[] a = new byte[n];
    for (int i = 0, i2 = 0; i < m; i++, i2 = i*i)
      for (int j = 0, j2 = 0; i2+j2+j < n; j++, j2= j*j)
       a[i2+j2+j]++;
    for (int i=0, j=2, d=2; i < n; d = (i==j)?d+2:d, j = (i==j)?j+d:j, i++) {
      if (a[i] <= 2) continue;
      int u = (a[i]-1)*(a[i]-2)/2, v = u * a[i]/3, w = (i!=j)? 0 : 1;
      for(long r = 4L*i + 1; r <= nl; r *= 2, w = (i!=j)?0:1-w) z += v - w*u;
    }
    System.out.println(z);
  }
}

