import Math.NumberTheory.ArithmeticFunctions ( sigma )
-- http://oeis.org/A055410 A046895 A000118
dss :: Int -> Int; dss n = sum [sigma 1 i | i <- [1..n]]
f n = succ $ 8 * dss n - 32 * dss (div n 4)
main = print [(i, f $ i*i) | i <- [2,5,100]]

