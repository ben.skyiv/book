value = {}
val = {"":0,"i":1,"ii":2,"iii":3,"iv":4,"v":5,"vi":6,"vii":7,"viii":8,"ix":9}
p = {"i":.14,"v":.14,"x":.14,"l":.14,"c":.14,"d":.14,"m":.14,"#":.02}
for h in val:
  for t in val:
    for u in val:
      hr = h.replace("x","m").replace("v","d").replace("i","c")
      tr = t.replace("x","c").replace("v","l").replace("i","x")
      value[hr+tr+u] = val[h]*100 + val[t]*10 + val[u]

def E(s): # expected value after leading string of Ms
  pt = 0; valid = []
  for n in p:
    if n=="#" or s+n in value: pt += p[n]; valid.append(n)
  ans = 0
  for n in valid: ans += p[n]/pt * (value[s] if n=="#" else E(s+n))
  return ans

# Expected value of leading string of Ms: x=.14(1000+x) => x=140/.86
print("%.8f" % (140/.86 + E("")))

