main = let n = 10^18 in print $ flip mod (10^9) $ sum
  [f n i | i <- [1 .. floor $ log (fromIntegral n) / log 2]] where
  f n m = let z = 2^m; a = z+z; c = div (n-z) a in if c < 1
    then 0 else let q = a-2; r = n-1-a*c; p = z-1 in if r < q
    then g a 1 (c-1) p q + g a c c p r else g a 1 c p q where
    g a b c p q = div ((p+1)*(q-p+1)*(c-b+1)*(5*p+q+2*a*(b+c)-2)) 2

