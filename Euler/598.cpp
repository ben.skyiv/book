#include <iostream>
#include <map>
using namespace std;

long gcd(long a, long b) { return b ? gcd(b, a%b) : a; }

map<pair<long, long>, long> getmap(int lo, int hi, int np[])
{
  map<pair<long, long>, long> m1, m2;
  m1[pair<long,long>(1, 1)] = 1;
  for (int i = hi; i >= lo; i--) {
    if (np[i] == 0) continue;
    m2.clear();
    for (const auto &p : m1)
      for (int j = 0; j <= np[i]; j++) {
        long a = p.first.first * (j+1);
        long b = p.first.second * (np[i]-j+1);
        long g = gcd(a, b);
        m2[make_pair(a/g, b/g)] += p.second;
      }
    m1.swap(m2);
  }
  return m1;
}

int main()
{
  int np[101] = {0};
  for (int i = 2; i <= 100; i++)
    for (int j = 2, k = i; j <= 100; j++)
      while (k % j == 0) k /= j, np[j]++;
  auto m1 = getmap(7, 100, np), m2 = getmap(0, 6, np);
  long z = 0;
  for (const auto &p : m1)
    if (m2.count(p.first)) z += m2[p.first] * p.second;
  cout << z / 2 << endl;
}

