import Data.List ( (\\),group )
import Math.NumberTheory.Primes ( primes )
import Math.NumberTheory.Powers ( powerMod )
m = 9 :: Int; m1 = 10^m :: Int; m2 = 4*5^(m-1) :: Int
pr :: [Int] -> Int; pr = foldl (\a b -> mod (a*b) m1) 1
p2 :: Int -> Int; p2 n = fromIntegral $ powerMod 2 n $ fromIntegral m1
bs :: Integer -> [Integer]; bs n = map (flip mod (fromIntegral m1)) $
  scanl (\a i -> div (a*(n-i+1)) i) 1 [1..n]
f1 :: Int -> [Int]; f1 n = map (\i -> floor $ logBase (fromIntegral i)
  (fromIntegral n)) $ takeWhile (<= fromIntegral n) primes
f2 :: [Int] -> [(Int,Int)]; f2 = map (\xs -> (head xs, length xs)) . group
f3 :: [(Int,Int)] -> [[([Int], Int)]]; f3 = map (\(a,b) -> zip
  [replicate i a | i <- [0..b]] $ map fromIntegral $ bs $ fromIntegral b)
f4 :: [[([Int], Int)]] -> [([Int],Int)]; f4 xs = zip
  (foldl (\a b -> [i++j         | i <- a, j <- b]) [[]] $ map (map fst) xs)
  (foldl (\a b -> [mod (i*j) m1 | i <- a, j <- b]) [1]  $ map (map snd) xs)
hl :: Int -> Int; hl n = let zs = f1 n; xss = f4 $ f3 $ f2 zs
  in foldl (\z (xs,i) -> mod (z +
  (if even (length zs - length xs) then 1 else -1) * i * p2
  (pr (map succ xs) * pr (zs \\ xs))) m1) 0 xss
main :: IO(); main = print $ hl 5000

