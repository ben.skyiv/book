using System;

static class E579
{
  static double D(int x, int y, int z, int a, int b, int c, int d)
  {
    return Math.Abs(a*x+b*y+c*z+d) / Math.Sqrt(a*a+b*b+c*c);
  }

  static void Main()
  {
    int cnt = 0, d = 3, n = 5;
    var ds = new double[6];
    for (var x = 0; x <= n; x++)
      for (var y = 0; y <= n; y++)
        for (var z = 0; z <= n; z++) {
          ds[0] = D(x,y,z,2,2,-1,-12); ds[1] = D(x,y,z,2,2,-1,-3);
          ds[2] = D(x,y,z,2,-1,2,-12); ds[3] = D(x,y,z,2,-1,2,-3);
          ds[4] = D(x,y,z,-1,2,2,-12); ds[5] = D(x,y,z,-1,2,2,-3);
          if (ds[0] + ds[1] != d || ds[2] + ds[3] != d
            || ds[4] + ds[5] != d) continue;
          var b = false; foreach (var t in ds) if (t == 0) b = true;
          Console.WriteLine("{0},{1},{2} {3}", x, y, z, b ? "*" : "");
          cnt++;
        }
    Console.WriteLine(cnt);
  }
}

