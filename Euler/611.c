#include <stdio.h>
#include <math.h>

#define N 1000000000000LLU
#define sqrt_N 1000000LLU

int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }

int main() {
  enum { R = sqrt_N+1, m = 4, phi = 2 };
  static long C_hi[R][phi], C_lo[R][phi];  // Get prime counts
  static int L[phi], L_inv[m], prods[phi][phi];
  for (int i = -1, r = 0; r < m; ++r)
    if (gcd(m, r) == 1) {
      L[++i] = r, L_inv[r] = i;
      for (int j = 1; j < R; ++j)
        C_lo[j][i] = (  j-r+m)/m - (r<=1) - !r,
        C_hi[j][i] = (N/j-r+m)/m - (r<=1) - !r;
    }
    else L_inv[r] = -1;
  for (int i = 0; i < phi; ++i)
    for (int j = 0; j <= i; ++j)
      prods[i][j] = prods[j][i] = L_inv[L[i] * L[j] % m];
  for (int px, p = 2; p < R; ++p)
    if((px=L_inv[p % m]) >= 0 && C_lo[p][px] != C_lo[p-1][px]) {
      int *prodp=prods[px];
      for(long j=1,q;(q=N/p/j)>=p && j<R;++j) {// N/p/j = (N/j)/p
        long *Cq=(q<R)?C_lo[q]:C_hi[N/q];
        for(int i=0;i<phi;++i)
          C_hi[j][prodp[i]]-= Cq[i]-C_lo[p-1][i];
      }
      for(int j=R-1,q;(q=j/p)>=p;--j)
        for(int i=0;i<phi;++i)
          C_lo[j][prodp[i]]-= C_lo[q][i]-C_lo[p-1][i];
    }
  long ans = 0;
  static char X[R];
  for(long p=5; p<R; p+=4)
    if(C_lo[p][0]!=C_lo[p-1][0]) {
      //odd power of small prime
      for(long c = N / p; c; c /= pow(p,4))
        for(long x, even = 0; even < 2; ++even)
          x = sqrt(c >> even), ans += x - x / p;
      //square or double a square
      for(long p_pow=p, p_exp=1; p_pow<R; p_pow*=p, p_exp^=1)
        for(long j=0, ind=0; j<R; j+=p_pow, ++ind) 
          if(ind%p)
            X[j]^=p_exp;
    }
  for(long ind=1; ind<R; ++ind)
    ans += X[ind] << (ind<=sqrt(N>>1));
  //big prime
  for(long q=R, c, prevc=C_lo[R-1][0]; --q; prevc=c)
    ans += ((c=C_hi[q][0]) - prevc) * ((long)sqrt(q) + (long)sqrt(q>>1));
  printf("%ld\n", ans);                                                              
}

