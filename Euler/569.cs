using System;
using System.Threading;
using System.Threading.Tasks;
using Skyiv.Utils;

static class E569
{
  static long[] xs;
  static int[] ys;

  static int Init(int max)
  {
    var ps = Primes.GetPrimes(0, max);
    var n = (ps.Length + 1) / 2;
Console.WriteLine("prime:{0:N0}, count:{1:N0}, n:{2:N0}",
ps[ps.Length-1], ps.Length, n);
    xs = new long[n];
    ys = new int[n];
    xs[0] = ys[0] = 2;
    for (var i = 1; i < n; i++) {
      int a = ps[i+i-1], b = ps[i+i];
      xs[i] = xs[i-1] + b + a;
      ys[i] = ys[i-1] + b - a;
    }
    return n;
  }
  
  static int P(int n)
  {
    n--;
    var z = 0;
    double min = 1;
    var low = Math.Max(0, n - 40000);
    for (var i = n-1; i >= low; i--) {
      var k = (double)(ys[n]-ys[i])/(xs[n]-xs[i]);
      if (k < min) { min = k; z++; }
    }
    return z;
  }

  static void Main()
  {
    var n = Init(86028113);//86,028,113==prime(2,500,000*2-1)
    long z = 0;
    //for (var i = 2; i <= n; i++) z += P(i);
    Parallel.For(2, n+1, () => 0L, (i,_,x) => x+P(i),
      x => Interlocked.Add(ref z, x));
    Console.WriteLine(z);
  }
}
//    25,000:  142491   3.912s   611951
//   250,000: 1752332 393.437s  7368743
// 2,500,000:21025060       ?s

