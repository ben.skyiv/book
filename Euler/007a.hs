isPrime (x:xs) n = (x*x > n) || (mod n x /= 0) && (isPrime xs n)
primes = 2 : filter (isPrime primes) [3,5..]
main = let n = 10001 in print $ primes !! (n - 1) -- 0m0.184s
{-
sieve (x:xs) = x : sieve (filter ((/=0) . (`mod` x)) xs)
main = let n = 10001 in print (sieve [2..] !! (n - 1))
-}

