#include <stdio.h>

int main()
{
  long z = 0, j = 1, m = 7+(long)1e9, k = (m+1)/2, n = (long)1e16;
  for (int i = 1; j <= n; j += ++i) {
    long a = n - j + 1, b = a / i, c = b + 1;
    z = (b % m * (c % m) % m * k % m * i % m + a % i * c % m + z) % m;
  }
  printf("S(%ld) = %ld\n", n, z);
}
