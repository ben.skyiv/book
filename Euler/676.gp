mx1(n,e1)=q=2^e1;k=floor(log(n)/log(q));2^k*(q^(k+1)-1)/(q-1)
m42(n)=q=2^4;k=floor(log(n)/log(q));r=3;r2=3;s=4^k;s2=s/4;t=6;\
r*t*s2*(q^k-1)/(q-1)+r2*s*q^k
m62(n)=q=2^6;k=floor(log(n)/log(q));r=4;s2=4^(k-1);t=6;\
r*t*s2*(q^(k+1)-1)/(q-1)
m63(n)=q=2^6;k=floor(log(n)/log(q));r=8;s2=8^(k-1);t=28;\
r*t*s2*(q^(k+1)-1)/(q-1)
\\ m5x(n,e2)=q=2^(5*e2);k=floor(log(n)/log(q));2^k*(q^(k+1)-1)/(q-1)
\\ m64(n)=q=2^(6*2);k=floor(log(n)/log(q));2^k*(q^(k+1)-1)/(q-1)
n=10^16;m=10^16
z=mx1(n,3)+mx1(n,4)+mx1(n,5)+mx1(n,6)+m42(n)+m62(n)+m63(n)
print(z%m)

