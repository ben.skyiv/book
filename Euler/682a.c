#include <stdio.h>

int main(void)
{
  const int m = 7 + (int)1e9, n = (int)1e7;
  const int a[][3] = { {4,5,7}, {5,6,8}, {7,8,10} };
  static int f[3][3][n + 1] = { 1 };
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      for (int z, k = 0; k <= n; k++) {
        if (i > 0) f[i][j][k]  = f[i - 1][j][k];
        if (j > 0) f[i][j][k] += f[i][j - 1][k];
        if (i > 0 && j > 0) f[i][j][k] -= f[i - 1][j - 1][k];
        if ((z=k-a[i][j]) >= 0) f[i][j][k] = (f[i][j][k]%m+f[i][j][z])%m;
      }
  printf("%d\n", f[2][2][n]);
}
