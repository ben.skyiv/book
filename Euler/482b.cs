using System;
using System.Collections.Generic;

sealed class E482
{ 
  int Gcd(int a, int b) { return (b == 0) ? a : Gcd(b, a % b); }
  int Lcm(int a, int b) { return a / Gcd(a, b) * b; }
  void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }

  HashSet<Tuple<int,int,int,double>> Step1(int p)
  {
    var set = new HashSet<Tuple<int,int,int,double>>();
    for (int n9 = p/8, n = 1; n < n9; n++)
      for (int n2 = n*n, m9 = (int)Math.Sqrt(n9-n2), m = n + 1; m < m9; m++)
      {
        if ((m - n) % 2 == 0 || Gcd(m, n) > 1) continue;
        int m2 = m*m, a = 2*m*n, b = m2 - n2, c = m2 + n2;
        if (a > b) Swap(ref a, ref b);
        set.Add(Tuple.Create(a, b, c, Math.Round(Math.Atan2(a, b), 8)));
      }
    return set;
  }

  long Compute(int p)
  {
    var d = new Dictionary<double, Tuple<int,int,int>>();
    foreach (var i in Step1(p))
      //if (!d.ContainsKey(i.Item4))
        d.Add(i.Item4, Tuple.Create(i.Item1,i.Item2,i.Item3));
    long sum = 0;
    foreach (var i in d)
      foreach (var j in d)
      { 
        if (i.Key > j.Key) continue;
        var r = Math.Round(Math.PI / 2 - i.Key - j.Key, 8);
        Tuple<int,int,int> t, vi = i.Value, vj = j.Value;
        if (r < j.Key || !d.TryGetValue(r, out t)) continue;
        var R = Lcm(Lcm(vi.Item1, vj.Item1), t.Item1);
        var L = (vi.Item2*R/vi.Item1+vj.Item2*R/vj.Item1+t.Item2*R/t.Item1)*2; 
        if (L <= p) sum += (L+(vi.Item3*R/vi.Item1+vj.Item3*R/vj.Item1+
          t.Item3*R/t.Item1))*(1+p/L)*(p/L)/2;
      }
    return sum;
  }
  
  static void Main()
  {
    Console.WriteLine(new E482().Compute(1000));
  }
}

