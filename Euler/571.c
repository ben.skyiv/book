#include <stdio.h>

const int max = 12, limit = 10;

int next_perm(char s[])
{ // return: 1:success, 0:fail
  int i = max - 1; while (s[i] >= s[i+1]) i--;
  if (i == 0) return 0;
  int j = max; while (s[i] >= s[j]) j--;
  char t = s[i]; s[i] = s[j]; s[j] = t;
  for (int a = i+1, b = max; a < b; a++, b--)
    t = s[a], s[a] = s[b], s[b] = t;
  return 1;
}

long to_decimal(const char s[])
{
  long z = 0;
  for (int i = 1; i <= max; i++) z = s[i] + z * max;
  return z;
}

int is_pandigital(long z, int base)
{
  int a = 0;
  for (long t; z > 0; z = t) a |= 1 << (z - (t = z / base) * base);
  return ++a == 1 << base;
}

int is_super_pandigital(long z)
{
  for (int i = max - 1; i >= 5; i--) if (!is_pandigital(z, i)) return 0;
  return 1;
}

int main(void)
{
  static char a[max + 1]; a[1] = 1; long sum = 0;
  for (int i = 3; i <= max; i++) a[i] = i - 1;
  for (int n = 0; n < limit; ) {
    long z = to_decimal(a);
    if (is_super_pandigital(z)) n++, sum += z;
    if (!next_perm(a)) break;
  }
  printf("%ld\n", sum);
  return 0;
}

