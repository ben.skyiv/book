#include <iostream>

typedef struct { int ps[10]; char ks[10], n; } item;

const int m = 100'000'000, n = 10'001; // n > sqrt(m + n)
static int e[7+1][27]; // next_prime(7) == 11, 11^11 > m, 2^27 > m

void factor(int i0, item a[], const char c[])
{
  static int t[n];
  for (int i = 0; i < n; i++) t[i] = 1, a[i].n = 0;
  for (int p = 2; p * p < i0 + n; p++)
    if (!c[p])
      for (long q = p; q < i0 + n; q *= p)
        for (int i = q-1-(i0-1)%q; i < n; t[i] *= p, i += q)
          if (q != p) ++a[i].ks[a[i].n - 1];
          else a[i].ps[a[i].n] = p, a[i].ks[a[i].n++] = 1;
  for (int i = 0; i < n; i++) {
    if (i + i0 == t[i]) continue;
    a[i].ps[a[i].n] = (i + i0) / t[i];
    a[i].ks[a[i].n++] = 1;
  }
}

void set(int p)
{
  for (int z = p, i = 1, j = 1; z <= m; j++)
    for (int k = j * p; k % p == 0; k /= p)
      e[p][i++] = j, z *= p;
}

int s1(int p, int k) { return p * ((p > 7) ? k : e[p][k]); }

int s(int ps[], char ks[], int k)
{
  int z = 0;
  for (int i = 0; i < k; i++) z = std::max(s1(ps[i], ks[i]), z);
  return z;
}

int main()
{
  set(2); set(3); set(5); set(7);
  static item a[n];
  static char c[n];
  for (int i = 2; i * i < n; i++)
    if (!c[i]) for (int j = i * i; j < n; j += i) c[j] = 1;
  long z = 0;
  for (int i0 = -n, i = 2; i <= m; i++) {
    if (i >= i0 + n) factor(i0 = i, a, c);
    z += s(a[i-i0].ps, a[i-i0].ks, a[i-i0].n);
  }
  std::cout << z << std::endl;
}

