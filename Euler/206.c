#include <stdio.h>
#include <math.h>

int check(long n)
{
  static char s[21];
  sprintf(s, "%ld", n);
  for (int i = 0; i < 9; i++)
    if (s[i << 1] != i + '1')
      return 0;
  return 1;
}

long search()
{
  for (long i = (long)sqrt(1929394959697989990)/10*10; ; i -= 10)
    if (check(i * i)) return i;
  return 0;
}

int main(void)
{
  printf("%ld\n", search());
  return 0;
}
