#include <iostream>
#include <cmath>

const long N = 5e15, S = sqrt(N);
static int P[S];

long dfs(long d, long g, int k, long z)
{
  z += N/d*g;
  for (long n=N/d, p, q; p=P[k], (q=p*p) <= n; k++)
    for (long i=2,u=1,v,m=N/d/q; m; i++,u=v,q*=p,m/=p)
      z = dfs(d*q, g*((v=(i%p==0)?q:q/p)-u), k+1, z);
  return z;
}

int main()
{
  int k = 0;
  static char cs[S+1];
  for (long i = 2; i <= S; i++)
    if (!cs[i])
      for (long j = (P[k++] = i) * i; j <= S; j += i)
        cs[j] = 1;
  P[k] = S + 1;
  std::cout << dfs(1, 1, 0, 0) - 1 << std::endl;
}

