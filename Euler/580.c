// gcc -O4 -march=native -fopenmp 580.c -lm && time ./a.out
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static long n = (long)1e16, n2;
static char b[25000000]; // sqrt(1e16) / 4
static int a[8000000]; // 7,025,390

long gcd(long a, long b)
{ for (long t; b != 0; a = b, b = t) t = a % b; return a; }
long lcm(long a, long b) { return a / gcd(a, b) * b; }
long f(long d) { return (d > n2) ? 0 : (1 + ((n/d/d - 1) >> 2)); }

long c(int size, long m, int odd)
{
  if (m > n2) return 0;
  long z = 0;
  #pragma omp parallel for reduction(+:z)
  for (int i = size - 1; i >= 0; i--) {
    long d = lcm(m, a[i]);
    if (d > n2) continue;
    if (odd) z -= f(d);
    else     z += f(d);
    z += c(i, d, !odd);
  }
  return z;
}

long h(long n, int size) { return 1 + (n-1) / 4 - c(size, 1, 0); }

int init()
{
  int n3 = (int)(n2 >> 2), n4 = (int)sqrt(n2) >> 2, size = 0;
  for (int i = 1; i <= n4; i++)
    for (int k = (i<<2)+1, j = i*((i<<2)+2); j < n3; j += k)
      b[j] = 1;
  for (int i = 1; i < n3; i++)
    if (!b[i]) a[size++] = 1 + (i << 2);
  printf("Count: %d\n", size); fflush(stdout);
  return size;
}

int main(int argc, char* argv[])
{
  if (argc > 1) n = atol(argv[1]);
  n2 = (long)sqrt(n);
  printf("n = %ld\n", n); fflush(stdout);
  printf("H(n) = %ld\n", h(n, init())); fflush(stdout);
}
// 2,327,213,148,095,366  16 CPUs
// real:12984m(9day) user:105936m(73.6day)

