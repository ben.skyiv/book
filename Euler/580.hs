import Data.Set ( empty,insert )
main = let n = 10^7 in print $ (-) (div (n+3) 4) $
  length $ foldr (\x z -> insert x z) empty $ concat $ map
  (\i -> [i,i*5..n]) $ takeWhile (<=n) [i*i | i <- [5,9..]]

