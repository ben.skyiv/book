f n = length [i | i <- [n+1..n+n], mod (i*n) (i-n) == 0]
main = let n = 10^3 in print $ head [i | i <- [1..], f i >= n]

