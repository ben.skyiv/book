\\ $ gp -s 80123456 -q 553.gp
c(n,k,m=10^9+7)={
my(p=vector(n+1),eps=O('x^(n+1)),ex=exp(Mod(1,m)*'x+eps),q,f,r);
p[1]=Mod(2,m);for(i=1,n,p[i+1]=p[i]^2);
q=serconvol(Polrev(p,'x)+eps,ex)*exp(-'x*Mod(1,m)+eps);
f=q'/q;r=1;for(i=1,k,r=intformal(r*f));
lift(polcoeff(serlaplace(ex*r),n))}
print(c(10^4,10));quit()

