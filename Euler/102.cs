using System;
using System.Drawing;

sealed class E102
{
  float Dot(PointF x, PointF y) { return x.X * y.X + x.Y * y.Y; }
  
  bool InTriangle(PointF a, PointF b, PointF c)
  {
    float ab = Dot(a, b), ac = Dot(a, c), bc = Dot(b, c), cc = Dot(c, c);
    return (bc * ac - cc * ab < 0) ? false : (ab * bc - ac * Dot(b, b) >= 0);
  }
  
  int Compute()
  {
    var sum = 0;
    while (true)
    {
      var s = Console.ReadLine();
      if (s == null) break;
      var ss = s.Split(',');
      var a = new PointF(float.Parse(ss[0]), float.Parse(ss[1]));
      var b = new PointF(float.Parse(ss[2]), float.Parse(ss[3]));
      var c = new PointF(float.Parse(ss[4]), float.Parse(ss[5]));
      if (InTriangle(a, b, c)) sum++;
    }
    return sum;
  }

  static void Main()
  {
    Console.WriteLine(new E102().Compute());
  }
}
