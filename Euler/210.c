#include <stdio.h>
#include <stdlib.h>

int seek_max(long *pu, long *pv, long *pw, long a, long b, long c)
{ // (5, 5, 2) 一定不是钝角三角形
  if      (a > b && a > c) *pw = a, *pu = b, *pv = c;
  else if (b > c && b > a) *pw = b, *pu = c, *pv = a;
  else if (c > a && c > b) *pw = c, *pu = a, *pv = b;
  else return 0;
  return 1;
}

long compute(long t)
{
  long r = 4 * t, count = 0, b2 = 2 * t * t;
  for (long x = -r; x <= r; x++)
  {
    long xabs = (x >= 0) ? x : -x;
    long xt = x - t, x2 = x * x;
    for (long y = -r; y <= r; y++)
    {
      if (x == y) continue;
      long yabs = (y >= 0) ? y : -y;
      if (xabs + yabs > r) continue;
      long c2 = x2 + y * y;
      long yt = y - t;
      long a2 = xt * xt + yt * yt;
      long u2, v2, w2;
      if (!seek_max(&u2, &v2, &w2, a2, b2, c2)) continue;
      if (w2 <= u2 + v2) continue;
      count++;
    }
  }
  return count;
}

int main(int argc, char *argv[])
{
  int t = (argc > 1) ? atoi(argv[1]) : 2;
  printf("N(%d) = %ld\n", 4 * t, compute(t));
  return 0;
}
