#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
  const int r = 105, r2 = r * r;
  static int angles[2 * r2];
  angles[0] = r - 1;
  int z = r + r - 2;
  for (int x = 1; x < r; ++x)
    for(int x2 = x*x+1, y = 1, w = 1; x2 < r2; z+=2, y++, x2+=w+=2)
      angles[(int)(atan2(y, x) * r2 * 4 / M_PI)]++;
  long n = 0, sum = 0;
  for (int x = 0; x < r2 * 2; sum += angles[x++])
    if (angles[x] != 0)
      n += angles[x] * sum * (z - sum - angles[x]);
  printf("%ld\n", n * 4);
  return 0;
}

