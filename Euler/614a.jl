using Nemo
n = 10^7; m = 10^9+7
z = div(1 + floor(Int64, sqrt(24*n + 1)), 6)
R,x = PowerSeriesRing(ResidueRing(ZZ,m), n+1, "x")
ϕ(a) = sum([(-1)^i*x^div(a*i*(3*i-1),2) for i = -z:z])
G = ϕ(2)^2*ϕ(8)*inv(ϕ(1)*ϕ(4)^2)
@time println(sum([coeff(G,i) for i = 1:n]))
# 10^7: 130694090, 9m39.159s, 10^4: 942064925, 1.567s
