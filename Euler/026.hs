main = print $ snd $ maximum $ map (\x -> (length $ takeWhile ((/=0) . flip
  mod x) [10^i-1 | i <- [1..]], x)) [i | i <- [1,3..999], mod i 5 /= 0]

