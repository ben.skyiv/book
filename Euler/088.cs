using System;
using System.Linq;
using System.Collections.Generic;

sealed class E088
{
  int Min(int k)
  {
    int n = k + 1;
    while (!Equal(n, n, k)) n++;
    return n;
  }

  bool Equal(int prod, int sum, int k)
  {
    if (sum < k) return false;
    if (prod == 1) return sum == k;
    if (k == 1) return prod == sum;
    for (int i = 2; i <= prod && sum-i >= k-1; i++)
      if (prod%i == 0 && Equal(prod/i, sum-i, k-1))
        return true;
    return false;
  }
  
  int Solve(int n)
  {
    var set = new HashSet<int>();
    for (var k = 2; k <= n; k++) set.Add(Min(k));
    return set.Sum();
  }
  
  static void Main()
  {
    Console.WriteLine(new E088().Solve(12000));
  }
}

