using System;

sealed class E011
{
  int size = 20;

  int[,] Read()
  {
    var data = new int[size, size];
    for (var i = 0; i < size; i++)
    {
      var ss = Console.ReadLine().Split();
      for (var j = 0; j < size; j++)
        data[i, j] = int.Parse(ss[j]);
    }
    return data;
  }
  
  int ComputeH(int[,] d, int v)
  {
    for (var i = 0; i < size; i++)
      for (var j = 0; j < size - 3; j++)
        v = Math.Max(v, d[i,j]*d[i,j+1]*d[i,j+2]*d[i,j+3]);
    return v;
  }
  
  int ComputeV(int[,] d, int v)
  {
    for (var i = 0; i < size; i++)
      for (var j = 0; j < size - 3; j++)
        v = Math.Max(v, d[j,i]*d[j+1,i]*d[j+2,i]*d[j+3,i]);
    return v;
  }
  
  int ComputeD1(int[,] d, int v)
  {
    for (var i = 0; i < size - 3; i++)
      for (var j = 0; j < size - 3; j++)
        v = Math.Max(v, d[i,j]*d[i+1,j+1]*d[i+2,j+2]*d[i+3,j+3]);
    return v;
  }
  
  int ComputeD2(int[,] d, int v)
  {
    for (var i = 0; i < size-3; i++)
      for (var j = 3; j < size; j++)
        v = Math.Max(v, d[i,j]*d[i+1,j-1]*d[i+2,j-2]*d[i+3,j-3]);
    return v;
  }
  
  int Compute()
  {
    var data = Read();
    var v = int.MinValue;
    v = ComputeH(data, v);
    v = ComputeV(data, v);
    v = ComputeD1(data, v);
    v = ComputeD2(data, v);
    return v;
  }
  
  static void Main()
  {
    Console.WriteLine(new E011().Compute());
  }
}
