import Text.Printf ( printf )

f a b c n = let d = a + b + c + 2 * sqrt (a*b + b*c + c*a) in
  1/(d*d) + if n <= 1 then 0 else f a b d (n-1) + f a d c (n-1) + f d b c (n-1)

main = let n = 10 in printf "%.8f\n" $ let a = -1; b = 1 + 2 / (sqrt 3)
  in 1 - (3/(b*b) + f b b b n + 3 * f b b a n) :: Double

