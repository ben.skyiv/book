#include <iostream>
#include <vector>
#include <map>

using namespace std;

template<class T> T gcd(T x, T y) { return x ? gcd(y%x,x) : y; }

int main()
{
  const long N = 10000000;
  map<long,vector<pair<long,long>>> m;
  for (long a = 1; a*a < N; a++)
    for (long b = 1; b < a; b++)
      m[a*a-b*b].push_back(make_pair(a,b));
  long z = 0;
  for (auto e : m) {
    auto v = e.second;
    for (auto i = 0; i < v.size(); i++)
      for ( auto j = i; j < v.size(); j++) {
        auto a = v[i].first, d = v[j].first;
        auto aa = v[i].second, dd = v[j].second;
        auto r0 = a+d, r1 = aa+dd, g = gcd(r0,r1);
        r0 /= g, r1 /= g;
        if (e.first % (r0*r0-r1*r1)) continue;
        auto bc = e.first/(r0*r0-r1*r1)*r1*r1;
        if (2*bc+a*a+d*d >= N) continue;
        bc /= r1*r1;
        for (long k = 1; k*k <= bc; k++)
          if (bc%k==0)
            z += (i==j?1:2) * (k*k==bc?1:2);
      }
  }
  cout << z << endl;
  return 0;
}

