import Math.NumberTheory.Primes.Sieve ( sieveFrom )
import Math.NumberTheory.Primes.Testing ( isPrime )

s n = let
  m = div ((n-1)*n) 2; f p = let
    b1 = g (-1) 2 1 1; b2 = g 1 0 (-1) 1
    b3 = g (-2) 4 2 2; b4 = g 2 0 (-2) 2
    b5 = g (-2) 2 2 0; b6 = g 2 2 (-2) 4
    b7 = g (-1) 0 1 (-1)
    g a b c d = isPrime $ p + (if even n then a*n+b else c*n+d)
    in b1 && (isPrime (p+2) || b3) || b2 && (b1 || b4 || b6) ||
       b7 && (isPrime (p-2) || b1 || b2 || b5)
  in sum $ filter f $ takeWhile (<=m+n) $ sieveFrom $ m+1

main = print $ s 5678027 + s 7208785

