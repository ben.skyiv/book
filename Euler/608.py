def fact_valuation(n, p):
  n //= p; m = n
  while n >= p: n //= p; m += n
  return m

def sigma0_sum(n):
  v = int(n ** 0.5); ret = 0
  for i in range(v, 0, -1): ret += n // i
  return ret * 2 - v * v

def prime_sieve(n):
  isp = [1] * (n + 1)
  for i in range(2, n + 1):
    if isp[i]:
      for j in range(i*i, n+1, i): isp[j] = 0
  return [i for i in range(2, n + 1) if isp[i]]

def rec(rest, pi, coef):
  ret = coef * (sigma0_sum(rest) % mod) % mod
  for pj in range(pi, len(primes)):
    p = primes[pj]
    if p > rest: break
    ncoef = -coef * if1[pj] % mod * f2[pj] % mod
    ret += rec(rest // p, pj + 1, ncoef)
  return ret % mod

M, N, mod = 200, 10**12, 10**9 + 7
primes = prime_sieve(M)
vals = [fact_valuation(M, p) for p in primes]
f1 = [(v + 1) * (v + 2) // 2 % mod for v in vals]
f2 = [(v + 0) * (v + 1) // 2 % mod for v in vals]
if1 = [pow(v, mod - 2, mod) for v in f1]

coef = 1
for v in f1: coef = coef * v % mod 
print(rec(N, 0, coef)) # from Min_25

