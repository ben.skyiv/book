import Control.Arrow ( (***) )
import Data.List ( sort,subsequences )
import Math.NumberTheory.Primes ( primes )

n = 12
ps = takeWhile (<n) primes
g = sort . map product . subsequences
q = floor $ sqrt $ fromIntegral $ product ps
f z (x:xs) (y:ys) = if x*y > q
  then f z xs (y:ys)
  else f (max (x*y) z) (x:xs) ys
f z _ _ = z
x = splitAt (div (length ps) 2) ps
y = (reverse . g *** g) x
z = uncurry (f 0) y
main = print z

