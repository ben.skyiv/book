using System;
using System.Threading;
using System.Threading.Tasks;

sealed class E407
{
  int M(int n)
  {
    int a = n - 1;
    while ((long)a*a % n != a) a--;
    return a;
  }

  long Compute(int n)
  {
    long z = 0;
    Parallel.For(1, n+1, () => 0L, (i,_,x) => x+M(i),
      x => Interlocked.Add(ref z, x));
    return z;
  }
  
  static void Main()
  {
    Console.WriteLine(new E407().Compute(10000000));
  }
}

