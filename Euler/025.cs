using System;
using System.Numerics;

sealed class E002
{
  static void Main()
  {
    int n = 2;
    for (BigInteger c,m=BigInteger.Pow(10,999),a=1,b=1; b < m; c=a+b,a=b,b=c) n++;
    Console.WriteLine(n);
  }
}

