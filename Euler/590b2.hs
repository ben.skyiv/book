import Data.List ( (\\),group )
import Math.NumberTheory.Primes ( primes )
import Math.NumberTheory.Powers ( powerMod )
m = 9; m1 = 10^m; m2 = 4*5^(m-1)
p2 n = powerMod 2 (if n < m then n else mod (n-m) m2 + m) m1
bs :: Integer -> [Integer]; f1 :: Integer -> [Integer]
bs n = map (flip mod m1) $ scanl (\a i -> div (a*(n-i+1)) i) 1 [1..n]
f1 n = map (\i -> floor $ logBase (fromIntegral i)
  (fromIntegral n)) $ takeWhile (<=n) primes
f2 = map (\xs -> (head xs, length xs)) . group
f3 = map (\(a,b) -> zip [replicate i a | i <- [0..b]] $ bs $ fromIntegral b)
f4 xs = zip
  (foldl (\a b -> [i++j         | i <- a, j <- b]) [[]] $ map (map fst) xs)
  (foldl (\a b -> [mod (i*j) m1 | i <- a, j <- b]) [1]  $ map (map snd) xs)
hl n = let zs = f1 n; xss = f4 $ f3 $ f2 zs; u = length zs in
  let ws = takeWhile (>1) zs in foldl (\z (xs,i) -> mod (z +
  let ys = takeWhile (>1) xs; w = length ys; v = length xs in
  (if even (u - v) then 1 else -1) * i * p2
  (2^(v-w) * product (map succ ys) * product (ws \\ xs))) m1) 0 xss
main = print $ hl 50000

