// clang -O 513.c lib/exactSquareRoot.c -lm && time ./a.out
#include <stdio.h>

extern int exactSquareRoot(long x);

int main(void)
{
  int n = 50, n2 = n >> 1, cnt = 0;
  for (int c2 = 1; c2 <= n2; c2++)
    for (int c = c2 << 1, b = 1; b <= c; b++)
    {
      int a = c - b + 1;
      if (((b - a) & 1) != 0) a++;
      for (int m; a <= b; a += 2)
      { // d*2 == 4m^2 = 2a^2 + 2b^2 - c^2
        long d = (long)a*a + (long)b*b - ((long)c2*c2 << 1);
        if ((m = exactSquareRoot(d >> 1)) < 0) continue;
//printf("(%3d,%3d,%3d)%3d\n", a, b, c, m);
        cnt++;
      }
    }
  printf("F(%d) = %d\n", n, cnt);
  return 0;
}

