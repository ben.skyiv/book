#include <stdio.h>

const long n = (long)1e10;
const int n1 =  (int)1e5;
const int n2 = 2 + n1; // n2 > sqrt(n + n2)

void factor(long i0, long a[], const char c[])
{ // a[]: largest prime factor
  static long t[n2];
  for (int i = 0; i < n2; i++) t[i] = 1;
  for (int p = 2; (long)p * p < i0 + n2; p++)
    if (!c[p])
      for (long q = p; q < i0 + n2; q *= p)
        for (long i = q-1-(i0-1)%q; i < n2; t[i] *= p, i += q)
          if (q == p) a[i] = p;
  for (int i = 0; i < n2; i++)
    if (i + i0 != t[i]) a[i] = (i + i0) / t[i];
}

int main(void)
{ // runtime: 4m54s
  static long a[n2]; static char c[n2];
  for (int i = 2; i * i < n2; i++)
    if (!c[i]) for (int j = i * i; j < n2; j += i) c[j] = 1;
  long z = 1;
  for (long p, i0 = -n2, i = 2; i <= n; i++) {
    if (i >= i0 + n2) factor(i0 = i, a, c);
    if ((p = a[i - i0]) >= n1) continue;
    if (p * p >= i) continue;
    z++;
  }
  printf("%ld: %ld\n", n, z);
}

