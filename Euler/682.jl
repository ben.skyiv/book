using Nemo
m = 10^9+7; n = 10^7
R,x = PowerSeriesRing(ResidueRing(ZZ,m), n+1, "x")
A = inv(prod(1-x^k for k in [5,7,8]))
B = inv(prod(1-x^k for k in [1,4,6,7,10]))
@time @show coeff(A, n) + coeff(B, n-4)
