// mcs 498.cs ../lib/ModPow.cs -r:System.Numerics.dll
using System;
using System.Numerics;
using Skyiv.Utils;

sealed class E498
{
  static void Main()
  {
    long n = 100, m = 10, d = 4;
    //long n = 10000000000000, m = 630005, d = 10000;
    long i, c = 999999937, v = 1, w = n-d+1, z = -v;
    for (i = 1; i < m-d; z = (z + (((++i & 1) == 0) ? v : -v)) % c)
    {
      v = (long)((BigInteger)v * --w * i.ModInv(c) % c);
    }
    for (i = n-d+1; i <= n; i++) z = (long)((BigInteger)z * i % c);
    for (i = w = 1; i <= d; i++) w = w * i % c;
    z = z * w.ModInv(c) % c;
    Console.WriteLine(z);
  }
}

