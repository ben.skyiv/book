import Text.Printf ( printf )

main = let n = 16 in printf "%X\n" (sum [15*16^x - 43*15^x + 41*14^x - 13^(x+1) | x <- [2..n-1]] :: Int)

