import Data.List ( foldl' )
import Data.Array ( (!),accumArray,listArray )
import Math.NumberTheory.Moduli ( powerMod )

main = let n = 250250; z = 250; m = 10^16; ix = (0,z-1); xs = [0..z-1]
  in print $ pred $ (!0) $ foldl' (\b (i,n) -> foldl' (\b _ ->
  listArray ix $ [mod (b ! k + b ! (mod (k-i) z)) m | k <- xs])
  b [1..n]) (listArray ix $ 1 : repeat 0) [(i, (accumArray (+)
  0 ix [(powerMod i i z, 1) | i <- [1..n]]) ! i) | i <- xs]

