factors n = if even n then 2 : factors (div n 2) else fact n 3
fact 1 _ = []
fact n k = let p = head [ x | x <- [k,k+2..], mod n x == 0 ] in p : fact (div n p) p
main = print $ {-last $-} factors 600851475144

