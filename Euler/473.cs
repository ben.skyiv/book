sealed class E473
{
  static void Main(string[] args)
  {
    long count = 17, n = (args.Length > 0) ? long.Parse(args[0]) : 10000000000;
    var fib = new System.Collections.Generic.List<long>(); fib.Add(0); fib.Add(1);
    var h = new long[9919]; h[0] = 0; h[1] = 2; h[2] = 14;
    for (long i=0,j=1,k=i+j; k < n; i=j,j=k,k=i+j) fib.Add(k);
    for (int i=3,j=9,a=3,b=3,c=2,d=2; j < fib.Count; d=c,c=b,b=a,i+=2,j+=2)
      for (var k = 0; k < d; a++, k++) count += h[a] = h[k] + fib[i] + fib[j];
    System.Console.WriteLine(count);
  }
}
