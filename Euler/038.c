#include <stdio.h>
#include <string.h>

static char buf[10];

int set(int n)
{
  for (; n > 0; n /= 10)
  {
    int r = n % 10;
    if (buf[r]) return 0;
    buf[r] = 1;
  }
  return 1;
}

int compute(int n)
{
  memset(buf, 0, sizeof(buf));
  buf[0] = 1;
  if (!set(n)) return 0;
  return set(n * 2);
}

int main(void)
{
  for (int i = 9123; i <= 9876; i++)
    if (compute(i))
      printf("%d%d\n", i, i * 2);
  return 0;
}
