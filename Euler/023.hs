import Data.Array ( (!),listArray )
import Math.NumberTheory.Primes.Factorisation ( divisorSum )

n = 28123
a = listArray (1,n) $ map ((\x -> divisorSum x - x > x)) [1..n]
main = print . sum . filter (not . (any (a!)) . (\x -> map (x-)
  $ takeWhile (<= div x  2) $ filter (a !) [1..n])) $ [1..n]

