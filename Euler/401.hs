main = let n = 10^15; m = 10^9 in print $ let z = floor $ sqrt $ fromIntegral n
  in foldl (\s x -> let i = fst x; k = snd x in mod (s + i*i*(k-z) +
  div (k*(k+1)*(2*k+1)) 6) m) 0 [(i,k) | i <- [1..z], let k = div n i]

{-
(m, n) = (10^9, 6)
f n = div ((n*n+n)*(2*n+1)) 6
g z j0 i = let j = div n i in if j < i then [z, j, i]
  else g (mod (z + f j + (j0-j)*f(i-1)) m) j (i+1)
main = print $ flip mod m $ let xs = g 0 n 1 in head xs
  + if xs!!1 == last xs - 2 then 0 else f (xs!!1)
-}

{-
m = 10^9
f n = div ((n*n+n)*(2*n+1)) 6
g xs = let zs = map snd xs in zip [1..] $ map snd $ map (\(a,b) -> (a,a-b)) $ zip zs $ tail zs
f1 xs = map (flip mod m . f) $ map snd xs
f2 xs = map (\(a,b) -> mod (b * f a) m) xs
f3 xs ys = let a = fst $ last ys; b = snd (last xs) - 1 in if a == b then 0 else f b

main = let n = 10^13{- 10^15 -} in print $
  let xs = takeWhile (\(i,z) -> z >= i)  [(i, div n i) | i <- [1..]]; ys = g xs in
  mod (sum (f1 xs) + (sum $ f2 ys) + f3 xs ys) m
-}
-- main = let n = 10^4{- 10^15 -}; m = 10^9 in print $ flip mod m $ sum
--   [(\n -> mod (div ((n*n+n)*(2*n+1)) 6) m) (div n i) | i <- [1..n]]

