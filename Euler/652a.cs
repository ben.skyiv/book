using System;
using System.Drawing;
using System.Collections.Generic;

static class E652a
{
  static void Main(string[] args)
  {
    int n = (args.Length > 0) ? int.Parse(args[0]) : 100;
    var dict = new Dictionary<double, List<Point>>();
    for (var a = 2; a <= n; a++)
      for (var b = 2; b <= n; b++) {
        var key = Math.Log(b, a);
        List<Point> value;
        if (!dict.TryGetValue(key, out value))
          dict.Add(key, value = new List<Point>());
        value.Add(new Point(a, b));
      }
    Console.WriteLine("D({0}) = {1}", n, dict.Count);
    foreach (var kvp in dict) {
      if (kvp.Value.Count <= 1) continue;
      if (kvp.Value[0].X > kvp.Value[0].Y) continue;
      foreach (var i in kvp.Value)
        Console.Write("{0}:{1} ", i.X, i.Y);
      Console.WriteLine("== " + kvp.Value.Count);
    }
  }
}

