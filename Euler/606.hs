import Math.NumberTheory.Primes ( primes )
main = let n = 10^2 in print $ [
  last $ takeWhile (<= min (q-1) (div n q)) primes
  | q <- tail $ takeWhile (<= div n 2) primes]

