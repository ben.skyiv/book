#include <stdio.h>

int main()
{
  long z = 0; int m = 1008691207, n = (int)1e8;
  for (int a = 1, i = 1; i < n; z = (z + a) % m)
    a = (long)a * i++ % m;
  printf("%ld\n", (n - 1 + (n - 3) * z) % m);
}

