using System;

sealed class E064
{
  static void Main()
  {
    int n = 10000, z = 0;
    for (int i = 2; i <= n; i++) {
      int k = (int)Math.Sqrt(i);
      if (k*k == i) continue;
      int period = 0, d = 1, m = 0;
      for (int a = k; a != 2*k; period++) {                
        m = d*a - m; a = (k + m) / (d = (i - m*m) / d);
      }
      if (period % 2 == 1) z++;
    }
    Console.WriteLine(z);
  }
}

