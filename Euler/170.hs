import Data.List ( (\\), delete, elemIndices, permutations )
import Data.Digits ( digits, unDigits )
main = print $ maximum [ b | z <- [7,6..4], a <- permutations $ delete z [0..7],
  let b = unDigits 10 $ [9,8]++(z:a), c <- [12,15..48], let (d,m) = divMod b c, m == 0,
  let xs = digits 10 d, (digits 10 c ++ xs) \\ [0..9] == [0], let k = elemIndices 0 xs,
  last xs /= 0, or [ n /= 0 && n < 100 | a <- k, let n = c * xs!!(a+1) ] ]
{-
1) The first integer must be a multiple of 3 
(otherwise the digital root of the result is not 9).
2) The first integer contains at most 2 digits 
(otherwise the result contains more than 10 digits).
3) The first integer must be less than 49 
(otherwise the result contains more than 10 digits). 
4) maybe answer is 98xxxx
5) This number must be a multiple of the first factor (f).
In the numbers f and cp/f all digits 1..9 have to occour 
once and at least one zeros. 
-}
