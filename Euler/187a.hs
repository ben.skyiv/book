import Math.NumberTheory.Primes.Factorisation ( factorise )
main = let n = 10^8 in print $ length $ filter (==2) $ map (sum . map snd . factorise) [1..n-1]

