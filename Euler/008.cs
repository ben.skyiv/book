using System;
using System.IO;

sealed class E008
{
  static long Product(string s, int idx, int n)
  {
    long product = 1;
    for (var i = 0; i < n; i++) product *= (long)(s[idx + i] - '0');
    return product;
  }

  static void Main()
  {
    var s = Console.In.ReadToEnd().Replace("\n", null).Replace(" ", null);
    var max = long.MinValue;
    for (int n = 13, i = s.Length - n; i >= 0; i--)
      max = Math.Max(max, Product(s, i, n));
    Console.WriteLine(max);
  }
}
