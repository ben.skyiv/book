import Data.Map ( (!),assocs,insertWith,keys,singleton )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Testing ( isPrime )

main = let n = 50; m = 10^16 in print $ flip mod m $ sum $ map
  snd $ filter (isPrime . fst) $ assocs $ foldl (\xs p -> foldl
  (\xs q -> insertWith (+) (p+q) (xs!q) xs) xs $ reverse $ keys
  xs) (singleton 0 1) $ takeWhile (<n) primes

