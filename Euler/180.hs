import Data.List ( nub )
import Data.Ratio ( (%),denominator,numerator )

main = let
  n = 35
  rSqrt x = let
    a = floor $ sqrt $ fromIntegral $ numerator x
    b = floor $ sqrt $ fromIntegral $ denominator x
    c = (a%b) * (a%b) in if x == c then (a%b) else 0
  inv x = if x == 0 then 0 else denominator x % numerator x
  s = nub [(a%b) | b <- [1..n], a <- [1..b-1]]
  v = sum $ nub $ map sum $ concat [
    [[x,y,z] | x <- s, y <- s, let z = x+y, elem z s],
    [[x,y,z] | x <- s, y <- s, let z = x*y*inv(x+y), elem z s],
    [[x,y,z] | x <- s, y <- s, let z = rSqrt(x*x+y*y), elem z s],
    [[x,y,z] | x <- s, y <- s, let z = x*y*inv(rSqrt(x*x+y*y)), elem z s]]
  in print $ numerator v + denominator v

