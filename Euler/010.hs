{-
isPrime n = null [ x | x <- [3,5..ceiling $ sqrt $ fromIntegral n], mod n x == 0 ]
main = print $ 2 + (sum $ takeWhile (<2*10^6) $ filter isPrime [3,5..])
-}
{-
small p (x:xs) = if p x then [] else x : small p xs
primes = 2:3:5:[ n | n <- [7,9..], all (\m -> 0 /= mod n m) $
  small (>ceiling(sqrt $ fromIntegral n)) primes ]
main = print $ sum $ takeWhile (<2*10^6) primes
-}
import Math.NumberTheory.Primes.Sieve
main = print $ sum $ takeWhile (<2*10^6) primes
-- 0m14.426s 0m6.171s 0m0.146s

