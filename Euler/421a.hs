import Math.NumberTheory.Primes.Factorisation ( factorise )

f n = let n1 = n+1; n2 = n*n; n3 = n2*n; n4 = n2*n2; n5 = n2-n+1
  in map factorise [n1, n5, n5+n4-n3, n1*(n3*n4+1)-n3*(n1+n2)]
main = let n = 30 in print $ [f i | i <- [1..n]]

