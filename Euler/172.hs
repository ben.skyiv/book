main = let n = 18 in print $ flip div 10 $ 9 * f 10 n where
  choose n k = div (product [n -i | i <- [0..k-1]]) $ product [1..k]
  f d p = let g = f (d-1) in if d == 0 then 0 else if p < 4 then d^p
    else g p +  p * g (p-1) + choose p 2 * g (p-2) + choose p 3 * g (p-3)

