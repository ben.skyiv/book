w=(d,n)->if(n<d||d<1,0,sum(k=0,d-1,(-1)^k*binomial(d,k)*(d-k)^n))
w0=(d,n)->if(n<d+1||d<1,0,w(d+1,n)-w(d+1,n-1)-w(d,n-1))
F=(m)->(10^m-1)*(10^m-2)/2-sum(d1=1,8,sum(d2=1,9-d1,9!/d1!/d2!/\
(9-d1-d2)!*sum(k=d1,m,sum(l=d2,m,w(d1,k)*(w(d2,l)/2+w0(d2,l))))))
print(F(18)%1000267129);quit()

