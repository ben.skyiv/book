using System;
using System.Collections.Generic;

sealed class E035
{
  bool[] composite;

  bool[] Sieve(int max)
  {
    int limit = (int)Math.Sqrt(max);
    var composite = new bool[max + 1];
    for (int i = 2; i <= limit; i++)
      if (!composite[i])
        for (int j = i * i; j <= max; j += i)
          composite[j] = true;
    return composite;
  }
  
  int Rotation(int n)
  {
    if (n % 10 == n) return n;
    var s = n.ToString();
    return int.Parse(s.Substring(1) + s[0]);
  }
  
  bool IsCircular(int prime)
  {
    if (prime.ToString().IndexOf('0') >= 0) return false;
    for (var n = prime; ; )
    {
      n = Rotation(n);
      if (n == prime) break;
      if (composite[n]) return false;
    }
    return true;
  }

  int Compute(int max)
  {
    var count = 0;
    composite = Sieve(max);
    for (var i = 2; i < composite.Length; i++)
    {
      if (composite[i]) continue;
      if (!IsCircular(i)) continue;
      Console.Write(i + " ");
      count++;
    }
    return count;
  }

  static void Main()
  {
    Console.WriteLine("[{0}]", new E035().Compute(1000000));
  }
}
