import Data.Map ( (!),fromList )
import Data.Complex ( Complex((:+)),conjugate )
import Math.NumberTheory.Primes.Sieve ( primes )

n = 150; m = length p
p = filter ((==1) . flip mod 4) $ takeWhile (<n) primes
a = fromList [(i*i + j*j, fromIntegral i :+ fromIntegral j) |
  i <- [1, 3 .. floor $ sqrt $ fromIntegral n],
  j <- [2, 4 .. floor $ sqrt $ fromIntegral $ n - i*i]]
f n x@(r:+i) = if n == m then floor $ min (abs r) $ abs i else let
  z = a!(p!!n) in sum $ map (f $ n+1) [x, x*z, x * conjugate z]
main = print $ sum [f i $ (a!) $ (p!!) $ i-1 | i <- [1..m]]

