using System;

sealed class E094
{
  static bool isSquare(long n)
  {
	  var a = (long)Math.Sqrt(n);
	  return a*a == n;
  }

	static void Main()
	{
		long max=1000000000, sum=0;
		for (long i=3; i<max/3; i+=2)
		{
		  if (isSquare(i*i-(i-1)*(i-1)/4)) sum+=3*i-1;
			if (isSquare(i*i-(i+1)*(i+1)/4)) sum+=3*i+1;
		}
	  Console.WriteLine(sum);
  }
}

