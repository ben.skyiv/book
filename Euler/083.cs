using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

sealed class E083
{
  static readonly int size = 80;

  int Compute()
  {
    int min;
    var grid = Read(out min);
    int[,] g = new int[size,size], h = new int[size,size], s = new int[size,size];
    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++)
      {
        h[i,j] = min * (2*size - 1 - i - j);
        g[i,j] = int.MaxValue;
      }
    g[0,0] = grid[0,0];
    var open = new SortedList<Tuple<int,int>, Tuple<int,int>>();
    open.Add(new Tuple<int,int>(g[0,0] + h[0,0], 0), new Tuple<int,int>(0, 0));
    for (int i1, j1; s[size-1, size-1] < 2; )
    {
      var current = open.ElementAt(0).Value;
      open.RemoveAt(0);
      s[i1 = current.Item1, j1 = current.Item2] = 2;
      for (int z, i2 = 0, j2 = 0, k = 0; k < 4; k++, i2 = j2 = 0)
      {
        switch (k)
        {
          case 0: i2 = i1-1; j2 = j1; break;
          case 1: i2 = i1+1; j2 = j1; break;
          case 2: i2 = i1; j2 = j1+1; break;
          case 3: i2 = i1; j2 = j1-1; break;
        }
        if (i2 < 0 || i2 >= size || j2 < 0 || j2 >= size || s[i2,j2] >= 2
          || g[i2,j2] <= g[i1,j1] + grid[i2,j2]) continue;
        g[i2,j2] = g[i1,j1] + grid[i2,j2];
        if (s[i2,j2] == 1) open.RemoveAt(open.IndexOfValue(new Tuple<int,int>(i2,j2)));
        for (z = 0; open.ContainsKey(new Tuple<int,int>(g[i2,j2] + h[i2,j2], z)); ) z++;
        open.Add(new Tuple<int,int>(g[i2,j2] + h[i2,j2], z), new Tuple<int, int>(i2,j2));
        s[i2,j2] = 1;
      }
    }
    return g[size-1, size-1];
  }

  int[,] Read(out int min)
  {
    min = int.MaxValue;
    int i = 0;
    var grid = new int[size, size];
    foreach (var line in File.ReadLines("081.txt"))
    {
      var fields = line.Split(',');
      for (var j = 0; j < fields.Length; j++)
      {
        grid[i, j] = int.Parse(fields[j]);
        if (min > grid[i, j]) min = grid[i, j];
      }
      i++;
    }
    return grid;
  }

  static void Main()
  {
    Console.WriteLine(new E083().Compute());
  }
}

