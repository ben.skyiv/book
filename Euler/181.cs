// https://projecteuler.net/thread=181&page=3#130688
public static class E181
{
  const int B = 60, W = 40;
  static void Main()
  {
    var res = new long[B+1, W+1];
    res[0, 0] = 1;
    for (int bm = 0; bm <= B; bm++)
      for (int wm = (bm == 0 ? 1 : 0); wm <= W; wm++)
        for (int b = bm; b <= B; b++)
          for (int w = wm; w <= W; w++)
            res[b, w] += res[b - bm, w - wm];
    System.Console.WriteLine(res[B, W]);
  }
}

