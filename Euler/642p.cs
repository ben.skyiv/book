using System;
using System.IO;

static class E642p
{
  static readonly int CPUs = 6;
  static readonly int m = (int)1e9;

  static void Main()
  {
    Directory.SetCurrentDirectory("/home/ben/tmp");
    long z = 0;
    for (var i = 1; i <= CPUs; i++)
      z = (z + long.Parse(File.ReadAllText("642-" +
        i.ToString() + ".txt").Split()[1])) % m;
    Console.WriteLine(z);
  }
}

