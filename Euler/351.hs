import Math.NumberTheory.Primes.Factorisation ( φ )

main = let n = 10^8 in print $ 6 * sum [x - φ x | x <- [1..n]]

