using System;
using System.Numerics;

static class E678
{
  static readonly long n = (long)1e7;

  static long L(double a) { return (long)(a + 0.5); }

  static bool Valid(long a, long c, int e, int f, long cf)
  {
    long ae = (long)BigInteger.Pow(a, e), be = cf - ae;
    if (be <= ae) return false;
    var bb = Math.Exp(Math.Log(be) / e);
    var b = L(bb);
    if (Math.Abs(bb - b) > 1e-6) return false;
    if ((long)BigInteger.Pow(b, e) != be) return false;
    Console.WriteLine("{0,4} {1,4} {2,4} {3,2} {4,2}", a, b, c, e, f);
    return true;
  }

  static void Main()
  {
    double nn = Math.Log(n), n2 = Math.Log(2);
    var z = 0;
    for (int f = 3; f <= (int)L(nn / n2); f++)
      for (long c = 2; c <= L(Math.Exp(nn / f)); c++) {
        var fnc2 = f * Math.Log(c) - n2;
        var cf = (long)BigInteger.Pow(c, f);
        if (cf > n) continue;
        for (int e = 2; e <= (int)L(fnc2 / n2); e++)
          if (e != f)
            for (long a = 1; a <= L(Math.Exp(fnc2 / e)); a++)
              if (Valid(a, c, e, f, cf)) z++;
      }
    Console.WriteLine("F({0}) = {1}", n, z);
  }
}

