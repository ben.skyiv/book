main = print $ until (\n -> let x = n / (n + sqrt(2*n) + 1)
  in (f 1 - f x + x*x/n/2) / (1 - pi/4) < 0.001) succ 1
  where f x = x - ((x-1) * sqrt(x*(2-x)) + asin(x-1)) / 2

