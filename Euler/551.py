from memoize import *
@memoize
def next2(r, ri, off):
    k = 0; rs = 10 ** (ri + 1)
    while r < rs: k, r = next1(k, r, ri - 1, off)
    return k, r
def next1(k, v, ri, off = 0):
    if ri < 0: return k + 1, v + ds(v) + off
    rs = 10 ** (ri + 1)
    c, r = v // rs, v % rs
    kk, r = next2(r, ri, off + ds(c))
    return k + kk, c * rs + r
def ds(n): return sum(int(x) for x in str(n))
n = 15; ri = n - 1; k, v = 1, 1
while ri >= 0:
    ri -= 1
    while 1:
        nk, nv = next1(k, v, ri)
        if nk <= 10**n: k, v = nk, nv
        else: break
print(v)

