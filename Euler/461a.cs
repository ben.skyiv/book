using System;

sealed class E461
{
  static void Main()
  {
    int n = 10000, a9 = (int)(Math.Log(Math.PI/4+1) * n);
    double n1 = n, eps = 1e-10;
    Console.WriteLine("{0} {1}", n, a9);
    for (var a = 1433/*0*/; a <= a9; a++)
    {
      var a1 = Math.Exp(a/n1);
      var b9 = (int)(Math.Log((Math.PI+4-a1)/3) * n);
      for (var b = a; b <= b9; b++)
      {
        var b1 = Math.PI + 4 - a1 - Math.Exp(b/n1);
        for (var c = b; ; c++)
        {
          var v = b1 - Math.Exp(c/n1);
          if (v <= 0) break;
          var u = n * Math.Log(v);
          var d = (int)(u + 0.5);
          if (d < c) break;
          var e = u - d;
          if (Math.Abs(e) > eps) continue;
          Console.WriteLine("{0,4} {1,4} {2,4} {3,5} {4,22}", a, b, c, d, e);
        }
      }
    }
  }
}

