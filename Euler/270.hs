f x y = if x == -y then 1 else if any (==0) [x,y] then
  0 else div (product [x..x+y-2]) $ product [2..y-1]
g x y = if any (==0) [x,y] then 1 else x+y
main = let n = 30; m = 10^8 in print $ flip mod m $ sum
  [f x y * sum [f a b * f (n+n - g x y) (n+n - g a b) |
  a <- [0..n], b <- [0..n]] | x <- [0..n], y <- [0..n]]

