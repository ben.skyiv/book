import Data.Array ( accumArray,elems )
 
main = let n = 15*10^5 in print $ length $ filter (==1) $ elems $
  accumArray (+) 0 (1,n) [(i,1) | i <- concatMap (\x -> [x,x+x..n])
  [2*a*(a+b) | a <- [2 .. floor $ sqrt $ fromIntegral n],
  b <- [1..a-1], gcd a b == 1, odd (a+b)]]

