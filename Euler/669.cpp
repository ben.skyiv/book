#include <iostream>

int main(void)
{
  static int a[] = {
    1,-0,1,-1, 2,-1,2,-2, 3,-3, 4,-3,4,-4, 5,-4,5,-5, 6,-6, 7,-6,7,-7, 8,-8,
    9,-8,9,-9, 10,-9,10,-10, 11,-11, 12,-11,12,-12, 13,-12,13,-13, 14,-14,
    15,-14,15,-15, 16,-16,
    17,-16,17,-17, 18,-17,18,-18, 19,-19, 20,-19,20,-20, 21,-21 };
  const int n = 83; // fib(83) = 99194853094755497
  long r = 1, s = 1, t;
  int i = 2; for (; i < n; i++) t = r + s, r = s, s = t;
  long m = (long)1e16;
  long k = s - m;
  long p = (k + 1) / 2; if (k % 2 == 0) p = -p;
  long q = a[k % 68], q2 = k / 68 * 21;
  if (q > 0) q += q2; else q -= q2;
  std::cout << "k=" << k << std::endl;
  std::cout << "r=" << r << std::endl;
  std::cout << "s=" << s << std::endl;
  std::cout << "p=" << p << std::endl;
  std::cout << "q=" << q << std::endl;
  //__int128 z = (__int128)p * r + (__int128)q * s;
  //std::cout << "z=" << z << std::endl;
}

