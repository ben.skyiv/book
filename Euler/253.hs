import Data.Map ( (!), empty, insert, member )

f t n m k
  | n == 0           = (t, if m == 0 then 1 else 0)
  | n > m || n > k   = (t, 0)
  | member (n,m,k) t = (t, t ! (n,m,k))
  | otherwise        = let
      a = f t (n-1) (m-1) k
      b = f (fst a) n (m-1) k
      c = f (fst b) (n+1) (m-1) k
      v = n * (snd a + 2 * snd b + snd c)
      in (insert (n,m,k) v $ fst c, v)

main = let n = 40 in print $ n - sum (map (snd . (f empty 1 n)) [0..n-1]) / product [1..n]

