using System;

sealed class E062
{
  string SortedString(long n)
  {
    var a = n.ToString().ToCharArray();
    Array.Sort(a);
    return new string(a);
  }

  long Compute()
  {
    var a = (long)Math.Ceiling(Math.Exp(Math.Log(Math.Pow(10, 11)) / 3));
    var b = (long)Math.Floor(Math.Exp(Math.Log(Math.Pow(10, 12)) / 3));
    for (var i = a; i <= b; i++)
    {
      var i3 = SortedString(i * i * i);
      for (var j = i + 1; j <= b; j++)
        if (i3 == SortedString(j * j * j))
          for (var k = j + 1; k <= b; k++)
            if (i3 == SortedString(k * k * k))
              for (var l = k + 1; l <= b; l++)
                if (i3 == SortedString(l * l * l))
                  for (var m = l + 1; m <= b; m++)
                    if (i3 == SortedString(m * m * m))
                      return (long)Math.Pow(i, 3);
    }
    return 0;
  }

  static void Main()
  {
    Console.WriteLine(new E062().Compute());
  }
}

