using System;
using System.Collections.Generic;

sealed class E073
{ // TODO: n = 1,200 ok, but n = 12,000 too big.
  long Compute(int n)
  {
    var set = new HashSet<Rational>();
    for (var q = 2; q <= n; q++)
      for (var p = 1; p < q; p++)
      {
        var r = new Rational(p, q);
        if (r.InRange) set.Add(r);
      }
    return set.Count;
  }

  static void Main(string[] args)
  {
    Console.WriteLine(new E073().Compute(int.Parse(args[0])));
  }
}

struct Rational
{
  int P, Q;
  public bool InRange { get { var z = P << 1; return z < Q && z > Q - P; } }
  public Rational(int p, int q) : this() { var z = Gcd(p, q); P = p / z; Q = q / z; }
  int Gcd(int a, int b) { for (int t; b != 0; a = b, b = t) t = a % b; return a; }
}
