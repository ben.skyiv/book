import System.IO ( hPrint,stderr )
import Math.NumberTheory.Primes.Testing ( isPrime )
import Math.NumberTheory.Primes.Factorisation ( factorise )
-- 49 580 620 131 241 271, 9 999 452 799 100 481, m = 10^4 for 0.5 hour
n = 9999116036873651; m = 10^4; z = 11 + 30 * div (n-11) 30
f z = (maximum xs, snd $ last xs) where
  xs = map (\x -> (sum $ map (fst .last . factorise) [x..x+8], x)) $
    take m $ filter (\x -> all isPrime [x,x+2,x+6,x+8]) [z-30,z-60..1]
main = mapM (hPrint stderr) $ scanl (\x _ -> let z = f $ snd $ fst x in
  (z, max (fst $ fst z) $ snd x)) (((n, fromIntegral m), z), 0) [1..]

