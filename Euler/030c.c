#include <stdio.h>

long sum(long n)
{
  long z = 0;
  for (; n > 0; n /= 10) {
    long i = n % 10;
    z += i*i*i*i*i*i*i*i*i*i*i;
  }
  return z;
}

long sum2(long n, long a[], int size)
{
  static long t = 0;
  int m = n % size;
  return (m != 0) ? (t + a[m]) : (t = sum(n));
}

int main()
{
  printf("Start\n"); fflush(stdout);
  static long a[100000000], z = 0;
  int size = sizeof(a) / sizeof(a[0]);
  for(int i = 1; i < size; i++) a[i] = sum(i);
  printf("Init finish\n"); fflush(stdout);
  for(long i = 2; i <= (long)1e12; i++) {
    if (sum2(i, a, size) != i) continue;
    printf("%ld\n", i); fflush(stdout);
    z += i;
  }
  printf("[%ld]\n", z); fflush(stdout);
  return 0;
}

