// mcs 660b16.cs 660.cs && ./660b16.exe

using System;

static class E660b16
{
  static E660 e = new E660(16);

  static int P556()
  { // (2xxxx, 3xxxx, 1xxxxx) (2xxxx, Fxxxx, 1xxxxx)
    var z = 0;
    for (var i1 = 2; i1 < e.B-1; i1++)
      for (var i2 = i1+1; i2 <= e.B-1; i2++)
        foreach (var s in E660.Permutation(e.GetRest(1, i1, i2), 8))
          z += e.F(e.B*(e.B*(e.B*(e.B*i1+s[1])+s[2])+s[3])+s[4],
                   e.B*(e.B*(e.B*(e.B*i2+s[5])+s[6])+s[7])+s[8]);
    return z;
  }

  static void Main() { Console.WriteLine(P556()); }
}

