main = let n = 1000 in print $ sum $ map (\n -> 2 * n * div (n-1) 2) [3..n]

