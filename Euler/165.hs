import Data.List ( tails )
import Data.Set ( fromList,size )

main = let n = 5000 in print . size . fromList . concat . map (\(l1:ls) -> [(x,y) | l2 <- ls,
  let (a1,b1,c1) = line l1; (a2,b2,c2) = line l2; d = fromIntegral $ a1*b2-a2*b1,
  d /= 0, let x = fromIntegral (b2*c1-b1*c2) / d; y = fromIntegral (a1*c2-a2*c1) / d,
  within x y l1, within x y l2]) . init . init . tails $ take n $
  map (take 4 . flip drop (map (flip mod 500) $ tail $ iterate
  (flip mod 50515093 . (^2)) 290797)) [0,4..] where
  between x a b = if a > b then between x b a else x >= fromIntegral a && x <= fromIntegral b
  within x y [x1,y1,x2,y2] = between x x1 x2 && between y y1 y2
    && (x /= fromIntegral x1 || y /= fromIntegral y1)
    && (x /= fromIntegral x2 || y /= fromIntegral y2)
  line [x1,y1,x2,y2] = (y2-y1, x1-x2, x1*y2-y1*x2)

