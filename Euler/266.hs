import Control.Arrow ( (***) )
import Data.List ( sort,subsequences )
import Math.NumberTheory.Primes.Sieve ( primes )

main = let
  n = 190; ps = takeWhile (<n) primes; g = sort . map product . subsequences
  f z (x:xs) (y:ys) = if x*y > (floor $ sqrt $ fromIntegral $ product ps)
    then f z xs (y:ys) else f (max (x*y) z) (x:xs) ys
  f z _ _ = z
  in print $ (flip mod (10^16)) $ uncurry (f 0) . (reverse . g *** g)
    . splitAt (div (length ps) 2) $ ps

