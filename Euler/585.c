#include <stdio.h>
#include <math.h>

int isSquare(long n) { long x = (long)sqrt(n); return x * x == n; }
int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }

int count(int a, int b0, int b9)
{ // count: b in (b0, b9]; b,ab != k^2
  for (int k, j = a % 2 + 1, i = j + 1; (k = i * i) <= a; )
    if (a % k == 0) a /= k; else i += j;
  return b9 - (int)sqrt(b9) - (int)sqrt(b9/a)
       - b0 + (int)sqrt(b0) + (int)sqrt(b0/a);
}

long f2a(int n)
{ // i + √b: 0 < i, 1 < b != k^2
  long z = 0;    // i^2 + b <= n
  for (int b, i = 1; 1 < (b = n - i * i); i++)
    z += b - (int)sqrt(b);
  return z;
}

long f2b(int n)
{ // √a + √b: 1 < a < b; a,b,ab != k^2
  long z = 0;            // a + b <= n
  for (int b, a = 2; a < (b = n - a); a++)
    if (!isSquare(a)) z += count(a, a, b);
  return z;
}

long f4(int n)
{ // -√a + √b + √c + √d: ad = bc;     ab,ac,bc != k^2
  long z = 0;    // 0 < a < b < c, a + b + c + d <= n
  for (int a = 1; (a << 2) + 6 <= n; a++)
    for (int b = a + 1; b < (long)a*(n-a-b)/(a+b); b++) {
      if (isSquare((long)a * b)) continue;
      int k = a / gcd(a, b), c = (b + 1) / k * k;
      if (c < b + 1) c += k;
      for (long e; a + b + c + c + 1 <= n; c += k)
        if (a + b + c + (e = (long)c * b) / a <= n &&
          !isSquare(e) && !isSquare((long)c * a)) z++;
    }
  return z;
}

long f(int n) { return f2a(n) + f2b(n) + f4(n); }

int main()
{
  static int a[] = { 10, 15, 20, 30, 100, 5000 };
  for (int i = 0; i < sizeof(a) / sizeof(a[0]); i++)
    printf("%ld ", f(a[i]));
  printf("\n17 46 86 213 2918 11134074\n");
}

