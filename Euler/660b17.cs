// mcs 660b17.cs 660.cs && ./660b17.exe

using System;

static class E660b17
{
  static E660 e = new E660(17);

  static int P566()
  { // (xxxxx, 1xxxxx, 2xxxxx) (xxxxx, Fxxxxx, Gxxxxx)
    var z = 0;
    for (var i1 = 1; i1 <= e.B - 2; i1++)
      foreach (var s in E660.Permutation(e.GetRest(i1, i1+1), 10))
        z += e.F( e.B*(e.B*(e.B*(e.B*s[1] +s[2])+s[3])+s[4])+s[5],
          e.B*(e.B*(e.B*(e.B*(e.B*i1+s[6])+s[7])+s[8])+s[9])+s[10]);
    return z;
  }

  static void Main() { Console.WriteLine(P566()); }
}

