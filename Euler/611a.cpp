// g++ -O3 -fopenmp 611a.cpp
#include <cmath>
#include <iostream>
#include <unordered_set>

long work(int n, int m)
{
  long a = n*(long)m + 1, b = a + n - 1; std::unordered_set<long> s;
  for (long i2, i = 1; (i2 = i*i) < b; i++)
    for (long k, j = std::max(i+1, (long)sqrt(std::max(0L, a-i2)));
      (k = i2 + j*j) <= b; j++) if (k >= a)
      if (s.count(k)) s.erase(k); else s.insert(k);
  return s.size();
}

int main()
{
  int n = 1e5, i9 = 1e7; long z = 0;
  #pragma omp parallel for reduction(+:z) schedule(dynamic)  
  for (int i = 0; i < i9; i++) z += work(n, i);
  std::cout << z << std::endl;
}

