#include <stdio.h>

long compute(const long fib[], long n, long s, int k)
{
  long s2, sum = s;
  for(int i = k; ; sum += compute(fib, n, s2, (i += 2) + 4))
    if((s2 = s + fib[i - 1] + fib[i + 5]) > n) return sum;
}

int main(void)
{
  long fib[60] = { 0, 1 }, n = 10000000000, sum = 1;
  for(int i=2; i < sizeof fib / sizeof fib[0]; i++) fib[i]=fib[i-1]+fib[i-2];
  sum += compute(fib, n, 0, 2); sum += compute(fib, n, 2, 4);
  printf("%ld\n", sum);
  return 0;
}
