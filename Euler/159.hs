import Data.Map ( (!),elems,fromList,insert )

main = let
  n = 10^6; m = floor $ logBase 2 $ fromIntegral n
  a = fromList $ zip [0..n-1] $ 9 : cycle [1..9]
  f a i = let x = 2^i; z = if i == m then n else x + x
    in foldl (\a j -> let c = j - 9 * div (j-1) 9; k = j * (1 + div (x-1) j)
    in foldl (\a k -> let v = c + a ! div k j in if a ! k >= v then a
    else insert k v a) a [k,k+j..z-1]) a $ takeWhile ((<z) . (^2)) [2..]
  in print $ sum $ drop 2 $ elems $ foldl f a [1..m]

