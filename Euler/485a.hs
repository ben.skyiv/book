import Data.List ( foldl' )
import Math.NumberTheory.Primes.Factorisation ( τ )

main = let u = 10^8; k = 10^5 in print $ fst $ foldl' (\(s,m) i -> let
  z = if m == 0 || m == τ (i-1) then foldl' (\m j -> max m $ τ j) 0
    [i..i+k-1] else max m $ τ (i+k-1) in (s+z,z)) (0,0) [1..u-k+1]

