import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Counting ( primeCount )

pqr n = sum $ map (\(p,q) -> let z = div n (p*q) in if z > q then primeCount
  z - primeCount q else 0) $ [(p,q) | p <- takeWhile ((<=n) . (^3)) primes,
  q <- takeWhile (< div n (p*p)) $ dropWhile (<=p) primes]
p3q n = sum [primeCount (div n (p^3)) - primeCount p | p <- takeWhile ((<=n) . (^4)) primes]
pq3 n = sum [let s = takeWhile (\p -> p < q && p*q^3 <= n) primes in if null s then 0
  else primeCount $ last s | q <- takeWhile ((<=n). (^3)) $ tail primes]
p7 n = primeCount $ floor $ fromIntegral n ** (1/7)
main = let n = 10^6 in print $ sum [pqr n, p3q n, pq3 n, p7 n]

