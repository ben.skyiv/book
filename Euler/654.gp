\\ time gp -q -s 2048000000 654.gp
\\ https://projecteuler.net/problem=654
c=10^9+7;n=500;m=10^12 \\ n=5000
print(lift((Mod(matrix(n-1,n-1,i,j,if(i<=n-j,1,0)),c)^(m+1))[1,1]))
quit()

