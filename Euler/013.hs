main = do
  x <- sequence $ replicate 100 getLine
  putStrLn $ take 10 $ show . sum $ map read x

