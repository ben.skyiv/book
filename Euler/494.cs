using System;
using System.Linq;
using System.Collections.Generic;

sealed class ArrayEqualityComparer : IEqualityComparer<int[]>
{
  public bool Equals(int[] x, int[] y)
  {
    if (x.Length != y.Length) return false;
    for (var i = x.Length - 1; i >= 0; i--)
      if (x[i] != y[i]) return false;
    return true;
  }

  public int GetHashCode(int[] obj)
  {
    int v = 0;
    foreach (var n in obj) v ^= n;
    return v;
  }
}

sealed class E494
{
  void Out<T>(IEnumerable<T> v)
  {
    foreach (var n in v) Console.Write(n + " ");
    Console.WriteLine();
  }

  int BitCount(uint x)
  {
    x -= ((x >> 1) & 0x55555555);
    x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
    x = (x + (x >> 4)) & 0x0F0F0F0F; x += x >> 8;
    return (int)((x + (x >> 16)) & 0x3F);
  }

  bool IsPowerOf2(uint x) { return BitCount(x) == 1; }

  uint[] Collatz(uint x)
  {
    var list = new List<uint>();
    for (; !IsPowerOf2(x); x = ((x&1) != 0) ? (3*x+1) : (x>>1))
      list.Add(x);
    return list.ToArray();
  }
  
  List<uint[]> GetCollatzs(int n, int max)
  {
    var list = new List<uint[]>();
    for (uint i = 1; i <= max; i++)
    {
      var a = Collatz(i);
      if (a.Length == n) list.Add(a);
    }
    return list;
  }

  List<uint[]> MergeCollatzs(List<uint[]> list)
  {
    var n = list[0].Length;
    var list2 = new List<uint[]>();
    var set = new HashSet<int[]>(new ArrayEqualityComparer());
    foreach (var v in list)
    {
      var a = Enumerable.Range(0, n).ToArray();
      Array.Sort((int[])v.Clone(), a);
      if (set.Add(a)) list2.Add(v);
    }
    return list2;
  }

  void Run(int n, int max)
  {
    var list = MergeCollatzs(GetCollatzs(n, max));
    foreach (var v in list) Out(v);
    Console.WriteLine("f({0}) >= {1}", n, list.Count);
  }
  
  static void Main() { new E494().Run(3, 10000); }
}

