#include <stdio.h>
#include <math.h>

int main(void)
{
  long n = (long)1e18, z = 0;
  for (int e = (int)(log(n-2) / log(2)); e > 1; e--) {
    double e1 = 1.0 / e; long a = (long)pow(n, e1);
    while ((long)pow(a, e) + a > n) a--; z += a - 1;
    for (int i = 1; (a = (long)pow(n, e1 /= e) - 1) > 0; i++)
      z += a * (i + i + 1);
  }
  printf("%ld\n", z);
}

