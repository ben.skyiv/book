#include <iostream>
#include <cmath>
#include <ctime>
#include <unordered_set>
#include <omp.h>

static float R = CLOCKS_PER_SEC * 60.0;

long f(int i, int m, long n)
{
  std::unordered_set<long> s;
  for (int b = 2; ; b++) {
    long b3 = (long)b * b * b, a2 = n / b3;
    if (a2 < 4) break;
    for (int a = (int)sqrt(a2); a > 1; a--) {
      long c = b3 * a * a;
      if (c % m == i) s.insert(c);
    }
  }
  std::cout << s.size() << ": " << i
    << " " << omp_get_thread_num()
    << "/" << omp_get_num_threads()
    << " " << clock() / R << std::endl;
  return s.size();
}

int main()
{
  int m = 149; long z = 0, n = (long)9e16;
  std::cout << n << " " << omp_get_num_procs()
    << " " << clock() / R << std::endl;
  omp_set_num_threads(2);
  #pragma omp parallel for reduction(+:z) schedule(static)
  for (int i = 0; i < m; i++) z += f(i, m, n);
  std::cout << z << " " << clock() / R << std::endl;
}
