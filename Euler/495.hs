import Data.Set ( elems )
import Math.NumberTheory.Primes.Factorisation ( divisors )

m = 10^9 + 7; n = 12; z = product [2..n]
f z l 1 = 1
f z l k = mod (sum $ map (\i -> mod (f (div z i) (i+1) (k-1)) m) $
  takeWhile (\i -> div z i >= i+1) $ filter (>=l) $ elems $ divisors z) m
w k = f z 2 (k-1) + f z 2 k
main = print $ w 10

