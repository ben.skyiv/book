import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Counting ( primeCount )
main = let n = 10^8; m = floor $ sqrt $ fromIntegral $ n-1 in print $ sum $
  map (\p -> (primeCount $ div n p) - (primeCount $ p-1)) $ takeWhile (<=m) primes

