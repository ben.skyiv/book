import Data.Bits
main = let n = 10^2 :: Int in print $ length [() | let z = div n 2,
  p <- [1..z-1], q <- [p+1..z], popCount (gcd p q) == 1]

