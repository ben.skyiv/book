#include <iostream>

typedef struct { int ps[10]; char ks[10], n; } item;

int inv(int a, int m)
{
  int x = 1, y = 0, z = 0, w = 1, b = m;
  for (a = (a < b) ? a : (a % b); a != 0; ) {
    int c = b / a; b -= c * a; z -= c * x; w -= c * y;
    std::swap(a, b); std::swap(x, z); std::swap(y, w);
  }
  return (z < 0) ? (z + m) : z;
}

int I(int ps[], char ks[], int k, int n, long m, long v)
{
  if (k == 0) return v;
  int u, r1, r2, p = ps[k - 1], w = 1;
  for (int i = 0; i < ks[k - 1]; i++) w *= p;
  if (p == 2) {
    if (ks[k - 1] == 1) return (v % 2) ? v : (v + m);
    if (ks[k - 1] == 2) {
      for (r1 = v; r1 % 4 != 1; ) r1 += m;
      for (r2 = v; r2 % 4 != 3; ) r2 += m;
      if (r1 == 4 * m - 1) return r2;
      if (r2 == 4 * m - 1) return r1;
      return (r1 > r2) ? r1 : r2;
    }
    r1 = ((u = inv(w, m)) * w * (v - 1L) + 1) % (w*m);
    r2 = (u * w * (v + 1L) - 1 + w*m) % (w*m);
    if (r1 < r2 && r2 != n - 1) r1 = r2;
    r2 = (u*w * (v + w*m - (w/2 + 1L)) + w/2 + 1) % (w*m);
    if (r1 < r2) r1 = r2;
    r2 = (u*w * (v + w*m - (w/2 - 1L)) + w/2 - 1) % (w*m);
    return (r1 > r2) ? r1 : r2;
  }
  r1 = I(ps,ks,k-1,n,w*m,((u=inv(w,m))*w*(v-1L)+1)%(w*m));
  r2 = I(ps,ks,k-1,n,w*m,(u*w*(v+1L)-1+w*m)%(w*m));
  return (r2 == n - 1) ? r1 : (r1 > r2) ? r1 : r2;
}

int main()
{
  const int m = 20'000'000;
  static item a[m + 1];
  for (int i = 2; i <= m; i++)
    if (a[i].n == 0)
      for (int j = i; j <= m; j += i)
        a[j].ps[a[j].n++] = i;
  for (int i = 2; i <= m; i++)
    for (int j = 0; j < a[i].n; j++)
      for (int k = i, d = a[i].ps[j]; k % d == 0; k /= d)
        a[i].ks[j]++;
  long z = 0;
  for (int i = 3; i <= m; i++)
    z += I(a[i].ps, a[i].ks, a[i].n, i, 1, 1);
  std::cout << z << std::endl;
}

