import Data.List ( nub )

eq p s k | s < k = False | p == 1 = s == k | k == 1 = p == s | True = any (\i ->
  mod p i == 0 && eq (div p i) (s-i) (k-1)) $ takeWhile ((>=k-1) . (-) s) [2..p]
main = let n = 12000 in print $ sum $ nub $ map (\k -> until (\n -> eq n n k) (+1) k) [2..n]

