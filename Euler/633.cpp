#include <iostream>
#include <vector>

void prime(double a[], int n)
{
  double x = 1.0 / n / n;
  for (int i = 7; i >= 0; i--) a[i+1] += a[i]*x, a[i] *= 1-x;
}

int main()
{
  long n = 1'000'000;
  std::vector<long> b(n + 1); b[1] = 1;
  for (long i = 2; i <= n; i += 2) b[i] = 2;
  double a[8]; a[0] = 1; prime(a, 2);
  for (long i = 3; i <= n; i += 2) {
    if (b[i]) continue;
    prime(a, b[i] = i);
    if (n / i < i) continue;
    for (long j = i*i; j <= n; j += i+i) b[j] = i;
  }
  std::cout << a[7] << std::endl;
}
