isPrime n = null [ x | x <- [3,5..ceiling $ sqrt $ fromIntegral n], mod n x == 0 ]
main = let n = 10001 in print $ filter isPrime [3,5..] !! (n-2)
-- 0m0.284s

