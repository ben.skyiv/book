import Data.Digits ( digitsRev )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )

f (p,q) = head [z | x <- [1..], let z = p + x * 10^(length $ digitsRev 10 p), mod z q == 0]

main = let n = 10^6 in print $ let ps = sieveFrom 5 in
  sum $ map f $ takeWhile ((<=n) . fst) $ zip ps $ drop 1 ps

-- 14m51.967s

