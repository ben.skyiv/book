import Data.Array ( (!),accum,listArray )
import Text.Printf ( printf )

main = let
  m = 36; a = listArray (0,m) $ [0,0..]
  peter = throw 4 9 0 a; colin = throw 6 6 0 a
  peterWin = sum [peter!i * colin!j | i <- [1..m], j <- [1..i-1]]
  total    = sum [peter!i * colin!j | i <- [1..m], j <- [1..m]]
  throw d n s p = if n == 0 then accum (+) p [(s,1)]
    else foldl (\a x -> throw d (n-1) (s+x) a) p [1..d]
  in printf "%.07f\n" $ (peterWin / total :: Double)

