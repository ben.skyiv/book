using System;
using System.Collections.Generic;

static class E617
{
  static SortedDictionary<Tuple<int,int>, List<int>> dict
   = new SortedDictionary<Tuple<int,int>, List<int>>();
  static HashSet<int> ns = new HashSet<int>();

  static int MPS(int n, int e, int a)
  {
    ns.Clear(); ns.Add(a);
    for (int c = a, i = 1; i < 100; i++) {
      long b = (long)Math.Pow(c, e), d = Math.Min(b, n - b);
      if (d < 2) return 0;
      if (!ns.Add(c = (int)d)) return ns.Count;
    }
    throw new Exception(string.Format("MPS:({0},{1},{2})", n, e, a));
  }

  static void C(int n)
  {
    for (int e9 = (int)Math.Log(n - 2, 2), e = 2; e <= e9; e++)
      for (var a = 2; ; a++) {
        var b = (long)Math.Pow(a, e);
        if (Math.Min(b, n - b) < 2) break;
        var c = MPS(n, e, a);
        if (c <= 0) continue;
        var key = Tuple.Create(e, n);
        List<int> v;
        if (!dict.TryGetValue(key, out v)) dict.Add(key, v = new List<int>());
        v.Add(a);
      }
  }

  static void Main()
  {
    for (int i = 2; i <= (int)1e3; i++) C(i);
    foreach (var kvp in dict) {
      Console.Write("{0,2}, {1,3}:", kvp.Key.Item1, kvp.Key.Item2);
      kvp.Value.Reverse();
      foreach (var t in kvp.Value) Console.Write(" {0,2}", t);
      Console.WriteLine();
    }
  }
}

