main = print $ snd $ foldl (\(u,v) x ->
  let a = 998-x-x; b = 999-x; w = (1000+a*u)/b
  in (w,(1000+a*v+w)/b)) (2.0,2.004) [498,497..0]

