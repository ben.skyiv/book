import Math.NumberTheory.GCD ( extendedGCD )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )

main = print $ sum $ map (\p -> let (_,x,y) = extendedGCD 8 p
  in mod (-3*x) p) $ takeWhile (<10^8) $ sieveFrom 5

