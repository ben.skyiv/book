import Data.List ( permutations,sort )

main = let n = 10^6 in putStrLn . (!!(n-1)) . sort $ permutations ['0'..'9']

