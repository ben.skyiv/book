import Data.Digits ( digits )
import Data.Array ( (!),listArray,range )

i = (0,22*10^5); chain xs@(x:_) | elem (a!x) xs = xs | True = chain $ (a!x):xs
a = listArray i $ map (sum . map (product . enumFromTo 2) . digits 10) $ range i
main = print $ length $ filter (==60) $ map (length . chain . (:[])) [1..10^6]

