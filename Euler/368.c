#include <stdio.h>

int isnotc3(long n)
{
  for (int ok = 0, d1 = n % 10, d0 = 10; n > 0; n /= 10, d0 = d1, d1 = n % 10)
    if (ok)
      if (d0 == d1) return 0;
      else ok = 0;
    else ok = d0 == d1;
  return 1;
}

int main()
{
  long n = 100000000;
  double h = 0;
  for (long i = 1; ; i++)
  {
    if (i % n == 0) printf("%5ld: %15.11lf\n", i / n, h);
    if (isnotc3(i)) h += 1.0 / i;
  }
  return 0;
}

