#include <stdio.h>

const int n = 500000000;
static int a[n+1];

int totient(int n)
{
  int v = n - 1, p = a[n], q;
  if (p != 0) {
    v = (p - 1) * a[q = n / p];
    if (q % p == 0) v = v / (p - 1) * p;
  }
  return a[n] = v;
}

int main(void)
{
  for (int i = 3; i * i <= n; i += 2)
    if (a[i] == 0)
      for (int k = i << 1, j = i * i; j <= n; j += k)
        a[j] = i;
  long v = 1;
  for (int i = 3; i <= n; i += 2) v += totient(i);
  printf("%ld\n", v); // 12.484s
  return 0;
}

