import Math.NumberTheory.Primes.Testing ( isPrime )
import Math.NumberTheory.Primes.Sieve ( primes )

main = let n = 10^8 in print $ sum $ filter (\n -> all isPrime $ map
  (uncurry (+)) $ filter ((==0) . (mod n) . fst) $ takeWhile (uncurry (<=))
  [(d, div n d) | d <- [2..]]) $ map pred $ takeWhile (<=n+1) $ primes

