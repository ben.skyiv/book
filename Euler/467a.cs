using System;
using System.Numerics;
using System.Collections;

class ProjectEuler467
{
  const int N = 10000;
  const int M = 104743; // See Problem 7
  static BitArray primes;

  static void PrimeSieve(int n)
  {
    primes = new BitArray(n + 1, true);
    primes[0] = primes[1] = false;
    int m = (int)Math.Sqrt(n);
    for (int i = 2; i <= m; i++)
      if (primes[i])
        for (int k = i << 1; k <= n; k += i)
          primes[k] = false;
  }

  static byte DigitalRoot(int i)
  {
    int d = 0;
    for (; i > 0; i /= 10)
    {
      d += i % 10;
      if (d > 9) d = d / 10 + d % 10;
    }
    return (byte)d;
  }

  static byte[,] f;

  static void LCS()
  {
    int[,] m = new int[N + 1, N + 1];
    f = new byte[N + 1, N + 1];
    for (int i = 0; i <= N; i++) m[i, 0] = 0;
    for (int j = 0; j <= N; j++) m[0, j] = 0;
    for (int i = 1; i <= N; i++)
      for (int j = 1; j <= N; j++)
        if (a[i - 1] == b[j - 1])
        {
          m[i, j] = m[i - 1, j - 1] + 1;
          f[i, j] = 7; // left_up
        }
        else if (m[i - 1, j] == m[i, j - 1])
        {
          m [i, j] = m[i - 1, j];
          f[i, j] = (a[i - 1] > b[j - 1]) ? (byte)8 : (byte)4;
        }
        else if (m[i - 1, j] > m[i, j - 1])
        {
          m [i, j] = m[i - 1, j];
          f[i, j] = 4; // left
        }
        else
        {
          m[i, j] = m[i, j - 1];
          f[i, j] = 8; // up
        }
    SubSequence(N, N);
    Make(0, 0);
  }

  static void SubSequence(int i, int j)
  {
    if (i == 0 || j == 0) return;
    if (f[i, j] == 7)
    {
      Make(i, j);
      z = z * 10 + a[u]; u--; v--;
      SubSequence(i - 1, j - 1);
    }
    else if (f[i, j] == 8) SubSequence(i, j - 1);
    else SubSequence(i - 1, j);
  }

  static void Make(int i, int j)
  {
    while (u > i - 1 && v > j - 1)
      if (a[u] < b[v]) { z = z * 10 + a[u]; u--; }
      else             { z = z * 10 + b[v]; v--; }
    for (; u > i - 1; u--) z = z * 10 + a[u];
    for (; v > j - 1; v--) z = z * 10 + b[v];
  }

  static BigInteger z = 0;
  static int u = N - 1;
  static int v = N - 1;
  static byte[] a = new byte[N];
  static byte[] b = new byte[N];

  static void Main()
  {
    PrimeSieve(M);
    for (int pi = 0, ci = 0, i = 2; pi < N; i++)
      if (primes[i]  ) a[pi++] = DigitalRoot(i);
      else if (ci < N) b[ci++] = DigitalRoot(i);
    Array.Reverse(a);
    Array.Reverse(b);
    LCS();
    Console.WriteLine(z % 1000000007);
  }
}
