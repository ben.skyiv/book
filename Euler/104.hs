import Data.List
main = print $ fst . head . filter ((=="123456789") . sort . take 9 . show . snd)
  $ filter ((=="123456789") . sort . show . flip mod (10^9) . snd)
  $ zip [1..] $ map fst $ iterate (\(a,b) -> (b,a+b)) (1,1)

