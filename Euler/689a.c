#include <stdio.h>
#include <math.h>

int main(void)
{
  long double a = 0.5, z = a;
  for (int i =  7; i <  11; i++) z += powl(a, i);
  for (int i = 12; i <  54; i++) z += powl(a, i);
  for (int i = 55; i < 519; i++) z += powl(a, i);
  printf("%.9llf\n", z);
}
