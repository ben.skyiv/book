main = let lgn = 100; m = 10^5 in print $ mod (sum [sum [n*10+d |
  d <- [1..9], p <- [1..9], let (n,m) = divMod ((z-p)*d) (10*p-1),
  m == 0, 10*n >= z] | x <- [1..lgn-1], let z = 10^x]) m

