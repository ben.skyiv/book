using System;
using System.Collections.Generic;
using System.Numerics;

sealed class E169
{
  long Compute(BigInteger n)
  {
    var a = new List<int>();
    for (var i = 1; n > 0; n /= 2, i++) if (n % 2 != 0) a.Add(i);
    for (var i = a.Count - 1; i > 0; i--) a[i] -= a[i - 1];
    long f = 1, z = 0;
    for (var i = 0; i < a.Count; i++)
    {
      long f2 = a[i] * f + z;
      z = f2 - f;
      f = f2;
    }
    return f;
  }

  static void Main(string[] args)
  {
    var e = (args.Length > 0) ? int.Parse(args[0]) : 3;
    var n = BigInteger.Pow(10, e);
    Console.WriteLine("f(10^{0}) = {1}", e, new E169().Compute(n));
  }
}
