using LinearAlgebra, Printf

v(n, i) = abs(min(i, n-i))
α(n, i, j) = sum((3 - abs(δ)) * Int(j == v(n, i+δ)) for δ in -2:2) / 9

Q(n) = [α(n, i, j) for i in 1:n÷2, j in 1:n÷2]
b(n) = [Int(2i < n) + Int(2i ≤ n) for i in 1:n÷2] ./ n

g(n) = (A = inv(I - Q(n)); dot((2A - I) * sum(A, dims=2), b(n)))
G(n) = sum(g(k) for k in 1:n)

@time @printf("%.8E\n", G(500))
