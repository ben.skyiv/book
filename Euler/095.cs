using System;
using System.Collections.Generic;

sealed class E095
{
  static int Sum(int n, int limit)
  {
    int sum = 1;
    for (int j, i = (int)Math.Sqrt(n); i > 1; i--)
    {
      if (n % i != 0) continue;
      sum += i;
      if ((j = n / i) != i) sum += j;
      if (sum > limit) return 0;
    }
    return sum;
  }

  static void Main()
  {
    int z = 0, n = 1000000;
    HashSet<int> a = new HashSet<int>(), b = new HashSet<int>();
    for (int i = 1; i <= n; i++, b.UnionWith(a), a.Clear())
    {
      for (z = i; !a.Contains(z) && !b.Contains(z); z = Sum(z, n)) a.Add(z);
      if (a.Count >= 30 && !b.Contains(z)) break;
    }
    Console.WriteLine(z);
  }
}

