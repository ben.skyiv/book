#include <iostream>
#include <unordered_set>

int main()
{
  std::unordered_set<long> s; long n = 1e8;
  for (long i2, i = 1; (i2 = i*i) < n; i++)
    for (long k, j = i+1; (k = i2 + j*j) <= n; j++)
      if (s.count(k)) s.erase(k); else s.insert(k);
  std::cout << s.size() << std::endl;
}

