using System;
using System.Collections.Generic;

sealed class E660
{
  public int B { get; private set; }

  public E660(int b) { B = b; }

  static void Swap<T>(ref T a, ref T b) { T t = a; a = b; b = t; }

  public static IEnumerable<byte[]> Permutation(byte[] a, int t)
  {
    foreach (var i in Combine(a, t))
      foreach (var s in Permutation(i))
        yield return s;
  }

  static IEnumerable<byte[]> Combine(byte[] a, int t)
  { // see TAOCP Algorithm 7.2.1.3L
    var d = new byte[t + 1];
    var c = new int[t + 3]; c[t + 1] = a.Length;
    for (var j = 1; j <= t; j++) c[j] = j - 1;
    for (int j = 0; j <= t; c[j]++) {
      for (var i = 1; i <= t; i++) d[i] = a[c[i]]; yield return d;
      for (j = 1; c[j] + 1 == c[j+1]; j++) c[j] = j - 1;
    }
  }

  static IEnumerable<byte[]> Permutation(byte[] a)
  { // see TAOCP Algorithm 7.2.1.2L
    // Note: a[0] < max(a[1..n])
    // Only permutation a[1..n] for sorted a[1..n]
    for (var n = a.Length - 1; ; ) {
      yield return a;
      var j = n-1; while (a[j] >= a[j+1]) j--; if (j == 0) yield break;
      var l = n; while (a[j] >= a[l]) l--; Swap(ref a[j], ref a[l]);
      for (int k = j+1, m = n; k < m; k++, m--) Swap(ref a[k], ref a[m]);
    }
  }

  public bool IsPandigital(params int[] a)
  {
    var s = new HashSet<int>();
    int k = 0;
    foreach (var n in a)
      for (var i = n; i > 0; i /= B) { k++; s.Add(i % B); }
    return k == B && s.Count == B;
  }

  public byte[] GetRest(params int[] a)
  {
    var list = new List<byte>();
    for (byte i = 0; i < B; i++)
      if (Array.IndexOf(a, i) < 0) list.Add(i);
    return list.ToArray();
  }

  public int F(int a, int b)
  {
    var c = Math.Sqrt((long)a * a + (long)b * b + (long)a * b);
    var d = (int)c;
    if (d != c) return 0;
    if (!IsPandigital(a, b, d)) return 0;
    Console.WriteLine("{0}: {1} {2}", c, a, b);
    return d;
  }
}

