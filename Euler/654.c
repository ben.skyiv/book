#include <stdio.h>

const int  c = 7 + (int)1e9;
const int  n = 100;
const long m = 10;

int main(void)
{
  static int a[n], b[n];
  int *p = a, *q = b;
  for (int i = 1; i < n; i++) p[i] = i;
  for (long k = m ; k > 1; k--) {
    for (int i = 1; i < n; i++) q[i] = p[n-i];
    for (int i = 2; i < n; i++) q[i] = (q[i] + q[i-1]) % c;
    int *t = p; p = q; q = t;
  }
  printf("T(%d, %ld) mod %d = %d\n", n, m, c, p[n-1]);
}

