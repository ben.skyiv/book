import Data.Ord ( comparing )
import Data.List ( maximumBy )
import Data.Array ( (!),assocs,listArray )
 
f n = a where
  a = listArray (1,n) $ 0:[1 + g n x | x <- [2..n]]
  g n x = let z = if even x then div x 2 else 3*x+1
    in if z <= n then a ! z else 1 + g n z

main = let n = 10^6 in print $ maximumBy (comparing snd) $ assocs $ f n

