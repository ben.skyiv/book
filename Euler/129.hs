main = let n = 10^6 in print $ head $ filter ((>n) . snd) $ zip
  [n..] $ map (\z -> if gcd z 10 /= 1 then 0 else (1+) $ length
  $ takeWhile (/=0) $ iterate (\x -> mod (x*10+1) z) 1) [n..]

