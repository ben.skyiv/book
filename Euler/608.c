#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct { long ps[12]; char ks[12], n; } item;

const long n =    (long)1e12, m = 7 + (long)1e9, R = (long)1e9;
const int n2 = 1 + (int)1e6, k9 = 15; // n2 > sqrt(n + n2)
const int p9 = 200, n0 = 46; // \pi(200) = 46
static int a2[n0], av[n0], pp[p9 + 1];
static int a1[n0] = {197,97,49,32,19,16,11,10,8,6,6,5,4,4,4,
  3,3,3,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};

int inv(int a)
{
  int x0 = 1;
  for (int c, q, x, x1 = 0, b = m; b; x0 = x1, x1 = x)
    c = a - (q = a/b) * b, a = b, b = c, x = x0 - q*x1;
  return (x0 < 0) ? (x0+m) : x0;
}

void factor(long i0, item a[], const char c[])
{
  static long t[n2];
  for (int i = 0; i < n2; i++) t[i] = 1, a[i].n = 0;
  for (int p = 2; (long)p * p < i0 + n2; p++)
    if (!c[p])
      for (long q = p; q < i0 + n2; q *= p)
        for (long i = q-1-(i0-1)%q; i < n2; t[i] *= p, i += q) {
          if (q != p) ++a[i].ks[a[i].n - 1];
          else a[i].ps[a[i].n] = p, a[i].ks[a[i].n++] = 1;
        }
  for (int i = 0; i < n2; i++) {
    if (i + i0 == t[i]) continue;
    a[i].ps[a[i].n] = (i + i0) / t[i];
    a[i].ks[a[i].n++] = 1;
  }
}

long g(const long ps[], const char ks[], int k)
{
  long z = 1;
  for (int i = 0; i < k; i++)
    if (ps[i] > p9) z = z * (ks[i] + 1) % m;
    else {
      int j = pp[ps[i]];
      z = z * (a2[j] + a1[j] * ks[i]) % m * av[j] % m;
    }
  return z;
}

int main(int argc, char *argv[])
{
  clock_t t = clock();
  static item a[n2]; static char c[n2];
  for (int i = 2; i * i < n2; i++)
    if (!c[i]) for (int j = i * i; j < n2; j += i) c[j] = 1;
  for (int j = 0, i = 2; i <= p9; i++) if (!c[i]) pp[i] = j++;
  long z0 = 1, z = 0;
  for (int i = 0; i < n0; i++)
    ++a1[i], z0 = z0 * (a2[i] = a1[i] * (a1[i]+1) / 2) % m,
    av[i] = inv(a2[i]);
  static long r[k9 + 1];
  for (int i = 1; i <= k9; i++) r[i] = n * i / k9;
  int k = (argc > 1) ? atoi(argv[1]) : 1;
  long i1 = r[k-1] + 1, i2 = r[k];
  printf("%ld - %ld\n", i1, i2); fflush(stdout);
  for (long i0 = -n2, i = i1; i <= i2; i++) {
    if (i >= i0 + n2) factor(i0 = i, a, c);
    z = (z + z0 * g(a[i-i0].ps, a[i-i0].ks, a[i-i0].n)) % m;
    if (i % R == 0) { printf("%ld ", i / R); fflush(stdout); }
  }
  t = clock() - t;
  printf("\n%ld (%f seconds)\n", z, (float)t / CLOCKS_PER_SEC);
}

