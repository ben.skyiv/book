import Data.Array
import Control.Arrow

main = do
  grid <- readFile "011.txt"
  print $ (\a -> maximum [ product $ map (a!) s | let b = bounds a,
    d <- [first (+1), (+1) *** (+1), second (+1), (+1) *** (+(-1))],
    i <- range b, let s = take 4 $ iterate d i, all (inRange b) s ])
    $ (\s -> let (n,k) = (length s, length $ head s)
    in (\(n,k) -> listArray ((1,1),(n,k))) (n,k) [ x | t <- s, x <- t ])
    . map (map read . words) . lines $ grid

