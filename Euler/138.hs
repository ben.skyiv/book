import Data.List ( nub, sort )

p 1 xs = xs
p n xs = (\[d,a,b] [_,u,v] -> [d,a*u+d*b*v,a*v+b*u]) xs $ p (n-1) xs

f x y = [(2*c+b)^2 + c^2 | n <- [1..20], let [_,b,c] = p n [5,x,y]]

main = print $ sum $ take 12 $ nub $ sort $ f 2 1 ++ f 9 4

