using System;
using System.Collections.Generic;
using System.Linq;
using Skyiv.Utils;

sealed class E516
{
  static ulong max = 1000000000000;
  static ulong[] a0, a2, a3, a5;
  static ulong M(ulong a, ulong b) { return (a > max / b) ? 0 : a * b; }

  static void Init()
  {
    var set = new SortedSet<ulong>();
    for (ulong i = 1; i < max; i *= 2)
      for (ulong u, j = 1; (u = i*j) < max; j *= 3)
        for (ulong v, w, k = 1; (v = u*k) < max; k *= 5)
          if ((w = v + 1).IsPrime()) set.Add(w);
    a0 = set.Skip(3).ToArray();
    a2 = Init(2); a3 = Init(3); a5 = Init(5);
  }

  static ulong[] Init(ulong p)
  {
    var list = new List<ulong>();
    for (ulong q = 1; q <= max; q *= p) list.Add(q);
    return list.ToArray();
  }

  static uint S(ulong a, int k)
  {
    uint v = 0; ulong z;
    for (var i = k; i < a0.Length && (z = M(a, a0[i])) != 0 ; i++)
      v += (uint)z + S(z, i+1);
    return v;
  }

  static void Main()
  {
    Init(); uint v = 0; ulong w, z;
    for (var i = 0; i < a2.Length; i++)
      for (var j = 0; j < a3.Length && (w = M(a2[i], a3[j])) != 0; j++)
        for (var k = 0; k < a5.Length && (z = M(w, a5[k])) != 0; k++)
          v += (uint)z + S(z, 0);
    Console.WriteLine(v);
  }
}

