using System;
using System.Collections.Generic;

static class E682
{
  static void Main()
  {
    int m = 7 + (int)1e9, n = (int)1e3;
    long z = 0; int nn = n / 5;
    var p = new int[3];
    var set = new HashSet<(int,int)>();
    for (int d = 0; d+d+d <= nn; d++)  // 5d <=> 6b+c:  5,5,5 -> 2,2,2,2,2,2,3
      for (int a = 0; d+d+d+a <= nn; a += 2) // 10a <=> 2(b+c) // 5     -> 2,3
        for (int b = 0; d+d+d+a+b+b <= nn; b += 2)       // 5,5   -> 2,2,2,2,2
          for (int c = 0; d+d+d+a+b+b+c+c+c <= nn; c++) {// 5,5,5 -> 3,3,3,3,3
            p[0] = a+5*b+6*d; p[1] = a+5*c+d;
            if (!set.Add((p[0], p[1]))) continue;
            p[2] = (n-2*p[0]-3*p[1])/5;
Console.Write("{0,2} {1,2} {2,2} {3,2}: ",d,a,b,c);
Console.Write("{0,2} {1,2} {2,2}: {3,2} {4,2}: ",
p[0],p[1],p[2],(p[0]+p[1]+p[2])/2,2*p[0]+3*p[1]+5*p[2]);
            Array.Sort(p);
            long z0 = (p[0] + 1L) * (p[1] + 1L);
            int s = (p[0] + p[1] - p[2]) / 2;
            if (s > 0) z0 -= s * (s + 1L);
            z = (z + z0) % m;
Console.WriteLine("{0,2}", z0);
          }
    Console.WriteLine("f({0}) = {1}", n, z);
  }
}
// 1e2:     3629
// 1e3: 25808429
// 1e4:  9403972
