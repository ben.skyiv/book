import Math.NumberTheory.Primes.Factorisation ( τ )

main = print $ head $ filter ((>500).τ) $ map (\n -> div (n*n+n) 2) [1..]

