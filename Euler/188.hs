import Math.NumberTheory.Powers ( powerMod )
main = print $ foldl (\x _ -> powerMod 1777 x $ 10^8) 1 [1..1855]

