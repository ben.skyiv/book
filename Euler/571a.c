#include <stdio.h>

const int max = 12;

int next_perm(char s[], int len)
{ // return: 1:success, 0:fail
  int i = len - 1; while (s[i] >= s[i+1]) i--;
  if (i == 0) return 0;
  int j = len; while (s[i] >= s[j]) j--;
  char t = s[i]; s[i] = s[j]; s[j] = t;
  for (int a = i+1, b = len; a < b; a++, b--)
    t = s[a], s[a] = s[b], s[b] = t;
  return 1;
}

long to_decimal(const char s[], int len)
{
  long z = 0;
  for (int i = 1; i <= len; i++) z = (s[i] - '0') + z * max;
  return z;
}

int is_pandigital(long z, int base)
{
  static int m[] = {1,2,4,8,0x10,0x20,0x40,0x80,0x100,0x200,0x400,0x800};
  int a = 0;
  for (long t; z > 0; z = t) a |= m[z - (t = z / base) * base];
  return a == m[base] - 1;
}

int is_super_pandigital(long z)
{
  for (int i = max - 1; i >= 5; i--) if (!is_pandigital(z, i)) return 0;
  return 1;
}

int main(void)
{
  static char a[] = ".10023456789:;";
  int len = sizeof(a) / sizeof(a[0]) - 2;
  long sum = 0;
  for (int n = 0; ; ) {
    long z = to_decimal(a, len);
    if (is_super_pandigital(z)) {
      n++, sum += z;
      printf("%3d: %s %ld%s\n", n, a, z, is_pandigital(z, 4) ? "" : " !4");
    }
    if (!next_perm(a, len)) break;
  }
  printf("%ld\n", sum);
  return 0;
}

