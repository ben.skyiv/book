import Math.NumberTheory.Powers.Squares ( integerSquareRoot' )

main = let n = 10^9 in print $ (6*n + 3) * div n 4 - n + 2 + 4 * let
  x = div (n*n) 32 - 1; a = integerSquareRoot' (div x 2); b = integerSquareRoot' x
  in a*a + b + 2 * sum [integerSquareRoot' (x - i*i) | i <- [a+1..b]]

