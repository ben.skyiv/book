#include <stdio.h>
#include <math.h>

const int MAX = (int)5e3 / 2;
static int sf[MAX];

int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }
int isSquare(long n) { long x = (long)sqrt(n); return x * x == n; }

int square_free(int a)
{
  for (int k, j = a % 2 + 1, i = j + 1; (k = i * i) <= a; )
    if (a % k == 0) a /= k; else i += j;
  return a;
}

int count(int a, int b0, int b9)
{ // count: b in (b0, b9]; b,ab != k^2
  a = sf[a];
  return b9 - (int)sqrt(b9) - (int)sqrt(b9/a)
       - b0 + (int)sqrt(b0) + (int)sqrt(b0/a);
}

int count4a(int k, int a, int b, int c9)
{ // count: c in (0, c9],  a | bc; ac,bc != k^2
  int c1 = c9 / k, c2 = c9 / sf[k];
  return c1 - (int)sqrt(c2 / sf[a])
            - (int)sqrt(c2 / sf[b]);
}

int count4(int a, int b, int c0, int c9)
{ // count: c in (c0, c9], a | bc; ac,bc != k^2
  int k = a / gcd(a, b);
  return count4a(k, a, b, c9) - count4a(k, a, b, c0);
}

long f2a(int n)
{ // i + √b: 0 < i, 1 < b != k^2
  long z = 0;    // i^2 + b <= n
  for (int b, i = 1; 1 < (b = n - i * i); i++)
    z += b - (int)sqrt(b);
  return z;
}

long f2b(int n)
{ // √a + √b: 1 < a < b; a,b,ab != k^2
  long z = 0;            // a + b <= n
  for (int b, a = 2; a < (b = n - a); a++)
    if (!isSquare(a)) z += count(a, a, b);
  return z;
}

long f4(int n)
{ // -√a + √b + √c + √d: ad = bc;     ab,ac,bc != k^2
  long e, z = 0; // 0 < a < b < c, a + b + c + d <= n
  for (int a = 1; (a << 2) + 6 <= n; a++)
    for (int c, b = a+1; b < (c = (long)a*(n-a-b)/(a+b)); b++)
      if (!isSquare((long)a * b)) z += count4(a, b, b, c);
  return z;
}

int main()
{ // TODO: F(5e3): 10,988,134
  for (int i = 1; i < MAX; i++) sf[i] = square_free(i);
  int n = MAX * 2;          printf("%d ",   n);
  long z = f2a(n) + f2b(n); printf("%ld ",  z);
  z += f4(n);               printf("%ld\n", z);
} // n:                F2                F
// 5e3:         6,239,252       11,134,074
// 5e4:       624,864,969    1,333,490,943
// 5e5:    62,498,374,805  155,264,187,822
// 5e6: 6,249,980,999,760

