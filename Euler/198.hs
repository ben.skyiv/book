main = let x = 100; q = 10^8 in print $ sum (takeWhile
  (>0) [f q (n+1) n 0 | n <- [x..]]) + div (q-x) 2 where
  f q i j k = if 2*i*j > q then k else f q (i+j) j $ f q i (i+j) $ k+1

