using System;
using System.Collections.Generic;

static class E719
{
  static IEnumerable<long> SplitSums(long n)
  {
    for (long m = 10; m <= n; m *= 10) {
      yield return n % m + n / m;
      foreach (var i in SplitSums(n / m))
        yield return n % m + i;
    }
  }

  static bool IsSquareSNumber(int n)
  {
    foreach (var i in SplitSums((long)n * n))
      if (i == n) return true;
    return false;
  }

  static void Main()
  {
    int n = (int)1e6;
    long z = 0;
    for (var i = 9; i <= n; i += 9) {
      if (IsSquareSNumber(i)) z += (long)i * i;
      if (IsSquareSNumber(i+1)) z += (long)(i+1) * (i+1);
    }
    Console.WriteLine("T({0:N0}) = {1:N0}", (long)n * n, z);
  }
}
