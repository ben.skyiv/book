main = let m = 904961; n = 10^12 in print $ -- odd m, even n
  (*(m+1)) $ sum $ takeWhile (>0) $ iterate (`div` 2) (div n 4)

