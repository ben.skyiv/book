// mcs 660b18.cs 660.cs && ./660b18.exe

using System;

static class E660b18
{
  static E660 e = new E660(18);

  /*static int P666()
  { // (1xxxxx, 2xxxxx, xxxxxx) (1xxxxx, Gxxxxx, xxxxxx)
    var z = 0;
    for (var i1 = 1; i1 <= e.B / 2; i1++)
      for (var i2 = i1+1; i2 <= e.B - i1; i2++)
        foreach (var s in E660.Permutation(e.GetRest(i1, i2), 10))
          z += e.F(e.B*(e.B*(e.B*(e.B*(e.B*i1+s[1])+s[2])+s[3])+s[4])+s[5],
                   e.B*(e.B*(e.B*(e.B*(e.B*i2+s[6])+s[7])+s[8])+s[9])+s[10]);
    return z;
  }*/

  static int P666a()
  { // (1xx, 2xx, xxx) (1xx, 7xx, xxx) (3xx, 5xx, xxx)
    var z = 0;
    for (var i1 = 1; i1 <= e.B/2 + 1; i1++)
      for (var i2 = 1+Math.Max(i1, e.B-i1); i2 <= Math.Min(e.B-i1+2,e.B-2); i2++) {
        Console.Write("({0},{1})", i1, i2);
        foreach (var s in E660.Permutation(e.GetRest(i1, i2), 10))
          z += e.F(e.B*(e.B*(e.B*(e.B*(e.B*i1+s[1])+s[2])+s[3])+s[4])+s[5],
                   e.B*(e.B*(e.B*(e.B*(e.B*i2+s[6])+s[7])+s[8])+s[9])+s[10]);
      }
    return z;
  }

  /*static int P567()
  { // (xxxxx, Hxxxxx, 10xxxxx)
    var z = 0;
    foreach (var s in E660.Permutation(e.GetRest(0, 1, e.B-1), 10))
      z += e.F(      e.B*(e.B*(e.B*(e.B*s[1] +s[2])+s[3])+s[4])+s[5],
        e.B*(e.B*(e.B*(e.B*(e.B*(e.B-1)+s[6])+s[7])+s[8])+s[9])+s[10]);
    return z;
  }*/

  static void Main() { Console.WriteLine(P666a()); }
}

