import Data.Digits ( digitsRev )
import Math.NumberTheory.GCD ( extendedGCD )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )

main = let n = 10^6 in print $ let ps = sieveFrom 5 in sum $ map (\(p,q)
  -> let a = 10^(length $ digitsRev 10 p); (_,u,_) = extendedGCD a q in
  p + a * mod (u*(q-p)) q) $ takeWhile ((<=n) . fst) $ zip ps $ drop 1 ps

