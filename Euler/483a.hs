import Data.Ratio ( (%) )
f n = product [2..n]
combin n r = if r > n-r then combin n (n-r) else div (product [n-r+1..n]) $ f r
r z = let m = length z; a = head z; b = z !! 1 in
  if m == 2 then combin (a+b) b * f (a-1) * f (b-1) % if a == b then 2 else 1
  else foldl (\s (i,x) -> if a == x then s/(i%(i-1)) else s)
  (combin (sum z) a * f (a-1) % 1 * r (tail z)) $ zip [2..] $ tail z
t 2 n k = [[i,j] | i <- [k .. div n 2], j <- [i..n-i]]
t l n k = concat [map (i:) $ t (l-1) (n-i) i | i <- [k .. div n l]]
g n = let m = f n in sum $ concat [[fromRational $ 
  (sum [combin i (k-1) * f (k-1) | i <- [k-1..n-1]]) * k * k % m | k <- [2..n]],
  map (\z -> fromRational $ (foldl1 lcm z)^2 * combin n (sum z) % m * r z) $
  concat $ takeWhile (not . null) [t l n 2 | l <- [2..]], [fromRational $ 1%m]]
main = let n = 20 in print $ g n

