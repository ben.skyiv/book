using System;
using System.Collections.Generic;

sealed partial class E548
{
  static void Main(string[] args)
  {
    var n = (args.Length > 0) ? int.Parse(args[0]) : 4; // g(a^n*b^2)
    int[] a = new int[n], b1 = new int[n+1], b2 = new int[n];
    a[0] = 2; b2[0] = (b1[0] = 3) * b1[0];
    for (var i = 1; ; i++) {
      b1[i] = b1[i-1] * 2;
      if (i >= n) break;
      b2[i] = b2[i-1] * 2;
      a [i] = a [i-1] * 2;
    }
    var c = new HashSet<Gozinta>();
    Gozinta.Add(c, b1);
    Make1(c, a , b1, 1, n);
    Make1(c, b1, b2, 0, n);
    Make2(c, a, b1, b2, n-1);
    Console.WriteLine("g(a^{0}*b^2) = {1}", n, c.Count);
  }
}

