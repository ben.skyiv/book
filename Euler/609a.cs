using System;
using Skyiv.Utils;

static class E609
{
  static void Main()
  {
    int n = (int)1e8, m = 1000000007;
    int[] ps = new int[64], a = Primes.GetPrimes(0, n);
    for (var i = 0; i < a.Length; i++)
      for (int c = 0, d = ((i < a.Length-1) ? (a[i+1]-1) : n) - a[i],
        j = i; (j = (j < 0) ? ~j : (j+1)) > 0; ps[c]++, ps[c+1] += d)
        if ((j = Array.BinarySearch(a, j)) < 0) c++;
    long z = 1;
    foreach (var p in ps) if (p != 0) z = (z * p) % m;
    Console.WriteLine(z);
  }
}

