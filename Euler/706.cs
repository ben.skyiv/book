using System;

static class E706
{
  static int F(int n)
  {
    int z = 0;
    var s = n.ToString();
    for (int u, i = 0; i < s.Length; i++) {
      if ((u = s[i] - '0') % 3 == 0) z++;
      for (int j = i + 1; j < s.Length; j++)
        if ((u += s[j] - '0') % 3 == 0) z++;
    }
    return z;
  }

  static bool F(int i, int j, int k, int r)
  {
    return F(k + r * (j + 10 * i)) % 3 == 0;
  }

  static int F(int i, int j)
  {
    int v = 0, r = 10000000;
    for (int k = 0; k < r; k++) if(F(i, j, k, r)) v++;
    return v;
  }

  static void Main()
  {
    int z = 0;
    for (int i = 1; i <= 9; i++) {
      int rowSum = 0;
      for (int v, j = 0; j <= 9; j++) {
        rowSum += v = F(i, j);
        Console.Write("{0,2} ", v);
      }
      z += rowSum;
      Console.WriteLine(": {0}", rowSum);
    }
    Console.WriteLine(z);
  }
}
