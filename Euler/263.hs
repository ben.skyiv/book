import Data.List ( zip4 )
import Math.NumberTheory.Primes.Sieve ( primes )
import Math.NumberTheory.Primes.Factorisation ( divisorSum,factorise )

main = print $ sum $ take 4 $ map (\(a,_,_,_) -> a+9) $ filter
  (\(a,b,c,d) -> d-c == 6 && c-b == 6 && b-a == 6 && all (\n ->
  let xs = concatMap (\(a,b) -> replicate b a) $ factorise n in all
  (\(n,p) -> p-1 <= divisorSum (product $ take n xs)) $ zip [1..] $ tail xs)
  [a+1,b-1..d-1]) $ zip4 primes (tail primes) (drop 2 primes) $ drop 3 primes

