#include <iostream>
#include <unordered_map>

const int m = (int)1e9 + 7, n = (int)1e3;
static int a[((n+1)>>1) + (n>>2)];
static std::unordered_map<long,int> d;

int f(int a, int b) { return ((b+1)>>1)+(b>>2)-((a+1)>>1)-(a>>2); }

int g0(int j, int n)
{
  int z = 0;
  for (int v, i = j; (v = f(a[i], n-a[i])) > 0; i++) z = (z + v) % m;
  return z;
}

int g(int k, int j, int n)
{
  long x = ((long)k << 48) | ((long)j << 24) | n; auto t = d.find(x);
  if (t != d.end()) return t->second; int z = 0;
  if (k == 0) z = g0(j, n);
  else for (int v, i = j; (v = g(k-1,i+1,n-a[i])) > 0; i++) z = (z+v)%m;
  d.insert(std::pair<long,int>(x, z)); return z;
}

int main()
{
  for (int i = 1, j = 0; i <= n; i++) if ((i & 3) != 2) a[j++] = i;
  int z = f(0, n);
  for (int v, i = 0; (v = g(i, 0, n)) > 0; i++) z = (z + v) % m;
  std::cout << n << ": " << z << std::endl;
}

