comb n k = div (product [n-k+1..n]) $ product [1..k]
units xs n = sum $ map (\(a,b) -> b * comb (n-2) (a-2)) $ take (fromIntegral (n-2)) xs
unitsA = units [(3,4),(4,54),(5,198),(6,264),(7,120)]
unitsB = units [(3,6),(4,76),(5,240),(6,288),(7,120)]
f a b c = c * (c-1) * comb (a+b) a * unitsA c ^ a * unitsB c ^ b
main = print . flip mod (10^8) $ f 25 75 1984

