import Data.List ( nub,permutations )
import Data.Digits ( digits, unDigits )
import Math.NumberTheory.Primes.Sieve ( sieveFrom )
import Math.NumberTheory.Primes.Testing ( isPrime )

main = print $ last $ nub $ concat $ filter (not . null) $ map
  (\n -> [(x,y,z) | x <- f n, y <- f n, z <- f n, x < y && y < z,
  y-x == z-y]) $ takeWhile (<9999) $ sieveFrom 1000 where
  f n = filter isPrime $ map (unDigits 10) $ nub $ filter
    ((/=0) . head) $ permutations $ digits 10 n

