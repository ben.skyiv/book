main = print $ sum $ map read $ foldr (\b a -> [x:zs | (x:xs) <- b, zs <- a,
  xs == take 2 zs, not $ elem x zs]) (f 17) $ map f [1,2,3,5,7,11,13] where
  f n = [[x,y,z] | let s = ['0'..'9'], x <- s, y <- s,
    z <- s, x /= y, x /= z, mod (read [x,y,z]) n == 0]

