import Data.Set ( elems )
import Math.NumberTheory.Primes.Factorisation ( divisors )

main = let p = 75*10^6 in print $
  length $ concat $ map (\(a,xs) -> filter (\(b,c) -> a+b+c <= p) $ filter
  ((>=a) . fst) $ map (\(b,c) -> (div b 2, div c 2)) $ filter (even . fst)
  $ map (\(n,m) -> (m-n,m+n)) $ take (div (length xs) 2) $ zip xs $
  reverse xs) [(a, elems $ divisors $ a*a+1) | a <-[1 .. div p 3]]

