#include <stdio.h>

const long N = 10000000;

int main(void)
{
  static long sum = 0, a[N + 1];
  for (long n = 3; n <= N; n++) {
    sum += a[n] += (n * n + 6) / 12 - (n >> 2) * ((n + 2) >> 2);
    for (long m = n + n; m <= N; m += n) a[m] -= a[n];
  }
  printf("%ld\n", sum);
  return 0;
}
