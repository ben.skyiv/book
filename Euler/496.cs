using System;
using System.Linq;
using System.Collections.Generic;

sealed class E496
{
  List<int>[] Sieve(int max)
  { // pfs[i]:the prime factorisation of i for p <= sqrt(max)
    int limit = (int)Math.Sqrt(max);
    var pfs = new List<int>[max + 1];
    for (int i = 2; i <= limit; i++)
      if (pfs[i] == null)
        for (int j = i + i; j <= max; j += i)
        {
          if (pfs[j] == null) pfs[j] = new List<int>();
          pfs[j].Add(i);
        }
    return pfs;
  }

  List<Tuple<int,int>> GetPrimeFactors(List<int> lst, int n)
  { // return: the prime factorisation of n
    var lst2 = new List<Tuple<int,int>>();
    for (int j = 0, k = 0; j < lst.Count; )
    {
      if(n % lst[j] == 0) { n /= lst[j]; k++; }
      else { lst2.Add(Tuple.Create(lst[j++], k)); k = 0; }
    }
    if (n != 1) lst2.Add(Tuple.Create(n, 1));
    return lst2;
  }

  List<List<T>> GetSubsets<T>(List<T> lst)
  {
    return (from m in Enumerable.Range(0, 1 << lst.Count)
      select (from i in Enumerable.Range(0, lst.Count)
      where (m & (1<<i)) != 0 select lst[i]).ToList()).ToList();
  }
  
  List<List<T>> Transform<T>(List<List<T>> lst)
  { // example: {{2,4,8},{3,9}} => {{2,3},{2,9},{4,3},{4,9},{8,3},{8,9}}
    var lst1 = new List<List<T>>();
    lst1.Add(new List<T>());
    foreach (var lst2 in lst)
    {
      var lst3 = new List<List<T>>();
      foreach (var t in lst2)
        foreach (var lst4 in lst1)
        {
          var lst5 = new List<T>(lst4);
          lst5.Add(t);
          lst3.Add(lst5);
        }
      lst1 = lst3;
    }
    return lst1;
  }
  
  static List<List<int>> Expand(List<Tuple<int,int>> pfs, int n)
  { // example: {(7,2),(23,1)}, 600 => {{7,49,343},{23,529}}
    var lst = new List<List<int>>();
    foreach (var i in pfs)
    {
      var lst2 = new List<int>();
      long z = 1;
      for (var j = 2*i.Item2; j > 0 && (z *= i.Item1) < n; j--) lst2.Add((int)z);
      lst.Add(lst2);
    }
    return lst;
  }
  
  HashSet<int> GetDivisors(List<Tuple<int,int>> pfs, int n)
  { // return divisors of n^2 in (1,n)
    var set = new HashSet<int>();
    foreach (var set1 in Transform(Expand(pfs, n)))
      foreach (var subset in GetSubsets(set1))
      {
        long z = 1;
        foreach (var i in subset) z *= i;
        if (z > 1 && z < n) set.Add((int)z);
      }
    return set;
  }
  
  long Compute(int a, List<int> lst)
  {
    long sum = 0;
    if (lst == null) return 0;
    long a2 = (long)a*a;
    foreach (var b in GetDivisors(GetPrimeFactors(lst, a), a))
      if (a2 / b < a + b + b)
        sum += a;
    return sum;
  }

  long Compute(int n)
  {
    var tasks = Sieve(n);
    long z = 0;
    for (var i = 0; i <= n; i++) z += Compute(i, tasks[i]);
    return z;
  }

  static void Main()
  {
    Console.WriteLine(new E496().Compute(1000000));
  }
}

