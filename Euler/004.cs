using System;

sealed class E004
{
  bool IsPalindrome(int n)
  {
    var n2 = n % 1000 / 10;
    return n / 1000 == (n % 10 * 100 + n2 % 10 * 10 + n2 / 10);
  }
  
  int Run()
  {
    var max = int.MinValue;
    for (int k, i = 999; i >= 900; i--)
      for (var j = i; j >= 900; j--)
        if (IsPalindrome(k = i * j))
          max = Math.Max(max, k);
    return max;
  }

  static void Main()
  {
    Console.WriteLine(new E004().Run());
  }
}
