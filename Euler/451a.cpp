#include <iostream>
#include <numeric>
#include <vector>

int main()
{
  const int m = 20'000'000;
  static int I[m + 1];
  static std::vector<int> divisors[m + 1];
  for (int i = 2; i <= m; I[i++] = 1)
    for (int j = i; j <= m; j += i)
      divisors[j].push_back(i);
  long n;
  for (int x = 2; x < m; x++)
    for (auto p : divisors[x - 1])
      for (auto q : divisors[x + 1])
        if ((n = (long)p * q) > m) break;
        else if (x < n - 1) I[n] = x;
  std::cout << std::accumulate(I+3,I+m+1,0L) << std::endl;
}

