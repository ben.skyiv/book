#include <stdio.h>

int main(void)
{
  static long xs[50], ys[50], ps[50];
  long x = xs[1] = 0, y = ys[1] = ps[1] = 1;
  long n = 1000000000000;
  int i = 2;
  for (long p = 1; p < n*10; i++) {
    long x0 = x, y0 = y;
    xs[i] = x = x0 + y0;
    ys[i] = y = y0 - x0;
    ps[i] = p = p + p;
  }
  x = y = 0;
  for (int d = 0; n > 0; i--) {
    if (n <= ps[i-1]) continue;
    switch (d) {
      case 0: x += xs[i]; y += ys[i]; break;
      case 1: x += ys[i]; y -= xs[i]; break;
      case 2: x -= xs[i]; y -= ys[i]; break;
      case 3: x -= ys[i]; y += xs[i]; break;
    }
    n = ps[i] - n; d = (d + 3) % 4;
  }
  printf("%ld,%ld\n", x, y);
  return 0;
}

