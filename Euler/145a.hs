main = let z = 9 in print $ sum $ map f [1..z] where
  f n | mod n 2 == 0 = 20 * 30 ^ (div n 2 - 1)
      | mod n 4 == 3 = 100 * 500 ^ div n 4
      | otherwise    = 0

