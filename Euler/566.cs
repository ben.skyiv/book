using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Skyiv.Numerics;

static class E566
{
  static readonly string fmt = "MM-dd HH:mm:ss";

  sealed class F
  {
    static readonly double C = 360;
    Real p = 0; Real[] xyz; bool icing = true;
    List<Real> s = new List<Real>(), t = new List<Real>();

    public F(int a, int b, int c)
    {
      xyz = new Real[]{ C / a, C / b, C / Math.Sqrt(c) };
      s.Add(0); s.Add(C);
    }

    void Step(Real v)
    {
      if (p + v > C) Left();
      var c = s.Count; var q = p + v; int i, j = 0;
      while (j < c && q > s[j]) j++;
      for (i = j; i > 0 && p < s[i]; ) i--;
      bool e = (j - i) % 2 == 0, b = p == s[i], d = q == s[j]; t.Clear();
      for (var k = i+1; k < j; k++) t.Add(q-s[k]+p);
      for (var k = i+1; k < j; k++) s[j-k+i+(e?0:b?-1:d?1:0)] = t[k-i-1];
      if (e) return;
      if ( b      )   s[j - 1] = q;
      if (!b &&  d) { s[i + 1] = p;    if (j == c-1) s.Add(C); }
      if ( b &&  d) { s.RemoveAt(j-1); if (j != c-1) s.RemoveAt(j-1);  }
      if (!b && !d) { s.Insert(j, q);                s.Insert(i+1, p); }
      if ( b && i == 0) { s.Insert(0, 0); icing = !icing; }
    }

    void Left()
    {
      var c = s.Count; var i = c - 2;
      while (i > 0 && p < s[i]) i--;
      if (i % 2 == 1) icing = !icing; t.Clear(); t.Add(0);
      for (var k = i+1; k < c - ((c%2==0) ? 1 : 0); k++) t.Add(s[k]-p);
      for (var k = 1; k <= i - ((p==s[i]) ? 1 : 0); k++) t.Add(s[k]-p+C);
      t.Add(C); s.Clear(); var u = s; s = t; t = u; p = 0;
    }

    public int Compute()
    {
      Real v;
      for (var k = 0; k < (int)1e9; p += v) {
        Step(v = xyz[k++ % 3]);
        if (s.Count <= 2) if (icing) return k; else p = 0 - v;
      }
      return 0;
    }
  }
  
  static long G1(int a, int n)
  {
    long z = 0;
    using (var sw = new StreamWriter("tmp/G" + a.ToString("D2"))) {
      sw.WriteLine("{0}: Begin", DateTime.Now.ToString(fmt));
      for (int b = a + 1; b < n; b++)
        for (int c = b + 1; c <= n; c++) {
          var w = new F(a, b, c).Compute();
          sw.WriteLine("{0}: F({1,2},{2,2},{3,2}) = {4,8}|",
            DateTime.Now.ToString(fmt), a, b, c, w);
          sw.Flush();
          z += w;
        }
      sw.WriteLine("{0}: End", DateTime.Now.ToString(fmt));
    }
    return z;
  }

  static void Main()
  {
    long z = 0; int n = 17;
    // for (int a = 9; a < n-1; a++) z += G1(a, n);
    Parallel.For(9, n-1, () => 0L, (a,_,x) => x+G1(a,n),
      x => Interlocked.Add(ref z, x));
    Console.WriteLine("G({0}) = {1}", n, z);
  }
}

