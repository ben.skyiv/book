import Math.NumberTheory.Powers ( powerMod )
f m = all id [a == powerMod a (m+4) m | a <- [2..m-1]]
main = let n = 10^6 in print $
  let xs = [i | i <- [1..n], f i] in (sum xs, xs)
