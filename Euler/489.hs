main = let m = 18; n = 1900 in print $ (0-) $ sum
  [snd . maximum $ [(gcd (n^3+b) ((n+a)^3+b), -n) |
  n <- [0..a*(a^6+27*b*b)]] | a <- [1..m], b <- [1..n]]

