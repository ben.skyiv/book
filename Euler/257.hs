main = let n = 10^8 in print $ div n 3 + f n 3 + f n 2
f n k = sum [div (n * foldl1 gcd s) z | let m = fromIntegral k,
  u <- [1 .. floor $ sqrt $ m*m*(m-1) * fromIntegral n / (m+3)],
  v <- [1 + div u k .. floor $ fromIntegral u / sqrt m],
  gcd u v == 1, let s = [(k-1)*u*v, (k*v+u)*v, u*(u+v)]; z = sum s]

