cache={0:1}
def Q(n):
	if n in cache:
		return cache[n]
	else:
		if (n%2)==0:
			a=Q(n/2)
		else:
			(q,r)=divmod(n,8)
			if r==1:
				a=5*Q(q)
			elif r==3:
				a=2*Q(4*q+1)-3*Q(q)
			elif r==5:
				a=3*Q(2*q+1)+2*Q(q)
			elif r==7:
				a=2*Q(4*q+1)+3*Q(2*q+1)-6*Q(q)
		cache[n]=a
		return a

print(sum([Q(10**k) for k in range(1,19)]))

