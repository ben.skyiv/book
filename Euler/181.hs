import Data.Map.Strict ((!),findWithDefault,insertWith,singleton)

main = let b = 60; w = 40 in print $ (foldl (\m (b1,w1,b2,w2) -> insertWith
  (+) (b2,w2) (findWithDefault 0 (b2-b1,w2-w1) m) m) (singleton (0,0) 1)
  [(b1,w1,b2,w2) | b1 <- [0..b], w1 <- [if b1 == 0 then 1 else 0 .. w],
  b2 <- [b1..b], w2 <- [w1..w]]) ! (b,w)

