#include <stdio.h>

int max(int a, int b) { return (a > b) ? a : b; }

int main(void)
{
  long sum = 0;
  for (int k = 1; k <= 1000; k++)
    for (int low = 4*k*k, high = 3*low, x = 1; x*x <= high; x++)
      for (int y = max(low / x + 1, x); x*y <= high; y++) {
        long p = (long)low*(x + y), q = x*y - low, z;
        if (p % q != 0) continue;
        if ((z = p / q) >= y) sum += z + x + y;
      }
  printf("%ld\n", 2 * sum);
  return 0;
} // slow: 12m33.477s

