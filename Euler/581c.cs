using System;
using System.Collections.Generic;

static class E581
{
  static readonly int[] ps = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47};

  static int IndexOfMinValue(long[] a)
  {
    int k = 0; long m = a[0];
    for (int i = a.Length - 1; i > 0; i--) if (m > a[i]) m = a[k = i];
    return k;
  }

  static IEnumerable<long> Smooth()
  {
    yield return 1;
    var a = new long[ps.Length];
    var b = new Queue<long>[a.Length];
    for (var i = a.Length - 1; i >= 0; i--)
      (b[i] = new Queue<long>()).Enqueue(a[i] = ps[i]);
    for (int k; ; ) {
      var x = a[k = IndexOfMinValue(a)];
      yield return x; b[k].Dequeue();
      for (var i = a.Length - 1; i >= k; i--) b[i].Enqueue(x * ps[i]);
      a[k] = b[k].Peek();
    }
  }

  static void Main()
  {
    var r = Smooth().GetEnumerator(); r.MoveNext();
    long z = 0, n0 = r.Current; r.MoveNext();
    for (long n; (n = r.Current) <= (long)1e13; n0 = n, r.MoveNext())
      if (n == n0 + 1) z += n0;
    Console.WriteLine(z);
  }
}

