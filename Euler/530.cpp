#include <bits/stdc++.h>

long f(long n)
{
  static std::unordered_map<long, long> a;
  if (a.count(n)) return a[n];
  if (n == 1) return a[n] = 1;
  long z = 0;
  for (long j, i = 1; i <= n; i = j + 1)
    z += ((j = n/(n/i)) - i + 1) * (n/i);
  for (long i = 2; i*i <= n; i++) z -= f(n/(i*i));
  return a[n] = z;
}

int main()
{
  long n = 1e15, z = 0;
  for (long i = 1; i*i <= n; i++) z += i*f(n/(i*i));
  std::cout << z << std::endl;
}

