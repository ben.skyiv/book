using System;
using System.Collections.Generic;

sealed class E628
{
  readonly int n;
  readonly bool[,] a;
  readonly int[] b;

  public E628(int n)
  {
    this.n = n; a = new bool[n+1, n+1]; b = new int[n+1];
    for (var i = 0; i <= n; i++) b[i] = i;
  }

  void MakeAFromB()
  {
    for (var i = 1; i <= n; i++)
      for (var j = 1; j <= n; j++)
        a[i, j] = b[i] != j;
  }

  IEnumerable<int> Next()
  {
    while (true) {
      yield return 0;
      var j = n - 1; while (b[j] >= b[j+1]) j--;
      if (j == 0) break;
      var k = n; while (b[j] >= b[k]) k--;
      var t = b[j]; b[j] = b[k]; b[k] = t; k = j + 1;
      for (int i = n; k < i; k++, i--) { t = b[k]; b[k] = b[i]; b[i] = t; }
    }
  }

  bool IsOpen(int i, int j)
  {
    if (i == 1 && j == 1) return true;
    if (a[i-1, j] && IsOpen(i-1, j)) return true;
    if (a[i, j-1] && IsOpen(i, j-1)) return true;
    return false;
  }

  public long F()
  {
    long z = 0;
    foreach (var _ in Next()) {
      if (b[1] == 1 || b[n] == n) continue;
      MakeAFromB();
      if (IsOpen(n, n)) z++;
    }
    return z;
  }
}

static class Runer
{  
  static void Main()
  {
    for (var i = 1; i <= 10; i++) Console.Write(new E628(i).F() + ",");
    Console.WriteLine();
  }
}

