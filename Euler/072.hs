import Math.NumberTheory.Primes.Factorisation ( φ )

main = let n = 10^6 in print $ sum [φ i | i <- [2..n]]

