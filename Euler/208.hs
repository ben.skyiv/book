import Math.Combinat.Numbers ( binomial )

main = let n = 70; m = div n 5 in print $ div (sum
  [(m^4 - 3*i*m^3 + 4*i*i*m*m - 2*i^3*m + i^4) *
  (binomial m i)^5 | i <- [0..m]]) $ m^4

