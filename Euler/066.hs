import Data.Function ( on )
import Data.Ratio ( numerator )
import Data.List ( (\\),maximumBy )

main = let n = 1000 in print $ fst $ maximumBy (on compare $ numerator
  . snd) $ map (\n -> (n, (\(p,xs) -> foldr1 (\a b -> a + 1/b) $ map
  fromIntegral $ uncurry take $ if even p then (p,xs) else (p+p, xs
  ++ tail xs)) $ f n)) $ [2..n] \\ takeWhile (<=n) (map (^2) [1..])
f n = (1 + length xs, map (\(x,_,_) -> x) (a:b:xs)) where
  g (_,u,v) = let a = div (m+v) b; b = div (n-v*v) u in (a, b, a*b-v)
  m = floor $ sqrt $ fromIntegral n; a = (m,1,m); b = g a
  xs = takeWhile (/=b) $ tail $ iterate g b

