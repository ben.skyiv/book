-- http://en.wikipedia.org/wiki/Farey_sequence
-- 3y - 7x = 1, x = 2, y = 5
main = print $ let d = 1000000; r = div (d - 5) 7 in 2 + 3 * r

