// gcc --std=c11 -O4 -march=native -fopenmp 572a.c
#include <stdio.h>
#include <stdlib.h>

int isIdem(int s[])
{
  return s[0] == s[0]*s[0]+s[1]*s[3]+s[2]*s[6]
    &&   s[1] == s[0]*s[1]+s[1]*s[4]+s[2]*s[7]
    &&   s[3] == s[0]*s[3]+s[3]*s[4]+s[5]*s[6]
    &&   s[4] == s[1]*s[3]+s[4]*s[4]+s[5]*s[7]
    &&   s[6] == s[0]*s[6]+s[3]*s[7]+s[6]*s[8];
}

int c1(int s[], int n)
{
  int e = s[4], f = s[5], g = s[6], h = s[7], i = s[8];
  int z = i * (1 - i) - f * h;
  if (z % g != 0) return 0;
  int c = s[2] = z / g;
  if (c < -n || c > n) return 0;
  z = h * (1 - e - i);
  if (z % g != 0) return 0;
  int b = s[1] = z / g;
  if (b < -n || b > n) return 0;
  if (c != 0) {
    z = f * (1 - e - i);
    if (z % c != 0) return 0;
    int d = s[3] = z / c;
    if (d < -n || d > n) return 0;
    z = c * (1 - i) - b * f;
    if (z % c != 0) return 0;
    int a = s[0] = z / c;
    if (a < -n || a > n) return 0;
    return isIdem(s);
  }
  if (f * (e+i-1) != 0 || b*f + c*i != 0) return 0;
  int v = 0;
  for (int d = -n; d <= n; d++)
    for (int a = -n; a <= n; a++)
      s[3] = d, s[0] = a, v += isIdem(s);
  return v;
}

int c2(int s[], int n)
{
  int e = s[4], f = s[5], h = s[7], i = s[8], v = 0;
  if (f*h+i*(i-1) != 0 || h*(e+i-1) != 0) return 0;
  for (int c = -n; c <= n; c++)
    if (c != 0) {
      int z = f * (1 - e - i);
      if (z % c != 0) continue;
      int d = s[3] = z / c;
      if (d < -n || d > n) continue;
      for (int b = -n; b <= n; b++) {
        z = c * (1 - i) - b * f;
        if (z % c != 0) continue;
        int a = s[0] = z / c;
        if (a < -n || a > n) continue;
        s[1] = b, s[2] = c, v += isIdem(s);
      }
    }
    else
      for (int b = -n; b <= n; b++) {
        if (f * (e+i-1) != 0 || b*f + c*i != 0) continue;
        for (int d = -n; d <= n; d++)
          for (int a = -n; a <= n; a++)
            s[3]=d, s[2]=c, s[1]=b, s[0]=a, v += isIdem(s);
      }
  return v;
}

long g(int n, int i)
{ // fprintf(stderr, "%d ", i);
  long sum = 0;
  int s[9]; s[8] = i;
  for (s[7] = -n; s[7] <= n; s[7]++)
    for (s[6] = -n; s[6] <= n; s[6]++)
      for (s[5] = -n; s[5] <= n; s[5]++)
        for (s[4] = -n; s[4] <= n; s[4]++)
          sum += (s[6] != 0) ? c1(s, n) : c2(s, n);
  return sum;
}

long f(int n)
{
  long sum = 0;
  #pragma omp parallel for reduction(+:sum)
  for (int i = -n; i <= n; i++) sum += g(n, i);
  return sum;
}

int main(int argc, char *argv[])
{ // http://oeis.org/A223455
  int n = (argc < 2) ? 2 : atoi(argv[1]);
  printf("C(%d) = %ld\n", n, f(n));
}

