using System;
using System.Linq;
using System.Collections.Generic;

static class E579
{
  sealed class E<T> : IEqualityComparer<T[]>
  {
    public bool Equals (T[] x, T[] y)
    {
      if (x.Length != y.Length) return false;
      for (var i = 0; i < x.Length; i++)
        if (!x[i].Equals(y[i])) return false;
      return true;
    }

    public int GetHashCode(T[] obj)
    {
      int z = 0;
      foreach (var v in obj) z ^= v.GetHashCode();
      return z;
    }
  }

  static readonly int n = 10;
  static long sq(int x) { return (long)x * x; }

  static HashSet<Tuple<int,int,int>[]> GetAll()
  {
    var set = new HashSet<Tuple<int,int,int>[]>(new E<Tuple<int,int,int>>());
    for (int i0 = 0; i0 <= n; i0++)
    for (int j0 = 0; j0 <= n; j0++)
    for (int k0 = 0; k0 <= n; k0++)
      for (int i1 = i0; i1 <= n; i1++)
      for (int j1 =  0; j1 <= n; j1++)
      for (int k1 =  0; k1 <= n; k1++) {
        var d01 = sq(i0-i1) + sq(j0-j1) + sq(k0-k1);
        if (d01 <= 25) continue;
        if (d01 == 0) continue;
        for (int i2 = 0; i2 <= n; i2++)
        for (int j2 = 0; j2 <= n; j2++)
        for (int k2 = 0; k2 <= n; k2++) {
          var d12 = sq(i1-i2) + sq(j1-j2) + sq(k1-k2);
          if (d12 != d01) continue;
          var d02 = sq(i0-i2) + sq(j0-j2) + sq(k0-k2);
          if (d02 != d01 + d01) continue;
          for (int i3 = 0; i3 <= n; i3++)
          for (int j3 = 0; j3 <= n; j3++)
          for (int k3 = 0; k3 <= n; k3++) {
            var d23 = sq(i2-i3) + sq(j2-j3) + sq(k2-k3);
            if (d23 != d01) continue;
            var d03 = sq(i0-i3) + sq(j0-j3) + sq(k0-k3);
            if (d03 != d01) continue;
            var d13 = sq(i1-i3) + sq(j1-j3) + sq(k1-k3);
            if (d13 != d02) continue;
            for (int i4 = 0; i4 <= n; i4++)
            for (int j4 = 0; j4 <= n; j4++)
            for (int k4 = 0; k4 <= n; k4++) {
              var d04 = sq(i0-i4) + sq(j0-j4) + sq(k0-k4);
              if (d04 != d01) continue;
              var d14 = sq(i1-i4) + sq(j1-j4) + sq(k1-k4);
              if (d14 != d02) continue;
              var d34 = sq(i3-i4) + sq(j3-j4) + sq(k3-k4);
              if (d34 != d02) continue;
              var d24 = sq(i2-i4) + sq(j2-j4) + sq(k2-k4); // delete?
              if (d24 != d04 + d02) continue;             // delete?
              for (int i5 = 0; i5 <= n; i5++)
              for (int j5 = 0; j5 <= n; j5++)
              for (int k5 = 0; k5 <= n; k5++) {
                var d45 = sq(i4-i5) + sq(j4-j5) + sq(k4-k5);
                if (d45 != d01) continue;
                var d15 = sq(i1-i5) + sq(j1-j5) + sq(k1-k5);
                if (d15 != d01) continue;
                var d05 = sq(i0-i5) + sq(j0-j5) + sq(k0-k5);
                if (d05 != d02) continue;
                var d25 = sq(i2-i5) + sq(j2-j5) + sq(k2-k5); // delete?
                if (d25 != d02) continue;                   // delete?
                var d35 = sq(i3-i5) + sq(j3-j5) + sq(k3-k5); // delete?
                if (d35 != d24) continue;                   // delete?
                for (int i6 = 0; i6 <= n; i6++)
                for (int j6 = 0; j6 <= n; j6++)
                for (int k6 = 0; k6 <= n; k6++) {
                  var d56 = sq(i5-i6) + sq(j5-j6) + sq(k5-k6);
                  if (d56 != d01) continue;
                  var d26 = sq(i2-i6) + sq(j2-j6) + sq(k2-k6);
                  if (d26 != d01) continue;
                  var d16 = sq(i1-i6) + sq(j1-j6) + sq(k1-k6);
                  if (d16 != d02) continue;
                  var d46 = sq(i4-i6) + sq(j4-j6) + sq(k4-k6); // delete?
                  if (d46 != d02) continue;                   // delete?
                  var d36 = sq(i3-i6) + sq(j3-j6) + sq(k3-k6); // delete?
                  if (d36 != d02) continue;                   // delete?
                  var d06 = sq(i0-i6) + sq(j0-j6) + sq(k0-k6); // delete?
                  if (d06 != d24) continue;                   // delete?
                  for (int i7 = 0; i7 <= n; i7++)
                  for (int j7 = 0; j7 <= n; j7++)
                  for (int k7 = 0; k7 <= n; k7++) {
                    var d67 = sq(i6-i7) + sq(j6-j7) + sq(k6-k7);
                    if (d67 != d01) continue;
                    var d37 = sq(i3-i7) + sq(j3-j7) + sq(k3-k7);
                    if (d37 != d01) continue;
                    var d47 = sq(i4-i7) + sq(j4-j7) + sq(k4-k7);
                    if (d47 != d01) continue;
                    var d57 = sq(i5-i7) + sq(j5-j7) + sq(k5-k7); // delete?
                    if (d57 != d02) continue;                   // delete?
                    var d27 = sq(i2-i7) + sq(j2-j7) + sq(k2-k7); // delete?
                    if (d27 != d02) continue;                   // delete?
                    var d07 = sq(i0-i7) + sq(j0-j7) + sq(k0-k7); // delete?
                    if (d07 != d02) continue;                   // delete?
                    var d17 = sq(i1-i7) + sq(j1-j7) + sq(k1-k7); // delete?
                    if (d17 != d24) continue;                   // delete?
                    var a = new Tuple<int,int,int>[8];
                    a[0] = Tuple.Create(i0, j0, k0);
                    a[1] = Tuple.Create(i1, j1, k1);
                    a[2] = Tuple.Create(i2, j2, k2);
                    a[3] = Tuple.Create(i3, j3, k3);
                    a[4] = Tuple.Create(i4, j4, k4);
                    a[5] = Tuple.Create(i5, j5, k5);
                    a[6] = Tuple.Create(i6, j6, k6);
                    a[7] = Tuple.Create(i7, j7, k7);
                    Array.Sort(a);
                    set.Add(a);
                  }
                }
              }
            }
          }
        }
      }
    return set;
  }
  
  static HashSet<Tuple<int,int,int>[]> Filter(HashSet<Tuple<int,int,int>[]> set)
  {
    var set2 = new HashSet<Tuple<int,int,int>[]>();
    foreach (var a in set) {
      var xs = new int[n + 1];
      foreach (var t in a) xs[t.Item1]++;
      if (xs.Max() <  3) set2.Add(a);
    }
    return set2;
  }

  static void Main()
  { // http://oeis.org/A098928
    var set = Filter(GetAll());
    foreach (var a in set) {
      foreach (var t in a) Console.Write("{0} ", t);
      Console.WriteLine();
    }
  }
}

