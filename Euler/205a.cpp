#include <iostream>
#include <algorithm>
#include <iomanip>
using namespace std;

int main()
{
	uint64_t cubic_colin[37]{};
	#define loop(n, x) for( unsigned i##n = 1; i##n != x + 1; ++i##n )
	loop(1, 6) loop(2, 6) loop(3, 6)
	loop(4, 6) loop(5, 6) loop(6, 6)
		++cubic_colin[i1+i2+i3+i4+i5+i6];
	uint64_t pyra_pete[37]{};
	loop(1, 4) loop(2, 4) loop(3, 4)
	loop(4, 4) loop(5, 4) loop(6, 4)
	loop(7, 4) loop(8, 4) loop(9, 4)
		++pyra_pete[i1+i2+i3+i4+i5+i6+i7+i8+i9];
	uint64_t pete_wins = 0, total = 0;
	for( size_t c = 6; c != 37; ++c )
	for( size_t p = 9; p != 37; ++p )
	{
		auto amount = cubic_colin[c] * pyra_pete[p];
		if( c < p )
			pete_wins += amount;
		total += amount;
	}
	cout << setprecision(7) << fixed << double(pete_wins)/total;
}

