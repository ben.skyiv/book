import Data.List ( sort )
import Data.Digits ( digitsRev )

main = let n = 30 in print $ sort [a | m <- [2..100], n <- [2..9],
  let a = m^n, (sum $ digitsRev 10 a) == m] !! (n-1)

