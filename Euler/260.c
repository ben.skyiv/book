#include <stdio.h>

int main()
{
  const int n = 1000;
  static int a[n+1][n+1], b[n+1][n+1], c[n+1][n+1], v = 0;
  for (int x = 0; x <= n; x++)
    for (int y = x; y <= n; y++)
      for (int z = y; z <= n; z++)
        if (!(a[y-x][z-y] || b[x][z-y] || b[y][z-x] || b[z][y-x]
          || c[x][y] || c[x][z] || c[y][z]))
          a[y-x][z-y] = b[x][z-y] = b[y][z-x] = b[z][y-x]
            = c[x][y] = c[x][z] = c[y][z] = 1, v += x + y + z;
  printf("%d\n", v);
}

