\\ f(n)=sum(i=1,n,if(sigma(i,0)%6==1,1,0))
f=(n)->ZM=Map();sum(a=1,sqrtnint(n,6),MP(sqrtnint(n\a^6,4)))
MP=(k)->(S(k)+M(k))/2
M=(k)->my(x,K=sqrtint(k));\
if(k==1,1,mapisdefined(ZM,k,&x),x,my(r=1-(sum(i=2,K,M(k\i))\
+sum(j=1,K,M(j)*(k\j-max(K,k\(j+1))))));mapput(ZM,k,r);r)
S=(n)->sum(i=1,sqrtint(n),moebius(i)*(n\i^2))
print(f(10^36))
quit()

