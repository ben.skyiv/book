import Data.Ratio ( (%) )

main = let n = 12345 in print $ (\x -> let z = (x-1)*n+2 in z*z-z) $ fst
  $ head $ dropWhile ((>=1%n) . snd) [(t, t % (2^t - 1)) | t <- [1..]]

