import Math.NumberTheory.Primes.Counting ( primeCount )

main = let n = 44 in print $ (+) 3 $ sum $ map
  (\n -> let z = div n 2 in n*(z-2) - z*(z-1) +
  sum (map primeCount [n,n-2]) + 2) $ 
  take (n-4) $ map fst $ iterate (\(a,b)->(b,a+b)) (5,8)

