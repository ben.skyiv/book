﻿using System.Diagnostics;

namespace GameOfLife;

public partial class MainPage : ContentPage
{
    const int MaxCellSize = 20;     // includes cell spacing
    const int CellSpacing = 2;

    // Generating too many BoxView elements can impact performance, particularly on iOS devices.
    const int MaxCellCount = 1000;

    // Calculated during SizeChanged event 
    int cols, rows, cellSize, xMargin, yMargin;
    GameGrid gameGrid = new GameGrid();
    List<(int,int)> gridData = new List<(int, int)>();
    bool isRunning, isFirst = true;

    public MainPage()
    {
        LoadMauiAsset();
        InitializeComponent();
    }

    async Task LoadMauiAsset()
    {
        MyDebug("LoadMauiAsset(): begin");
        using var stream = await FileSystem.OpenAppPackageFileAsync("Life01.txt");
        using var reader = new StreamReader(stream);
        for (int y = 0; ; y++) {
            var s = reader.ReadLine();
            if (s == null) break;
            for (var x = 0; x < s.Length; x++)
                if (s[x] == 'x') gridData.Add((x, y));
        }
        MyDebug("LoadMauiAsset(): end");
    }

    void InitGrid()
    {
        gameGrid.Clear();
        foreach (var (x, y) in gridData)
            if (x < cols && y < rows) gameGrid.SetStatus(x, y, true);
        UpdateLives();
        MyDebug("InitGrid()");
    }

    void OnLayoutSizeChanged(object sender, EventArgs args)
    {
        var layout = (Layout)sender;
        cols = (int)Math.Round(layout.Width / MaxCellSize);
        rows = (int)Math.Round(layout.Height / MaxCellSize);
        if (cols * rows > MaxCellCount)
        {
            cellSize = (int)Math.Sqrt((layout.Width * layout.Height) / MaxCellCount);
            cols = (int)(layout.Width / cellSize);
            rows = (int)(layout.Height / cellSize);
        }
        else
            cellSize = (int)Math.Min(layout.Width / cols, layout.Height / rows);
        xMargin = (int)((layout.Width - cols * cellSize) / 2);
        yMargin = (int)((layout.Height - rows * cellSize) / 2);
        if (cols > 0 && rows > 0)
        {
            gameGrid.SetSize(cols, rows);
            UpdateLayout();
            UpdateLives();
        }
    }

    void UpdateLayout()
    {
        int count = rows * cols;
        MyDebug("Grid cells count: " + count);

        // Remove unneeded LifeCell children
        while (absoluteLayout.Children.Count > count)
            absoluteLayout.Children.RemoveAt(0);

        // Possibly add more LifeCell children
        while (absoluteLayout.Children.Count < count)
        {
            var gameCell = new GameCell();
            gameCell.Tapped += OnTapGestureTapped;
            absoluteLayout.Children.Add(gameCell);
        }

        int index = 0;
        for (int x = 0; x < cols; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                var gameCell = (GameCell)absoluteLayout.Children[index];
                gameCell.Col = x;
                gameCell.Row = y;
                gameCell.IsAlive = gameGrid.IsAlive(x, y);
                var rect = new Rect(
                    x * cellSize + xMargin + CellSpacing / 2,
                    y * cellSize + yMargin + CellSpacing / 2,
                    cellSize - CellSpacing,
                    cellSize - CellSpacing);
                AbsoluteLayout.SetLayoutBounds(gameCell, rect);
                index++;
            }
        }
        
        if (!isFirst) return;
        isFirst = false;
        InitGrid();
    }

    void UpdateLives()
    {
        foreach (View view in absoluteLayout.Children)
        {
            var gameCell = (GameCell)view;
            gameCell.IsAlive = gameGrid.IsAlive(gameCell.Col, gameCell.Row);
        }
    }

    void OnTapGestureTapped(object sender, EventArgs args)
    {
        var gameCell = (GameCell)sender;
        gameCell.IsAlive ^= true;
        gameGrid.SetStatus(gameCell.Col, gameCell.Row, gameCell.IsAlive);
    }

    void OnRunButtonClicked(object sender, EventArgs args)
    {
        if (!isRunning)
        {
            gameGrid.Wrap = wrapCheckBox.IsChecked;
            runButton.Text = "暂停";
            isRunning = true;
            clearButton.IsEnabled = resetButton.IsEnabled = wrapCheckBox.IsEnabled = false;
            Dispatcher.StartTimer(TimeSpan.FromMilliseconds(250), OnTimerTick);
        }
        else StopRunning();
    }

    void StopRunning()
    {
        isRunning = false;
        runButton.Text = "开始";
        clearButton.IsEnabled = resetButton.IsEnabled = wrapCheckBox.IsEnabled = true;
        wrapCheckBox.IsChecked = !wrapCheckBox.IsChecked;
        wrapCheckBox.IsChecked = !wrapCheckBox.IsChecked;
    }

    bool OnTimerTick()
    {
        if (isRunning)
        {
            bool isLifeLeft = gameGrid.Tick();
            UpdateLives();
            if (!isLifeLeft) StopRunning();
        }
        return isRunning;
    }

    void OnClearButtonClicked(object sender, EventArgs args)
    {
        gameGrid.Clear();
        UpdateLives();
    }
    void OnResetButtonClicked(object sender, EventArgs args)
    {
        InitGrid();
    }

    async void OnAboutButtonClicked(object sender, EventArgs args)
    {
        await Navigation.PushModalAsync(new AboutPage());
    }

    void MyDebug(string message)
    {
        Debug.WriteLine(string.Format("{0:HH:mm:ss}: MyDebug: {1}", DateTime.Now, message));
    }
}
