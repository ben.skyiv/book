﻿namespace GameOfLife;

public partial class AboutPage : ContentPage
{
    public AboutPage()
    {
        InitializeComponent();
        SystemInfoLabel.Text = string.Format("CLR {0} on {1}",
            Environment.Version, Environment.OSVersion);
    }

    async void OnHyperlinkTapped(object sender, EventArgs args)
    {
        Label label = (Label)sender;
        await Launcher.OpenAsync(label.Text);
    }

    async void OnCloseButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PopModalAsync();
    }
}
