namespace PushBox;

public partial class AboutPage : ContentPage
{
	public AboutPage()
	{
		InitializeComponent();
		var md = DeviceDisplay.Current.MainDisplayInfo;
        SystemInfoLabel.Text = $"CLR {Environment.Version} on {Environment.OSVersion}";
		DisplayLabel.Text = $"屏幕(宽x高): {md.Width}x{md.Height}";
	}
}