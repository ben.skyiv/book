﻿using MiniExcelLibs;
using MiniExcelLibs.OpenXml;

/*
var path = "/home/ben/git/MiniExcel/samples/xlsx/TestStartCellQuery.xlsx";
using (var reader = MiniExcel.GetReader(path, useHeaderRow:true, startCell:"B3"))
while (reader.Read())
{
  for (var i = 0; i < reader.FieldCount; i++) Console.Write(reader.GetValue(i) + "|");
  Console.WriteLine();
}

var rows = MiniExcel.Query(path, useHeaderRow:true, startCell:"B3");
foreach (var row in rows) {
  foreach (var col in row) Console.Write(col); Console.WriteLine();
  //Console.WriteLine("{0}|{1}|{2}", row.Name, row.Dept, row.Age);
}
*/

var data = new[] {
    new { 项目 = "MiniExcel", 值 = 1 },
    new { 项目 = "这是一个很长的更，用于测试最合适的列宽",值 = 2},
};
var config = new OpenXmlConfiguration() { AutoFilter = false };
MiniExcel.SaveAs("a.xlsx", data, overwriteFile:true, configuration:config);
