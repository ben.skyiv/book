﻿using System;

static class Program
{
  static void Main()
  {
    Console.WriteLine("CLR : " + Environment.Version);
    Console.WriteLine("Main: " + Num());
  }
  
  static int Num()
  {
    var i = 10;
    try { return i; }
    finally { i = 11; Console.WriteLine($"i={i}"); }
  }
}
