﻿using System;
using System.IO;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

static class Program
{
    public static void Main (string[] args)
    {
        var message = new MimeMessage();
        message.From.Add(new MailboxAddress("黄志斌", "skyivben@qq.com"));
        message.To.Add (new MailboxAddress("银河", "skyivben@126.com"));
        message.Subject = "带有附件的邮件";
        var body = new TextPart("plain") { Text = @"附件中包含一个zip文件。" };
        var path = "/home/ben/repo/ctf-tools1/Misc/crc32工具.zip";
        var type = MimeMapping.MimeUtility.GetMimeMapping(path).Split("/");
        Console.WriteLine("{0}/{1}", type[0], type[1]);
        var attachment = new MimePart(type[0], type[1]) {
            Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
            ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
            ContentTransferEncoding = ContentEncoding.Base64,
            FileName = Path.GetFileName(path)
        };
        var multipart = new Multipart("mixed");
        multipart.Add(body);
        multipart.Add(attachment);
        message.Body = multipart;
        using (var client = new SmtpClient()) {
            client.Connect("smtp.qq.com", 587, false);
            client.Authenticate("skyivben@qq.com", "hzfjxeeujnmkcbce");
            client.Send(message);
            client.Disconnect(true);
        }
    }
}
