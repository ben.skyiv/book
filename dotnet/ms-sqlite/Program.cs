﻿using System;
using Microsoft.Data.Sqlite;

static class Program
{
  static void Main(string[] args)
  {
    Console.WriteLine("OS    : " + Environment.OSVersion);
    Console.WriteLine("CLR   : " + Environment.Version);
    using var con = new SqliteConnection("Data Source=:memory:");
    con.Open();
    using var cmd = new SqliteCommand("SELECT SQLITE_VERSION()", con);
    Console.WriteLine("SQLite: " + cmd.ExecuteScalar());    
  }
}
