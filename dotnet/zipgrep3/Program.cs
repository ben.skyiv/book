﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

static class Program
{
  static readonly string pre = "e-";
  static readonly Encoding Encode = Encoding.GetEncoding("GB18030");
  static readonly string workDirectory = "/home/ben/Documents/ICBC/data/";
  static readonly string[] dirs = { "e-20010609-20011231" } ; //{ "a-2001-08" };
  static readonly string[] bins = { "nfbalan", "nfcjnl" };
  //{"nfbalan","nflnint0","nflnowe","nfbali","nfballn","nfcklst","nfpdrf","nfchint"};
  static readonly DateTime date1 = new DateTime(2001, 8, 8);
  static readonly DateTime date2 = new DateTime(2001,12,31);
  static readonly string value = "14060498090080030";

  static void Run(string bin)
  {
    Console.WriteLine("Make: " + bin);
    using (var sw = new StreamWriter(pre + bin + ".txt", false, Encode))
      foreach (var dir in dirs)
        for (var date = date1; date <= date2; date = date.AddDays(1)) {
          var file = Path.Combine(Path.Combine(workDirectory, dir),
            string.Format("{0}{1:yyyyMMdd}-1406-0000.zip", pre, date));
          var binFileName = string.Format("{0}{1:yyyyMMdd}-{2}.bin", pre, date, bin);
          using (var zip = ZipFile.OpenRead(file))
            foreach (var arch in zip.Entries) {
              if (arch.Name != binFileName) continue;
              using (var reader = new StreamReader(arch.Open(), Encode))
                for (string s; (s = reader.ReadLine()) != null; )
                  if (s.Contains(value))
                    sw.WriteLine("{0}: {1}", arch.Name, s);
            }
        }
  }

  static void Main()
  {
    Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
    Console.WriteLine(Environment.CurrentDirectory);
    foreach (var bin in bins) Run(bin);
  }
}
