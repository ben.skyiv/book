﻿using System;
using System.IO;
using System.Data.OleDb;

namespace ExcelTester
{
  static class ExcelTester
  {
    static void Main()
    {
      try {
        var ver = ExcelVersion.V2007;
        var fileName = "Test.xlsx";
        Write(ver, fileName);
        Read(ver, fileName);
      }
      catch (Exception ex) { Console.WriteLine(ex.Message); }
    }

    static void Read(ExcelVersion ver, string excelFileName)
    {
      var tableName = excelFileName.GetExcelTableNames(ver)[0];
      using (var conn = new OleDbConnection(excelFileName.GetExcelConnectString(ver, ExcelHeader.None, ExcelMode.Read)))
      {
        conn.Open();
        var sql = string.Format("SELECT * FROM [{0}]", tableName);
        Console.WriteLine("正在读取：" + excelFileName);
        Console.WriteLine(sql);
        using (var comm = new OleDbCommand(sql, conn))
        using (var reader = comm.ExecuteReader())
        {
          while (reader.Read())
          {
            var ss = new object[reader.FieldCount];
            reader.GetValues(ss);
            foreach (var s in ss) Console.Write(s + "|");
            Console.WriteLine();
          }
        }
      }
    }

    static void Write(ExcelVersion ver, string fileName)
    {
      string[] columns = { "姓名", "出生年份" };
      using (var writer = new ExcelWriter(ver, fileName, true))
      {
        writer.CreateSheet("Sheet1", columns);
        writer.Write("张三", "1953");
        writer.Write("李四", "2012");
      }
    }
  }
}
