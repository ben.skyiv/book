﻿using System;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;

namespace ExcelTester
{
  public enum ExcelVersion { V2003, V2007 };
  public enum ExcelHeader { None, Has };
  public enum ExcelMode { Write = 0, Read = 1, ReadWrite = 2 };

  public static class Utils
  {
    public static string GetExcelConnectString(this string fileName, ExcelVersion ver, ExcelHeader header, ExcelMode mode)
    {
      // IMEX=?: 0:输出模式, 1:输入模式 2:连接模式
      // Excel 8.0     : Excel 2003
      // Excel 12.0 Xml: Excel 2007
      return string.Format(
        "Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=\"Excel {0};HDR={1};IMEX={2}\";Data Source={3}",
        (ver == ExcelVersion.V2003) ? "8.0" : "12.0 Xml",
        (header == ExcelHeader.Has) ? "yes" : "no",
        mode,
        fileName
      );
    }

    public static string[] GetExcelTableNames(this string excelFileName, ExcelVersion ver)
    {
      if (!File.Exists(excelFileName)) return null;
      var list = new List<string>();
      using (var conn = new OleDbConnection(excelFileName.GetExcelConnectString(ver, ExcelHeader.None, ExcelMode.Read)))
      {
        conn.Open();
        foreach (DataRow row in conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows)
        {
          var s = row[2].ToString().Trim();
          if (s.EndsWith("FilterDatabase")) continue;
          list.Add(s);
        }
      }
      return list.ToArray();
    }
  }
}
