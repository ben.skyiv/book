﻿using System;
using System.IO;
using System.Text;
using System.Data.OleDb;

namespace ExcelTester
{
  public sealed class ExcelWriter : IDisposable
  {
    string tableName;
    int fieldCount;
    OleDbConnection conn;
    OleDbCommand comm;

    public ExcelWriter(ExcelVersion ver, string fileName, bool deleteIfExists)
    {
      if (deleteIfExists && File.Exists(fileName)) File.Delete(fileName);
      conn = new OleDbConnection(fileName.GetExcelConnectString(ver, ExcelHeader.None, ExcelMode.Write));
      conn.Open();
    }

    public void CreateSheet(string sheetName, params string[] columnNames)
    {
      tableName = sheetName;
      comm = new OleDbCommand();
      comm.Connection = conn;
      var sb = new StringBuilder();
      sb.AppendFormat("CREATE TABLE [{0}] (", tableName);
      foreach (var name in columnNames) sb.AppendFormat("[{0}] CHAR,", name);
      sb.Length--;
      sb.Append(")");
      comm.CommandText = sb.ToString();
      comm.ExecuteNonQuery();
      fieldCount = columnNames.Length;
      Prepare();
    }

    public void SelectSheet(string sheetName, int fieldCount)
    {
      tableName = sheetName;
      comm = new OleDbCommand();
      comm.Connection = conn;
      this.fieldCount = fieldCount;
      Prepare();
    }

    void Prepare()
    {
      var sb = new StringBuilder();
      sb.AppendFormat("INSERT INTO [{0}] VALUES (", tableName);
      for (var i = 0; i < fieldCount; i++) sb.Append("@p" + i + ",");
      sb.Length--;
      sb.Append(")");
      comm.CommandText = sb.ToString();
      for (var i = 0; i < fieldCount; i++) comm.Parameters.Add("@p" + i, OleDbType.Variant);
    }

    public void Write(params object[] columns)
    {
      if (columns.Length != fieldCount) throw new InvalidProgramException("栏目数(" + columns.Length + ")应等于字段数(" + fieldCount + ")");
      for (var i = 0; i < fieldCount; i++) comm.Parameters[i].Value = columns[i];
      comm.ExecuteNonQuery();
    }

    public void UpdateSingleField(string value)
    {
      comm.CommandText = "UPDATE [" + tableName + "] SET [F1]='" + value + "'";
      comm.ExecuteNonQuery();
    }


    public void Dispose()
    {
      if (comm != null) comm.Dispose();
      if (conn != null) conn.Dispose();
    }
  }
}
