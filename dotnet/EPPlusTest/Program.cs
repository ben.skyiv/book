using System;
using System.IO;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;

class Sample_Main
{
  static (int, int) Fill(ExcelWorksheet ws)
  {
    ws.Cells[1, 1].Value = "名称";
    ws.Cells[1, 2].Value = "价格";
    ws.Cells[1, 3].Value = "销量";

    ws.Cells[2, 1].Value = "大米";
    ws.Cells[2, 2].Value = 5890.17m;
    ws.Cells[2, 3].Value = 123456789;

    ws.Cells[3, 1].Value = "玉米";
    ws.Cells[3, 2].Value = 4898.70m;
    ws.Cells[3, 3].Value = 150;

    ws.Cells[4, 1].Value = "小米";
    ws.Cells[4, 2].Value = 38.78m;
    ws.Cells[4, 3].Value = 130;

    ws.Cells[5, 1].Value = "糯米";
    ws.Cells[5, 2].Value = 22.32m;
    ws.Cells[5, 3].Value = 200;

    ws.Cells[6, 1].Value = "小麦";
    ws.Cells[6, 2].Value = 28.78m;
    ws.Cells[6, 3].Value = 8;

    int maxRow = 7, maxCol = 4;

    ws.Cells[1, maxCol].Value = "金额";
    ws.Cells[maxRow, 1].Value = "合计";

    using (var range = ws.Cells[1, 1, 1, maxCol]) {
      range.Style.Font.Bold = true;
      range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
    }

    ws.Cells[2, maxCol, maxRow - 1, maxCol].Formula = "B2*C2";
    ws.Cells[maxRow, 2, maxRow, maxCol].Formula = string.Format
      ("SUM({0})", new ExcelAddress(2, 2, maxRow - 1, 2).Address);

    for (var i = 2; i <= maxCol; i++)
      ws.Cells[2, i, maxRow, i].Style.Numberformat.Format
        = "#,##0" + ((i == 3) ? "" : ".00");

    ws.Calculate();
    ws.Cells.AutoFitColumns(0);  // Autofit columns for all cells

    return (maxRow, maxCol);
  }

  static void SetBorder(ExcelWorksheet ws, int r1, int c1, int r2, int c2)
  {
    ws.Cells[r1, c1, r2, c2].Style.Border
      .BorderAround(ExcelBorderStyle.Thin, Color.Black);
  }

  static void SetBorders(ExcelWorksheet ws, int row, int col)
  {
    for (var i = 1; i <= row; i += 2) SetBorder(ws, i, 1, i, col);
    for (var i = 1; i <= col; i += 2) SetBorder(ws, 1, i, row, i);
    if (row % 2 == 0) SetBorder(ws, row, 1, row, col);
    if (col % 2 == 0) SetBorder(ws, 1, col, row, col);
  }

  static void Main(string[] args)
  {
    var stream = File.Create("/home/ben/tmp/test.xlsx");
    using (var package = new ExcelPackage(stream)) {
      var ws = package.Workbook.Worksheets.Add("食物价格");
      var (row, col) = Fill(ws);
      SetBorders(ws, row, col);
      package.Save();
    }
  }
}

