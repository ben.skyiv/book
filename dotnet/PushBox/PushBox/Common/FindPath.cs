using System;
using System.Drawing;
using System.Collections.Generic;

namespace Skyiv.Ben.PushBox.Common
{
  /// <summary>
  /// 寻找最短路线
  /// </summary>
  static class FindPath
  {
    static Size[] offsets = { new Size(0, 1), new Size(1, 0), new Size(0, -1), new Size(-1, 0) };
    static Direction[] directions = { Direction.South, Direction.East, Direction.North, Direction.West };

    /// <summary>
    /// 寻找最短路线
    /// </summary>
    /// <param name="map">地图</param>
    /// <param name="from">出发点</param>
    /// <param name="to">目的地</param>
    /// <returns>最短路线</returns>
    public static Queue<Direction> Seek(byte[,] map, Point from, Point to)
    {
      Queue<Direction> moveQueue = new Queue<Direction>(); // 路线
      int value; // 与离目的地距离相关的一个量,变化规律: ... => 2 => 1 => 3 => 2 => 1 => 3 => 2 => 1
      if (Seek(map, to, out value)) // 找到了一条路线
      {
        Point here = from; // 出发点(即工人的位置)
        Point nbr = new Point(); // 四周的邻居
        for (value = (value + 1) % 3 + 1; here != to; value = (value + 1) % 3 + 1) // 逐步走向目的地
        {
          for (int i = 0; i < offsets.Length; i++)
          {
            nbr = Fcl.Add(here, offsets[i]); // 开始寻找四周的邻居
            if (Block.Value(map[nbr.Y, nbr.X]) == value) // 就往这个方向走
            {
              moveQueue.Enqueue(directions[i]); // 路线向目的地延伸一步
              break;
            }
          }
          here = nbr; // 继续前进
        }
      }
      Block.CleanAllMark(map); // 清除所有标志,恢复现场
      return moveQueue; // 所寻找的路线,如果无法到达目的地则为该路线的长度为零
    }

    /// <summary>
    /// 寻找最短路线,使用广度优先搜索
    /// </summary>
    /// <param name="map">地图</param>
    /// <param name="to">目的地</param>
    /// <param name="value">输出:搜索完成时标记的值</param>
    /// <returns>是否成功</returns>
    static bool Seek(byte[,] map, Point to, out int value)
    {
      Queue<Point> q = new Queue<Point>();
      Block.Mark(ref map[to.Y, to.X], 1); // 从目的地开始往回寻找出发点,目的地标记为1
      Point nbr = Point.Empty; // 四周的邻居
      for (; ; )
      {
        value = Block.Value(map[to.Y, to.X]) % 3 + 1; // 与离目的地距离相关的一个量,用作标记,变化规律:
        for (int i = 0; i < offsets.Length; i++)      // 1 => 2 => 3 => 1 => 2 => 3 => 1 => 2 => 3 => ...
        {
          nbr = Fcl.Add(to, offsets[i]); // 开始寻找四周的邻居
          if (Block.IsMan(map[nbr.Y, nbr.X])) break; // 到达出发点(即工人的位置)
          if (Block.IsBlank(map[nbr.Y, nbr.X])) // 可以走的路
          {
            Block.Mark(ref map[nbr.Y, nbr.X], value); // 标记,防止以后再走这条路
            q.Enqueue(nbr); // 加入队列,等待以后继续寻找
          }
        }
        if (Block.IsMan(map[nbr.Y, nbr.X])) break; // 到达出发点
        if (q.Count == 0) return false; // 无法到达出发点
        to = q.Dequeue(); // 出队,继续寻找,这是广度优先搜索,因为前面已经把四周能够走的路全部加入队列中了.
      }
      return true; // 找到一条路线
    }
  }
}
