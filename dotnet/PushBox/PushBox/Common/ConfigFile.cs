using System;
using System.IO;
using System.Xml;
using System.Text;

namespace Skyiv.Ben.PushBox.Common
{
  /// <summary>
  /// 管理配置文件: PushBox.cfg
  /// </summary>
  sealed class ConfigFile
  {
    const string ElmRoot = "PushBox"; // 配置文件的根元素
    const string ElmFiles = "files";
    const string ElmOption = "option";
    const string AttrMaxLevelSize = "maxlevelsize";
    const string AttrStepDelay = "stepdelay";
    const string AttrReplayDelay = "replaydelay";
    const string AttrSave = "save";
    const string ElmStack = "stack";
    const string AttrGroup = "group";
    const string AttrLevel = "level";
    const string AttrData = "data";

    int maxLevelSize; // 最大关尺寸(宽度和高度)
    int stepDelay;    // 移动时间间隔(毫秒)
    int replayDelay;  // 回放时间间隔(毫秒)
    bool isSave;      // 是否保护现场
    bool isSaveOem;   // 是否保护现场(第一次读取时的值)
    int group;        // 当前组数(0起始)
    string[] groups;  // 各组的数据文件主名
    int[] levels;     // 指明各组的当前关数(0起始)
    string steps;     // 工人移动路线，用于程序启动时恢复现场

    public int MaxLevelSize { get { return maxLevelSize; } set { maxLevelSize = value; } }
    public int StepDelay { get { return stepDelay; } set { stepDelay = value; } }
    public int ReplayDelay { get { return replayDelay; } set { replayDelay = value; } }
    public bool IsSave { get { return isSave; } set { isSave = value; } }
    public int Group { get { return group; } set { group = value; } }
    public string[] Groups { get { return groups; } }
    public int[] Levels { get { return levels; } }
    public string Steps { get { return steps; } }

    /// <summary>
    /// 装入配置文件
    /// </summary>
    public void LoadConfig()
    {
      maxLevelSize = Pub.DefaultMaxLevelSize;
      stepDelay = Pub.DefaultStepDelay;
      replayDelay = Pub.DefaultReplayDelay;
      isSave = isSaveOem = false;
      group = 0;
      int level = 0;
      XmlDocument doc = new XmlDocument();
      doc.Load(Pub.ConfigFileName);
      XmlElement elm = doc.DocumentElement[ElmOption];
      XmlAttributeCollection attrs;
      XmlAttribute attr;
      if (elm != null)
      {
        attrs = elm.Attributes;
        if ((attr = attrs[AttrMaxLevelSize]) != null)
        {
          try { maxLevelSize = int.Parse(attr.Value); }
          catch { }
          if (maxLevelSize < 1) maxLevelSize = 1;
        }
        if ((attr = attrs[AttrStepDelay]) != null)
        {
          try { stepDelay = int.Parse(attr.Value); }
          catch { }
          if (stepDelay < 0) stepDelay = 0;
          if (stepDelay > Pub.MaxDelay) stepDelay = Pub.MaxDelay;
        }
        if ((attr = attrs[AttrReplayDelay]) != null)
        {
          try { replayDelay = int.Parse(attr.Value); }
          catch { }
          if (replayDelay < 0) replayDelay = 0;
          if (replayDelay > Pub.MaxDelay) replayDelay = Pub.MaxDelay;
        }
        if ((attr = attrs[AttrSave]) != null)
        {
          try { isSave = isSaveOem = bool.Parse(attr.Value); }
          catch { }
        }
        if (isSave && (elm = doc.DocumentElement[ElmStack]) != null)
        {
          attrs = elm.Attributes;
          if ((attr = attrs[AttrGroup]) != null)
          {
            try { group = int.Parse(attr.Value) - 1; }
            catch { }
          }
          if ((attr = attrs[AttrLevel]) != null)
          {
            try { level = int.Parse(attr.Value) - 1; }
            catch { }
          }
          if ((attr = attrs[AttrData]) != null)
          {
            steps = attr.Value;
          }
        }
      }
      elm = doc.DocumentElement[ElmFiles];
      if (elm == null) throw new Exception("配置文件错:缺少<" + ElmFiles +">元素");
      XmlNodeList elms = elm.ChildNodes;
      if (elms.Count < 1) throw new Exception("配置文件错:<" + ElmFiles + ">元素必须至少包括一项");
      groups = new string[elms.Count];
      levels = new int[elms.Count];
      for (int i = 0; i < elms.Count; i++) groups[i] = elms[i].InnerText;
      if (group < 0) group = 0;
      if (group > groups.Length - 1) group = groups.Length - 1;
      if (level < 0) level = 0;
      levels[group] = level;
    }

    /// <summary>
    /// 保存组信息到配置文件
    /// </summary>
    /// <param name="groups">组信息</param>
    public void SaveConfig(string[] groups)
    {
      XmlDocument doc = new XmlDocument();
      if (File.Exists(Pub.ConfigFileName)) doc.Load(Pub.ConfigFileName);
      else
      {
        XmlElement root = doc.CreateElement(ElmRoot);
        root.AppendChild(doc.CreateElement(ElmFiles));
        doc.AppendChild(root);
      }
      XmlElement files = (doc.DocumentElement)[ElmFiles];
      if (files == null)
      {
        files = doc.CreateElement(ElmFiles);
        doc.DocumentElement.AppendChild(files);
      }
      files.RemoveAll();
      foreach (string s in groups)
      {
        XmlElement file = doc.CreateElement("file");
        file.InnerText = s;
        files.AppendChild(file);
      }
      SaveStack(doc, 0, 0, "");
      doc.Save(Pub.ConfigFileName);
    }

    /// <summary>
    /// 保存当前选项及当前走法到配置文件
    /// </summary>
    /// <param name="steps">当前走法</param>
    public void SaveConfig(Step[] steps)
    {
      if (!isSave && isSave == isSaveOem) return; // “保护现场”复选框没有选中，且程序启动时就没有选中
      XmlDocument doc = new XmlDocument();
      doc.Load(Pub.ConfigFileName);
      XmlElement elm = doc.DocumentElement[ElmOption];
      if (elm == null)
      {
        elm = doc.CreateElement(ElmOption);
        doc.DocumentElement.AppendChild(elm);
      }
      elm.SetAttribute(AttrSave, isSave.ToString()); // 保存“保护现场”复选框的状态
      if (isSave) // “保护现场”复选框已选中
      {
        elm.SetAttribute(AttrMaxLevelSize, maxLevelSize.ToString()); // 保存“最大关尺寸”
        elm.SetAttribute(AttrStepDelay, stepDelay.ToString()); // 保存“移动速度”
        elm.SetAttribute(AttrReplayDelay, replayDelay.ToString()); // 保存“回放速度”
        SaveStack(doc, group, levels[group], Pub.ToString(steps)); // 保存当前组数、当前关数和当前走法
      }
      doc.Save(Pub.ConfigFileName);
    }

    /// <summary>
    /// 保存当前组数、当前关数和当前走法到配置文件
    /// </summary>
    /// <param name="doc">配置文件</param>
    /// <param name="group">当前组数</param>
    /// <param name="level">当前关数</param>
    /// <param name="steps">当前走法</param>
    void SaveStack(XmlDocument doc, int group, int level, string steps)
    {
      XmlElement elm = doc.DocumentElement[ElmStack];
      if (elm == null)
      {
        elm = doc.CreateElement(ElmStack);
        doc.DocumentElement.AppendChild(elm);
      }
      elm.SetAttribute(AttrGroup, (group + 1).ToString()); // 保存当前组数
      elm.SetAttribute(AttrLevel, (level + 1).ToString()); // 保存当前关数
      elm.SetAttribute(AttrData, steps); // 保存当前走法
    }
  }
}
