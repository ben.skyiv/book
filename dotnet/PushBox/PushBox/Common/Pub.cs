using System;
using System.Drawing;
using System.Text;
using System.IO;
using System.Reflection;

namespace Skyiv.Ben.PushBox.Common
{
  /// <summary>
  /// 公共的字段和方法
  /// </summary>
  static class Pub
  {
    public const int OverY = 4; // 允许在屏幕(Y)方向超过的像素数
    public const int DefaultMaxLevelSize = 32; // 缺省的最大关尺寸(宽度和高度)
    public const int DefaultStepDelay = 100;   // 缺省移动时间间隔(毫秒)
    public const int DefaultReplayDelay = 300; // 缺省回放时间间隔(毫秒)
    public const int MaxDelay = 1000;          // 允许的最大时间间隔(毫秒)
    public readonly static string ConfigFileName = Path.Combine(baseDirectory, "PushBox.cfg"); // 配置文件全路径名
    public readonly static Encoding Encode = Encoding.GetEncoding("GB2312"); // Windows Mobile 6.0 不支持 GB18030
    static string baseDirectory { get { return Path.GetDirectoryName(Pub.CodeBases); } } // 本程序所在的目录

    static Assembly Assembly { get { return Assembly.GetExecutingAssembly(); } }
    static AssemblyName AssemblyName { get { return Pub.Assembly.GetName(); } }
    public static Version Version { get { return Pub.AssemblyName.Version; } } // 本程序的版本
    public static string TextDirectory { get { return Path.Combine(baseDirectory, "text"); } }
    public static string DataDirectory { get { return Path.Combine(baseDirectory, "data"); } }
    public static string StepsDirectory { get { return Path.Combine(baseDirectory, "steps"); } }
    public const string TextExtName = ".bxa"; // 文本文件扩展名
    public const string DataExtName = ".bxb"; // 数据文件扩展名
    public const string StepsExtName = ".bxs"; // 通关步骤文件扩展名

    /// <summary>
    /// 本程序的全路径名
    /// </summary>
    public static string CodeBases
    {
      get
      {
        string codeBase = Pub.AssemblyName.CodeBase;
        string uri = "file:///";
        if (codeBase.StartsWith(uri)) codeBase = codeBase.Substring(uri.Length);
        return codeBase;
      }
    }

    /// <summary>
    /// 给出指定尺寸的显示字符串，格式为: 宽x高
    /// </summary>
    /// <param name="size">指定的尺寸</param>
    /// <returns>指定尺寸的显示字符串</returns>
    public static string ToString(Size size)
    {
      return size.Width + "x" + size.Height;
    }

    /// <summary>
    /// 将走法步骤转换为字符串
    /// </summary>
    /// <param name="steps">走法步骤</param>
    /// <returns>转换后的字符串</returns>
    public static string ToString(Step[] steps)
    {
      StringBuilder sb = new StringBuilder();
      foreach (Step step in steps) sb.Append((char)step);
      char[] array = sb.ToString().ToCharArray();
      Array.Reverse(array);
      return new string(array);
    }

    /// <summary>
    /// 给出指定版本的信息，格式为: x.x (build: yyyy-MM-dd)
    /// </summary>
    /// <param name="version">指定的版本</param>
    /// <returns>指定版本的信息</returns>
    public static string GetVersionBuildString(Version version)
    {
      double days = version.Build + 2 * version.Revision / ((double)TimeSpan.TicksPerDay / TimeSpan.TicksPerSecond);
      return string.Format("{0} (Build: {1})", version.ToString(2), (new DateTime(2000, 1, 1)).AddDays(days).ToString("yyyy-MM-dd HH:mm:ss"));
    }

    /// <summary>
    /// 给出指定异常的信息，包含其内含异常的信息
    /// </summary>
    /// <param name="ex">指定的异常</param>
    /// <param name="isDebug">是否给出详细信息</param>
    /// <returns>指定异常的信息</returns>
    public static string GetMessage(Exception ex, bool isDebug)
    {
      StringBuilder sb = new StringBuilder();
      for (Exception e = ex; e != null; e = e.InnerException)
      {
        sb.Append(isDebug ? e.ToString() : e.Message);
        sb.Append(Fcl.NewLine);
      }
      return sb.ToString();
    }
  }
}
