namespace Skyiv.Ben.PushBox.Common
{
  enum Direction { None, East, South, West, North } // 方向: 无 东 南 西 北
  public enum Action { None, Create, Edit, Delete } // 设计: 无 创建 编辑 删除

  /// <summary>
  /// 走法步骤
  /// </summary>
  struct Step
  {
    Direction direct; // 前进方向
    bool isBox; // 是否推着箱子一起前进
    bool isStop; // “撤销”时是否停留

    public Direction Direct { get { return direct; } }
    public bool IsBox { get { return isBox; } }
    public bool IsStop { get { return isStop; } }

    public Step(Direction direct, bool isBox, bool isStop)
    {
      this.direct = direct;
      this.isBox = isBox;
      this.isStop = isStop;
    }

    // isBox isStop None East South West North
    //               A    B     C    D    E
    //  x            F    G     H    I    J
    //        x      K    L     M    N    O
    //  x     x      P    Q     R    S    T

    public static implicit operator char(Step step)
    {
      char c = "ABCDE"[step.direct - Direction.None];
      if (step.isBox) c = (char)(c + 5);
      if (step.isStop) c = (char)(c + 10);
      return c;
    }

    public static implicit operator Step(char c)
    {
      int n = c - 'A';
      return new Step((Direction)(n % 5), (n % 10 >= 5), (n >= 10));
    }
  }
}
