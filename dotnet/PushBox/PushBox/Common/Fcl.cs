using System;
using System.IO;
using System.Drawing;

namespace Skyiv.Ben.PushBox.Common
{
  /// <summary>
  /// 这里是 .NET Framework 支持，而 .NET Compact Framework 不支持的东东
  /// </summary>
  static class Fcl
  {
    /// <summary>
    /// 获取为此环境定义的换行字符串。-- Environment
    /// </summary>
    public static string NewLine { get { return "\r\n";  } }

    /// <summary>
    /// 打开一个文本文件，将文件的所有行读入一个字符串，然后关闭该文件。-- File
    /// </summary>
    /// <param name="path">要打开以进行读取的文件</param>
    /// <returns>包含文件所有行的字符串</returns>
    public static string ReadAllText(string path)
    {
      string text = "";
      if (File.Exists(path))
      {
        using (StreamReader sr = new StreamReader(path, Pub.Encode))
        {
          text = sr.ReadToEnd();
        }
      }
      return text;
    }

    /// <summary>
    /// 创建一个新文件，在其中写入指定的字符串，然后关闭该文件。-- File
    /// </summary>
    /// <param name="path">要写入的文件</param>
    /// <param name="contents">要写入文件的字符串</param>
    public static void WriteAllText(string path, string contents)
    {
      using (StreamWriter sw = new StreamWriter(path, false, Pub.Encode))
      {
        sw.Write(contents);
      }
    }

    /// <summary>
    /// 将指定的 Size 添加到指定的 Point。-- Point
    /// </summary>
    /// <param name="point">要添加的 Point</param>
    /// <param name="size">要添加的 Size</param>
    /// <returns>加法运算的结果</returns>
    public static Point Add(Point point, Size size)
    {
      return new Point(point.X + size.Width, point.Y + size.Height);
    }

    /// <summary>
    /// 将一维数组的大小更改为指定的新大小。-- Array
    /// </summary>
    /// <typeparam name="T">数组元素的类型</typeparam>
    /// <param name="array">要调整大小的一维数组</param>
    /// <param name="newSize">新数组的大小</param>
    public static void Resize<T>(ref T[] array, int newSize)
    {
      if (array != null && array.Length == newSize) return;
      if (array == null) array = new T[0];
      T[] newArray = new T[newSize];
      Array.Copy(array, newArray, Math.Min(array.Length, newArray.Length));
      array = newArray;
    }
  }
}
