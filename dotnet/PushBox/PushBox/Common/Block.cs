namespace Skyiv.Ben.PushBox.Common
{
  /// <summary>
  /// 基本单元格: 地 槽 墙 砖 箱子 工人
  /// </summary>
  static class Block
  {
    public const byte Land = 0; // 地
    public const byte Slot = 1; // 槽
    public const byte Wall = 2; // 墙
    public const byte Brick = 3; // 砖: 等同于墙,一般放在墙的外围
    public const byte Box0 = 4; // 箱子放在地上
    public const byte Box1 = 5; // 箱子放在槽上
    public const byte Man0 = 6; // 工人站在地上
    public const byte Man1 = 7; // 工人站在槽上

    const string mask = "-+#%xX()"; // (*.bxa)文件用,依次代表以上各项

    public static char GetChar(byte block)
    {
      return mask[block & 0x07];
    }

    public static byte GetByte(char block)
    {
      return (byte)mask.IndexOf(block);
    }

    public static void CleanAllMark(byte[,] bb)
    {
      for (int i = 0; i < bb.GetLength(0); i++)
        for (int j = 0; j < bb.GetLength(1); j++)
          bb[i, j] &= 0x07;
    }

    public static void Mark(ref byte block, int value)
    {
      block |= (byte)(value << 3);
    }

    public static int Value(byte block)
    {
      return block >> 3;
    }

    public static string GetPenName(byte block)
    {
      return "地槽墙砖箱箱人人"[block & 0x07].ToString();
    }

    public static void Update(ref byte block, byte pen)
    {
      if (IsSlot(block) && pen == Block.Man0) pen = Block.Man1;
      if (IsSlot(block) && pen == Block.Box0) pen = Block.Box1;
      block = pen;
    }

    public static void ManIn(ref byte block)
    {
      block += (Man0 - Land);
    }

    public static void ManOut(ref byte block)
    {
      block -= (Man0 - Land);
    }

    public static void BoxIn(ref byte block)
    {
      block += (Box0 - Land);
    }

    public static void BoxOut(ref byte block)
    {
      block -= (Box0 - Land);
    }

    public static bool IsBlock(byte block)
    {
      return block <= Man1;
    }

    public static bool IsSlot(byte block)
    {
      return block == Slot || block == Box1 || block == Man1;
    }

    public static bool IsBlank(byte block)
    {
      return block == Land || block == Slot;
    }

    public static bool IsBox(byte block)
    {
      return block == Box0 || block == Box1;
    }

    public static bool IsMan(byte block)
    {
      return block == Man0 || block == Man1;
    }
  }
}
