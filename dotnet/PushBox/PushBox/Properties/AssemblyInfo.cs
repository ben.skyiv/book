﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("PushBox")]
[assembly: AssemblyDescription("推箱子")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Skyiv Studio")]
[assembly: AssemblyProduct("PushBox")]
[assembly: AssemblyCopyright("Copyright © Skyiv Studio 2007")]
[assembly: AssemblyTrademark("Skyiv")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ca257752-e7ff-4226-b358-832253341d47")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.2.*")]

