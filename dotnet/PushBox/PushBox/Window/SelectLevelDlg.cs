using System;
using System.Windows.Forms;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// ��ѡ�ء��Ի���
  /// </summary>
  public partial class SelectLevelDlg : Form
  {
    public int MaxLevel { set { nudMain.Maximum = value; } }
    public int Level { get { return (int)nudMain.Value - 1; } set { nudMain.Value = value + 1; } }

    public SelectLevelDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      lblMsg.Text += nudMain.Maximum.ToString();
    }
  }
}