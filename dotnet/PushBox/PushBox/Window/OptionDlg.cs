using System;
using System.Windows.Forms;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// ��ѡ��Ի���
  /// </summary>
  public partial class OptionDlg : Form
  {
    public bool IsSave { get { return chkSave.Checked; } set { chkSave.Checked = value; } }
    public int MaxLevelSize { get { return (int)nudMaxLevelSize.Value; } set { nudMaxLevelSize.Value = value; } }

    public int StepDelay
    {
      get
      {
        return Pub.MaxDelay - tkbStepSpeed.Value;
      }
      set
      {
        tkbStepSpeed.Maximum = Pub.MaxDelay;
        tkbStepSpeed.TickFrequency = tkbStepSpeed.LargeChange = 100;
        tkbStepSpeed.Value = Pub.MaxDelay - value; 
      }
    }

    public int ReplayDelay
    {
      get
      {
        return Pub.MaxDelay - tkbReplaySpeed.Value;
      }
      set
      {
        tkbReplaySpeed.Maximum = Pub.MaxDelay;
        tkbReplaySpeed.TickFrequency = tkbReplaySpeed.LargeChange = 100;
        tkbReplaySpeed.Value = Pub.MaxDelay - value;
      }
    }

    public OptionDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }

    private void tkbStepSpeed_ValueChanged(object sender, EventArgs e)
    {
      tbxStepSpeed.Text = StepDelay.ToString();
    }

    private void tkbReplaySpeed_ValueChanged(object sender, EventArgs e)
    {
      tbxReplaySpeed.Text = ReplayDelay.ToString();
    }
  }
}