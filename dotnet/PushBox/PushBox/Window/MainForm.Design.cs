using System;
using System.Drawing;
using System.Windows.Forms;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  partial class MainForm
  {
    void miDesign_Click(object sender, EventArgs e)
    {
      using (DesignDlg dlg = new DesignDlg(TopMost))
      {
        dlg.MaxLevelSize = env.MaxLevelSize;
        dlg.LevelSize = env.LevelSize;
        dlg.Level = env.Level;
        dlg.MaxLelvel = env.MaxLevel;
        if (dlg.ShowDialog() == DialogResult.OK)
        {
          env.Active = dlg.Active;
          if (env.Active == Action.Delete) DeleteLastLevel();
          else CreateOrEditLevel(dlg.IsCopy, dlg.LevelSize);
        }
      }
    }

    /// <summary>
    /// 新建一关或者编辑当前关
    /// </summary>
    /// <param name="isCopy">新建时是否复制当前关</param>
    /// <param name="size">新建关的尺寸</param>
    void CreateOrEditLevel(bool isCopy, Size size)
    {
      if (env.Active == Action.Create)
      {
        env.NewLevel(isCopy, size);
        if (Environment.OSVersion.Platform != PlatformID.WinCE)
          ClientSize = env.GetClientSize(sbrMain.Visible ? sbrMain.Height : 0);
        ClientSizeChanged();
      }
      env.Pen = Block.Land;
      UpdateStatus();
    }

    /// <summary>
    /// 删除最后一关
    /// </summary>
    void DeleteLastLevel()
    {
      env.Active = Action.None;
      if (env.Level != env.MaxLevel - 1 || env.MaxLevel <= 1) return;
      env.DeleteLastLevel();
      miPrevLevel2OrSlot_Click(null, null);
    }

    /// <summary>
    /// “设计”模式下, 鼠标点击时要采取的动作
    /// </summary>
    void Design()
    {
      Rectangle invalid;
      if (!env.Design(out invalid)) return;
      Invalidate(invalid);
      UpdateStatus();
    }
  }
}
