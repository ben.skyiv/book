﻿namespace Skyiv.Ben.PushBox.Window
{
  partial class OptionDlg
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private System.Windows.Forms.MainMenu mnuMain;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.mnuMain = new System.Windows.Forms.MainMenu();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.tkbStepSpeed = new System.Windows.Forms.TrackBar();
      this.chkSave = new System.Windows.Forms.CheckBox();
      this.label2 = new System.Windows.Forms.Label();
      this.tkbReplaySpeed = new System.Windows.Forms.TrackBar();
      this.label3 = new System.Windows.Forms.Label();
      this.nudMaxLevelSize = new System.Windows.Forms.NumericUpDown();
      this.tbxStepSpeed = new System.Windows.Forms.TextBox();
      this.tbxReplaySpeed = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // btnOk
      // 
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.Location = new System.Drawing.Point(3, 3);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(112, 20);
      this.btnOk.TabIndex = 0;
      this.btnOk.Text = "确定";
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(120, 3);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(112, 20);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "取消";
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(3, 26);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(98, 20);
      this.label1.Text = "移动速度 (延时";
      // 
      // tkbStepSpeed
      // 
      this.tkbStepSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tkbStepSpeed.Location = new System.Drawing.Point(3, 45);
      this.tkbStepSpeed.Name = "tkbStepSpeed";
      this.tkbStepSpeed.Size = new System.Drawing.Size(234, 45);
      this.tkbStepSpeed.TabIndex = 3;
      this.tkbStepSpeed.ValueChanged += new System.EventHandler(this.tkbStepSpeed_ValueChanged);
      // 
      // chkSave
      // 
      this.chkSave.Location = new System.Drawing.Point(3, 167);
      this.chkSave.Name = "chkSave";
      this.chkSave.Size = new System.Drawing.Size(90, 20);
      this.chkSave.TabIndex = 7;
      this.chkSave.Text = "保护现场";
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(3, 85);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(98, 20);
      this.label2.Text = "回放速度 (延时";
      // 
      // tkbReplaySpeed
      // 
      this.tkbReplaySpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tkbReplaySpeed.Location = new System.Drawing.Point(3, 104);
      this.tkbReplaySpeed.Name = "tkbReplaySpeed";
      this.tkbReplaySpeed.Size = new System.Drawing.Size(234, 45);
      this.tkbReplaySpeed.TabIndex = 5;
      this.tkbReplaySpeed.ValueChanged += new System.EventHandler(this.tkbReplaySpeed_ValueChanged);
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(3, 144);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(74, 20);
      this.label3.Text = "最大关尺寸";
      // 
      // nudMaxLevelSize
      // 
      this.nudMaxLevelSize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.nudMaxLevelSize.Location = new System.Drawing.Point(87, 142);
      this.nudMaxLevelSize.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.nudMaxLevelSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMaxLevelSize.Name = "nudMaxLevelSize";
      this.nudMaxLevelSize.Size = new System.Drawing.Size(145, 22);
      this.nudMaxLevelSize.TabIndex = 6;
      this.nudMaxLevelSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // tbxStepSpeed
      // 
      this.tbxStepSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tbxStepSpeed.Location = new System.Drawing.Point(102, 26);
      this.tbxStepSpeed.Name = "tbxStepSpeed";
      this.tbxStepSpeed.ReadOnly = true;
      this.tbxStepSpeed.Size = new System.Drawing.Size(82, 21);
      this.tbxStepSpeed.TabIndex = 2;
      // 
      // tbxReplaySpeed
      // 
      this.tbxReplaySpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tbxReplaySpeed.Location = new System.Drawing.Point(102, 85);
      this.tbxReplaySpeed.Name = "tbxReplaySpeed";
      this.tbxReplaySpeed.ReadOnly = true;
      this.tbxReplaySpeed.Size = new System.Drawing.Size(82, 21);
      this.tbxReplaySpeed.TabIndex = 4;
      // 
      // label4
      // 
      this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label4.Location = new System.Drawing.Point(190, 26);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(42, 20);
      this.label4.Text = "毫秒)";
      // 
      // label5
      // 
      this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label5.Location = new System.Drawing.Point(190, 85);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(42, 20);
      this.label5.Text = "毫秒)";
      // 
      // OptionDlg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.tbxReplaySpeed);
      this.Controls.Add(this.tbxStepSpeed);
      this.Controls.Add(this.nudMaxLevelSize);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.tkbReplaySpeed);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.chkSave);
      this.Controls.Add(this.tkbStepSpeed);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOk);
      this.Menu = this.mnuMain;
      this.Name = "OptionDlg";
      this.Text = "选项 - 推箱子";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TrackBar tkbStepSpeed;
    private System.Windows.Forms.CheckBox chkSave;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TrackBar tkbReplaySpeed;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown nudMaxLevelSize;
    private System.Windows.Forms.TextBox tbxStepSpeed;
    private System.Windows.Forms.TextBox tbxReplaySpeed;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
  }
}