using System;
using System.IO;
using System.Windows.Forms;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// “转换”对话框
  /// </summary>
  public partial class TranDlg : Form
  {
    bool isEdit; // 是否处于编辑状态
    int maxLevelSize;
    DataFile db;

    public string[] Groups { set { cbxGroup.DataSource = value; } }
    public int MaxLevelSize { set { maxLevelSize = value; } }

    public TranDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      db = new DataFile();
      if (cbxGroup.Items.Count <= 0)
      {
        btnEdit.Enabled = btnExportOrSave.Enabled = btnImportOrCancel.Enabled = cbxGroup.Enabled = false;
        tbxMsg.Text = "请先做“菜单 -> 数据 -> 配置”";
      }
    }

    protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
    {
      if (isEdit)
      {
        MessageBox.Show("正在编辑，不能退出。请先“保存”或“放弃”。", "提醒");
        e.Cancel = true;
      }
      else if (db != null) db.Dispose();
      base.OnClosing(e);
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      tbxMsg.Text = Fcl.ReadAllText(Path.Combine(Pub.TextDirectory, cbxGroup.SelectedItem + Pub.TextExtName));
      cbxGroup.Enabled = false;
      tbxMsg.ReadOnly = false;
      isEdit = true;
      btnEdit.Enabled = false;
      btnExportOrSave.Text = "保存";
      btnImportOrCancel.Text = "放弃";
    }

    private void btnExportOrSave_Click(object sender, EventArgs e)
    {
      SetEnable(false);
      try
      {
        if (isEdit) Save(true);
        else db.Export(cbxGroup.SelectedItem.ToString(), tbxMsg);
      }
      catch (Exception ex)
      {
        tbxMsg.Text += Pub.GetMessage(ex, false);
      }
      tbxMsg.Select(tbxMsg.Text.Length, 0);
      tbxMsg.ScrollToCaret();
      SetEnable(true);
    }

    private void btnImportOrCancel_Click(object sender, EventArgs e)
    {
      SetEnable(false);
      try
      {
        if (isEdit) Save(false);
        else db.Import(cbxGroup.SelectedItem.ToString(), maxLevelSize, tbxMsg);
      }
      catch (Exception ex)
      {
        tbxMsg.Text += Pub.GetMessage(ex, false);
      }
      tbxMsg.Select(tbxMsg.Text.Length, 0);
      tbxMsg.ScrollToCaret();
      SetEnable(true);
    }

    void SetEnable(bool isEnable)
    {
      btnEdit.Enabled = btnExportOrSave.Enabled = btnImportOrCancel.Enabled = cbxGroup.Enabled = isEnable;
      Update();
    }

    void Save(bool isSave)
    {
      if (isSave)
      {
        if (!Directory.Exists(Pub.TextDirectory)) Directory.CreateDirectory(Pub.TextDirectory);
        Fcl.WriteAllText(Path.Combine(Pub.TextDirectory, cbxGroup.SelectedItem + Pub.TextExtName), tbxMsg.Text);
      }
      tbxMsg.Text = isSave ? "游戏数据文本文件已经保存完毕" : "已经放弃编辑";
      isEdit = false;
      btnEdit.Enabled = true;
      btnExportOrSave.Text = "导出";
      btnImportOrCancel.Text = "导入";
      cbxGroup.Enabled = true;
      tbxMsg.ReadOnly = true;
    }
  }
}