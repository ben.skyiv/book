﻿namespace Skyiv.Ben.PushBox.Window
{
  partial class TranDlg
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private System.Windows.Forms.MainMenu mnuMain;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.mnuMain = new System.Windows.Forms.MainMenu();
      this.btnEdit = new System.Windows.Forms.Button();
      this.btnExportOrSave = new System.Windows.Forms.Button();
      this.btnImportOrCancel = new System.Windows.Forms.Button();
      this.cbxGroup = new System.Windows.Forms.ComboBox();
      this.tbxMsg = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // btnEdit
      // 
      this.btnEdit.Location = new System.Drawing.Point(3, 3);
      this.btnEdit.Name = "btnEdit";
      this.btnEdit.Size = new System.Drawing.Size(72, 20);
      this.btnEdit.TabIndex = 0;
      this.btnEdit.Text = "编辑";
      this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
      // 
      // btnExportOrSave
      // 
      this.btnExportOrSave.Location = new System.Drawing.Point(84, 3);
      this.btnExportOrSave.Name = "btnExportOrSave";
      this.btnExportOrSave.Size = new System.Drawing.Size(72, 20);
      this.btnExportOrSave.TabIndex = 1;
      this.btnExportOrSave.Text = "导出";
      this.btnExportOrSave.Click += new System.EventHandler(this.btnExportOrSave_Click);
      // 
      // btnImportOrCancel
      // 
      this.btnImportOrCancel.Location = new System.Drawing.Point(165, 3);
      this.btnImportOrCancel.Name = "btnImportOrCancel";
      this.btnImportOrCancel.Size = new System.Drawing.Size(72, 20);
      this.btnImportOrCancel.TabIndex = 2;
      this.btnImportOrCancel.Text = "导入";
      this.btnImportOrCancel.Click += new System.EventHandler(this.btnImportOrCancel_Click);
      // 
      // cbxGroup
      // 
      this.cbxGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.cbxGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxGroup.Location = new System.Drawing.Point(3, 29);
      this.cbxGroup.Name = "cbxGroup";
      this.cbxGroup.Size = new System.Drawing.Size(234, 22);
      this.cbxGroup.TabIndex = 3;
      // 
      // tbxMsg
      // 
      this.tbxMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tbxMsg.Location = new System.Drawing.Point(3, 57);
      this.tbxMsg.Multiline = true;
      this.tbxMsg.Name = "tbxMsg";
      this.tbxMsg.ReadOnly = true;
      this.tbxMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbxMsg.Size = new System.Drawing.Size(234, 208);
      this.tbxMsg.TabIndex = 4;
      // 
      // TranDlg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Controls.Add(this.tbxMsg);
      this.Controls.Add(this.cbxGroup);
      this.Controls.Add(this.btnImportOrCancel);
      this.Controls.Add(this.btnExportOrSave);
      this.Controls.Add(this.btnEdit);
      this.Menu = this.mnuMain;
      this.Name = "TranDlg";
      this.Text = "转换 - 推箱子";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnEdit;
    private System.Windows.Forms.Button btnExportOrSave;
    private System.Windows.Forms.Button btnImportOrCancel;
    private System.Windows.Forms.ComboBox cbxGroup;
    private System.Windows.Forms.TextBox tbxMsg;
  }
}