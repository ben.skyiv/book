﻿namespace Skyiv.Ben.PushBox.Window
{
  partial class AboutDlg
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.tclMain = new System.Windows.Forms.TabControl();
      this.tpgVersion = new System.Windows.Forms.TabPage();
      this.tbxVersion = new System.Windows.Forms.TextBox();
      this.tpgSystem = new System.Windows.Forms.TabPage();
      this.tbxSystem = new System.Windows.Forms.TextBox();
      this.tclMain.SuspendLayout();
      this.tpgVersion.SuspendLayout();
      this.tpgSystem.SuspendLayout();
      this.SuspendLayout();
      // 
      // tclMain
      // 
      this.tclMain.Controls.Add(this.tpgVersion);
      this.tclMain.Controls.Add(this.tpgSystem);
      this.tclMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tclMain.Location = new System.Drawing.Point(0, 0);
      this.tclMain.Name = "tclMain";
      this.tclMain.SelectedIndex = 0;
      this.tclMain.Size = new System.Drawing.Size(240, 294);
      this.tclMain.TabIndex = 0;
      // 
      // tpgVersion
      // 
      this.tpgVersion.Controls.Add(this.tbxVersion);
      this.tpgVersion.Location = new System.Drawing.Point(0, 0);
      this.tpgVersion.Name = "tpgVersion";
      this.tpgVersion.Size = new System.Drawing.Size(240, 271);
      this.tpgVersion.Text = "版本";
      // 
      // tbxVersion
      // 
      this.tbxVersion.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxVersion.Location = new System.Drawing.Point(0, 0);
      this.tbxVersion.Multiline = true;
      this.tbxVersion.Name = "tbxVersion";
      this.tbxVersion.ReadOnly = true;
      this.tbxVersion.Size = new System.Drawing.Size(240, 271);
      this.tbxVersion.TabIndex = 0;
      // 
      // tpgSystem
      // 
      this.tpgSystem.Controls.Add(this.tbxSystem);
      this.tpgSystem.Location = new System.Drawing.Point(0, 0);
      this.tpgSystem.Name = "tpgSystem";
      this.tpgSystem.Size = new System.Drawing.Size(232, 268);
      this.tpgSystem.Text = "系统";
      // 
      // tbxSystem
      // 
      this.tbxSystem.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxSystem.Location = new System.Drawing.Point(0, 0);
      this.tbxSystem.Multiline = true;
      this.tbxSystem.Name = "tbxSystem";
      this.tbxSystem.ReadOnly = true;
      this.tbxSystem.Size = new System.Drawing.Size(232, 268);
      this.tbxSystem.TabIndex = 0;
      // 
      // AboutDlg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 294);
      this.Controls.Add(this.tclMain);
      this.Name = "AboutDlg";
      this.Text = "关于 - 推箱子";
      this.tclMain.ResumeLayout(false);
      this.tpgVersion.ResumeLayout(false);
      this.tpgSystem.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tclMain;
    private System.Windows.Forms.TabPage tpgVersion;
    private System.Windows.Forms.TabPage tpgSystem;
    private System.Windows.Forms.TextBox tbxVersion;
    private System.Windows.Forms.TextBox tbxSystem;

  }
}