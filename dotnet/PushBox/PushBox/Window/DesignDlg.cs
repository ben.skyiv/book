using System;
using System.Drawing;
using System.Windows.Forms;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// ����ơ��Ի���
  /// </summary>
  public partial class DesignDlg : Form
  {
    Action active;
    int level;
    int maxLevel;
    Size levelSize;

    public Action Active { get { return active; } }
    public bool IsCopy { get { return chkCopy.Checked; } }
    public int Level { set { level = value; } }
    public int MaxLelvel { set { maxLevel = value; } }

    public DesignDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      tbxLevel.Text = (level + 1).ToString() + "/" + maxLevel.ToString();
      rbnDelete.Enabled = (level == maxLevel - 1 && maxLevel > 1);
      Action_CheckedChanged(rbnCreate, null);
    }

    public Size LevelSize
    {
      get
      {
        return (active == Action.Create) ? new Size((int)nudWidth.Value, (int)nudHeight.Value) : levelSize;
      }
      set
      {
        levelSize = value;
        if (value.Width > nudWidth.Maximum) nudWidth.Maximum = value.Width;
        if (value.Height > nudHeight.Maximum) nudHeight.Maximum = value.Height;
        nudWidth.Value = value.Width;
        nudHeight.Value = value.Height;
      }
    }

    public int MaxLevelSize
    {
      set
      {
        nudWidth.Maximum = (nudWidth.Value > value) ? nudWidth.Value : value;
        nudHeight.Maximum = (nudHeight.Value > value) ? nudHeight.Value : value;
      }
    }

    private void Action_CheckedChanged(object sender, EventArgs e)
    {
      RadioButton s = (RadioButton)sender;
      if (s == rbnCreate && s.Checked)
      {
        active = Action.Create;
        chkCopy.Enabled = nudWidth.Enabled = nudHeight.Enabled = true;
        tbxDeleteLevel.ReadOnly = true;
        btnOk.Enabled = true;
      }
      if (s == rbnEdit && s.Checked)
      {
        active = Action.Edit;
        chkCopy.Enabled = nudWidth.Enabled = nudHeight.Enabled = false;
        tbxDeleteLevel.ReadOnly = true;
        btnOk.Enabled = true;
      }
      else if (s == rbnDelete && s.Checked)
      {
        active = Action.Delete;
        chkCopy.Enabled = nudWidth.Enabled = nudHeight.Enabled = false;
        tbxDeleteLevel.ReadOnly = false;
        tbxDeleteLevel.Text = string.Empty;
        btnOk.Enabled = (level + 1).ToString() == tbxDeleteLevel.Text;
      }
    }

    private void tbxDeleteLevel_TextChanged(object sender, EventArgs e)
    {
      btnOk.Enabled = (level + 1).ToString() == tbxDeleteLevel.Text;
    }
  }
}