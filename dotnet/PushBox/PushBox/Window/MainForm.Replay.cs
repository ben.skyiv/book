using System;
using System.Drawing;
using System.Threading;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  partial class MainForm
  {
    Rectangle workerThreadInvalidRectangle;
    bool workerThreadIsStop;

    private void miReplayOrLand_Click(object sender, EventArgs e)
    {
      if (env.IsDesign)
      {
        env.Pen = Block.Land;
        UpdateStatus();
      }
      else
      {
        env.IsReplay = true;
        workerThreadIsStop = false;
        UpdateStatus();
        ThreadPool.QueueUserWorkItem(WorkerThreadReplay, env.GetSteps());
      }
    }

    /// <summary>
    /// 回放通关步骤,使用后台工作线程
    /// </summary>
    /// <param name="steps">通关步骤</param>
    private void WorkerThreadReplay(object steps)
    {
      try
      {
        foreach (char c in (string)steps)
        {
          if (workerThreadIsStop) break;
          if (env.ReplayDelay > 0) Thread.Sleep(env.ReplayDelay);
          Step step = c;
          if (!env.StepIt(step.Direct, step.IsStop, out workerThreadInvalidRectangle)) break;
          Invoke(new EventHandler(WorkerThreadUpdateStatus));
        }
      }
      finally
      {
        env.IsReplay = false;
        Invoke(new EventHandler(WorkerThreadUpdateStatus));
      }
    }

    /// <summary>
    /// 更新主窗体状态
    /// </summary>
    /// <param name="sender">事件源</param>
    /// <param name="e">不包含任何事件数据的事件参数</param>
    void WorkerThreadUpdateStatus(object sender, EventArgs e)
    {
      Invalidate(workerThreadInvalidRectangle);
      UpdateStatus();
    }
  }
}

