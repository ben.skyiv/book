﻿namespace Skyiv.Ben.PushBox.Window
{
  partial class ErrorMsgDlg
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.tclMain = new System.Windows.Forms.TabControl();
      this.tpgBase = new System.Windows.Forms.TabPage();
      this.tbxBase = new System.Windows.Forms.TextBox();
      this.tpgAdv = new System.Windows.Forms.TabPage();
      this.tbxAdv = new System.Windows.Forms.TextBox();
      this.tclMain.SuspendLayout();
      this.tpgBase.SuspendLayout();
      this.tpgAdv.SuspendLayout();
      this.SuspendLayout();
      // 
      // tclMain
      // 
      this.tclMain.Controls.Add(this.tpgBase);
      this.tclMain.Controls.Add(this.tpgAdv);
      this.tclMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tclMain.Location = new System.Drawing.Point(0, 0);
      this.tclMain.Name = "tclMain";
      this.tclMain.SelectedIndex = 0;
      this.tclMain.Size = new System.Drawing.Size(240, 294);
      this.tclMain.TabIndex = 0;
      // 
      // tpgBase
      // 
      this.tpgBase.Controls.Add(this.tbxBase);
      this.tpgBase.Location = new System.Drawing.Point(0, 0);
      this.tpgBase.Name = "tpgBase";
      this.tpgBase.Size = new System.Drawing.Size(240, 271);
      this.tpgBase.Text = "简明";
      // 
      // tbxBase
      // 
      this.tbxBase.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxBase.Location = new System.Drawing.Point(0, 0);
      this.tbxBase.Multiline = true;
      this.tbxBase.Name = "tbxBase";
      this.tbxBase.ReadOnly = true;
      this.tbxBase.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbxBase.Size = new System.Drawing.Size(240, 271);
      this.tbxBase.TabIndex = 0;
      // 
      // tpgAdv
      // 
      this.tpgAdv.Controls.Add(this.tbxAdv);
      this.tpgAdv.Location = new System.Drawing.Point(0, 0);
      this.tpgAdv.Name = "tpgAdv";
      this.tpgAdv.Size = new System.Drawing.Size(240, 271);
      this.tpgAdv.Text = "详细";
      // 
      // tbxAdv
      // 
      this.tbxAdv.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxAdv.Location = new System.Drawing.Point(0, 0);
      this.tbxAdv.Multiline = true;
      this.tbxAdv.Name = "tbxAdv";
      this.tbxAdv.ReadOnly = true;
      this.tbxAdv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbxAdv.Size = new System.Drawing.Size(240, 271);
      this.tbxAdv.TabIndex = 0;
      // 
      // ErrorMsgDlg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 294);
      this.Controls.Add(this.tclMain);
      this.Name = "ErrorMsgDlg";
      this.Text = "错误信息 - 推箱子";
      this.tclMain.ResumeLayout(false);
      this.tpgBase.ResumeLayout(false);
      this.tpgAdv.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tclMain;
    private System.Windows.Forms.TabPage tpgBase;
    private System.Windows.Forms.TabPage tpgAdv;
    private System.Windows.Forms.TextBox tbxBase;
    private System.Windows.Forms.TextBox tbxAdv;
  }
}