using System;
using System.Windows.Forms;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// �����á��Ի���
  /// </summary>
  public partial class ConfigDlg : Form
  {
    public ConfigDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }

    public string[] Groups
    {
      get
      {
        string[] groups = new string[lbxGroup.Items.Count];
        for (int i = 0; i < lbxGroup.Items.Count; i++) groups[i] = lbxGroup.Items[i].ToString();
        return groups;
      }
      set
      {
        if (value != null)
        {
          lbxGroup.BeginUpdate();
          foreach (string group in value) lbxGroup.Items.Add(group);
          lbxGroup.EndUpdate();
          if (lbxGroup.Items.Count > 0) lbxGroup.SelectedIndex = 0;
        }
      }
    }

    private void btnAdd_Click(object sender, EventArgs e)
    {
      string s = tbxGroup.Text.Trim();
      if (s.Length == 0) return;
      int idx = lbxGroup.SelectedIndex;
      if (idx < 0)
      {
        lbxGroup.Items.Add(s);
        idx = lbxGroup.Items.Count - 1;
      }
      else lbxGroup.Items.Insert(idx, s);
      lbxGroup.SelectedIndex = idx;
    }

    private void btnDelete_Click(object sender, EventArgs e)
    {
      int idx = lbxGroup.SelectedIndex;
      if (idx < 0) return;
      lbxGroup.Items.RemoveAt(idx);
      if (lbxGroup.Items.Count <= 0) return;
      lbxGroup.SelectedIndex = (idx < lbxGroup.Items.Count) ? idx : (idx - 1);
    }

    private void btnUp_Click(object sender, EventArgs e)
    {
      int idx = lbxGroup.SelectedIndex;
      if (idx < 1) return;
      lbxGroup.Items.Insert(idx - 1, lbxGroup.SelectedItem);
      lbxGroup.Items.RemoveAt(idx + 1);
      lbxGroup.SelectedIndex = idx - 1;
    }

    private void btnDown_Click(object sender, EventArgs e)
    {
      int idx = lbxGroup.SelectedIndex;
      if (idx < 0 || idx >= lbxGroup.Items.Count - 1) return;
      lbxGroup.Items.Insert(idx + 2, lbxGroup.SelectedItem);
      lbxGroup.Items.RemoveAt(idx);
      lbxGroup.SelectedIndex = idx + 1;
    }
  }
}