﻿namespace Skyiv.Ben.PushBox.Window
{
  partial class ConfigDlg
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private System.Windows.Forms.MainMenu mnuMain;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.mnuMain = new System.Windows.Forms.MainMenu();
      this.lbxGroup = new System.Windows.Forms.ListBox();
      this.tbxGroup = new System.Windows.Forms.TextBox();
      this.btnSave = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnAdd = new System.Windows.Forms.Button();
      this.btnDelete = new System.Windows.Forms.Button();
      this.btnUp = new System.Windows.Forms.Button();
      this.btnDown = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // lbxGroup
      // 
      this.lbxGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbxGroup.Location = new System.Drawing.Point(3, 30);
      this.lbxGroup.Name = "lbxGroup";
      this.lbxGroup.Size = new System.Drawing.Size(182, 226);
      this.lbxGroup.TabIndex = 1;
      // 
      // tbxGroup
      // 
      this.tbxGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tbxGroup.Location = new System.Drawing.Point(3, 3);
      this.tbxGroup.MaxLength = 64;
      this.tbxGroup.Name = "tbxGroup";
      this.tbxGroup.Size = new System.Drawing.Size(182, 21);
      this.tbxGroup.TabIndex = 0;
      // 
      // btnSave
      // 
      this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnSave.Location = new System.Drawing.Point(191, 3);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(46, 20);
      this.btnSave.TabIndex = 2;
      this.btnSave.Text = "保存";
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(191, 29);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(46, 20);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "取消";
      // 
      // btnAdd
      // 
      this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnAdd.Location = new System.Drawing.Point(191, 55);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size(46, 20);
      this.btnAdd.TabIndex = 4;
      this.btnAdd.Text = "添加";
      this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
      // 
      // btnDelete
      // 
      this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnDelete.Location = new System.Drawing.Point(191, 81);
      this.btnDelete.Name = "btnDelete";
      this.btnDelete.Size = new System.Drawing.Size(46, 20);
      this.btnDelete.TabIndex = 5;
      this.btnDelete.Text = "删除";
      this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
      // 
      // btnUp
      // 
      this.btnUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnUp.Location = new System.Drawing.Point(191, 107);
      this.btnUp.Name = "btnUp";
      this.btnUp.Size = new System.Drawing.Size(46, 20);
      this.btnUp.TabIndex = 6;
      this.btnUp.Text = "上移";
      this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
      // 
      // btnDown
      // 
      this.btnDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnDown.Location = new System.Drawing.Point(191, 133);
      this.btnDown.Name = "btnDown";
      this.btnDown.Size = new System.Drawing.Size(46, 20);
      this.btnDown.TabIndex = 7;
      this.btnDown.Text = "下移";
      this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
      // 
      // ConfigDlg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Controls.Add(this.btnDown);
      this.Controls.Add(this.btnUp);
      this.Controls.Add(this.btnDelete);
      this.Controls.Add(this.btnAdd);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.tbxGroup);
      this.Controls.Add(this.lbxGroup);
      this.Menu = this.mnuMain;
      this.Name = "ConfigDlg";
      this.Text = "配置 - 推箱子";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListBox lbxGroup;
    private System.Windows.Forms.TextBox tbxGroup;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnAdd;
    private System.Windows.Forms.Button btnDelete;
    private System.Windows.Forms.Button btnUp;
    private System.Windows.Forms.Button btnDown;
  }
}