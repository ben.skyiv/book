using System;
using System.Windows.Forms;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// “关于”对话框
  /// </summary>
  public partial class AboutDlg : Form
  {
    public AboutDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      LoadVersionInfo();
      LoadSystemInfo();
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      LoadSystemInfo();
    }

    void LoadVersionInfo()
    {
      tbxVersion.Text = "skyiv (R) 推箱子" + Fcl.NewLine +
        "版本 " + Pub.GetVersionBuildString(Pub.Version) + Fcl.NewLine + Fcl.NewLine +
        "(C) Skyiv Studio (ben)" + Fcl.NewLine +
        "http://ben.skyiv.com" + Fcl.NewLine +
        "skyivben@gmail.com" + Fcl.NewLine + Fcl.NewLine +
        "推箱子是一款绿色软件，使用 C# 开发，基于 Microsoft .NET Compact Framework 2.0。" +
        "运行环境为使用 Microsoft Windows Mobile 6.0 的智能手机，也可以在安装有 " +
        "Microsoft .NET Framework 2.0 运行库的计算机上运行。";
    }

    void LoadSystemInfo()
    {
      tbxSystem.Text = "操作系统标识: " + Environment.OSVersion.Platform + Fcl.NewLine +
        "操作系统版本: " + Environment.OSVersion + Fcl.NewLine +
        "公共语言运行库版本: " + Environment.Version + Fcl.NewLine +
        "屏幕分辨率: " + Pub.ToString(Screen.PrimaryScreen.Bounds.Size) + Fcl.NewLine +
        "程序位置: " + Pub.CodeBases;
    }
  }
}