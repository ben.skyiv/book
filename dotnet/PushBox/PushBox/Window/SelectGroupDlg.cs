using System.Windows.Forms;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// ��ѡ�顱�Ի���
  /// </summary>
  public partial class SelectGroupDlg : Form
  {
    public string[] Groups { set { lbxGroup.DataSource = value; } }

    public int Group
    {
      get { return lbxGroup.SelectedIndex; }
      set { lbxGroup.SelectedIndex = value; }
    }

    public SelectGroupDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }
  }
}