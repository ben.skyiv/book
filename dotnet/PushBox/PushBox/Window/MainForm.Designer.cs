namespace Skyiv.Ben.PushBox.Window
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private System.Windows.Forms.MainMenu mnuMain;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.mnuMain = new System.Windows.Forms.MainMenu();
      this.miSystem = new System.Windows.Forms.MenuItem();
      this.miStatusbar = new System.Windows.Forms.MenuItem();
      this.miTopMost = new System.Windows.Forms.MenuItem();
      this.miOption = new System.Windows.Forms.MenuItem();
      this.miDash1 = new System.Windows.Forms.MenuItem();
      this.miLevl = new System.Windows.Forms.MenuItem();
      this.miFirstLevel = new System.Windows.Forms.MenuItem();
      this.miPrevLevel = new System.Windows.Forms.MenuItem();
      this.miNextLevel = new System.Windows.Forms.MenuItem();
      this.miLastLevel = new System.Windows.Forms.MenuItem();
      this.miSelectLevel = new System.Windows.Forms.MenuItem();
      this.miGroup = new System.Windows.Forms.MenuItem();
      this.miFirstGroup = new System.Windows.Forms.MenuItem();
      this.miPrevGroup = new System.Windows.Forms.MenuItem();
      this.miNextGroup = new System.Windows.Forms.MenuItem();
      this.miLastGroup = new System.Windows.Forms.MenuItem();
      this.miSelectGroup = new System.Windows.Forms.MenuItem();
      this.miData = new System.Windows.Forms.MenuItem();
      this.miConfig = new System.Windows.Forms.MenuItem();
      this.miTran = new System.Windows.Forms.MenuItem();
      this.miDesign = new System.Windows.Forms.MenuItem();
      this.miDash2 = new System.Windows.Forms.MenuItem();
      this.miHelp = new System.Windows.Forms.MenuItem();
      this.miErrorMsg = new System.Windows.Forms.MenuItem();
      this.miTopic = new System.Windows.Forms.MenuItem();
      this.miAbout = new System.Windows.Forms.MenuItem();
      this.miExit = new System.Windows.Forms.MenuItem();
      this.miRestartOrStopOrSave = new System.Windows.Forms.MenuItem();
      this.miUndoOrCancel = new System.Windows.Forms.MenuItem();
      this.miBackOrManOrBox = new System.Windows.Forms.MenuItem();
      this.miPrevLevel2OrSlot = new System.Windows.Forms.MenuItem();
      this.miNextLevel2OrWall = new System.Windows.Forms.MenuItem();
      this.miRecordOrBrick = new System.Windows.Forms.MenuItem();
      this.miReplayOrLand = new System.Windows.Forms.MenuItem();
      this.sbrMain = new System.Windows.Forms.StatusBar();
      this.SuspendLayout();
      // 
      // mnuMain
      // 
      this.mnuMain.MenuItems.Add(this.miSystem);
      this.mnuMain.MenuItems.Add(this.miRestartOrStopOrSave);
      this.mnuMain.MenuItems.Add(this.miUndoOrCancel);
      this.mnuMain.MenuItems.Add(this.miBackOrManOrBox);
      this.mnuMain.MenuItems.Add(this.miPrevLevel2OrSlot);
      this.mnuMain.MenuItems.Add(this.miNextLevel2OrWall);
      this.mnuMain.MenuItems.Add(this.miRecordOrBrick);
      this.mnuMain.MenuItems.Add(this.miReplayOrLand);
      // 
      // miSystem
      // 
      this.miSystem.MenuItems.Add(this.miStatusbar);
      this.miSystem.MenuItems.Add(this.miTopMost);
      this.miSystem.MenuItems.Add(this.miOption);
      this.miSystem.MenuItems.Add(this.miDash1);
      this.miSystem.MenuItems.Add(this.miLevl);
      this.miSystem.MenuItems.Add(this.miGroup);
      this.miSystem.MenuItems.Add(this.miData);
      this.miSystem.MenuItems.Add(this.miDash2);
      this.miSystem.MenuItems.Add(this.miHelp);
      this.miSystem.MenuItems.Add(this.miExit);
      this.miSystem.Text = "菜单";
      // 
      // miStatusbar
      // 
      this.miStatusbar.Checked = true;
      this.miStatusbar.Text = "状态栏";
      this.miStatusbar.Click += new System.EventHandler(this.miStatusbar_Click);
      // 
      // miTopMost
      // 
      this.miTopMost.Text = "前端显示";
      this.miTopMost.Click += new System.EventHandler(this.miTopMost_Click);
      // 
      // miOption
      // 
      this.miOption.Text = "选项";
      this.miOption.Click += new System.EventHandler(this.miOption_Click);
      // 
      // miDash1
      // 
      this.miDash1.Text = "-";
      // 
      // miLevl
      // 
      this.miLevl.MenuItems.Add(this.miFirstLevel);
      this.miLevl.MenuItems.Add(this.miPrevLevel);
      this.miLevl.MenuItems.Add(this.miNextLevel);
      this.miLevl.MenuItems.Add(this.miLastLevel);
      this.miLevl.MenuItems.Add(this.miSelectLevel);
      this.miLevl.Text = "关";
      // 
      // miFirstLevel
      // 
      this.miFirstLevel.Text = "首关";
      this.miFirstLevel.Click += new System.EventHandler(this.miFirstLevel_Click);
      // 
      // miPrevLevel
      // 
      this.miPrevLevel.Text = "前关";
      this.miPrevLevel.Click += new System.EventHandler(this.miPrevLevel2OrSlot_Click);
      // 
      // miNextLevel
      // 
      this.miNextLevel.Text = "后关";
      this.miNextLevel.Click += new System.EventHandler(this.miNextLevel2OrWall_Click);
      // 
      // miLastLevel
      // 
      this.miLastLevel.Text = "末关";
      this.miLastLevel.Click += new System.EventHandler(this.miLastLevel_Click);
      // 
      // miSelectLevel
      // 
      this.miSelectLevel.Text = "选关";
      this.miSelectLevel.Click += new System.EventHandler(this.miSelectLevel_Click);
      // 
      // miGroup
      // 
      this.miGroup.MenuItems.Add(this.miFirstGroup);
      this.miGroup.MenuItems.Add(this.miPrevGroup);
      this.miGroup.MenuItems.Add(this.miNextGroup);
      this.miGroup.MenuItems.Add(this.miLastGroup);
      this.miGroup.MenuItems.Add(this.miSelectGroup);
      this.miGroup.Text = "组";
      // 
      // miFirstGroup
      // 
      this.miFirstGroup.Text = "首组";
      this.miFirstGroup.Click += new System.EventHandler(this.miFirstGroup_Click);
      // 
      // miPrevGroup
      // 
      this.miPrevGroup.Text = "前组";
      this.miPrevGroup.Click += new System.EventHandler(this.miPrevGroup_Click);
      // 
      // miNextGroup
      // 
      this.miNextGroup.Text = "后组";
      this.miNextGroup.Click += new System.EventHandler(this.miNextGroup_Click);
      // 
      // miLastGroup
      // 
      this.miLastGroup.Text = "末组";
      this.miLastGroup.Click += new System.EventHandler(this.miLastGroup_Click);
      // 
      // miSelectGroup
      // 
      this.miSelectGroup.Text = "选组";
      this.miSelectGroup.Click += new System.EventHandler(this.miSelectGroup_Click);
      // 
      // miData
      // 
      this.miData.MenuItems.Add(this.miConfig);
      this.miData.MenuItems.Add(this.miTran);
      this.miData.MenuItems.Add(this.miDesign);
      this.miData.Text = "数据";
      // 
      // miConfig
      // 
      this.miConfig.Text = "配置";
      this.miConfig.Click += new System.EventHandler(this.miConfig_Click);
      // 
      // miTran
      // 
      this.miTran.Text = "转换";
      this.miTran.Click += new System.EventHandler(this.miTran_Click);
      // 
      // miDesign
      // 
      this.miDesign.Text = "设计";
      this.miDesign.Click += new System.EventHandler(this.miDesign_Click);
      // 
      // miDash2
      // 
      this.miDash2.Text = "-";
      // 
      // miHelp
      // 
      this.miHelp.MenuItems.Add(this.miErrorMsg);
      this.miHelp.MenuItems.Add(this.miTopic);
      this.miHelp.MenuItems.Add(this.miAbout);
      this.miHelp.Text = "帮助";
      // 
      // miErrorMsg
      // 
      this.miErrorMsg.Text = "错误信息";
      this.miErrorMsg.Click += new System.EventHandler(this.miErrorMsg_Click);
      // 
      // miTopic
      // 
      this.miTopic.Text = "帮助主题";
      this.miTopic.Click += new System.EventHandler(this.miTopic_Click);
      // 
      // miAbout
      // 
      this.miAbout.Text = "关于";
      this.miAbout.Click += new System.EventHandler(this.miAbout_Click);
      // 
      // miExit
      // 
      this.miExit.Text = "退出";
      this.miExit.Click += new System.EventHandler(this.miExit_Click);
      // 
      // miRestartOrStopOrSave
      // 
      this.miRestartOrStopOrSave.Text = "〓";
      this.miRestartOrStopOrSave.Click += new System.EventHandler(this.miRestartOrStopOrSave_Click);
      // 
      // miUndoOrCancel
      // 
      this.miUndoOrCancel.Text = "↖";
      this.miUndoOrCancel.Click += new System.EventHandler(this.miUndoOrCancel_Click);
      // 
      // miBackOrManOrBox
      // 
      this.miBackOrManOrBox.Text = "←";
      this.miBackOrManOrBox.Click += new System.EventHandler(this.miBackOrManOrBox_Click);
      // 
      // miPrevLevel2OrSlot
      // 
      this.miPrevLevel2OrSlot.Text = "▲";
      this.miPrevLevel2OrSlot.Click += new System.EventHandler(this.miPrevLevel2OrSlot_Click);
      // 
      // miNextLevel2OrWall
      // 
      this.miNextLevel2OrWall.Text = "▼";
      this.miNextLevel2OrWall.Click += new System.EventHandler(this.miNextLevel2OrWall_Click);
      // 
      // miRecordOrBrick
      // 
      this.miRecordOrBrick.Text = "●";
      this.miRecordOrBrick.Click += new System.EventHandler(this.miRecordOrBrick_Click);
      // 
      // miReplayOrLand
      // 
      this.miReplayOrLand.Text = "回放";
      this.miReplayOrLand.Click += new System.EventHandler(this.miReplayOrLand_Click);
      // 
      // sbrMain
      // 
      this.sbrMain.Location = new System.Drawing.Point(0, 246);
      this.sbrMain.Name = "sbrMain";
      this.sbrMain.Size = new System.Drawing.Size(240, 22);
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Controls.Add(this.sbrMain);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Menu = this.mnuMain;
      this.Name = "MainForm";
      this.Text = "推箱子";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.StatusBar sbrMain;
    private System.Windows.Forms.MenuItem miSystem;
    private System.Windows.Forms.MenuItem miExit;
    private System.Windows.Forms.MenuItem miLevl;
    private System.Windows.Forms.MenuItem miFirstLevel;
    private System.Windows.Forms.MenuItem miPrevLevel;
    private System.Windows.Forms.MenuItem miNextLevel;
    private System.Windows.Forms.MenuItem miLastLevel;
    private System.Windows.Forms.MenuItem miSelectLevel;
    private System.Windows.Forms.MenuItem miGroup;
    private System.Windows.Forms.MenuItem miFirstGroup;
    private System.Windows.Forms.MenuItem miPrevGroup;
    private System.Windows.Forms.MenuItem miNextGroup;
    private System.Windows.Forms.MenuItem miLastGroup;
    private System.Windows.Forms.MenuItem miSelectGroup;
    private System.Windows.Forms.MenuItem miRestartOrStopOrSave;
    private System.Windows.Forms.MenuItem miUndoOrCancel;
    private System.Windows.Forms.MenuItem miPrevLevel2OrSlot;
    private System.Windows.Forms.MenuItem miNextLevel2OrWall;
    private System.Windows.Forms.MenuItem miBackOrManOrBox;
    private System.Windows.Forms.MenuItem miStatusbar;
    private System.Windows.Forms.MenuItem miOption;
    private System.Windows.Forms.MenuItem miRecordOrBrick;
    private System.Windows.Forms.MenuItem miHelp;
    private System.Windows.Forms.MenuItem miErrorMsg;
    private System.Windows.Forms.MenuItem miTopic;
    private System.Windows.Forms.MenuItem miAbout;
    private System.Windows.Forms.MenuItem miTopMost;
    private System.Windows.Forms.MenuItem miDash1;
    private System.Windows.Forms.MenuItem miDash2;
    private System.Windows.Forms.MenuItem miData;
    private System.Windows.Forms.MenuItem miConfig;
    private System.Windows.Forms.MenuItem miTran;
    private System.Windows.Forms.MenuItem miDesign;
    private System.Windows.Forms.MenuItem miReplayOrLand;
  }
}

