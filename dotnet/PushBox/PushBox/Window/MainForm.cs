using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// 程序的主窗体
  /// </summary>
  public partial class MainForm : Form
  {
    Env env; // 工作环境

    public MainForm()
    {
      env = new Env();
      InitializeComponent();
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      miTopMost.Enabled = (Environment.OSVersion.Platform != PlatformID.WinCE);
      env.LoadConfig();
      env.LoadGroup();
      LoadLevel(true);
      if (env.IsSave) Restore(env.Steps);
    }

    protected override void OnClosing(CancelEventArgs e)
    {
      if (env.IsDesign || env.IsReplay)
      {
        MessageBox.Show("正在“" + (env.IsDesign ? "设计" : "回放") + "”，不能退出。请" +
          (env.IsDesign ? "先“保存”或者“放弃”。" : "等待“回放”完成，或者停止回放。"), "提醒");
        e.Cancel = true;
      }
      else
      {
        env.SaveConfig();
        env.Dispose();
      }
      base.OnClosing(e);
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      ClientSizeChanged();
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      if (!env.IsReplay && !env.IsDesign)
      {
        Direction dir = Direction.None;
        switch (e.KeyCode)
        {
          case Keys.Right: dir = Direction.East; break;
          case Keys.Down: dir = Direction.South; break;
          case Keys.Left: dir = Direction.West; break;
          case Keys.Up: dir = Direction.North; break;
        }
        StepIt(dir, true);
        e.Handled = true;
      }
      base.OnKeyDown(e);
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      base.OnMouseDown(e);
      if (!env.IsReplay) env.ToPixel = new Point(e.X, e.Y);
    }

    protected override void OnClick(EventArgs e)
    {
      if (env.IsDesign) Design();
      else if (!env.IsReplay && !TryPush()) TryMove();
      base.OnClick(e);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);
      env.Draw(e.Graphics, e.ClipRectangle);
    }

    private void miExit_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void miOption_Click(object sender, EventArgs e)
    {
      using (OptionDlg dlg = new OptionDlg(TopMost))
      {
        dlg.MaxLevelSize = env.MaxLevelSize;
        dlg.StepDelay = env.StepDelay;
        dlg.ReplayDelay = env.ReplayDelay;
        dlg.IsSave = env.IsSave;
        if (dlg.ShowDialog() == DialogResult.OK)
        {
          env.MaxLevelSize = dlg.MaxLevelSize;
          env.StepDelay = dlg.StepDelay;
          env.ReplayDelay = dlg.ReplayDelay;
          env.IsSave = dlg.IsSave;
        }
      }
    }

    private void miErrorMsg_Click(object sender, EventArgs e)
    {
      using (ErrorMsgDlg dlg = new ErrorMsgDlg(TopMost))
      {
        dlg.ErrorMsg = env.ErrorMsg;
        dlg.DebugMsg = env.DebugMsg;
        dlg.ShowDialog();
      }
    }

    private void miAbout_Click(object sender, EventArgs e)
    {
      using (Form dlg = new AboutDlg(TopMost))
      {
        dlg.ShowDialog();
      }
    }

    private void miTopic_Click(object sender, EventArgs e)
    {
      using (Form dlg = new TopicDlg(TopMost))
      {
        dlg.ShowDialog();
      }
    }

    private void miStatusbar_Click(object sender, EventArgs e)
    {
      miStatusbar.Checked = !miStatusbar.Checked;
      sbrMain.Visible = miStatusbar.Checked;
      ClientSizeChanged();
    }

    private void miTopMost_Click(object sender, EventArgs e)
    {
      TopMost = miTopMost.Checked = !miTopMost.Checked;
    }

    private void miRestartOrStopOrSave_Click(object sender, EventArgs e)
    {
      if (env.IsDesign)
      {
        env.SaveDesign();
        LoadLevel(true);
      }
      else if (env.IsReplay) workerThreadIsStop = true;
      else LoadLevel(false);
    }

    private void miUndoOrCancel_Click(object sender, EventArgs e)
    {
      if (env.IsDesign)
      {
        if (env.Active == Action.Create) env.Level = env.LeveLOem;
        LoadLevel(true);
      }
      else
      {
        for (bool isFirst = true; ; isFirst = false)
        {
          if (!isFirst && env.StepDelay > 0) Thread.Sleep(env.StepDelay);
          if (Back()) break;
        }
      }
    }

    private void miRecordOrBrick_Click(object sender, EventArgs e)
    {
      if (env.IsDesign)
      {
        env.Pen = Block.Brick;
        UpdateStatus();
      }
      else
      {
        env.Record();
        UpdateStatus();
        miRecordOrBrick.Enabled = false;
        Update();
      }
    }

    private void miBackOrManOrBox_Click(object sender, EventArgs e)
    {
      if (env.IsDesign)
      {
        env.Pen = env.HasWorker ? Block.Box0 : Block.Man0;
        UpdateStatus();
      }
      else Back();
    }

    private void miConfig_Click(object sender, EventArgs e)
    {
      using (ConfigDlg dlg = new ConfigDlg(TopMost))
      {
        dlg.Groups = env.Groups;
        if (dlg.ShowDialog() == DialogResult.OK)
        {
          env.Init();
          env.SaveConfig(dlg.Groups);
          env.LoadConfig();
          env.LoadGroup();
          LoadLevel(true);
        }
      }
    }

    private void miTran_Click(object sender, EventArgs e)
    {
      env.Dispose();
      using (TranDlg dlg = new TranDlg(TopMost))
      {
        dlg.MaxLevelSize = env.MaxLevelSize;
        dlg.Groups = env.Groups;
        dlg.ShowDialog();
      }
      env.Init();
      env.LoadGroup();
      LoadLevel(true);
    }

    private void miFirstLevel_Click(object sender, EventArgs e)
    {
      env.Level = 0;
      LoadLevel(true);
    }

    private void miPrevLevel2OrSlot_Click(object sender, EventArgs e)
    {
      if (env.IsDesign)
      {
        env.Pen = Block.Slot;
        UpdateStatus();
      }
      else
      {
        if (env.Level <= 0) return;
        env.Level--;
        LoadLevel(true);
      }
    }

    private void miNextLevel2OrWall_Click(object sender, EventArgs e)
    {
      if (env.IsDesign)
      {
        env.Pen = Block.Wall;
        UpdateStatus();
      }
      else
      {
        if (env.Level >= env.MaxLevel - 1) return;
        env.Level++;
        LoadLevel(true);
      }
    }

    private void miLastLevel_Click(object sender, EventArgs e)
    {
      env.Level = env.MaxLevel - 1;
      LoadLevel(true);
    }

    private void miSelectLevel_Click(object sender, EventArgs e)
    {
      using (SelectLevelDlg dlg = new SelectLevelDlg(TopMost))
      {
        dlg.MaxLevel = env.MaxLevel;
        dlg.Level = env.Level;
        if (dlg.ShowDialog() == DialogResult.OK)
        {
          env.Level = dlg.Level;
          LoadLevel(true);
        }
      }
    }

    private void miFirstGroup_Click(object sender, EventArgs e)
    {
      env.Group = 0;
      env.LoadGroup();
      LoadLevel(true);
    }

    private void miPrevGroup_Click(object sender, EventArgs e)
    {
      if (env.Group <= 0) return;
      env.Group--;
      env.LoadGroup();
      LoadLevel(true);
    }

    private void miNextGroup_Click(object sender, EventArgs e)
    {
      if (env.Group > env.Groups.Length - 1) return;
      env.Group++;
      env.LoadGroup();
      LoadLevel(true);
    }

    private void miLastGroup_Click(object sender, EventArgs e)
    {
      env.Group = env.Groups.Length - 1;
      env.LoadGroup();
      LoadLevel(true);
    }

    private void miSelectGroup_Click(object sender, EventArgs e)
    {
      using (SelectGroupDlg dlg = new SelectGroupDlg(TopMost))
      {
        dlg.Groups = env.Groups;
        dlg.Group = env.Group;
        if (dlg.ShowDialog() == DialogResult.OK)
        {
          env.Group = dlg.Group;
          env.LoadGroup();
          LoadLevel(true);
        }
      }
    }
  }
}