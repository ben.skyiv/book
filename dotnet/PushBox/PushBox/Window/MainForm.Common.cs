using System;
using System.Drawing;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  partial class MainForm
  {
    /// <summary>
    /// 装入当前关数据
    /// </summary>
    /// <param name="isReSizeClient">是否根据当前关尺寸改变主窗体客户区尺寸</param>
    void LoadLevel(bool isReSizeClient)
    {
      env.LoadLevel();
      if (isReSizeClient && Environment.OSVersion.Platform != PlatformID.WinCE)
        ClientSize = env.GetClientSize(sbrMain.Visible ? sbrMain.Height : 0);
      ClientSizeChanged();
      UpdateStatus();
    }

    /// <summary>
    /// 主窗体客户区尺寸改变时要采取的动作
    /// </summary>
    void ClientSizeChanged()
    {
      env.ClientSize = new Size(ClientSize.Width, ClientSize.Height +
        Pub.OverY - (sbrMain.Visible ? sbrMain.Height : 0));
      env.SetBoxInfo();
      Invalidate();
    }

    /// <summary>
    /// 恢复上次退出时保存的走法
    /// </summary>
    /// <param name="steps">走法</param>
    void Restore(string steps)
    {
      Rectangle invalid;
      foreach (char c in steps)
      {
        Step step = c;
        if (!env.StepIt(step.Direct, step.IsStop, out invalid)) break;
      }
      Invalidate();
      UpdateStatus();
    }

    /// <summary>
    /// 工人往指定方向前进一步(可能推着箱子)
    /// </summary>
    /// <param name="dir">前进的方向</param>
    /// <param name="isStop">“撤销”时是否停留</param>
    void StepIt(Direction dir, bool isStop)
    {
      Rectangle invalid;
      if (!env.StepIt(dir, isStop, out invalid)) return;
      Invalidate(invalid);
      UpdateStatus();
    }

    /// <summary>
    /// 工人后退一步(可能连带箱子一起后退)
    /// </summary>
    /// <returns>是否完成“撤消”</returns>
    bool Back()
    {
      Rectangle invalid;
      bool isStop = env.Back(out invalid);
      Invalidate(invalid);
      UpdateStatus();
      return isStop;
    }

    /// <summary>
    /// 尝试将工人移动到鼠标点击的位置
    /// </summary>
    /// <returns>是否成功</returns>
    bool TryMove()
    {
      Queue<Direction> moveQueue = env.GetMoveInfo();
      if (moveQueue == null) return false;
      if (moveQueue.Count <= 0) return false;
      int count = moveQueue.Count;
      while (moveQueue.Count > 0)
      {
        bool isStop = (count == moveQueue.Count);
        if (!isStop && env.StepDelay > 0) Thread.Sleep(env.StepDelay);
        StepIt(moveQueue.Dequeue(), isStop);
      }
      return true;
    }

    /// <summary>
    /// 尝试将箱子推动到鼠标点击的位置
    /// </summary>
    /// <returns>是否成功</returns>
    bool TryPush()
    {
      Direction dir;
      int steps = env.GetPushInfo(out dir);
      if (steps <= 0) return false;
      for (int i = 0; i < steps; i++)
      {
        bool isStop = (i == 0);
        if (!isStop && env.StepDelay > 0) Thread.Sleep(env.StepDelay);
        StepIt(dir, isStop);
      }
      return true;
    }

    /// <summary>
    /// 更新状态栏信息和菜单项
    /// </summary>
    void UpdateStatus()
    {
      sbrMain.Text = env.StatusMessage;
      miExit.Enabled = !env.IsReplay && !env.IsDesign;
      miSystem.Enabled = !env.IsReplay;
      miOption.Enabled = !env.HasError && !env.IsReplay;
      miSelectLevel.Enabled = !env.IsDesign && !env.HasError && !env.IsReplay && (!env.CanUndo || env.IsFinish);
      miSelectGroup.Enabled = !env.IsDesign && miSelectLevel.Enabled && env.Groups.Length > 1;
      miLastLevel.Enabled = miNextLevel.Enabled = miSelectLevel.Enabled && env.Level < env.MaxLevel - 1;
      miFirstLevel.Enabled = miPrevLevel.Enabled = miSelectLevel.Enabled && env.Level > 0;
      miLastGroup.Enabled = miNextGroup.Enabled = miSelectGroup.Enabled && env.Group < env.Groups.Length - 1;
      miFirstGroup.Enabled = miPrevGroup.Enabled = miSelectGroup.Enabled && env.Group > 0;
      miConfig.Enabled = miTran.Enabled = !env.IsDesign && !env.IsReplay && (!env.CanUndo || env.IsFinish);
      miDesign.Enabled = miSelectLevel.Enabled && !env.CanUndo;
      miRestartOrStopOrSave.Enabled = !env.HasError && (env.IsDesign ? (!env.IsReplay && env.HasWorker) : (env.IsReplay || env.CanUndo));
      miUndoOrCancel.Enabled = miBackOrManOrBox.Enabled = !env.HasError && !env.IsReplay && (env.IsDesign || env.CanUndo);
      miPrevLevel2OrSlot.Enabled = !env.HasError && !env.IsReplay && (env.IsDesign || (miSelectLevel.Enabled && env.Level > 0));
      miNextLevel2OrWall.Enabled = !env.HasError && !env.IsReplay && (env.IsDesign || (miSelectLevel.Enabled && env.Level < env.MaxLevel - 1));
      miRecordOrBrick.Enabled = !env.HasError && !env.IsReplay && (env.IsDesign || env.IsFinish);
      miReplayOrLand.Enabled = !env.HasError && !env.IsReplay && (env.IsDesign || env.CanReplay);
      miRestartOrStopOrSave.Text = !env.IsDesign ? (!env.IsReplay ? "〓" : "■") : "保存";
      miUndoOrCancel.Text = !env.IsDesign ? "↖" : "放弃";
      miBackOrManOrBox.Text = !env.IsDesign ? "←" : env.HasWorker ? "箱" : "人";
      miPrevLevel2OrSlot.Text = !env.IsDesign ? "▲" : "槽";
      miNextLevel2OrWall.Text = !env.IsDesign ? "▼" : "墙";
      miRecordOrBrick.Text = !env.IsDesign ? "●" : "砖";
      miReplayOrLand.Text = !env.IsDesign ? "回放" : "地";
      Update();
    }
  }
}
