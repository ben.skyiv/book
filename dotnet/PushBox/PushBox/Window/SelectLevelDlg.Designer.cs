﻿namespace Skyiv.Ben.PushBox.Window
{
  partial class SelectLevelDlg
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private System.Windows.Forms.MainMenu mnuMain;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.mnuMain = new System.Windows.Forms.MainMenu();
      this.nudMain = new System.Windows.Forms.NumericUpDown();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.lblMsg = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // nudMain
      // 
      this.nudMain.Location = new System.Drawing.Point(121, 52);
      this.nudMain.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMain.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMain.Name = "nudMain";
      this.nudMain.Size = new System.Drawing.Size(112, 22);
      this.nudMain.TabIndex = 2;
      this.nudMain.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // btnOk
      // 
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.Location = new System.Drawing.Point(3, 3);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(112, 20);
      this.btnOk.TabIndex = 0;
      this.btnOk.Text = "确定";
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(121, 3);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(112, 20);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "取消";
      // 
      // lblMsg
      // 
      this.lblMsg.Location = new System.Drawing.Point(3, 30);
      this.lblMsg.Name = "lblMsg";
      this.lblMsg.Size = new System.Drawing.Size(230, 20);
      this.lblMsg.Text = "可选的关数是从1到";
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(3, 54);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(114, 20);
      this.label2.Text = "请输入要玩的关数";
      // 
      // SelectLevelDlg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.lblMsg);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOk);
      this.Controls.Add(this.nudMain);
      this.Menu = this.mnuMain;
      this.Name = "SelectLevelDlg";
      this.Text = "选关 - 推箱子";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.NumericUpDown nudMain;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Label lblMsg;
    private System.Windows.Forms.Label label2;
  }
}