using System;
using System.Windows.Forms;
using Skyiv.Ben.PushBox.Common;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// “帮助”对话框
  /// </summary>
  public partial class TopicDlg : Form
  {
    public TopicDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      tbxLevel1.Text = "状态栏各项依次为:" + Fcl.NewLine +
        ">:正常 |:回放 +:新建 =:编辑" + Fcl.NewLine +
        "当前关数/总关数" + Fcl.NewLine +
        "当前关宽度x高度" + Fcl.NewLine +
        "已完成/总任务数 或 箱子=槽数" + Fcl.NewLine +
        "总步数(推箱子步数) 或 画笔" + Fcl.NewLine +
        "[通关的总步数(通关的推箱子步数)]" + Fcl.NewLine +
        "当前组名称" + Fcl.NewLine + Fcl.NewLine +
        "正常模式下菜单栏按钮依次为:" + Fcl.NewLine +
        "〓:重玩 ↖:撤消 ←:后退" + Fcl.NewLine +
        "▲:前关 ▼:后关 ●:录像" + Fcl.NewLine + Fcl.NewLine +
        "注意: 当工人能够推着箱子到达目的地时,总是优先推着箱子.如果需要绕过箱子,请先往旁边移动一下.";
      tbxLevel2.Text = "在“菜单 -> 数据 -> 设计”对话框中，“编辑”当前关时不能改变当前关的尺寸，" +
        "如需改变尺寸，可选中“复制当前关”复选框后使用“新建”。“删除”只能删除最后一关。" +
        "如需删除其它关，或者需要移动当前关到其它位置，可以在“菜单 -> 数据 -> 转换”对话框中进行" +
        "“导出”，然后“编辑”，最后再“导入”就行了。";
      tbxLevel3.Text = "文本文件格式:" + Fcl.NewLine +
        "-:空地 +:槽 #:墙 %:砖" + Fcl.NewLine +
        "x:箱子位于空地 X:箱子位于槽" + Fcl.NewLine +
        "(:工人位于空地 ):工人位于槽" + Fcl.NewLine +
        "第一行如果以!开头的话, 则为组名" + Fcl.NewLine +
        "以:开头的行为通关步骤" + Fcl.NewLine +
        "以'开头的行为注释" + Fcl.NewLine +
        "各关之间必须以空行分隔" + Fcl.NewLine + Fcl.NewLine +
        "通关步骤字符的含义:" + Fcl.NewLine +
        "+------+----------------+" + Fcl.NewLine +
        "|箱 停 | 无 东 南 西 北 |" + Fcl.NewLine +
        "+------+----------------+" + Fcl.NewLine +
        "|      |  A  B  C  D  E |" + Fcl.NewLine +
        "| x    |  F  G  H  I  J |" + Fcl.NewLine +
        "|    x |  K  L  M  N  O |" + Fcl.NewLine +
        "| x  x |  P  Q  R  S  T |" + Fcl.NewLine +
        "+------+----------------+";
      tbxLevel4.Text = "数据文件格式(括号中的数字为字节数):" + Fcl.NewLine +
        "文件头: 保留(4) 版本(1) BOX(3) 组名(16) 总关数(4) 地址表位置(4)" + Fcl.NewLine +
        "文件尾: 各关数据起始地址(4)列表" + Fcl.NewLine +
        "关数据: @(1) 通关标志(1) 总步数(4) 推箱子步数(4) 保留(14) 宽(4) 高(4) 数据(宽x高)";
    }
  }
}