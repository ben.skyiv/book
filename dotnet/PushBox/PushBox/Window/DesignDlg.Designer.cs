﻿namespace Skyiv.Ben.PushBox.Window
{
  partial class DesignDlg
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private System.Windows.Forms.MainMenu mnuMain;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.mnuMain = new System.Windows.Forms.MainMenu();
      this.nudWidth = new System.Windows.Forms.NumericUpDown();
      this.nudHeight = new System.Windows.Forms.NumericUpDown();
      this.rbnCreate = new System.Windows.Forms.RadioButton();
      this.rbnEdit = new System.Windows.Forms.RadioButton();
      this.rbnDelete = new System.Windows.Forms.RadioButton();
      this.label6 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.chkCopy = new System.Windows.Forms.CheckBox();
      this.label2 = new System.Windows.Forms.Label();
      this.tbxDeleteLevel = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.tbxLevel = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // btnOk
      // 
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.Location = new System.Drawing.Point(3, 3);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(112, 20);
      this.btnOk.TabIndex = 0;
      this.btnOk.Text = "确定";
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(121, 3);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(112, 20);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "取消";
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(3, 29);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(32, 20);
      this.label3.Text = "宽度";
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(3, 54);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(32, 20);
      this.label4.Text = "高度";
      // 
      // nudWidth
      // 
      this.nudWidth.Enabled = false;
      this.nudWidth.Location = new System.Drawing.Point(44, 26);
      this.nudWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudWidth.Name = "nudWidth";
      this.nudWidth.Size = new System.Drawing.Size(189, 22);
      this.nudWidth.TabIndex = 2;
      this.nudWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudHeight
      // 
      this.nudHeight.Enabled = false;
      this.nudHeight.Location = new System.Drawing.Point(44, 51);
      this.nudHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudHeight.Name = "nudHeight";
      this.nudHeight.Size = new System.Drawing.Size(189, 22);
      this.nudHeight.TabIndex = 3;
      this.nudHeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // rbnCreate
      // 
      this.rbnCreate.Checked = true;
      this.rbnCreate.Location = new System.Drawing.Point(175, 99);
      this.rbnCreate.Name = "rbnCreate";
      this.rbnCreate.Size = new System.Drawing.Size(58, 20);
      this.rbnCreate.TabIndex = 7;
      this.rbnCreate.Text = "新建";
      this.rbnCreate.CheckedChanged += new System.EventHandler(this.Action_CheckedChanged);
      // 
      // rbnEdit
      // 
      this.rbnEdit.Location = new System.Drawing.Point(175, 120);
      this.rbnEdit.Name = "rbnEdit";
      this.rbnEdit.Size = new System.Drawing.Size(58, 20);
      this.rbnEdit.TabIndex = 8;
      this.rbnEdit.TabStop = false;
      this.rbnEdit.Text = "编辑";
      this.rbnEdit.CheckedChanged += new System.EventHandler(this.Action_CheckedChanged);
      // 
      // rbnDelete
      // 
      this.rbnDelete.Location = new System.Drawing.Point(175, 141);
      this.rbnDelete.Name = "rbnDelete";
      this.rbnDelete.Size = new System.Drawing.Size(58, 20);
      this.rbnDelete.TabIndex = 9;
      this.rbnDelete.TabStop = false;
      this.rbnDelete.Text = "删除";
      this.rbnDelete.CheckedChanged += new System.EventHandler(this.Action_CheckedChanged);
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point(3, 77);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(114, 20);
      this.label6.Text = "当前关数/总关数";
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(3, 122);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(145, 20);
      this.label1.Text = "注意:只能删除最后一关";
      // 
      // chkCopy
      // 
      this.chkCopy.Location = new System.Drawing.Point(3, 99);
      this.chkCopy.Name = "chkCopy";
      this.chkCopy.Size = new System.Drawing.Size(138, 20);
      this.chkCopy.TabIndex = 5;
      this.chkCopy.Text = "复制当前关";
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(3, 143);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(75, 20);
      this.label2.Text = "确认删除第";
      // 
      // tbxDeleteLevel
      // 
      this.tbxDeleteLevel.Location = new System.Drawing.Point(76, 141);
      this.tbxDeleteLevel.MaxLength = 5;
      this.tbxDeleteLevel.Name = "tbxDeleteLevel";
      this.tbxDeleteLevel.Size = new System.Drawing.Size(41, 21);
      this.tbxDeleteLevel.TabIndex = 6;
      this.tbxDeleteLevel.TextChanged += new System.EventHandler(this.tbxDeleteLevel_TextChanged);
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(123, 143);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(28, 20);
      this.label5.Text = "关";
      // 
      // tbxLevel
      // 
      this.tbxLevel.Location = new System.Drawing.Point(118, 75);
      this.tbxLevel.Name = "tbxLevel";
      this.tbxLevel.ReadOnly = true;
      this.tbxLevel.Size = new System.Drawing.Size(115, 21);
      this.tbxLevel.TabIndex = 4;
      // 
      // DesignDlg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Controls.Add(this.tbxLevel);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.tbxDeleteLevel);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.chkCopy);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.rbnDelete);
      this.Controls.Add(this.rbnEdit);
      this.Controls.Add(this.rbnCreate);
      this.Controls.Add(this.nudHeight);
      this.Controls.Add(this.nudWidth);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOk);
      this.Menu = this.mnuMain;
      this.Name = "DesignDlg";
      this.Text = "设计 - 推箱子";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.NumericUpDown nudWidth;
    private System.Windows.Forms.NumericUpDown nudHeight;
    private System.Windows.Forms.RadioButton rbnCreate;
    private System.Windows.Forms.RadioButton rbnEdit;
    private System.Windows.Forms.RadioButton rbnDelete;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.CheckBox chkCopy;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox tbxDeleteLevel;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox tbxLevel;
  }
}