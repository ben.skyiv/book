﻿namespace Skyiv.Ben.PushBox.Window
{
  partial class TopicDlg
  {
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
      this.tclMain = new System.Windows.Forms.TabControl();
      this.tpgLevel1 = new System.Windows.Forms.TabPage();
      this.tbxLevel1 = new System.Windows.Forms.TextBox();
      this.tpgLevel2 = new System.Windows.Forms.TabPage();
      this.tbxLevel2 = new System.Windows.Forms.TextBox();
      this.tpgLevel3 = new System.Windows.Forms.TabPage();
      this.tpgLevel4 = new System.Windows.Forms.TabPage();
      this.tbxLevel3 = new System.Windows.Forms.TextBox();
      this.tbxLevel4 = new System.Windows.Forms.TextBox();
      this.tclMain.SuspendLayout();
      this.tpgLevel1.SuspendLayout();
      this.tpgLevel2.SuspendLayout();
      this.tpgLevel3.SuspendLayout();
      this.tpgLevel4.SuspendLayout();
      this.SuspendLayout();
      // 
      // tclMain
      // 
      this.tclMain.Controls.Add(this.tpgLevel1);
      this.tclMain.Controls.Add(this.tpgLevel2);
      this.tclMain.Controls.Add(this.tpgLevel3);
      this.tclMain.Controls.Add(this.tpgLevel4);
      this.tclMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tclMain.Location = new System.Drawing.Point(0, 0);
      this.tclMain.Name = "tclMain";
      this.tclMain.SelectedIndex = 0;
      this.tclMain.Size = new System.Drawing.Size(240, 294);
      this.tclMain.TabIndex = 0;
      // 
      // tpgLevel1
      // 
      this.tpgLevel1.Controls.Add(this.tbxLevel1);
      this.tpgLevel1.Location = new System.Drawing.Point(0, 0);
      this.tpgLevel1.Name = "tpgLevel1";
      this.tpgLevel1.Size = new System.Drawing.Size(240, 271);
      this.tpgLevel1.Text = "入门";
      // 
      // tbxLevel1
      // 
      this.tbxLevel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxLevel1.Location = new System.Drawing.Point(0, 0);
      this.tbxLevel1.Multiline = true;
      this.tbxLevel1.Name = "tbxLevel1";
      this.tbxLevel1.ReadOnly = true;
      this.tbxLevel1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbxLevel1.Size = new System.Drawing.Size(240, 271);
      this.tbxLevel1.TabIndex = 0;
      // 
      // tpgLevel2
      // 
      this.tpgLevel2.Controls.Add(this.tbxLevel2);
      this.tpgLevel2.Location = new System.Drawing.Point(0, 0);
      this.tpgLevel2.Name = "tpgLevel2";
      this.tpgLevel2.Size = new System.Drawing.Size(240, 271);
      this.tpgLevel2.Text = "进阶";
      // 
      // tbxLevel2
      // 
      this.tbxLevel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxLevel2.Location = new System.Drawing.Point(0, 0);
      this.tbxLevel2.Multiline = true;
      this.tbxLevel2.Name = "tbxLevel2";
      this.tbxLevel2.ReadOnly = true;
      this.tbxLevel2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbxLevel2.Size = new System.Drawing.Size(240, 271);
      this.tbxLevel2.TabIndex = 0;
      // 
      // tpgLevel3
      // 
      this.tpgLevel3.Controls.Add(this.tbxLevel3);
      this.tpgLevel3.Location = new System.Drawing.Point(0, 0);
      this.tpgLevel3.Name = "tpgLevel3";
      this.tpgLevel3.Size = new System.Drawing.Size(240, 271);
      this.tpgLevel3.Text = "高级";
      // 
      // tpgLevel4
      // 
      this.tpgLevel4.Controls.Add(this.tbxLevel4);
      this.tpgLevel4.Location = new System.Drawing.Point(0, 0);
      this.tpgLevel4.Name = "tpgLevel4";
      this.tpgLevel4.Size = new System.Drawing.Size(240, 271);
      this.tpgLevel4.Text = "专家";
      // 
      // tbxLevel3
      // 
      this.tbxLevel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxLevel3.Location = new System.Drawing.Point(0, 0);
      this.tbxLevel3.Multiline = true;
      this.tbxLevel3.Name = "tbxLevel3";
      this.tbxLevel3.ReadOnly = true;
      this.tbxLevel3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbxLevel3.Size = new System.Drawing.Size(240, 271);
      this.tbxLevel3.TabIndex = 0;
      // 
      // tbxLevel4
      // 
      this.tbxLevel4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxLevel4.Location = new System.Drawing.Point(0, 0);
      this.tbxLevel4.Multiline = true;
      this.tbxLevel4.Name = "tbxLevel4";
      this.tbxLevel4.ReadOnly = true;
      this.tbxLevel4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbxLevel4.Size = new System.Drawing.Size(240, 271);
      this.tbxLevel4.TabIndex = 0;
      // 
      // TopicDlg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 294);
      this.Controls.Add(this.tclMain);
      this.Name = "TopicDlg";
      this.Text = "帮助 - 推箱子";
      this.tclMain.ResumeLayout(false);
      this.tpgLevel1.ResumeLayout(false);
      this.tpgLevel2.ResumeLayout(false);
      this.tpgLevel3.ResumeLayout(false);
      this.tpgLevel4.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tclMain;
    private System.Windows.Forms.TabPage tpgLevel1;
    private System.Windows.Forms.TabPage tpgLevel2;
    private System.Windows.Forms.TabPage tpgLevel3;
    private System.Windows.Forms.TabPage tpgLevel4;
    private System.Windows.Forms.TextBox tbxLevel1;
    private System.Windows.Forms.TextBox tbxLevel2;
    private System.Windows.Forms.TextBox tbxLevel3;
    private System.Windows.Forms.TextBox tbxLevel4;
  }
}