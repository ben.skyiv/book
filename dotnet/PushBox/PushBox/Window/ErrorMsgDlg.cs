using System.Windows.Forms;

namespace Skyiv.Ben.PushBox.Window
{
  /// <summary>
  /// ��������Ϣ���Ի���
  /// </summary>
  public partial class ErrorMsgDlg : Form
  {
    public string ErrorMsg { set { tbxBase.Text = value; } }
    public string DebugMsg { set { tbxAdv.Text = value; } }

    public ErrorMsgDlg(bool isTopMost)
    {
      InitializeComponent();
      TopMost = isTopMost;
    }
  }
}