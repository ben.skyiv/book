﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

static class Program
{
  static readonly Encoding Encode = Encoding.GetEncoding("GB18030");
  static readonly string workDirectory = "/home/ben/Documents/ICBC/data/";
  static readonly string[] dirs = { "a-2001-08" };
  static readonly string value = "14060498090080030";

  static void Main(string[] args)
  {
    Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
    foreach (var dir in dirs)
      foreach (var file in Directory.EnumerateFiles(
        Path.Combine(workDirectory, dir), "*.zip"))
        using (var zip = ZipFile.OpenRead(file))
          foreach (var arch in zip.Entries)
            using (var reader = new StreamReader(arch.Open(), Encode))
              for (string s; (s = reader.ReadLine()) != null; )
                if (s.Contains(value))
                  Console.WriteLine("{0}: {1}", arch.FullName, s);
  }
}
