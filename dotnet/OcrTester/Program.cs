﻿using System;
using PaddleOCRSharp;

static class OcrTester
{
  static void Main(string[] args)
  {
    if (args.Length == 0) {
      Console.WriteLine("OcrTester.exe image_file_name");
      return;
    }
    OCRModelConfig config = null;
    OCRParameter oCRParameter = new  OCRParameter();
    OCRResult ocrResult;
    using (var engine = new PaddleOCREngine(config, oCRParameter))
    {
      ocrResult = engine.DetectText(args[0]);
    }
    if (ocrResult != null) Console.WriteLine(ocrResult.Text);
  }
}
