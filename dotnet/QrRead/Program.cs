﻿using System;
using OpenCvSharp;

static class QrRead
{
    static void Main()
    {
        Console.WriteLine("OS : " + Environment.OSVersion);
        Console.WriteLine("CLR: " + Environment.Version);
        var fileName = "/home/ben/src/book/CTF/test/qr.png";
        var src = Cv2.ImRead(fileName);
        Console.WriteLine(src);
    }
}
