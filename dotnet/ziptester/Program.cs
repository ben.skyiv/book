﻿using System;
using System.IO;
using System.IO.Compression;

static class ZipTester
{
  static void Test1(string zipFileName)
  {
    if (File.Exists(zipFileName)) File.Delete(zipFileName);
    ZipFile.CreateFromDirectory("/home/ben/src/book/etc", zipFileName);
  }
  
  static void Test2(string zipFileName)
  {
    string[] files = { "Program.cs", "ziptester.csproj" };
    if (File.Exists(zipFileName)) File.Delete(zipFileName);
    using (var zip = ZipFile.Open(zipFileName, ZipArchiveMode.Create))
      foreach (var file in files)
        zip.CreateEntryFromFile(file, Path.GetFileName(file), CompressionLevel.Optimal);
  }

  static void Main()
  {
    Test1("a.zip");
    //Test2("a.zip");
  }
}
