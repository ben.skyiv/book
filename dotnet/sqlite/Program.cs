﻿using System;
using System.Data.SQLite;

Console.WriteLine("OS    : " + Environment.OSVersion);
Console.WriteLine("CLR   : " + Environment.Version);
using var con = new SQLiteConnection("Data Source=:memory:");
con.Open();
using var cmd = new SQLiteCommand("SELECT SQLITE_VERSION()", con);
Console.WriteLine("SQLite: " + cmd.ExecuteScalar());    
