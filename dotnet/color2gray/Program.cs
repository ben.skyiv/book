﻿using System;
using System.IO;

static class Golor2Gray
{
    static readonly string key = "setrgbcolor";

    static void Main(string[] args)
    {
        var home = Environment.GetEnvironmentVariable("HOME") ?? "";
        var src = Path.Combine(home, "src/taocp/work/figs");
        var dest = Path.Combine(home, "tmp/gray");
        if (Directory.Exists(dest)) Directory.Delete(dest, true);
        Directory.CreateDirectory(dest);
        foreach (var path in Directory.EnumerateFiles(src, "v4b.*")) {
            var file = Path.GetFileName(path);
            if (file.EndsWith(".mp") || file.EndsWith(".mpx")) continue;
            Console.WriteLine(file);
            using (var sw = new StreamWriter(Path.Combine(dest, file)))
            foreach (var s in File.ReadLines(path)) sw.WriteLine(ToGray(s));
        }
    }

    static string ToGray(string s)
    {
        if (!s.Contains(key)) return s;
        var ss = s.Trim().Split();
        if (ss[3] != key) return s;
        var r = double.Parse(ss[0]);
        var g = double.Parse(ss[1]);
        var b = double.Parse(ss[2]);
        if (r == g && r == b) return s;
        var gray = 0.299 * r + 0.587 * g + 0.114 * b;
        ss[0] = ss[1] = ss[2] = Math.Round(gray, 3).ToString();
        return string.Join(' ', ss);
    }
}
