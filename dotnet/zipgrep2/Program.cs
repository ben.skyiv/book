﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Collections.Generic;

static class Program
{
  static readonly Encoding Encode = Encoding.GetEncoding("GB18030");
  static readonly string workDirectory = "/home/ben/Documents/ICBC/data/";
  static readonly string[] dirs = { "e-20010609-20011231" }; //{ "a-2001-08" };
  static readonly string value = "14060498090080030";
  static readonly int namePreLength = "a-20011223-".Length;
  static HashSet<string> names = new HashSet<string>();
  
  static void Main(string[] args)
  {
    Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
    foreach (var dir in dirs)
      foreach (var file in Directory.EnumerateFiles(
        Path.Combine(workDirectory, dir), "*.zip"))
        using (var zip = ZipFile.OpenRead(file))
          foreach (var arch in zip.Entries)
            using (var reader = new StreamReader(arch.Open(), Encode))
              for (string s; ; ) {
                try { s = reader.ReadLine(); }
                catch (Exception) { Console.WriteLine("Skip: " + file); continue; }
                if (s == null) break;
                if (s.Contains(value)) {
                  var name = arch.Name;
                  Console.WriteLine("{0}", name);
                  names.Add(name.Substring(namePreLength));
                }
              }
    Console.WriteLine();
    foreach (var name in names) Console.WriteLine(name);
  }
}
